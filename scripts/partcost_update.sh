#!/usr/bin/ksh
#
# This script will run after DailyReports2.sh runs from cron at 6AM.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#
#  This is to allow for a prompt to run the daily reports

sql << //
truncate table usr_parts;
commit;
exit
//

. /opt/mchugh/prod/les/db/data/load/dload parts parts.csv

cd ${LESDIR}/scripts

sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/ItemCostsUpdate.out
@ItemCostsUpdate.sql
spool off
exit
//
exit 0
