/* Created by Al Driver */
/* This should run twice daily, once at noon and once in the evening */

/* Script checks for any records in locmst that have the asgflg checked */
/* ,but do not have an associated record in the rplcfg table */
/* Wal4 - Wal10 should not appear */

select stoloc,rescod from locmst
where asgflg =1
and not exists
(select 'x' from rplcfg where rplcfg.stoloc = locmst.stoloc);

select stoloc,rescod from locmst
where asgflg =0
and exists
(select 'x' from rplcfg where rplcfg.stoloc = locmst.stoloc);

/* update locmst */
/* set asgflg =0 */
/* where asgflg =1 */
/* and not exists */
/* (select 'x' from rplcfg where rplcfg.stoloc = locmst.stoloc); */
/* commit; */

/* update locmst */
/* set asgflg =1 */
/* where asgflg =0 */
/* and exists */
/* (select 'x' from rplcfg where rplcfg.stoloc = locmst.stoloc); */
/* commit; */
