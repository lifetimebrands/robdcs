 select * from invlod where lodnum = '00000000000014219535';

LODNUM                         STOLOC                   LODWGT     PRMFLG
------------------------------ -------------------- ---------- ----------
    UNKFLG     MVLFLG ADDDTE    LSTMOV    LSTDTE    LSTCOD
---------- ---------- --------- --------- --------- ----------
LST_USR_ID                               LODUCC               UCCDTE
---------------------------------------- -------------------- ---------
PALPOS
--------------------
00000000000014219535           SHIP-33B029                              0
         0          1 12-DEC-05 13-DEC-05 13-DEC-05 GENMOV
NAHUNM



SQL> update invlod set stoloc = 'FSTG01' where lodnum = '00000000000014219535';

1 row updated.

SQL> commit;

Commit complete.

SQL> select ship_id, carcod, srvlvl from shipment where ship_id = 'SID0307044
  2  ';

no rows selected

SQL> select ship_id, carcod, srvlvl from shipment where ship_id = 'SID0307044';

SHIP_ID                        CARCOD     SRVLVL
------------------------------ ---------- ----------
SID0307044                     XXXX       CC

SQL> update shipment set carcod = 'UPSG'  where ship_id = 'SID0307044';

1 row updated.

SQL> commit;

Commit complete.

SQL> select appqty, pckqty, cmbcod from pckwrk where ship_id = 'SID0307044';

    APPQTY     PCKQTY CMBCOD
---------- ---------- ----------
         3          3 CMB4720033
         3          3 CMB4782007
         1          1 CMB4783542

SQL> update pckmov set arecod 'FSTG', stoloc = 'FSTG01' where cmbcod in ('CMB4720033', 'CMB4782007', 'CMB4783542');
update pckmov set arecod 'FSTG', stoloc = 'FSTG01' where cmbcod in ('CMB4720033', 'CMB4782007', 'CMB4783542')
                         *
ERROR at line 1:
ORA-00927: missing equal sign


SQL> update pckmov set arecod = 'FSTG', stoloc = 'FSTG01' where cmbcod in ('CMB4720033', 'CMB4782007', 'CMB4783542');

3 rows updated.

SQL> commit;

Commit complete.

SQL> select srcloc, prtnum, cmbcod, subucc, adddte from pckwrk where wrkref = ''FSTG', stoloc = 'FSTG01' where cmbcod in ('CMB4720033', 'CMB4782007', 'CMB4783542');
ERROR:
ORA-01756: quoted string not properly terminated


SQL> select srcloc, prtnum, cmbcod, subucc, adddte from pckwrk where wrkref = 'WRK3659551';

SRCLOC               PRTNUM                         CMBCOD
-------------------- ------------------------------ ----------
SUBUCC               ADDDTE
-------------------- ---------
WESTSAMP54           KS051OB                        CMB4716258
                     08-DEC-05


SQL> select srcloc, prtnum, cmbcod, subucc, adddte from pckwrk where cmbcod = 'CMB4716258';

SRCLOC               PRTNUM                         CMBCOD
-------------------- ------------------------------ ----------
SUBUCC               ADDDTE
-------------------- ---------
WESTSAMP54           KS051OB                        CMB4716258
                     08-DEC-05

T2C164               542                            CMB4716258
00000530050089867984 28-APR-05


SQL> delete from pckwrk where cmbcod = 'CMB4716258' and subucc = '00000530050089867984';

1 row deleted.

SQL> commit;

Commit complete.

