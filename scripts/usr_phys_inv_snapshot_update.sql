/**********************************************************************************************************/
/* This script will populate the phys_inv_snapshot table prior to the start of the Physical Inventory.    */
/* This information is used in reporting.  A spreadsheet of this information should also be generated and */
/* placed in the pcall directory in a Physical Inventory folder . The table should be truncated first to  */
/* to get rid of last years data.                                                                         */
/* Trailer information is also stored where the shipment has been closed, but not dispatched              */
/**********************************************************************************************************/

alter session set nls_date_format='DD-MON-YY';

truncate table phys_inv_snapshot;

/* Populate table with current data */

insert into phys_inv_snapshot(prtnum,arecod,adddte,untqty,stoloc,lotnum,invsts,untpak,untcas,
gentyp,prt_client_id,untcst,case_cube, loc_cube) 
(
select ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(a.arecod)) arecod,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.invsts, d.untpak, d.untcas,  
       'INITIAL' gentyp, '----' prt_client_id, e.untcst,
       round((f.caslen*f.caswid*f.cashgt)/1728,2) case_cube,
       round((sum(d.untqty)/d.untcas)*round((f.caslen*f.caswid*f.cashgt)/1728,2),2) loc_cube
from aremst a,locmst m, invlod l, invsub s, invdtl d, prtmst e, ftpmst f
 where a.fwiflg = 1 and
       a.arecod != 'SHIP' and
       a.arecod  = m.arecod and
       m.stoloc  = l.stoloc and
       l.lodnum  = s.lodnum and
       s.subnum  = d.subnum
       and d.prtnum=e.prtnum
       and e.prt_client_id='----'
       and d.ftpcod=f.ftpcod
 group
    by d.prtnum,
       a.arecod,
       m.stoloc,
       d.lotnum,
       d.invsts,
       d.untpak,
       d.untcas,
       e.untcst, f.caslen, f.caswid, f.cashgt)
union
select ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(a.arecod)) arecod,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.invsts, d.untpak, d.untcas,
       'INITIAL' gentyp, '----' prt_client_id, e.untcst,
       round((f.caslen*f.caswid*f.cashgt)/1728,2) case_cube,
       round((sum(d.untqty)/d.untcas)*round((f.caslen*f.caswid*f.cashgt)/1728,2),2) loc_cube
from aremst a,locmst m, invlod l, invsub s, invdtl d, prtmst e, ftpmst f
 where a.fwiflg = 0 and
       a.arecod = 'PROD' and
       a.arecod  = m.arecod and
       m.stoloc  = l.stoloc and
       l.lodnum  = s.lodnum and
       s.subnum  = d.subnum
       and d.prtnum  = e.prtnum
       and e.prt_client_id='----'
       and d.ftpcod=f.ftpcod
 group
    by d.prtnum,
       a.arecod,
       m.stoloc,
       d.lotnum,
       d.invsts,
       d.untpak,
       d.untcas,
       e.untcst, f.caslen, f.caswid, f.cashgt
union
select ltrim(rtrim(d.prtnum)) prtnum,
             ltrim(rtrim(a.arecod)) arecod,
             ltrim(rtrim(sysdate))  adddte,
             sum(d.untqty) untqty,
             ltrim(rtrim(trlr.stoloc)) stoloc,
             ltrim(rtrim(d.lotnum)) lotnum,
             d.invsts, d.untpak, d.untcas,'FINAL' gentyp, '----' prt_client_id, e.untcst,
       round((f.caslen*f.caswid*f.cashgt)/1728,2) case_cube,
       round((sum(d.untqty)/d.untcas)*round((f.caslen*f.caswid*f.cashgt)/1728,2),2) loc_cube
             from invdtl d, invsub s, invlod l, locmst m, aremst a,
             pckmov, pckwrk, shipment, stop, 
            car_move, trlr, shipment_line, prtmst e, ftpmst f
            where d.subnum = s.subnum
            and d.ship_line_id = shipment_line.ship_line_id
            and shipment_line.ship_id = shipment.ship_id and
            shipment.stop_id  = stop.stop_id
            and stop.car_move_id  = car_move.car_move_id
            and car_move.trlr_id
            =trlr.trlr_id
            and trlr.trlr_id = m.stoloc
            and trlr.trlr_stat   in  ('O','L','H','C','P')
            and shipment.loddte is not null
            and d.ship_line_id  =
            pckwrk.ship_line_id  and d.wrkref = pckwrk.wrkref
            and d.prtnum = pckwrk.prtnum and d.prt_client_id = pckwrk.prt_client_id
            and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
            = pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY')
            and pckmov.arecod = a.arecod
            and l.lodnum  = s.lodnum
            and s.subnum  = d.subnum
            and d.prtnum = e.prtnum
            and e.prt_client_id='----'
            and d.ftpcod=f.ftpcod
     group by d.prtnum,
            a.arecod,
            trlr.stoloc,
            d.lotnum,
            d.invsts,
            d.untpak,
            d.untcas,
            e.untcst, f.caslen, f.caswid, f.cashgt
/
commit;

spool /opt/mchugh/prod/les/oraclereports/phys_inv_snapshot&1..txt

select * from phys_inv_snapshot;

spool off;
