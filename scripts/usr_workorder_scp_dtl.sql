
/* Update script is used to insert yesterdays previous scapped detail records */
/* into the usr_workorder_scrap table.  For use in the IC-Workorder Scrapped Detail Report */

alter session set nls_date_format ='dd-mon-yyyy';

insert into usr_workorder_scrap
select clsdte, wkodtl.prtnum, rpt_scpqty, (rpt_scpqty * untcst) cost, rtrim(wkohdr.wkonum) wkonum,
NULL dstloc,wkodtl.mod_usr_id, untcst
from wkohdr, wkodtl,prtmst
where
wkohdr.client_id = wkodtl.client_id
and wkohdr.wkonum = wkodtl.wkonum
and wkodtl.prtnum = prtmst.prtnum
and rpt_scpqty <> 0
and trunc(clsdte) = trunc(sysdate) -1 
order by clsdte
/
