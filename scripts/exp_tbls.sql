To export table data (single)

EXP username/password@instance TABLES=(tablename)

exp scott/tiger tables=emp query="where job='SALESMAN' and sal<1600"

  exp scott/tiger file=emp.dmp log=emp.log tables=emp rows=yes indexes=no
         exp scott/tiger file=emp.dmp tables=(emp,dept)

         imp scott/tiger file=emp.dmp full=yes
         imp scott/tiger file=emp.dmp fromuser=scott touser=scott tables=dept

         exp userid=scott/tiger@orcl parfile=export.txt

         ... where export.txt contains:
		 	 BUFFER=100000
			 FILE=account.dmp
			 FULL=n
			 OWNER=scott
			 GRANTS=y
			 COMPRESS=y

NOTE: If you do not like command line utilities, you can import and export data with the "Schema Manager" GUI that ships with Oracle Enterprise Manager (OEM). 

Back to top of file 
--------------------------------------------------------------------------------

Can one export a subset of a table?
From Oracle8i one can use the QUERY= export parameter to selectively unload a subset of the data from a table. Look at this example: 
         exp scott/tiger tables=emp query=\"where deptno=10\"



To export table data (Multiple tables)
set heading off

select decode( rownum, 1, 'tables=(', ',' ), table_name
  from user_tables
 where table_name like 'E%'
union all
select ')', null
  from dual ;

Would return... 
tables=( E
,        EMP
,        EMP2
,        EMPLOYEE
,        EMP_DEMO
,        EMP_DEPT
,        EMP_SNAPSHOT
)

(spool that to a file) and use parfile
