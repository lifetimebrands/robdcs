#!/usr/bin/ksh
. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts

sql << //
truncate table usr_upsinfo_bak;
truncate table usr_upsinfo_pre;
exit
//

/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/URC_PRE.ctl -D /opt/mchugh/prod/les/db/data/load/URC_PRE -d urc995a.csv

exit 0
