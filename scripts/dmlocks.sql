/*
$Log: dmlocks.sql,v $
Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
Initial Import

Revision 1.1  2001/06/11  20:22:28  20:22:28  lh51sh
System Test Start

# Revision 1.10  1996/11/27  19:29:36  dmp_sw
# freeze for V4_2
#
# Revision 1.9  1996/08/09  18:41:04  dmp_sw
# freeze for V4_10
#
# Revision 1.8  1996/04/19  14:11:27  dmp_sw
# freeze for V4_09
#
# Revision 1.7  1996/02/01  15:11:55  phx_sw
# freeze for V4_08
#
# Revision 1.6  1996/01/04  20:51:21  phx_sw
# Converted to more common oracle views
#
# Revision 1.5  1995/11/09  20:44:39  phx_sw
# freeze for V4_07
#
# Revision 1.4  1995/10/09  15:25:12  phx_dp
# freeze for V4_06
#
# Revision 1.3  1995/09/07  23:47:54  phx_dp
# freeze for V4.05
#
# Revision 1.2  1995/07/27  18:01:06  phx_sw
# Remember GRANT SELECT ON TABLES OWNERED BY SYS
#
# Revision 1.1  1995/07/27  17:59:05  phx_sw
# Initial revision
#
*/
set feedback off;
set pagesize 60;

column OSUSER	heading 'USER';
column TERMINAL	heading 'TERM';
column PROCESS	heading 'PID';
column SID	heading 'SID';
column NAME	heading 'ENQUEUE NAME';
column OBJ_NAME	heading 'OBJECT NAME';

column OSUSER	format A9;
column SID	format 999;
column PROCESS	format A5;
column TERMINAL	format A14;
column NAME	format A25;
column OBJ_NAME	format A18;

select distinct	ss.osuser,
		ss.sid,
		ss.process,
		ss.terminal,
		a.name,
		o.name OBJ_NAME,
 decode(l.lmode,
	0,'None',
	1,'Null',
	2,'Row-S (SS)',
	3,'Row-X (SX)',
	4,'Share',
	5,'S/Row-X (SSX)',
	6,'Exclusive') LOCK_MODE
  from sys.obj$ o,
       sys.dbms_lock_allocated a,
       v$lock l,
       v$session ss
 where o.obj# (+) = l.id1
   and a.lockid (+) = l.id1
   and ss.sid = l.sid
   and ss.type = 'USER';

exit;

