#!/usr/bin/ksh
#
# This script will run the forecast update every day WB 1/14/09. 
#


#. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the report.




sql << //
truncate table usr_forecast;
truncate table usr_totalforecast;
commit;
exit
//
/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/forecast.ctl -D /opt/mchugh/prod/les/db/data/load/forecast -d forecast.csv 

cd ${LESDIR}/scripts

sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/ForecastUpdate.out
@ForeCastUpdate.sql
spool off
exit
//
exit 0




