#!/usr/bin/ksh
#
# This script will run the Report at 6 pm using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports



sql << //
set trimspool on
set pagesize 5000
set linesize 200
spool /opt/mchugh/prod/les/oraclereports/PI2004/shipdock.out
@usr_shipdock_update.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 5000 
set linesize 300  
spool /opt/mchugh/prod/les/oraclereports/PI2004/closedbutnotdispatched.out
@User-ShipmentsClosedButNotDispatched.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000 
set linesize 400  
@phys_inv_snapshot_update.sql
exit
//
sql << //
set trimspool on
set pagesize 20000 
set linesize 500  
spool /opt/mchugh/prod/les/oraclereports/PI2004/OpenOrdsReport.out
@Usr-OpenOrdsReport.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 200 
set linesize 400  
spool /opt/mchugh/prod/les/oraclereports/PI2004/OpenSalesOrdsSummary.out
@Usr-OpenSalesOrdsSummary.sql
spool off
exit
//
sql << //
set trimspool on
set linesize 100
set pagesize 50000
@Usr-KHSumm.sql
exit
//
exit 0
