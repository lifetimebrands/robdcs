/**********************************************************************************************************/
/* This script will populate the usr_phys_inv_rcvkey table with the contents of the aremst table,         */
/* before the start of Physical.  This should be done when the snapshot and other updates are done.       */
/**********************************************************************************************************/

truncate table usr_phys_inv_rcvkey;

/* Populate table with current data */

insert into usr_phys_inv_rcvkey 
(
select ltrim(rtrim(l.lodnum)) lodnum, ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.untpak, d.untcas, d.rcvkey, d.cmpkey
       from invdtl d, invsub s, invlod l, locmst m, aremst a
       where l.lodnum = s.lodnum
       and s.subnum = d.subnum
       and l.stoloc = m.stoloc
       and m.arecod = a.arecod
       and a.cntflg = 1
 group
    by d.prtnum,
       l.lodnum,
       m.stoloc,
       d.lotnum,
       d.untpak,
       d.untcas,
       d.rcvkey,
       d.cmpkey)
/
commit;
