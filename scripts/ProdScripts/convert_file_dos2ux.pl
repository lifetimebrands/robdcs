#!/opt/perl5/bin/perl
#
# File    : convert_file_dos2ux.pl
#
# Purpose : Convert file from dos format to unix format (wrapper around dos3ux)
#
# Author  : Steve R. Hanchar
#
# Date    : 21-Aug-2001
#
# Usage   : convert_file_dos2ux <filnam>
#
###############################################################################
#
# Include needed sub-components
#
use strict;
#
###############################################################################
#
# Initialize internal parameters
#
my ($par_pthsep);
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
###############################################################################
#
# Initialize internal variables
#
my $arg_filnam = "";
#
my $arg_vbsflg = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Convert file from dos format to unix format (wrapper around dos3ux)\n");
    printf ("\n");
    printf ("Usage:  convert_file_dos2ux <filnam>\n");
    printf ("\n");
    printf ("    <filnam>) File to covert.  May include directory specification.\n");
    printf ("\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$arg_filnam = @ARGV [0];
$arg_vbsflg = @ARGV [1];
#
# Verify required arguments are present
#
if (!$arg_filnam)
{
    printf ("File name is not defined\n");
    print_help ();
    exit (1);
}
#
# If enabled, output processed input arguments
#
if ($arg_vbsflg)
{
    printf ("\nArgument List\n");
    printf ("FilNam ($arg_filnam)\n");
    printf ("VbsFlg ($arg_vbsflg)\n");
    printf ("\n");
}
#
###############################################################################
#
# Determine unique working file name and verify file does not already exist
#
my @dattim = localtime;
my $dattim = sprintf ("%0.2d%0.2d%0.2d%0.2d%0.2d%0.2d",
                   $dattim[5]-100, $dattim[4]+1, $dattim[3],
		   $dattim[2], $dattim[1], $dattim[0]);
#
my $wrk_filnam = $arg_filnam."_".$dattim;
if (-e $wrk_filnam)
{
    die "FATAL ERROR - Temporary file ($wrk_filnam) already exists\nStopped";
}
#
open (WRKFIL, "> $wrk_filnam") || die "FATAL ERROR - Could not open working file ($wrk_filnam)\nStopped";
#
###############################################################################
#
# Convert file from dos format to unix format
#
my $cmdtxt = "dos2ux $arg_filnam > $wrk_filnam";
#
printf ("CmdTxt ($cmdtxt)\n") if ($arg_vbsflg);
#
system ($cmdtxt);
if ($? != 0)
{
    die "FATAL ERROR - Error $! converting file ($arg_filnam) from dos format to unix format.\nStopped";
}
#
unlink ($arg_filnam) || die "FATAL ERROR - Error $! deleting original file ($arg_filnam)\nStopped";
#
rename ($wrk_filnam, $arg_filnam) || die "FATAL ERROR - Error $! renaming converted file ($wrk_filnam) to ($arg_filnam)\nStopped";
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
if (-e $wrk_filnam)
{
    unlink ($wrk_filnam) || die "FATAL ERROR - Could not remove working file ($wrk_filnam)";
}
#
exit (0);
#
###############################################################################

