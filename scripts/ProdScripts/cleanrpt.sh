# This Script will delete the rpt files that are in the reports directory at 12am CW

cd /opt/mchugh/prod/les/oraclereports

rm *rpt

mv forecast5.out forecast5.out.old
mv forecast5.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles

mv khsumm5.out khsumm5.out.old
mv khsumm5.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles

mv workorder.out workorder.out.old
mv workorder.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles

mv statusd.out statusd.out.old
mv statusd.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles

mv WestburyOpenOrders5.out WestburyOpenOrders5.out.old
mv WestburyOpenOrders5.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles

mv FileStatus5.out FileStatus5.out.old
mv FileStatus5.out.old /opt/mchugh/prod/les/oraclereports/OldOutFiles


exit 0
