#!/opt/perl5/bin/perl
#
# File    : RfBootstrap.pl
#
# Purpose : RF Terminal Driver Interface Bootstrap Script
#
# Author  : Steve R. Hanchar
#
# Date    : 23-Mar-2000
#
# Usage   : RfBootstrap [-r <remote-host>] [-h] [-i] [-v] [-d]
#
###############################################################################
#
# Installation Instructions:
#
#    1) Copy RfBootstrap_template.pl & RfBootstrap_template.dat to desired
#       location.  Recommended locations are rf terminal account home directory
#       or %LESDIR%\scripts.
#
#    2) Rename RfBootstrap_template.pl to RfBootstrap.pl and
#       rename RfBootstrap_template.dat to RfBootstrap.dat
#
#    3) Edit RfBootstrap.dat to define dcs server, remote hosts (terminal
#       controllers), & rf terminals.  Remember to also define file paths
#       for where the rf terminal driver (dcsrdt) is and where you want
#       temporary working files to be created.
#
#    4) For each rf terminal account, edit the account's profile to change its
#       shell to perl and the shell argument to run RfBootstrap.pl.  If using
#       multiple accounts, remember include the 'remote host' information
#       (example: "RfBootstrap -r intctl01").
#
#    5) Test completed installation by logging in with an rf terminal
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
################################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			    # NT
    $par_pthsep = "\\";
    $par_inidir = $1 if ($0 =~ /(.+\\).+$/);
    $par_inicmd = $1 if ($0 =~ /(.+)\..+$/);
    $par_inicmd = $1 if ($0 =~ /.+\\([^\.]+).+$/);
}
else
{			    # Unix
    $par_pthsep = "\/";
    $par_inidir = $1 if ($0 =~ /(.+\/).+$/);
    $par_inicmd = $1 if ($0 =~ /.+\/([^\.]+).+$/);
}
#
printf ("\n %s", $par_inidir );
printf ("\n %s", $par_inicmd );
$par_inifil = "${par_inidir}${par_inicmd}.dat";
if (!-e "$par_inifil" && -e "${par_inidir}RfBootstrap.dat")
{
    $par_inifil = "${par_inidir}RfBootstrap.dat";
}
#
$par_trmsfx_h = "";
#
$par_trmsfx_v = "W";
#
printf ("\n %s", $par_inifil );
################################################################################
#
# Initialize internal variables
#
$arg_rmthst = "default";
$arg_rmthst = "trm_".$ENV{MOCA_ENVNAME} if ($ENV{MOCA_ENVNAME});
#
$arg_inqflg = "0";
#
$arg_vbsflg = "0";
#
$arg_dbgflg = "0";
#
$arg_drvpfx = "";
#
$arg_drvsfx = "";
#
###############################################################################
printf ("\n %s", $arg_rmthst);
