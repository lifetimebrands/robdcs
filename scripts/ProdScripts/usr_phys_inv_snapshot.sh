#!/usr/bin/ksh
#
# This script will run the Report at 5:30AM using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports



sql << //
set trimspool on
set linesize 400
set pagesize 50000
alter session set nls_date_format='mmddyy';
column sysdte new_value 1 noprint
select sysdate sysdte from dual;
@usr_phys_inv_snapshot_update.sql
exit
//
exit 0



