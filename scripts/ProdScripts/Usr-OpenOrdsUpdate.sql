/* #REPORTNAME= Open Sales Orders Update Report */
/* #HELPTEXT= This report updates the Open Sales */
/* #HELPTEXT= Orders and Open Sales Orders Summary */
/* #HELPTEXT= reports with todays present information. */
/* #HELPTEXT= This should be run only if both Open Orders */
/* #HELPTEXT= reports need to be updated with todays info. */
/* #HELPTEXT= Requested by Order Management */

/* Author: Al Driver */
/* Description: This report is run nightly to update the */
/* usr_openords table. This table houses the information */
/* for the Open Sales Orders and Summary reports. A .out */
/* file under the name OpenSalesOrders5.out contains the */
/* detail information and the file OpenSalesOrdersSummary.out */
/* has the summary information.  */

column cponum heading 'PO Number'
column cponum format A20
column ordnum heading 'Order'
column ordnum format A10
column status heading 'Status'
column status format A10
column btcust heading 'Customer'
column btcust format A8
column ship_to_name  heading 'Name'
column ship_to_name  format A15
column early_shpdte heading 'Early'
column early_shpdte format A11
column adddte heading 'Added'
column adddte format A11
column cancel_date heading 'Late'
column cancel_date format A11
column unsamt heading '$'
column unsamt format '9,999,990.00'
column extdte heading 'Extension'
column extdte format A11
column ptksts heading 'Preticketing'
column ptksts format A15
column cstcat heading 'Cstcat'
column cstcat format A10
column shpcat heading 'Ship Cat.'
column shpcat format A15
column vc_numbch heading '# of Batch Attempts'
column vc_numbch format 999999

/* First truncate table that houses information */

truncate table usr_openords;

insert into usr_openords(cponum,ordnum,status,btcust,ship_to_name,early_shpdte,
adddte,cancel_date,unsamt,extdte,ptksts,cstcat,shpcat,vc_numbch)
SELECT
        distinct rtrim(ord.cponum) cponum, 
        rtrim(ord.ordnum) ordnum,       
        rtrim(dscmst.lngdsc) status,
        rtrim(ord.btcust) btcust,
        rtrim( substr(lookup1.adrnam,1,15))  Ship_To_Name, 
        rtrim(to_char(ord_line.early_shpdte,'MM/DD/YYYY')) early_shpdte,
        rtrim(ord.adddte) adddte,       
        rtrim(to_char(ord_line.late_shpdte, 'MM/DD/YYYY')) Cancel_Date,
        decode(shipment_line.linsts, 'I', sum(ord_line.vc_cust_untcst * (shipment_line.stgqty+shipment_line.inpqty)), sum(ord_line.vc_cust_untcst*shipment_line.pckqty)) unsamt,
        ord_line.vc_extdte,  decode(cstmst.vc_ptkstk, 0, 'N', 'Y') ptksts,
                        decode(rtrim(ord.btcust),'H2707100','FARB',' ') cstcat,
                        decode(sign(trunc(ord_line.early_shpdte)-trunc(sysdate)),
                        -1,'Before Today',
                         decode(to_char(ord_line.early_shpdte,'YYYYMM'),
                         to_char(sysdate,'YYYYMM'),'This Month',
                         'Next Month +')) shpcat,
       ord.vc_numbch 
       FROM pckbat, stop, dscmst, adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, 
           shipment_line, shipment, ord_line, ord
       WHERE ord.st_adr_id              = lookup1.adr_id
         AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
        and ord.stcust = cstmst.cstnum (+)
        and ord.client_id = cstmst.client_id (+)
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.schbat        = pckbat.schbat (+)
        AND shipment_line.ship_id       = shipment.ship_id
        AND shipment.stop_id            = stop.stop_id (+)
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled' 
        GROUP BY ord.cponum, ord.ordnum, dscmst.lngdsc, 
        ord.btcust, lookup1.adrnam,
        ord_line.early_shpdte, ord.adddte,
        ord_line.late_shpdte, cstmst.vc_ptkstk, ord_line.vc_extdte, 
        shipment_line.linsts, ord.vc_numbch
UNION    -- Second script grabs pending and backorder orders.  Will not exist in shipment, shipment_line tables
(select distinct  rtrim(ord.cponum) cponum , rtrim(ord.ordnum) ordnum,
decode(varGetShpSts(ord.ordnum),'P','Pending','B',decode(cstmst.bckflg,0,'Backorder Cancel','Backorder'))status,
          rtrim(ord.btcust) btcust, rtrim(substr(lookup1.adrnam,1,15))  Ship_to_Name,
       rtrim(to_char(ord_line.early_shpdte,'MM/DD/YYYY')) early_shpdte,
          rtrim(ord.adddte) adddte,
       rtrim(to_char(ord_line.late_shpdte, 'MM/DD/YYYY')) Cancel_Date,
          sum(ord_line.vc_cust_untcst*ord_line.pckqty) unsamt,
          ord_line.vc_extdte,  decode(cstmst.vc_ptkstk, 0, 'N', 'Y') ptksts,
                          decode(rtrim(ord.btcust),'H2707100','FARB',' ') cstcat,
                          decode(sign(trunc(ord_line.early_shpdte)-trunc(sysdate)),
                          -1,'Before Today',
                          decode(to_char(ord_line.early_shpdte,'YYYYMM'),
                          to_char(sysdate,'YYYYMM'),'This Month',
                          'Next Month +')) shpcat, ord.vc_numbch
     from adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, ord_line, ord, shipment,
(select shipment_line.* from shipment_line, ord_line where shipment_line.client_id=ord_line.client_id
and shipment_line.ordnum=ord_line.ordnum and shipment_line.ordlin=ord_line.ordlin
and shipment_line.ordsln=ord_line.ordsln)sl
    where
      ord.client_id = '----'
    and ord.ordnum = ord_line.ordnum
      and ord.client_id = ord_line.client_id
      and ord.st_adr_id = lookup1.adr_id
     AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
      and ord.stcust = cstmst.cstnum (+)
      and ord.client_id = cstmst.client_id (+)
     and ord_line.ordnum=sl.ordnum(+)
      and ord_line.client_id=sl.client_id(+)
      and ord_line.ordlin=sl.ordlin(+)
      and ord_line.ordsln=sl.ordsln(+)
      and ord_line.pckqty > 0
      and sl.ship_id = shipment.ship_id (+)
      and shipment.shpsts (+) <> 'C'           -- Shipment has not shipped
      and shipment.shpsts (+) <>  'B'          -- Shipment had not been cancelled
    group by ord.cponum, ord.ordnum,
    decode(varGetShpSts(ord.ordnum),'P','Pending','B',decode(cstmst.bckflg,0,'Backorder Cancel','Backorder')),
          ord.btcust,
          lookup1.adrnam,
           ord_line.Early_ShpDte,
          ord.adddte,ord.adddte,
          ord_line.late_shpdte, cstmst.vc_ptkstk, ord_line.vc_extdte, vc_numbch);

commit;


set linesize 500
set pagesize 20000

select * from usr_openords;
