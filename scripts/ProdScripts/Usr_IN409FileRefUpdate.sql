/* This script will populate the usr_hotlist table with items, qty, pastdue and customer information */
/* where the qty is 0 and the item is past due.  This is a variation of the IN409 report             */
/* This information will be referenced when running Usr-IN409FileRef.sql which resides in the        */
/* Oraclereports directory.                                                                          */
/* This script will be activated by the cron job on a nightly basis.                                 */
/* Created on 10/24/2003                                                                             */
/* The name of the report was changed to User IN409 Hot Box on 10/04/2004, as per Craig Phillips     */
/* The sql script is now entitled Usr-IN409HotBox.sql.                                               */ 
/*****************************************************************************************************/



truncate table usr_hotlist;

insert into usr_hotlist
(select
prtnum,
nvl(onhandqty,0) onhqty,
to_char(sum(pastdue),'99990')||decode(sign(sum(pastdue - onhandqty)),1,'*',' ') pstdue,
custname customer from prtdsc,
   (select prtmst.prtnum,
    usrIN400.OnHandQty(prtmst.prtnum) onhandqty,
    sum(decode(sign(ord_line.early_shpdte - sysdate), -1, ord_line.pckqty, 0)) pastdue,
    ord.btcust custname
from bomhdr, prtmst, ord_line, ord
    where bomhdr.bomnum(+) = prtmst.prtnum
    and bomhdr.client_id(+) = prtmst.prt_client_id
    and prtmst.prtnum = ord_line.prtnum
    and prtmst.prt_client_id = ord_line.prt_client_id
    and ord_line.client_id=ord.client_id
    and ord.ordnum = ord_line.ordnum
    and ord_line.client_id = '----'
    and prtmst.prt_client_id = '----'
    and ord.client_id = '----'
    and not exists(select 'x' from shipment_line
    where ord_line.client_id=shipment_line.client_id
    and ord_line.ordnum=shipment_line.ordnum
    and ord_line.ordsln=shipment_line.ordsln
    and ord_line.ordlin=shipment_line.ordlin)
    group
    by prtmst.prtnum, ord.btcust
union all
select prtmst.prtnum,
usrIN400.OnHandQty(prtmst.prtnum) onhandqty,
sum(decode(sign(ord_line.early_shpdte - sysdate), -1, shipment_line.pckqty, 0)) pastdue,
 ord.btcust custname
from bomhdr, shipment_line, prtmst, ord_line, ord
where bomhdr.bomnum(+) = prtmst.prtnum
and ord_line.client_id = shipment_line.client_id
and ord_line.ordnum    = shipment_line.ordnum
and ord_line.ordlin    = shipment_line.ordlin
and ord_line.ordsln    = shipment_line.ordsln
and ord_line.ordnum    = ord.ordnum
and bomhdr.client_id(+) = prtmst.prt_client_id
and ord_line.prtnum(+) = prtmst.prtnum
and ord_line.prt_client_id = '----'
and ord_line.client_id ='----'
and prtmst.prt_client_id = '----'
and ord_line.prt_client_id (+) = prtmst.prt_client_id
and shipment_line.linsts = 'P'
and shipment_line.shpqty = 0
group
by prtmst.prtnum, ord.btcust
having sum(ord_line.ordqty) > usrIN400.OnHandQty(prtmst.prtnum)) prtlst
where
colnam = 'prtnum|prt_client_id'
and colval = prtnum||'|----'
and locale_id ='US_ENGLISH'
and prtlst.pastdue <> 0
group by prtnum ,onhandqty, custname)
/
commit;
