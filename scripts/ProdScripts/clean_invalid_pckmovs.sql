/* This will remove the invalid pick moves from the pick move table */
/* This was causing a problem and causing the pckmgr to fail */

 select a.cmbcod from pckmov a
 where not exists(select 'x' from pckwrk b
 where a.cmbcod = b.cmbcod);

delete from pckmov a
where not exists(select 'x' from pckwrk b
where a.cmbcod = b.cmbcod);


/* The following steps will remove any data that is <= 2 days  */
/* This table was created by Raman - Red Prairie to monitor    */
/* invalid labels                                              */
select count(*) from prswrk_dup;

alter session set nls_date_format ='dd-mon-yyyy';

select min(moddte), max(moddte) from prswrk_dup;

delete from prswrk_dup
where moddte <=sysdate - 2;

select min(moddte), max(moddte) from prswrk_dup;
commit;
