/* Author:  Al Driver */

/* This script is used to identify invalid loads that are */
/* scheduled to go to QCLOC. After identification they  */
/* will be deleted. */

select a.prtnum prtnum,a.untqty untqty,b.lodnum lodnum,a.stoloc stoloc1,c.stoloc stoloc2
        from invsum a,invlod b,invmov c 
        where a.stoloc = b.stoloc
        and b.lodnum = c.lodnum
        and a.untqty = 0 
        and a.comqty = 0 and a.pndqty > 0 
        and a.stoloc = 'QCLOC';

delete from invmov d 
where d.lodnum in (select b.lodnum 
        from invsum a,invlod b,invmov c
        where a.stoloc = b.stoloc
        and b.lodnum = c.lodnum
        and a.untqty = 0
        and a.comqty = 0 and a.pndqty > 0
        and a.stoloc = 'QCLOC');
commit; 
