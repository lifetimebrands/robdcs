#!perl
#
# File    : (dcs_)dload.pl
#
# Purpose : DCS distributed data load (friendly interface to mload)
#
# Author  : Steve R. Hanchar
#
# Date    : 15-Nov-2000
#
# Usage   : dload <topdir> <dirnam> <filnam>
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
###############################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
$par_basdir = $par_pthsep."db".$par_pthsep."data".$par_pthsep."load";
#
$par_safetoload    = $par_basdir."";
$par_bootstraponly = $par_basdir."";
#
###############################################################################
#
# Initialize internal variables
#
$arg_topdir = "";
#
$arg_dirnam = "";
#
$arg_filnam = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Distributed data load (friendly interface to mload)\n");
    printf ("\n");
    # printf ("Usage:  dload <topdir> <dirnam> <filnam>\n");
    printf ("Usage:  dload  <dirnam> <filnam>\n");
    printf ("\n");
    # printf ("    <topdir>) Top directory name (usually defined by alias)\n");
    # printf ("\n");
    printf ("    <dirnam>) Distributed data directory name\n");
    printf ("\n");
    printf ("    <filnam>) Distributed data file name\n");
    printf ("\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$arg_topdir = @ARGV[0];
$arg_dirnam = @ARGV[1];
$arg_filnam = @ARGV[2];
#
# Verify required arguments are present
#
if (!$arg_topdir)
{
    printf ("Top directory name is not defined\n");
    print_help ();
    exit (1);
}
#
if (!$arg_dirnam)
{
    printf ("Distributed data directory name is not defined\n");
    print_help ();
    exit (1);
}
#
#if (!$arg_filnam)
#{
#    printf ("Distributed data file name is not defined\n");
#    print_help ();
#    exit (1);
#}
#
printf ("\nArgument List\n");
# printf ("TopDir ($arg_topdir)\n");
printf ("DirNam ($arg_dirnam)\n");
printf ("FilNam ($arg_filnam)\n");
printf ("\n");
#
###############################################################################
#
# Move to appropriate data dictionary directory structure
# (safetoload or bootstraponly)
#
chdir ("$arg_topdir$par_safetoload");
if (!-d $arg_dirnam)
{
    chdir ("$arg_topdir$par_bootstraponly");
    if (!-d $arg_dirnam)
    {
	printf ("Directory name ($arg_dirnam) is not valid\n");
	exit (1);
    }
    #
    # If working in bootstraponly structure, then file name must be defined
    #
    if (!$arg_filnam)
    {
        printf ("Distributed data file name is not defined\n");
        print_help ();
        exit (1);
    }
    #
    printf ("\nWarning - Loading bootstrap only data - Warning\n");
}
#
###############################################################################
#
# Invoke standard DCS utility to load distributed data file
#
$cmdtxt = "mload -H -D $arg_dirnam -c $arg_dirnam.ctl";
if ($arg_filnam)
{
    $cmdtxt .= " -d $arg_filnam";
}
#
printf ("\nLoad Command\n$cmdtxt\n");
#
system ($cmdtxt);
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################
