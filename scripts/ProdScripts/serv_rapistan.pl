#!/opt/perl5/bin/perl -w

#----------------------------- libraries and use directives --------------------
use Socket;
use strict;
use Getopt::Std;


# ---------------------------- function forward declarations -------------------
sub show_usage;
sub get_printable_msg;
sub spawn;
sub logmsg;
sub new_mask;
sub FD_ISSET;
sub send_ack;

# ----------------------------- # defines ---------------------------------------
# some #defines used.  All these are decimal values converted to strings of one char length.
my $STX = chr(2);
my $ETX = chr(3);
my $ACK = chr(6);
my $NAK = chr(21); 

# variables used to indicate the state
my ($START, $IN_MSG, $END) = (0, 1, 9);
my %state_map = (
	0 => "START",
	1 => "IN_MSG",
	9 => "END"
	);

# ---------------------------------  global variables -----------------------------
my %buff = (); # used to keep the buffer of all socket connections.

# this is the list which contains all the active client socket connections
# that come in.
my @fd = ();

# -------------------------- MAIN PROGRAM --------------------------------------
my $fn = "main() -";

# get the command line options
my ($port, $timeout, $debug_level) = getOptions();

# socket() requires that the protocol be specified in a perticular format
my $protocol = getprotobyname('tcp');

# create a new socket
local *Server;
socket (Server, AF_INET, SOCK_STREAM, $protocol)
	|| die "socket: $!";
# set the socket options so that we can reuse socket in case of a abnormal crash
setsockopt(Server, SOL_SOCKET, SO_REUSEADDR, pack("l", 1))
	|| die "setsockopt: $!";

# bind to our specified port, and accecpt incoming connections from anyone
bind(Server, pack_sockaddr_in($port, INADDR_ANY))
	|| die "bind: $!";

# now start listening on that port
listen(Server, SOMAXCONN)
	|| die "listen: $!";

# add the server socket on to the list of sockets to select(2) on
push @fd, *Server;
logmsg 1, $fn, "Server fd is(" . fileno(Server) . ")";

# server is ready
logmsg 1, $fn, "server started on port $port";

# now let us accecpt incoming connections
while (1) {
	# not we will select on this socket and wait for new conections to come in.
	my ($rin, $rout, $timeleft, $nfound) = ();

	# get the bit mask for select.  This contains the main listening socket and 
	# all the client connections that have comein
	$rin = new_mask();

	($nfound, $timeleft) = select ($rout = $rin, undef, undef, $timeout);

	if (0 == $nfound) {
		# we timed out.
		logmsg 2, $fn, "select() timed out!  Current timeout value is $timeout.\n";
	}
	else {
		# some reading to be done.
		# findout if it is the listening socket.
		if (vec($rout, fileno(Server), 1)) {
			local *Client;
			# listening socket has a connection coming in.
			my $packed_addr = accept(Client, Server); 

			if (!defined($packed_addr)) {
				die "accept: $!.\n";
			}
			else {
				# find out who is trying to connect to us
				my ($in_port, $in_host) = unpack_sockaddr_in($packed_addr);
				my $hostname_a = gethostbyaddr($in_host, AF_INET);
				my $hostname_n = inet_ntoa($in_host);

				logmsg 1, $fn, "Accepted new connection from $hostname_a "
					. "[ $hostname_n ] ";
				
				# add this new socket to the list of sockets that we will select
				# for and wait.  Use typeglob to pass store the socket fd.
				logmsg 1, $fn, "Client fd is(" . fileno(Client) . ")";
				push @fd, *Client;
			}
		}
		else {
			# it is one of the client sockets that has data on it!
			# find out which socket has data coming in.
			foreach (@fd) {
				if (vec($rout, fileno($_), 1)) {
					# this is the socket that has data.
					read_from($_);
				}
			}
		}
	}
}

# close the server socket
close(Server) || die "close: $!";

# close all the client connections
foreach (@fd) {
	close ($_) || die "close: $!";
}

# ------------------ SUBROUTINES -----------------------
# level 4, 5 and 6 are not about severity level of message but they control how much
# or how little information gets printed out.  Level 1, 2 and 3 control the severity
# of the messages itself.
sub logmsg {
	my $lvl = shift;
	my $fn = shift;

	if ($lvl <= $debug_level) {
		my ($sec, $min, $hour, $mday, $month, $year) = localtime(time);
		if ($debug_level == 6) {
			# show every thing at this level
			print sprintf("$0 %4d - %02d/%02d/%04d %02d:%02d:%02d - %s - @_\n", $$,
				$mday, $month + 1, $year + 1900, $hour, $min, $sec, $fn);
		}
		elsif ($debug_level == 5) {
			# do not show script-name and line # information at this level
			print sprintf("%02d/%02d/%04d %02d:%02d:%02d - %s - @_\n",
				$mday, $month + 1, $year + 1900, $hour, $min, $sec, $fn);
		}
		elsif ($debug_level == 4) {
			# stop showing function name at this stage
			print sprintf("%02d/%02d/%04d %02d:%02d:%02d - @_\n",
				$mday, $month + 1, $year + 1900, $hour, $min, $sec);
		}
		else {
			# Further skip the date information at this level and below
			printf sprintf("%02d:%02d:%02d - @_\n", $hour, $min, $sec);
		}
	}
}

sub show_usage {
	die "correct usage for the conveyor lisnter is as follows:\n"
		. "$0\n"
		. "\t-p <port>\n"
		. "\t-t <timout-value-in-seconds>\n"
		. "\t-v <verbose?:debug-level-from 1(least) through 4(most)>\n";
}

sub new_mask {
	my $fn = "new_mask() -";
	# start by setting the mask to null;
	my $mask = '';
	my $tmp =  unpack("b*", $mask);
	
	# now add the mask for other client connection sockets
	foreach my $sock (@fd) {
		vec($mask, fileno($sock), 1) = 1;
	}

	$tmp = unpack("b*", $mask);
	logmsg 3, $fn, "new mask for select() is $tmp.";
	return $mask;
}

sub FD_ISSET {
	my $mask = shift;
	my $fd = shift;
	return vec($mask, fileno($fd), 1) == 1 ? 1 : undef;
}

sub read_from {
	# get the file descriptor from the argument list.
	my $fn = "read_from() -";
	logmsg 3, $fn, "-" x 50;

	my $fd = shift;
	my $fd_fileno = fileno($fd);
	logmsg 3, $fn, "data coming in on fd (" . fileno($fd) .")" ;

	my $onechar = undef
	my $state = undef;

	$onechar = read_onechar ($fd, $onechar, 1);
	if (!defined($onechar)) {
		logmsg 2, $fn, "read_onechar returned undef";
		# to indicate that an error was encountered during read
		# and the socket connection was shutdown and closed.
		return undef;
	}

	# read was sucessful
	logmsg 3, $fn, "read one char($onechar) ascii("
		. sprintf("%d", ord(substr($onechar, 0, 1))) . ")";

	# Append the recd character to the message buffer.
	$buff{$fd_fileno} .= $onechar;
	logmsg 3, $fn, sprintf("New buffer |%s|.", get_printable_msg($buff{$fd_fileno}));

	$state = process_onechar($fd, $onechar);
	logmsg 3, $fn, "state($state) - " . $state_map{$state} . ".";

	# if it is the end of the message process the message.
	if ($state == $END) {
		logmsg 2, $fn, "received end of message";
		logmsg 1, $fn, sprintf("Received over fd(%d)   |%s|", $fd_fileno, get_printable_msg($buff{$fd_fileno}));

		if (defined(send_ack($fd, $buff{$fd_fileno}))) {
			logmsg 3, $fn, "read_from - ack sent on fd($fd_fileno)";
		}
		else {
			logmsg 3, $fn, "read_from() - ERROR sending ACK on fd($fd_fileno)";
		}

		# reset the message buffer for this socket.
		$buff{$fd_fileno} = undef;
	}
	return 1;
}

sub read_onechar {
	# get the socket descriptor from the passed arguments.
	my $fn = "read_onechar() -";

	my $fd = shift;
	my ($onechar, $retval) = ();
	($retval = sysread($fd, $onechar, 1))
		or logmsg 3, $fn, "sysread: $!";

	if (!defined($retval) || ($retval == 0)) {
		logmsg 1, $fn, sprintf("Error encountered while reading from socket(%d).", fileno($fd));
		# this should not happen, because we have attempted to read only after 
		# anyhow, we will have to close the socket.

		# first remove it from our list of sockets.
		my @newfd = ();
		foreach (@fd) {
			if (fileno($fd) != fileno($_)) {
				push @newfd, $_;
			}
			else {
				logmsg 1, $fn, sprintf("Removing socket (%d) from the list of accecpted connections", fileno($_));
			}
		}
		@fd = @newfd;

		shutdown($fd, 2) || die "shutdown: $!";
		close($fd) || die "close: $!";
		# remove it from the list 
		return undef;
	}
	else {
		return $onechar;
	}
}

sub add_fd {
	my $fd = shift;
	push @fd, $fd;
	$buff{fileno($fd)} = '';
}

sub send_ack {
	my $fn = "send_ack()";
	my ($fd, $msg) = @_;
	my $retval = undef;
	
	my ($win, $wout) = ('', '');
	vec($win, fileno($fd), 1) = 1;
	my $n = select (undef, $wout = $win, undef, 0);
	if ($n != 1) {
		logmsg 1, $fn, "Cannot write on socket (", fileno($fd), ")";
		shutdown($fd, 2);
		close($fd);
	}
	else {
		my $ack_txt = get_ack_string($msg);
		logmsg 3, $fn, sprintf("Attempting to send back ack string on socket(%d).", fileno($fd));
		if ($retval = send($fd, $ack_txt, 0)) {
			logmsg 1, $fn, sprintf("Sent ack on socket(%d) |%s|", fileno($fd), get_printable_ack_string($ack_txt));
		}
		else {
			logmsg 1, $fn, "send: $!";
		}
	}
	return $retval;
}

sub getOptions {
	my %opts = ();
	getopts('p:t:v:', \%opts);

	my ($port, $timeout) = ();
	#get the port number from the arguments
	$port = $opts{p} if defined $opts{p};
	# get the timeout value from the arguments, this is the time in seconds.
	$timeout = $opts{t} if defined $opts{t};

	# port and timeout are required arguments
	unless ($port && $timeout) {
		show_usage();
	}

	# now get the optional arguments.
	# by default log as few messages as possible because it clutters up the output screen.
	my $debug_level = 1;
	$debug_level = $opts{v} if defined $opts{v};
	# valid values for debug level are 1 through 6 only.
	show_error() unless $debug_level >=1 && $debug_level <= 6;

	return ($port, $timeout, $debug_level);
}

#-------------------------------------------------------------------------------
# Protocol specific changes.
# This function is passed the message received as an argument.  If the ack, however,
# is such that it does not depend on the message being received, then the implementation
# can ignore it.

sub get_ack_string {
	return sprintf "%s%s%s%s", $STX, substr($_[0], 1, 5), $ACK, $ETX;
}

sub get_printable_ack_string {
	my $msg = shift;

	# convert the non-printable ASCII characters to meaningful strings.
	$msg =~ s/^$STX/<STX>/;
	$msg =~ s/$ETX$/<ETX>/;
	$msg =~ s/$ACK/<ACK>/;
	$msg =~ s/$NAK/<NAK>/;
	return $msg;
}

sub get_printable_msg {
	my $msg = shift;

	# convert the non-printable ASCII characters to meaningful strings.
	$msg =~ s/^$STX/<STX>/;
	$msg =~ s/$ETX$/<ETX>/;
	$msg =~ s/$ACK/<ACK>/;
	$msg =~ s/$NAK/<NAK>/;
	return $msg;
}

sub process_onechar {
	my ($fd, $onechar) = @_;
	my $fd_fileno = fileno($fd);
	my $state = undef;
	my $fn = 'process_onechar()';

	if ($onechar eq $STX) {
		# this means it is a start of a new message.  But if so then the buffer
		# should be empty at this time.  Because <STX> cannot occure inside of a
		# message.  And if so then it would be an illegal message.
		logmsg 3, $fn, "received start of message";
		if ( length($buff{$fd_fileno}) != 1 ) {
			warn "<STX> received inside a message string.  Illegal message"
				. " format.\nResetting the message text.  Discarding the"
				. " current message buffer of ($buff{$fd_fileno})).\n"
				. "Treating this <STX> as start of a new messge.\n";
			# zap the current buffer.
			$buff{$fd_fileno} = '';
			# now add the <STX> to the buffer.
		}
		$state = $START;
	}
	elsif ($onechar eq $ETX) {
		# we have received the end of message.
		$state = $END;
	}
	else {
		# this must be a part of the message!
		$state = $IN_MSG;
	}

	return $state;
}
