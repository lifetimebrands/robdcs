/* first create temporary table to hold all shipment information for the last 12 months */


drop table forecast_temp;

create table forecast_temp as
          select b.shpqty, a.prtnum, a.ordlin, a.ordnum,trunc(g.dispatch_dte) shpdte
                 from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
                 car_move f,trlr g
                 where x.host_ext_id = d.btcust
                 and x.client_id = d.client_id
                 and d.ordnum = a.ordnum
                 and d.client_id = a.client_id
                 and d.client_id = b.client_id
               and d.rt_adr_id = c.rt_adr_id
               and a.ordnum = b.ordnum
               and a.ordlin = b.ordlin
               and a.client_id = b.client_id
               and a.ordsln = b.ordsln
               and b.ship_id = c.ship_id
               and c.stop_id = e.stop_id
               and e.car_move_id  = f.car_move_id
               and f.trlr_id  = g.trlr_id
               and a.client_id ||''= '----'
               and c.shpsts ||''= 'C'
               and x.adrtyp = 'CST'
               and trunc(g.dispatch_dte) between add_months(trunc(sysdate),-12) and trunc(sysdate)
               union
               select b.shpqty, a.prtnum, a.ordlin, a.ordnum, trunc(g.dispatch_dte) shpdte
               from adrmst@arch x,ord@arch d,ord_line@arch a,shipment_line@arch b,shipment@arch c, stop@arch e,
               car_move@arch f,trlr@arch g
               where x.host_ext_id = d.btcust
               and x.client_id = d.client_id
               and d.ordnum = a.ordnum
               and d.client_id = a.client_id
               and d.client_id = b.client_id
               and d.rt_adr_id = c.rt_adr_id
               and a.ordnum = b.ordnum
               and a.ordlin = b.ordlin
               and a.client_id = b.client_id
               and a.ordsln = b.ordsln
               and b.ship_id = c.ship_id
               and c.stop_id = e.stop_id
               and e.car_move_id  = f.car_move_id
               and f.trlr_id  = g.trlr_id
               and a.client_id ||''= '----'
               and c.shpsts ||''= 'C'
               and x.adrtyp = 'CST'
             and trunc(g.dispatch_dte) between add_months(trunc(sysdate),-12) and trunc(sysdate);


/* insert summary of all units shipped quantities into second temporary table */


truncate table forecast_temp1;

insert into forecast_temp1(prtnum,shpqty) 
select a.prtnum,sum(a.shpqty) shpqty from forecast_temp a
group by a.prtnum;

commit;

