#!/usr/bin/ksh
#
# This script will clean up the invalid generated replen pick moves.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql script.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the script

sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/CleanPickMoves.out
@clean_invalid_pckmovs.sql
spool off
exit
//
exit 0
