#!perl
#
# File    : Pcom.pl
#
# Purpose : Process source files via command
#
# Author  : Steve R. Hanchar
#
# Date    : 09-Aug-2000
#
# Usage   : Pcom [-c <cmdtxt>] [-d <dircmd>] [-f <fillst>] [-e] [-h] [-p] [-q] [-s] [-v]
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
###############################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
###############################################################################
#
# Initialize internal variables
#
$arg_cmdtxt = "perl <topdir>pcom_stub.pl -f {filnam} -c {curdir} -t {topdir}";
#
$arg_dircmd = "";
#
$arg_direxc = "";
$arg_direxp = "";   # Dependant global variable
#
$arg_fillst = "";
$arg_filexp = "";   # Dependant global variable
#
$arg_prmflg = "";
#
$arg_quiflg = "";
#
$arg_subflg = "";
#
$arg_vbsflg = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Process source files via command\n");
    printf ("\n");
    printf ("Usage:  Pcom [-c <cmdtxt>] [-d <dircmd>] [-f <fillst>] [-e] [-h] [-q] [-s] [-v]\n");
    printf ("\n");
    printf ("    -c) Command text - Command to process source files against\n");
    if ($arg_cmdtxt)
    {
	printf ("        (default is '$arg_cmdtxt')\n");
    }
    printf ("\n");
    printf ("        The command can contain the following special purpose tokens.  These\n");
    printf ("        tokens will be replaced by their representative value before the\n");
    printf ("        command is actually executed.  Using curly brackets {} instead of the\n");
    printf ("        angle brackets shown will cause the token value to be inserted with\n");
    printf ("        double quotes around the value.\n");
    printf ("\n");
    printf ("        <filnam> - The file name and type of the file being processed\n");
    printf ("        <curdir> - The relative path specification from the top level directory\n");
    printf ("                   of the directory tree being processed.  Intended primarily\n");
    printf ("                   for use with the 'subdirectory mode' option.\n");
    printf ("        <topdir> - The relative path specification to the top level directory\n");
    printf ("                   of the directory tree being processed.  Intended primarily\n");
    printf ("                   for use with the 'subdirectory mode' option.\n");
    printf ("\n");
    printf ("    -d) Directory command - Command to process each directory against\n");
    if ($arg_dircmd)
    {
        printf ("        (default is '$arg_dircmd')\n");
    }
    printf ("\n");
    printf ("        Like the (file) command text, the directory command can contain special\n");
    printf ("        purpose tokens.  This command can use the <curdir> and <topdir> tokens.\n");
    printf ("\n");
    printf ("    -f) File list - File name list to process (do not include directory)\n");
    printf ("\n");
    printf ("    -e) Exclusion list - Directory name list to exclude (skip over)\n");
    printf ("\n");
    printf ("    -h) Help mode - display this help screen\n");
    printf ("\n");
    printf ("    -q) Quiet mode - don't display running status messages\n");
    printf ("\n");
    printf ("    -p) Prompt mode - prompt user for confirmation after each command\n");
    printf ("\n");
    printf ("    -s) Subdirectory mode - Process sub-directory tree as well\n");
    printf ("\n");
    printf ("    -v) Verbose mode - display additional running status messages\n");
    printf ("\n");
}
#
###############################################################################
#
# Define process directory routine
#
sub process_directory
{
    my ($dirnam, $prvcur, $prvtop, $spacer) = @_;
    #
    my ($curdir) = "";
    my ($topdir) = "";
    my ($newspc) = "";
    my ($pardir) = "";
    #
    my (@dirlst) = "";
    #
    my (@fillst) = "";
    my (@tmplst) = "";
    my ($tmpnam) = "";
    my ($filnam) = "";
    my ($cmdtxt) = "";
    #
    my (@sublst) = "";
    my ($subnam) = "";
    #
    # Output informational message
    #
    printf ("${spacer}Processing directory\n") if ($arg_subflg && !$dirnam && !$arg_quiflg);
    printf ("${spacer}Processing directory ($dirnam)\n") if ($arg_subflg && $dirnam && !$arg_quiflg);
    #
    # Initialize local variables
    #
    if (!$dirnam)
    {
	$curdir = $prvcur;
	$topdir = $prvtop;
	$newspc = $spacer."  ";
	$pardir = ".";
    }
    elsif (!$prvcur)
    {
	$curdir = $dirnam;
	$topdir = "\\.\\."."\\".$par_pthsep;
	$newspc = $spacer."  ";
	$pardir = "..";
    }
    else
    {
	$curdir = $prvcur."\\".$par_pthsep.$dirnam;
	$topdir = "\\.\\."."\\".$par_pthsep.$prvtop;
	$newspc = $spacer."  ";
	$pardir = "..";
    }
    #
    # If directory name is defined, then move to desired directory
    #
    if ($dirnam)
    {
	chdir ("$dirnam");
    }
    #
    # Get sorted directory listing
    #
    opendir (DIRLST, ".") || die "Error getting directory ($dirnam) listing.\nStopped";
    @dirlst = grep (!/^\.\.?$/, sort (readdir (DIRLST)));
    close (DIRLST);
    #
    # If (file) command text is defined, then do the following
    #
    if ($arg_cmdtxt)
    {
	#
	# Get directory file listing
	#
	@fillst = grep (!-d, @dirlst);	
	#
	# If file name list is not defined, then select all text files in directory
	#
	if (!$arg_fillst)
	{
	    @fillst = grep (-T, @fillst);  # Only grab text files
	}
	#
	# Else (file name list is defined), then parse directory file listing to select desired files
	#
	else
	{
	    #
	    # Convert file name list into regular expression list
	    #
	    if (!$arg_filexp)
	    {
		@tmplst = split ( /,/, $arg_fillst);
		foreach $tmpnam (@tmplst)
		{
		    $tmpnam =~ s/\./\\\./g;
		    $tmpnam =~ s/\*/\.\*/g;
		    $tmpnam =~ s/^\s*(\S(.*\S)?)\s*$/(^$1\$\)/g;
		}
		$arg_filexp = join ("|", @tmplst);
	    }
	    #
	    # Evaluate directory file listing against converted file name list to select desired files
	    #
	    eval "\@fillst = grep ( /$arg_filexp/, \@fillst)";
	}
	#
	# Do for each selected file
	#
	foreach $filnam (@fillst)
	{
	    chomp $filnam;
	    #
	    printf ("${spacer}..Processing file ($filnam)\n") if (!$arg_quiflg && $arg_subflg);
	    printf ("${spacer}Processing file ($filnam)\n") if (!$arg_quiflg && !$arg_subflg);
	    #
	    # Format command text (aka insert token values)
	    #
	    $cmdtxt = $arg_cmdtxt;
	    #
	    if ($cmdtxt =~ /\<filnam\>/)
	    {
		eval "\$cmdtxt =~ s/\\<filnam\\>/$filnam/g";
	    }
	    elsif ($cmdtxt =~ /\{filnam\}/)
	    {
		eval "\$cmdtxt =~ s/\\{filnam\\}/\"$filnam\"/g";
	    }
	    else
	    {
		#
		# Append file name to command if no token symbol is found and 
		# file name is not already present in command.
		#
		eval "\$test = (\$cmdtxt =~ / $filnam /)";
		if (!$test)
		{
		    $cmdtxt = $cmdtxt." ".$filnam
		}
	    }
	    #
	    if ($cmdtxt =~ /\<curdir\>/)
	    {
		eval "\$cmdtxt =~ s/\\<curdir\\>/$curdir/g";
	    }
	    #
	    if ($cmdtxt =~ /\{curdir\}/)
	    {
		eval "\$cmdtxt =~ s/\\{curdir\\}/\"$curdir\"/g";
	    }
	    #
	    if ($cmdtxt =~ /\<topdir\>/)
	    {
		eval "\$cmdtxt =~ s/\\<topdir\\>/$topdir/g";
	    }
	    #
	    if ($cmdtxt =~ /\{topdir\}/)
	    {
		eval "\$cmdtxt =~ s/\\{topdir\\}/\"$topdir\"/g";
	    }
	    #
	    # Update environment variables used for passing arguments/information
	    #
	    $ENV{PCOM_LVL} = $ENV{PCOM_LVL} + 1;
	    $ENV{PCOM_SPC} = "$spacer";
	    #
	    # Invoke formatted command
	    #
	    printf ("${spacer}..Command (%s)\n", $cmdtxt) if ($arg_vbsflg);
	    #
	    # Unix Hack: If first word of formatted command text maps to an alias,
	    #            then insert the alias command string into command text
	    #
	    $test = ($^O =~ /win32/i);
	    if (!$test)
	    {
		$cmdvrb = $cmdtxt;
		$cmdvrb =~ s/^(\w*)\s.*$/$1/;
		eval "\$cmdals = \$ENV{ALIAS_$cmdvrb}";
		if ($cmdals)
		{
		    $cmdtxt =~ s/^\w*(\s.*)$/$1/;
		    $cmdtxt = "pwd >/dev/null;".$cmdals.$cmdtxt;
		    #printf ("${spacer}..Translated Command (%s)\n", $cmdtxt) if ($arg_vbsflg);
		}
	    }
	    #
	    system ($cmdtxt);
	    #
	    # Update environment variables used for passing arguments/information
	    #
	    $ENV{PCOM_LVL} = $ENV{PCOM_LVL} - 1;
	    #
	    # If enabled, prompt user for confirmation to continue
	    #
	    if ($arg_prmflg)
	    {
		printf ("Done processing ($filnam)\nPress return to continue (a=abort): ");
		$usrrsp = <STDIN>;
		chomp $usrrsp;
		if ($usrrsp =~ /^[aA]$/)
		{
		    exit (0);
		}
	    }
	}
    }
    #
    # If directory command is defined, then do the following
    #
    if ($arg_dircmd)
    {
	#
	# Format command text (aka insert token values)
	#
	$cmdtxt = $arg_dircmd;
	#
	if ($cmdtxt =~ /\<curdir\>/)
	{
	    eval "\$cmdtxt =~ s/\\<curdir\\>/$curdir/g";
	}
	#
	if ($cmdtxt =~ /\{curdir\}/)
	{
	    eval "\$cmdtxt =~ s/\\{curdir\\}/\"$curdir\"/g";
	}
	#
	if ($cmdtxt =~ /\<topdir\>/)
	{
	    eval "\$cmdtxt =~ s/\\<topdir\\>/$topdir/g";
	}
	#
	if ($cmdtxt =~ /\{topdir\}/)
	{
	    eval "\$cmdtxt =~ s/\\{topdir\\}/\"$topdir\"/g";
	}
	#
	# Update environment variables used for passing arguments/information
	#
	$ENV{PCOM_LVL} = $ENV{PCOM_LVL} + 1;
	$ENV{PCOM_SPC} = "$spacer";
	#
	# Invoke formatted command
	#
	printf ("${spacer}.. Directory Command (%s)\n", $cmdtxt) if ($arg_vbsflg);
	#
	# Unix Hack: If first word of formatted command text maps to an alias,
	#            then insert the alias command string into command text
	#
	$test = ($^O =~ /win32/i);
	if (!$test)
	{
	    $cmdvrb = $cmdtxt;
	    $cmdvrb =~ s/^(\w*)\s.*$/$1/;
	    eval "\$cmdals = \$ENV{ALIAS_$cmdvrb}";
	    if ($cmdals)
	    {
		$cmdtxt =~ s/^\w*(\s.*)$/$1/;
		$cmdtxt = "pwd >/dev/null;".$cmdals.$cmdtxt;
		#printf ("${spacer}..Translated Command (%s)\n", $cmdtxt) if ($arg_vbsflg);
	    }
	}
	#
	system ($cmdtxt);
	#
	# Update environment variables used for passing arguments/information
	#
	$ENV{PCOM_LVL} = $ENV{PCOM_LVL} - 1;
	#
	# If enabled, prompt user for confirmation to continue
	#
	if ($arg_prmflg)
	{
	    printf ("Done processing directory ($dirnam)\nPress return to continue (a=abort): ");
	    $usrrsp = <STDIN>;
	    chomp $usrrsp;
	    if ($usrrsp =~ /^[aA]$/)
	    {
		exit (0);
	    }
	}
    }
    #
    # If process subdirectories option is enabled, then do the following
    #
    if ($arg_subflg)
    {
	#
	# Get directory subdirectory listing
	#
	@sublst = grep (-d, @dirlst);
	#
	# If directory exclusion list is defined, then do the following
	#
	if ($arg_direxc)
	{
	    #
	    # Convert directory exclusion list into regular expression list
	    #
	    if (!$arg_direxp)
	    {
		@tmplst = split ( /,/, $arg_direxc);
		foreach $tmpnam (@tmplst)
		{
		    $tmpnam =~ s/\./\\\./g;
		    $tmpnam =~ s/\*/\.\*/g;
		    $tmpnam =~ s/^\s*(\S(.*\S)?)\s*$/(^$1\$\)/g;
		}
		$arg_direxp = join ("|", @tmplst);
	    }
	    #
	    # Evaluate directory listing against converted directory exclusion list to select desired directories
	    #
	    eval "\@sublst = grep (!/$arg_direxp/, \@sublst)";
	}
	#
	# If subdirectory list is not empty, then process subdirectories
	#
	if ($sublst [0])
	{
	    #
	    # Do for each subdirectory
	    #
	    foreach $subnam (@sublst)
	    {
		chomp $subnam;
		#
		# Process subdirectory
		#
		$status = &process_directory ($subnam, $curdir, $topdir, $newspc);
	    }
	}
    }
    #
    # Return to parent directory
    #
    chdir ($pardir);
    #
    # Normal successful completion
    #
    return "1";
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$status = &Getopts ('c:d:e:f:hpqsv');
#
# If help mode enabled, then print help screen & exit
#
if ($opt_h || !$status)
{
    &print_help ();
    exit (0);
}
#
# Load arguments into local variables
#
$arg_cmdtxt = $opt_c if ($opt_c);
$arg_dircmd = $opt_d if ($opt_d);
$arg_direxc = $opt_e if ($opt_e);
$arg_fillst = $opt_f if ($opt_f);
$arg_prmflg = "1"    if ($opt_p);
$arg_quiflg = "1"    if ($opt_q && !$opt_v);
$arg_subflg = "1"    if ($opt_s);
$arg_vbsflg = "1"    if ($opt_v);
#
# Verify required arguments are present
#
if (!$opt_c && $opt_d)
{
    $arg_cmdtxt = "";
}
#
if (!$opt_c && !$opt_d)
{
    #
    # If command text was not explicitly defined, verify default command
    # (with pcom_stub.pl) is valid.  I.e. verify pcom_stub.pl exists.
    # Assumes default command of "perl <topdir>pcom_stub.pl ..."
    #
    $test = $arg_cmdtxt;
    $test =~ s/\S*\s*(\S*).*/$1/;
    $test =~ s/\<topdir\>//g;
    $test =~ s/{<topdir\}//g;
    if (!-e $test)
    {
	printf ("Command text is not defined\n");
	print_help ();
	exit (1);
    }
}
#
#
if ($arg_vbsflg)
{
    printf ("\nArgument List\n");
    printf ("CmdTxt ($arg_cmdtxt)\n");
    printf ("DirCmd ($arg_dircmd)\n");
    printf ("DirExc ($arg_direxc)\n");
    printf ("FilLst ($arg_fillst)\n");
    printf ("PrmFlg ($arg_prmflg)\n");
    printf ("QuiFlg ($arg_quiflg)\n");
    printf ("SubFlg ($arg_subflg)\n");
    printf ("VbsFlg ($arg_vbsflg)\n");
    printf ("\n");
}
elsif (!$arg_quiflg)
{
    printf ("\n");
}
#
###############################################################################
#
# Process current directory
#
$status = &process_directory ("", "", "", "");
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################
