/* #REPORTNAME=KhSumm Report  */
/* #HELPTEXT= This report lists the four wall, shipped and allocated quantities */
/* #HELPTEXT= Requested by Imports */

truncate table usr_temp_snap_4wall;

/* To store 4 wall inventory */


insert into usr_temp_snap_4wall (prtnum, untqty)
select d.prtnum,
       sum(d.untqty) untqty  
  from invdtl d, invsub s, invlod l, locmst m, aremst a 
 where (a.fwiflg = 1 or a.arecod = 'EXPR') and 
       a.arecod != 'SHIP'   and 
       a.arecod  = m.arecod and 
       m.stoloc  = l.stoloc and 
       l.lodnum  = s.lodnum and 
       d.invsts not in ('D','L') and
       s.subnum  = d.subnum  
       group 
       by d.prtnum;
commit;


spool /opt/mchugh/prod/les/oraclereports/4wallinv5.out

select rtrim(prtnum) prtnum, untqty from usr_temp_snap_4wall;

spool off;


/*  To store shipped quantities  */

truncate table usr_temp_snap_ship;

insert into usr_temp_snap_ship (prtnum, shpqty)
  select prtnum, sum(shipment_line.shpqty) shpqty
  from shipment_line, shipment, ord, ord_line, stop
  where shipment_line.client_id  = ord_line.client_id
  AND   shipment_line.ordnum     = ord_line.ordnum
  AND   shipment_line.ordlin     = ord_line.ordlin
  AND   shipment_line.ordsln     = ord_line.ordsln
  AND   shipment_line.ship_id    = shipment.ship_id
  AND   shipment_line.ordnum     = ord.ordnum
  AND   shipment.stop_id         = stop.stop_id
  AND   shipment.doc_num         = stop.doc_num
  AND   ord.client_id            = '----'
  AND   ord.ordnum               = ord_line.ordnum
  AND   shipment.shpsts = 'C'
  AND   ord.btcust != 'WASH'
  AND   shipment.loddte between to_date('01-jan-2004','dd-mon-yyyy')
  AND   trunc(sysdate) + .99999
  group
    by prtnum
union
select prtnum, sum(a.shpqty) shpqty
from shipment_line@arch a, shipment@arch b, ord@arch c, ord_line@arch d, stop@arch e
where a.client_id  = d.client_id
AND   a.ordnum     = d.ordnum
AND   a.ordlin     = d.ordlin
AND   a.ship_id    = b.ship_id
AND   b.stop_id         = e.stop_id
AND   b.doc_num         = e.doc_num
AND   c.client_id            = '----'
 AND   c.ordnum               = d.ordnum
AND   b.shpsts = 'C'
AND   c.btcust != 'WASH'
AND   b.loddte between to_date('01-jan-2004','dd-mon-yyyy')
 AND   trunc(sysdate) + .99999
 group
 by prtnum;

commit;

truncate table usr_temp_snap_pend;

/*  To store pending quantities    */

insert into usr_temp_snap_pend(prtnum, pndqty)
select prtnum, (sum(shipment_line.inpqty) + sum(shipment_line.stgqty)) pndqty
from ord_line, shipment_line
where ord_line.client_id = shipment_line.client_id
and ord_line.ordnum=shipment_line.ordnum
and ord_line.ordlin=shipment_line.ordlin
and ord_line.ordsln=shipment_line.ordsln 
and shipment_line.linsts != 'C'
group by prtnum
union
select prtnum, 
sum((ol.pckqty + nvl(sl.pckqty,0))) pndqty
from ord_line ol, 
     shipment_line sl
   where ol.prt_client_id = '----'
     and ol.client_id = '----'
    and ol.client_id = sl.client_id(+)
    and ol.ordnum = sl.ordnum(+)
    and ol.ordlin = sl.ordlin(+)
    and ol.ordsln = sl.ordsln(+)
    and sl.linsts(+) <> 'C'
    and sl.linsts(+) <> 'B'
    and sl.linsts(+) <> 'I'
    and sl.shpqty(+)  = 0
group by ol.prtnum;

commit;

create index USR_TEMP_SNAP_SHIP_PK on usr_temp_snap_ship(prtnum);

create index USR_TEMP_SNAP_PEND_PK on usr_temp_snap_pend(prtnum);

truncate table usr_temp_khsumm;

insert into usr_temp_khsumm
(prtnum, untqty, shpqty, pndqty)
select prtnum,sum(untqty) untqty,0,0 from usr_temp_snap_4wall a
where not exists(select 'x' from usr_temp_khsumm b
where a.prtnum=b.prtnum)
group by prtnum;
commit;

insert into usr_temp_khsumm
(prtnum, untqty, shpqty, pndqty) 
select prtnum, 0, sum(shpqty) shpqty, 0 from usr_temp_snap_ship a
where not exists(select 'x' from usr_temp_khsumm b
where a.prtnum=b.prtnum)
group by prtnum;
commit;

insert into usr_temp_khsumm
(prtnum, untqty, shpqty, pndqty)
select prtnum, 0,0, sum(pndqty) pndqty from usr_temp_snap_pend a
where not exists(select 'x' from usr_temp_khsumm b
where a.prtnum = b.prtnum)
group by prtnum;
commit;

update usr_temp_khsumm a
set a.shpqty=(select sum(b.shpqty)  from usr_temp_snap_ship b
where a.prtnum=b.prtnum)
where exists(select 'x' from usr_temp_snap_ship c
where a.prtnum=c.prtnum);
commit;

update usr_temp_khsumm a
set a.pndqty=(select sum(b.pndqty)  from usr_temp_snap_pend b
where a.prtnum=b.prtnum)
where exists(select 'x' from usr_temp_snap_pend c
where a.prtnum=c.prtnum);
commit;

update usr_temp_khsumm 
set refdte=sysdate;

commit;

drop index USR_TEMP_SNAP_SHIP_PK;

drop index USR_TEMP_SNAP_PEND_PK;

column prtnum heading 'Part #'
column prtnum format A12
column shpqty heading 'Qty Shipped'
column shpqty format 99,999,990
column onhand heading 'On Hand'
column onhand format 99,999,990
column pndqty heading 'Allocated'
column pndqty format 99,999,990
column prtnum heading 'Part #'
column prtnum format A20
column lngdsc heading 'Part Description'
column lngdsc format A55
column refdte heading 'Ref Date'
column refdte format a20

spool /opt/mchugh/prod/les/oraclereports/khsumm5.out

set pagesize 50000
set linesize 300


select rtrim(prtmst.prtnum) prtnum, substr(prtdsc.lngdsc,1,55) lngdsc,
nvl(untqty,0) onhand, nvl(shpqty,0) shpqty,  nvl(pndqty ,0) pndqty, to_char(refdte,'DD-MON-YYYY HH24:MI:SS') refdte
from prtdsc, usr_temp_khsumm, prtmst
where
prtmst.prtnum||'|----' = prtdsc.colval and
prtmst.prtnum = usr_temp_khsumm.prtnum(+)
and prtmst.prt_client_id = '----'
and prtdsc.locale_id = 'US_ENGLISH'
order
by prtmst.prtnum;

spool off;


spool /opt/mchugh/prod/les/oraclereports/workorder.out

set pagesize 2000
set linesize 132


select a.wkonum, b.prtnum, sum(b.rpt_rtsqty) from wkohdr a, wkodtl b
where
a.wkonum=b.wkonum and a.client_id=b.client_id and a.wkorev=b.wkorev
and b.rpt_rtsqty != 0 
and a.wkosts='I'
group by a.wkonum, b.prtnum;
spool off;

set pagesize 1000
set linesize 200

spool /opt/mchugh/prod/les/oraclereports/statusd.out

   select d.prtnum,
            sum(d.untqty) untqty, d.invsts
       from invdtl d, invsub s, invlod l, locmst m, aremst a
      where (a.fwiflg = 1 or a.arecod = 'EXPR') and
         a.arecod != 'SHIP'   and
       a.arecod  = m.arecod and
          m.stoloc  = l.stoloc and
          l.lodnum  = s.lodnum and
          d.invsts  in ('D','L') and
          s.subnum  = d.subnum
          group
         by d.prtnum, d.invsts;
spool off;
