#!/usr/local/bin/perl -w

# This scriptgets the latest file in the latest directory .
# This was needed to strip yesterday's reconciliation file from yesterday's directory

# Raman Parthasarathy, 07/10/01

#Include required sub components
require "getopts.pl";

# Initialize internal parameters
# This checks the Operating System

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

my ($filnam) = "";    
my ($curdir) = "";    
my $discrepancy = 0;
my ($yesterdir) = "";
my $tran_num = 0;
my $ctran_num = "";
my $count = 0;
my $HOME = "/home/dcsmgr";
my $yesterday_file = "";
my $cmdlin = "";
my $adj_file =  "";
my $tom_adj_file =  "";
my $adjnum = "";
my $n_adjnum = 0;
my $scripts_dir = "/opt/mchugh/prod/les/scripts";

chdir '/tmp/RECONC' || die "Can't cd to /tmp/RECONC. Stopped. \n" ;

#Open the directory and read contents. List only entries of the type tran200
opendir (DIRLST, ".") || die "Error Getting directory listing. \n Stopped";
@dirlst = grep ( /tran200/, readdir (DIRLST)); 
close (DIRLST);

#We have the contents of the directory now. Sort it and get the last entry(yesterday's entry)

@sortdirlst = sort {$b cmp $a}  @dirlst;

# I need the second element of the array (yesterday)

$yesterday = $sortdirlst[1];

$today = $sortdirlst[0];  

printf ("\n New yesterday is %s", $yesterday);
printf ("\n New today is %s", $today);

# Get the REC file sitting in yesterday's directory. There will be 2 files. We need the latest
chdir $yesterday || die "Can't cd to $yesterday: $!\n";

opendir (DIRLST, ".") || die "Error getting directory listing (2). \n Stopped";

@dirlst = grep ( /REC/, readdir (DIRLST));
close (DIRLST);

#We have the contents of the directory now. Sort it and get the last entry(yesterday's entry)

@sortdirlst = sort {$a cmp $b}  @dirlst;

# I need the first element of the array (yesterday)

$yesterday_file = $sortdirlst[1];

printf ("\n %s", $yesterday_file);

#Copy this into today's directory

$cmdlin = "cp -p $yesterday_file  ../$today";
printf (" ($cmdlin)\n") ;
system ($cmdlin);   

#doing a premature exit because we dont want to create the adj file at this point
exit (0);

#The transaction number in the awk/tranfile needs to be replaced with the new number
# The new number will be got from the ADJ0* file

opendir (DIRLST, ".") || die "Error getting directory listing(3). \n Stopped ";

@dirlst = grep (/ADJ0/, readdir(DIRLST));
close (DIRLST);

$adj_file = $dirlst[0]; 

# Create the new adjustment file in today's directory. We will fill it later

$adjnum = $adj_file;
$adjnum = substr ($adj_file,3, 5); 

$n_adjnum = $adjnum;

$tom_adj_file = sprintf ( "ADJ%5.5d.txt", $n_adjnum+1 );

printf ("\n Today's file is %s", $tom_adj_file);

$cmdlin = "touch ../$today/$tom_adj_file";
printf (" ($cmdlin)\n") ;
system ($cmdlin);

open (FILNAM, "<$adj_file")|| die "Could not open source file ($adj_file) for reading.\nStopped";

@fillin = <FILNAM>;
close (FILNAM);

$count = 0;
foreach (@fillin)
{
 $adj_line = $_;
 if ($count == 0)
 {
   @segments = split (/ +/, $adj_line);   
   $ctran_num = substr($segments[2], 3);  
   $first_tran_num = $ctran_num;           
   printf ("The first trannum is %d", $first_tran_num);
 }
 $count++;
}

printf ("The first trannum is %d", $first_tran_num);

# Now we have the last line of the file 

@segments = split (/ +/, $adj_line);

$ctran_num = substr($segments[2], 3);
$last_tran_num = $ctran_num;

$last_tran_num++;

printf ("The last trannum is %d", $last_tran_num);

#Replace the transaction number to the new one. Use sed.
$cmdlin = "sed \"s\/$first_tran_num\/$last_tran_num\/\" $scripts_dir/awk/tranfile > $scripts_dir/awk/tranfile.out";
printf (" \n ($cmdlin)\n");
system ($cmdlin);  

# Rename the tranfile.out to tranfile
$cmdlin = "mv $scripts_dir/awk/tranfile.out $scripts_dir/awk/tranfile";
system ($cmdlin);
