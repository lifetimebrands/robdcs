/* The following script updates the usr_weeklystats table. */
/* The script will run on a daily basis and update with various categories */
/* Looks at production and the archive tables */ 

create table var_prtqty_stats as
select 'XXXXXXXXXXXXXXXXXXXX' stoloc, x.prtnum, sum(avlqty) units, sum(avlqty * nvl(prtmst.untcst,0)) value,
sum((avlqty/prtmst.untcas)*(caslen*caswid*cashgt/1728)) qube, decode(x.invsts,'A',x.invsts) invsts, trunc(sysdate) sys_date
from
(select distinct invsum.stoloc,locmst.arecod,
          invdtl.prtnum,
           invdtl.lotnum,
           invdtl.invsts,
           invsum.untqty,
           invsum.comqty,
           decode (locmst.pckflg, 1, nvl (((invsum.untqty + invsum.pndqty) - invsum.comqty), 0), 0) avlqty,
          0 pndqty
     from locmst,
          invsum,
          invlod,
          invsub,
          invdtl
    where invsum.prt_client_id = invdtl.prt_client_id
      and invsum.prtnum = invdtl.prtnum
      and invdtl.subnum = invsub.subnum
      and invsub.lodnum = invlod.lodnum
      and invlod.stoloc = invsum.stoloc
      and invdtl.prt_client_id = '----'
      and invsum.stoloc = locmst.stoloc
      and invsum.arecod = locmst.arecod) x,prtmst,ftpmst
  where prtmst.prtnum = x.prtnum
  and prtmst.prtnum = ftpmst.ftpcod
group by x.prtnum, x.invsts;

-- insert distinct items in 4 -wall
 insert into usr_weeklystats(counts ,trndte,status)
 select count(distinct prtnum),sys_date,'IVI' from var_prtqty_stats
 group by sys_date;

-- insert units in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(units),sys_date,'IVU' from var_prtqty_stats
 group by sys_date;

-- insert total $ value of inventory in 4-wall

 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVV' from var_prtqty_stats
 group by sys_date;

-- insert $ value of available items in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVA'
 from var_prtqty_stats
 where invsts = 'A'
 group by sys_date;

-- insert $ value of not available items in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVN'
 from var_prtqty_stats
 where nvl(invsts,'X') != 'A'
 group by sys_date;

-- insert cubic volume of storage in 4-wall

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'ICV'
from var_prtqty_stats
group by sys_date;

commit;

truncate table var_prtqty_stats;

/* Obtain Production Inventory info */

insert into var_prtqty_stats
select locmst.stoloc,invdtl.prtnum,0 units, sum(untqty * nvl(untcst,0)) value,
sum((untqty/prtmst.untcas)*(caslen*caswid*cashgt/1728)) qube,
decode(invsts,'A',invsts) invsts, trunc(sysdate) sys_date
        from
        locmst,prtmst,ftpmst,invlod,invsub,invdtl
        where locmst.arecod = 'PROD'
        and locmst.stoloc = invlod.stoloc
        and invlod.lodnum = invsub.lodnum
        and invsub.subnum = invdtl.subnum
       and invdtl.prtnum = prtmst.prtnum
       and prtmst.prtnum = ftpmst.ftpcod
    group by locmst.stoloc,invdtl.prtnum, decode(invsts,'A',invsts);

-- insert distinct items in production
insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'PVI'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;

-- insert total production $ value
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVV'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;

-- insert production $ value of available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVA'
from var_prtqty_stats
where invsts = 'A'
and stoloc not like 'SHOP%'
group by sys_date;

-- insert production $ value of not available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
and stoloc not like 'SHOP%'
group by sys_date;

-- insert cubic volume of storage in production

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'PCV'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;

-- insert distinct items in production- shop locations
insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'PSI'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

-- insert total production $ value -shop
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSV'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

-- insert production $ value of available items -shop locations
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSA'
from var_prtqty_stats
where invsts = 'A'
and stoloc like 'SHOP%'
group by sys_date;

-- insert production $ value of not available items -shop locations
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
and stoloc like 'SHOP%'
group by sys_date;

-- insert cubic volume of storage in production -shop locations

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'PSC'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

commit;

truncate table var_prtqty_stats;

/* Obtain Ship Stage information */
/* script taken from Open Orders Report, numbers should match everything in Printed, Staged or Transfer status */

insert into var_prtqty_stats
select NULL stoloc, x.prtnum, 0 units, sum(unsamt) value,sum((units/untcas) * (caslen*caswid*cashgt/1728)) qube,
decode(invsts,'A',invsts) invsts, trunc(sysdate) sys_date
 from
(SELECT ord_line.prtnum,
        rtrim(ord_line.ordnum) ordnum,
        rtrim(dscmst.lngdsc) status,
        decode(shipment_line.linsts, 'I', sum(ord_line.vc_cust_untcst * (shipment_line.stgqty+shipment_line.inpqty)),
        sum(ord_line.vc_cust_untcst*shipment_line.pckqty)) unsamt,
   decode(shipment_line.linsts, 'I',sum(shipment_line.stgqty+shipment_line.inpqty),sum(shipment_line.pckqty)) units,
       ord_line.invsts
       FROM  dscmst,
           shipment_line, ord_line, shipment
        where ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.ship_id       = shipment.ship_id
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled'
        GROUP BY ord_line.prtnum,ord_line.ordnum, dscmst.lngdsc,
        shipment_line.linsts, ord_line.invsts) x,prtmst,ftpmst
where x.status in ('Staged','Printed','Transfer')
and x.prtnum = prtmst.prtnum
and prtmst.prtnum = ftpmst.ftpcod
group by x.prtnum,invsts;

--insert distinct items in Ship Stage

insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'SVI'
from var_prtqty_stats
group by sys_date;

-- insert total Ship Stage $ value

insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVV'
from var_prtqty_stats
group by sys_date;

-- insert Ship Stage $ value for available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVA'
from var_prtqty_stats
where invsts = 'A'
group by sys_date;

-- insert Ship Stage $ value for not available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
group by sys_date;

-- insert cubic volume stored in Ship Stage

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'SCV'
from var_prtqty_stats
group by sys_date;

commit;

drop table var_prtqty_stats;

create table var_inventory_stats as
        SELECT prtmst.prtnum, vc_prdcod, invdtl.untqty, invsum.arecod, invsum.stoloc, untcst, invsum.invsts, trunc(sysdate) dte
        from prtmst,invsum,invlod,invsub,invdtl
        where prtmst.prtnum = invsum.prtnum
        and prtmst.prt_client_id = invsum.prt_client_id
        and invsum.stoloc = invlod.stoloc
        and invsum.prtnum = invdtl.prtnum
        and invsum.prt_client_id = invdtl.prt_client_id
       and invlod.lodnum = invsub.lodnum
    and invsub.subnum = invdtl.subnum;

/* Insert # of Discontinued Items in Warehouse - Code DIS */
/* This script emulates ENG-Discontinued Items Report */
/* areas SHIP, SSTG and location %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct prtnum),trunc(dte),'DIS'
    from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
group by trunc(dte);
commit;

/* Insert $ Value of Discontinued Items - Code DVA */
/* This script emulates ENG-Discontinued Items Report */
/* areas SHIP, SSTG and location %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT sum(untqty * nvl(untcst,0)),trunc(dte),'DVA'
    from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
group by trunc(dte);
commit;

/* Insert # of Locations w Discontinued Items - Code RDI */
/* ISTG must be obtained by different means . */
/* This script emulates ENG-Discontinued Items Report */
/* Does not look at areas FGPROD, NOPK or PROB  */
/* area ISTG and stoloc %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct(stoloc)),trunc(dte),'RDI'
from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
    and arecod  in ('RACKHIGH','RACKCOMP','RACKMED','RACKNC','RACKSPCK',
'RACKSRES','TOWERFCC','TOWERFCP','TOWERREP','CFPP','GWALL')
group by trunc(dte);
commit;

/* Insert # of Rework Items in Warehouse - Code RWI */
/* This script emulates the IC-Rework report */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),trunc(dte),'RWI'
             from var_inventory_stats
              where
              invsts in ('W','C','O','B','T','L')
group by trunc(dte);
commit;


/* Insert $ Value of Rework Items- Code RWV */
/* This script emulates the IC-Rework report */

insert into usr_weeklystats(counts,trndte,status)
SELECT sum(untqty * nvl(untcst,0)),trunc(dte),'RWV'
from var_inventory_stats 
where invsts in ('W','C','O','B','T','L')
group by trunc(dte);
commit;

/* Insert # of Locations Containing Rework Items - Code RRI */
/* This script emulates the IC-Rework report */
/* Does not look at areas FGPROD, NOPK or PROB   */
/* ISTG does not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct (stoloc)),trunc(dte),'RRI'
from var_inventory_stats 
where invsts in ('W','C','O','B','T','L')
and arecod  in  ('RACKHIGH','RACKCOMP','RACKMED','RACKNC','RACKSPCK',
'RACKSRES','TOWERFCC','TOWERFCP','TOWERREP','CFPP','GWALL')
group by trunc(dte);
commit;

drop table var_inventory_stats;

/* Delete previous days inventory information */
/* Except on first day of the month */
/* ADD_MONTHS(LAST_DAY(sysdate),-1)+1 gives 1st day of month */

delete from usr_weeklystats
where status in ('DIS','DVA','RDI','RWI','RWV','RRI','IVI','IVU','IVV','IVA','IVN','PVI', 
'PVV','PVA','PVN','SVI','SVV','SVA','SVN','ICV','PCV','SCV','PSV','PSA','PSN','PSI','PSC')
and trunc(trndte) = trunc(sysdate) -1
and trunc(sysdate) != trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1);
commit;

/* Insert Total Cube of Shipments - Code CUE */

insert into usr_weeklystats(counts,trndte,status)
select sum((x.shpqty/x.untcas)*(x.cube/1728)) qube, dispatch_dte, 'CUE'
                     from
                     (select distinct shipment.ship_id, i.prtnum, ord.ordnum,
                                          ord_line.ordlin,shipment_line.shpqty shpqty,
                          (ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl i,ftpmst, prtmst, shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND trunc(dispatch_dte) = trunc(sysdate)-1) x
       group by dispatch_dte;
 
commit;


/* Insert Total Cube of Receipts - Code 'CUR' */

insert into usr_weeklystats(counts,trndte,status)
select trunc(sum((cube/ 1728) * (x.rcvqty/x.untcas)),2) qube, clsdte, 'CUR'
         from
           (select distinct rcvtrk.trknum,rcvlin.prtnum,rcvlin.rcvqty,prtmst.untcas untcas,
            trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum,caslen*caswid*cashgt cube,rcvlin.supnum,rcvlin.rcvkey
                                   from rcvtrk,rcvinv,rcvlin, ftpmst, prtmst, invdtl
                                   where
                                   rcvtrk.trknum = rcvlin.trknum
                                   and rcvlin.trknum = rcvinv.trknum
                                   and rcvlin.client_id = rcvinv.client_id
                                   and rcvlin.supnum = rcvinv.supnum
                                   and rcvlin.invnum = rcvinv.invnum
                                   and rcvlin.prtnum = prtmst.prtnum
                                   and prtmst.prtnum = ftpmst.ftpcod
                                   and rcvlin.rcvkey = invdtl.rcvkey(+)
                                   and rcvtrk.expdte is not null
                                   and rcvtrk.trksts = 'C'
                                   and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x
                    group by clsdte;
commit;



/* Insert Total Weight of Shipments - Code 'WGT' */

insert into usr_weeklystats(counts,trndte,status)
select sum(x.shpqty * x.vc_grswgt_each) ladwgt,trunc(x.dispatch_dte), 'WGT' status  
from 
(select distinct shipment.ship_id, i.prtnum, ord.ordnum, 
  ord_line.ordlin,shipment_line.shpqty shpqty,prtmst.vc_grswgt_each, 
    prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte 
    from invdtl i, prtmst, shipment, shipment_line,  ord, ord_line,  stop, 
         trlr tr,car_move cr 
         where i.ship_line_id = shipment_line.ship_line_id 
        AND prtmst.prtnum = ord_line.prtnum   
        AND ord.ordnum                  = ord_line.ordnum  
        AND ord.client_id               = ord_line.client_id  
        AND ord_line.client_id          = shipment_line.client_id  
        AND ord_line.ordnum             = shipment_line.ordnum  
        AND ord_line.ordlin             = shipment_line.ordlin  
        AND ord_line.ordsln             = shipment_line.ordsln  
        AND shipment_line.ship_id       = shipment.ship_id  
        AND shipment.stop_id                  = stop.stop_id  
        AND stop.car_move_id            = cr.car_move_id   
        AND cr.trlr_id                  = tr.trlr_id  
        AND shipment.shpsts  ||''= 'C'  
        AND ord.ordtyp                   != 'W'  
        AND trunc(tr.dispatch_dte) = trunc(sysdate)-1) x  
  group by trunc(x.dispatch_dte);   

commit;  

/* Insert Total Weight of Receipts - Code WGR */

insert into usr_weeklystats(counts,trndte,status)  
select trunc(sum(x.rcvqty * x.vc_grswgt_each),2) wgt, x.clsdte, 'WGR' status
from
(select distinct rcvtrk.trknum,rcvlin.prtnum,rcvlin.rcvqty,prtmst.untcas untcas,
 prtmst.vc_grswgt_each,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum,caslen,caswid,cashgt,rcvlin.supnum,rcvlin.rcvkey
                   from rcvtrk,rcvinv,rcvlin, ftpmst, prtmst, invdtl
                   where
                   rcvtrk.trknum = rcvlin.trknum
                   and rcvlin.trknum = rcvinv.trknum
                   and rcvlin.client_id = rcvinv.client_id
                   and rcvlin.supnum = rcvinv.supnum
                   and rcvlin.invnum = rcvinv.invnum
                   and rcvlin.prtnum = prtmst.prtnum
                   and prtmst.prtnum = ftpmst.ftpcod
                   and rcvlin.rcvkey = invdtl.rcvkey(+)
                   and rcvtrk.expdte is not null
                   and rcvtrk.trksts = 'C'
                   and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x
   group by x.clsdte;

commit;


/* Insert Number of Workorders Closed - Code WKC */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct wkohdr.wkonum),trunc(wkohdr.clsdte),'WKC' status
    from wkohdr,wkodtl
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C'
    and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);
commit; 

/* Insert Number of Units Going (Delivered) into Production - Code UNP */

insert into usr_weeklystats(counts,trndte,status)
select sum(tot_dlvqty),trunc(wkohdr.clsdte),'UNP' status
        from wkohdr,wkodtl
        where wkohdr.wkonum = wkodtl.wkonum
        and wkohdr.wkorev = wkodtl.wkorev
        and wkohdr.client_id = wkodtl.client_id
        and wkosts = 'C'
        and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);
commit;

/* Insert Number of Cartons Going (Delivered) into Production - Code CPR */

insert into usr_weeklystats(counts,trndte,status)
select round(sum(tot_dlvqty/decode(untcas,0,1,untcas))),trunc(wkohdr.clsdte),'CPR' status
        from wkohdr,wkodtl
        where wkohdr.wkonum = wkodtl.wkonum
        and wkohdr.wkorev = wkodtl.wkorev
        and wkohdr.client_id = wkodtl.client_id
        and wkosts = 'C'
        and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);
commit;


/* Insert Total Number of Units Coming out of Production - Code UOP */
/* Sum of both Reworks(RWP) ,BOM Units (PUP) and XBOM Units (XBM) */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'UOP' status from
(select distinct wkohdr.wkonum,prdqty,trunc(clsdte) clsdte
from wkohdr,wkodtl,prtmst
where wkohdr.wkonum = wkodtl.wkonum
and wkohdr.wkorev = wkodtl.wkorev
and wkohdr.client_id = wkodtl.client_id
and wkohdr.prtnum = prtmst.prtnum
and wkosts = 'C'
and trunc(clsdte) = trunc(sysdate) -1) i
group by trunc(i.clsdte);
commit; 


/* Insert Number of Containers Received - Code COR */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct rcvtrk.trknum),trunc(rcvtrk.clsdte),'COR' status
    from rcvtrk, rcvinv, rcvlin
    where rcvtrk.trknum = rcvinv.trknum
    and rcvtrk.trknum = rcvlin.trknum
    and rcvlin.trknum=rcvinv.trknum
    and rcvlin.supnum= rcvinv.supnum
    and rcvlin.invnum =rcvinv.invnum
    and rcvlin.client_id = rcvinv.client_id
    and rcvtrk.trksts = 'C'
    and rcvtrk.expdte is not null
    and trunc(rcvtrk.clsdte) = trunc(sysdate) -1 
group by trunc(rcvtrk.clsdte);
commit;


/* Insert Number of Items Shipped - Code IS */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ord_line.prtnum)) counts,trunc(tr.dispatch_dte),'IS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts  ||''= 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte);
commit;


/* Insert Avg# of lines Shipped - Code OLN */

insert into usr_weeklystats(counts,trndte,status)
select round(count(a.ordlin)/decode(count(distinct a.ordnum),0,1,count(distinct a.ordnum))) counts,
trunc(LAST_DAY(sysdate-1)) trndte,'OLN' status
                          from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
                          car_move f,trlr g
                           where x.host_ext_id= d.btcust
                           and x.client_id = d.client_id
                           and d.ordnum = a.ordnum
                           and d.client_id = a.client_id
                           and d.client_id = b.client_id                          
                           and d.rt_adr_id = c.rt_adr_id                         
                           and a.ordnum = b.ordnum
                           and a.ordlin = b.ordlin
                           and a.client_id = b.client_id
                           and a.ordsln = b.ordsln
                           and b.ship_id = c.ship_id
                           and c.stop_id = e.stop_id
                           and e.car_move_id  = f.car_move_id
                           and f.trlr_id  = g.trlr_id
                           and d.client_id ||''= '----'
                           and c.shpsts ||''= 'C'
                           and x.adrtyp = 'CST'
 and trunc(g.dispatch_dte) between trunc(ADD_MONTHS(LAST_DAY(sysdate -1),-1)+1)
 and trunc(LAST_DAY(sysdate-1))
and trunc(sysdate) = trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1)  /* Run on first day of the month for previous month */
group by trunc(LAST_DAY(sysdate-1));
commit; 



/* Insert Cartons Shipped - Code S */
/* updated 10/9/03 */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'S' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);
commit;


/* Insert Full Case Cartons Shipped -Code FUL */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'FUL' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte);
commit;

/* Insert Piece Pick Cartons Shipped -Code PCK */

insert into usr_weeklystats(counts,trndte,status)
 select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'PCK' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'C'  /* Piece Pick cases */
group by trunc(tr.dispatch_dte);
commit;



/* Insert % of Piece Picks Cartons Shipped - Code PPC */
/* For this script the Piece Pick and Full Case Cartons Shipped Categores */
/* code (PCK, FUL) must have run already.  We can also use the */
/* usr_carton_counts table to get the same numbers */

insert into usr_weeklystats(counts,trndte,status)
select round(pck.counts/nvl(ful.counts,0)*100,2) counts, trunc(sysdate)-1 trndte, 'PPC' status
from (select to_char(trndte,'MON') dte,sum(counts) counts from usr_weeklystats
where trndte between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1)
and  last_day(trunc(sysdate-1)) and status in ('FUL','PCK')
group by to_char(trndte,'MON')) ful,
(select to_char(trndte,'MON') dte, sum(counts) counts from
usr_weeklystats where trndte between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1)
and last_day(trunc(sysdate-1))
and status = 'PCK'
group by to_char(trndte,'MON')) pck
where pck.dte = ful.dte;

commit;

/* Insert % of Piece Pick $Value Picked - Code PPV */
/* Codes PPV, PPL and PPU update daily for the entire month. */
/* At the end of the month, the final calculation for the month */
/* should be done. */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1 trndte, round((pcpck.cost/total.cost * 100),2) cost, 'PPV' status
from
(select to_char(lstdte,'MON') pckdte, sum(b.untqty * untcst) cost
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1))
              group by to_char(lstdte,'MON')) total,
(select to_char(lstdte,'MON') pckdte,  sum(b.untqty * untcst) cost
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1))
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;
commit;

/* insert % of Piece Pick Lines Picked - Code PPL */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1, round((pcpck.lines/total.lines *100),2) lines, 'PPL' status
from
(select to_char(lstdte,'MON') pckdte, count(a.ordlin) lines
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) total,
(select to_char(lstdte,'MON') pckdte, count(a.ordlin) lines
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;

commit;

/* insert % of Piece Pick Units Picked - PPU */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1, round((pcpck.units/total.units *100),2) units, 'PPU' status
from
(select to_char(lstdte,'MON') pckdte, sum(b.untqty) units
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) total,
(select to_char(lstdte,'MON') pckdte, sum(b.untqty) units
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;

commit;


/* delete % Piece Pick Carton Shipped amount from 2 days ago */
/* do not delete on the second day of the month , would wipe out previous month */

delete from usr_weeklystats
where status in ('PPC','PPL','PPU','PPV')
and trunc(trndte) = trunc(sysdate) -2
and trunc(sysdate) != trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1)+1;
commit;


/* Insert Total Dollar Value Shipped - Code VS */

insert into usr_weeklystats(counts,trndte,status)
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) dlramt,
trunc(tr.dispatch_dte),'VS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);                  
commit;


/* Insert Total Orders Shipped - Code OS */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ord.ordnum)) counts,trunc(tr.dispatch_dte),'OS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts     ||'' = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte);
commit;


/* Get Cartons Received from Domestic Suppliers - Code CRD */

insert into usr_weeklystats(counts,trndte,status) 
select trunc(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRD' status 
           from 
          (select distinct prtmst.untcas untcas,  
            rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum 
            ,rcvlin.trknum,rcvlin.supnum 
             from rcvtrk,rcvinv,rcvlin, prtmst
              where 
              rcvtrk.trknum = rcvlin.trknum 
              and rcvlin.trknum = rcvinv.trknum  
              and rcvlin.client_id = rcvinv.client_id  
              and rcvlin.supnum = rcvinv.supnum  
              and rcvlin.invnum = rcvinv.invnum  
              and rcvlin.prtnum = prtmst.prtnum   
              and rcvtrk.expdte is not null  
              and rcvtrk.trksts = 'C'  
              and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x  
   where (length(trknum) < 6 or ASCII(upper(substr(trknum,6,1))) between 65 and 90  
              or (supnum not in ('0237','3747','2205','2680','2681','3056',  
                    '3117','3150','3250','4630','5491','5505','5528','6409','7887',  
                    '8595','8675','9467','CY02','CY05') and supnum in  
                 ('0143','1206','1301','1316'  
                   ,'1753','2112','4309','4460',  
                   '5167','5306','5321','5340','5510','5518','5534','9129',  
                '7885','8419','8673','8679','9328')))  
  group by trunc(clsdte);
  
commit;




/* insert into usr_weeklystats(counts,trndte,status) */
/* select round(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRD' status */
   /*           from usr_cartons_received_view */
           /*    where (length(trknum) < 6 or ASCII(upper(substr(trknum,6,1))) between 65 and 90 */
         /*  or (supnum not in ('0237','3747','2205','2680','2681','3056', */
           /*      '3117','3150','3250','4630','5491','5505','5528','6409','7887', */
             /*    '8595','8675','9467','CY02','CY05') and supnum in */
            /*  ('0143','1206','1301','1316' */
            /*    ,'1753','2112','4309','4460', */
           /*     '5167','5306','5321','5340','5510','5518','5534','9129', */
          /*   '7885','8419','8673','8679','9328'))) */
   /*  group by trunc(clsdte); */
/* commit; */



/* Get Cartons Received from International Suppliers - Code CRI */

insert into usr_weeklystats(counts,trndte,status)  
select trunc(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRI' status                                
               from
           (select distinct prtmst.untcas untcas,                        
            rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum
            ,rcvlin.trknum,rcvlin.supnum
                 from rcvtrk,rcvinv,rcvlin, prtmst
                    where
                    rcvtrk.trknum = rcvlin.trknum
                    and rcvlin.trknum = rcvinv.trknum
                    and rcvlin.client_id = rcvinv.client_id
                    and rcvlin.supnum = rcvinv.supnum
                    and rcvlin.invnum = rcvinv.invnum
                    and rcvlin.prtnum = prtmst.prtnum
                    and rcvtrk.expdte is not null
                    and rcvtrk.trksts = 'C'
                    and trunc(rcvtrk.clsdte) =trunc(sysdate)-1) x
         where (length(trknum) >= 6 or trknum like ('03%')
                  or (supnum in ('0237','3747','2205','2680','2681','3056',
                     '3117','3150','3250','4630','5491','5505','5528','6409','7887',
                     '8595','8675','9467','CY02','CY05') and supnum not in
                  ('0143','1206','1301','1316'
                     ,'1753','2112','4309','4460',
                   '5167','5306','5321','5340','5510','5518','5534',
                   '7885','8419','8673','8679','9328','9129')))
                 and ASCII(upper(substr(trknum,6,1))) not between 65 and 90
             group by trunc(clsdte);

commit;

/* insert into usr_weeklystats(counts,trndte,status) */
/* select round(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRI' status */
   /*      from usr_cartons_received_view */
    /*    where (length(trknum) >= 6 or trknum like ('03%') */
     /*    or (supnum in ('0237','3747','2205','2680','2681','3056', */
      /*      '3117','3150','3250','4630','5491','5505','5528','6409','7887', */
      /*      '8595','8675','9467','CY02','CY05') and supnum not in */
      /*   ('0143','1206','1301','1316' */
      /*      ,'1753','2112','4309','4460', */
      /*     '5167','5306','5321','5340','5510','5518','5534', */
      /*    '7885','8419','8673','8679','9328','9129'))) */
      /*  and ASCII(upper(substr(trknum,6,1))) not between 65 and 90 */
      /* group by trunc(clsdte); */
/* commit; */


/* Get Total Cartons Received (should add up to Intl & Domestic - Code CR */

insert into usr_weeklystats(counts,trndte,status)
 select trunc(sum(rcvqty/decode(untcas,0,1,untcas))), clsdte, 'CR'
         from
         (select distinct prtmst.untcas untcas,
                  rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum
                  ,rcvlin.trknum,rcvlin.supnum
                    from rcvtrk,rcvinv,rcvlin, prtmst
                    where
                    rcvtrk.trknum = rcvlin.trknum
                    and rcvlin.trknum = rcvinv.trknum
                    and rcvlin.client_id = rcvinv.client_id
                    and rcvlin.supnum = rcvinv.supnum
                    and rcvlin.invnum = rcvinv.invnum
                    and rcvlin.prtnum = prtmst.prtnum
                    and rcvtrk.expdte is not null
                    and rcvtrk.trksts = 'C'
                    and trunc(rcvtrk.clsdte) = trunc(sysdate)-1 )
  group by clsdte;

commit;

 
/* insert into usr_weeklystats(counts,trndte,status) */
/* select round(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CR' status */
            /*  from usr_cartons_received_view */
     /* group by trunc(clsdte); */
/* commit; */

/* Get Items(distinct parts) Received - Code IR */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),trunc(clsdte),'IR' status
        from usr_cartons_received_view
    group by trunc(clsdte);
commit;

/* Get Units Received - Code URC */

insert into usr_weeklystats(counts,trndte,status)
select sum(rcvqty),trunc(clsdte),'URC' status
        from usr_cartons_received_view
    group by trunc(clsdte);
commit;

/* Insert Orders Shipped Complete - Code SC */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'SC' status
    from usr_shippedcomplete_view
group by trunc(dispatch_dte);
commit;

/* Insert OrdersShippedComplete On Time- Code OTC */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'OTC' status
    from usr_shippedcompleteontime_view
group by trunc(dispatch_dte);
commit;

/* Insert Units Shipped Complete On Time - Code UTC */

insert into usr_weeklystats(counts,trndte,status)
select sum(shpqty) counts,trunc(dispatch_dte) trndte,'UTC' status
    from usr_shippedcompleteontime_view
group by trunc(dispatch_dte);
commit; 


/* Insert OrdersShippedNotComplete On Time- Code OTN */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'OTN' status
    from usr_shippedntcompltontime_view
group by trunc(dispatch_dte);
commit;

/* Insert Units Shipped Not Complete On Time - Code UTN */

insert into usr_weeklystats(counts,trndte,status)
select sum(shpqty) counts,trunc(dispatch_dte) trndte,'UTN' status
    from usr_shippedntcompltontime_view
group by trunc(dispatch_dte);
commit;

/* Insert # of Items Returned by Customers - Code IRT */

insert into usr_weeklystats(counts,trndte,status)
select count(b.prtnum),trunc(trndte),'IRT' status
    from dlytrn a, prtmst b
    where a.prtnum = b.prtnum
    and a.reacod = 'CUSTRTRN'
    and a.actcod = 'ADJ'
    and a.prtnum != 'LABOR'
    and trunc(a.trndte) = trunc(sysdate) - 1 
    and trnqty >= 0
   group by trunc(trndte);
commit;

/* Insert # of Units Returned by Customers - Code URT */

insert into usr_weeklystats(counts,trndte,status)
select sum(a.trnqty),trunc(trndte),'URT' status
    from dlytrn a,prtmst b
    where a.prtnum = b.prtnum
    and a.reacod = 'CUSTRTRN'
    and a.actcod = 'ADJ'
    and a.prtnum != 'LABOR'
    and trunc(a.trndte) = trunc(sysdate) - 1 
    and trnqty <> 0
   group by trunc(trndte);
commit;

/* Insert Total Units Ordered - Code 'UNO' */
/* Updated 10/14/03  */

insert into usr_weeklystats(counts,trndte,status)
select sum(a.ordqty) counts,trunc(g.dispatch_dte),'UNO' status
                  from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
                  car_move f,trlr g
                   where x.host_ext_id = d.btcust
                   and x.client_id = d.client_id
                   and d.ordnum = a.ordnum
                   and d.client_id = a.client_id
                   and d.client_id = b.client_id
                   and d.rt_adr_id = c.rt_adr_id 
                   and a.ordnum = b.ordnum
                   and a.ordlin = b.ordlin
                   and a.client_id = b.client_id
                   and a.ordsln = b.ordsln
                   and b.ship_id = c.ship_id
                   and c.stop_id = e.stop_id
                   and e.car_move_id  = f.car_move_id
                   and f.trlr_id  = g.trlr_id
                   and a.client_id ||''= '----'
                   and c.shpsts ||''= 'C'
                   and x.adrtyp = 'CST'
and trunc(g.dispatch_dte) = trunc(sysdate) -1 
group by trunc(g.dispatch_dte);
commit;

/* Insert Total Units Shipped - Code 'UNS' */

insert into usr_weeklystats(counts,trndte,status)
select sum(b.shpqty) counts,trunc(g.dispatch_dte) trndte,'UNS'  status
              from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
              car_move f,trlr g
               where x.host_ext_id = d.btcust
               and x.client_id = d.client_id
               and d.ordnum = a.ordnum
               and d.client_id = a.client_id
               and d.client_id = b.client_id             
               and d.rt_adr_id = c.rt_adr_id 
               and a.ordnum = b.ordnum
               and a.ordlin = b.ordlin
               and a.client_id = b.client_id
               and a.ordsln = b.ordsln
               and b.ship_id = c.ship_id
               and c.stop_id = e.stop_id
               and e.car_move_id  = f.car_move_id
               and f.trlr_id  = g.trlr_id
               and a.client_id ||''= '----'
               and c.shpsts ||''= 'C'
               and x.adrtyp = 'CST'
      and trunc(g.dispatch_dte)  = trunc(sysdate) - 1
    group by trunc(g.dispatch_dte);
commit;

/* Insert Orders Rolling Over(once at beginning of month) - Code 'ORO' */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(d.adddte) trndte,'ORO' status
           from ord d,ord_line a,shipment_line b,shipment c
             where d.ordnum = a.ordnum
             and d.client_id = a.client_id
             and d.client_id = b.client_id
             and d.rt_adr_id = c.rt_adr_id
             and a.ordnum = b.ordnum
             and a.ordlin = b.ordlin
             and a.client_id = b.client_id
             and a.ordsln = b.ordsln
             and b.ship_id = c.ship_id
             and a.client_id = '----'
             and c.shpsts != 'C'
             and c.loddte is null
        and to_date(substr(d.adddte,4,3),'MON') < to_date(substr(sysdate,4,3),'MON')
          and trunc(d.adddte) between trunc(ADD_MONTHS(sysdate,-1)) and trunc(sysdate) -1
   and substr(trunc(sysdate),1,2) = '01'
      group by trunc(d.adddte);
commit;

/* Insert All Categories into summary table (once every quarter, 2 quarters prior ) */

insert into usr_weeklystatssum(month,count,status)
        select trunc(trndte) month, 
        counts,status 
           from usr_weeklystats 
            where trunc(trndte) between trunc(ADD_MONTHS((sysdate),-6)) and trunc(ADD_MONTHS((sysdate-1),-3)) 
         and substr(trunc(sysdate),1,6) in ('01-JAN','01-APR','01-JUL','01-OCT');
commit;



/* Clear out Weeklystats Table (once every quarter, 2 quarters prior) */

delete from usr_weeklystats
where trunc(trndte) between trunc(ADD_MONTHS((sysdate),-6)) and trunc(ADD_MONTHS((sysdate-1),-3))
and substr(trunc(sysdate),1,6) in ('01-JAN','01-APR','01-JUL','01-OCT');
commit;


/* Clear out Rollovers Table (once every quarter) */

/* delete from usr_rollovers */
/* where trunc(trndte) between trunc(ADD_MONTHS((sysdate),-6)) and trunc(ADD_MONTHS((sysdate-1),-3)) */
/* and substr(trunc(sysdate),1,6) in ('01-JAN','01-APR','01-JUL','01-OCT'); */
/* commit; */

/* Insert Number of BOM Units Produced -Code PUP */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'PUP' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') != 'XBOM'
        and wkosts = 'C' and exists (select 'x' from wkodtl i
        where i.wkonum = wkohdr.wkonum and
        i.prtnum in ('PRODOHLABR','LABOR'))
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);
commit;

/* Insert Number of Rework Units Produced - Code RWP */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'RWP' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') != 'XBOM'
        and wkosts = 'C' and not exists (select 'x' from wkodtl i
        where i.wkonum = wkohdr.wkonum and
        i.prtnum in ('PRODOHLABR','LABOR'))
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);
commit;


/* Insert Number of XBOM Units Produced - Code XBM */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'XBM' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') = 'XBOM'
        and wkosts = 'C' 
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);
commit;


/* Insert todays new inventory records in inventory table (except on last day of month) */

--create table var_prtqty_stats as
--select var_prtqty_view.prtnum prtnum , sum(avlqty) IVU, sum(avlqty * nvl(prtmst.untcst,0)) IVV,
-- decode(invsts,'A',invsts) invsts,trunc(sysdate) sys_date
-- from prtmst,var_prtqty_view
-- where prtmst.prtnum = var_prtqty_view.prtnum
-- group by var_prtqty_view.prtnum, decode(invsts,'A',invsts);

/* insert into usr_inventory(prtnum,stoloc,untqty,untcst,invsts,vc_prdcod,dtlnum) */
/* SELECT invdtl.prtnum,invsum.stoloc,invdtl.untqty,prtmst.untcst,invdtl.invsts,prtmst.vc_prdcod, */
/* invdtl.dtlnum */
     /* from invdtl,invsub,invlod,invsum,prtmst */
    /*  where invdtl.prtnum = prtmst.prtnum */
    /*  and invdtl.subnum = invsub.subnum */
    /*  and invlod.lodnum = invsub.lodnum */
    /*  and invsum.prtnum = invdtl.prtnum */
    /*  and invsum.prt_client_id = invdtl.prt_client_id */
    /*  and invdtl.prt_client_id = prtmst.prt_client_id */
    /* and invsum.stoloc = invlod.stoloc */
/* and not exists(select 'X' from usr_inventory a where a.dtlnum = invdtl.dtlnum) */
/* and trunc(sysdate) != usrGetEndDate(sysdate); */
/* commit; */

/* Insert inventory for last day of month only */

/* create table usr_inventory_main(prtnum varchar2(30) not null, */
/* stoloc varchar2(20) not null, untqty number(10),pndqty number(10), */
/* comqty number(10),untcst number(19,4),invsts varchar2(1)); */
/* commit; */

/* Insert Last day of the months inventory records (used by following scripts) */
/* usrGetEndDate(sysdate) */

/* insert into usr_inventory_main(prtnum,stoloc,untqty,pndqty,comqty,untcst,invsts) */
/* SELECT distinct invsum.prtnum,invsum.stoloc,invsum.untqty,invsum.pndqty,invsum.comqty,prtmst.untcst,invsum.invsts */
   /*   from invdtl,invsub,invlod,invsum,prtmst */
   /*   where invdtl.prtnum = prtmst.prtnum */
    /*  and invdtl.subnum = invsub.subnum */
    /*  and invlod.lodnum = invsub.lodnum */
    /*  and invsum.prtnum = invdtl.prtnum */
    /*  and invsum.prt_client_id = invdtl.prt_client_id */
    /*  and invdtl.prt_client_id = prtmst.prt_client_id */
    /* and invsum.stoloc = invlod.stoloc */
/* and trunc(sysdate) != trunc(LAST_DAY(sysdate)); */    
/* commit; */

/* Insert Total Inventory $ Value (last day of month) - Code IVV */

/* insert into usr_weeklystats(counts,trndte,status) */
/* SELECT round(sum(((untqty+pndqty)-comqty) * nvl(untcst,0))),trunc(sysdate),'IVV' */
/*  from usr_inventory_main */
/* where trunc(sysdate) = trunc(LAST_DAY(sysdate)) */ 
/* group by trunc(sysdate); */
/* commit; */

/* Insert Total Items in Inventory (last day of month) - Code IVI */

/* insert into usr_weeklystats(counts,trndte,status) */
/* SELECT count(distinct(prtnum)),trunc(sysdate),'IVI' */
/* from usr_inventory_main */
/* where trunc(sysdate) != trunc(LAST_DAY(sysdate)) */ 
/* group by trunc(sysdate); */
/* commit; */

/* Insert Total Units in Inventory (last day of month) - Code IVU */

/* insert into usr_weeklystats(counts,trndte,status) */
/* SELECT sum((untqty+pndqty)- comqty),trunc(sysdate),'IVU' */
/* from usr_inventory_main */
/* where trunc(sysdate) = trunc(LAST_DAY(sysdate)) */ 
/* group by trunc(sysdate); */
/* commit; */


/* Insert Inventory $ Value (Available Status, last day of month) - Code IVA */

/* insert into usr_weeklystats(counts,trndte,status) */
/* SELECT round(sum(((untqty+pndqty)-comqty) * nvl(untcst,0))),trunc(sysdate),'IVA' */
/* from usr_inventory_main */
/* where invsts = 'A' */
/* and trunc(sysdate) = trunc(LAST_DAY(sysdate)) */ 
/* group by trunc(sysdate); */
/* commit; */

/* Insert Inventory $ Value - (Non Available Status, last day of month) - Code IVN */

/* insert into usr_weeklystats(counts,trndte,status) */
/* SELECT round(sum(((untqty+pndqty)-comqty) * nvl(untcst,0))),trunc(sysdate),'IVN' */
/* from usr_inventory_main */
/* where invsts != 'A' */
/* and trunc(sysdate) = trunc(LAST_DAY(sysdate)) */
/* group by trunc(sysdate); */
/* commit; */

/* Get rid of usr_inventory_main table(every day) */

/* truncate table usr_inventory_main; */

/* drop table usr_inventory_main; */
/* commit; */

/* Clear out inventory table (last day of month) */

/* delete from usr_inventory */
/* where trunc(sysdate) = trunc(LAST_DAY(sysdate)); */
/* commit; */

