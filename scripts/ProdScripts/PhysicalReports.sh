#!/usr/bin/ksh
#
# This script will run the Physical Reports using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports

sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-LottedException.out
@PI-LottedExceptionReport.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 20000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-VarianceByLocation.out
@PI-VarianceByLocation.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-StatusChangeDetail.out
@PI-StatusChangeDetail.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-LotCodeChangeDetail.out          
@PI-LotCodeChangeDetail.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-CountedFullReport.out
@PI-CountedFullReport.sql 
spool off
exit
//
sql << //
set trimspool on
set pagesize 5000
set linesize 200
spool /opt/mchugh/prod/les/oraclereports/PI-CountedEmptyReport.out
@PI-CountedEmptyReport.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-ItemChangeDetail.out
@PI-ItemChangeDetail.sql 
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/PI-compare_1.out
@PI-compare_1.sql
spool off
exit
//
exit 0
