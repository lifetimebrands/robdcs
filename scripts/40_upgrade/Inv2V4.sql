-- This script will take from DCS 4.0 all inventory that is in the four walls
-- and is not in building 3 or a ship staging location
set pagesize 1000
set head off
set feed off
set line 300
select 'insert into var_v4trn values ( '''||rtrim(invlod.lodnum)||''','''|| 
	rtrim(invlod.stoloc)||''','''|| 
	rtrim(prtnum)|| ''','||
	untcas||','|| untpak||','''|| 
	rtrim(invsts) ||''','''|| 
	rtrim(ftpcod)||''','''|| 
	rtrim(orgcod)||''','''|| 
	rtrim(revlvl)||''','''|| 
        rtrim(lotnum)||''', to_date('''||
        to_char(min(fifdte),'DD-Mon-YYYY HH:MI:SS') ||
           ''',''DD-Mon-YYYY HH:MI:SS''), '||
        sum(untqty)||', ''N'');' 
from invdtl, invsub, invlod, locmst 
where locmst.arecod in 
  (select arecod 
   from aremst 
   where pckcod != 'N' 
     and usr_bldnum != '3'
     and fwiflg = 'Y')
and locmst.stoloc = invlod.stoloc 
and invlod.lodnum = invsub.lodnum 
and invsub.subnum = invdtl.subnum
group by rtrim(invlod.lodnum), rtrim(prtnum), untcas, untpak, rtrim(invsts), 
	 rtrim(orgcod), rtrim(ftpcod), rtrim(revlvl), rtrim(lotnum), 
	 rtrim(invlod.stoloc);
