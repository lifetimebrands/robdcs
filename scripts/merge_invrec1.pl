#!/usr/local/bin/perl 
#######################################################################

# merge_invrec1.pl --  This script basically compresses all the files
#                      in the host out directory. Earlier, when 4.0
#                      was up and running, this was replaced by merge_invrec.pl. 
# DCS 
# usage : --
#######################################################################
use IO::Handle;

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

# Set the hostout directory in the 5.x system
my $hostout_dir=$ENV{LESDIR}.$par_pthsep."files".$par_pthsep."hostout".$par_pthsep."prodrecon/";
my $tar_file="";
my $log_file = "$ENV{LESDIR}/log/merge_invrec1.log";
my $rec_fil_count = 0;
# Set the environment variables in the 4.0 system
$status = open(LOGFILE, "> $ENV{LESDIR}/log/merge_invrec1.log") || die "Unable to open log file. exiting...";

LOGFILE->autoflush(1);

printf (LOGFILE "The hostout directory is $hostout_dir");

chdir ($hostout_dir) || die "Failed to chdir to hostout directory ";

#Verify that we have 1 REC file in the directory now.

opendir (DIRLST, ".") || die "Error Getting directory listing. \n Stopped";
@dirlst = grep ( /REC/, readdir (DIRLST));
close (DIRLST);

$rec_fil_count = $#dirlst + 1;

printf ($rec_fil_count);
if ($rec_fil_count != 1)
{
   printf (LOGFILE "\n Check the number of REC in the directory now. Expecting 1. Found $rec_file_count.Exiting.... \n");
   printf ( "\n Check the number of REC in the directory now. Expecting 1. Exiting....");
#  close (LOGFILE);
#  exit 1;
} 
chdir ($hostout_dir) || die "Failed to chdir to hostout prodrecon directory ";

#Before I go further,  I need to move any existing tar files in the 5.x system.
if ( -e <hostout*.Z> )
{
   foreach $tar_file (<hostout*.Z>)
   {
     rename ($tar_file, "prc".$par_pthsep.$tar_file);
   }
}

($day, $month) = (localtime)[3,4];                         
$tar_file = sprintf ("hostout%2.2d%2.2d.tar", $month + 1, $day);
# Create a new tar file.
system ("tar -cf $tar_file *.txt; compress *.tar");
exit 0;
