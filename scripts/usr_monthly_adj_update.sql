
/* Update script is used to insert yesterdays previous daily tran records */
/* into the usr_monthly_adjs table.  For use in the IC-Monthly Adjustment Detail Report */

alter session set nls_date_format ='dd-mon-yyyy HH:MI:SS AM';

insert into usr_monthly_adjs(prtnum,lngdsc,actcod,reacod,adjqty,frstol,tostol,usr_id,dtlnum,trndte,adjamt,untcst)
select c.prtnum, substr(b.lngdsc,1,25) lngdsc, c.actcod, c.reacod, sum(decode(c.actcod,'ADJ',c.trnqty,c.trnqty*-1)) adjqty, c.frstol, 
c.tostol, c.usr_id, c.dtlnum, 
    c.trndte, sum(decode(c.actcod,'ADJ',c.trnqty,c.trnqty*-1) * a.untcst) adjamt, a.untcst
    from  prtmst a,  dscmst b, dlytrn c
    where a.prtnum=c.prtnum
    and a.prt_client_id = c.prt_client_id
    and c.prtnum != 'LABOR'
    and c.reacod is not null
    and c.reacod = b.colval
    and b.colnam ='reacod'
     /* and c.reacod = 'AUTHDISP' */
   and (c.actcod = 'ADJ' or (c.actcod = 'GENMOV' and c.tostol = 'PERM-CNT-LOC') or (c.actcod = 'PCEPCK' and c.reacod = 'CYCLE'))
   and trndte between  to_date(substr(sysdate-1,1,11)||' 12:00:00 AM')  and to_date(substr(sysdate-1,1,11)||' 11:59:59 PM')
   group
   by c.trndte, substr(b.lngdsc,1,25), c.actcod, c.reacod, c.prtnum, c.frstol, c.tostol, c.usr_id, c.dtlnum, a.untcst
   having sum(c.trnqty) <> 0
  order by 10 
/
