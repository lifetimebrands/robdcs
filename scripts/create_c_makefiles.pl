#!/opt/perl5/bin/perl
#
# Dynamically create the makefile and makefile.nt files for a C 
# libsrc directory
#
# NOTE: you must have cygwin tools installed and in your path for
# NT/2000 users.
#
# Usage: create_c_makefiles.pl (run in the directory you wish to 
#                              create the makefile in)

my @line;

#we will open the makefile.nt
#we will dynamicaly create it based on the c functions in it's directory 
chdir ($ENV{LESDIR}."/src/libsrc/varint"); 
unless (open(OUTFILE, ">makefile.nt"))
{die ("Cannot open input file makefile.nt\n");}
unless (open(OUTFILE2, ">makefile"))
{die ("Cannot open input file makefile\n");}

#we will print all the *.c files into a file
#the wierd name is to decrease the chances of writing over someones file
system("ls -1 *.c > sourcecode.names.txt");
unless (open(INFILE, "sourcecode.names.txt"))
{die ("Cannot open output file file1\n");}

#print make file header for NT
print OUTFILE ("LESDIR=..\\..\\..");
print OUTFILE ("\n\n");
print OUTFILE ("TARGET=VARint\n");
print OUTFILE ("TARGETTYPE=.dll\n\n");
print OUTFILE ("OFILES=");
#print make file header for Unix
print OUTFILE2 ("LESDIR=../../..");
print OUTFILE2 ("\n\n");
print OUTFILE2 ("include \$\(LESDIR)/makefiles/StandardHeader.mk\n\n");
print OUTFILE2 ("LIBNAME=VARint\n\n");
print OUTFILE2 ("OFILES=");

while (<INFILE>)
{
@line = split('.c\n');
$command = shift @line;
#Do not print the slash on the last line
print OUTFILE ("$command");
print OUTFILE2 ("$command");
if (eof(INFILE))
   {
    print OUTFILE (".obj \n\n");
    print OUTFILE2 (".o \n\n");
   }
else
   { 
    print OUTFILE (".obj \\\n");
    print OUTFILE2 (".o \\\n");
   }
}

#print tail for each makefile
print OUTFILE ("!include \$\(LESDIR\)\\makefiles\\ntstd.mk\n");
print OUTFILE2 ("include \$\(LESDIR)/makefiles/Component.mk\n");
print OUTFILE2 ("include \$\(LESDIR)/makefiles/StandardFooter.mk\n");
unless (close(INFILE)) {die ("Cannot close infile")}


system("rm sourcecode.names.txt");
#system("mv makefile $ENV{LESDIR}/src/libsrc/varint");
