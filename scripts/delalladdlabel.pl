#This is for the sorter simulation

#!perl
use lib "$ENV{MOCADIR}/scripts";
use MocaReg; 
#use strict;
use DBI;

$file_name = $ARGV[0];

# Open a file for reading
$in = $file_name;

$/=",";

open IN, $in or die "Cannot open $in for read :$!";


# Open a file for writing
$out = "delalladdlabel.dat";
open OUT, ">$out" or die "Cannot open $out for write :$!";

$count = 0;

while (<IN>)
{
   alltrim($_);
   push (@list, $_);   
}

$counter = 0;

for ($x=0; $x <=$#list; $x++) 
{
   if ($counter == 0)
   {
      $space_cnt = 20-length($list[$x]);
      print OUT "DELALLADDLABEL:",'0'x $space_cnt,"$list[$x]:";
   }   
   if ($counter == 1)
   {
      $space_cnt = 19-length($list[$x]);
      print OUT "$list[$x]",'0'x$space_cnt,":";
   }   
   if ($counter == 2)
   {
      print OUT "$list[$x]","|:!\n";
   
   }
   if ($counter == 3)
   {
      print OUT "$list[$x]",":!\n";
   }
   $counter++;

   if ($counter == 3)
   {
      $counter = 0;
   }
}


sub alltrim($)
{
  $_ = shift;
  s/^\s*//;
  s/\s*$//;
  s/,//;
  return $_;
}

