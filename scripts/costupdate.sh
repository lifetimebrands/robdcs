#!/usr/bin/ksh
#
# This script will run the Cost update before the Flash every day CW 1/8/09. 
#


#. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the report.




sql << //
truncate table usr_parts;
exit
//
/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/parts.ctl -D /opt/mchugh/prod/les/db/data/load/parts -d parts.csv

cd ${LESDIR}/scripts

sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/ItemCostsUpdate.out
@ItemCostsUpdate.sql
spool off
exit
//
exit 0




