/************************************************************************************/
/* This script will update the usr_carton_counts table on a nightly basis with      */
/* Piece Pick and Full Case counts from the previous business day                   */
/* Piece Pick information will stored under Code PP and Full Case information       */
/* will be store under code FC.  This information is gathered at the request of the */
/* controller, allowing him to access shipped carton counts                         */
/* The update will be done via the cron nightly process                             */
/************************************************************************************/



/* Insert the previous day's information into the usr_carton_counts table           */


alter session set nls_date_format ='dd-mon-yyyy';

insert into usr_carton_counts
(select trunc(tr.dispatch_dte) shpdte, count(distinct(i.subnum)) counts, 'PP' coding
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop, adrmst lookup1,
 trlr tr,car_move cr
 where i.ship_line_id            =  sd.ship_line_id
 AND substr(i.subnum,1,3) ='CTN'
 AND ord.st_adr_id               = lookup1.adr_id
 AND ord.client_id               = lookup1.client_id
 AND ord.ordnum                  = ord_line.ordnum
 AND ord.client_id               = ord_line.client_id
 AND ord_line.client_id          = sd.client_id
 AND ord_line.ordnum             = sd.ordnum
 AND ord_line.ordlin             = sd.ordlin
 AND ord_line.ordsln             = sd.ordsln
 AND sd.ship_id                  = sh.ship_id
 AND sh.stop_id                  = stop.stop_id
 AND stop.car_move_id            = cr.car_move_id
 AND cr.trlr_id                  = tr.trlr_id
 AND sh.shpsts                   = 'C'
 AND ord.ordtyp                   != 'W'
 AND trunc(tr.dispatch_dte)  = trunc(sysdate) - 1
 group by trunc(tr.dispatch_dte)
 union
 select trunc(tr.dispatch_dte) shpdte, count(distinct(i.subnum)) counts, 'FC'
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop, adrmst lookup1,
 trlr tr,car_move cr
 where i.ship_line_id            =  sd.ship_line_id
 AND substr(i.subnum,1,3) <> 'CTN'
 AND ord.st_adr_id               = lookup1.adr_id
 AND ord.client_id               = lookup1.client_id
 AND ord.ordnum                  = ord_line.ordnum
 AND ord.client_id               = ord_line.client_id
 AND ord_line.client_id          = sd.client_id
 AND ord_line.ordnum             = sd.ordnum
 AND ord_line.ordlin             = sd.ordlin
 AND ord_line.ordsln             = sd.ordsln
 AND sd.ship_id                  = sh.ship_id
 AND sh.stop_id                  = stop.stop_id
 AND stop.car_move_id            = cr.car_move_id
 AND cr.trlr_id                  = tr.trlr_id
 AND sh.shpsts                   = 'C'
 AND ord.ordtyp                   != 'W'
 AND trunc(tr.dispatch_dte)   = trunc(sysdate) - 1 
 group by trunc(tr.dispatch_dte))
/
commit;
