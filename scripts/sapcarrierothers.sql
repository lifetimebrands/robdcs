insert into carmtx
    (mtxcol,
     mtxval,
    cargrp,
    seqnum,
    carcod,
    srvlvl,
    netwgt,
    grswgt,
    mod_usr_id,
    moddte)
    select
    mtxcol,
    sap_gp_xref_routes.sap_id,
    cargrp,
    seqnum,
    carcod,
    srvlvl,
    netwgt,
    grswgt,
    mod_usr_id,
    sysdate
    from carmtx, sap_gp_xref_routes
    where carmtx.mtxval = sap_gp_xref_routes.gp_id
    and sap_gp_xref_routes.sap_id not in (select carmtx.mtxval from carmtx
   where carmtx.mtxval = sap_gp_xref_routes.sap_id);
