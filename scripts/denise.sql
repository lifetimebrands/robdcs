select trunc(a.pckdte), sum(a.appqty) total
     from pckwrk a, invdtl b
      where a.wrktyp ='P'
      and a.appqty > 0
      and a.subucc is not null
      and a.wrkref = b.wrkref 
      and a.prtnum = b.prtnum 
      and a.prt_client_id = b.prt_client_id
    and a.subucc is not null
group by a.pckdte
