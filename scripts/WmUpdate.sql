/* This script follows the updating steps for the costs  */
/* updates on parts in the prtmst table, performed every Friday.  */
/* This can be run after the new parts.csv file is */
/* placed in /opt/mchugh/prod/les/db/data/load/parts directory */
/* the usr_parts table has been truncated and updated. */
/* When doing the dload parts parts.csv, if any major errors */ 
/* occur that cannot be fixed, report to Denise.  If one part errors */
/* investigate, because it could cause other records to fail. */


/* Do the following queries */

/* Get a count of everything in usr_parts */

select count(*) from usr_parts;

/* Get a count of everything in usr_parts that also */
/* exists in prtmst (these are the only parts that should be updated */

select count(*) from usr_parts a
    where exists(select 'x' from prtmst b
    where a.prtnum = b.prtnum);

/* Spool out the details of the parts which exist in */
/* prtmst and usr_parts. */

spool Parts.txt
set pagesize 20000

select a.prtnum,a.untcst
from prtmst a
where exists(select 'x' from usr_parts c
    where a.prtnum = c.prtnum);

spool off

/* update related parts in usr_parts and prtmst */

update prtmst a
    set a.untcst=(select b.untcst from usr_parts b
    where a.prtnum = b.prtnum)
   where exists(select 'x' from usr_parts c
    where a.prtnum = c.prtnum);


commit;

/
