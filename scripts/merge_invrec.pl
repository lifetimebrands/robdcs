#!/usr/local/bin/perl 
#######################################################################

# merge_invrec.pl --  Script to merge reconciliations from 5.1 and 4.0 
# DCS 
# usage : --
#######################################################################
use IO::Handle;

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

# Set the hostout directory in the 5.x system
my $hostout_dir=$ENV{LESDIR}.$par_pthsep."files".$par_pthsep."hostout";
my $tar_file="";
my $log_file = "$ENV{LESDIR}/log/merge_invrec.log";
my $rec_fil_count = 0;

# Set the environment variables in the 4.0 system
my $hostout_40="/home/app/dcs/projects/prod/hostout/oldinv";
my $remote_host="172.16.1.23";
my $remote_user="dcsmgr";
my $remote_pwd="dcsmgr";

$status = open(LOGFILE, "> $ENV{LESDIR}/log/merge_invrec.log") || die "Unable to open log file. exiting...";

LOGFILE->autoflush(1);

printf (LOGFILE "The hostout directory is $hostout_dir");

chdir ($hostout_dir) || die "Failed to chdir to hostoutdirectory ";

#Verify that we have 1 REC file in the directory now.

opendir (DIRLST, ".") || die "Error Getting directory listing. \n Stopped";
@dirlst = grep ( /REC/, readdir (DIRLST));
close (DIRLST);

$rec_fil_count = $#dirlst + 1;

printf ($rec_fil_count);
if ($rec_fil_count != 1)
{
   printf (LOGFILE "\n Check the number of REC in the directory now. Expecting 1. Found $rec_file_count.Exiting.... \n");
   printf ( "\n Check the number of REC in the directory now. Expecting 1. Exiting....");
   close (LOGFILE);
   exit 1;
} 

$rec_file_50 = $dirlst[0];

#Before I get data from 4.0, I need to move any existing tar files in the 5.x system.
if ( -e <hostout*.Z> )
{
   foreach $tar_file (<hostout*.Z>)
   {
     rename ($tar_file, "prc".$par_pthsep.$tar_file);
   }
}

($day, $month) = (localtime)[3,4];                         
$tar_file = sprintf ("hostout%2.2d%2.2d.tar", $month + 1, $day);

my $ftpcmd="get $tar_file\.Z";

printf (LOGFILE "\n The tar fle from the 4.0 system is %s", $tar_file);

# Determine working file name and delete any old working files
#
$ftpwrk = "$ENV{LESDIR}${par_pthsep}log${par_pthsep}rec_$$.tmp";
if (-e $ftpwrk)
{
    unlink ($ftpwrk) || die "\nFATAL ERROR - Could not delete old working file ($ftpwrk).\n Stopped";
}

# We need to copy the tar file from the 4.0 system into the 5.x system

open   (FTPCMD, "| ftp -n $remote_host > $ftpwrk") || die "\nFATAL ERROR - Could not start ftp.\nStopped";
printf (FTPCMD  "user $remote_user $remote_pwd \n");
printf (FTPCMD  "cd $hostout_40 \n");
printf (FTPCMD  "lcd $hostout_dir \n");
printf (FTPCMD  "$ftpcmd \n");
printf (FTPCMD  "bye\n");
close  (FTPCMD); # || die "\nFATAL ERROR - Could not run ftp.\n Stopped";

# At this point, we have the tar file from the 4.0 system. Xhdir to the working directory
print LOGFILE "\n Ftp from the 4.0 to 5.x completed \n";

if (!-e "$tar_file\.Z")
{
   print LOGFILE "\n Tar file does not exist in the 4.0 system. Exiting....";
   close LOGFILE;
   exit 1;
}

# I have to write the win32 processing part. later..
if ($^O =~ /win32/i)
{                       # NT
   printf (LOGFILE  "I am not sure how to handle this");
   exit;
}
else
{
   system ("uncompress $tar_file\.Z" );
   system ("tar -xf $tar_file"); 
}

#Verify that we have 2 REC files in the directory now.

opendir (DIRLST, ".") || die "Error Getting directory listing. \n Stopped";
@dirlst = grep ( /REC/, readdir (DIRLST));
close (DIRLST);

$rec_fil_count = $#dirlst + 1;

if ($rec_fil_count != 2)
{
   printf (LOGFILE  "I need 2 REC files to perform the reconciliation. Exiting...");
   exit 1;
} 

printf (LOGFILE  "Starting the reconciliation....");
# This sub program merges 2 files and sorts them by part number.
system ("$ENV{LESDIR}/scripts/u_merge");

# We now have all the host transaction files from both 4.0 and 5.0. Remove the existing tar 
# Also create a new tar file.

if (-e $tar_file)
{
    unlink ($tar_file) || die "\nFATAL ERROR - Could not delete old tar file ($tar_file).\n Stopped";
}

system ("tar -cf $tar_file *.txt; compress *.tar"); 

exit 0;
