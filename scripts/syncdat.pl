#!/usr/local/bin/perl
#
# Dustin Radtke
# This file should include all database archives to get 
# "master" data in sync between production and the 
# archive instance
#
$| = 1;                   # Flush output
# This will not log a trace anymore. - Raman P 
#/* set trace where activate = 1 and trcfil = 'sync_arch_data.trc' */
$cmd=<<EOF;                                
archive production data where arc_nam = 'SYNC-CARRIER'
/
archive production data where arc_nam = 'SYNC-CTNMST'
/
archive production data where arc_nam = 'SYNC-CUSTOMERS'
/
archive production data where arc_nam = 'SYNC-DSCMST'
/
archive production data where arc_nam = 'SYNC-FOOTPRINTS'
/
archive production data where arc_nam = 'SYNC-PARTS'
/
archive production data where arc_nam = 'SYNC-ALT-PARTS'
/
archive production data where arc_nam = 'SYNC-PERM-ADRMST'
/
#archive production data where arc_nam = 'SYNC-BOM'
/
EOF

$time = localtime;
print "$time \n $cmd";

open(MSQL,"| msql ") || die;
print MSQL $cmd;
close(MSQL);

exit(0);
