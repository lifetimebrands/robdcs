#!/usr/bin/ksh
#
# This script will run perl using the Oracle 10g client
#

ORACLE_HOME=/prod/oracle/OraHome_1
SHLIB_PATH=$ORACLE_HOME/lib32:$ORACLE_HOME/lib:$SHLIB_PATH

perl $1
