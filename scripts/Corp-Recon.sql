/* #REPORTNAME=IC-Lotted Inventory Report */
/* #HELPTEXT= This report displays the item, lot code, */
/* #HELPTEXT= available quantity, committed quantity, */
/* #HELPTEXT= pending quantity, status , location and area */
/* #HELPTEXT= for all inventory for all lots except NOLOT. */
/* #HELPTEXT= Requested by Inventory Control */



/* This will produce a reconciliation output for the Corporate Office  */
/* The standard Reconciliation process will still run on a nightly basis */

set heading off


column tranid format a10
column sitcod format a4
column prtnum format a30
column lotnum format a20
column orgcod format a20
column revlvl format a4
column lotnum format a20
column invsts format a1
column hstacc format a30
column untqty format 0999999999

set linesize 200

select usr_prtqty_view_sum.stoloc,
           usr_prtqty_view_sum.arecod,
           usr_prtqty_view_sum.prtnum,
           usr_prtqty_view_sum.lotnum,
           usr_prtqty_view_sum.invsts,
           sum(usr_prtqty_view_sum.untqty) untqty,
           sum(usr_prtqty_view_sum.comqty) comqty,
           sum(usr_prtqty_view_sum.avlqty) avlqty,
           sum(usr_prtqty_view_sum.pndqty) pndqty
    from usr_prtqty_view_sum
  group by stoloc,arecod,prtnum,lotnum,invsts
/
