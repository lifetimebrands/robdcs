/*This query list all complete shipment */
set pagesize 10000
set linesize 90
spool shipment_completed.rpt

select distinct s.ship_id, sl.ordnum, sl.ordlin, ol.prtnum, sl.shpqty
from 
     ord_line ol, 
     shipment_line sl,
     shipment s
where ol.ordnum = sl.ordnum
  and ol.ordlin = sl.ordlin
  and ol.ordsln = sl.ordsln
  and ol.client_id = sl.client_id
  and ol.client_id = '----'
  and sl.ship_id = s.ship_id
  and s.shpsts  = 'C'
order by s.ship_id,  
         sl.ordnum,
         sl.ordlin;

spool off
/
