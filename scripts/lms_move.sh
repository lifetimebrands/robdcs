#!/usr/bin/ksh
#
# This script will move LMS files to a tarred copy and move to the prc directory
# Existing files in the LMS directory will be deleted.
#
#TERM=vt100;export TERM
#. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/files/hostout/LMS
#

#Before I go further,  I need to move any existing tar files in the 5.x system.

datevar=`date +%m%d`

tar -cf lms$datevar.tar  *.calm
compress *.tar
rm *.calm
mv lms$datevar.tar.Z /${LESDIR}/files/hostout/prc
exit 0
