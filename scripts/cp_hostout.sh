## This Scripts is to copy all the TXT files that are in the Hostout directory to a New location CW 12-30-03


cd /opt/mchugh/prod/les/files/hostout/prodrecon 

cp -p  *.txt /opt/mchugh/prod/les/files/hostout/txtfiles

echo " Done copying all files ending with txt " > /opt/mchugh/prod/les/files/hostout/txtfiles/txt_files.out

ll -ltr *txt > /opt/mchugh/prod/les/files/hostout/txtfiles/daily_txt_files.out

rm *.txt

echo " Done removing all files ending in txt " > /opt/mchugh/prod/les/files/hostout/txtfiles/clean_txt.out

exit 0



