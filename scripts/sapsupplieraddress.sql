  insert into adrmst (
      adr_id,
      adrnam,
      client_id,
      host_ext_id,
      adrtyp,
      adrln1,
      adrln2,
      adrln3,
      adrcty,
      adrstc,
      Adrpsz,
      ctry_name,
      rsaflg,
      phnnum,
      temp_flg,
      locale_id)
     select
     usr_supplier.adr_id,
     adrnam,
     CLIENT_ID                                         ,
     usr_supplier.supnum,
      ADRTYP                                            ,
      ADRLN1                                            ,
      ADRLN2,
      ADRLN3                                            ,
      ADRCTY                                            ,
      ADRSTC                                            ,
      ADRPSZ                                            ,
      CTRY_NAME                                         ,
      RSAFLG                                            ,
      PHNNUM,
      TEMP_FLG                                          ,
      LOCALE_ID                                      
     from adrmst, usr_supplier
     where
     adrmst.host_ext_id  =usr_supplier.oldsupnum and usr_supplier.adr_id not in (select adrmst.adr_id from adrmst
   where usr_supplier.adr_id = adrmst.adr_id)
;
commit;
