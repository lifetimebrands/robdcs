Before
Filesystem          kbytes    used   avail %used Mounted on
/dev/vg00/lvol3     143360   41668   95514   30% /
/dev/vg00/lvol1      83733   42465   32894   56% /stand
/dev/vg00/lvol8    1536000  851327  642173   57% /var
/dev/vg00/lvol7    1282048  585182  653346   47% /usr
/dev/vg00/lvol10    512000   71792  412759   15% /usr/local
/dev/vg00/lvol4     512000   53538  430208   11% /tmp
/dev/vg00/lvol6    1048576  538829  477905   53% /opt
/dev/vg00/lvol5     102400    1277   94863    1% /home
35328000 3632272 31448368   10% /mnt/db09

After
Filesystem          kbytes    used   avail %used Mounted on
/dev/vg00/lvol3     143360   43206   94072   31% /
/dev/vg00/lvol1      83733   42465   32894   56% /stand
/dev/vg00/lvol8    1536000  850955  642519   57% /var
/dev/vg00/lvol7    1282048  585182  653346   47% /usr
/dev/vg00/lvol10    512000   71792  412759   15% /usr/local
/dev/vg00/lvol4     512000   53538  430208   11% /tmp
/dev/vg00/lvol6    1048576  538829  477905   53% /opt
/dev/vg00/lvol5     102400    1277   94863    1% /home
 35328000 11352432 23788496   32% /mnt/db09
