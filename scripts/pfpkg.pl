#!/usr/local/bin/perl
################################################################################
#
# $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/scripts/pfpkg.pl,v $
# $Revision: 1.2 $
# $Author: devsh $
#
# Description: Project fix packaging script.
#              This script packages a project fix and publishes it to the web 
#              server.
#
# $McHugh_Copyright-Start$
#
#   Copyright (c) 2000
#   McHugh Software International
#   Waukesha, Wisconsin
#
# This software is furnished under a corporate license for use on a
# single computer system and can be copied (with inclusion of the
# above copyright) only for use on such a system.
#
# The information in this document is subject to change without notice
# and should not be construed as a commitment by McHugh Software
# International.
#
# McHugh Software International assumes no responsibility for the use of
# the software described in this document on equipment which has not been
# supplied or approved by McHugh Software International.
#
# $McHugh_Copyright-End$
#
################################################################################

require 5.005;

use strict;
use Cwd;
use English;
use File::Basename;
use File::Find;
use File::stat;
use FindBin;
use IO::Handle;
use POSIX;
use Getopt::Std;

# ------------------------------------------------------------------------------
# Global Variables
# ------------------------------------------------------------------------------

my ($PrjFix, $MyDir, $Platform);

# ------------------------------------------------------------------------------
#
# FUNCTION: CheckPrereqs
#
# PURPOSE:  Check project fix packaging prerequisites.
#
# ------------------------------------------------------------------------------

sub CheckPrereqs
{
    # Check for the project fix utility.
    if (! -f "prjfix.pl")
    {
        print STDERR "ERROR: The project fix utility does not exist.\n";
        return 1;
    }

    # Check for the project fix script.
    if (! -f "$PrjFix")
    {
        print STDERR "ERROR: The project fix script does not exist.\n";
        return 1;
    }

    # Check for the release notes file.
    if (! -f "README.txt")
    {
        print STDERR "ERROR: The release notes file does not exist.\n";
        return 1;
    }
}


# ------------------------------------------------------------------------------
#
# FUNCTION: SetPlatform
#
# PURPOSE:  Set the platform we're running on.
#
# ------------------------------------------------------------------------------

sub SetPlatform
{
    my ($os, $kernel, $hostname, $release, $version, $hardware);

    print "Determining the target platform... \n";

    # Determine the Win32/UNIX platform.
    if ($OSNAME =~ /win32/i)
    {
	$Platform = "win32";
    }
    else
    {
        eval '($kernel, $hostname, $release, $version, $hardware) = uname( )';
    }

    # Determine the UNIX platform.
    if ($kernel =~ /aix/i)
    {
        $os = "aix";
        $Platform = "$os$version.$release";
    }
    elsif ($kernel =~ /freebsd/i)
    {
        $os  = "bsd";
	$release =~ /(.*)[-(].*/;
        $Platform = "$os$1";
    }
    elsif ($kernel =~ /hp-ux/i)
    {
        $os  = "hpux";
	$release =~ /[^.]*.[0B]*(.*)/;
        $Platform = "$os$1";
    }
    elsif ($kernel =~ /linux/i)
    {
        $os  = "linux";
	$release =~ /([0-9]*\.[0-9]*).*/;
        $Platform = "$os$1";
    }
    elsif ($kernel =~ /sco/i)
    {
        $os  = "sco";
        $Platform = "$os$release";
    }
    elsif ($kernel =~ /sunos/i)
    {
        $os  = "solaris";
        $Platform = "$os" . "2.5.1";
    }

    return;
}


# ------------------------------------------------------------------------------
#
# FUNCTION: CreateFileList
#
# PURPOSE:  Create the project fix file list for Win32 platforms.
#
# ------------------------------------------------------------------------------

sub CreateFileList
{
    # Add this file to the file list.
    sub AddFile
    {
	# We don't want to package everything into the project fix.
        if (-f basename($File::Find::name)                            and 
	       basename($File::Find::name) ne "$PrjFix-$Platform.exe" and
	       basename($File::Find::name) ne "$PrjFix-$Platform.tar" and
	       basename($File::Find::name) ne "$PrjFix-$Platform.zip" and
	       basename($File::Find::name) ne "$PrjFix.input"         and
	       basename($File::Find::name) ne "$PrjFix.filelist")
	{
	    print OUTFILE "$File::Find::name\n";
	}
    }

    # Change to the directory we're being ran from.
    chdir("$MyDir");

    # Open the file list.
    open(OUTFILE, ">$PrjFix.filelist");

    # Set autoflushing on the file list.
    OUTFILE->autoflush(1);

    # Find every file under the given directory.
    find(\&AddFile, ".");

    # Close the file list.
    close(FILELIST);

    return;
}


# ------------------------------------------------------------------------------
#
# FUNCTION: CreateMessageFile
#
# PURPOSE:  Create the project fix message file for Win32 platforms.
#
# ------------------------------------------------------------------------------

sub CreateMessageFile
{
    # Change to the directory we're being ran from.
    chdir("$MyDir");

    # Open the message file.
    open(OUTFILE, ">$PrjFix.diz");

    # Write to the message file.
    print OUTFILE "This is the self-extracting zip file for project fix $PrjFix.\n";
    print OUTFILE "\n";
    print OUTFILE "It's very important that you follow the project fix ";
    print OUTFILE "installation instructions exactly in order to ";
    print OUTFILE "successfully install this project fix.  The installation ";
    print OUTFILE "instructions can be found in the release notes within ";
    print OUTFILE "this zip file.\n";

    # Close the message file.
    close(OUTFILE);

    return;
}


# ------------------------------------------------------------------------------
#
# FUNCTION: PackagePrjFix
#
# PURPOSE:  Package the project fix in the current directory.
#
# ------------------------------------------------------------------------------

sub PackagePrjFix
{
    my $status = 0; 

    print "Creating the project fix distribution file... \n";

    if ($OSNAME =~ /win32/i)
    {
        $status = PackagePrjFix_win32( );
    }
    else
    {
        $status = PackagePrjFix_unix( );
    }

    return $status;
}


# ------------------------------------------------------------------------------
#
# FUNCTION: PackagePrjFix_win32
#
# PURPOSE:  Package the project fix in the current directory.
#
# ------------------------------------------------------------------------------

sub PackagePrjFix_win32
{
    my $status = 0; 

    # Create the file list.
    CreateFileList( );

    # Create the message file.
    CreateMessageFile( );

    # Create the zip file.
    system("wzzip -P $PrjFix-$Platform.zip \@$PrjFix.filelist");

    # Open the self-extractor input file.
    open(OUTFILE, ">$PrjFix.input");

    # Write self-extractor arguments to the input file.
    print OUTFILE "-y\n";
    print OUTFILE "-3\n";
    print OUTFILE "-win32\n";
    print OUTFILE "-d \"YOUR LESDIR HERE\\temp\\$PrjFix\"\n";
    print OUTFILE "-st \"Prj Fix $PrjFix\"\n";
    print OUTFILE "-m $PrjFix.diz\n";

    # Close the input file.
    close(OUTFILE);

    # Create the self-extracting zip file.
    system("winzipse $PrjFix-$Platform.zip \@$PrjFix.input");

    return $status;
}


# ------------------------------------------------------------------------------
#
# FUNCTION: PackagePrjFix_unix
#
# PURPOSE:  Package the project fix in the current directory.
#
# ------------------------------------------------------------------------------

sub PackagePrjFix_unix
{
    my $status = 0;

    # Create the file list.
    CreateFileList( );

    # Create the tar file.
    system("cat $PrjFix.filelist | xargs tar cvf $PrjFix-$Platform.tar >/dev/null 2>&1");
    if ($? != 0)
    {
	print STDERR "ERROR: Could not create the tar file.\n";
	print STDERR "       $!\n";
	$status = 1;
    }

    return $status;
}

# ------------------------------------------------------------------------------
#
# Start of Execution.
#
# ------------------------------------------------------------------------------

# Define local variables.
my ($status, $usage);

# Handle command line arguments.
$PrjFix = $ARGV[0];
shift @ARGV;
$usage = 0;
getopts("h") || $usage++;

if (! $PrjFix || $PrjFix eq "-h" || $usage || $Getopt::Std::opt_h)
{
  printf STDERR "Usage: perl pfpkg.pl <prjfix #> [-h]\n";
  printf STDERR " -h          - Help (Print this message) \n";
  exit 1;
}

# Get the current directory.
$MyDir = cwd;

# Check project fix packaging prerequisites.
$status = CheckPrereqs( );
if ($status != 0)
{
    exit 1;
}

# Display a message for the user.
print "\n";
print "Packaging project fix $PrjFix...\n";

# Set the platform we're on.
SetPlatform( );

# Package the project fix files.
$status = PackagePrjFix( );
if ($status != 0)
{
    print STDERR "ERROR: Could not package the project fix.\n";
    exit 1;
}

exit 0;
