#!/usr/local/bin/perl 
#######################################################################

# table_diffs.pl --  Script to check table differences. 
# This creates a snapshot for a table and compares the snapshot to the
# Existing snapshot. 
# usage : -- table_diffs.pl -c|-d -t table_name 
#######################################################################
use IO::Handle;

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

# Set the unload directory in the system
my $unload_dir=$ENV{LESDIR}.$par_pthsep."db".$par_pthsep."data".$par_pthsep."unload";
my $log_file = $ENV{LESDIR}.$par_pthsep."log".$par_pthsep."table_diffs.log";
my $cmdtxt = "";
my $old_load_dir="";
my $std_csv_file = "";
my $new_csv_file = "";

if (-e $log_file )
{
   unlink ($log_file) || die "Unable to remove existing log file. exiting ... ";
}
$status = open(LOGFILE, "> $log_file") || die "Unable to open log file. exiting...";

LOGFILE->autoflush(1);

printf (LOGFILE "The unload directory is $unload_dir");

###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
my $arg_opts = @ARGV[0];
my $arg_table = @ARGV[1];
#

# Verify required arguments are present
#
if (!($arg_opts eq "-c") && !($arg_opts eq "-d"))
{
    printf ("Options are not defined\n");
    print_help ();
    exit (1);
}
#
if (!$arg_table)
{
    printf ("Table name is not defined\n");
    print_help ();
    exit (1);
}
printf (LOGFILE "\n Argument List\n");
printf (LOGFILE "\n Options ($arg_opts)\n");
printf (LOGFILE "\n Tablename ($arg_table)\n");

#Check to see if the unload directory is defined 
if (!$ENV{"UNLOAD_DIR"})
{
   printf (LOGFILE "\n The environment variable UNLOAD_DIR is not defined. Exiting...");
   printf ("\n The environment variable UNLOAD_DIR is not defined. Exiting...\n");
   exit (1); 
}
# Check to see if we are creating a snapshot or we are generating a diff
#
if ($arg_opts eq "-c" )
{
    chdir ($unload_dir) || die "Failed to chdir to unload directory ";
# if a csv file already exists, rename it.
    printf (LOGFILE "\n $arg_table$par_pthsep$arg_table.csv" );
    if (-e $ENV{UNLOAD_DIR}.$par_pthsep.$arg_table.$par_pthsep.$arg_table.".csv")
    { 
       printf (LOGFILE "\n $arg_table.csv file exists...");
       printf (LOGFILE "\n Renaming the current csv file ...");
       my ($sec, $min, $hour, $mday, $month, $year) = localtime(time);
       rename ($ENV{UNLOAD_DIR}.$par_pthsep.$arg_table.$par_pthsep.$arg_table.".csv", $ENV{UNLOAD_DIR}.$par_pthsep.$arg_table.$par_pthsep.$arg_table."_".$month.$mday.$hour.$min.".csv");
    }
    printf (LOGFILE "\nCreating a standard snapshot");
    $cmdtxt = "mload -H -D $arg_table -c $arg_table.ctl -d $arg_table.csv -t* -o $ENV{LESDIR}/log/mload.log";
    printf (LOGFILE "Load Command\n  $cmdtxt\n");
    system ($cmdtxt);

}

if ($arg_opts eq "-d" )
{
# Generate a snapshot and check differences. save previous env variable
#
    $old_load_dir = $ENV{"UNLOAD_DIR"};  
    printf (LOGFILE "\nThe old environment variable is $ENV{UNLOAD_DIR}");
    $ENV{"UNLOAD_DIR"} = $ENV{"HOME"};
    printf (LOGFILE "\nThe new environment variable is $ENV{UNLOAD_DIR}");

    chdir ($unload_dir) || die "Failed to chdir to unload directory ";
    printf (LOGFILE "\nCreating a new snapshot to compare with standard");
    my $cmdtxt = "mload -H -D $arg_table -c $arg_table.ctl -d $arg_table.csv";
    printf (LOGFILE "Load Command\n $cmdtxt\n");
    system ($cmdtxt);

    # Put back the old environment variable
    #
    $ENV{"UNLOAD_DIR"} = $old_load_dir;
    printf (LOGFILE "\nThe old environment variable is $ENV{UNLOAD_DIR}");
    ###############################################################################
    #
    # Verify that we have 2 different csv files to compare against each other  

    $old_csv_file = $ENV{UNLOAD_DIR}.$par_pthsep.$arg_table.$par_pthsep.$arg_table.".csv";
    $new_csv_file = $ENV{HOME}.$par_pthsep.$arg_table.$par_pthsep.$arg_table.".csv";

    if ((!-e $old_csv_file) || (!-e $new_csv_file))
    {
        printf (LOGFILE "\n One of the csv files is missing. Unable to compare. Exiting");
        printf (LOGFILE "\n Expecting File 1 - $old_csv_file");
        printf (LOGFILE "\n Expecting File 2 - $new_csv_file");
        close LOGFILE;
        exit 0;
    }
    printf  (LOGFILE "\n About to start comparing.....");
    printf  (LOGFILE "\n \n");
    close LOGFILE;
    ###############################################################################
    #
    # Run the compare to check the differences 

    system ("diff -y $old_csv_file $new_csv_file >> $log_file" );
}

###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Lists table differences by comparing with a standard snapshot \n");
    printf ("\n");
    printf ("Usage:  table_diffs <-c|-d> <table>\n");
    printf ("\n");
    printf ("    <-c>) Create a snapshot to be used as the standard \n");
    printf ("\n");
    printf ("    <-d>) Lists the differences by comparing with standard snapshot\n");
    printf ("\n");
    printf ("   <table>) The table name like prtmst, aremst, vehacc \n");
    printf ("\n");
}

#
###############################################################################
#
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################
