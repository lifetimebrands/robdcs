#!/usr/local/bin/perl  
#######################################################################

# COMPARE_NF.PL - Script to compare files. Very specific. This is a 
# small enhancement to the original compare.pl file. Just lists the
# output in a different format
# Usage :- compare.pl  -a <file1> -b <file2> -h

#######################################################################

# $MF_Copyright-Start$

#   DISTRIBUTION MANAGER
#   Copyright (c) 1999
#   McHugh Software International
#   Waukesha, Wisconsin

# This software is furnished under a corporate license for use on a
# single computer system and can be copied (with inclusion of the
# above copyright) only for use on such a system.

# The information in this document is subject to change without notice
# and should not be construed as a commitment by McHugh Software
# International.

# McHugh Software International assumes no responsibility for the use of
# the software described in this document on equipment which has not been
# supplied or approved by McHugh Software International.

# $MF_Copyright-End$

# $Id$

# Modification History

# $Log$

#######################################################################
#

#Include required sub components
require "getopts.pl";

# Initialize internal parameters
# This checks the Operating System

if ($^O =~ /win32/i)
{                       # NT
    $par_pthsep = "\\";
}
else
{                       # Unix
    $par_pthsep = "\/";
}

# Initialize local variable list

my $file1 = "";
my $file2 = "";

my $f1_index = 0;
my $f2_index = 0;

my $f1_count = 0; 
my $f2_count = 0; 

my $status = "";
my $count = 0;

###############################################################################
# Input argument checking/processing
#
#
# Get command line arguments
#
$status = &Getopts ('a:b:');
#
# If help mode enabled, then print help screen & exit
#

# Load arguments into local variables
#
$file1 = $opt_a if ($opt_a);
$file2 = $opt_b if ($opt_b);

if (!$file1 or !$file2)
{
   &print_help ();
   exit (0);
}

open (FILE1, $file1) || die "Unable to open file1. Check if file exists" ;
@f1_list = <FILE1>;
close (FILE1);

open (FILE2, $file2) || die "Unable to open file2. Check if file exists" ;
@f2_list = <FILE2>;
close (FILE2);


# Get the count of the number of records in each file
$f1_count = @f1_list;
$f2_count = @f2_list;

if ($f1_count == 0 or $f2_count == 0)
{
   die "One of the files has 0 records. Stopped.\n" ;
}

# Loop through the records
while ($f1_index < $f1_count or $f2_index < $f2_count)
{
   if ($f1_index < $f1_count)
   {
      $f1_line = @f1_list[$f1_index];
   }
   else
   {
      $f1_line = "";
   }

   if ($f2_index < $f2_count)
   {
      $f2_line = @f2_list[$f2_index];
   }
   else
   {
      $f2_line = "";
   }

   @f1_item = split ( / +/, $f1_line);
   @f2_item = split ( / +/, $f2_line);

   # If the item number matches, then we can get the difference
   if ( $f1_item[0] eq $f2_item[0] )
   {
      if ($f1_item[1] != $f2_item[1] )
      {
         printf ("\n%-20.20s %10d", $f1_item[0], $f2_item[1] - $f1_item[1] );
      }  
      $f1_index++;
      $f2_index++;
   }
   elsif (( $f1_item[0] gt $f2_item[0] ) or ($f1_index >= $f1_count))
   # This means that the part is existing in File2 alone. 
   {
      printf ("\n%-20.20s %10d", $f2_item[0], $f2_item[1] );
      $f2_index++;
   }
   elsif (( $f1_item[0] lt $f2_item[0] )or ($f2_index >= $f2_count))
   # This means that the part is existing in File1 alone. 
   {
      if ($f1_item[1] > 0)
      {
         printf ("\n%-20.20s %10d", $f1_item[0], 0 - $f1_item[1] );
      }
      $f1_index++;
   }
}

# Define print help routine
#
###############################################################################
#
sub print_help()
{
    printf ("\n");
    printf ("Compares 2 files. And generates a diff file ");
    printf ("\n");
    printf ("Usage:  compare -a <file1> -b <file2> [-h] \n");
    printf ("\n");
}

