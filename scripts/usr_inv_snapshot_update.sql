/**********************************************************************************************************/
/* This script will populate the phys_inv_snapshot table.                                                 */
/* This information is used in reporting.                                                                 */
/* The table should be truncated first to  get rid of last years data.                                    */
/**********************************************************************************************************/

truncate table phys_inv_snapshot;

/* Populate table with current data */

insert into phys_inv_snapshot(prtnum,arecod,adddte,untqty,stoloc,lotnum,invsts,untpak,untcas,
gentyp,prt_client_id,untcst) 
(
select ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(a.arecod)) arecod,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.invsts, d.untpak, d.untcas,  
       'INITIAL' gentyp, '----' prt_client_id, e.untcst
from aremst a,locmst m, invlod l, invsub s, invdtl d, prtmst e
 where a.fwiflg = 1 and
       a.arecod != 'SHIP' and
       a.arecod  = m.arecod and
       m.stoloc  = l.stoloc and
       l.lodnum  = s.lodnum and
       s.subnum  = d.subnum
       and d.prtnum = e.prtnum
       and e.prt_client_id ='----'
 group
    by d.prtnum,
       a.arecod,
       m.stoloc,
       d.lotnum,
       d.invsts,
       d.untpak,
       d.untcas,
       e.untcst)
/
commit;

spool /opt/mchugh/prod/les/oraclereports/phys_inv_snapshot.out
set linesize 300
select * from phys_inv_snapshot;

spool off;
