#!/usr/local/bin/perl

open (TFILE, "temp.file");
@lines = <TFILE>;
close (TFILE);

open (ITMLST, "altprt.lst");
@items = <ITMLST>;
close (ITMLST);

$count = 0;
foreach $line (@lines) {

  @item_qty = split ( / +/, $line);
  $pat_found = 0;
  foreach $item (@items) {
     @item = split (/ +/, $item);
      if ( $item_qty[0] eq $item [0] ) {
       if ($count == 0) {
         printf ("%-20s ", $item[1]);
       }
       else
       { 
         printf ("\n%-20s ", $item[1]);
       }
       $count++;
       $pat_found = 1;
     }
  }
  if ( $pat_found == 0 ){
    if ($count == 0) {
      printf ("%-20s ", $item_qty[0]);
    }
    else {
      printf ("\n%-20s ", $item_qty[0]);
    }
    $count++;
  } 
   printf ("%10d", $item_qty[1]);   
}
printf ("\n");
