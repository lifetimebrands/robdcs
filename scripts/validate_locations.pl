!/usr/local/bin/perl
# Don Schiewer 10/10/00
# Call validate location function on individual locations and apply fix if desired
# Fixing one location at a time reduces the chance of stale updates

require "getopts.pl";

$| = 1;                   # Flush output
$0 =~ s:^.*/::;           # get just the basename of $0
$usage = 0;
&Getopts('hfa:b:e:');
if ($usage || $opt_h) {
  printf(STDERR "usage: $0 [-h (help)] [-f (do database fix) [-a Area Code] [-b Begin Loc] [-e End loc] \n");
  exit $opt_h? 0 : 2
}
		    
$LOGFILE = "$ENV{LESDIR}/log/validate_locations";

$EXCLUDE_AREAS = "'ADJS','SADJ','EXPR','RDTS','SHIP'";

$WHERE = "arecod = '$opt_a'" if ($opt_a); # Build where clause based on inputs
if ($opt_b) {
  $WHERE .= " and " if ($WHERE);
  $WHERE .= "stoloc >= '$opt_b'";
}
if ($opt_e) {
  $WHERE .= " and " if ($WHERE);
  $WHERE .= "stoloc <= '$opt_e'";
}
$WHERE = ' and '.$WHERE if ($WHERE);

$cmd=<<EOF;                                # Select each location to validate
[select '>'||rtrim(stoloc)||'<' from locmst where arecod not in ($EXCLUDE_AREAS) $WHERE order by stoloc] 
/
EOF
$time = localtime;
print "$time    $cmd";
open(SQL,"| msql > $LOGFILE.$$.1 2>&1") || die;
print SQL $cmd; 
close(SQL);
open(DATA,"$LOGFILE.$$.1") || die;
while (<DATA>) {
   chomp;
   ($stoloc) = /^>(.+)</;
   push @locs, $stoloc if ($stoloc); 
}
close(DATA);
unlink "$LOGFILE.$$.1";

foreach $loc (sort @locs) {        # For each location, validate and re-validate if fix applied
  print  "Validate $loc\n";
  $out = &val_loc($loc);
  $did_fix = '';
  if ($out =~ /\[(.*)\]/) {
      print "> $1 \n";  
      $did_fix = 1 if ($opt_f);
  }
  if ($did_fix) {
    print  "Re-Validate $loc\n";
    $out = &val_loc($loc);
    if ($out =~ /\[(.*)\]/) {
      print "> $1 \n";  
    }
  }
}
		    
sub val_loc {
my ($loc) = @_;
my ($out,$stoloc,$error,$fix,@fixes);

$cmd=<<EOF;    # Get location validation
validate location where begloc = '$loc' and endloc = '$loc'
/
EOF
open(SQL,"| msql > $LOGFILE.$$.1") || die;
print SQL $cmd; 
close(SQL);
open(DATA,"$LOGFILE.$$.1") || die;
while (<DATA>) {
   $out .= $_;
   chomp;
   ($stoloc,$error,$fix) = /(.+?) (.*)(\[.*\])/;
   push @fixes, $fix."\n\/\n" if ($fix); 
}
close(DATA);
unlink "$LOGFILE.$$.1";

if ($opt_f and @fixes) {   # apply fix if desired
  open(MSQL,"| msql > $LOGFILE.$$.2") || die;
  print MSQL "@fixes";
  close(MSQL);
  unlink "$LOGFILE.$$.2";
}
return $out;
}

exit(0);
