#!/usr/bin/ksh
#
# This script will run the Forecasting Report at 6AM using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports


sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/monthly_adjs.out
@usr_monthly_adj_update.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/ScrappedDetail.out
@usr_workorder_scp_dtl.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/Sl_Table_Cleanse.out
@usr_sl_table_cleanse.sql
spool off
exit
//
exit 0


