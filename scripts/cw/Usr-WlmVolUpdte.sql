

insert into usr_wmvolume(prtnum,files,units,cartons,lodnums,dollars,qube,dte,type)
select rtrim(prtnum) prtnum,
  trknum files, nvl(sum(rcvqty),0) units, sum(x.rcvqty/x.untcas) cartons,sum(x.rcvqty/x.untpal) lodnums,
  nvl(sum(rcvqty * untcst),0) dollars,
   nvl(trunc(sum((cube/ 1728) * (x.rcvqty/x.untcas)),2),0) qube,
  clsdte dte, 'RCV' type
         from
           (select distinct
           rcvtrk.trknum,rcvlin.prtnum,rcvlin.rcvqty,prtmst.untcas untcas,prtmst.untpal, prtmst.untcst untcst,
            trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum,caslen*caswid*cashgt cube,rcvlin.supnum,rcvlin.rcvkey
                                   from rcvtrk,rcvinv,rcvlin,trlr,ftpmst, prtmst, invdtl,usr_wmitems
                                   where trlr.trlr_id = rcvtrk.trlr_id
                                   and rcvtrk.trknum = rcvlin.trknum
                                   and rcvlin.trknum = rcvinv.trknum
                                   and rcvlin.client_id = rcvinv.client_id
                                   and rcvlin.supnum = rcvinv.supnum
                                   and rcvlin.invnum = rcvinv.invnum
                                   and rcvlin.prtnum = prtmst.prtnum
                                   and prtmst.prtnum = ftpmst.ftpcod
                                   and usr_wmitems.prtnum = prtmst.prtnum
                                   and rcvlin.rcvkey = invdtl.rcvkey(+)
                                   and rcvtrk.expdte is not null
                                   and trlr.trlr_stat = 'C'
                                   and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x
                    group by prtnum,clsdte,trknum;

insert into usr_wmvolume(prtnum,files,units,cartons,lodnums,dollars,qube,dte,type)
select prtnum,ship_id files,  sum(x.shpqty) units,sum(x.shpqty/x.untcas) cartons,sum(x.shpqty/x.untpal) lodnums,
sum(x.shpqty *untcst) dollars,
trunc(sum((x.shpqty/x.untcas)*(x.cube/1728)),2) qube, dispatch_dte, 'SHP' type
                     from
                     (select distinct shipment.ship_id, i.prtnum, ord.ordnum,prtmst.untcst,prtmst.untpal,
                                          ord_line.ordlin,shipment_line.shpqty shpqty,
                          ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl i,ftpmst, prtmst, shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr,usr_wmitems
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND prtmst.prtnum = usr_wmitems.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND trunc(dispatch_dte) = trunc(sysdate)-1) x
       group by prtnum, ship_id,dispatch_dte;

insert into usr_wmvolume(prtnum,stoloc, units,cartons, lodnums, dollars,qube,dte,type)
select distinct prtnum, stoloc, sum(avlqty) units,sum(x.avlqty/x.untcas) cartons,sum(lodnum) lodnums,
         sum(untcst * avlqty) dollars,
          trunc(sum((x.avlqty/x.untcas)*(x.cube/1728)),2) qube, trunc(sysdate) dte, 'STK' type
             from
             (select distinct invsum.stoloc,invsum.arecod,
                     invdtl.prtnum,
                     invdtl.lotnum,
                     invdtl.invsts,
                    invsum.untqty,
                    invsum.comqty,
                  decode (locmst.pckflg, 1, nvl (((invsum.untqty + invsum.pndqty) - invsum.comqty), 0), 0) avlqty,
            ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt cube, prtmst.untcas,prtmst.untcst, count(distinct invlod.lodnum) lodnum
           from usr_wmitems, prtmst, ftpmst, locmst, invsum,invlod,invsub,invdtl
           where usr_wmitems.prtnum = prtmst.prtnum
           and prtmst.prtnum = ftpmst.ftpcod
           and prtmst.prtnum = invsum.prtnum
           and prtmst.prt_client_id = invsum.prt_client_id
           and invsum.stoloc = invlod.stoloc
           and invsum.stoloc = locmst.stoloc
           and invsum.arecod = locmst.arecod
           and invsum.prtnum = invdtl.prtnum
           and invsum.prt_client_id = invdtl.prt_client_id
           and invdtl.subnum = invsub.subnum
           and invsub.lodnum = invlod.lodnum
           and invlod.stoloc = locmst.stoloc
           and invdtl.prt_client_id = '----'
           group by invsum.stoloc,invsum.arecod,
           invdtl.prtnum,
           invdtl.lotnum,
           invdtl.invsts,
           invsum.untqty,invsum.pndqty,caslen,caswid,cashgt,
           invsum.comqty,locmst.pckflg,prtmst.untcas,prtmst.untcst) x
        group by prtnum,stoloc;

commit;

-- delete yesterdays storage records, only if todays records loaded successfully

delete from usr_wmvolume a
where a.type = 'STK' and trunc(a.dte) < trunc(sysdate)
and exists(select 'x' from usr_wmvolume b where b.type = a.type
and trunc(b.dte) = trunc(sysdate));

commit;
