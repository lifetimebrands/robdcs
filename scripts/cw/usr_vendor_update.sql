/* Author:  Al Driver */

/* This script is used to identify and update */
/* the usr_vendor_receipts table. Used in the  */
/* Vendor Scorecard report. This freezes the untcst */
/* of receipts and will correspond with the KPI report  */
/* (Dollars Received) category */

insert into usr_vendor_receipts
    select rcvtrk.trknum,rcvlin.prtnum,rcvinv.client_id,rcvlin.supnum,untcst,idnqty, trunc(rcvtrk.clsdte) clsdte
                           from rcvtrk,rcvinv,prtmst,rcvlin,trlr
                           where trlr.trlr_id = rcvtrk.trlr_id
                           and rcvtrk.trknum = rcvlin.trknum
                           and rcvlin.trknum = rcvinv.trknum
                           and rcvlin.client_id = rcvinv.client_id
                           and rcvlin.supnum = rcvinv.supnum
                           and rcvlin.invnum = rcvinv.invnum
                           and rcvlin.prtnum = prtmst.prtnum
                           and rcvtrk.expdte is not null              /* This line eliminates anything from Dayton B3's */
                           and rcvlin.idnqty <> 0                    /* This eliminates duplicate records */
                           and trlr.trlr_stat = 'C'
                           and rcvinv.invtyp = 'P'
                           and rcvinv.client_id = '----'
            and trunc(rcvtrk.clsdte) = trunc(sysdate) -1; 

commit;
