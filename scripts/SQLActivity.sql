/*********************************************************************/
/* This script will run three times daily and spool the information  */
/* to an outfile entitled Sql Activity.  This information will then  */
/* be stored in the scripts directory.                                */
/* This script will keep track of sql statements/updates              */
/**********************************************************************/     


alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';

set pagesize 1000
set linesize 120


select /*+RULE*/ p.spid orapid, s.process syspid, t.sql_text, 
substr(s.username,1,8) username, sysdate date_time from v$sqltext t,
v$session s, v$process p where s.sql_address = t.address and
s.paddr = p.addr  order by s.logon_time, p.spid,t.piece;
