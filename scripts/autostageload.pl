#!/usr/local/bin/perl
# Raman Parthasarathy 05/29/02 
# This is run to stage and load any small parcel shipments that are stranded 

my $cmd = "";
$LOGFILE = "$ENV{LESDIR}/log/autostage.log";

$cmd=<<EOF;                                #Command to execute 
set environment where variable = 'DEVCOD' and value = 'RPARTHAS' | auto stage and load shipment
/
EOF

open(SQL,"| msql > $LOGFILE.$$.1 2>&1") || die ("Failed to open log file");
print SQL $cmd;
close(SQL);

exit(0);
