/* This script follows the updating steps for the costs  */
/* updates on parts in the prtmst table, performed every Friday.  */
/* This can be run after the new parts.csv file is */
/* placed in /opt/mchugh/prod/les/db/data/load/parts directory */
/* the usr_parts table has been truncated and updated. */
/* When doing the dload parts parts.csv, if any major errors */ 
/* occur that cannot be fixed, report to Denise.  If one part errors */
/* investigate, because it could cause other records to fail. */


/* Do the following queries */

/* Get a count of everything in usr_parts */

update foncst set untcst=(select untcst
	from usr_parts
	where plant=foncst.plant
	and prtnum=foncst.prtnum
	and plant='SYML')
where exists (select 'x'
	from usr_parts
	where plant=foncst.plant
	and prtnum=foncst.prtnum
        and plant='SYML');

commit;

insert into foncst (select prtnum, untcst, plant from usr_parts where plant='SYML' and not exists (select' x' from foncst where prtnum=usr_parts.prtnum and plant=usr_parts.plant));

delete from usr_parts where plant not in ('1000','1600');

commit;

delete from usr_parts where plant='1000' and prtnum in (select prtnum from prtmst where vc_profcnt='40070');

commit;

delete from usr_parts where plant='1000' and prtnum in (select prtnum from (select prtnum, count(*) from usr_parts group by prtnum having count(*)>1));

commit;

select count(*) from usr_parts;

/* Get a count of everything in usr_parts that also */
/* exists in prtmst (these are the only parts that should be updated */

select count(*) from usr_parts a
    where exists(select 'x' from prtmst b
    where a.prtnum = b.prtnum);

/* Spool out the details of the parts which exist in */
/* prtmst and usr_parts. */

spool /opt/mchugh/prod/les/oraclereports/PartsUpdated.txt
set pagesize 1000
set linesize 200

select a.prtnum part,a.untcst oldcost, b.untcst newcost, (b.untcst-a.untcst) change,
case when a.untcst>0 and b.untcst=0 then -100
when a.untcst =0 and b.untcst>0 then null
when a.untcst =0 and b.untcst=0 then 0
when a.untcst> 0 and b.untcst>0 then round((((b.untcst-a.untcst)/a.untcst)*100),1)
else null
end perchange, decode(d.fwiflg,1,sum(c.untqty),0) fourwallqty
from prtmst a, usr_parts b, invsum c, aremst d
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and a.untcst!=b.untcst
and b.untcst>0
and a.prtnum=c.prtnum(+)
and c.arecod=d.arecod(+)
group by a.prtnum, a.untcst, b.untcst, d.fwiflg;

spool off

/* Spool out the details of the parts which exist in */
/* prtmst and have a cost of 0.00 in usr_parts. */

spool /opt/mchugh/prod/les/oraclereports/zerocost.txt
set pagesize 1000

select b.prtnum part, b.untcst newcost, a.untcst curcost, decode(d.fwiflg,1,sum(c.untqty),0) fourwallqty
from usr_parts b, prtmst a, invsum c, aremst d
where b.untcst=0
and a.prtnum=b.prtnum
and a.untcst!=b.untcst
and a.prt_client_id='----'
and a.prtnum=c.prtnum(+)
and c.arecod=d.arecod(+)
group by b.prtnum, b.untcst, a.untcst, d.fwiflg;

spool off

/* update related parts in usr_parts and prtmst */

update prtmst a
    set a.untcst=(select b.untcst from usr_parts b
    where a.prtnum = b.prtnum
    and b.untcst>0)
   where exists(select 'x' from usr_parts c
    where a.prtnum = c.prtnum
    and c.untcst>0);

commit;

/
