#!/usr/local/bin/perl
# Raman Parthasarathy 04/24/02 
# Call cleanup_waves to fix any hanging waves that must be Complete but still have a 
# status of released

my $cmd = "";
$LOGFILE = "$ENV{LESDIR}/log/phybatch.log";

$cmd=<<EOF;                                #Command to execute 
process var physical count release where user_pswd = 'maybe'
/
EOF

open(SQL,"| msql > $LOGFILE.$$.1 2>&1") || die ("Failed to open log file");
print SQL $cmd;
close(SQL);

exit(0);
