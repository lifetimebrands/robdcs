select shpdtl.shipid, shpdtl.ordnum, shpdtl.ordlin, shpdtl.prtnum, 
       shpdtl.linqty, shpdtl.shpqty, shpdtl.rsvqty, shpdtl.pckqty,
       shpdtl.inpqty, shpdtl.remqty, shpdtl.stgqty, shpdtl.oviqty
  from shphdr, shpdtl 
 where shphdr.shipid = shpdtl.shipid 
   and shphdr.shpseq = shpdtl.shpseq
   and shphdr.subsid = shpdtl.subsid
   and shphdr.ordnum = shpdtl.ordnum
   and shphdr.shpsts = 'C'
   and shphdr.loddte >= to_date('1/1/2002','MM/DD/YYYY') 
/
