#!/usr/bin/ksh
#. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts

sql << //
delete from usr_saprecon where trunc(recdte) = trunc(sysdate-1);
INSERT INTO usr_saprecon(recdte, rectyp, reccnt, recsrc) 
SELECT TRUNC(SYSDATE-1), evt.evt_id doc_type, COUNT (evt.evt_id) doc_counts, 'DCS'
FROM sl_evt_data evt
WHERE TRUNC (evt.evt_dt) = TRUNC(SYSDATE -1)
AND evt.sys_id = 'LIFETIME_DCS505'
and evt_data_seq not in (SELECT DISTINCT
                ed.evt_data_seq
                FROM SL_IFD_DATA_DTL ifd, SL_IFD_DATA_HDR ifh, SL_EVT_DATA ed
 WHERE ed.evt_id = 'VC_ORDER_MANAGEMENT'
   AND trunc(ed.evt_dt)= TRUNC(SYSDATE -1)
   AND ifh.evt_data_seq = ed.evt_data_seq
   AND ifd.ifd_data_seq = ifh.ifd_data_seq
   AND ifd.ifd_seg_id = 'ORDER_SEG'
   and SUBSTR( ifd_seg_data, 61, 15 )  is not null
   and trim(SUBSTR( ifd_seg_data, 61, 15 ) )  like '08%')
GROUP BY evt_id;
COMMIT;
exit
//
mv ${LESDIR}/files/hostin/saprecon.csv ${LESDIR}/db/data/load/saprecon
/opt/mchugh/prod/moca/bin/mload -H -c /opt/mchugh/prod/les/db/data/load/saprecon.ctl -D /opt/mchugh/prod/les/db/data/load/saprecon -d saprecon.csv

sql << //
insert into usr_saprecon(recdte, rectyp, reccnt, recsrc)
SELECT recdte, 'EXPDEL/EXPEXT', SUM(reccnt), 'SAP' 
  FROM usr_saprecon
 WHERE rectyp IN('EXPDEL', 'EXPEXT')
   AND recsrc = 'SAP'
   AND TRUNC(recdte) = TRUNC(SYSDATE-1)
 GROUP
    BY recdte;
INSERT INTO USR_SAPRECON(
  recdte, 
  rectyp, 
  rectyp2,
  reccnt, 
  reccnt2, 
  recsrc)
VALUES(
  SYSDATE-1, 
  'MATMAS',
  'MNT-ITM-FOOT', 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'MATMAS' 
	  AND recsrc = 'SAP' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0), 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'MNT-ITM-FOOT' 
	  AND recsrc = 'DCS' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0),
   'FIN');
INSERT INTO USR_SAPRECON(
  recdte, 
  rectyp, 
  rectyp2,
  reccnt, 
  reccnt2, 
  recsrc)
VALUES(
  SYSDATE-1, 
  'SLSEXT',
  'VC_ORDER_MANAGEMENT', 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'SLSEXT' 
	  AND recsrc = 'SAP' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0), 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'VC_ORDER_MANAGEMENT' 
	  AND recsrc = 'DCS' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0),
   'FIN');
INSERT INTO USR_SAPRECON(
  recdte, 
  rectyp, 
  rectyp2,
  reccnt, 
  reccnt2, 
  recsrc)
VALUES(
  SYSDATE-1, 
  'WKORD',
  'MNT-WO', 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'WKORD' 
	  AND recsrc = 'SAP' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0), 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'MNT-WO' 
	  AND recsrc = 'DCS' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0),
   'FIN');
INSERT INTO USR_SAPRECON(
  recdte, 
  rectyp, 
  rectyp2,
  reccnt, 
  reccnt2, 
  recsrc)
VALUES(
  SYSDATE-1, 
  'EXPDEL/EXPEXT',
  'RCPT_MANAGEMENT', 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'EXPDEL/EXPEXT' 
	  AND recsrc = 'SAP' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0), 
  NVL((SELECT reccnt 
	 FROM USR_SAPRECON 
	WHERE rectyp = 'RCPT_MANAGEMENT' 
	  AND recsrc = 'DCS' 
	  AND TRUNC(recdte) = TRUNC(SYSDATE-1)),0),
   'FIN');
COMMIT;
exit
//

exit 0
