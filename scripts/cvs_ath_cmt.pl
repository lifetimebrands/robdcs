#!/opt/perl5/bin/perl
#
# File    : cvs_ath_cmt.pl
#
# Purpose : Authorize user to use cvs commit
#
# Author  : Steve R. Hanchar
#
# Date    : 19-Sep-2001
#
# Usage   : cvs_ath_cmt
#
###############################################################################
#
# Include needed sub-components
#
use strict;
#
#use Getopt::Std; (unneeded);
#
###############################################################################
#
# Initialize internal parameters
#
my $par_pthsep;
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
}
else
{			# Unix
    $par_pthsep = "\/";
}
#
my $par_athusr = "lh51df,lh51kh,lh51sh";
#
my $par_usrnam = `whoami`;
chomp $par_usrnam;
#
###############################################################################
#
# Verify user name is in authorization list
#
my $athusr = ",".$par_athusr.",";
#
my $usrnam = ",".$par_usrnam.",";
#
my $status = !($athusr =~ /$usrnam/);
#
#printf ("AthUser (%s)\nUsrNam(%s)\nStatus(%s)\n", $athusr, $usrnam, $status);
#
###############################################################################
#
if ($status)
{
    printf ("
    *******************************************************************
    ** You are not authorized to commit changes in this environment! **
    *******************************************************************
    \n");
}
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit ($status);
#
###############################################################################
