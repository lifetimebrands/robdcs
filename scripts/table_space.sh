#!/usr/bin/ksh
#
# This script will run the Table_Space Report at 6AM using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports

sql << //
set trimspool on
set linesize 300
set pagesize 200
spool /opt/mchugh/prod/les/oraclereports/Table_Space.out
 

 

column FILE_NAME        format A52               heading "File Name"
column TABLESPACE_NAME  format A20               heading "Tablespace"
column BYTES            format 999999999999      heading "Bytes"
column MAXBYTES         format 999999999999999   heading "Max Bytes"
column RUNDATE          format A13               heading "Date" 

select FILE_NAME, TABLESPACE_NAME, BYTES, MAXBYTES, round(100*BYTES/MAXBYTES) "Percent Used", sysdate RUNDATE
from DBA_DATA_FILES
where MAXBYTES > 0
order by "Percent Used";


spool off
exit




