#!/usr/local/bin/perl
#
# Dustin Radtke
# This file should include all database purges and archives
# A cron task or at job should be created to call this script
# once a day.
#
$| = 1;                   # Flush output
# This will not log a trace any more. Check the log file for errors. Raman P
$cmd=<<EOF;
#set trace where activate = 1 and trcfil = 'archive_purge_data.trc'
#/
#archive production data where arc_nam = 'WORK-HISTORY'
/
#archive production data where arc_nam = 'RECEIVE-TRUCK'
/
#archive production data where arc_nam = 'DISPATCHED-TRAILERS'
/
#archive production data where arc_nam = 'WORK-ORDERS'
/
#archive production data where arc_nam = 'USER-COUNT-HISTORY'
/
#archive production data where arc_nam = 'ADJUSTMENT-HISTORY'
/
#archive production data where arc_nam = 'ORDER-ACTIVITY'
/
#archive production data where arc_nam = 'STDPRD-CANCELED-RPL-PICKS'
/
#archive production data where arc_nam = 'VAR-LABEL-SEQ' 
/
#archive production data where arc_nam = 'DAILY-TRAN'
/
#archive production data where arc_nam = 'DELETED-ORDERS'
/
#sl_purge evt_data where older_than_days = 7 
/
#sl_purge dwnld where older_than_days = 7
/
#sl_purge msg_log where older_than_days = 7
/
[truncate table srvusg]
/
EOF

$time = localtime;
print "$time \n $cmd";

open(MSQL,"| msql ") || die;
print MSQL $cmd;
close(MSQL);

exit(0);
