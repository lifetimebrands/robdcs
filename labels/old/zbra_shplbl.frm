#Standard Pick/Ship label with ship and product info.
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# reprinting a label.
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@srvlvl' SavSrvlvl from dual if (@srvlvl)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to 
# determine the flow of data.
#
#SELECT=select ' ' InWrkref from dual
#
# The first select will fill the InWrkref if a valid wrkref is received
# so that we'll either have a valid wrkref or a space in that field. We
# will also need to know if this is 'P'ick or 'R'eplenish work and if we
# have anything left to pick.
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# The next select is issued, if we have a wrkref and its 'P'ick work
# and we have remaining quantity,
# 
#
#SELECT=select var_shpord.shipid, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.ordnum, var_shpord.btcust, var_shpord.slscat, pckwrk.prtnum, pckwrk.cmbcod, pckwrk.devcod, pckwrk.srcloc, var_shpdtl_view.cstprt, var_shpord.carcod, var_shpord.srvlvl, pckwrk.dstloc, pckmov.stoloc, var_shpord.cponum from pckwrk, pckmov, var_shpord, var_shpdtl_view where pckwrk.wrkref = '@wrkref' and pckmov.cmbcod = pckwrk.cmbcod and (pckmov.arrqty = 0 or pckmov.arrqty > pckmov.prcqty) and pckwrk.shipid = var_shpord.shipid and pckwrk.ordnum = var_shpord.ordnum and pckwrk.shipid = var_shpdtl_view.shipid and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln order by pckmov.seqnum if (@InWrkref > ' ' and @remqty > 0 and @wrktyp != 'R')
#
# The next select is issued, if we have a wrkref and its 'P'ick work
# and we don't have any remaining quantity,
# 
#SELECT=select var_shpord.shipid, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.ordnum, var_shpord.btcust, var_shpord.slscat, pckwrk.prtnum, pckwrk.cmbcod, pckwrk.devcod, pckwrk.srcloc, var_shpdtl_view.cstprt, var_shpord.carcod, var_shpord.srvlvl, pckwrk.dstloc, pckmov.stoloc, var_shpord.cponum from pckwrk, pckmov, var_shpord, var_shpdtl_view where pckwrk.wrkref = '@wrkref' and pckmov.cmbcod = pckwrk.cmbcod and pckwrk.shipid = var_shpord.shipid and pckwrk.ordnum = var_shpord.ordnum and pckwrk.shipid = var_shpdtl_view.shipid and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln order by pckmov.seqnum if (@InWrkref > ' ' and @remqty = 0 and @wrktyp != 'R')
#
# The next select is issued, if we have don't have a wrkref (which means
# this is a label reprint and we were given a shipid instead).
# 
#SELECT=select var_shpord.shipid, var_shpord.stcust stname, var_shpord.stname junk, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, ' ' stposc, var_shpord.ordnum, var_shpord.btcust, var_shpord.slscat, ' ' prtnum, ' ' cmbcod, ' ' devcod, ' ' srcloc, var_shpdtl_view.cstprt, var_shpord.carcod, var_shpord.srvlvl, ' ' dstloc, ' ' stoloc, var_shpord.cponum from var_shpord, var_shpdtl_view where var_shpord.shipid = '@shipid' and var_shpdtl_view.shipid = var_shpord.shipid and var_shpdtl_view.ordnum = var_shpord.ordnum if (@InWrkref = ' ')
#
#SELECT=select rtrim('@stcity')||', '||'@ststcd' stadd4 from dual
#
# The next select is issued, if we have a wrkref and its 'R'eplinish work.
# 
#SELECT=select 'XXX' shipid, 'XXX' stcust, 'XXX' stname, '*************' stadd1, 'REPLENISHMENT' stadd2, 'REPLENISHMENT' stadd3, '*************' stadd4, 'XXX' stposc, 'XXX' ordnum, ' ' btcust, 'DOM' slscat, pckwrk.prtnum, pckwrk.cmbcod, pckwrk.devcod, pckwrk.srcloc, 'X' cstprt, 'X' carcod, 'X' srvlvl, pckwrk.dstloc, pckwrk.dstloc stoloc, 'XXX' cponum from pckwrk where pckwrk.wrkref = '@wrkref' if (@InWrkref > ' ' and @wrktyp = 'R')
#
# Initialize the pckqty because it needs to print as '0' (instead of null)
# if it's not selected or sent as an input parameter.  We also need to know the
# load level and how many labels to print.
#
#SELECT=select 0 pckqty from dual
#SELECT=select lodlvl, max(untcas) untcas, to_char(sum(pckqty)) pckqty, sum(pckqty) / max(untcas) newnum from pckwrk where cmbcod='@cmbcod' group by lodlvl if (@InWrkref > ' ')
#
# Set the first text field to the appropriate value depending on the whether
# or not you have a wrkref and what the load level is for this work.
#
#SELECT=select '******** PALLET PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'L' and @wrktyp != 'R')
#
#SELECT=select '********* CASE  PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'S' and @wrktyp != 'R')
#
#SELECT=select '********* PIECE PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'D' and @wrktyp != 'R')
#
#SELECT=select '**** CUSTOMER INFORMATION ****' text1 from dual if (@InWrkref = ' ')
#
# The next select will concatinate label & data into text fields if we have
# a work reference.
#
#SELECT=select 'PICK LOC  '||'@srcloc' text2, 'DEST LOC  '||'@stoloc' text3, 'P/N  '||'@prtnum'||'  QTY '||@pckqty text4, 'CARRIER    '||'@carcod'||' '||'@srvlvl' text5, 'S.O. #     '||'@ordnum' text6, 'CUST PO #  '||'@cponum' text7, 'CUST PN:   '||'@cstprt' text8 from dual if (@InWrkref > ' ')
#
# If a shipid was received, the next several selects will execute.  They will
# default the field values to the input values we saved at the beginning and
# then do the concatination select.
#
#SELECT=select '@SavStoloc' stoloc from dual if (@SavStoloc > ' ' and @InWrkref=' ')
#SELECT=select to_char(@SavPckqty) pckqty from dual if (@SavPckqty>0 and @InWrkref=' ')
#SELECT=select '@SavPrtnum' prtnum from dual if (@SavPrtnum > ' ' and @InWrkref=' ')
#SELECT=select '@SavCstprt' cstprt from dual if (@SavCstprt > ' ' and @InWrkref=' ')
#SELECT=select '@SavCarcod' carcod from dual if (@SavCarcod > ' ' and @InWrkref=' ')
#
#SELECT=select 'CUST PO #  '||'@cponum' text2, 'S.O. #     '||'@ordnum' text3, 'CARRIER    '||'@carcod'||' '||'@srvlvl'||'  QTY '||@pckqty text4, 'P/N  '||'@prtnum' text5, ' ' text7, 'CUST PN:   '||'@cstprt' text8 from dual if (@InWrkref = ' ')
#
# The following field value is dependant upon whether we have a stoloc or not.
#
#SELECT=select 'LOC        '||'@stoloc' text6 from dual if (@InWrkref = ' ' and @stoloc > ' ')
#SELECT=select ' ' text6 from dual if (@InWrkref = ' ' and @stoloc = ' ')
#
#
# The data statement is a list of the fields to be printed separated by
# tildens.  The rest of the file contains label formatting data.
#
#DATA=@stname~@shipid~@stadd1~@stadd2~@stadd3~@stadd4~@text1~@text2~@text3~@text4~@text5~@text6~@text7~@text8~@wrkref~@stposc~@stposc~
^XA^DFshplbl^FS;
^LH30,30;
^BY3,1,100;
^FWN;
^FO10,150^AF,62,16^FN1^FS;
^FO10,205^AF,63,15^FN3^FS;
^FO10,260^AF,63,15^FN4^FS;
^FO10,315^AF,63,15^FN5^FS;
^BY2,1,140;
^FO470,220^BCI,,N,N,N^FN17^FS;
^FO10,370^AF,63,15^FN6^FS;
^FO520,370^AF,60,15^FN16^FS;
^FO10,420^GB850,0,6^FS;
^FO10,435^AD,50,20^FN11^FS;
^FO10,495^AD,50,20^FN12^FS;
^FO10,555^AD,50,20^FN13^FS;
^FO10,615^AD,50,20^FN14^FS;
^BY4,1,100;
^FO10,665^BCN,,N,N,N^FN15^FS;
^FO10,770^AD,50,20^FN15^FS;
^FO10,820^GB850,0,6^FS;
^FO10,840^AD,60,20^FN7^FS;
^FO10,895^AD,60,20^FN8^FS;
^FO10,950^AD,60,20^FN9^FS;
^FO10,1005^AD,60,20^FN10^FS;
^BY2,1,90;
^FO400,1060^BCN,,N,N,N^FN2^FS;
^FO460,1155^AF,62,16^FN2^FS;
^FO10,0^AA,25,15^FDFROM:^FS;
^FO110,0^AA,25,15^FDSOFTWARE ARCHITECTS, INC.^FS;
^FO110,30^AA,25,15^FD450 N. SUNNNYSLOPE, STE 320^FS;
^FO110,60^AA,25,15^FDBROOKFILED, WI  53005^FS;
^FO10,90^GB200,50,50^FS;
^FO20,100^AD,40,15^FR^FDSHIP TO:^FS;
^XZ;
