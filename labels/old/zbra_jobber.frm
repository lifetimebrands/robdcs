#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
# # Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpord.pronum, var_shpord.waybil, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, var_shpdtl_view.deptno,  pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, pckwrk.cur_cas, pckwrk.tot_cas_cnt from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 35) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@lblffposc', 1, 5)lblffzip, substr('@pronum', 1, 10) lblpronum, substr('@waybil', 7, 7) lblway, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@carcod', 1, 20) lblcarrier, 'PO#: '||substr('@cponum', 1, 14) lblcustpo,substr('@deptno', 1, 5) lbldeptno, substr('@cstprt', 1, 3)||'-'||substr('@cstprt', 4, 4) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 8) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')'||' @lblstzip' lbldesc1, '('||'91'||')' lbl_91,'@cstprt' catalog,'@pckqty' lblqty from dual
#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#SELECT=select '>;>8420'||'@lblstzip' topbar, '('||'91'||')' lbl_91, '@stprt' lblcarrier from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select '>;>8910'||'@lblstcust' midbar from dual
#SELECT=select substr('@cstprt', 8, 4)lblcstprt from dual
#SELECT=select min('@pronum') lblpronum from dual
#SELECT=select min(substr('@waybil', 7, 7))lblway from dual
#
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select 'UPC#: '||decode('@prtnum','KITPART','Mixed',(select upccod from prtmst where prtnum='@prtnum')) lblupccod, 'Carton Quantity: '||decode('@prtnum','KITPART','Mixed','@untcas') cartqty, 'SKU#: '||decode('@prtnum','KITPART','Mixed','@prtnum') skuprt from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx from dual
#
#DATA=@lblstzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@dummy~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@dummy~@lbldesc1~@prtnum~@lblsrcloc~@lbl_91~@dummy~@dummy~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@topbar~@midbar~@dummy~@dumy~@dummy~@lblstcust~@lblcarrier~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblcasesort~@vc_slotno~@rplbl~@dummy~@dummy~@lblxofx~@lblupccod~@cartqty~@skuprt~ 
#
^XA^DFjobber^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,209^ADN,36,10^FN24^FS;
^FO15,34^ADN,36,10^FNLIFETIME BRANDS INC.^FS;
^FO15,69^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,104^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,139^ADN,36,10^FN16^FS;
^FO15,174^ADN,36,10^FN17^FS;
^FO170,139^ADN,36,10^FDU/C:^FS;
^FO225,139^ADN,36,10^FN22^FS;
^FO170,209^ADN,36,10^FN23^FS;
^FO15,244^ADN,36,10^FN36^FS;
^FO320,52^GB0,227,2^FS;
^FO170,174,^ADN,36,10^FN21^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO680,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE^FS;
^FO100,322^ADN,36,10^FN15^FS;
^FO450,278^GB0,223,2^FS;
^BY3,1.0,120^FO75,360^BCN,,N,N,N^FN25^FS;
^FO15,498^GB785,0,2^FS;
^FO480,295^ADN,36,10^FDCARRIER:^FS;
^FO600,295^ADN,40,15^FN31^FS;
^FO480,335^ADN,36,10^FDPRO#:^FS;
^FO480,375^ADN,36,10^FDB/L NUMBER:^FS;
^FO480,405^ADN,36,10^FDNumber of cartons:^FS;
^FO480,445^ADN,36,10^FN45^FS;
^FO670,580^ADN,18,10^FN34^FS;
^FO670,555^ADN,18,10^FN35^FS;
^FO15,650^GB785,0,2^FS;
^FO580,700^ADN,160,30^FN30^FS;
^FO480,650^GB0,225,2^FS;
^FO15,510^ADN,36,10^FDContents:^FS;
^FO15,550^ADN,36,10^FN7^FS;
^FO15,590^ADN,36,10^FN46^FS:
^FO380,550^ADN,36,10^FN48^FS;
^FO380,590^ADN,36,10^FN47^FS;
^FO15,658^ADN,18,10^FDSTORE NUMBER:^FS;
^FO500,658^ADN,18,10^FDFOR STORE:^FS;
^BY4,2.1,150^FO80,710^BCN,,N,N,N^FN26^FS;
^FO100,675^ADN,36,10^FN18^FS;
^FO185,675^ADN,36,10^FN30^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO90,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN40^FS;
^FO685,1560^ADN,54,15^FN41^FS;
^FO15,1414^ADN,54,15^FN17^FS;
^FO15,1474^ADN,54,15^FN16^FS;
^FO270,14^ADN,36,10^FN42^FS;
^FO455,1610^ADN,18,10^FN44^FS;
^PQ1,0,0,N^XZ;
