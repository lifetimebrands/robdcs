#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select min(sum(totwgt)) lbltotwgt from var_shpord where ordnum = '@ordnum' group by 1
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblstcust~@lbldesc~@lblxofx~@lbldept~@lbldestloc~@lblpckqty~@lblordnum~@lblitem~@ship_id~@cpodte~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@entdte~@lblffadr3~@lbltotwgt~@lblcasesort~@vc_slotno~@rplbl~
#
^XA^DFwestmar^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO100,15^ADN,54,15^FDWEST MARINE EC SHIPMENT^FS;
^FO15,81^ADN,36,10^FDFROM:^FS;
^FO115,81^ADN,36,10^FN28^FS;
^FO270,14^ADN,36,10^FN43^FS;
^FO15,121^ADN,36,10^FDLIFETIME BRANDS INC.^FS;
^FO15,161^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,201^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,241^ADN,36,10^FDSKU:^FS;
^FO70,241^ADN,36,10^FN3^FS;
^FO15,276^ADN,36,10^FDLOC:^FS;
^FO70,276^ADN,36,10^FN9^FS;
^FO170,276^ADN,36,10^FN24^FS;
^FO15,311^ADN,36,10^FDQTY:^FS;
^FO70,311^ADN,36,10^FN25^FS;
^FO170,311^ADN,36,10^FN26^FS;
^FO320,76^GB0,274,2^FS;
^FO15,360^ADN,36,10^FDTO:^FS;
^FO65,360^ADN,36,10^FN4^FS;
^FO65,405^ADN,36,10^FN5^FS;
^FO65,451^ADN,36,10^FN6^FS;
^FO65,497^ADN,36,10^FN39^FS;
^FO65,530^ADN,36,10^FN7^FS;
^FO385,530^ADN,36,10^FN1^FS;
^FO15,350^GB785,0,2^FS;
^FO15,572^GB785,0,2^FS;
^FO15,76^GB785,0,2^FS;
^FO15,582^ADN,36,10^FDPO Number:^FS;
^FO250,582^ADN,36,14^FN8^FS;
^FO685,127^ADN,36,14^FN38^FS;
^FO685,162^ADN,36,14^FN29^FS;
^FO15,755^ADN,36,10^FDQTY:^FS;
^FO250,755^ADN,36,10^FN25^FS;
^FO15,785^GB785,0,2^FS;
^FO15,793^ADN,36,10^FDCarton quantity:^FS;
^FO15,620^GB785,0,2^FS;
^FO15,630^ADN,36,10^FDContents:^FS;
^FO15,673^ADN,36,10^FDVPN#^FS;
^FO250,673^ADN,30,10^FN3^FS;
^FO15,713^ADN,36,10^FDSKU#^FS;
^FO470,625^ADN,36,10^FDUPC:^FS;
^BY2,,122^FO470,655^BCN,,N,N,N^FN27^FS;
^FO250,713^ADN,36,10^FN27^FS;
^FO450,793^ADN,36,10^FN22^FS;
^FO330,81^ADN,36,10^FDCARRIER:^FS;
^FO335,121^ADN,54,15^FN20^FS;
^FO330,195^ADN,36,10^FDB/L NUMBER:^FS;
^FO15,835^ADN,36,10^FDWeight:^FS;
^FO450,835^ADN,36,10^FN40^FS;
^FO450,620^GB0,165,2^FS;
^FO15,873^GB785,0,2^FS;
^FO250,888^ADN,36,10^FDSERIAL SHIPPING CONTAINER CODE^FS;
^FO191,922^ADN,36,10^FN10^FS;
^FO276,922^ADN,36,10^FN11^FS;
^FO331,922^ADN,36,10^FN12^FS;
^FO462,922^ADN,36,10^FN13^FS;
^FO629,922^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN41^FS;
^FO685,1560^ADN,54,15^FN42^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^PQ1,0,0,N^XZ;
