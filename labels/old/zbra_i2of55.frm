#
#I2of5 LABELS
#
#
#SELECT=select usr_altprtmst.alt_prtnum vc_i2of5, prtmst.untpak vc_untpak, prtmst.untcas vc_untcas, substr(prtdsc.lngdsc,1,25) vc_desc, usr_altprtmst.prtnum vc_part from usr_altprtmst, prtdsc, prtmst where usr_altprtmst.conv_flg=1 and usr_altprtmst.prtnum||'|----'=prtdsc.colval and prtdsc.colnam='prtnum|prt_client_id' and prtdsc.locale_id='US_ENGLISH' and usr_altprtmst.prtnum=prtmst.prtnum and usr_altprtmst.prtnum='@prtnum' 
#
#
#DATA=@vc_i2of5~@vc_untpak~@vc_untcas~@vc_desc~@vc_part~
#
#
#
^XA^DFi2of55^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^PW800^CI0^LRN;
^LH0,0;
^BY5,,195^FO60,60^B2N,,N,N^FN1^FS;
^FO235,21^AB30,30^FN1^FS;
^FO10,280^AB18,18^FDITEM#:^FS;
^FO125,280^AB18,18^FN5^FS;
^FO335,280^AB18,18^FN4^FS;
^FO10,320^AB18,18^FDMASTER PACK QTY:^FS;
^FO325,320^AB18,18^FN3^FS;
^FO400,320^AB18,18^FDINNER PACK QTY:^FS;
^FO700,320^AB18,18^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
