#SELECT=select ltrim(cstprt) cstprt, ltrim(prtnum) prtnum, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, substr(ltrim(vc_lblfd3),1,12) vc_lblfd3 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#
#DATA=@cstprt~@vc_lblfd2~@prtnum~
#
#
^XA^DF47^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH6,10;
^FO0,30^AB20,20^FD   LOWES^FS;
^BY1,1,30^FO0,60^B3N,N,N,N,N^FN1^FS;
^FO0,95^AB20,20^FN1^FS;
^FO40,160^AB30,30^FN3^FS;

^FO270,30^AB20,20^FD LOWES^FS;
^BY1,1,30^FO270,60^B3N,N,N,N,N^FN1^FS;
^FO270,95^AB20,20^FN1^FS;
^FO310,160^AB30,30^FN3^FS;

^FO540,30^AB20,20^FD   LOWES^FS;
^BY1,1,30^FO540,60^B3N,N,N,N,N^FN1^FS;
^FO540,95^AB20,20^FN1^FS;
^FO580,160^AB30,30^FN3^FS;
^XZ;
^PQ1,0,0,N^XZ;
