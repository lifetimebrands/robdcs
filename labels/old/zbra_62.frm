#SELECT=select cstprt, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(vc_lblfd2) vc_lblfd3, ltrim(vc_cstpr1) vc_cstpr1 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#SELECT=select rtrim(upccod)upccod from prtmst where prtnum = '@prtnum'
#SELECT=select rtrim(prtnum) prtnum from prtmst where prtnum = '@prtnum'
#
#DATA=@cstprt~@vc_lblfd3~@vc_retprc~@vc_cstpr1~@upccod~@prtnum~
#
#
^XA^DF62^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;

^FO50,52^CFD20,20^FN6^FS;
^FO50,60^CFD20,20^FN1^FS;
^FO10,80^CFD20,20^FN2^FS;
^FO50,140^AB20,20^FN3^FS;
^BY2,1.2,30^FO5,80^BUN,40,Y,N,Y^FN5^FS;
^FO25,170^CFD20,20^FN4^FS;

^FO320,52^CFD20,20^FN6^FS; 
^FO320,60^CFD20,20^FN1^FS;
^FO280,80^CFD20,20^FN2^FS;
^FO320,140^AB20,20^FN3^FS;
^BY2,1.2,30^FO275,80^BUN,40,Y,N,Y^FN5^FS;
^FO295,170^CFD20,20^FN4^FS;

^FO600,52^CFD20,20^FN6^FS;
^FO600,60^CFD20,20^FN1^FS;
^FO560,80^CFD20,20^FN2^FS;
^FO600,140^AB20,20^FN3^FS;
^BY2,1.2,30^FO545,80^BUN,40,Y,N,Y^FN5^FS;
^FO575,170^CFD20,20^FN4^FS;
^XZ;
^PQ1,0,0,N^XZ;
