#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, var_shpdtl_view.untcas, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, var_shpdtl_view.untcas, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 8, 10) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcardsc, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, to_char(@tot_cas_cnt, 'fm999') lbluntcas, substr('@stname',-3) lbldcnum, substr('@btcust',1,20) lblvendnum, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblcarrier, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select '>;' || '>8' || '420' || '@ffposc' lblcodezip from dual
#
#SELECT=select '>;' || '>8' || '92' || substr('@stcust', 8, 10) lblstcode from dual 
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblstcust~@lbldesc~@lblxofx~@lbldept~@lbldestloc~@lblpckqty~@lblordnum~@lblitem~@ship_id~@cpodte~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@entdte~@lblffadr3~@lblcarrier~@lblcardsc~@lbluntcas~@lbldcnum~@lblvendnum~@lblcodezip~@lblstcode~@lblcasesort~@vc_slotno~@rplbl~
#
^XA^DFnordstrom^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,14^ADN,24,10^FDFROM:^FS;
^FO115,14^ADN,24,10,^FN28^FS;
^FO15,34^ADN,30,10^FDLIFETIME BRANDS INC.^FS;
^FO15,74^ADN,30,10^FD12 APPLEGATE DR.^FS;
^FO15,114^ADN,30,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,154^ADN,24,10^FDSKU:^FS;
^FO70,154^ADN,24,10^FN3^FS;
^FO15,177^ADN,24,10^FDLOC:^FS;
^FO70,177^ADN,24,10^FN9^FS;
^FO170,177^ADN,24,10^FN24^FS;
^FO15,200^ADN,24,10^FDQTY:^FS;
^FO70,200^ADN,24,10^FN25^FS;
^FO170,200^ADN,24,10^FN26^FS;
^FO320,14^GB0,216,2^FS;
^FO330,14^ADN,24,10^FDTO^FS;
^FO380,14^ADN,30,10^FN4^FS;
^FO380,54^ADN,30,10^FN5^FS;
^FO380,94^ADN,30,10^FN6^FS;
^FO380,134^ADN,30,10^FN39^FS;
^FO380,169^ADN,30,10^FN7^FS;
^FO700,169^ADN,30,10^FN1^FS;
^FO15,230^GB785,0,2^FS;
^FO15,240^ADN,24,10^FD(420)SHIP TO POST^FS;
^FO145,262^ADN,24,10^FN21^FS;
^FO230,262^ADN,24,10^FN1^FS;
^BY3,,120^FO30,292^BCN,,N,N,N,A
^FN45^FS;
^FO460,230^GB0,200,2^FS;
^FO15,442^ADN,40,10^FDPO #:^FS;
^FO150,442^ADN,40,10^FN8^FS;
^FO15,477^ADN,40,10^FDDEPT:^FS;
^FO150,477^ADN,40,10^FN23^FS;
^FO15,542^ADN,40,10^FDDC #:^FS;
^FO150,542^ADN,40,10^FN43^FS;
^FO15,577^ADN,40,10^FDVEND:^FS;
^FO150,577^ADN,40,10^FN44^FS;
^FO470,442^ADN,40,10^FDCARTON:^FS;
^FO590,442^ADN,40,15^FN22^FS;
^FO470,487^ADN,40,10^FDSKU:^FS;
^FO510,487^ADN,40,10^FN27^FS;
^FO470,577^ADN,40,10^FDCASE QTY:^FS;
^FO590,577^ADN,40,10^FN42^FS;
^FO685,350^ADN,36,14^FN38^FS;
^FO685,385^ADN,36,14^FN29^FS;
^FO15,430^GB785,0,2^FS;
^FO15,618^GB785,0,2^FS;
^FO470,240^ADN,24,10^FDCARRIER:^FS;
^FO470,267^ADN,36,10^FDCARR:^FS;
^FO540,267^ADN,36,10^FN41^FS;
^FO470,317^ADN,24,10^FDPRO #:^FS;  
^FO450,618^GB0,216,2^FS;
^FO15,630^ADN,24,10^FDSTORE^FS;
^FO145,640^ADN,24,10^FD(92)^FS;
^FO230,640^ADN,24,10^FN20^FS;
^BY4,,122^FO40,680^BCN,,N,N,N,A
^FN46^FS;
^FO460,630^ADN,40,15^FDSTORE # ^FS; 
^FO670,630^ADN,40,20^FN20^FS;
^FO15,833^GB785,0,2^FS;
^FO15,848^ADN,24,10^FDSSCC-18^FS;
^FO191,860^ADN,36,10^FN10^FS;
^FO276,860^ADN,36,10^FN11^FS;
^FO331,860^ADN,36,10^FN12^FS;
^FO462,860^ADN,36,10^FN13^FS;
^FO629,860^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,920^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN47^FS;
^FO685,1560^ADN,54,15^FN48^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO270,14^ADN,36,10^FN49^FS;
^PQ1,0,0,N^XZ;
