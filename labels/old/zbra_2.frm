#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(vc_lblfd1) vc_lblfd1 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@vc_lblfd1~@vc_retprc~
#
^XA^DF2^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;
^FO10,40^AB20,20^FDHoffritz Sug^FS;
^FO60,70^AB20,20^FDPrice^FS;
^FO60,100^AB20,20^FN1^FS;
^FO0,130^AB20,20^FDOur Low Price^FS;
^FO50,160^AB30,30^FN2^FS;

^FO280,40^AB20,20^FDHoffritz Sug^FS;
^FO330,70^AB20,20^FDPrice^FS;
^FO330,100^AB20,20^FN1^FS;
^FO270,130^AB20,20^FDOur Low Price^FS;
^FO320,160^AB30,30^FN2^FS;

^FO550,40^AB20,20^FDHoffritz Sug^FS;
^FO600,70^AB20,20^FDPrice^FS;
^FO600,100^AB20,20^FN1^FS;
^FO540,130^AB20,20^FDOur Low Price^FS;
^FO590,160^AB30,30^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
