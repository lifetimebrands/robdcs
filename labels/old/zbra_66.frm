#SELECT=select cstprt, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(vc_lblfd2) vc_lblfd3, ltrim(vc_cstpr1) vc_cstpr1 from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#SELECT=select rtrim(upccod)upccod from prtmst where prtnum = '@prtnum'
#
#DATA=@cstprt~@vc_lblfd3~@vc_retprc~@vc_cstpr1~@upccod~
#
#
^XA^DF66^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;
^FO20,30^CFD30,20^FDMontgomery Ward^FS;
^FO50,60^CFD20,20^FN1^FS;
^FO10,80^CFD20,20^FN2^FS;
^FO50,100^AB20,20^FN3^FS;
^BY1,1.2,30^FO5,130^B3N,N,N,Y,N^FN5^FS;
^FO25,170^CFD20,20^FN4^FS;

^FO290,30^CFD30,20^FDMontgomery Ward^FS;
^FO320,60^CFD20,20^FN1^FS;
^FO280,80^CFD20,20^FN2^FS;
^FO320,100^AB20,20^FN3^FS;
^BY1,1.2,30^FO275,130^B3N,N,N,Y,N^FN5^FS;
^FO295,170^CFD20,20^FN4^FS;

^FO570,30^CFD30,20^FDMontgomery Ward^FS;
^FO600,60^CFD20,20^FN1^FS;
^FO560,80^CFD20,20^FN2^FS;
^FO600,100^AB20,20^FN3^FS;
^BY1,1.2,30^FO545,130^B3N,N,N,Y,N^FN5^FS;
^FO575,170^CFD20,20^FN4^FS;
^XZ;
^PQ1,0,0,N^XZ;
