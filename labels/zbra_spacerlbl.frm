#
#SELECT=select '@stoloc' loc, '@prtnum' prt, '@numlbls' numlbls, to_char(sysdate,'DD-MON-YYYY HH12MISS AM') printdate from dual
#
#SELECT=select decode(z.srcare, 'RACKSPCK', decode(z.untqty, z.pckqty, 'PALLET', 'CASES'), '') message from (select x.*, (select sum(i.untqty) from invsum i where i.prtnum = x.prtnum and i.stoloc = x.srcloc) untqty from (select p.srcare, p.srcloc, p.lblbat, p.prtnum, sum(p.pckqty) pckqty, sum(p.appqty) appqty from pckwrk p where p.pcksts = 'R' and p.wrktyp = 'P' and p.lodlvl = 'S' and p.lblbat is not null and p.pckqty != p.appqty and p.srcloc = '@stoloc' and p.lblbat = '@lblbat' group by p.srcare, p.srcloc, p.lblbat, p.prtnum) x) z
#
#DATA=@loc~@prt~@numlbls~@printdate~@message~
^XA^DFspacerlbl^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,1284^ADN,160,50^FN5^FS;
^FO380,15^ADN,36,10^FN4^FS;
^FO15,100^ADN,74,25^FDLocation:^FS;
^FO15,200^ADN,160,50^FN1^FS;
^FO16,400^ADN,74,25^FDItem:^FS;
^FO15,500^ADN,160,50^FN2^FS;
^FO15,700^ADN,74,25^FDLabel Qty:^FS;
^FO15,800^ADN,160,50^FN3^FS;
^FO15,1000^ADN,74,25^FDPicked By: ^FS;
^FO15,1200^GB785,0,2^FS;
^PQ1,0,0,N^XZ;
