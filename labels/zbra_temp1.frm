#
# $Id: zbra_temp1.frm,v 1.1.1.1 2001/09/18 23:05:47 lh51sh Exp $
#
# RPS LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' srvnum, ' ' shipper, ' ' packid, ' ' traknm, ' ' carcod, ' ' dstlvl from dual
#SELECT=select '('||substr(rtrim('@traknm'),1,7)||')' srvnum, substr(rtrim('@traknm'),8,7) shipper, substr(rtrim('@traknm'),15,8) packid from dual if (@traknm)
#SELECT=select rtrim('@traknm') traknm from dual if (@traknm)
#DATA=~
XA^DFtemp^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
~DG:SAMPLE.GRF,00080,010,FFFFFFFFFFFFFFFFFFFF8000FFFF0000FFFF0001;
#8000FFFF0000FFFF0001;
#8000FFFF0000FFFF0001;
#FFFF0000FFFF0000FFFF;
#FFFF0000FFFF0000FFFF;
#FFFF0000FFFF0000FFFF;
#FFFFFFFFFFFFFFFFFFFF;
^FO100,100^XG:SAMPLE.GRF,1,1^FS;
^FO65,70^AB20,20^FDLINENS N'^FS;
^XZ;
