#SELECT=select a.price, a.prtnum, a.upccod, a.lngdsc, a.cstprt, a.deptno, a.vendor, fi.display_trans, fi.header_lbl header, substr(fi.label_trans,1,1950) label_trans from (select '$'||ltrim(to_char(ord_line.vc_cust_retail_price,9999.99)) price, prtmst.prtnum, prtmst.upccod, substr(prtdsc.lngdsc,1,25) lngdsc, ord_line.cstprt, ord_line.deptno, ord.usr_vendor vendor, ord.btcust from ord, ord_line, prtmst, prtdsc where ord.ordnum='@ordnum' and ord.ordnum=ord_line.ordnum and ord_line.prtnum='@prtnum' and ord_line.client_id='----' and ord_line.prtnum=prtmst.prtnum and prtmst.prtnum||'|----'=prtdsc.colval and prtdsc.colnam='prtnum|prt_client_id') a, usr_frenchitems fi where a.prtnum=fi.prtnum(+) and a.btcust=fi.cstnum(+)
#
#DATA=@prtnum~@header~@label_trans~
*
^XA^DFtestsmfrench^EG^MCY^FWN^CFD,24^LH0,0^CI0^PR6^MNY^MTT^MMT^MD0.0^JJ0,0^PON^PMN^LRN;
^ADN,36,15^FO10,15^FN1~^FS;
^ADN,36,10^FO10,55^FN2~^FS;
^ADN,18,8^FO10,93^FB770,40,,^FH^FN3^FS;
^XZ;
^PQ1,0,0,N^XZ;
