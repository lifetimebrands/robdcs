#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2,var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc,var_shpdtl_view.deptno, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.tot_cas_cnt, pckwrk.untcas, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1,substr('@ffadd3',1,16) lblffadr3, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz,substr('@deptno',1, 10) lbldeptno,  substr('@ffposc', 1, 10) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd'||' '||substr('@stposc', 1, 10) lblstcstz, substr('@stposc', 1, 10) lblstzip, 'CARRIER: '||substr('@cardsc', 1, 20) lblcarrier, 'PO: '||substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, 'Carton Qty: '||to_char(@untcas, 'fm9999') lbluntcas, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort, substr('@stcust',8,10) stonum, '(91) '||substr('@stcust',8,10) stodis from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, 'STC#' lblstcust, 'Some Description' lbldesc, to_char(1, 'fm9999')||' of '||to_char(2, 'fm9999') lblxofx, 'XXXX' lbldept, '('||'420'||')' lbldesc1, 'R' lblordtyp from dual
#
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select 'UPC: '||(decode('@prtnum','KITPART',' ',(select upccod lblupccod from prtmst where prtnum='@prtnum'))) upccod from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select 'CARRIER: ' ||'@carcod' lblcarcod, 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt' ,'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblsrcloc~@lblcustpo~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblcarrier~@lbldesc1~@lblordtyp~@dummy~@lbluntcas~@lblordnum~@dummy~@lblstname~@dummy~@dummy~@ship_id~@lblupccod~@dummy~@dummy~@dummy~@lblcasesort~@vc_slotno~@rplbl~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@stonum~@stodis~@from_name~@print_date~
#
^XA^DFindigo^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^PW800^CI0^LRN;
^LH10,0;
^FO15,35^ADN,36,10^FDSHIP FROM:^FS;
^FO350,35^ADN,36,10^FN25^FS;
^FO15,94^ADN,36,10^FN39^FS;
^FO15,134^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,174^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO350,75^ADN,36,10^FN20^FS;
^FO15,225^ADN,36,10^FDSHIP TO:^FS;
^FO15,265^ADN,36,10^FN4^FS;
^FO15,305^ADN,36,10^FN5^FS;
^FO15,345^ADN,36,10^FN6^FS;
^FO15,385^ADN,36,10^FN7^FS;
^FO350,385^ADN,36,10^FN1^FS;
^FO15,220^GB785,0,2^FS;
^FO400,430^ADN,18,10^FD(420) SHIP TO POSTAL CODE:^FS;
^FO415,450^ADN,36,10^FN16^FS;
^FO510,450^ADN,36,10^FN1^FS;
^BY3,,122^FO415,488^BCN,,N,N,N^FN1^FS;
^FO15,425^GB785,0,2^FS;
^FO390,425^GB0,275,2^FS;
^FO15,510^ADN,36,10^FN9^FS;
^FO15,470^ADN,36,10^FDB/L Number: ^FS;
^FO15,550^ADN,36,10^FN26^FS;
^FO15,590^ADN,36,10^FN19^FS;
^FO700,465^ADN,36,10^FN27^FS;
^FO700,435^ADN,36,10^FN28^FS;
^FO550,610^ADN,36,10^FN24^FS;
^FO390,545^GB0,340,2^FS;
^FO15,630^GB785,0,2^FS;
^FO15,635^ADN,36,10^FDFinal Destination Store:^FS;
^FO95,675^ADN,36,10^FN38^FS;
^BY3,,122^FO25,715^BCN,,N,N,N^FN37^FS;
^FO400,635^ADN,36,10^FDFinal Destination:^FS;
^FO400,675^ADN,36,10^FN33^FS;
^FO400,715^ADN,36,10^FN34^FS;
^FO400,745^ADN,36,10^FN35^FS;
^FO400,785^ADN,36,10^FN36^FS;
^FO15,430^ADN,36,10^FN15^FS;
^FO15,885^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN30^FS;
^FO685,1560^ADN,54,15^FN31^FS;
^FO15,1414^ADN,54,15^FN8^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO270,14^ADN,36,10^FN32^FS;
^FO455,1610^ADN,18,10^FN40^FS;
^PQ1,0,0,N^XZ;
