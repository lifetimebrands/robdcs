#
#SELECT=select 'CONTENTS: '||p.prtnum prtnum, o.cponum, ol.vc_lblfield_string_1 descrip, ol.cstprt, 'QTY: '||p.untcas qty, 'STORE#: '||substr(o.stcust,instr(o.stcust,'-')+1,10) store from pckwrk p, ord_line ol, ord o where p.subucc='@subucc' and p.ordnum=ol.ordnum and p.ordlin=ol.ordlin and p.ordsln=ol.ordsln and ol.client_id='----'  and ol.ordnum=o.ordnum and ol.client_id=o.client_id
#
#DATA=@prtnum~@cponum~@descrip~@cstprt~@qty~@store~
#
^XA^DFwalpulse^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,15^ADN,54,15^FDSHIPPER: LIFETIME BRANDS, INC.^FS;
^FO15,85^ADN,54,15^FDATTN:  Dept. Mgr.  DEPT#: 0014^FS;
^FO15,155^ADN,54,15^FN6^FS;
^FO15,225^ADN,54,15^FN1^FS;
^FO15,365^ADN,54,15^FDPO#:^FS;
^FO115,365^ADN,54,15^FN2^FS;
^BY4,2.3,125^FO100,435^BCN,,N,N,Y,U^FN2^FS;
^FO15,595^ADN,54,15^FDDESC: ^FS;
^FO135,595^ADN,54,10^FN3^FS;
^FO15,665^ADN,54,15^FDSAP#:^FS;
^FO135,660^ADN,54,15^FN4^FS;
^BY4,2.3,125^FO100,740^BCN,,N,N,Y,U^FN4^FS;
^FO15,905^ADN,54,15^FDCATEGORY: DEPARTMENT MANAGER^FS;
^FO15,975^ADN,54,15^FN5^FS;
^FO120,1100^ADN,36,10^FDFor questions or additional information call^FS;
^FO320,1150^ADN,36,10^FD479-273-5444^FS;
^PQ1,0,0,N^XZ;
