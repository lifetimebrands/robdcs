#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
# # Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpord.pronum, var_shpord.waybil, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.tot_cas_cnt, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 35) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@lblffposc', 1, 5)lblffzip, substr('@pronum', 1, 10) lblpronum, substr('@waybil', 7, 7) lblway, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, 'Carrier: '||substr('@carcod', 1, 20) lblcarrier, 'PO#: '||substr('@cponum', 1, 14) lblcustpo,substr('@deptno', 1, 5) lbldeptno, substr('@cstprt', 1, 3)||'-'||substr('@cstprt', 4, 4) lblitem, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 8) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')'||' @lblstzip' lbldesc1, '('||'91'||')' lbl_91,'@cstprt' catalog,'@pckqty' lblqty from dual
#
#SELECT=select '>;>8420'||'@lblstzip' topbar, '('||'91'||')' lbl_91 from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select '>;>8910'||'@lblstcust' midbar, '(91)'||'@lblstcust' lblstcust2 from dual
#SELECT=select substr('@cstprt', 8, 4)lblcstprt from dual
#SELECT=select min('@pronum') lblpronum from dual
#SELECT=select min(substr('@waybil', 7, 7))lblway from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,'MIXED',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#SELECT=select 'VPN: '||decode('@prtnum','KITPART','Multiple SKUS','@prtnum') lblstyle from dual
#
#SELECT=select decode('@prtnum','KITPART','Partial Container',' ') lblpartial, 'Carton Number: '||'@cur_cas'||' of '||'@tot_cas_cnt' lblxofx from dual
#
#DATA=@lblstzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblpartial~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblxofx~@lbldesc1~@prtnum~@lblsrcloc~@vc_slotno~@dummy~@dummy~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@topbar~@midbar~@lbldeptno~@dumy~@dummy~@lblstcust2~@lblcarrier~@dummy~@dummy~@dummy~@dummy~@lblcasesort~@lbldescr~@from_name~@lblmessage~@print_date~@lblstyle~ 
#
^XA^DFgander^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FN38^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,194^ADN,36,10^FN24^FS;
^FO15,234^ADN,36,10^FN23^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO330,94^ADN,36,10^FN3^FS;
^FO330,134^ADN,36,10^FN4^FS;
^FO330,174^ADN,36,10^FN5^FS;
^FO330,214^ADN,36,10^FN6^FS;
^FO530,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE^FS;
^FO100,322^ADN,36,10^FN15^FS;
^FO450,278^GB0,596,2^FS;
^BY3,1.0,120^FO75,360^BCN,,N,N,N^FN25^FS;
^FO15,498^GB436,0,2^FS;
^FO15,760^GB785,0,2^FS;
^FO15,550^ADN,36,10^FN41^FS;
^FO15,600^ADN,36,10^FDQTY:^FS;
^FO120,600^ADN,36,10^FN22^FS;
^FO15,650^ADN,36,10^FDCountry Of Origin: China^FS;
^FO450,498^GB0,202,2^FS;
^FO15,510^ADN,36,10,^FN7^FS;
^FO480,635^ADN,36,10^FDDept: ^FS;
^FO540,635^ADN,160,20^FN27^FS;
^FO480,295^ADN,18,10^FDMark For:^FS;
^FO480,335^ADN,160,20^FN30^FS;
^BY3,1.0,120^FO480,505^BCN,,N,N,N^FN26^FS;
^FO450,630^GB335,0,2^FS;
^FO15,765^ADN,36,10^FN8^FS;
^FO480,765^ADN,36,10^FN31^FS;
^FO480,805^ADN,36,10^FDB/L Number:^FS;
^FO480,845^ADN,36,10^FN14^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO90,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN36^FS;
^FO685,1560^ADN,54,15^FN18^FS;
^FO15,1560^ADN,54,15^FN39^FS;
^FO15,1414^ADN,54,15^FN17^FS;
^FO15,1474^ADN,54,15^FN16^FS;
^FO455,1610^ADN,18,10^FN40^FS;
^PQ1,0,0,N^XZ;
