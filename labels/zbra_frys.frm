#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, var_shpdtl_view.untcas, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, var_shpdtl_view.untcas, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 8, 10) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcardsc, substr('@cponum', 1, 14) lblcustpo, decode('@prtnum','KITPART','Mixed',substr('@cstprt', 1, 14)) lblitem, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, decode((select prtnum from pckwrk where wrkref='@wrkref'),'KITPART',' ',to_char(@pckqty, 'fm9999')) lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, to_char(@tot_cas_cnt, 'fm999') lbluntcas, substr('@stname',-3) lbldcnum, substr('@btcust',1,20) lblvendnum, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblcarrier, '(420)' lbldesc, 'Number of Cartons: '||to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select '>;' || '>8' || '420' || '@ffposc' lblcodezip from dual
#
#SELECT=select '>;' || '>8' || '92' || substr('@stcust', 8, 10) lblstcode from dual 
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@prtnum'),null,'Mixed',(select upccod lblupc from prtmst where prtnum='@prtnum')) lblupc from dual
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@from_name~@lblcasesort~@vc_slotno~@lblmessage~@print_date~@lblstcust~@lbldesc~@lblxofx~@rplbl~@lbldestloc~@lblpckqty~@lblordnum~@lblitem~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblffadr3~@dummy~@lblcardsc~@dummy~@dummy~@dummy~@dummy~@dummy~@lblupc~@dummy~@dumy~@dummy~@dummy~
#
^XA^DFfrys^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,34^ADN,36,10^FDSHIP FROM:^FS;
^FO15,74^ADN,30,10^FN15^FS;
^FO15,114^ADN,30,10^FD12 APPLEGATE DR.^FS;
^FO15,154^ADN,30,10^FDROBINSVILLE, NJ 08691^FS;
^FO320,14^GB0,216,2^FS;
^FO330,14^ADN,24,10^FDSHIP TO:^FS;
^FO330,44^ADN,30,10^FN4^FS;
^FO330,79^ADN,30,10^FN5^FS;
^FO330,114^ADN,30,10^FN6^FS;
^FO330,149^ADN,30,10^FN39^FS;
^FO330,184^ADN,30,10^FN7^FS;
^FO680,184^ADN,30,10^FN1^FS;
^FO15,230^GB785,0,2^FS;
^FO15,240^ADN,24,10^FDSHIP TO POSTAL CODE^FS;
^FO70,262^ADN,24,10^FN21^FS;
^FO155,262^ADN,24,10^FN1^FS;
^BY3,,120^FO30,292^BCN,,N,N,Y,N^FN1^FS;
^FO370,230^GB0,200,2^FS;
^FO15,437^ADN,36,10^FDPO Number:^FS;
^FO200,437^ADN,36,10^FN8^FS;
^FO15,482^ADN,36,10^FDUPC#:^FS;
^FO200,482^ADN,36,10^FN47^FS;
^FO15,527^ADN,36,10^FDSKU#:^FS;
^FO200,527^ADN,36,10^FN27^FS;
^FO380,387^ADN,40,10^FN22^FS;
^FO15,577^ADN,40,10^FDCARTON QTY:^FS;
^FO200,577^ADN,40,10^FN25^FS;
^FO15,430^GB785,0,2^FS;
^FO15,618^GB785,0,2^FS;
^FO380,240^ADN,24,10^FDCARRIER:^FS;
^FO380,267^ADN,36,10^FN41^FS;
^FO380,317^ADN,24,10^FDPRO #:^FS;  
^FO380,357^ADN,24,10^FDB/L Number:^FS;
^FO340,618^GB0,216,2^FS;
^FO350,630^ADN,36,10^FDStore Location Number: ^FS; 
^FO350,670^ADN,100,30^FN20^FS;
^FO15,833^GB785,0,2^FS;
^FO15,848^ADN,24,10^FDSSCC-18^FS;
^FO191,860^ADN,36,10^FN10^FS;
^FO276,860^ADN,36,10^FN11^FS;
^FO331,860^ADN,36,10^FN12^FS;
^FO462,860^ADN,36,10^FN13^FS;
^FO629,860^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,920^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN16^FS;
^FO685,1560^ADN,54,15^FN17^FS;
^FO15,1560^ADN,54,15^FN18^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO455,1610^ADN,18,10^FN19^FS;
^FO330,14^ADN,36,10^FN23^FS;
^PQ1,0,0,N^XZ;
