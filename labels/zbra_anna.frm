#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.deptno, var_shpdtl_view.cstprt, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2,  var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, null deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.vc_slotno, pckwrk.uc_pck_seqnum uc_seqnum from var_shpord, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K') 
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, 'SKU: '||substr('@prtnum', 1, 20) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 20) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty,  to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2, 'SCAC: '||'@carcod' carrier from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select 'FFC#' lblffcust, '' lbldesc, to_char('@cur_cas', 'fm9999')||' of '||to_char('@tot_cas_cnt', 'fm9999') lblxofx, '@deptno' lbldept, '('||'420'||')' lbldesc1, '' lbldesc2 from dual
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode((select upccod from prtmst where prtnum='@prtnum'),null,'MIXED',(select upccod from prtmst where prtnum='@prtnum')) lblupccod from dual
#
#SELECT=select decode((select lngdsc from prtdsc where colval='@prtnum'||'|----'),null,'MIXED',(select lngdsc from prtdsc where colval='@prtnum'||'|----')) lbldescription from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#SELECT=select decode('@prtnum','KITPART','Multiple',(select cstprt from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@prtnum')) cstitem from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@dummy~@dummy~@dummy~@dummy~@dummy~@lblstcust~@lbldesc~@dummy~@dummy~@lbldesc1~@lbldesc2~@prtnum~@lblsrcloc~@lbldestloc~@lblpckqty~@lblordnum~@ship_id~@cstitem~@dummy~@lblupccod~@lbldescription~@lblcasesort~@carrier~@lblitem~@vc_slotno~@rplbl~@from_name~@print_date~
#
^XA^DFanna^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,174^ADN,36,10^FN29^FS;
^FO15,54^ADN,36,10^FN39^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO170,174^ADN,36,10^FN28^FS;
^FO15,214,^ADN,36,10^FN26^FS;
^FO320,52^GB0,200,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,250^GB785,0,2^FS;
^FO470,270^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO550,300^ADN,36,10^FN22^FS;
^FO630,300^ADN,36,10^FN1^FS;
^BY3,,100^FO470,330^BCN,,N,N,N^FN1^FS;
^FO450,250^GB0,212,2^FS;
^FO15,270^ADN,36,10^FDCARRIER INFO:^FS;
^FO15,470^ADN,36,14^FDP O  Number:^FS;
^BY3,,50^FO200,510^BCN,,N,N,N^FN7^FS;
^FO180,470^ADN,36,14^FN7^FS;
^FO17,330^ADN,36,10^FDB/L:^FS;
^FO17,380^ADN,36,10^FN35^FS;
^FO17,425^ADN,36,10^FDPRO:^FS;
^FO500,450^ADN,36,10^FN19^FS;
^FO15,460^GB785,0,2^FS;
^FO470,590^ADN,36,10^FN36^FS;
^FO15,590^ADN,36,10^FDQTY:^FS;
^FO70,590^ADN,36,10^FN27^FS;
^FO15,640^ADN,36,10^FDU P C^FS;:
^FO100,640^ADN,36,10^FN32^FS;
^FO15,680^ADN,36,10^FDDescription:^FS;
^FO190,680^ADN,36,10^FN33^FS;
^FO15,730^ADN,36,10^FDCustomer Material #^FS;
^FO15,770^ADN,64,20^FN30^FS;
^FO15,600^ADN,36,10^FN23^FS;
^FO15,570^GB785,0,2^FS;
^FO15,720^GB785,0,2^FS;
^FO450,720^GB0,150,2^FS;
^FO470,740^ADN,36,10^FDSTORE:^FS;
^FO500,790^ADN,54,15^FN18^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN8^FS;
^FO276,920^ADN,36,10^FN9^FS;
^FO331,920^ADN,36,10^FN10^FS;
^FO462,920^ADN,36,10^FN11^FS;
^FO629,920^ADN,36,10^FN12^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN34^FS;
^FO685,1560^ADN,54,15^FN37^FS;
^FO15,1414^ADN,54,15^FN25^FS;
^FO15,1474^ADN,54,15^FN24^FS;
^FO270,14^ADN,33,10^FN38^FS;
^FO455,1610^ADN,18,10^FN40^FS;
^PQ1,0,0,N^XZ;
