#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.cponum, pckwrk.tot_cas_cnt, var_shpord.ordnum, var_shpdtl_view.cstprt, substr(pckwrk.subucc,5,5) vndcod, pckwrk.pckqty, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.wrkref, pckwrk.untcas from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 19) lblffname, substr('@ffadd1', 1, 19) lblffadr1, substr('@ffadd2', 1, 19) lblffadr2, substr(rtrim('@ffcity'), 1, 10)||','||'@ffstcd'||' '||substr('@ffposc', 1, 5) lblffcitystzip, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 12) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@wrkref',4,7) lblctnid, to_char(sysdate,'MM/DD/YY') lbldat from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')'||' '||substr('@subucc',3,1)||' '||substr('@subucc',4,7)||' '||substr('@subucc',11,9)||' '||substr('@subucc',20,1) lblsn_5, substr('@subucc', 1, 20) lblsn_bar from dual
#
#SELECT=select '' lblhangcod, '' lblshortqty, 'UNITED STATES' lblcountry, to_char('@cur_cas', 'fm9999') lblctn1, to_char('@tot_cas_cnt', 'fm9999') lblctn2 from dual
#
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#DATA=@vndcod~@lblprtnum~@lblsrcloc~@lblcustpo~@lbldat~@lblcountry~@lblctnid~@lblffname~@lblffadr1~@lblffadr2~@lblffcitystzip~@lblitem~@lblctn1~@lblctn2~@lblprtnum~@lblhangcod~@lblsn_bar~@lblsn_5~@lblshortqty~@pckqty~@lbldestloc~@lbluntcas~@lblordnum~
#
^XA^DFhsn^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FWB;
^FO15,1070^CFD,26,10^FDVendor Code:^FS;
^FO15,1000^CFD,26,10^FN1^FS;
^FO145,1160^CFD,26,10^FDSKU:^FS;
^FO145,1090^CFD,26,10^FN2^FS;
^FO165,1160^CFD,26,10^FDLOC:^FS;
^FO165,1090^CFD,26,10^FN3^FS;
^FO165,980^CFD,26,10^FN21^FS;
^FO185,1160^CFD,26,10^FDU/C:^FS;
^FO185,1110^CFD,26,10^FN22^FS;
^FO185,980^CFD,26,10^FN23^FS;
^FO45,1150^CFD,32,10^FDFROM:^FS;
^FO85,980^CFD,26,10^FDLIFETIME HOAN CORP.^FS;
^FO105,1050^CFD,26,10^FD8-2 CORN ROAD^FS;
^FO125,1000^CFD,26,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,970^GB538,0,2^FS;
^FO200,15^GB0,1200,2^FS;
^FO300,15^GB0,1200,2^FS;
^FO550,15^GB0,1200,2^FS;
^FO15,350^GB785,0,2^FS;
^FO470,15^GB0,335,2^FS;
^FO630,15^GB0,335,2^FS;
^FO300,180^GB170,0,2^FS;
^FO15,690^CFD,40,20^FDP.O. NUMBER^FS;
^FO15,400^CFD,32,10^FN4^FS;
^BY3,1,100^FO70,380^BCB,,N,N,N^FN4^FS;
^FO210,1150^CFD,32,10^FDDATE:^FS;
^FO250,1050^CFD,32,10^FN5^FS;
^FO210,570^CFD,32,10^FDCOUNTRY OF ORIGIN:^FS;
^FO250,530^CFD,40,20^FN6^FS;
^FO210,100^CFD,32,10^FDCARTON ID NO.^FS;
^FO250,80^CFD,32,20^FN7^FS;
^FO310,1120^CFD,32,10^FDSHIP TO:^FS;
^FO350,975^CFD,26,12^FB240,1^FN8^FS;
^FO370,975^CFD,26,12^FB240,1^FN9^FS;
^FO390,975^CFD,26,12^FB240,1^FN10^FS;
^FO410,975^CFD,26,12^FB240,1^FN11^FS;
^FO310,570^CFD,32,10^FDHSN SKU NUMBER:^FS;
^FO350,400^CFD,32,20^FN12^FS;
^BY2.5,1,120^FO400,400^BCB,,N,N,N^FN12^FS;
^FO310,200^CFD,32,10^FDCARTON:^FS;
^FO350,220^CFD,32,10^FN13^FS;
^FO390,240^CFD,32,10^FDOF^FS;
^FO430,220^CFD,32,10^FN14^FS;
^FO310,70^CFD,32,10^FDVENDOR^FS;
^FO350,70^CFD,32,10^FDITEM NO^FS;
^FO410,70^CFD,32,10^FN15^FS;
^FO480,110^CFD,26,10^FDHANG CODE^FS;
^FO510,100^CFD,32,15^FN16^FS;
^BY4,2.3,150^FO570,410^BCB,,N,N,Y,U^FN17^FS;
^FO750,400^CFD,32,20^FN18^FS;
^FO560,100^CFD,26,10^FDSHORT QTY CODE^FS;
^FO590,100^CFD,40,20^FN19^FS;
^FO640,260^CFD,26,12^FDCARTON^FS;
^FO660,270^CFD,26,12^FDQTY:^FS;
^FO650,100^CFD,40,20^FN20^FS;
^BY3,1,100^FO690,80^BCB,,N,N,N^FN20^FS;
^FWN;
^PQ1,0,0,N^XZ;

