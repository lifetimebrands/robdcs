#
#SELECT=select loducc, palpos, '('||substr(loducc,1,2)||') '||substr(loducc,3,1)||' '||substr(loducc,4,7)||' '||substr(loducc,11,9)||' '||substr(loducc,20,1) loducctxt from invlod where lodnum='@lodnum'
#
#SELECT=select distinct 'PO: '||o.cponum lblpo, sl.ship_id, o.ordnum, o.rt_adr_id from ord o, ord_line ol, shipment_line sl, invdtl iv, invsub ib, invlod il where il.lodnum='@lodnum' and il.lodnum=ib.lodnum and ib.subnum=iv.subnum and iv.ship_line_id=sl.ship_line_id and sl.ordnum=ol.ordnum and sl.ordlin=ol.ordlin and sl.ordsln=ol.ordsln and ol.client_id='----' and ol.ordnum=o.ordnum
#
#SELECT=select count(distinct subnum) numcases from invsub where lodnum='@lodnum'
#
#SELECT=select distinct carnam from carhdr where carcod in (select distinct carcod from shipment where ship_id='@ship_id')
#
#SELECT=select adrnam rtname, adrln1 rtline1, adrln2 rtline2, adrcty||', '||adrstc||' '||adrpsz citystate from adrmst where adr_id='@rt_adr_id'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#DATA=@loducc~@palpos~@rtname~@rtline1~@rtline2~@citystate~@from_name~@numcases~@lblpo~@carnam~@loducctxt~
#
^XA^DFhomedep^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFROM^FS;
^FO15,94^ADN,36,10^FN7^FS;
^FO15,134^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,174^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO15,278^GB785,0,2^FS;
^FO15,285^ADN,36,10^FDCarrier:^FS;
^FO15,325^ADN,36,10^FN10^FS;
^FO15,365^ADN,36,10^FDTMS ID#:^FS;
^FO15,405^ADN,36,10^FDVICS BOL#:^FS;
^FO450,278^GB0,171,2^FS;
^FO455,285^ADN,36,10^FDStore #:^FS;
^FO15,450^GB785,0,2^FS;
^FO200,500^ADN,54,15^FN9^FS;
^FO15,610^GB785,0,2^FS;
^FO15,620^ADN,36,10^FDPallet No.:^FS;
^FO30,710^ADN,54,15^FN2^FS;
^FO392,610^GB0,212,2^FS;
^FO400,620^ADN,36,10^FDNo. of Cartons:^FS;
^FO415,710^ADN,54,15^FN8^FS;
^FO15,823^GB785,0,2^FS;
^FO85,1150^ADN,54,15^FN11^FS;
^BY4,2.3,255^FO85,845^BCN,,N,N,Y,U^FN1^FS;
^PQ1,0,1,Y^XZ;
