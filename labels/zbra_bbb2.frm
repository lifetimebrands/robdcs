#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname,substr(adrmst.adrln3,(instr(adrmst.adrln3,'#')+1))ffvendor, substr(var_shpord.stcust,(instr(var_shpord.stcust,'-')+1)) ffstnum, substr(var_shpord.ffcust,(instr(var_shpord.ffcust, '-')+1)) ffcarrier, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, adrmst.rgncod ffdestloc, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, '817' stvendor,var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, adrmst, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and var_shpord.stcust = adrmst.host_ext_id and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, substr(adrmst.adrln3,(instr(adrmst.adrln3,'#')+1))ffvendor,substr(var_shpord.stcust,(instr(var_shpord.stcust,'-')+1)) ffstnum, substr(var_shpord.ffcust,(instr(var_shpord.ffcust, '-')+1)) ffcarrier, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, adrmst.rgncod ffdestloc, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, '817' stvendor, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, null deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, adrmst, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and var_shpord.stcust = adrmst.host_ext_id  and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@rgncod',1,3) ffdestloc, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, substr('@ffvendor',1,9)lblffvendor, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select '>;>8420'||substr('@stposc', 1, 5) lblffzcode from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept, '@ffdestloc'||'-VDP' ffdestloc2 from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#SELECT=select decode('@prtnum','KITPART',null,(select upccod from prtmst where prtnum='@prtnum')) lblupc, decode('@prtnum','KITPART',null,(select substr(lngdsc,1,30) from prtdsc where colnam='prtnum|prt_client_id' and colval='@prtnum'||'|----')) lbldescrip, decode('@prtnum','KITPART',null,'@lblpckqty') lblpckqty2 from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblstcust~@lbldesc~@lblxofx~@lbldept~@lbldestloc~@lblpckqty2~@lblordnum~@lblitem~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@ffstnum~@lblffvendor~@lblffzcode~@ffcarrier~@ffdestloc2~@stvendor~@stposc~@lblcasesort~@vc_slotno~@rplbl~@from_name~@print_date~@lblupc~@lbldescrip~
#
^XA^DFbbb2^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO330,14^ADN,36,10^FN28^FS;
^FO270,14^ADN,36,10^FN48^FS;
^FO15,24^ADN,36,10^FN49^FS;
^FO15,64^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,104^ADN,36,10^FDROBBINSVILLE, NJ  08691^FS;
^FO15,209^ADN,54,15^FN9^FS;
^FO15,144^ADN,36,10^FN24^FS;
^FO550,14^ADN,36,10^FN26^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,54,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN15^FS;
^FO380,122^ADN,36,10^FN16^FS;
^FO380,168^ADN,36,10^FN17^FS;
^FO380,214^ADN,36,10^FN18^FS;
^FO700,214^ADN,36,10^FN19^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,36,10^FDCARRIER INFO:^FS;
^FO30,348^ADN,36,10^FDB/L:^FS;
^FO30,388^ADN,36,10^FDSCAC:^FS;
^FO30,428^ADN,36,10^FDPRO:^FS;
^FO15,490^ADN,36,10^FDPO Number:^FS;
^FO220,490^ADN,36,10^FN8^FS;
^FO460,490^ADN,36,10^FDVendor:^FS;
^FO650,490^ADN,36,10^FN44^FS;
^BY3,,80^FO150,530^BCN,,N,N,N^FN8^FS;
^FO420,330^ADN,36,10^FN21^FS;
^FO505,330^ADN,36,10^FN45^FS;
^BY3,,90^FO380,370^BCN,,N,N,N^FN41^FS;
^FO290,278^GB0,201,2^FS;
^FO300,290^ADN,30,10^FDPOSTAL ZIP#:^FS;
^FO15,580^ADN,36,14^FN38^FS;
^FO15,620^ADN,36,14^FN29^FS;
^FO15,478^GB785,0,2^FS;
^FO450,739^GB0,135,2^FS;
^FO15,620^GB785,0,2^FS;^M
^FO15,625^ADN,36,10^FDQTY:^FS;
^FO70,625^ADN,36,10^FN25^FS;
^FO15,665^ADN,36,10^FDUPC:^FS;
^FO70,665^ADN,36,10^FN51^FS;
^FO15,705^ADN,36,10^FDDESCRIPTION:^FS;
^BY3,,50^FO280,625^BUN,,Y,N,Y^FN51^FS;
^FO170,705^ADN,36,10^FN52^FS;
^FO550,705^ADN,36,10^FDCARTON:^FS;
^FO665,705^ADN,36,10^FN22^FS;
^FO15,739^GB785,0,2^FS;
^FO455,751^ADN,36,10^FDSTORE:^FS;
^FO455,796^ADN,70,50^FN39^FS;
^FO15,786^ADN,100,50^FN43^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN46^FS;
^FO15,1560^ADN,54,15^FDSLIPSHEET^FS;
^FO685,1560^ADN,54,15^FN47^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO455,1610^ADN,18,10^FN50^FS;
^PQ1,0,0,N^XZ;
