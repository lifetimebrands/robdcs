#SELECT=select 'Creative Tops Ltd' line1, 'Created House, 47/48 Causeway Rd' line2, 'Corby, Northants, NN17 4DU' line3, 'Attn: Dom Politano' line4, 'Tel: 44(0) 1536 207710' line5 from dual
#
#DATA=@line1~@line2~@line3~@line4~@line5~
^XA^DFdiana^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,100^ADN,50,20^FN1^FS;
^FO10,160^ADN,50,20^FN2^FS;
^FO10,220^ADN,50,20^FN3^FS;
^FO10,280^ADN,50,20^FN4^FS;
^FO10,340^ADN,50,20^FN5^FS;
^PQ1,0,0,N^XZ;
