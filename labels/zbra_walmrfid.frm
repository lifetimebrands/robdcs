# #UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#SELECT=select min(upccod) upccod from prtmst where prtnum = '@prtnum'  
#
#SELECT=select '200' || substr('@upccod', 2, 10) lblupccod from dual  
#
#SELECT=select '2 00 '  ||  substr('@upccod', 2, 5) lblhuman from dual 
#
#SELECT=select substr('@upccod', 7, 5) lblread from dual
#SELECT=select substr('@upccod', 13, 1) lblreads from dual
#		   
#
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')' lbldesc1, '@carcod' lblcarrier from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select tag_id from var_ucc2tag where ucccod = '@subucc'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#DATA=@lblffzip~@lblsnbar~@lblffcust~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblitem~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lbldesc1~@prtnum~@lblsrcloc~@lblcarrier~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@lblupccod~@cpodte~@entdte~@tag_id~
#
^XA^DFwalmrfid^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFROM:^FS;
^FO115,54^ADN,36,10^FN22^FS;
^FO15,94^ADN,36,10^FDLIFETIME HOAN CORP.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,174^ADN,36,10^FDSKU:^FS;
^FO70,174^ADN,36,10^FN16^FS;
^FO15,209^ADN,36,10^FDLOC:^FS;
^FO70,209^ADN,36,10^FN17^FS;
^FO170,209^ADN,36,10^FN19^FS;
^FO15,244^ADN,36,10^FDU/C:^FS;
^FO70,244^ADN,36,10^FN20^FS;
^FO170,244^ADN,36,10^FN21^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN7^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO145,322^ADN,36,10^FN15^FS;
^FO230,322^ADN,36,10^FN1^FS;
^BY3,,122^FO90,360^BCN,,N,N,N^FN1^FS;
^FO450,278^GB0,221,2^FS;
^FO669,650^ADN,18,10^FDDC NUMBER:^FS;
^FO500,612^ADN,66,25^FN3^FS;
^FO15,498^GB785,0,2^FS;
^FO15,580^GB785,0,2^FS;
^FO15,503^GFA,00888,00888,00012,7FFFFFFFFFFFFFFFFE,FF5AD6B5AD6B5AD6FE,F8000000000000001F,E00000000000000007,E00000000000000007,C00000000000000003,C00000000000000003,C00000000000000003,800000000000000001,C00000000000000003,800000FFFFFF000001,C00001FFFFFF000003,800001FFFFFE400001,C00003FFFFFCE00003,800007FFFFF9E00001,C0000FFEADF1F00003,80001FF80003F00001,C0001FF00007F80003,80003FE00007FC0001,C0007FFFFF0FFC0003,8000FFFFFF0FFE0001,C001FFFFFE1FFC0003,8001FFFFFC1FBC0001,C003FFFFF83FB80003,8007FEADF03F100001,C00FFC00003F100003,801FF800007E000001,C01FF000007E000003,803FFB6D407E000001,C07FFFFFC07C000003,80FFFFFF807C000801,C0FFFFFF00FC000C03,81FFFFFF00FC000C01,C3FFFFFE00FC001E03,8000000000FC001E01,C000000000FE003F03,81FFFFF000FE003F01,C1FFFFFE00FE007F03,80FFFFFF80FF00FE01,C07FFFFFE0FF00FE03,807FFFFFF07F81FC01,C03FF07FF87FE7FC03,803FF01FFC7FFFF801,C01FF81FFE7FFFF003,800FF80FFE3FFFF001,C00FFC07FF3FFFE003,8007FE07FF3FFFE001,C003FFFFFF1FFFC003,8003FFFFFF0FFF8001,C001FFFFFE0FFF0003,8001FFFFFC07FE0001,C000FFFFE001F80003,80007FC00000000001,C0007FE00000000003,80003FE00000000001,C0003FF0000000EB03,80001FF80000004A01,C0000FF80000004D03,80000FFC0000004501,C00007FC0000004403,800000000000000001,C00000000000000003,800000000000000001,C00000000000000003,800000000000000001,C00000000000000003,800000000000000001,C00000000000000003,C00000000000000003,E00000000000000007,E00000000000000007,F8000000000000001F,FF6B5AD6B5AD6B5AFE,3FFFFFFFFFFFFFFFFC
,
^ABN,11,8^FO95,513^FDTO IMPROVE PRODUCT AVAILABILITY FOR^FS;
^ABN,11,8^FO425,513^FDCONSUMERS, PACKAGING CONTAINS AN^FS;
^ABN,11,8^FO95,530^FDELECTRONIC PRODUCT CODE.  UPON PURCHASE,^FS;
^ABN,11,8^FO453,530^FDTHE EPC TAG CAN BE DISCARDED.^FS;
^ABN,11,8^FO95,547^FDHTTP://WWW.EPCGLOBALINC.ORG^FS;
^FO15,600^ADN,36,10^FDPO#:^FS;
^FO100,600^ADN,36,15^FN8^FS;
^FO15,650^ADN,36,10^FDWMIT:^FS;
^FO100,650^ADN,36,15^FN9^FS;
^FO15,690^GB785,0,2^FS;
^RS8,,,1^FS;  
^FO460,298^ADN,18,10^FDCARRIER:^FS;
^FO460,348^ADN,54,15^FN18^FS;
^FO685,465^ADN, 36,10^FN24^FS;
^FO685,435^ADN, 36,10^FN25^FS;
^FO15,908^GBN785,0,2^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^WT,5,,,1,^FN26^FS;
^PQ1,0,0,N^XZ;
