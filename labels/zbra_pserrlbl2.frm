#Error label with ordnum, ship_id, shpseq, subsid and wrkref for incorrect format associated with an Order 
#
# Get the info.
#
#SELECT=select ordnum, wrkref, ship_id, '@subucc' shpseq, uc_pck_seqnum, srcloc, vc_slotno, to_char(sysdate,'MM/DD/YYYY HH:MM:SS AM') printdate, prtnum, pckqty, subnum, wrktyp from pckwrk where subucc = '@subucc' 
#
#SELECT=select '******** ERROR LABEL *********' text0, substr('@err_mess',1,30) err1, substr('@err_mess',31,30) err2, substr('@err_mess',61,30) err3, substr('@err_mess',91,30) err4, substr('@err_mess',121,30) err5, substr('@err_mess',151,30) err6, substr('@err_mess',181,30) err7 from dual 
#
#SELECT=select decode(ord.btcust,'155438','CATALOG',null) cattag from ord, pckwrk where pckwrk.wrkref='@wrkref' and pckwrk.ordnum=ord.ordnum
#
#SELECT=select nvl(substr(dscmst.lngdsc,16),' ') carton_name from pckwrk, dscmst where pckwrk.wrkref='@wrkref' and pckwrk.ctncod=dscmst.colval(+) and dscmst.colnam(+) = 'ctncod' and dscmst.locale_id(+) = nvl(NULL, 'US_ENGLISH')
#
#SELECT=select pckmov.stoloc dstloc from pckwrk, pckmov where pckwrk.wrkref='@wrkref' and pckwrk.cmbcod=pckmov.cmbcod and pckmov.seqnum=(select max(pm.seqnum) from pckmov pm where pm.cmbcod=pckwrk.cmbcod)
#
#SELECT=select decode('@wrktyp','K',(select min(a.wrkzon) from locmst a, pckwrk b where a.stoloc=b.srcloc and b.ctnnum='@subnum'),(select a.wrkzon from locmst a, pckwrk b where a.stoloc=b.srcloc and b.wrkref='@wrkref')) strzon, decode('@wrktyp','K',(select max(a.wrkzon) from locmst a, pckwrk b where a.stoloc=b.srcloc and b.ctnnum='@subnum'),(select a.wrkzon from locmst a, pckwrk b where a.stoloc=b.srcloc and b.wrkref='@wrkref')) endzon from dual
#
#SELECT=select rtstr1 kitlvl_msg from poldat pd where pd.polcod = 'USR' and polvar = 'DTC-LABEL-MSG' and polval = 'KIT-ITEM'
#SELECT=select rtstr1 ordlvl_msg from poldat pd where pd.polcod = 'USR' and polvar = 'DTC-LABEL-MSG' and polval = 'ORDER-LVL'
#SELECT=select rtstr1 linlvl_msg from poldat pd where pd.polcod = 'USR' and polvar = 'DTC-LABEL-MSG' and polval = 'LINE-LVL'
#SELECT=select rtstr1 nonlvl_msg from poldat pd where pd.polcod = 'USR' and polvar = 'DTC-LABEL-MSG' and polval = 'NON-LVL'
#
#SELECT=select decode ('@wrktyp','K',decode ((select count(p.wrkref) from pckwrk p, ord_line ol where p.ordnum = ol.ordnum and p.ordlin = ol.ordlin and p.ordsln = ol.ordsln and p.client_id = ol.client_id and p.ctnnum = '@subnum' and ol.prjnum is not null),0,(select decode(min(ol.vc_order_proc_logic), '1', '@ordlvl_msg', '2', '@linlvl_msg', '@nonlvl_msg') order_fill_for_label from pckwrk p, ord_line ol where p.ordnum = ol.ordnum and p.ordlin = ol.ordlin and p.ordsln = ol.ordsln and p.client_id = ol.client_id and p.ctnnum = '@subnum' and ol.vc_order_proc_logic is not null),'@kitlvl_msg'),decode((select count(p.wrkref) from pckwrk p, ord_line ol where p.ordnum = ol.ordnum and p.ordlin = ol.ordlin and p.ordsln = ol.ordsln and p.client_id = ol.client_id and p.wrkref = '@wrkref' and ol.prjnum is not null and ol.vc_order_proc_logic is not null),0,(select decode(min(ol.vc_order_proc_logic), '1', '@ordlvl_msg', '2', '@linlvl_msg', '@nonlvl_msg') order_fill_for_label from pckwrk p, ord_line ol where p.ordnum = ol.ordnum and p.ordlin = ol.ordlin and p.ordsln = ol.ordsln and p.client_id = ol.client_id and p.wrkref = '@wrkref' and ol.vc_order_proc_logic is not null),'@kitlvl_msg')) logic from dual
#
#SELECT=select decode(nvl('@uc_pck_seqnum', '-'), '-', '@logic', '@uc_pck_seqnum') seqnum from dual
#
#DATA=@text0~@ordnum~@wrkref~@ship_id~@shpseq~@dummy~@err1~@err2~@err3~@err4~@err5~@err6~@err7~@seqnum~@srcloc~@dstloc~@vc_slotno~@printdate~@cattag~@prtnum~@pckqty~@carton_name~@strzon~@endzon~
^XA^DFpserrlbl2^FS^PR6;
^LH10,0;
^FO240,150^AD,80,15^FN1^FS;
^FO25,285^AD,80,15^FDORDER:^FS;
^FO240,285^AD,80,15^FN2^FS;
^FO25,360^AD,80,15^FDWRKREF:^FS;
^FO240,360^AD,80,15^FN3^FS;
^FO25,435^AD,80,15^FDSHIP ID:^FS;
^FO240,435^AD,80,15^FN4^FS;
^FO25,510^AD,80,20^FDSUBUCC:^FS;
^FO240,510^AD,80,20^FN5^FS;
^FO15,600^GB785,0,2^FS;
^FO25,655^AD,36,15^FN7^FS;
^FO25,695^AD,36,15^FN8^FS;
^FO25,735^AD,36,15^FN9^FS;
^FO25,775^AD,36,15^FN10^FS;
^FO25,815^AD,36,15^FN11^FS;
^FO25,855^AD,36,15^FN12^FS;
^FO25,895^AD,36,15^FN13^FS;
^A0N,34,34^FO400,1244^FN5^FS;
^A0B,28,28^FO14,1283^FN14^FS;
^A0N,34,34^FO64,1548^FN16^FS;
^A0N,34,34^FO224,1548^FN15^FS;
^A0N,34,34^FO620,1548^FN2^FS;
^A0N,34,34^FO620,1590^FN4^FS;
^A0N,17,18^FO620,1620^FN18^FS;
^A0N,34,34^FO64,1590^FN20^FS;
^A0N,34,34^FO284,1590^FN21^FS;
^BY4,2.3,255^FO74,1283^BCN,,N,N,Y,U^FN5^FS;
^A0B,28,28^FO727,1376^FN22^FS;
^A0B,28,28^FO782,1376^FN19FS;
^A0N,34,34^FO250,1244^FDSLOT:^FS;
^A0N,34,34^FO350,1244^FN17^FS;
^A0N,34,34^FO465,1548^FN23^FS;
^A0N,34,34^FO465,1590^FN24^FS;
^PQ1,0,0,N^XZ;
