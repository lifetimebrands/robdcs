#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, ord_line.vc_lblfield_string_1, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, pckwrk.cur_cas, pckwrk.tot_cas_cnt from var_shpord, var_shpdtl_view, pckwrk, ord_line, shipment_line where ord_line.client_id='----' and ord_line.ordnum=pckwrk.ordnum and pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, '@ffposc' lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, 'FOR: '||substr('@stadd1', 1, 30)||' #'||substr('@stcust',instr('@stcust','-')+1,10) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, 'PO#/No de PO: ' ||'@cponum' lblcustpo, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, 'PROD: '||rtrim('@vc_lblfield_string_1') lbldeptcode, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select '('||'420'||')' lbl428, '@carcod' lblcarcod, 'FFC#' lblffcust, '' lblbol, '@deptno' lbldept from dual
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select 'UPC#/CUP: '||decode('@prtnum','KITPART','MULTI',(select upccod from prtmst where prtnum='@prtnum')) upccod, 'SKU#/UGS: '||decode('@prtnum','KITPART','MULTI','@cstprt') lblitem, 'Count/Decompte '||'@cur_cas'||' of/de '||'@tot_cas_cnt' lblxofx from dual
#
#SELECT=select '>;' || '>8' || '420' || '@ffposc' lblcodezip, 'Mfg Model#/No de modele du fabricant: '||decode('@prtnum','KITPART','MULTI','@prtnum') ltbprt from dual
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@lbl428~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblbol~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblitem~@ltbprt~@upccod~@prtnum~@lblsrcloc~@lblcarcod~@lbldestloc~@lblxofx~@lblordnum~@ship_id~@dummy~@dummy~@dummy~@lblcodezip~@dummy~@dummy~@dummy~@dummy~@dummy~@lblcasesort~@vc_slotno~@dummy~@from_name~@print_date~
#
^XA^DFstapcan^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,19^ADN,18,10^FDShip From/^FS;
^FO15,39^ADN,18,10,^FDAdresse d'expedition:^FS;
^FO15,59^ADN,36,10^FN37^FS;
^FO15,99^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,139^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,179^ADN,36,10^FN23^FS;
^FO170,179^ADN,36,10^FN24^FS;
^FO15,219,^ADN,36,10^FN21^FS; 
^FO320,19^GB0,260,2^FS;
^FO330,19^ADN,18,10^FDShip To/Adresse de destination:^FS;
^FO330,59^ADN,36,10^FN4^FS;
^FO330,99^ADN,36,10^FN5^FS;
^FO330,139^ADN,36,10^FN6^FS;
^FO330,179^ADN,36,10^FN7^FS;
^FO621,179^ADN,36,10^FN1^FS;
^FO15,285^ADN,18,10^FDShip To Postal Code/^FS;
^FO15,300^ADN,18,10^FDCode postal d'exp.^FS;
^FO100,322^ADN,36,10^FN3^FS;
^FO185,322^ADN,36,10^FN1^FS;
^BY3,,122^FO35,360^BCN,,N,N,N^FN28^FS;
^FO15,278^GB785,0,2^FS;
^FO340,278^GB0,223,2^FS;
^FO15,498^GB785,0,2^FS;
^FO15,510^ADN,36,10^FN8^FS;
^FO15,550^ADN,36,10^FN15^FS;
^FO15,590^ADN,36,10^FN16^FS;
^FO15,630^ADN,36,10^FN17^FS;
^FO350,298^ADN,36,10^FDCarrier/Transporteur:^FS;
^FO350,350^ADN,36,10^FN20^FS;
^FO350,420^ADN,36,1o^FDB/L Number/No du connaissement:^FS;
^FO400,460^ADN,36,10^FN9^FS;
^FO15,670^GB785,0,2^FS;
^FO100,725^ADN,36,15^FN22^FS;
^FO15,873^GB785,0,2^FS;
^FO15,880^ADN,18,10^FDSerialized Shipping Container Number/^FS;
^FO15,900^ADN,18,10^FDNumero de serie du contenant d'expedition^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN34^FS;
^FO685,1560^ADN,54,15^FN35^FS;
^FO15,1414^ADN,54,15^FN19^FS;
^FO15,1474^ADN,54,15^FN18^FS;
^FO455,1610^ADN,18,10^FN38^FS;
^PQ1,0,1,Y^XZ;
