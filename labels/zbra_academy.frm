#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.deptno, var_shpdtl_view.cstprt, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.vc_slotno, pckwrk.uc_pck_seqnum uc_seqnum from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2,  var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, null deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.vc_slotno, pckwrk.uc_pck_seqnum uc_seqnum from var_shpord, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum if (@InWrkref > ' ' and @wrktyp = 'K') 
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblc2arrier, 'PO#: '||substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, decode((select prtnum from pckwrk where wrkref='@wrkref'),'KITPART',' ',to_char(@pckqty, 'fm9999')) lblpckqty,  to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') llblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '' lbldesc, 'Carton: '||to_char('@cur_cas', 'fm9999')||' of '||to_char('@tot_cas_cnt', 'fm9999') lblxofx, '@deptno' lbldept, '('||'420'||')' lbldesc1, '' lbldesc2, 'Quantity: '||decode('@prtnum','KITPART',' ','@pckqty') lblqty from dual
#
#SELECT=select decode('@prtnum','KITPART',null,(select upccod from prtmst where prtnum='@prtnum')) lblupccod, 'Style: '||decode('@prtnum','KITPART','MIXED','@prtnum') lblcstprt, 'Carrier: '||'@carcod' lblcarrier from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select usr_vendor vend from ord where ordnum='@ordnum'
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select decode('@prtnum','KITPART','SEE CARTON CONTENT LIST',null) lblmess from dual
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,'MIXED',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage, to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcstprt~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblqty~@lblxofx~@vend~@lblmess~@dummy~@lblstcust~@lblcarrier~@dummy~@dummy~@dummy~@dummy~@prtnum~@lblsrcloc~@lblcasesort~@vc_slotno~@lblordnum~@ship_id~@lblmessage~@print_date~@lblupccod~@dummy~@dummy~@dummy~@dummy~@lblcustpo~@dummy~
#
^XA^DFacademy^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFrom:^FS;
^FO100,120^ADN,54,15^FN15^FS;
^FO320,52^GB0,200,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,250^GB785,0,2^FS;
^FO470,270^ADN,36,10^FN19^FS;
^FO470,310^ADN,36,10^FDPRO#:^FS;
^FO470,350^ADN,36,10^FDB/L:^FS;
^FO450,250^GB0,142,2^FS;
^FO15,270^ADN,36,10^FDComments:^FS;
^FO15,390^GB785,0,2^FS;
^FO15,400^ADN,36,10^FN37^FS;
^FO380,440^ADN,36,10^FN13^FS;
^FO15,440^ADN,36,10^FN7^FS;
^FO15,480^ADN,36,10^FDColor: N/A^FS;
^FO15,520^ADN,36,10^FDSize: N/A^FS;
^FO380,560^ADN,36,10^FN14^FS;
^FO15,560^ADN,36,10^FDWidth: N/A^FS;
^FO15,600^GB785,0,2^FS;
^FO15,610^ADN,36,10^FDUPC:^FS;
^FO450,600^GB0,270,2^FS;
^BY3,,50^FO100,650^BUN,90,Y,N,Y^FN32^FS;
^FO15,660^ADN,36,10^FN16^FS;
^FO470,620^ADN,36,10^FDSTORE:^FS;
^FO470,660^ADN,54,15^FN18^FS;
^FO15,873^GB785,0,2^FS;
^FO191,920^ADN,36,10^FN8^FS;
^FO276,920^ADN,36,10^FN9^FS;
^FO331,920^ADN,36,10^FN10^FS;
^FO462,920^ADN,36,10^FN11^FS;
^FO629,920^ADN,36,10^FN12^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1414^ADN,54,15^FN24^FS;
^FO15,1474^ADN,54,15^FN25^FS;
^FO400,1414^ADN,54,15^FN29^FS;
^FO400,1474^ADN,54,15^FN28^FS;
^FO15,1284^ADN,130,35^FN26^FS;
^FO685,1560^ADN,54,15^FN27^FS;
^FO15,1560^ADN,54,15^FN30^FS;
^FO455,1610^ADN,18,10^FN31^FS;
^PQ1,0,0,N^XZ;
