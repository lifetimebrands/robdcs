#
#I2of5 LABELS
#
#
#SELECT=select usr_altprtmst.alt_prtnum vc_i2of5, usr_altprtmst.untpak vc_untpak, usr_altprtmst.untcas vc_untcas, substr(prtdsc.lngdsc,1,25) vc_desc, usr_altprtmst.prtnum vc_part from usr_altprtmst, prtdsc, prtmst where usr_altprtmst.prtnum=prtmst.prtnum and usr_altprtmst.conv_flg=1 and usr_altprtmst.prtnum||'|----'=prtdsc.colval and prtdsc.colnam='prtnum|prt_client_id' and prtdsc.locale_id='US_ENGLISH' and prtmst.ndccod='@ndccod'
#
#
#DATA=@vc_i2of5~@vc_untpak~@vc_untcas~@vc_desc~@vc_part~
#
#
#
^XA^DFi2of5york^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,0;
^BY5,,195^FO60,85^B2N,,N,N^FN1^FS;
^FO235,46^AB30,30^FN1^FS;
^FO10,305^AB18,18^FDITEM#:^FS;
^FO125,305^AB18,18^FN5^FS;
^FO335,305^AB18,18^FN4^FS;
^FO10,345^AB18,18^FDMASTER PACK QTY:^FS;
^FO325,345^AB18,18^FN3^FS;
^FO400,345^AB18,18^FDINNER PACK QTY:^FS;
^FO700,345^AB18,18^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
