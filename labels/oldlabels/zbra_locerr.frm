#Error label with ordnum, ship_id, shpseq, subsid and wrkref for incorrect format associated with an Order 
#
# Get the info.
#
#  MDS 04-Apr-2000 PTF 362, Allow for possibility location may be empty or have more than one part in it
#
#SELECT=select '******** LOC ERROR LABEL *********' text0 from dual 
#SELECT=select locmst.lstemp lstemp, locmst.lstdte lstdte from locmst where locmst.stoloc = '@stoloc' 
#SELECT=select count(prtnum) prtnum_cnt from invloc where stoloc='@stoloc'
#SELECT=select 'EMPTY'    prtnum from dual                          if (@prtnum_cnt = 0)
#SELECT=select  prtnum    prtnum from invloc where stoloc='@stoloc' if (@prtnum_cnt = 1) 
#SELECT=select 'MULTIPLE' prtnum from dual                          if (@prtnum_cnt > 1)
#
#DATA=@text0~@stoloc~@prtnum~@lstemp~@lstdte~
^XA^DFlocerr^FS;
^LH10,0;
^FO10,100^AD,80,15^FN1^FS;
^FO10,200^AD,80,15^FDLOCATION :^FS;
^FO250,200^AD,80,15^FN2^FS;
^FO10,300^AD,80,15^FDPART#:^FS;
^FO150,300^AD,80,15^FN3^FS;
^FO10,400^AD,80,15^FDEMP :^FS;
^FO150,400^AD,80,15^FN4^FS;
^FO10,500^AD,80,15^FDDATE :^FS;
^FO150,500^AD,80,15^FN5^FS;
^PQ1,0,0,N^XZ;
