#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select min(ffcust) ffcust from shphdr where waybil = '@waybil'
#SELECT=select sum(usr_totctn) usr_vc_totctn from shphdr where waybil = '@waybil'
#SELECT=select sum(usr_totwgt) usr_totwgt from shphdr where waybil = '@waybil'
#SELECT=select min(cponum) cponum,min(carcod) carcod,min(srvlvl) srvlvl from shphdr where waybil = '@waybil'
#SELECT=select distinct ffname,ffadd1,ffadd2,ffcity,ffstcd,ffposc from shphdr where ffcust = '@ffcust' and waybil = '@waybil'
#SELECT=select distinct cardsc from shphdr where carcod = '@carcod' and waybil='@waybil'
#SELECT=select substr(sysdate,1,11) stgdte,to_char('@usr_totwgt','99999') usr_totwgt from dual  
#
#The next selects check to see if there are multiple cponum's on the shipment
#if there is more then 1 it prints See Manifest in the purchace order box on the
#pre-printed form otherwise it prints the cponum.
#
#SELECT=select count(distinct cponum) cpocnt from shphdr where waybil = '@waybil'
#SELECT=select DECODE('@cpocnt', '1','@cponum','SEE MANIFEST') tmpcpo  from dual
#
#The next select grabs the trailer seal fields from the shpmst for user text on
#the bol
#
#SELECT=select trlsl1,trlsl2,trlsl3,trlsl4 from shpmst where shpnum = (select min(shpnum) from shphdr where waybil ='@waybil')
#
#SELECT=select count(*) from empmst
# Grab the substrings so the field lengths are correct for the labels...
#
# Add special instructions - modified 7/16/99 for Lifetime
#SELECT=select usrGetSpclInstr('@waybil') instxt from dual
#
# Add order notes - modified 8/26/99 for Lifetime
#SELECT=select usrGetOrdNotes('@waybil') nottxt from dual  
#
#SELECT=select '@stgdte' lblstgdte, '@waybil' lblwaybil, substr('@ffcust', 1, instr('@ffcust','-',1,1)-1) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd'||'  '||substr('@ffposc', 1, 5) lblffcstz, substr('@cardsc', 1, 20) lblcarrier, substr('@tmpcpo', 1, 14) lblcustpo, '@trlsl1' lbltrlsl1, '@trlsl2' lbltrlsl2, '@trlsl3' lbltrlsl3, '@trlsl4' lbltrlsl4 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select 'LIFETIME HOAN CORP.' lbllif, 'ROBBINSVILLE, NJ 08691' lblday, 'MASTER A/C #' lblmast, '***** TRIAL BOL *****' lbldesc1, '***** TRIAL BOL *****' lbldesc2, 'FAK70' lblfak70, '@carcod' lblcarcod from dual
#
#SELECT=select decode('@srvlvl','CC','COLLECT','PP','PREPAID') pplbl from dual
#
#SELECT=select substr('@instxt', 1, 40) lblinstxt1, substr('@instxt', 41, 40) lblinstxt2, substr('@instxt', 81, 40) lblinstxt3, substr('@nottxt', 1, 40) lblnottxt1, substr('@nottxt', 41, 40) lblnottxt2, substr('@nottxt', 81, 40) lblnottxt3 from dual  
#
# Get the total number of cartons - modified 9/28/99 for Lifetime
#
#SELECT=select count(distinct dtl.subnum) usr_vc_totctn from invdtl dtl, shpdtl sd, shphdr sh where dtl.ship_line_id = sd.ship_line_id and sd.ship_id = sh.ship_id and sd.shpseq = sh.shpseq and sd.subsid = sh.subsid and sd.ordnum = sh.ordnum and sh.waybil = '@waybil'
#
#DATA=\n\n\n\n\n\n\p50 @lblstgdte\p70 @lblwaybil\n\n\p5 @lbllif\n\p5 @lblday\n\n\n\p5 @lblffname\n\p5 @lblffadr1\n\p5 @lblffadr2\n\p5 @lblffcstz\p35 @lblmast\n\p37 @lblffcust\n\n\p10 @lblcustpo\p26 @lblcarrier\p45 @usr_vc_totctn\n\n\n\n\p1 @usr_vc_totctn\p54 @usr_totwgt\n\p62 @lblfak70\n\p26 @lbldesc1\n\p26 @lbldesc2\n\p26 @lbltrlsl1\n\p26 @lbltrlsl2\n\p26 @lbltrlsl3\n\p26 @lbltrlsl4\n\p26 @lblinstxt1\n\p26 @lblinstxt2\n\p26 @lblinstxt3\n\p26 @lblnottxt1\n\p26 @lblnottxt2\n\p26 @lblnottxt3\n\p1 @usr_vc_totctn\p54 @usr_totwgt\n\p62 @lblfak70\n\p22 @pplbl\f
