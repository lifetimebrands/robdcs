#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, var_shpdtl_view.deptno, shipment_line.vc_wave_lane, shipment_line.uc_seqnum from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, '@ffposc' lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@carcod', 1, 10)lblcarcod, substr('@cponum', 1, 14) lblcustpo,substr('@deptno', 1, 5) lbldeptno, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999')lblqty, '@uc_seqnum' lblcasesort from dual
#
#SELECT=select 'J.C. PENNEY PEPS (9050-6)' lblffname, '1 BELL DRIVE' lblffadr1, '.' lblffadr2, rtrim('RIDGEFIELD')||','||'NJ' lblffcstz, '07657' lblffzip from dual if(@carcod ='TTPJ')
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')'||'@lblffzip' lbldesc1, '('||'91'||')' lbl_91,'@cstprt' catalog,'@pckqty' lblqty from dual
#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#SELECT=select '>;>8420'||  '@lblffzip' topbar, '('||'91'||')' lbl_91, '@cstprt' lblcarrier from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#SELECT=select '>;>8910'||'@lblstcust' midbar from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblitem~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstcust~@lbldesc1~@prtnum~@lblsrcloc~@lbl_91~@catalog~@lblqty~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@topbar~@midbar~@lbldeptno~@cpodte~@lblcarcod~@entdte~@stnam~@ststreet~@stcustadr~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblcasesort~
#
^XA^DFjpenct^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO330,14^ADN,36,10^FN24^FS;
^FO15,24^ADN,36,10^FDLIFETIME BRANDS CORP.^FS;
^FO15,64^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,104^ADN,36,10^FN16^FS;
^FO15,209^ADN,54,15^FN17^FS;
^FO170,104^ADN,36,10^FDU/C:^FS;
^FO225,104^ADN,36,10^FN22^FS;
^FO550,14^ADN,36,10^FN23^FS;
^FO275,52^GB0,227,2^FS;
^FO15,144,^ADN,36,10^FN21^FS;
^FO290,54^ADN,36,10^FDTO:^FS;
^FO340,54^ADN,70,13^FN3^FS;
^FO340,122^ADN,36,10^FN4^FS;
^FO340,168^ADN,36,10^FN5^FS;
^FO340,214^ADN,36,10^FN6^FS;
^FO600,214^ADN,36,10^FN1^FS;
^FO340,244^ADN, 36,10^FN28^FS;
^FO685,244^ADN, 36,10^FN30^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POST^FS;
^FO100,322^ADN,36,10^FN15^FS;
^FO500,278^GB0,223,2^FS;
^BY4,1.0,120^FO75,360^BCN,,N,N,N^FN25^FS;
^FO15,498^GB785,0,2^FS;
^FO510,300^ADN,54,15^FDPO:^FS;
^FO585,300^ADN,54,15^FN7^FS;
^FO510,360^ADN,54,15^FDSUB:^FS;
^FO610,360^ADN,54,15^FN27^FS;
^FO510,420^ADN,48,10^FDHOME FURN/LEISURE^FS;
^FO530,550^ADN,100,30^FN14^FS;
^FO505,655^ADN,36,10^FN4^FS;
^FO505,765^ADN,36,10^FN5^FS;
^FO505,820^ADN,36,10^FN6^FS;
^FO720,820^ADN,36,10^FN1^FS;
^FO498,500^GB0,375,2^FS;
^FO15,873^GB785,0,2^FS;
^FO15,525^ADN,18,10^FDFOR^FS;
^BY4,2.1,300^FO80,550^BCN,,N,N,N^FN26^FS;
^FO100,520^ADN,36,10^FN18^FS;
^FO185,520^ADN,36,10^FN14^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO90,963^BCN,,N,N,Y,U^FN2^FS;
^FO35,1284^ADN,130,35^FN39^FS;
^PQ1,0,0,N^XZ;
