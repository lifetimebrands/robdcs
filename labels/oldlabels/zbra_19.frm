#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, 'Made in '||ltrim(vc_lblfd1) lblctry, vc_lblfd2 lblupc, prtnum cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#SELECT=select 'D'||nvl(max(deptno),'49') lbldept from ord_line, shipment_line, ord where btcust = '@cstnum' and ord_line.client_id='----' and ord.ordnum = shipment_line.ordnum and shipment_line.ordlin = ord_line.ordlin and ord_line.ordnum = ord.ordnum and ord_line.ordnum = shipment_line.ordnum and ord_line.prtnum = '@prtnum'
#
#SELECT=select '@lbldept'||' '||'@lblctry' lblinfo from dual
#
#DATA=@cstprt~@vc_retprc~@lblinfo~@lblupc~
#
^XA^DF19^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,0;
^FO20,10^ADN,30,12^FDGarden Ridge^FS;
^BY1,,60^FO50,90^BUN,65,Y,N,Y^FN4^FS;
^FO50,40^AP2,2^FN1^FS;
^FO10,55^AP2,2^FDHouston, TX 77084-6099^FS;
^FO12,70^AP2,2^FN3^FS;
^FO20,170^ADN,30,20^FN2^FS;
^FO245,10^ADN,30,12^FDGarden Ridge^FS;
^BY1,,60^FO270,90^BUN,65,Y,N,Y^FN4^FS;
^FO275,40^AP2,2^FN1^FS;
^FO236,55^AP2,2^FDHouston, TX 77084-6099^FS;
^FO237,70^AP2,2^FN3^FS;
^FO245,170^ADN,30,20^FN2^FS;
^FO475,10^ADN,30,12^FDGarden Ridge^FS;
^BY1,,60^FO500,90^BUN,65,Y,N,Y^FN4^FS;
^FO505,40^AP2,2^FN1^FS;
^FO466,55^AP2,2^FDHouston, TX 77084-6099^FS;
^FO467,70^AP2,2^FN3^FS;
^FO475,170^ADN,30,20^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
