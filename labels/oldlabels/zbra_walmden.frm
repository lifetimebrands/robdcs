#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#SELECT=select min(upccod) upccod from prtmst where prtnum = '@prtnum' if (@prtnum <> 'KITPART')
#
#SELECT=select '200' || substr('@upccod', 2, 10) lblupccod from dual if (@prtnum <> 'KITPART') 
#
#SELECT=select '2 00 '  ||  substr('@upccod', 2, 5) lblhuman from dual if (@prtnum <> 'KITPART')
#
#SELECT=select substr('@upccod', 7, 5) lblread from dual
#SELECT=select substr('@upccod', 13, 1) lblreads from dual
#		   
#
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')' lbldesc1, '@carcod' lblcarrier from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#DATA=@lblffzip~@lblsnbar~@lblffcust~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblitem~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lbldesc1~@prtnum~@lblsrcloc~@lblcarrier~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@lblupccod~@cpodte~@entdte~
#
^XA^DFwalm1^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFROM:^FS;
^FO115,54^ADN,36,10^FN22^FS;
^FO15,94^ADN,36,10^FDLIFETIME HOAN CORP.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,174^ADN,36,10^FDSKU:^FS;
^FO70,174^ADN,36,10^FN16^FS;
^FO15,209^ADN,36,10^FDLOC:^FS;
^FO70,209^ADN,36,10^FN17^FS;
^FO170,209^ADN,36,10^FN19^FS;
^FO15,244^ADN,36,10^FDU/C:^FS;
^FO70,244^ADN,36,10^FN20^FS;
^FO170,244^ADN,36,10^FN21^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN7^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO145,322^ADN,36,10^FN15^FS;
^FO230,322^ADN,36,10^FN1^FS;
^BY3,,122^FO90,360^BCN,,N,N,N^FN1^FS;
^FO450,278^GB0,221,2^FS;
^FO550,520^ADN,18,10^FDDC NUMBER:^FS;
^FO500,570^ADN,66,25^FN3^FS;
^FO15,498^GB785,0,2^FS;
^BY4,2.2,255^FO250,680^B2N,150,Y,N,Y^FN23^FS;
^FO100,720^ADN,70,15^FDI2of5^FS;
^FO15,530^ADN,36,10^FDPO#:^FS;
^FO390,835^ADN,36,10^FN26^FS;
^FO100,530^ADN,36,15^FN8^FS;
^FO15,590^ADN,36,10^FDWMIT:^FS;
^FO100,590^ADN,36,15^FN9^FS;
^FO15,658^GB785,0,2^FS;
^FO460,298^ADN,18,10^FDCARRIER:^FS;
^FO460,348^ADN,54,15^FN18^FS;
^FO685,465^ADN, 36,10^FN24^FS;
^FO685,435^ADN, 36,10^FN25^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^PQ1,0,0,N^XZ;
