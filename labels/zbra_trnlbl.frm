#Standard Pick/Ship label with ship and product info.
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# reprinting a label.
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@srvlvl' SavSrvlvl from dual if (@srvlvl)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to 
# determine the flow of data.
#
#SELECT=select ' ' InWrkref from dual
#
# The first select will fill the InWrkref if a valid wrkref is received
# so that we'll either have a valid wrkref or a space in that field. We
# will also need to know if this is 'P'ick or 'R'eplenish work and if we
# have anything left to pick.
#
#SELECT=select wrkref, subucc, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# 
#SELECT=select '0000000000' sprcnt, '' ship_id, '' stcust, '' stname, '*************' stadd1, 'STOCK MOVEMENT' stadd2, 'STOCK MOVEMENT' stadd3, '*************' stadd4, '' stposc, '' ordnum, ' ' btcust, 'DOM' slscat, pckwrk.prtnum, pckwrk.lotnum, pckwrk.cmbcod, pckwrk.devcod, pckwrk.srcloc, 'X' cstprt, 'X' carcod, 'X' srvlvl, pckwrk.dstloc, pckwrk.dstloc stoloc, '' cponum from pckwrk where pckwrk.wrkref = '@wrkref'
#
#
#SELECT=select 0 pckqty from dual
#SELECT=select lodlvl, pckqty, max(untcas) untcas, to_char(sum(pckqty / untcas)) casqty from pckwrk where wrkref = '@wrkref' group by lodlvl, pckqty if (@InWrkref > ' ')
#
# Set the first text field to the appropriate value depending on the whether
# or not you have a wrkref and what the load level is for this work.
#
#SELECT=select stoloc from pckmov, pckwrk where wrkref = '@wrkref' and pckwrk.cmbcod = pckmov.cmbcod order by seqnum
#SELECT=select '******** PALLET PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'L' and @wrktyp != 'R')
#
#SELECT=select '********* CASE  PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'S' and @wrktyp != 'R')
#
#SELECT=select '********* PIECE PICK *********' text1 from dual if (@InWrkref > ' ' and @lodlvl = 'D' and @wrktyp != 'R')
#
#SELECT=select '**** CUSTOMER INFORMATION ****' text1 from dual if (@InWrkref = ' ')
#
# The next select will concatinate label & data into text fields if we have
# a work reference.
#
#SELECT=select 'PICK LOC '|| '@srcloc ' text2, 'DEST LOC '||'@stoloc ' text3, 'P/N '|| '@prtnum ' text4, ' ' text5, '' text6, '' text7, '' text8, @untcas || ' PER CASE ' text10, 'COMMENT: ' ||'@movcmt' text11, ' @casqty '|| 'CASE(S)' text12 from dual if (@InWrkref > ' ')
#SELECT=select 'PICK LOC '||' @srcloc ' text13, 'DEST LOC '||'@stoloc ' text14, 'P/N '||' @prtnum ' text15, @untcas|| ' PER CASE ' text16, ' @casqty '||' CASE(S) ' text17,' @pckqty '||' PCS ' text18  from dual if (@InWrkref > ' ' and @wrktyp = 'R')
#
#SELECT=select decode('@wrktyp','R','DEST. WORK ZONE: '||(select wrkzon from locmst where stoloc='@stoloc'),' ') wrkzon from dual
#
# If a ship_id was received, the next several selects will execute.  They will
# default the field values to the input values we saved at the beginning and
# then do the concatination select.
#
#SELECT=select '@SavStoloc' stoloc from dual if (@SavStoloc > ' ' and @InWrkref=' ')
#SELECT=select to_char(@SavPckqty) pckqty from dual if (@SavPckqty>0 and @InWrkref=' ')
#SELECT=select '@SavPrtnum' prtnum from dual if (@SavPrtnum > ' ' and @InWrkref=' ')
#SELECT=select '@SavCstprt' cstprt from dual if (@SavCstprt > ' ' and @InWrkref=' ')
#SELECT=select '@SavCarcod' carcod from dual if (@SavCarcod > ' ' and @InWrkref=' ')
#
#SELECT=select 'CUST PO #  '||'@cponum' text2, 'S.O. #     '||'@ordnum' text3, '' text5, ' ' text7, 'CUST PN:   '||'@cstprt' text8 from dual if (@InWrkref = ' ')
#
#SELECT=select ' ' text5 from dual
#SELECT=select 'SPREAD # '||'@cponum' text5 from dual if (@sprcnt > ' ' && @sprcnt != '0000000000')
#SELECT=select '@ordnum' text9 from dual
#
# The following field value is dependant upon whether we have a stoloc or not.
#
#SELECT=select 'LOC        '||'@stoloc' text6 from dual if (@InWrkref = ' ' and @stoloc > ' ')
#SELECT=select ' ' text6 from dual if (@InWrkref = ' ' and @stoloc = ' ')
#
#SELECT=select 'C'||min(to_char(cpodte, 'MMDDYY')) cpodte, 'S'||min(to_char(entdte,'MMDDYY')) entdte from var_shpord where ordnum = '@ordnum' and  ship_id = '@ship_id' if (@InWrkref > ' ' and @wrkref != 'R ')
#
#SELECT=select 'LOT  '||rtrim('@lotnum') text19 from dual if (@wrktyp = 'R')
#
# The data statement is a list of the fields to be printed separated by
# tildens.  The rest of the file contains label formatting data.
#
#DATA=@stname~@ship_id~@stadd1~@stadd2~@stadd3~@stadd4~@text1~@text2~@text3~@text4~@text5~@text6~@text7~@text8~@subucc~@stposc~@text9~@text10~@text11~@text12~@text13~@text14~@text15~@text16~@text17~@text18~@cpodte~@text19~@entdte~@wrkzon~
^XA^DFtrnlbl^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH30,30;
^FWN;
^FO10,150^AF,62,16^FN1^FS;
^FO10,205^AF,63,15^FN3^FS;
^FO10,260^AF,63,15^FN4^FS;
^FO10,315^AF,63,15^FN5^FS;
^FO10,370^AF,63,15^FN6^FS;
^FO550,370^AF,63,15^FN16^FS;
^FO10,420^GB850,0,6^FS;
^FO670,385^ADN,36,14^FN27^FS;
^FO670,345^ADN,36,14^FN29^FS;
^FO10,435^AD,36,10^FN11^FS;
^FO350,435^AD,36,10^FN17^FS;
^FO10,495^AD,50,20^FN12^FS;
^FO10,555^AD,50,20^FN13^FS;
^FO10,615^AD,50,20^FN14^FS;
^BY4,1,100;
^FO10,440^AD,60,20^FN19^FS;
^BY4,2.3,255^FO10,505^BCN,,N,N,Y,U^FN15^FS;
^FO10,770^AD,50,20^FN15^FS;
^FO10,820^GB850,0,6^FS;
^FO10,850^AD,60,20^FN7^FS;
^FO10,900^AD,60,20^FN8^FS;
^FO10,950^AD,60,20^FN9^FS;
^FO450,900^AD,60,20^FN20^FS;
^FO10,995^AD,60,20^FN10^FS;
^FO500,950^AD,60,20^FN26^FS;
^FO10,1060^AD,60,20^FN30^FS;
^FO470,1000^AD,60,20^FN18^FS;
^FO10,1165^AF,30,10^FN2^FS;
^FO10,900^BCN,,N,N,N^FN2^FS;
^FO10,90^GB200,50,50^FS;
^FO20,100^AD,40,15^FR^FDMOVEMENT^FS;
^FO250,120^AD,40,20^FN21^FS;
^FO250,155^AD,40,20^FN22^FS;
^FO250,205^AD,40,20^FN23^FS;
^FO250,250^AD,40,20^FN28^FS;
^FO275,320^AD,40,20^FN24^FS;
^FO250,375^AD,40,20^FN25^FS;
^XZ;
