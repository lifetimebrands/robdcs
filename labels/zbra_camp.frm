#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, var_shpord.btcust from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, var_shpord.btcust from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, rtrim('@ffposc') lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, rtrim('@stposc') lblstzip, substr('@cardsc', 1, 20) lblcarrier, 'PO#: '||substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 25) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 15) lblprtnum, 'Quantity: '||to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select decode('@prtnum','KITPART','Mixed SKU','@lblitem') lblitem2 from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#SELECT=select 'SKU: '||'@prtnum' lblsku, decode('@prtnum','KIPART',null,(select substr(lngdsc,1,30) from prtdsc where colval='@prtnum'||'|----')) prt_descr from dual
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select decode(btcust,'187394','WEB','187393','DOMESTIC','187396','CANADA',null) aetype from ord where ordnum='@ordnum'
#
#SELECT=select decode('@prtnum','KITPART',null,(select upccod from prtmst where prtnum='@prtnum')) upccod, decode('@prtnum','KITPART',null,(select max(alt_prtnum) from usr_altprtmst where prtnum='@prtnum' and conv_flg=1)) gtin from dual
#
#SELECT=select 'UPC: '||'@upccod' lblupc, 'GTIN: '||'@gtin' lblgtin from dual
#
#SELECT=select decode('@prtnum','KITPART',null,(select caslen from ftpmst where ftpcod='@prtnum')) caslen, decode('@prtnum','KITPART',null,(select caswid from ftpmst where ftpcod='@prtnum')) caswid, decode('@prtnum','KITPART',null,(select cashgt from ftpmst where ftpcod='@prtnum')) cashgt from dual
#
#SELECT=select 'Dimensions: '||'@caslen'||'"x'||'@caswid'||'"x'||'@cashgt'||'"' lbldims from dual
#
#SELECT=select 'Weight: '||decode('@prtnum','KITPART',null,(select grswgt from prtmst where prtnum='@prtnum'))||' lbs.' lblweight from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@prt_descr~@lblpckqty~@lblsku~@upccod~@lblupc~@lblgtin~@gtin~@lbldims~@lblweight~@dummy~@dummy~@lblordnum~@lblitem2~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblcasesort~@vc_slotno~@lblmessage~@dummy~@from_name~@print_date~@aetype~
#
^XA^DFcamp^FS^SZ2^MMT^JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,14^ADN,36,10^FDFROM:^FS;
^FO330,14^ADN,36,10^FN43^FS;
^FO15,54^ADN,36,10^FN44^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO320,14^GB0,201,2^FS;
^FO330,14^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,36,10^FN4^FS;
^FO380,94^ADN,36,10^FN5^FS;
^FO380,134^ADN,36,10^FN6^FS;
^FO380,174^ADN,36,10^FN7^FS;
^FO670,174^ADN,36,10^FN1^FS;
^FO15,214^GB785,0,2^FS;
^FO15,220^ADN,45,15^FDCAMP, Inc.^FS;
^FO14,270^ADN,45,15^FN15^FS;
^FO15,320^GB785,0,2^FS;
^FO15,325^ADN,40,15^FN8^FS;
^FO15,365^ADN,40,15^FN16^FS;
^FO15,405^ADN,40,15^FN17^FS;
^BY3,,40^FO15,440^BCN,,N,N,N^FN3^FS; 
^FO15,485^ADN,40,15^FN19^FS;
^BY2,,100^FO15,525^B3N,N,N,N,N^FN18^FS;
^FO14,630^ADN,40,15^FN20^FS;
^FO15,670^GB500,2,20^FS;
^BY4,,100^FO35,670^B2N,100,N,N,N^FN21^FS;
^FO15,770^GB500,2,20^FS;
^FO15,800^GB785,0,2^FS;
^FO15,810^ADN,40,15^FN22^FS;
^FO15,845^ADN,40,15^FN23^FS;
^FO15,880^ADN,40,15^FDCountry Of Origin: China^FS;
^FO15,915^GB785,0,2^FS;
^FO191,930^ADN,36,10^FN10^FS;
^FO276,930^ADN,36,10^FN11^FS;
^FO331,930^ADN,36,10^FN12^FS;
^FO462,930^ADN,36,10^FN13^FS;
^FO629,930^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN40^FS;
^FO685,1560^ADN,54,15^FN41^FS;
^FO15,1560^ADN,54,15^FN42^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO380,1414^ADN,54,15^FN26^FS;
^FO380,1474^ADN,54,15^FN28^FS;
^FO330,1414^ADN,54,15^FN46^FS;
^FO455,1610^ADN,18,10^FN45^FS;
^PQ1,0,0,N^XZ;
