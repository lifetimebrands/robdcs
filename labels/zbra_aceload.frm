#
#SELECT=select distinct il.lodnum pallod, il.loducc, il.palpos, sl.ship_id, 'PO#: '||o.cponum cponum2, o.cponum, 'Carrier: '||s.carcod carcod, a.adrnam rtadrnam, a.adrln1 rtln1, a.adrln2 rtln2, a.adrln3 rtln3, a.adrcty rtcty, a.adrstc rtstc, a.adrpsz rtpsz, a.adrcty||', '||a.adrstc||' '||a.adrpsz rtstz, a1.adrnam stadrnam, a1.adrln1 stln1, a1.adrln2 stln2, a1.adrln3 stln3, a1.adrcty stcty,a1.adrstc ststc, a1.adrpsz stpsz, a1.adrcty||', '||a1.adrstc||' '||a1.adrpsz ststz, 'Vendor: '||o.usr_vendor vend, s.track_num doc_num from invlod il, invsub ib, invdtl iv, shipment_line sl, shipment s, ord_line ol, ord o, adrmst a, adrmst a1 where il.lodnum='@lodnum' and il.lodnum=ib.lodnum and ib.subnum=iv.subnum and iv.ship_line_id=sl.ship_line_id and sl.ship_id=s.ship_id and sl.ordnum=ol.ordnum and sl.ordlin=ol.ordlin and sl.ordsln=ol.ordsln and ol.client_id='----' and ol.ordnum=o.ordnum and o.rt_adr_id=a.adr_id and o.st_adr_id=a1.adr_id 
#
#SELECT=select  '('||substr('@loducc',1,2)||')' lblsn_1, substr('@loducc',3,1) lblsn_2, substr('@loducc',4,7) lblsn_3, substr('@loducc',11,9) lblsn_4, substr('@loducc',20,1) lblsn_5 from dual
#
#SELECT=select distinct to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, 'Lifetime Brands Inc.' from_name from ord where cponum='@cponum'
#
#DATA=@rtpsz~@loducc~@pallod~@rtadrnam~@rtln1~@rtln2~@rtstz~@cponum2~@vend~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@stadrnam~@stln1~@stln2~@ststz~@doc_num~@carcod~@dummy~@palpos~@dummy~@dummy~@dummy~@dumy~@dummy~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@rtln3~@dummy~@dummy~@dummy~@dummy~@from_name~@print_date~@dummy~
#
^XA^DFaceload^FS^SZ2^MMT^JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,14^ADN,36,10^FDFROM:^FS;
^FO330,14^ADN,36,10^FN43^FS;
^FO200,14^ADN,36,10^FN28^FS;
^FO15,54^ADN,36,10^FN44^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILE, NJ 08691^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN39^FS;
^FO380,247^ADN,36,10^FN7^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO145,322^ADN,36,10^FD(420)^FS;
^FO230,322^ADN,36,10^FN1^FS;
^BY3,,122^FO80,360^BCN,,N,N,N^FN1^FS;
^FO450,278^GB0,221,2^FS;
^FO15,510^ADN,74,25^FN8^FS;
^FO15,610^ADN,54,15^FN9^FS;
^FO15,498^GB785,0,2^FS;
^FO15,658^GB785,0,2^FS;
^FO470,300^ADN,36,10^FN20^FS;
^FO470,340^ADN,36,10^FDPRO:^FS;
^FO470,380^ADN,36,10^FDB/L:^FS;
^FO470,420^ADN,36,10^FDLOAD ID#:^FS;
^FO470,460^ADN,36,10^FN19^FS;
^FO450,658^GB0,216,2^FS;
^FO455,678^ADN,54,15^FDPALLET^FS;
^FO455,738^ADN,54,15^FN22^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,74,25^FDPALLET LABEL^FS;
^FO15,1384^ADN,74,25^FN3^FS;
^FO455,1610^ADN,18,10^FN45^FS;
^PQ1,0,0,N^XZ;
