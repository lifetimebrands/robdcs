#batch label 
#
# Get the info.
#
#SELECT=select to_char(sysdate,'MM/DD HH24: MI') time from dual
#
#SELECT=select '- NEW  BATCH STARTS  HERE -' text0 from dual 
#
#
#DATA=@text0~@time~
^XA^DFvcbatch^FS;
^LH10,0;
^FO100,100^AD,80,15^FN1^FS;
^FO100,180^AD,80,15^FN1^FS;
^FO100,20^AD,80,15^FN1^FS;
^FO100,360^AD,80,15^FDTIME :^FS;
^FO260,360^AD,80,15^FN2^FS;
^PQ1,0,0,N^XZ;
