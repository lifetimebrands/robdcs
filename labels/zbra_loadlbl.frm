#
#SELECT=select '@lodnum' lblload, 'Device: '||'@devcod' devcod, 'Batch: '||'@lblbat' lblbathdr,'@print_dte' print_dte, '@msg1' msg1, '@msg2' msg2, '@msg3' msg3 from dual
#
#SELECT=select 'Picks: '||count(subucc) pick_count from pckwrk where lblbat='@lblbat' and subucc is not null
#
#SELECT=select 'User ID: '||min(vc_prt_usr_id) usr_id, '@lblbat' lblbar from pckwrk where lblbat='@lblbat'
#
#DATA=@dummy~@devcod~@lblbathdr~@pick_count~@usr_id~@print_dte~@lblbar~@msg1~@msg2~@msg3~
^XA^DFloadlbl^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,10^ADN,74,25^FN2^FS;
^FO15,110^ADN,74,25^FN3^FS;
^FO16,210^ADN,74,25^FN4^FS;
^FO15,310^ADN,74,25^FN5^FS;
^FO15,410^ADN,74,25^FDDate/Time Printed:^FS;
^FO15,510^ADN,74,25^FN6^FS;
^FO15,610^ADN,74,25^FDDate/Time Out:^FS;
^FO15,800^GB785,0,2^FS;
^FO15,810^ADN,74,25^FDDate/Time Back:^FS;
^FO15,1000^GB785,0,2^FS;
^FO15,1010^ADN,74,25^FN8^FS;
^FO15,1110^ADN,74,25^FN9^FS;
^FO15,1210^ADN,74,25^FN10^FS;
^BY4,2.3,255^FO15,1310^BCN,,N,N,N,N^FN7^FS;
^PQ1,0,0,N^XZ;
