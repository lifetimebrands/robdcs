#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname,substr(adrmst.adrln3,(instr(adrmst.adrln3,'#')+1))ffvendor, substr(var_shpord.stcust,(instr(var_shpord.stcust,'-')+1)) ffstnum, substr(var_shpord.ffcust,(instr(var_shpord.ffcust, '-')+1)) ffcarrier, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, adrmst.rgncod ffdestloc, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, '817' stvendor,var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, adrmst, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and var_shpord.stcust = adrmst.host_ext_id and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, substr(adrmst.adrln3,(instr(adrmst.adrln3,'#')+1))ffvendor,substr(var_shpord.stcust,(instr(var_shpord.stcust,'-')+1)) ffstnum, substr(var_shpord.ffcust,(instr(var_shpord.ffcust, '-')+1)) ffcarrier, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, adrmst.rgncod ffdestloc, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, '817' stvendor, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, null deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, adrmst, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and var_shpord.stcust = adrmst.host_ext_id  and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd'||' '||substr('@ffposc',1,5) lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stcity', 1, 17)||','||'@ststcd'||' '||'@stposc' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@rgncod',1,3) ffdestloc, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, substr('@ffvendor',1,9)lblffvendor, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5, substr('@subucc',-6) extraucc from dual
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select substr('@stposc', 1, 5) lblffzcode, 'To Zip: '||'@stposc' stposc2 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept, '@ffdestloc' ffdestloc2 from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select case when '@lblstadr2' like '%ARGIX%' then ' ' else '@lblstadr2' end lblstadr22 from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select count(*) notecnt from pckwrk p, ord_line_note oln where p.wrkref='@wrkref' and '@prtnum'!='KITPART' and p.ordnum=oln.ordnum and p.ordlin=oln.ordlin and rownum<2
#
#SELECT=select 'Category: '||decode(nvl('@notecnt',0),0,decode('@prtnum','KITPART','Mixed',null),(select oln.nottxt from pckwrk p, ord_line_note oln where p.wrkref='@wrkref' and p.ordnum=oln.ordnum and p.ordlin=oln.ordlin and rownum<2)) lblcat from dual
#
#SELECT=select 'SKU: '||decode('@prtnum','KITPART','Mixed SKU','@prtnum') lblsku from dual
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#SELECT=select decode('@prtnum','KITPART',null,(select upccod from prtmst where prtnum='@prtnum')) lblupc, 'Description: '||decode('@prtnum','KITPART','Pick and Pack',(select substr(lngdsc,1,30) from prtdsc where colnam='prtnum|prt_client_id' and colval='@prtnum'||'|----'))||'     QTY: '||decode('@prtnum','KITPART',null,'@lblpckqty') lbldescrip  from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffnamex~@lblffadr1x~@lblffadr2x~@lblffcstzx~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr22~@lblstcstz~@lblstzip~@lblstcust~@lblcat~@lblsku~@lbldept~@dummy~@dummy~@lblordnum~@lblitem~@ship_id~@dummy~@lblmessage~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@ffstnum~@lblffvendor~@lblffzcode~@ffcarrier~@ffdestloc2~@extraucc~@stposc2~@lblcasesort~@vc_slotno~@rplbl~@from_name~@print_date~@lblupc~@lbldescrip~
#
^XA^DFbbb^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO270,14^ADN,36,10^FN48^FS;
^FO15,24^ADN,36,10^FN49^FS;
^FO15,64^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,104^ADN,36,10^FDROBBINSVILLE, NJ  08691^FS;
^FO320,15^GB0,268,2^FS;
^FO330,15^ADN,54,10^FDTO:^FS;
^FO380,24^ADN,36,10^FN4^FS;
^FO380,64^ADN,36,10^FN5^FS;
^FO380,104^ADN,36,10^FN6^FS;
^FO380,144^ADN,36,10^FN7^FS;
^FO380,24^ADN,36,10^FN15^FS;
^FO380,64^ADN,36,10^FN16^FS;
^FO380,104^ADN,36,10^FN17^FS;
^FO380,144^ADN,36,10^FN18^FS;
^FO15,190^GB785,0,2^FS;
^FO15,200^ADN,36,10^FN45^FS; 
^BY3,,90^FO15,250^BCN,,N,N,N^FN41^FS;
^FO320,278^GB0,95,2^FS;
^FO330,210^ADN,36,10^FDMark For:^FS;
^FO435,210^ADN,36,10^FN15^FS;
^FO330,250^ADN,36,10^FN16^FS;
^FO330,290^ADN,36,10^FN17^FS;
^FO330,330^ADN,36,10^FN18^FS;
^FO650,330^ADN,36,10^FN19^FS;
^FO15,370^GB785,0,2^FS;
^FO15,380^ADN,36,10^FDPO Number:^FS;
^FO220,380^ADN,36,10^FN8^FS;
^BY3,,80^FO380,380^BCN,,N,N,N^FN8^FS;
^FO450,670^GB0,70,2^FS;
^FO15,470^GB785,0,2^FS;^M
^FO15,480^ADN,36,10^FN52^FS;
^FO15,520^ADN,36,10^FDUPC:^FS;
^FO70,520^ADN,36,10^FN51^FS;
^FO15,560^ADN,36,10^FN22^FS;
^BY3,,50^FO300,520^BUN,,Y,N,Y^FN51^FS;
^FO15,610^ADN,54,15^FN21^FS;
^FO15,670^GB785,0,2^FS;
^FO15,680^ADN,36,10^FDPOOL ID:^FS;
^FO455,680^ADN,36,10^FDSTORE:^FS;
^FO650,680^ADN,54,15^FN39^FS;
^FO125,680^ADN,54,15^FN43^FS;
^FO15,740^GB785,0,2^FS;
^FO15,750^ADN,36,10^FDEVENT:^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN46^FS;
^FO450,1284^ADN,130,35^FN44^FS:
^FO15,1560^ADN,54,15^FN30^FS;
^FO685,1560^ADN,54,15^FN47^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO450,1414^ADN,54,15^FN28^FS;
^FO450,1474^ADN,54,15^FN26^FS;
^FO455,1610^ADN,18,10^FN50^FS;
^PQ1,0,0,N^XZ;
