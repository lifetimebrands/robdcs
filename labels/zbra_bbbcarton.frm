#SELECT=select subucc, prtnum, ordnum from pckwrk where wrkref='@wrkref'
#
#SELECT=select 'PO: '||cponum cpotxt, cponum from ord where client_id='----' and ordnum='@ordnum'
#
#SELECT=select 'UCC: '||@subucc ucctxt, decode('@prtnum','KITPART',null,(select upccod from prtmst where prtnum='@prtnum')) upc from dual
#
#DATA=@ucctxt~@subucc~@prtnum~@upc~@cpotxt~@cponum~
#
^XA^DFbbbcarton^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,15^ADN,54,15^FN1^FS;    
^BY4,2.3,255^FO50,75^BCN,,N,N,Y,U^FN2^FS; 
^FO15,350^ADN,54,15^FDUPC:^FS;
^BY3,,200^FO50,410^BUN,,Y,N,Y^FN4^FS;
^FO15,675^ADN,54,15^FDITEM:^FS;
^FO15,730^ADN,54,15^FN3^FS;
^FO15,810^ADN,54,15^FN5^FS;
^BY3,,200^FO50,860^BCN,,N,N,N^FN6^FS;
^FO15,1284^ADN,130,35^FDSECOND LABEL FOR^FS;
^FO15,1400^ADN,130,30^FN2^FS;
^PQ1,0,1,Y^XZ;
