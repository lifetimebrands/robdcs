#
#SELECT=select wrktyp from pckwrk where subucc='@subucc'
#
#SELECT=select distinct '#'||o.cponum cponum, ol.prtnum, trim(substr(ol.cstprt,1,instr(ol.cstprt,'/')-1)) cstprt, '$'||ltrim(to_char(ol.vc_cust_retail_price/100,9999.99)) price, p.pckqty, trim(substr(ol.cstprt,instr(ol.cstprt,'/')+1,20)) upccod from ord o, ord_line ol, pckwrk p where p.subucc='@subucc' and p.ordnum=ol.ordnum and p.ordlin=ol.ordlin and p.ordsln=ol.ordsln  and ol.client_id='----' and ol.ordnum=o.ordnum and ol.client_id=o.client_id if (@wrktyp!='K')
#
#SELECT=select distinct '#'||o.cponum cponum, ol.prtnum, trim(substr(ol.cstprt,1,instr(ol.cstprt,'/')-1)) cstprt, '$'||ltrim(to_char(ol.vc_cust_retail_price/100,9999.99)) price, pw.pckqty, trim(substr(ol.cstprt,instr(ol.cstprt,'/')+1,20)) upccod from ord o, ord_line ol, pckwrk p, pckwrk pw where p.subucc='@subucc' and p.subnum=pw.ctnnum and pw.prtnum='@prtnum' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.ordnum=o.ordnum and ol.client_id=o.client_id and ol.client_id='----' if(@wrktyp='K')
#
#SELECT=select substr(lngdsc,1,20) lngdsc from prtdsc where colnam='prtnum|prt_client_id' and colval='@prtnum'||'|----'
#
#DATA=@cstprt~@price~@lngdsc~@upccod~@pckqty~@cponum~
^XA^DFunivinner^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO100,100^ADN,54,15^FN1^FS;
^FO15,160^ADN,54,15^FN3^FS;
^FO15,300^GB130,0,54^FS;
^FO300,250^ADN36,10^FN6^FS;
^FO15,300^FR^ADN,54,15^FN5^FS;
^FO150,300^ADN,54,15^FDPCS^FS;
^FO375,300^ADN,54,15^FN2^FS;
^BY3,,50^FO600,700^BUR,90,Y,N,Y^FN4^FS;
^PQ1,0,0,N^XZ;
