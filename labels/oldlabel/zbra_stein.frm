#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpdtl_view.deptno lbldept, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, pckwrk.cur_cas, pckwrk.tot_cas_cnt from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 2, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, substr('@stcust', instr('@stcust', '-', 1, 1)+3 , 20) lblstrore2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')' lbldesc1, '('||'91'||')' lbl_91, '@carcod' lblcarrier, 'Carton '||to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx from dual
#
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select '>;>8420'||'@lblffzip' topbar, '>;>891'||'@lblstcust' midbar from dual
#
#SELECT=select usr_vendor from ord where ordnum='@ordnum'
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select '@lblcustpo' lblpo from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lbldept~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstcust~@lbldesc1~@prtnum~@lblsrcloc~@lbl_91~@lblcarrier~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@cpodte~@entdte~@topbar~@midbar~@lblxofx~@usr_vendor~@lblpo~@lblstrore2~
#
^XA^DFstein^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,14^ADN,36,10^FDFROM:^FS;
^FO10,54^ADN,36,10^FDLIFETIME BRANDS CORP.^FS;
^FO10,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO10,134^ADN,36,10^FDROBBINSVILLE, NJ  08691^FS;
^FO10,174^ADN,36,10^FN16^FS;
^FO165,174^ADN,36,10^FN17^FS;
^FO165,14^ADN,36,10^FN23^FS;
^FO310,14^GB0,205,2^FS;
^FO320,14^ADN,36,10^FDTO:^FS;
^FO370,14^ADN,54,13^FN3^FS;
^FO370,82^ADN,36,10^FN4^FS;
^FO370,128^ADN,36,10^FN5^FS;
^FO370,174^ADN,36,10^FN6^FS;
^FO611,174^ADN,36,10^FN1^FS;
^FO15,219^GB785,0,2^FS;
^FO15,224^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO165,248^ADN,36,10^FN15^FS;
^FO252,248^ADN,36,10^FN1^FS;
^BY4,,122^FO55,286^BCN,,N,N,N^FN26^FS;
^FO500,219^GB0,205,2^FS;
^FO15,424^GB785,0,2^FS;
^FO15,430^ADN,50,20^FDPO#:^FS;
^FO120,430^ADN,50,20^FN30^FS;
^FO15,490^ADN,36,15^FDDEPT:^FS;
^FO140,490^ADN,36,15^FN8^FS;
^FO15,530^ADN,36,15^FN28^FS;
^FO15,570^GB785,0,2^FS;
^FO165,580^ADN,36,10^FD(91)^FS;
^FO230,580^ADN,36,10^FN14^FS;
^BY4,,255^FO85,620^BCN,,N,N,N^FN27^FS;
^FO15,590^ADN,18,10^FDFOR:^FS;
^FO520,718^ADN,100,50^FN31^FS;
^FO500,570^GB0,313,2^FS;
^FO15,883^GB785,0,2^FS;
^FO15,888^ADN,18,10^FDSSCC^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO520,224^ADN,36,10^FDVendor#:^FS;
^FO520,264^ADN,36,10^FN29^FS;
^FO685,394^ADN,36,10^FN24^FS;
^FO685,360^ADN,36,10^FN25^FS;
^PQ1,0,0,N^XZ;
