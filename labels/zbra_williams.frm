#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, rtrim('@ffposc') lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, rtrim('@stposc') lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 25) lblitem2, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, 'Item#: '||substr('@prtnum', 1, 12) lblprtnum, decode((select prtnum from pckwrk where wrkref='@wrkref'),'KITPART',' ',to_char(@pckqty, 'fm9999')) lblpckqty, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, '('||'420'||')' lbldesc, 'Number of Cartons: '||to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, '@deptno' lbldept from dual
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select 'SKU: '||decode('@prtnum','KITPART','MIXED','@lblitem2') lblitem, 'Carton Qty: '||'@lblpckqty' qty2 from dual
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,' ',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select 'Vendor ID: '||usr_vendor vend from ord where ordnum='@ordnum'
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select 'Weight: '||usr_getupsweight('@wrkref') lbltotweight from dual
#
#SELECT=select  o.vc_by_store||' '||substr(o.stcust,instr(o.stcust,'-')+1,10)||' '||ol.vc_lblfield_string_1 lblcode from ord o, ord_line ol, pckwrk p where p.wrkref='@wrkref' and p.ordnum=ol.ordnum and p.ordlin=ol.ordlin and p.ordsln=ol.ordsln and ol.client_id='----' and ol.ordnum=o.ordnum and ol.client_id=o.client_id
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode((select prtfam from prtmst where prtnum='@prtnum'),null,' ',(select prtfam from prtmst where prtnum='@prtnum')) prtfam from dual
#
#DATA=@lblffzip~@lblsnbar~@lblcode~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@qty2~@vend~@from_name~@lblcasesort~@dummy~@lbltotweight~@dummy~@lblxofx~@dummy~@dummy~@dummt~@lblordnum~@lblitem~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblffadr3~@lbldescr~@dummy~@lblprtnum~@vc_slotno~
#
^XA^DFwilliams^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFROM:^FS;
^FO15,94^ADN,36,10^FN17^FS;
^FO15,134^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,174^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN39^FS;
^FO380,247^ADN,36,10^FN7^FS;
^FO670,247^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO16,300^ADN,100,40^FN3^FS;
^FO15,500^ADN,36,10^FDWilliams Sonoma PO Number^FS;
^BY2,,100^FO15,550^BCN,,N,N,N^FN8^FS;
^FO15,660^ADN,36,10^FN8^FS;
^FO15,712^ADN,36,10^FN16^FS;
^FO15,752^ADN,36,10^FN27^FS;
^FO15,498^GB785,0,2^FS;
^FO430,498^GB0,160,2^FS;
^FO15,792^ADN,36,10^FDDESC:^FS;
^FO80,792^ADN,36,10^FN40^FS;
^FO15,834^ADN,36,10^FN15^FS;
^FO450,660^ADN,36,10^FN22^FS;
^FO15,700^GB785,0,2^FS;
^FO450,500^ADN,36,10^FDMADE IN CHINA for^FS;
^FO450,540^ADN,36,10^FDWilliams Sonoma^FS:
^FO450,580^ADN,36,10^FDSan Francisco, CA 94109^FS;
^FO450,620^ADN,36,10^FN20^FS;
^FO430,658^GB0,216,2^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN18^FS;
^FO685,1560^ADN,54,15^FN19^FS;
^FO15,1560^ADN,54,15^FN42^FS;
^FO15,1414^ADN,54,15^FN9^FS;
^FO15,1474^ADN,54,15^FN28^FS;
^FO320,1414^ADN,54,15^FN26^FS;
^FO320,1474^ADN,54,15^FN28^FS;
^FO685,1560^ADN,54,15^FN43^FS;
^PQ1,0,0,N^XZ;
