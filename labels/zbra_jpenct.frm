#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
# # Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, var_shpdtl_view.deptno, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, ord.usr_vendor, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line, ord where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and var_shpord.ordnum=ord.ordnum and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@ffposc', 1,5) lblfrzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo,substr('@deptno', 1, 5) lbldeptno, substr('@cstprt', 1,3)||'-'||substr('@cstprt', 4, 25) lblitem, substr('@srcloc',1,15) lblsrcloc, substr(rtrim('@cstprt'), 8) lblcustsku, substr('@subucc', 1, 21) lblsnbar, substr('@cstprt', 7, 11) lblprtnum, substr('@stadd1',1,(instr('@stadd1','-', 1, 1) -1)) lblst1, substr('@stadd1', instr('@stadd1','-', 1, 1) + 1, 20) lblst2,  to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2, 'SUPP#: '||rtrim('@usr_vendor') lblvend, rtrim('@ffstcd') lblcity from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')'||' 07657 ' lbldesc1, '('||'91'||')' lbl_91,'@cstprt' catalog,'@pckqty' lblqty from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#SELECT=select '>;>8420'||'07657' topbar, '('||'91'||')' lbl_91, '@cstprt' lblcarrier from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select '>;>8910'||'@lblstcust' midbar from dual	
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode((select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum')),null,' ',(select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum'))) placecode1 from dual
#
#SELECT=select decode((select count(*) from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),1,(select lbmess from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),'@placecode1') placecode from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblitem~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstcust~@lbldesc1~@prtnum~@lblsrcloc~@lbl_91~@dummy~@lblqty~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@topbar~@midbar~@lbldeptno~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblfrzip~@lblcasesort~@placecode~@lblvend~@lblcity~@vc_slotno~@rplbl~@from_name~@print_date~
#
^XA^DFjpenct^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO330,14^ADN,36,10^FN24^FS;
^FO15,24^ADN,36,10^FN45^FS;
^FO15,64^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,104^ADN,36,10^FN16^FS;
^FO15,209^ADN,54,15^FN17^FS;
^FO170,104^ADN,36,10^FDU/C:^FS;
^FO225,104^ADN,36,10^FN22^FS;
^FO550,14^ADN,36,10^FN23^FS;
^FO275,26^GB0,227,2^FS;
^FO15,144,^ADN,36,10^FN21^FS;
^FO290,54^ADN,36,10^FDTO:^FS;
^FO340,54^ADN,70,13^FN3^FS;
^FO340,122^ADN,36,10^FN4^FS;
^FO340,168^ADN,36,10^FN5^FS;
^FO340,214^ADN,36,10^FN6^FS;
^FO555,214^ADN,36,10^FN38^FS;
^FO15,250^GB785,0,2^FS;
^FO15,260^ADN,18,10^FDSHIP TO POST^FS;
^FO185,275^ADN,36,10^FN15^FS;
^FO270,275^ADN,36,10^FN1^FS;
^FO490,250^GB0,250,2^FS;
^BY4,1.0,120^FO75,315^BCN,,N,N,N^FN25^FS;
^FO15,450^GB785,0,2^FS;
^FO510,260^ADN,45,12^FN41^FS;
^FO510,305^ADN,45,12^FDPO:^FS;
^FO585,305^ADN,45,12^FN7^FS;
^FO510,350^ADN,45,12^FDSUB:^FS;
^FO610,350^ADN,45,12^FN27^FS;
^FO510,395^ADN,45,12^FDHOME FURN/LEISURE^FS;
^FO530,600^ADN,100,30^FN14^FS;
^FO530,765^ADN,100,30^FN42^FS;
^FO490,500^GB0,373,2^FS;
^FO150,460^ADN,36,14^FDITEM NUMBER^FS;
^FO100,490^ADN,36,15^FN8^FS;
^FO560, 460^ADN,36,14^FDQUANTITY^FS;
^FO600, 490^ADN,36,15^FN20^FS;
^FO315, 490^ADN,36,15^FN34^FS;
^FO15,521^GB785,0,2^FS;
^FO15,550^ADN,18,10^FDFOR^FS;
^BY4,2.1,300^FO80,558^BCN,,N,N,N^FN26^FS;
^FO150,527^ADN,36,10^FN18^FS;
^FO230,527^ADN,36,10^FN14^FS;
^FO15,873^GB785,0,2^FS;
^FO191,888^ADN,36,10^FN9^FS;
^FO276,888^ADN,36,10^FN10^FS;
^FO331,888^ADN,36,10^FN11^FS;
^FO462,888^ADN,36,10^FN12^FS;
^FO629,888^ADN,36,10^FN13^FS;
^BY4,2.3,290^FO90,928^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN39^FS;
^FO15,1480^GB785,0,2^FS;
^FO380,1485^ADN,190,55^FN40^FS;
^FO685,1560^ADN,54,15^FN43^FS;
^FO15,1560^ADN,54,15^FDSLIPSHEET^FS;
^FO270,14^ADN,36,10^FN44^FS;
^FO455,1610^ADN,18,10^FN46^FS;
^PQ1,0,0,N^XZ;
