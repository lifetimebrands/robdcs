This directory holds the font and form definitions used to setup the Monarch Rf Guns.
The files are downloaded when the Monarch unit logs into its Unix account.  The files 
are suffixed with ".rfm" and are downloaded in the alpabetical order (as shown by the
Unix ls command).

