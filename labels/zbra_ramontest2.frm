#SELECT=select '@cponum' cponum from dual
#
#DATA=@cponum~
^XA^DFramontest2^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,100^ADN,74,25^FDFBA^FS;
^FO15,190^GB785,0,2^FS;
^FO15,210^ADN,36,10^FDSHIP FROM:^FS;
^FO15,250^ADN,36,10^FDMikasa Crystal^FS;
^FO15,290^ADN,36,10^FD12 APPLEGATE DR^FS;
^FO15,330^ADN,36,10^FD12 APPLEGATE DR^FS;
^FO15,370^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,410^ADN,36,10^FDUnited States^FS;
^FO330,210^ADN,54,10^FDSHIP TO:^FS;
^FO330,270^ADN,54,10^FDAmazon.com^FS;
^FO330,330^ADN,54,10^FD705 Boulder Drive^FS;
^FO330,390^ADN,54,10^FDBreinigsville, PA 18031^FS;
^FO330,450^ADN,54,10^FDUnited States^FS;
^PQ1,0,0,N^XZ;
