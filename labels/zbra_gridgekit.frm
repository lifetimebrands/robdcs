#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, pckwrk.untcas, ord_line.vc_lblfield_string_1 upc, ord_line.vc_lblfield_string_2 prt from var_shpord, var_shpdtl_view, pckwrk, ord_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and ord_line.client_id='----' if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno, pckwrk.untcas, ord_line.vc_lblfield_string_1 upc, ord_line.vc_lblfield_string_2 prt from var_shpord, var_shpdtl_view, pckwrk, ord_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id  and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and ord_line.client_id='----' if (@InWrkref > ' ' and @wrktyp = 'K')
#
#SELECT=select substr(lngdsc,1,30) lbldescription from prtdsc, prtmst, pckwrk where prtdsc.colval = prtmst.prtnum||'|----' and prtdsc.colnam= 'prtnum|prt_client_id' and prtdsc.locale_id ='US_ENGLISH' and pckwrk.prtnum=prtmst.prtnum and pckwrk.wrkref='@wrkref' union select ' ' lbldescription from pckwrk where prtnum='KITPART' and wrkref='@wrkref'
#
#Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, substr('@ffadd3',1,30) lblffadr3, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stadd3',1,30) lblstadr3, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem2, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 16) lblprtnum, 'QTY: '||decode((select prtnum from pckwrk where wrkref='@wrkref'),'KITPART',' ',to_char(@pckqty, 'fm9999')) lblpckqty2, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, substr('@stcust',8) lblcustnum, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select logo_name from ord where ordnum='@ordnum'
#
#SELECT=select 'FFC#' lblffcust, 'Carrier: '||'@carcod' lblstcust, '('||'420'||')' lbldesc, to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx, 'Dept: '||'@deptno' lbldept from dual
#
#SELECT=select case when '@prtnum'='KITPART' then '@upc' else (select upccod from prtmst where prtnum='@prtnum') end lblupc2 from dual
#
#SELECT=select decode('@logo_name','FREDx',null,'@lblupc2') lblupc, decode('@logo_name','FREDx',null,'@prt') lblitem, decode('@logo_name','FREDx',null,'UPC: ') upctxt, decode('@logo_name','FREDx',null,'At Home SKU: ') skutxt, decode('@logo_name','FREDx',null,'@lblpckqty2') lblpckqty, decode('@logo_name','FREDx',null,'@prtnum') ltbprt from dual
#
#SELECT=select case when '@prtnum'!='KITPART' then null else decode('@logo_name','FREDx',null,null) end upcmessage from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select nvl((select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and arecod in (select arecod from aremst where stgflg=1) and rownum < 2),' ') lbldestloc from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode('@logo_name','FREDx',null,'Vendor SKU: '||'@prtnum') vendpart, decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#SELECT=select decode((select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum')),null,' ',(select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum'))) placecode1 from dual
#
#SELECT=select decode((select count(*) from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),1,(select lbmess from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),'@placecode1') placecode from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=1),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=1)) subitem1 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=2),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=2)) subitem2 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=3),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=3)) subitem3 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=4),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=4)) subitem4 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=5),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=5)) subitem5 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=6),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=6)) subitem6 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=7),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=7)) subitem7 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=8),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=8)) subitem8 from dual
#
#SELECT=select decode((select count(*) from (select p2.prtnum, rownum rtest from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.ctnnum) where rtest=9),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=9)) subitem9 from dual
#
#SELECT=select decode((select count(*) from (select 'x' from pckwrk p1, pckwrk p2 where p1.wrkref='@wrkref' and p1.subnum=p2.subnum) where rownum=10),0,null,(select subitem from (select rownum row_num, 'SKU: '||ol.cstprt||' QTY: '||pw.pckqty subitem from pckwrk p, pckwrk pw, ord_line ol where p.wrkref='@wrkref' and p.subnum=pw.ctnnum and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' order by pw.prtnum) where row_num=10)) subitem10 from dual
#
#SELECT=select 'Total: '||sum(pw.pckqty) subtotal from pckwrk p, pckwrk pw where p.wrkref='@wrkref' and p.subnum=pw.ctnnum
#
#DATA=@lblffzip~@lblsnbar~@ltbprt2~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@vendpart~@upctxt~@skutxt~@dummy~@dummy~@lblstcust~@dummy~@lblxofx~@lbldept~@lbldestloc~@lblpckqty~@lblordnum~@lblitem~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lblffadr3~@dummy~@lblcustnum~@lblupc~@dummy~@dummy~@dummy~@dummy~@dummy~@from_name~@dummy~@dummy~@upcmessage~@subitem1~@subitem2~@subitem3~@subitem4~@subitem5~@subtotal~@subitem6~@subitem7~@subitem8~@subitem9~@subitem10~
#
^XA^DFgridgekit^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,174^ADN,36,10^FN28^FS;
^FO15,54^ADN,36,10^FN48^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,214^ADN,36,10^FN9^FS;
^FO170,214^ADN,36,10^FN24^FS;
^FO15,840^ADN,36,10^FN25^FS;
^FO170,174^ADN,36,10^FN26^FS;
^FO320,47^GB0,232,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN39^FS;
^FO380,247^ADN,36,10^FN7^FS;
^FO700,247^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FN16^FS;
^BY5,,290^FO15,320^BUN,,N,N,Y^FN42^FS;
^FO15,320^ADN,54,14^FN51^FS;
^FO525,278^GB0,221,2^FS;
^FO535,300^ADN,36,10^FDPO#:^FS;
^FO535,340^ADN,54,15^FN8^FS;
^FO535,400^ADN,36,10^FN23^FS;
^FO15,255^ADN,26,10^FN38^FS;
^FO185,255^ADN,26,10^FN29^FS;
^FO525,498^GB350,0,2^FS;
^FO525,498^GB0,160,2^FS;
^FO15,665^ADN,36,10^FDCARTON:^FS;
^FO535,505^ADN,54,15^FDSTORE^FS;
^FO550,560^ADN,74,35^FN41^FS;
^FO15,705^ADN,74,25^FN22^FS;
^FO15,780^ADN,54,14^FDDO NOT CUT WITH SHARP OBJECT^FS;
^FO130,840^ADN,36,10^FN15^FS;
^FO15,658^GB785,0,2^FS;
^FO535,440^ADN,36,10^FN20^FS;
^FO450,658^GB0,216,2^FS;
^FO455,678^ADN,36,10^FN16^FS;
^FO455,718^ADN,54,14^FN42^FS;
^FO455,778^ADN,36,10^FN17^FS;   
^FO455,818^ADN,54,14^FN27^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,54,10^FN52^FS;
^FO15,1344^ADN,54,10^FN53^FS;
^FO15,1404^ADN,54,10^FN54^FS;
^FO15,1464^ADN,54,10^FN55^FS;
^FO15,1524^ADN,54,10^FN56^FS;
^FO400,1284^ADN,54,10^FN58^FS;
^FO400,1344^ADN,54,10^FN59^FS;
^FO400,1404^ADN,54,10^FN60^FS;
^FO400,1464^ADN,54,10^FN61^FS;
^FO400,1524^ADN,54,10^FN62^FS;
^FO500,1584^ADN,36,15^FN57^FS;
^FO455,1610^ADN,18,10^FN49^FS;
^PQ1,0,0,N^XZ;
