#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#SELECT=select upccod from prtmst where prtnum='@prtnum' if (@prtnum)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
# (10/27/1999) EXCEPT FOR WRKTYP KITPART, FOR WHICH WE READ ONLY
# PICK WORK (PCKWRK) AND SHIP HEADER (SHPHDR) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.cstprt, var_shpdtl_view.deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, ord_line.vc_lblfield_string_2, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line, ord_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and pckwrk.ordsln=ord_line.ordsln and ord_line.client_id='----' if (@InWrkref > ' ' and @wrktyp != 'R' and @wrktyp != 'K')
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, null cstprt, null deptno, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.pckqty, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, ord_line.vc_lblfield_string_2, pckwrk.vc_slotno from var_shpord, pckwrk, shipment_line, ord_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and pckwrk.ordsln=ord_line.ordsln and ord_line.client_id='----' if (@InWrkref > ' ' and @wrktyp = 'K')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, substr('@stcity', 1, 17)||','||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, rtrim('@cponum') lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@pckqty, 'fm9999') lblpckqty, substr('@upccod', 1, 11) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2, substr('@vc_lblfield_string_2',1,length('@vc_lblfield_string_2')-5) store_name from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '@carcod' lblstcust, ' ' lbldesc, to_char('@cur_cas', 'fm999999') lblxofx from dual
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select sum(nummin) nummin from (select case when (sum(b.pckqty)/b.untpak) like '%.%' then sum(b.pckqty) else (sum(b.pckqty)/b.untpak) end nummin from pckwrk a, pckwrk b where a.wrkref='@wrkref' and a.subnum=b.ctnnum group by b.untpak)
#
#SELECT=select substr('@store_name',1,22) store_name2 from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode(pckwrk.prtnum,'KITPART',' ',ord_line.cstprt) cust_part, pckwrk.pckqty pck_amt from pckwrk, ord_line where pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and pckwrk.ordsln=ord_line.ordsln and ord_line.client_id='----' and pckwrk.wrkref= '@wrkref'
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,'See Packing List',(select substr(lngdsc, 1, 20)lbldesc from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select decode('@prtnum','KITPART',' ',rpad('@cust_part',15)||rpad('@lbldescr',25)||'@pck_amt') item_info, decode('@prtnum','KITPART','See Packing List',' ') item_info_2 from dual
#SELECT=select substr(stcust,-4) store_number from ord where ordnum='@ordnum'
#
#SELECT=select rtstr1 lbldept from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='MENARD'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@prtnum~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsrcloc~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@lbldesc~@dummy~@lbldept~@lbldestloc~@lblpckqty~@lblordnum~@dummy~@ship_id~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@dummy~@store_number~@dummy~@dummy~@lblcasesort~@item_info~@store_name2~@item_info_2~@nummin~@vc_slotno~@rplbl~@from_name~@print_date~@lblmessage~
#
^XA^DFmaynard^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,14^ADN,36,10^FDFROM:^FS;
^FO270,14^ADN,36,10^FN46^FS;
^FO115,14^ADN,36,10^FN28^FS;
^FO15,54^ADN,36,10^FN47^FS;
^FO15,94^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,174^ADN,36,10^FDSKU:^FS;
^FO70,174^ADN,36,10^FN3^FS;
^FO15,209^ADN,36,10^FDLOC:^FS;
^FO70,209^ADN,36,10^FN9^FS;
^FO170,209^ADN,36,10^FN24^FS;
^FO15,244^ADN,36,10^FDQTY:^FS;
^FO70,244^ADN,36,10^FN25^FS;
^FO170,244^ADN,36,10^FN26^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN4^FS;
^FO380,122^ADN,36,10^FN5^FS;
^FO380,168^ADN,36,10^FN6^FS;
^FO380,214^ADN,36,10^FN7^FS;
^FO700,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO550,278^GB0,221,2^FS;
^FO15,498^GB785,0,2^FS;
^FO550,498^GB0,160,2^FS;
^FO575,302^ADN,36,10^FDDept:^FS;
^FO575,342^ADN,100,50^FN23^FS;
^FO575,530^ADN,36,10^FDCarton Count:^FS;
^FO590,570^ADN,50,30^FN44^FS;
^FO15,658^GB785,0,2^FS;
^FO15,510^ADN,18,10^FDStore Name:^FS;
^FO15,570^ADN,54,15^FN42^FS;
^FO15,670^ADN,18,10^FDStore^FS;
^FO15,710^ADN,120,50^FN37^FS;
^FO320,670^ADN,18,10^FDVendor Name^FS;
^FO320,690^ADN,54,15^FDLifetime Brands^FS;
^FO310,750^GB490,0,2^FS;
^FO320,760^ADN,18,10^FDVendor Number^FS;
^FO320,790^ADN,70,40^FDLIFET006^FS;
^FO15,302^ADN,18,10^FDPO Number:^FS;
^FO170,285^ADN,54,15^FN8^FS;
^FO15,340^ADN,18,10^FDSKU^FS;
^FO160,340^ADN,18,10^FDDESCRIPTION^FS;
^FO450,340^ADN,18,10^FDQTY^FS;
^FO15,360^ADN,18,10^FN41^FS;
^FO15,360^ADN,36,10^FN43^FS;
^FO310,658^GB0,216,2^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,70,30^FN40^FS;
^FO15,1514^ADN,40,30^FDPrint Carton Content^FS;
^FO685,1560^ADN,54,15^FN45^FS;
^FO15,1384^ADN,54,15^FN9^FS;
^FO15,1444^ADN,54,15^FN3^FS;
^FO15,1560^ADN,54,15^FN49^FS;
^FO455,1610^ADN,18,10^FN48^FS;
^PQ1,0,0,N^XZ;
