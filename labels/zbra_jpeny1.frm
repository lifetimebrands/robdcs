#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblmffdr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, 'STC#' lblstcust, '('||'420'||')' lbldesc1 from dual
#
#
#SELECT=select 'UPS' lblcarcod from dual if (@carcod = 'UPS')
#
#SELECT=select 'RPS' lblcarcod from dual if (@carcod = 'RPS')
#
#DATA=@lblffzip~@lblsnbar~@lblffcust~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblitem~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblstcust~@prtnum~@lblsrcloc~@lblcarcod~
#
^XA^DFjpeny1^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^FO15,54^ADN,36,10^FDFROM:^FS;
^FO15,94^ADN,36,10^FDLIFETIME HOAN.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,174^ADN,36,10^FDSKU:^FS;
^FO70,174^ADN,36,10^FN21^FS;
^FO15,214^ADN,36,10^FDLOC:^FS;
^FO70,214^ADN,36,10^FN22^FS;
^FO250,214,^ADN,36,10^FN23^FS; 
^FO15,298^ADN,36,10^FDTO:^FS;
^FO90,298^ADN,36,10^FN4^FS;
^FO90,340^ADN,36,10^FN5^FS;
^FO90,380^ADN,36,10^FN6^FS;
^FO90,420^ADN,36,10^FN7^FS;
^FO621,420^ADN,36,10^FN1^FS;
^F015,278^GB785,0,2^FS;
^FX100,590^ADN,36,15^FN3^;
^FO15,498^GB785,0,2^FS;
^FX100,530^ADN,36,15^FN8^;
^FO100,590^ADN,36,15^FN9^FS;
^FO15,718^GB785,0,2^FS;
^FX120,718^ADN,54,15^FN20^;
^FO15,520^ADN,36,10^FDFOR:^FS;
^FO90,520^ADN,36,10^FN15^FS;
^FO90,560^ADN,36,10^FN16^FS;
^FO90,600^ADN,36,10^FN17^FS;
^FO90,640^ADN,36,10^FN18^FS;
^FO621,640^ADN,36,10^FN19^FS;
^FO15,725^ADN,36,10^FDSUB:^FS;
^FO50,765^ADN,36,10^FDSUB^FS;
^FO95,805^ADN,36,10^FDRETAIL^FS;
^FO50,875^ADN,36,10^FDSUB^FS; 
^FO80,915^ADN,36,10^FDCATALOG^FS;
^FO170,718^GB0,230,2^FS;
^FO175,725^ADN,36,10^FDLOT NO:^FS;
^FO215,765^ADN,36,10^FDLOT^FS;
^FO335,805^ADN,36,10^FDRETAIL^FS;
^FO215,875^ADN,36,10^FDLOT^FS;
^FO320,915^ADN,36,10^FDCATALOG^FS;
^FO410,718^GB0,230,2^FS;
^FO415,725^ADN,36,10^FDLINE:^FS;
^FO465,765^ADN,36,10^FDLINE^FS;
^FO545,805^ADN,36,10^FDRETAIL^FS;
^FO465,875^ADN,36,10^FDLINE^FS;
^FO530,915^ADN,36,10^FDCATALOG^FS;
^FO620,718^GB0,230,2^FS;
^FO625,725^ADN,36,10^FDSKU:^FS;
^FO675,765^ADN,36,10^FDSKU^FS;
^FO725,805^ADN,36,10^FDRETAIL^FS;
^FO675,875^ADN,36,10^FDSKU^FS;
^FO710,915^ADN,36,10^FDCATALOG^FS;
^FO15,838^GB785,0,2^FS;
^FO370,948^GB0,230,2^FS;
^FO580,948^GB0,230,2^FS;
^FO15,960^ADN,36,10^FDPURCHASE ORDER^FS;
^FO50,1000^ADN,36,10^FDPURCHASE ORDER^FS;
^FO380,960^ADN,36,10^FDPACK QUANTITY^FS;
^FO480,1000^ADN,36,10^FD6^FS;
^FO585,960^ADN,36,10^FDPREPACK CODE:^FS;
^FO635,1000^ADN,36,10^FDCODE^FS;
^FO15,1070^ADN,36,10^FDCOUNTRY OF ORIGIN^FS;
^FO50,1110^ADN,36,10^FDCOUNTRY^FS;
^FO380,1070^ADN,36,10^FDCASE NO:^FS;
^FO480,1110^ADN,36,10^FDCASE#^FS;
^FO585,1070^ADN,36,10^FDCOLOR^FS;
^FO635,1110^ADN,36,10^FDCOLOR^FS;
^FO15,948^GB785,0,2^FS;
^FO15,1058^GB785,0,4^FS;
^FO15,1178^GB785,0,3^FS;
^PQ1,0,0,N^XZ;
