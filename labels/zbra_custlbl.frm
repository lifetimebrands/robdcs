#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH12MISS PM') prtdte, ship_id, wrktyp, subnum, ordnum, subucc, uc_pck_seqnum, srcloc, cmbcod, vc_slotno, 'Case '||cur_cas||' of '||tot_cas_cnt xofx, prtnum from pckwrk where wrkref='@wrkref'
#
#SELECT=select cponum from ord where ordnum='@ordnum'
#
#SELECT=select stoloc dstloc from pckmov where cmbcod='@cmbcod' and seqnum=(select max(seqnum) from pckmov where cmbcod='@cmbcod')
#
#SELECT=select '# of Items: '||decode('@wrktyp','K',(select count(distinct prtnum) from pckwrk where ctnnum='@subnum'),1) itmcnt from dual
#
#SELECT=select 'Total Qty: '||decode('@wrktyp','K',(select sum(pckqty) from pckwrk where ctnnum='@subnum'),(select pckqty from pckwrk where subucc='@subucc')) totpck from dual
#
#SELECT=select  substr('@subucc', 1, 21) lblsnbar, '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5, decode(substr('@uc_pck_seqnum',1,7),'LANE039','LANE39'||substr('@uc_pck_seqnum',instr('@uc_pck_seqnum','-'),9),'@uc_pck_seqnum') lblcasesort2, substr('@prtnum',1,30) lblprt from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@prtdte~@cponum~@itmcnt~@totpck~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblsnbar~@lblcasesort~@srcloc~@dstloc~@vc_slotno~@xofx~@lblprt~@ship_id~@rplbl~
#
^XA^DFcustlbl^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO20,50^ADN,54,15^FN1^FS;
^FO20,110^ADN,54,15^FDPO#:^FS;
^FO20,170^ADN,130,35^FN2^FS;
^FO20,300^ADN,54,15^FN15^FS;
^FO20,360^ADN,54,15^FN3^FS;
^FO20,430^ADN,54,15^FN4^FS;
^FO20,500^ADN,54,15^FDPicked By:^FS;
^FO15,670^GB785,0,2^FS;
^FO20,700^ADN,54,15^FDPacked By:^FS;
^FO15,870^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN5^FS;
^FO276,920^ADN,36,10^FN6^FS;
^FO331,920^ADN,36,10^FN7^FS;
^FO462,920^ADN,36,10^FN8^FS;
^FO629,920^ADN,36,10^FN9^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN10^FS;
^FO15,1284^ADN,130,35^FN11^FS;
^FO20,1424^ADN,54,15^FN12^FS;
^FO400,1424^ADN,54,16^FN16^FS;
^FO20,1494^ADN,54,15^FN13^FS;
^FO400,1494^ADN,54,15^FN17^FS;
^FO20,1560^ADN,54,15^FN15^FS;
^FO685,1560^ADN,54,15^FN14^FS;
^FO270,14^ADN,36,10^FN18^FS;
^PQ1,0,0,N^XZ;
