#
# MaKing mask for tracking number to be UPS compliant
#SELECT=select ' ' FmtTraknm from dual
#SELECT=select ' ' SavTraknm from dual
#SELECT=select '@traknm' SavTraknm from dual
#SELECT=select distinct var_shpord.ship_id, var_shpord.ordnum, var_shpord.shpseq, var_shpord.subsid from invdtl, var_shpord, var_shpdtl_view where '@subnum' = invdtl.subnum and var_shpord.ship_id = var_shpdtl_view.ship_id and var_shpord.shpseq = var_shpdtl_view.shpseq and var_shpord.ordnum = var_shpdtl_view.ordnum and var_shpord.subsid = var_shpdtl_view.subsid and invdtl.ship_line_id = var_shpdtl_view.ship_line_id
#SELECT=select substr('@SavTraknm',1,2) || ' ' || substr('@SavTraknm',3,3) || ' ' || substr('@SavTraknm',6,3) || ' ' || substr('@SavTraknm',9,2) || ' ' || substr('@SavTraknm',11,4) || ' ' || substr('@SavTraknm',15,3) || substr('@SavTraknm',18,1) FmtTraknm from dual
#
# Initialize SavZip so we can save off just the first 5 chars of the zip code
#SELECT=select ' ' SavZip from dual
# upstxt always 'TRACKING NUMBER', only straight ground service uses
# 'IDENTIFICATION NUMBER' which we aren't using
#SELECT=select 'TRACKING NUMBER' upstxt from dual
#
# Default string to error message.  If no valid service level found
# error string will appear on label
#
#SELECT=select DECODE(RTRIM(var_shpord.carcod),'UPSN','UPS NEXT DAY AIR','UPS2','UPS 2ND DAY AIR','UPS3', 'UPS 3 DAY SELECT', 'UPSG','UPS GROUND','UPSS','UPS GROUND','Invalid Service') SavSrvlvl, DECODE(RTRIM(var_shpord.carcod),'UPSN','1','UPS2','2','UPS3','3','UPSG','','UPSS','','?') SavSrvcod, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stadd3, var_shpord.stcity, var_shpord.ststcd, RPAD(var_shpord.stposc,5) SavZip, var_shpord.cponum, var_shpord.ordnum, var_shpord.ship_id, var_shpord.shpseq, var_shpord.subsid from var_shpord where var_shpord.ordnum = '@ordnum' and var_shpord.ship_id= '@ship_id'
#
#SELECT=select substr('@stname', 1, 27) stname, RTRIM('@stcity') || ' ' || RTRIM('@ststcd') || '  ' || RTRIM('@SavZip') SavAddress from dual
#
#SELECT=select RTRIM('@ordnum')||' ('||RTRIM('@ship_id')||') ' SavOrdShp from dual
#
#SELECT=select ' ' SavNoteLn1, ' ' SaveNoteLn2 from dual
#SELECT=select count(*) TmpLincnt from shphnt where ship_id = '@ship_id' and shpseq = '@shpseq' and subsid = '@subsid' and ordnum = '@ordnum' and notlin = '0001'
#SELECT=select nottxt SavNoteLn1 from shphnt where ship_id = '@ship_id' and shpseq = '@shpseq' and subsid = '@subsid' and ordnum = '@ordnum' and notlin = '0001' if (@TmpLincnt > 0)
#SELECT=select count(*) TmpLincnt from shphnt where ship_id = '@ship_id' and shpseq = '@shpseq' and subsid = '@subsid' and ordnum = '@ordnum' and notlin = '0002'
#SELECT=select nottxt SavNoteLn2 from shphnt where ship_id = '@ship_id' and shpseq = '@shpseq' and subsid = '@subsid' and ordnum = '@ordnum' and notlin = '0002' if (@TmpLincnt > 0)
#
# The data statement is a list of the fields to be printed separated by
# tildens.  The rest of the file contains label formatting data.
#
#DATA=@stname~@stadd1~@stadd2~@stadd3~@SavAddress~@SavSrvlvl~@upstxt~@FmtTraknm~@SavTraknm~@cponum~@SavOrdShp~@SavSrvcod~@SavNoteLn1~@SavNoteLn2~
^XA^DFupslbl^FS;
^LH0,30;
^FWN;
^FO120,120^CFD,28,18^FN1^FS;
^FO120,160^CFD,28,18^FN2^FS;
^FO120,200^CFD,28,18^FN3^FS;
^FO120,240^CFD,28,18^FN4^FS;
^FO120,290^CFD,28,18^FN5^FS;
^BY3,2.0^FS;
^FO14,400^CFD,55,25^FN6^FS;
^FO14,465^CFD,40,25^FN7^FS;
^FO130,580^CFD,27,22^FN8^FS;
^BY3,2.0^FS;
^FO55,630^BCN,200,N,N^FN9^FS;
^FO10,958^CFD,27,15^FN10^FS;
^FO10,1050^CFD,27,15^FN11^FS;
^BY4,2.0^FS;
^FO4,366^GB800,0,20^FS;
^FO662,366^GB150,150,150^FS;
^FO4,515^GB800,0,2^FS;
^FO4,886^GB800,0,20^FS;
^FX == END VARIABLE DECLARATIONS ==;
^FO680,400^CFD,120,120^FR^FN12^FS;
^FO19,10^CFD,20,12^FDLIFETIME HOAN CORP.^FS;
^FO19,40^CFD,20,12^FD12 APPLEGATE DRIVE^FS;
^FO19,70^CFD,20,12^FDROBBINSVILLE, NJ 08691^FS;
^FO14,120^CFD,30,20^FDSHIP^FS;
^FO45,160^CFD,30,20^FDTO:^FS;
^FO10,918^CFD,27,15^FDCUSTOMER PO# :^FS;
^FO10,1010^CFD,27,15^FDORDER NUMBER :^FS;
^FO400,1100^CFD,27,14^FN13^FS;
^FO400,1130^CFD,27,14^FN14^FS;
^XZ;
