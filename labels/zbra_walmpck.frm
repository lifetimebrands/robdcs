#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
# # Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpord.pronum, var_shpord.waybil, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, var_shpdtl_view.deptno, ord_line.vc_lblfield_string_1, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, ord_line, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ordnum = ord_line.ordnum and pckwrk.ordlin = ord_line.ordlin and pckwrk.ordsln = ord_line.ordsln and ord_line.client_id='----' and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, 'CUSTOMER: '||substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@lblffposc', 1, 5)lblffzip, substr('@pronum', 1, 10) lblpronum, substr('@waybil', 7, 7) lblway, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, 'SCAC Code: '||substr('@carcod', 1, 20) lblcarrier, 'WM.com PO#: '||substr('@cponum', 1, 14) lblcustpo,substr('@deptno', 1, 5) lbldeptno, substr('@cstprt', 1, 3)||'-'||substr('@cstprt', 4, 4) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 8) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, ('@vc_lblfield_string_1') lbldiv, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name, 'Processing Date: '||to_char(cpodte,'MM/DD/YYYY') pros_dte from ord where ordnum='@ordnum'
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select '('||'420'||')'||' @lblstzip' lbldesc1, '('||'91'||')' lbl_91,'@cstprt' catalog,'@pckqty' lblqty from dual
#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#SELECT=select '>;>8420'||'@lblstzip' topbar, '('||'91'||')' lbl_91 from dual
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select '>;>8910'||'@lblstcust' midbar from dual
#SELECT=select substr('@cstprt', 8, 4)lblcstprt from dual
#SELECT=select substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust from dual
#SELECT=select min('@pronum') lblpronum from dual
#SELECT=select min(substr('@waybil', 7, 7))lblway from dual
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,'MIXED',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#SELECT=select '@prtnum' wm_part, 'QTY: '||'@lbluntcas' wm_cas, 'Dest: '||'@lbldestloc' wm_dest, 'Ship ID: '||'@ship_id' wm_ship_id, 'Order: '||'@ordnum' wm_ord from dual
#
#SELECT=select '@prtnum'||' U/C: '||'@lbluntcas' prtcas, '@lblsrcloc'||'  '||'@lbldestloc' lblloc, '@lblsrcloc' wm_src, '@ship_id'||' '||'@ordnum' sidord from dual
#
#DATA=@lblsnbar~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblcasesort~@vc_slotno~@dummy~@dummy~@print_date~@dummy~@prtcas~@lblloc~@sidord~@wm_part~@dummy~@wm_src~@wm_dest~@wm_ship_id~@wm_ord~ 
#
^XA^DFwalmpck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,873^GB785,0,2^FS;
^FO15,35^ADN,54,15^FDWALMART PICK LABEL^FS;
^FO15,95^ADN,54,15^FN20^FS;
^FO15,155^ADN,54,15^FN21^FS;
^FO15,215^ADN,54,15^FN1^FS;
^FO15,335^ADN,54,15^FDSOURCE LOC:^FS;
^FO15,395^ADN,130,35^FN18^FS;
^FO15,535^ADN,54,15^FDITEM:^FS;
^FO15,595^ADN,130,35^FN16^FS;
^FO15,275^ADN,54,15^FN19^FS;
^FO191,920^ADN,36,10^FN2^FS;
^FO276,920^ADN,36,10^FN3^FS;
^FO331,920^ADN,36,10^FN4^FS;
^FO462,920^ADN,36,10^FN5^FS;
^FO629,920^ADN,36,10^FN6^FS;
^BY4,2.3,255^FO90,963^BCN,,N,N,Y,U^FN1^FS;
^FO15,1284^ADN,130,35^FN7^FS;
^FO685,1560^ADN,54,15^FN8^FS;
^FO15,1414^ADN,54,15^FN13^FS;
^FO15,1474^ADN,54,15^FN14^FS;
^FO15,1534^ADN,54,15^FN15^FS;
^FO15,1589^ADN,54,15^FN1^FS;
^FO505,1610^ADN,18,10^FN11^FS;
^PQ1,0,0,N^XZ;
