#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpdtl_view.deptno lbldept, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, shipment_line.vc_wave_lane, pckwrk.uc_pck_seqnum uc_seqnum, pckwrk.vc_slotno from var_shpord, var_shpdtl_view, pckwrk, shipment_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ship_id=shipment_line.ship_id and pckwrk.ordnum=shipment_line.ordnum and pckwrk.ordlin=shipment_line.ordlin and pckwrk.ordsln=shipment_line.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 15) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort2 from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select decode('@uc_seqnum',null,' ',(select rtstr2 from poldat where polcod='USR' and polvar='LANE-SLOT-XREF' and polval='XREF' and rtstr1='@uc_seqnum')) lblcasesort  from dual
#
#SELECT=select '('||'420'||')' lbldesc1, '('||'91'||')' lbl_91, '@carcod' lblcarrier2 from dual
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select substr(carnam,1,10) lblcarrier from carhdr where carcod='@carcod'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select 'C'||to_char(cpodte, 'MMDDYY') cpodte, 'S'||to_char(entdte, 'MMDDYY') entdte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select decode('@prtnum','KITPART','GRP: ',(select 'GRP: '||vc_lblfield_string_1 lblgrp from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@prtnum')) lblgrp from dual
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode((select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum')),null,' ',(select rtstr2 placecode from poldat where polcod='USR' and polvar='LABEL-PLACEMENT'and polval='BTCUST' and rtstr1=(select btcust from ord where ordnum='@ordnum'))) placecode1 from dual
#
#SELECT=select decode((select count(*) from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),1,(select lbmess from usr_message_override where prtnum='@prtnum' and btcust=(select btcust from ord where ordnum='@ordnum')),'@placecode1') placecode from dual
#
#SELECT=select decode((select count(distinct ackdte) from var_lblseq where wrkref='@wrkref'),0,' ',1,' ','RP') rplbl from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lbldept~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstcust~@lbldesc1~@prtnum~@lblsrcloc~@lbl_91~@lblcarrier~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@dummy~@dummy~@lblcasesort~@placecode~@vc_slotno~@rplbl~@from_name~@print_date~@lblgrp~
#
^XA^DFsaks^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,144^ADN,36,10^FN23^FS;
^FO330,14^ADN,36,10^FN29^FS;
^FO15,24^ADN,36,10^FN30^FS;
^FO15,64^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,104^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO420,560^ADN,36,10^FDQTY:^FS;
^FO475,560^ADN,36,10^FN21^FS;
^FO170,144^ADN,36,10^FN22^FS;
^FO15,184,^ADN,36,10^FN20^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO100,322^ADN,36,10^FN15^FS;
^FO185,322^ADN,36,10^FN1^FS;
^FO360,278^GB0,223,2^FS;
^BY3,,122^FO50,360^BCN,,N,N,N^FN1^FS;
^FO15,498^GB785,0,2^FS;
^FO15,510^ADN,36,10^FDPO#:^FS;
^FO80,510^ADN,36,15^FN7^FS;
^FO420,510^ADN,36,10^FDSKU:^FS;
^FO500,510^ADN,36,10^FN16^FS;
^FO15,560^ADN,36,10^FDDEPT:^FS;
^FO80,560^ADN,36,15^FN8^FS;
^FO15,600^ADN,36,10^FN32^FS;
^FO15,658^GB785,0,2^FS;
^FO430,668^ADN,36,10^FDSTORE #^FS;
^FO430,718^ADN,54,15^FN14^FS;
^FO420,658^GB0,216,2^FS;
^BY3,,120^FO50,730^BCN,,N,N,N^FN14^FS;
^FO100,692^ADN,36,10^FN18^FS;
^FO185,692^ADN,36,10^FN14^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO380,298^ADN,36,10^FDCARRIER:^FS;
^FO420,340^ADN,36,15^FN19^FS;
^FO380,400^ADN,36,10^FDBOL: ^FS;
^FO15,1284^ADN,130,35^FN26^FS;
^FO380,1420^ADN,190,55^FN27^FS;
^FO685,1560^ADN,54,15^FN28^FS;
^FO15,1414^ADN,54,15^FN17^FS;
^FO15,1474^ADN,54,15^FN16^FS;
^FO455,1610^ADN,18,10^FN31^FS;
^PQ1,0,0,N^XZ;
