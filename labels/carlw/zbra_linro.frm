#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, pckwrk.tot_cas_cnt, var_shpdtl_view.deptno, var_shpdtl_view.cstprt, pckwrk.cur_cas, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 8) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select 'FFC#' lblffcust, '' lbldesc, to_char('@cur_cas', 'fm9999')||' of '||to_char('@tot_cas_cnt', 'fm9999') lblxofx, '@deptno' lbldept, '('||'420'||')' lbldesc1, '' lbldesc2 from dual
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstname~@lblstadr1~@lblstadr2~@lblstcstz~@lblstzip~@lblstcust~@lbldesc~@lblxofx~@lbldept~@lbldesc1~@lbldesc2~@prtnum~@lblsrcloc~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@cpodte~
#
^XA^DFlinens^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^FO15,54^ADN,36,10^FDFROM:^FS;
^FO115,54^ADN,36,10^FN29^FS;
^FO15,94^ADN,36,10^FDLIFETIME HOAN CORP.^FS;
^FO15,134^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,174^ADN,36,10^FDSKU:^FS;
^FO70,174^ADN,36,10^FN24^FS;
^FO15,209^ADN,36,10^FDLOC:^FS;
^FO70,209^ADN,36,10^FN25^FS;
^FO15,244^ADN,36,10^FDU/C:^FS;
^FO70,244^ADN,36,10^FN27^FS;
^FO170,244^ADN,36,10^FN28^FS;
^FO170,209,^ADN,36,10^FN26^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO15,298^ADN,18,10^FDSHIP TO POSTAL CODE:^FS;
^FO145,322^ADN,36,10^FN22^FS;
^FO230,322^ADN,36,10^FN1^FS;
^BY3,,122^FO90,360^BCN,,N,N,N^FN1^FS;
^FO450,278^GB0,221,2^FS;
^FO470,300^ADN,36,10^FDPO#:^FS;
^FO550,300^ADN,36,14^FN7^FS;
^FO470,350^ADN,36,10^FDDEPT:^FS;
^FO550,350^ADN,36,14^FN21^FS;
^FO470,400^ADN,36,10^FDDESC:^FS;
^FO500,450^ADN,36,10^FN19^FS;
^FO675,465^ADN,36,10^FN30^FS;
^FO15,498^GB785,0,2^FS;
^FO15,540^ADN,36,10^FDCARTON:^FS;
^FO150,540^ADN,36,15^FN20^FS;
^FO15,600^ADN,36,10^FN23^FS;
^FO15,658^GB785,0,2^FS;
^FO15,678^ADN,18,10^FDSTORE:^FS;
^FO30,718^ADN,54,15^FN18^FS;
^FO320,658^GB0,216,2^FS;
^FO330,678^ADN,18,10^FDFOR:^FS;
^FO330,708^ADN,36,10^FN13^FS;
^FO330,750^ADN,36,10^FN14^FS;
^FO330,792^ADN,36,10^FN15^FS;
^FO330,834^ADN,36,10^FN16^FS;
^FO621,834^ADN,36,10^FN17^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN8^FS;
^FO276,920^ADN,36,10^FN9^FS;
^FO331,920^ADN,36,10^FN10^FS;
^FO462,920^ADN,36,10^FN11^FS;
^FO629,920^ADN,36,10^FN12^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^PQ1,0,0,N^XZ;
