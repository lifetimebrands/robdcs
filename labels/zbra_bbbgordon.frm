#
#SELECT=select var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffadd3, var_shpord.ffcity||', '||var_shpord.ffstcd||' '||var_shpord.ffposc ffctz, 'PO# '||var_shpord.cponum cponum, 'Carton '||pckwrk.cur_cas||' of '|| pckwrk.tot_cas_cnt xofx, pckwrk.subucc from var_shpord, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum
#
#DATA=@cponum~@ffname~@ffadd1~@ffadd2~@ffadd3~@ffctz~@xofx~@subucc~
#
^XA^DFbbbgordon^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^FO20,20^ADN,36,10^FDFrom: LIFETIME BRANDS^FS;
^FO20,60^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO20,100^ADN,36,10^FDROBBINSVILLE, NJ 08691^FS;
^FO20,220^ADN,54,15^FN1^FS;
^FO270,350^ADN,54,15^FDShip To^FS;
^FO30,400^ADN,54,15^FN2^FS;
^FO30,450^ADN,54,15^FN3^FS;
^FO30,500^ADN,54,15^FN4^FS; 
^FO30,550^ADN,54,15^FN5^FS;
^FO30,600^ADN,54,15^FN6^FS;
^FO250,750^ADN,54,15^FN7^FS;
^FO15,870^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,900^ADN,36,10^FN8^FS;
^BY4,2.3,255^FO100,940^BCN,,N,N,Y,U^FN8^FS;
^PQ1,0,1,Y^XZ;
