#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2,var_shpord.ffadd3, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc,var_shpdtl_view.deptno, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, pckwrk.vc_slotno, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, pckwrk.uc_pck_seqnum uc_seqnum, ord_line.vc_lblfield_string_1 string1, ord_line.vc_lblfield_string_2 string2 from var_shpord, var_shpdtl_view, pckwrk, ord_line where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and pckwrk.ordsln=ord_line.ordsln and ord_line.client_id='----' if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', 6, 9) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1,substr('@ffadd3',1,16) lblffadr3, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd'||' '||substr('@ffposc', 1, 5) lblffcstz,substr('@deptno',1, 10) lbldeptno,  substr('@ffposc', 1, 5) lblffzip, substr('@stcust', 6, 9) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, 'PO: '||substr('@cponum', 1, 30) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty, decode(substr('@uc_seqnum',1,7),'LANE039','LANE39'||substr('@uc_seqnum',instr('@uc_seqnum','-'),9),'@uc_seqnum') lblcasesort from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5, substr('@subucc',17,4) lblsn_6 from dual
#
#SELECT=select 'Vendor: '||usr_vendor vend from ord where ordnum='@ordnum'
#
#SELECT=select 'FFC#' lblffcust, 'STC#' lblstcust, 'Some Description' lbldesc, 'Carton '||to_char(1, 'fm9999')||' of '||to_char(2, 'fm9999') lblxofx, '@lbldeptno'||' '||'@string1' lbldept, substr('@string2',1,2)||' '||substr('@string2',3,20) lblclass from dual
#SELECT=select min(upccod) upccod from prtmst where prtnum = '@prtnum'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select decode('@prtnum','KITPART', (select sum(pw.pckqty*pm.vc_grswgt_each) from pckwrk pw, prtmst pm where pw.ctnnum=(select subnum from pckwrk where wrkref='@wrkref')and pw.prtnum=pm.prtnum),(select grswgt from prtmst where prtnum='@prtnum'))||' lbs' lblweight from dual
#
#SELECT=select decode('@prtnum','KITPART','Multiple SKUs','@prtnum') prtnum2, decode('@prtnum','KITPART','Multiple SKUs','@lblitem') lblitem2 from dual
#
#SELECT=select substr(lngdsc,1,30) lbldescription from prtdsc, prtmst, pckwrk where prtdsc.colval = prtmst.prtnum||'|----' and prtdsc.colnam= 'prtnum|prt_client_id' and prtdsc.locale_id ='US_ENGLISH' and pckwrk.prtnum=prtmst.prtnum and pckwrk.wrkref='@wrkref' union select ' ' lbldescription from pckwrk where prtnum='KITPART' and wrkref='@wrkref'
#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH24MISS') print_date, decode(logo_name,null,'Lifetime Brands Inc.',(select rtstr2 from poldat where polcod='USR' and polvar='LABEL-DATA' and polval='CUSTOMER' and rtstr1=logo_name)) from_name from ord where ordnum='@ordnum'
#
#SELECT=select decode ((select count(*) from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust'),0,' ', (select rtstr2 from poldat where polcod='USR' and polvar='LABEL-MESSAGE' and polval='BTCUST' and rtstr1='@btcust')) lblmessage from dual
#
#DATA=@lblweight~@lblsnbar~@prtnum2~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblsrcloc~@lblcustpo~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblsn_6~@lblclass~@dummy~@dummy~@lbluntcas~@lblordnum~@lblitem~@vend~@lbldept~@dummy~@ship_id~@dummy~@dummy~@dummy~@dummy~@lbldescription~@dummy~@dummy~@dummy~@dummy~@dummy~@print_date~@from_name~@lblcasesort~@vc_slotno~@lblmessage~
#
^XA^DFcrate^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDFrom:^FS;
^FO15,134^ADN,36,10^FN37^FS;
^FO15,174^ADN,36,10^FD12 APPLEGATE DR.^FS;
^FO15,214^ADN,35,10^FDROBBINSVILLE, NJ 08691^FS;
^FO15,94^ADN,36,10^FN22^FS;
^FO320,54^GB0,301,2^FS;
^BY3,2.3,230^FO350,54^B3N,,N,N,Y,U^FN21^FS;
^FO350,295^ADN,54,15^FN21^FS;
^FO15,355^GB785,0,2^FS;
^FO15,360^ADN,36,10^FDTO:^FS;
^FO65,360^ADN,54,13^FN4^FS;
^FO65,428^ADN,36,10^FN5^FS;
^FO65,468^ADN,36,10^FN6^FS;
^FO65,508^ADN,36,10^FN7^FS;
^FO410,355^GB0,236,2^FS;
^FO415,370^ADN,36,10^FDCarton Qty:^FS;
^FO415,410^ADN,36,10^FN19^FS;
^FO415,450^ADN,36,10^FDWeight:^FS;
^FO415,490^ADN,36,10^FN1^FS;
^FO15,590^GB785,0,2^FS;
^FO15,595^ADN,54,15^FN30^FS;
^FO16,655^ADN,36,10^FN3^FS;
^FO15,695^GB785,0,2^FS;
^FO15,700^ADN,54,15^FN23^FS;
^FO15,756^ADN,54,15^FN16^FS;
^FO15,800^GB785,0,2^FS;
^FO15,810^ADN,36,10^FN9^FS;
^FO15,850^ADN,36,10^FDCountry: China^FS;
^FO410,800^GB0,86,2^FS;
^FO415,810^ADN,54,15^FN15^FS;
^FO15,885^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN10^FS;
^FO276,920^ADN,36,10^FN11^FS;
^FO331,920^ADN,36,10^FN12^FS;
^FO462,920^ADN,36,10^FN13^FS;
^FO629,920^ADN,36,10^FN14^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO15,1284^ADN,130,35^FN38^FS;
^FO685,1560^ADN,54,15^FN39^FS;
^FO15,1560^ADN,54,15^FN40^FS;
^FO415,1414^ADN,54,15^FN20^FS;
^FO415,1474^ADN,54,15^FN25^FS;
^FO15,1414^ADN,54,15^FN8^FS;
^FO15,1474^ADN,54,15^FN3^FS;
^FO455,1610^ADN,18,10^FN36^FS;
^PQ1,0,0,N^XZ;
