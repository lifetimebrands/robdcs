/* #REPORTNAME=User JCPenny Staged Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is staged for JC Penny. */
/* #HELPTEXT= Requested by Senior Management - Shipping */
 
/* Author: Al Driver */        

/* This report was created off of the Usr Linens Staged Report */
/* A view is used a view to filter out */                 
/* duplicates to obtain the correct weights and */ 
/* use a subquery to count the cartons. */

ttitle left '&1 ' print_time -
		center 'User JC Penny Staged Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column stname heading 'Ship To Name'
column stname format a45
column state heading 'State'
column state format a10
column cponum heading 'PO #'
column cponum format a18
column customer heading 'Customer'
column customer format a20
column shipid heading 'Ship ID'
column shipid format a14
column NUMCTN heading '# Ctns'
column NUMCTN format 999999
column carcod heading 'Carrier'
column carcod format a15
column stgdte heading 'StgDte'
column stgdte format a13
column cpodte heading 'CnlDte'
column cpodte format a13
column weight heading 'Weight'
column weight format 9999.99 


set linesize 200
set pagesize 500

alter session set nls_date_format ='DD-MON-YYYY'
/
   select 
           stname,
           state,
           cponum,
           customer,
           shipid,
           NUMCTN,
           carcod,
           stgdte,
           cpodte,
           sum((stgqty/untcas) * netwgt) weight
     from usr_jcpennystaged_view
    group by stname,state,cponum,customer,shipid,NUMCTN,
    carcod,stgdte,cpodte
/
