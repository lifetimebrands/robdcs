/* #REPORTNAME=User BBB Staged Load Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is staged for BBB. */ 
/* #HELPTEXT= Requested by Senior Management - Shipping */

ttitle left '&1 ' print_time -
		center 'User BBB Staged Load Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

/* Author: Al Driver */

/* This report was created off of the Usr Linens Staged Report */
/* A view is  used a view to filter out */
/* duplicates to obtain the correct weights and */
/* use a subquery to count the cartons. */

column schdte heading 'SchShipDate'
column schdte format a11
column stgdte heading 'StgDte'
column stgdte format a15
column NUMCTN heading '# Ctns'
column NUMCTN format 999999
column shipid heading 'Ship ID'
column shipid format a14
column stname heading 'Ship To Name'
column stname format a45
column carcod heading 'Carrier'
column carcod format a15
column cpodte heading 'CnlDte'
column cpodte format a15
column extdte heading 'ExtDte'
column extdte format a13
column cponum heading 'PO #'
column cponum format a18
column state heading 'State'
column state format a10
column earlydte heading 'EarlyShpDte'
column earlydte format a11

set linesize 300
set pagesize 500 


alter session set nls_date_format ='DD-MON-YYYY';


   select
           stname,
           state,
           cponum,
           customer,
           shipid,
           NUMCTN,
           carcod,to_char(earlydte,'DD-MON-YYYY') earlydte, 
           to_char(stgdte,'DD-MON-YYYY') stgdte,
           to_char(cpodte,'DD-MON-YYYY') cpodte,
           sum((stgqty/untcas) * netwgt) weight
     from usr_bbbstaged_view
    group by stname,state,cponum,customer,shipid,NUMCTN,
    carcod,earlydte,stgdte,cpodte
/
