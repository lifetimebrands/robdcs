/* #REPORTNAME=IC FG in RackComp Area Report */
/* #HELPTEXT= Requested by Engineering/ Inventory Control */

ttitle left print_time center 'IC Finished Good in RackComp Area Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a12
column lodnum heading 'Pallet'
column lodnum format a20
column prtnum heading 'Item'
column prtnum format a12
column invsts heading 'Status'
column invsts format a5
column vc_itmtyp heading 'Type'
column vc_itmtyp format a3
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Committed'
column comqty format 999999

select a.stoloc, a.lodnum, b.prtnum, b.invsts, b.untqty, d.comqty
from invlod a, invdtl b, prtmst c, invsum d, invsub e
where d.arecod in ('RACKCOMP')
and a.lodnum=e.lodnum
and a.stoloc=d.stoloc
and d.prt_client_id='----'
and e.subnum=b.subnum
and b.prtnum=d.prtnum
and b.prtnum=c.prtnum
and c.prt_client_id='----'
and c.vc_itmtyp ='FG'
order by a.stoloc 
/
