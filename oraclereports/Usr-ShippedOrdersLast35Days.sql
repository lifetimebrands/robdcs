set termout off;
set feedback off;
set heading off;
set linesize 120;
set pagesize 9999;

spool Usr-ShippedOrdersLast35Days.txt;


select rtrim(ord.ordnum)||'~'|| 
rtrim(ord_line.ordlin) ||'~'|| 
rtrim(ord.cponum)||'~'||  
trunc(tr.dispatch_dte)||'~'|| 
sd.shpqty ||'~'|| 
'1000' output_data
From shipment_line sd, shipment sh, ord, ord_line, stop, trlr tr,car_move cr
Where  ord.ordnum                  = ord_line.ordnum
AND ord.client_id               = ord_line.client_id
AND ord_line.client_id          = sd.client_id
AND ord_line.ordnum             = sd.ordnum
AND ord_line.ordlin             = sd.ordlin
AND ord_line.ordsln             = sd.ordsln
AND sd.ship_id                  = sh.ship_id
AND sh.stop_id                  = stop.stop_id
AND stop.car_move_id            = cr.car_move_id
AND cr.trlr_id                  = tr.trlr_id
AND sh.shpsts                   = 'C'
AND ord.ordtyp                  != 'W'
AND  trunc(tr.dispatch_dte) >  trunc(sysdate) - 35
order by   trunc(tr.dispatch_dte), cponum, sd.ordnum, sd.ordlin;

spool off;
set termout on;

/
