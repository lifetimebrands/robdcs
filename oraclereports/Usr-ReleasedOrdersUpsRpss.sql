/* #REPORTNAME=User Released UPS/RPS Report */
/* #HELPTEXT= The Released Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is released for UPS/RPS. */ 
/* #HELPTEXT= Requested by Order Management. */

ttitle left '&1 ' print_time -
		center 'User Released UPS RPS Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column schdte heading 'SchShipDate'
column schdte format a11
column early heading 'Early Ship Date'
column early format a13
column NUMCTN heading '# Ctns'
column NUMCTN format 999999
column shipid heading 'Ship ID'
column shipid format a14
column stname heading 'Ship To Name'
column stname format a45
column carcod heading 'Carrier'
column carcod format a15
column cpodte heading 'CnlDte'
column cpodte format a13
column extdte heading 'ExtDte'
column extdte format a13
column cponum heading 'PO #'
column cponum format a18

set linesize 200
set pagesize 5000

alter session set nls_date_format ='DD-MON-YYYY'
/

select sh.entdte schdte,
       rtrim(lookup1.adrnam) stname,
       rtrim(ord.cponum) cponum,
       rtrim(sh.ship_id) shipid,
       count(distinct invsub.subnum) NUMCTN,
       sh.carcod,
       ord_line.early_shpdte stgdte,
       ord.cpodte cpodte,
       ord_line.vc_extdte extdte
 from
       locmst lm,
       aremst am,
       invlod,
       invsub,
       invdtl,
       shipment_line  sd,
       shipment sh,
       ord,
       ord_line,
       dscmst, adrmst lookup1
      WHERE invlod.stoloc = lm.stoloc
   AND lm.arecod                   = am.arecod
        AND am.stgflg                   = 1
        AND invlod.lodnum = invsub.lodnum
        AND invsub.subnum = invdtl.subnum
        AND ord.st_adr_id              = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND invdtl.ship_line_id         = sd.ship_line_id
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND dscmst.colval               = sh.shpsts
        AND sh.shpsts                   in ('I')
        AND sh.carcod in ('UPSS','UPSG','UPSCC','RPSS')
group
    by
       sh.entdte,
       lookup1.adrnam,
       ord.cponum,
       sh.ship_id,
       sh.carcod,
       sh.shpsts,
       ord_line.early_shpdte ,
       ord.cpodte,
       ord_line.vc_extdte
/
