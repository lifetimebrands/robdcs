/* #REPORTNAME=PI Status Change Detail Report */
/* #HELPTEXT= This report displays discrepancies between statuses */
/* #HELPTEXT= Requested by Inventory Control */


ttitle left '&1' print_time center 'PI Status Change Detail Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location '
column stoloc format a12
column ctprt heading 'Counted Part'
column ctprt format a12
column cntsts heading 'Counted Status'
column cntsts format a14
column invsts heading 'Prior Status'
column invsts format a12
column prtnum heading 'Prior Part'
column prtnum format a12
column cntqty heading 'Counted Qty'
column cntqty format 999999
column untqty heading 'Prior Qty'
column untqty format 999999
column lodnum heading 'Pallet Id  '
column lodnum format a30

set pagesize 3000
set linesize 200


select a.stoloc, a.lodnum, b.prtnum, c.prtnum ctprt, b.invsts, c.invsts cntsts, b.untqty, c.untqty cntqty
from phys_inv_snapshot b, invsub d, invdtl c, invlod a
where b.stoloc not in ('CFPP-RTN','RETURN-XFER') and b.stoloc not like '%TRL%'
and b.stoloc = a.stoloc
and b.prtnum = c.prtnum
and c.prt_client_id = '----'
and b.invsts <> c.invsts
and a.lodnum = d.lodnum
and d.subnum = c.subnum
order by 1
/
