/* #REPORTNAME=User Remove Invalid Moves That Prevent the Shipment Moves  */
/* #VARNAM=Detail# , #REQFLG=Y */
/* #VARNAM=stoloc , #REQFLG=Y */

/* #HELPTEXT= This deletes the invalid location that is  */
/* #HELPTEXT= preventing the ship move  */
/* #HELPTEXT= Requested by Order Management */
/* #GROUPS=NOONE */

set pagesize 50
set linesize 160


ttitle left print_time -
center 'User Remove  Invalid Moves for Detail &1  and Bad Location &2 ' -
right print_date skip 2 -


delete from invmov 
where lodnum = '&1'
and stoloc = '&2';

commit;
