/* #REPORTNAME =CC Unit Counts and Values TEST */
/* #VARNAM=from , #REQFLG=Y */
/* #VARNAM=to , #REQFLG=Y */


alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';

column cntdte heading 'Date'
column cntdte format a20
column stoloc heading 'Location '
column stoloc format a12
column dcs_units heading 'Beginning Units'
column dcs_units format 999999999
column dcs_value heading 'Unit Value'
column dcs_value format 99999.99
column arecod  heading 'Area'
column arecod  format a12
column cntbat heading 'Batch'
column cntbat format a20

set linesize 200
set pagesize 15000

select b.cntdte, a.stoloc, b.arecod, a.cntbat, nvl(to_char(a.dcs_units),'EMPTY') dcs_units,
nvl(to_char((a.dcs_units*a.cost)),'EMPTY') dcs_value
from
(select b.stoloc, b.cntbat, max(b.untqty) dcs_units, max(b.untcst) cost, b.cntdte
from
(select distinct cntbat, max(cntdte) cntdte, stoloc, area
from 
(select a.cntbat, a.cntdte, nvl(to_char(a.untqty),'EMPTY') dcs_units, nvl(to_char((a.untqty*a.untcst)),'EMPTY') dcs_value, 
a.stoloc stoloc, nvl(b.arecod,'Deleted') area
     from usr_cnthst a, usr_cnthst c, locmst b
where a.stoloc=c.stoloc(+)
and a.stoloc=b.stoloc(+)
     and a.cnttyp = 'B' and c.cnttyp='C'
     and a.cntmod = 'C' 
     and exists (select 1 from usr_cnthst u where u.cnttyp='C' 
and trunc(u.cntdte) >  =  to_date('&1','DD-MON-YYYY') 
     and trunc(u.cntdte) <  to_date('&2','DD-MON-YYYY' )+1 
     and u.cntbat =  a.cntbat and u.stoloc =  a.stoloc) 
     and exists (select 1 from usr_cnthst u where u.cntbat = a.cntbat
     and u.stoloc = a.stoloc and (u.cnttyp = 'F' or u.cnttyp ='E' and u.untqty >= 0)
     and  u.cntmod ='C')  
union
select a.cntbat, a.cntdte, nvl(to_char(a.untqty),'EMPTY') dcs_units, nvl(to_char((a.untqty*a.untcst)),'EMPTY') dcs_value, 
a.stoloc stoloc, nvl(b.arecod,'Deleted') area
     from usr_cnthst a, locmst b
where a.stoloc=b.stoloc(+)
     and a.cnttyp = 'E'
     and a.cntmod = 'C' 
     and not exists (select 1 from usr_cnthst u where u.cnttyp='C' 
     and u.cntbat =  a.cntbat and u.stoloc =  a.stoloc) 
     and trunc(a.cntdte) >  =  to_date('&1','DD-MON-YYYY')
     and trunc(a.cntdte) <  to_date('&2','DD-MON-YYYY' )+1)
group by cntbat, stoloc, area) a, usr_cnthst b
where a.stoloc=b.stoloc
and a.cntbat=b.cntbat
and a.cntdte=b.cntdte
group by b.stoloc, b.cntbat, b.cntdte) a, locmst b
where a.stoloc=b.stoloc(+)
union
select a.cntdte, a.stoloc, nvl(b.arecod,'Deleted'), a.cntbat, nvl(to_char(a.untqty),'EMPTY') dcs_units,
nvl(to_char((a.untqty*a.untcst)),'EMPTY') dcs_value
from usr_cnthst a, locmst b
where a.stoloc=b.stoloc
and a.cnttyp='B'
and trunc(a.cntdte) >  =  to_date('&1','DD-MON-YYYY') 
     and trunc(a.cntdte) <  to_date('&2','DD-MON-YYYY' )+1
and a.stoloc in ('07A160','18B112','19B118')
order by stoloc;
