/* #REPORTNAME=IC-KA Samples Location Detail Report */
/* #VARNAM=Starting_Location , #REQFLG=Y */
/* #VARNAM=Ending_Location , #REQFLG=Y */
/* #HELPTEXT= This report location, item, item status*/
/* #HELPTEXT= lot cod, unit quatntity, inner quantitiy */
/* #HELPTEXT= and master quantitiy for a range of locations */

column stoloc heading 'Location'
column stoloc format a20
column prt heading 'Item'
column prt format a30
column sts heading 'Status'
column sts format a30
column lot heading 'Lot Code'
column lot format a10
column unt heading 'Quantity'
column unt format 99,999
column pak heading 'Inner Qty'
column pak format 99,999
column cas heading 'Master Qty'
column cas format 99,999

set linesize 300
set pagesize 200

select b.stoloc, a.prtnum prt, c.lngdsc sts, a.lotnum lot, a.untqty unt , a.untpak pak, a.untcas cas
from invloc a, locmst b, dscmst c
where a.stoloc=b.stoloc
and b.stoloc between '&&1' and '&&2'
and a.orgcod='----'
and a.prt_client_id='----'
and a.invsts=c.colval
and c.colnam='invsts'
and c.locale_id='US_ENGLISH'
union
select distinct b.stoloc, 'Empty' prt, ' ' sts, ' ' lot, 0 unt, 0 pak, 0 cas
from invloc a, locmst b
where a.stoloc=b.stoloc
and b.stoloc between '&&1' and '&&2'
and b.stoloc not in (select stoloc from invloc where stoloc between '&&1' and '&&2' and orgcod='----');
