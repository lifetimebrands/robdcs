/* #REPORTNAME=PI Counted Empty Report */
/* #HELPTEXT= This report compares the empty counted location */
/* #HELPTEXT= against the snapshot data */
/* #HELPTEXT= Requested by Inventory Control */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'PI Counted Empty Report' -
right print_date skip 2 -

column arecod heading 'Area  '
column arecod format a20
column stoloc heading 'Location'
column stoloc format A30
column prtnum heading 'Item At Freeze'
column prtnum format  A14
column lotnum heading 'Lot Code At Freeze'
column lotnum format A20
column invsts heading 'Stat. At Freeze'
column invsts format A15
column untqty heading 'Qty On-Hand At Freeze'
column untqty format 9999999999
column cnt_usr_id heading 'User'
column cnt_usr_id format A15


select pi.arecod, pi.stoloc, pi.prtnum, pi.lotnum, pi.invsts, sum(pi.untqty) untqty, c.cnt_usr_id 
from invlod il, phys_inv_snapshot pi, cntwrk c
where pi.gentyp ='INITIAL'
and pi.stoloc = c.stoloc
and c.cntsts ='C'
and c.cntbat in (select max (cntbat) from cntwrk c2 where c2.stoloc = c.stoloc)
and c.stoloc = il.stoloc (+)
and il.stoloc is null
group by pi.arecod, pi.stoloc, pi.prtnum, pi.lotnum, pi.invsts, c.cnt_usr_id
/* union 
select a.arecod, a.stoloc, a.prtnum, a.lotnum, a.invsts, sum(a.untqty) untqty
from phys_inv_snapshot a, usr_cnthst b
where a.gentyp ='INITIAL'
and a.stoloc = b.stoloc
and b.cnttyp ='C'
and a.stoloc not in (select c.stoloc from invsum c
where b.stoloc = c.stoloc)
group by a.arecod, a.stoloc, a.prtnum, a.lotnum, a.invsts
union
select a.arecod, a.stoloc, a.prtnum, a.lotnum, a.invsts, sum(a.untqty) untqty
from phys_inv_snapshot a, cnthst b
where a.gentyp ='INITIAL'
and a.stoloc = b.stoloc
and b.cnttyp ='C'
and a.stoloc not in (select c.stoloc from invsum c
where b.stoloc = c.stoloc)
group by a.arecod, a.stoloc, a.prtnum, a.lotnum, a.invsts */
order by 1,2
/
