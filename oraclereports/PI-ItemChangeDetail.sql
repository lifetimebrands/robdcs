/* #REPORTNAME=PI Item Change Detail Report */
/* #HELPTEXT= This report displays discrepancies between items in the same location */
/* #HELPTEXT= Requested by Inventory Control */


 ttitle left '&1' print_time center 'PI Item Change Detail Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location '
column stoloc format a12
column ctprt heading 'Counted Part'
column ctprt format a12
column inval1 heading 'Counted Lot'
column inval1 format a30
column lotnum heading 'Prior Lot'
column lotnum format a30
column prtnum heading 'Prior Part'
column prtnum format a12
column cntqty heading 'Counted Qty'
column cntqty format 999999
column untqty heading 'Prior Qty'
column untqty format 999999
column lodnum heading 'Pallet Id'
column lodnum format a25
column cntlot heading 'Counted Lot'
column cntlot format a30

set pagesize 3000
set linesize 200


select a.stoloc, a.lodnum, b.prtnum, c.prtnum ctprt, b.lotnum, c.lotnum cntlot, b.untqty, c.untqty cntqty
    from invlod a, invsub d, invdtl c, phys_inv_snapshot b, cntwrk e 
    where b.stoloc not in ('CFPP-RTN','RETURN-XFER') and b.stoloc not like '%TRL%'
    and a.stoloc = b.stoloc
    and b.gentyp <> 'FINAL'
    and a.stoloc = e.stoloc
    and e.cntsts = 'C'
    and c.prtnum <> b.prtnum 
    and a.lodnum = d.lodnum
    and d.subnum = c.subnum
union
select a.stoloc, a.lodnum, b.prtnum, c.prtnum ctprt, b.lotnum, c.lotnum cntlot, b.untqty, c.untqty cntqty
    from invlod a, invsub d, invdtl c, phys_inv_snapshot b, usr_cnthst e
    where b.stoloc not in ('CFPP-RTN','RETURN-XFER') and b.stoloc not like '%TRL%'
    and a.stoloc = b.stoloc
    and b.gentyp <> 'FINAL'
    and a.stoloc = e.stoloc
    and e.cnttyp = 'C'
    and c.prtnum <> b.prtnum
    and a.lodnum = d.lodnum
    and d.subnum = c.subnum
order by 1
/
