/* #REPORTNAME=User Inventory  Status Report */
/* #HELPTEXT= This report displays inventory status information */
/* #HELPTEXT = Requested by Westbury */

ttitle left  print_time -
       center 'User Inventory Status Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a15
column lngdsc heading 'Description'
column lngdsc format a30 
column invsts heading 'Inv. Status'
column invsts format a11
column untqty heading 'Qty'
column untqty format 999999
column comqty heading 'Committed'
column comqty format 999999
column pckable heading 'Pickable'
column pckable format a2
column prdcod heading 'Product Code'
column prdcod format a4
column itmtyp heading 'Item Type'
column itmtyp format a4

set pagesize 10000
set linesize 200


select a.prtnum, substr(b.lngdsc,1,30) lngdsc, a.invsts, sum(a.untqty) untqty, sum(a.comqty) comqty
from invsum a, prtdsc b
where
a.prtnum||'|----' = b.colval
and b.colnam ='prtnum|prt_client_id'
and b.locale_id ='US_ENGLISH'
group by a.prtnum, a.invsts,substr(b.lngdsc,1,30)
/
