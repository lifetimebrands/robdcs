/* #REPORTNAME=ENG Old Receiving Records Report */
/* #HELPTEXT = Requested by Engineering */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */  

ttitle left  print_time -
       center 'ENG Old Receiving Records Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column trknum heading 'PO #'
column trknum format a10 trunc
column supnum heading 'Vendor'
column supnum format a10
column adrnam heading 'Supp Name'
column adrnam format a12 trunc
column expqty heading 'Exp|Qty'
column expqty format 9999999999
column indqty heading 'Received'
column indqty format 9999999999
column arrdte heading 'Arrived'
column arrdte format a12
column dueqty heading 'Due Qty'
column dueqty format 999999999
column price heading 'Price Per Thousand'
column price format  $9999999.99
column total heading 'Total'
column total format 999999.99

alter session set nls_date_format ='dd-mon-yyyy';

SELECT 
       sum(rcvlin.idnqty) IndQty,
       sum(rcvlin.idnqty) * prtmst.untcst total,
       substr(trlr.arrdte,4,3) arrdte, rcvlin.prtnum
  FROM rcvlin, rcvinv, rcvtrk, prtmst, adrmst,supmst,trlr
 WHERE trunc(trlr.arrdte) between '&1' and '&2'
   AND trlr.trlr_stat ='C'
   AND rcvtrk.trknum = rcvinv.trknum
   AND rcvtrk.trlr_id = trlr.trlr_id
   AND rcvinv.trknum = rcvlin.trknum
   AND rcvlin.supnum not in ('CY02','CY05','DAYTON','PROD','RET')
   AND rcvinv.supnum = rcvlin.supnum
   AND rcvinv.invnum = rcvlin.invnum
   AND rcvlin.trknum = rcvtrk.trknum
   AND rcvlin.supnum = supmst.supnum
   AND supmst.adr_id = adrmst.adr_id
   AND rcvlin.prtnum = prtmst.prtnum
   AND prtmst.prt_client_id = '----'
group by substr(trlr.arrdte,4,3), rcvlin.prtnum, prtmst.untcst
union
SELECT 
       sum(rcvlin.idnqty) IndQty,
       sum(rcvlin.idnqty) * prtmst.untcst total,
       substr(arrdte,4,3) arrdte, rcvlin.prtnum
  FROM rcvlin@arch rcvlin, rcvinv@arch rcvinv, rcvtrk@arch rcvtrk, prtmst@arch prtmst, adrmst@arch adrmst, supmst@arch supmst
 WHERE trunc(arrdte) between '&1' and '&2'  
   AND rcvtrk.trksts = 'C'                      /* archive tables */
   AND rcvtrk.trknum = rcvinv.trknum
   AND rcvinv.trknum = rcvlin.trknum
   AND rcvlin.supnum not in ('CY02','CY05','DAYTON','PROD','RET')
   AND rcvinv.supnum = rcvlin.supnum
   AND rcvinv.invnum = rcvlin.invnum
   AND rcvlin.trknum = rcvtrk.trknum
   AND rcvlin.supnum = supmst.supnum
   AND supmst.adr_id = adrmst.adr_id
   AND rcvlin.prtnum = prtmst.prtnum
   AND prtmst.prt_client_id = '----'
group by substr(rcvtrk.arrdte,4,3), rcvlin.prtnum, prtmst.untcst
/

