/* #REPORTNAME=User  CFPP Movement Report */
/* #HELPTEXT= This report lists all CFPP */
/* #HELPTEXT= locations, that are not assigned, and are not */
/* #HELPTEXT= pending or committed. */
/* #HELPTEXT= Requested by Senior Management - Westbury */

column stoloc heading 'Location'
column stoloc format A15
column prtnum heading 'Item'
column prtnum format  A15
column lngdsc heading 'Description'
column lngdsc format A30
column untqty heading 'Qty   '
column untqty format 999,999
column adddte heading 'Picked Date     '
column adddte format A24

alter session set nls_date_format ='dd-mon-yyyy';

set pagesize 4000
set linesize 200

 ttitle left print_time center 'User  CFPP Movement Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno
select invlod.stoloc, 
       invdtl.prtnum, sum(invdtl.untqty) untqty,
       substr(prtdsc.lngdsc,1,30) lngdsc,
       max(pckwrk.adddte) adddte
 from invlod, invsub, prtdsc, locmst, invdtl, invsum,pckwrk  
 where invlod.stoloc = locmst.stoloc
 and invlod.stoloc = invsum.stoloc
 and invsum.arecod = locmst.arecod
 and invlod.stoloc = pckwrk.srcloc
 and invdtl.prtnum = pckwrk.prtnum
 and invlod.lodnum = invsub.lodnum
 and invsub.subnum = invdtl.subnum
 and locmst.arecod    = 'CFPP'
 and trunc(pckwrk.adddte)  <= sysdate - 30      
 and locmst.asgflg = 0 and invsum.pndqty = 0 and invsum.comqty = 0
 and invdtl.prtnum||'|----' = prtdsc.colval 
 and prtdsc.colnam ='prtnum|prt_client_id'
 and prtdsc.locale_id ='US_ENGLISH'
group by pckwrk.adddte, invlod.stoloc, invdtl.prtnum, substr(prtdsc.lngdsc,1,30)
/
