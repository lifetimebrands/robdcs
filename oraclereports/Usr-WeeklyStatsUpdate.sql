/* The following script updates the usr_weeklystats table. */
/* The script will run on a daily basis and update with various categories */
/* Looks at production and the archive tables */ 

truncate table usr_wstats_timer;
insert into usr_wstats_timer values( 'Start', sysdate );

create table var_prtqty_stats as
select 'XXXXXXXXXXXXXXXXXXXX' stoloc, x.prtnum, sum(avlqty) units, sum(avlqty * nvl(prtmst.untcst,0)) value,
sum((avlqty/prtmst.untcas)*(caslen*caswid*cashgt/1728)) qube, decode(x.invsts,'A',x.invsts) invsts, trunc(sysdate) sys_date
from
(select distinct invlod.stoloc,locmst.arecod, 
          invdtl.prtnum,
           invdtl.lotnum,
           invdtl.invsts,
           invdtl.untqty avlqty,
          0 pndqty
     from locmst,
          invlod,
          invsub,
          invdtl, aremst
    where invdtl.prt_client_id = '----'
      and invdtl.subnum = invsub.subnum
      and invsub.lodnum = invlod.lodnum
      and invlod.stoloc = locmst.stoloc
      and locmst.arecod = aremst.arecod
      and aremst.fwiflg = 1
      and aremst.arecod not in ('SSTG')) x,prtmst,ftpmst
  where prtmst.prtnum = x.prtnum
  and prtmst.prtnum = ftpmst.ftpcod
group by x.prtnum, x.invsts;



-- insert cubic volume of storage in 4-wall

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'ICV'
from var_prtqty_stats
group by sys_date;

insert into usr_wstats_timer values( '1', sysdate );
commit;

truncate table var_prtqty_stats;


/* Obtain Production Inventory info */

insert into var_prtqty_stats
select locmst.stoloc,invdtl.prtnum,0 units, sum(untqty * nvl(untcst,0)) value,
sum((untqty/prtmst.untcas)*(caslen*caswid*cashgt/1728)) qube,
decode(invsts,'A',invsts) invsts, trunc(sysdate) sys_date
        from
        locmst,prtmst,ftpmst,invlod,invsub,invdtl
        where locmst.arecod = 'PROD'
        and locmst.stoloc = invlod.stoloc
        and invlod.lodnum = invsub.lodnum
        and invsub.subnum = invdtl.subnum
       and invdtl.prtnum = prtmst.prtnum
       and prtmst.prtnum = ftpmst.ftpcod
    group by locmst.stoloc,invdtl.prtnum, decode(invsts,'A',invsts);

-- insert cubic volume of storage in production

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'PCV'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;


-- insert cubic volume of storage in production -shop locations

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'PSC'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

insert into usr_wstats_timer values( '2', sysdate );
commit;

truncate table var_prtqty_stats;


/* Obtain Ship Stage information */
/* script taken from Open Orders Report, numbers should match everything in Printed, Staged or Transfer status */

insert into var_prtqty_stats
select NULL stoloc, x.prtnum, 0 units, sum(unsamt) value,sum((units/untcas) * (caslen*caswid*cashgt/1728)) qube,
decode(invsts,'A',invsts) invsts, trunc(sysdate) sys_date
  from
(SELECT ord_line.prtnum,
        rtrim(ord_line.ordnum) ordnum,
        rtrim(dscmst.lngdsc) status,
        decode(shipment_line.linsts, 'I', sum(prtmst.untcst * (shipment_line.stgqty+shipment_line.inpqty)),
        sum(prtmst.untcst*shipment_line.pckqty)) unsamt,
   decode(shipment_line.linsts, 'I',sum(shipment_line.stgqty+shipment_line.inpqty),sum(shipment_line.pckqty)) units,
       ord_line.invsts_prg invsts
       FROM  dscmst,
           shipment_line, ord_line, shipment, prtmst
        where ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.ship_id       = shipment.ship_id
        AND ord_line.prtnum             = prtmst.prtnum
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled'
        GROUP BY ord_line.prtnum,ord_line.ordnum, dscmst.lngdsc,
        shipment_line.linsts, ord_line.invsts_prg) x,prtmst,ftpmst
where x.status in ('Staged','Printed','Transfer')
and x.prtnum = prtmst.prtnum
and prtmst.prtnum = ftpmst.ftpcod
group by x.prtnum,invsts;

--insert distinct items in Ship Stage

insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'SVI'
from var_prtqty_stats
group by sys_date;

-- insert total Ship Stage $ value

insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVV'
from var_prtqty_stats
group by sys_date;

-- insert Ship Stage $ value for available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVA'
from var_prtqty_stats
where invsts = 'A'
group by sys_date;

-- insert Ship Stage $ value for not available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'SVN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
group by sys_date;

-- insert cubic volume stored in Ship Stage

insert into usr_weeklystats(counts,trndte,status)
select sum(qube),sys_date,'SCV'
from var_prtqty_stats
group by sys_date;

insert into usr_wstats_timer values( '3', sysdate );
commit;

insert into var_prtqty_stats
select 'XXXXXXXXXXXXXXXXXXXX' stoloc, prtnum, sum(untqty) units, sum(untqty * nvl(untcst,0)) value, 0 qube, 
 invsts, trunc(sysdate) sys_date
from
(select /*+ ALL_ROWS */ ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(a.arecod)) arecod,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.invsts, d.untpak, d.untcas,  
       'INITIAL' gentyp, '----' prt_client_id, e.untcst
from aremst a,locmst m, invlod l, invsub s, invdtl d, prtmst e
 where a.fwiflg = 1 and
       a.arecod not in ('SSTG','PROD') and
       a.arecod  = m.arecod and
       m.stoloc  = l.stoloc and
       l.lodnum  = s.lodnum and
       s.subnum  = d.subnum
       and d.prtnum=e.prtnum
       and e.prt_client_id='----'
 group
    by d.prtnum,
       a.arecod,
       m.stoloc,
       d.lotnum,
       d.invsts,
       d.untpak,
       d.untcas,
       e.untcst)
group by prtnum,  invsts;

-- insert distinct items in 4 -wall
 insert into usr_weeklystats(counts ,trndte,status)
 select count(distinct prtnum),sys_date,'IVI' from var_prtqty_stats
 group by sys_date;

-- insert units in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(units),sys_date,'IVU' from var_prtqty_stats
 group by sys_date;

-- insert total $ value of inventory in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVV' from var_prtqty_stats
 group by sys_date;

-- insert $ value of available items in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVA'
 from var_prtqty_stats
 where invsts = 'A'
 group by sys_date;

-- insert $ value of not available items in 4-wall
 insert into usr_weeklystats(counts ,trndte,status)
 select sum(value),sys_date,'IVN'
 from var_prtqty_stats
 where nvl(invsts,'X') != 'A'
 group by sys_date;

insert into usr_wstats_timer values( '4', sysdate );
commit;

truncate table var_prtqty_stats;

insert into var_prtqty_stats
select 'XXXXXXXXXXXXXXXXXXXX' stoloc, prtnum, sum(untqty) units, sum(untqty * nvl(untcst,0)) value, 0 qube, 
 invsts, trunc(sysdate) sys_date
from
(select ltrim(rtrim(d.prtnum)) prtnum,
       ltrim(rtrim(a.arecod)) arecod,
       ltrim(rtrim(sysdate)) adddte,
       sum(d.untqty) untqty,
       ltrim(rtrim(m.stoloc)) stoloc, ltrim(rtrim(d.lotnum)) lotnum, d.invsts, d.untpak, d.untcas,  
       'INITIAL' gentyp, '----' prt_client_id, e.untcst
from aremst a,locmst m, invlod l, invsub s, invdtl d, prtmst e
 where a.arecod = 'PROD' and
       a.arecod  = m.arecod and
       m.stoloc  = l.stoloc and
       l.lodnum  = s.lodnum and
       s.subnum  = d.subnum
       and d.prtnum=e.prtnum
       and e.prt_client_id='----'
 group
    by d.prtnum,
       a.arecod,
       m.stoloc,
       d.lotnum,
       d.invsts,
       d.untpak,
       d.untcas,
       e.untcst)
group by prtnum, invsts;


-- insert distinct items in production
insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'PVI'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;

-- insert total production $ value
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVV'
from var_prtqty_stats
where stoloc not like 'SHOP%'
group by sys_date;

-- insert production $ value of available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVA'
from var_prtqty_stats
where invsts = 'A'
and stoloc not like 'SHOP%'
group by sys_date;

-- insert production $ value of not available items
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PVN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
and stoloc not like 'SHOP%'
group by sys_date;

-- insert distinct items in production- shop locations
insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),sys_date,'PSI'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

-- insert total production $ value -shop
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSV'
from var_prtqty_stats
where stoloc like 'SHOP%'
group by sys_date;

-- insert production $ value of available items -shop locations
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSA'
from var_prtqty_stats
where invsts = 'A'
and stoloc like 'SHOP%'
group by sys_date;

-- insert production $ value of not available items -shop locations
insert into usr_weeklystats(counts,trndte,status)
select sum(value),sys_date,'PSN'
from var_prtqty_stats
where nvl(invsts,'X') != 'A'
and stoloc like 'SHOP%'
group by sys_date;

truncate table var_prtqty_stats;

drop table var_prtqty_stats;

create table var_inventory_stats as
        SELECT /*+ ALL_ROWS */ prtmst.prtnum, vc_prdcod, invdtl.untqty, invsum.arecod, 
              invsum.stoloc, untcst, invsum.invsts, trunc(sysdate) dte
        from prtmst,invsum,invlod,invsub,invdtl
        where prtmst.prtnum = invsum.prtnum
        and prtmst.prt_client_id = invsum.prt_client_id
        and invsum.stoloc = invlod.stoloc
        and invsum.prtnum = invdtl.prtnum
        and invsum.prt_client_id = invdtl.prt_client_id
       and invlod.lodnum = invsub.lodnum
    and invsub.subnum = invdtl.subnum;

/* Insert # of Discontinued Items in Warehouse - Code DIS */
/* This script emulates ENG-Discontinued Items Report */
/* areas SHIP, SSTG and location %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct prtnum),trunc(dte),'DIS'
    from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
group by trunc(dte);

insert into usr_wstats_timer values( '5', sysdate );
commit;

/* Insert $ Value of Discontinued Items - Code DVA */
/* This script emulates ENG-Discontinued Items Report */
/* areas SHIP, SSTG and location %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT sum(untqty * nvl(untcst,0)),trunc(dte),'DVA'
    from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
group by trunc(dte);

insert into usr_wstats_timer values( '6', sysdate );
commit;

/* Insert # of Locations w Discontinued Items - Code RDI */
/* ISTG must be obtained by different means . */
/* This script emulates ENG-Discontinued Items Report */
/* Does not look at areas FGPROD, NOPK or PROB  */
/* area ISTG and stoloc %PERM% do not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct(stoloc)),trunc(dte),'RDI'
from var_inventory_stats 
    where substr(vc_prdcod,2,1) ='X'
and (arecod in ('TOWERFCC','TOWERFCP','TOWERREP','CFPP','GWALL')
or arecod like 'RACK%')
group by trunc(dte);

insert into usr_wstats_timer values( '7', sysdate );
commit;

/* Insert # of Rework Items in Warehouse - Code RWI */
/* This script emulates the IC-Rework report */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct prtnum),trunc(dte),'RWI'
             from var_inventory_stats
              where
              invsts in ('W','C','O','B','T','L')
group by trunc(dte);

insert into usr_wstats_timer values( '8', sysdate );
commit;


/* Insert $ Value of Rework Items- Code RWV */
/* This script emulates the IC-Rework report */

insert into usr_weeklystats(counts,trndte,status)
SELECT sum(untqty * nvl(untcst,0)),trunc(dte),'RWV'
from var_inventory_stats 
where invsts in ('W','C','O','B','T','L')
group by trunc(dte);

insert into usr_wstats_timer values( '9', sysdate );
commit;

/* Insert # of Locations Containing Rework Items - Code RRI */
/* This script emulates the IC-Rework report */
/* Does not look at areas FGPROD, NOPK or PROB   */
/* ISTG does not exist in invsum */

insert into usr_weeklystats(counts,trndte,status)
SELECT count(distinct (stoloc)),trunc(dte),'RRI'
from var_inventory_stats 
where invsts in ('W','C','O','B','T','L')
and (arecod in ('TOWERFCC','TOWERFCP','TOWERREP','CFPP','GWALL')
or arecod like 'RACK%')
group by trunc(dte);

insert into usr_wstats_timer values( '10', sysdate );
commit;

drop table var_inventory_stats;

/* Delete previous days inventory information */
/* Except on first day of the month */
/* ADD_MONTHS(LAST_DAY(sysdate),-1)+1 gives 1st day of month */

delete from usr_weeklystats
where status in ('DIS','DVA','RDI','RWI','RWV','RRI','IVI','IVU','IVA','IVN','PVI', 
'PVA','PVN','SVI','SVA','SVN','ICV','PCV','SCV','PSV','PSA','PSN','PSI','PSC')
and trunc(trndte) = trunc(sysdate) -1
and trunc(sysdate) != trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1);

insert into usr_wstats_timer values( '11', sysdate );
commit;

/* Insert Total Cube of Shipments - Code CUE */

insert into usr_weeklystats(counts,trndte,status)
select sum((x.shpqty/x.untcas)*(x.cube/1728)) qube, dispatch_dte, 'CUE'
                     from
                     (select distinct shipment.ship_id, i.prtnum, ord.ordnum,
                                          ord_line.ordlin,shipment_line.shpqty shpqty,
                          (ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl i,ftpmst, prtmst, shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND trunc(dispatch_dte) = trunc(sysdate)-1) x
       group by dispatch_dte;
 
insert into usr_wstats_timer values( '12', sysdate );
commit;


/* Insert Total Cube of Receipts - Code 'CUR' */

insert into usr_weeklystats(counts,trndte,status)
select trunc(sum((cube/ 1728) * (x.rcvqty/x.untcas)),2) qube, clsdte, 'CUR'
         from
           (select distinct rcvtrk.trknum,rcvlin.prtnum,rcvlin.rcvqty,prtmst.untcas untcas,
            trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum,caslen*caswid*cashgt cube,rcvlin.supnum,rcvlin.rcvkey
                                   from rcvtrk,rcvinv,rcvlin,trlr, ftpmst, prtmst, invdtl
                                   where trlr.trlr_id = rcvtrk.trlr_id
                                   and rcvtrk.trknum = rcvlin.trknum
                                   and rcvlin.trknum = rcvinv.trknum
                                   and rcvlin.client_id = rcvinv.client_id
                                   and rcvlin.supnum = rcvinv.supnum
                                   and rcvlin.invnum = rcvinv.invnum
                                   and rcvlin.prtnum = prtmst.prtnum
                                   and prtmst.prtnum = ftpmst.ftpcod
                                   and rcvlin.rcvkey = invdtl.rcvkey(+)
                                   --and rcvtrk.expdte is not null
                                   and trlr.trlr_stat = 'C'
                                   and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x
                    group by clsdte;

insert into usr_wstats_timer values( '13', sysdate );
commit;



/* Insert Total Weight of Shipments - Code 'WGT' */

insert into usr_weeklystats(counts,trndte,status)
select sum(x.shpqty * x.vc_grswgt_each) ladwgt,trunc(x.dispatch_dte), 'WGT' status  
from 
(select distinct shipment.ship_id, i.prtnum, ord.ordnum, 
  ord_line.ordlin,shipment_line.shpqty shpqty,prtmst.vc_grswgt_each, 
    prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte 
    from invdtl i, prtmst, shipment, shipment_line,  ord, ord_line,  stop, 
         trlr tr,car_move cr 
         where i.ship_line_id = shipment_line.ship_line_id 
        AND prtmst.prtnum = ord_line.prtnum   
        AND ord.ordnum                  = ord_line.ordnum  
        AND ord.client_id               = ord_line.client_id  
        AND ord_line.client_id          = shipment_line.client_id  
        AND ord_line.ordnum             = shipment_line.ordnum  
        AND ord_line.ordlin             = shipment_line.ordlin  
        AND ord_line.ordsln             = shipment_line.ordsln  
        AND shipment_line.ship_id       = shipment.ship_id  
        AND shipment.stop_id                  = stop.stop_id  
        AND stop.car_move_id            = cr.car_move_id   
        AND cr.trlr_id                  = tr.trlr_id  
        AND shipment.shpsts  ||''= 'C'  
        AND ord.ordtyp                   != 'W'  
        AND trunc(tr.dispatch_dte) = trunc(sysdate)-1) x  
  group by trunc(x.dispatch_dte);   

insert into usr_wstats_timer values( '14', sysdate );
commit;  

/* Insert Total Weight of Receipts - Code WGR */

insert into usr_weeklystats(counts,trndte,status)  
select trunc(sum(x.rcvqty * x.vc_grswgt_each),2) wgt, x.clsdte, 'WGR' status
from
(select distinct rcvtrk.trknum,rcvlin.prtnum,rcvlin.rcvqty,prtmst.untcas untcas,
 prtmst.vc_grswgt_each,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum,caslen,caswid,cashgt,rcvlin.supnum,rcvlin.rcvkey
                   from rcvtrk,rcvinv,rcvlin, trlr,ftpmst, prtmst, invdtl
                   where trlr.trlr_id = rcvtrk.trlr_id
                   and rcvtrk.trknum = rcvlin.trknum
                   and rcvlin.trknum = rcvinv.trknum
                   and rcvlin.client_id = rcvinv.client_id
                   and rcvlin.supnum = rcvinv.supnum
                   and rcvlin.invnum = rcvinv.invnum
                   and rcvlin.prtnum = prtmst.prtnum
                   and prtmst.prtnum = ftpmst.ftpcod
                   and rcvlin.rcvkey = invdtl.rcvkey(+)
                   --and rcvtrk.expdte is not null
                   and trlr.trlr_stat = 'C'
                   and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x
   group by x.clsdte;

insert into usr_wstats_timer values( '15', sysdate );
commit;


/* Insert Number of Workorders Closed - Code WKC */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct wkohdr.wkonum),trunc(wkohdr.clsdte),'WKC' status
    from wkohdr,wkodtl
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C'
    and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);

insert into usr_wstats_timer values( '16', sysdate );
commit; 

/* Insert Number of Units Going (Delivered) into Production - Code UNP */

insert into usr_weeklystats(counts,trndte,status)
select sum(tot_dlvqty),trunc(wkohdr.clsdte),'UNP' status
        from wkohdr,wkodtl
        where wkohdr.wkonum = wkodtl.wkonum
        and wkohdr.wkorev = wkodtl.wkorev
        and wkohdr.client_id = wkodtl.client_id
        and wkosts = 'C'
        and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);

insert into usr_wstats_timer values( '17', sysdate );
commit;

/* Insert Number of Cartons Going (Delivered) into Production - Code CPR */

insert into usr_weeklystats(counts,trndte,status)
select round(sum(tot_dlvqty/decode(untcas,0,1,untcas))),trunc(wkohdr.clsdte),'CPR' status
        from wkohdr,wkodtl
        where wkohdr.wkonum = wkodtl.wkonum
        and wkohdr.wkorev = wkodtl.wkorev
        and wkohdr.client_id = wkodtl.client_id
        and wkosts = 'C'
        and trunc(clsdte) = trunc(sysdate) -1
group by trunc(wkohdr.clsdte);

insert into usr_wstats_timer values( '18', sysdate );
commit;


/* Insert Total Number of Units Coming out of Production - Code UOP */
/* Sum of both Reworks(RWP) ,BOM Units (PUP) and XBOM Units (XBM) */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'UOP' status from
(select distinct wkohdr.wkonum,prdqty,trunc(clsdte) clsdte
from wkohdr,wkodtl,prtmst
where wkohdr.wkonum = wkodtl.wkonum
and wkohdr.wkorev = wkodtl.wkorev
and wkohdr.client_id = wkodtl.client_id
and wkohdr.prtnum = prtmst.prtnum
and wkosts = 'C'
and trunc(clsdte) = trunc(sysdate) -1) i
group by trunc(i.clsdte);

insert into usr_wstats_timer values( '19', sysdate );
commit; 


/* Insert Number of Containers Received - Code COR */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct rcvtrk.trknum),trunc(rcvtrk.clsdte),'COR' status
    from rcvtrk, rcvinv, rcvlin, trlr
    where trlr.trlr_id = rcvtrk.trlr_id
    and rcvtrk.trknum = rcvinv.trknum
    and rcvtrk.trknum = rcvlin.trknum
    and rcvlin.trknum=rcvinv.trknum
    and rcvlin.supnum= rcvinv.supnum
    and rcvlin.invnum =rcvinv.invnum
    and rcvlin.client_id = rcvinv.client_id
    and trlr.trlr_stat = 'C'
    --and rcvtrk.expdte is not null
    and trunc(rcvtrk.clsdte) = trunc(sysdate) -1 
group by trunc(rcvtrk.clsdte);

insert into usr_wstats_timer values( '20', sysdate );
commit;


/* Insert Number of Items Shipped - Code IS */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ord_line.prtnum)) counts,trunc(tr.dispatch_dte),'IS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts  ||''= 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '21', sysdate );
commit;


/* Insert Avg# of lines Shipped - Code OLN */

insert into usr_weeklystats(counts,trndte,status)
select round(count(a.ordlin)/decode(count(distinct a.ordnum),0,1,count(distinct a.ordnum))) counts,
trunc(LAST_DAY(sysdate-1)) trndte,'OLN' status
                          from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
                          car_move f,trlr g
                           where x.host_ext_id= d.btcust
                           and x.client_id = d.client_id
                           and d.ordnum = a.ordnum
                           and d.client_id = a.client_id
                           and d.client_id = b.client_id                          
                           and d.rt_adr_id = c.rt_adr_id                         
                           and a.ordnum = b.ordnum
                           and a.ordlin = b.ordlin
                           and a.client_id = b.client_id
                           and a.ordsln = b.ordsln
                           and b.ship_id = c.ship_id
                           and c.stop_id = e.stop_id
                           and e.car_move_id  = f.car_move_id
                           and f.trlr_id  = g.trlr_id
                           and d.client_id ||''= '----'
                           and c.shpsts ||''= 'C'
                           and x.adrtyp = 'CST'
 and trunc(g.dispatch_dte) between trunc(ADD_MONTHS(LAST_DAY(sysdate -1),-1)+1)
 and trunc(LAST_DAY(sysdate-1))
and trunc(sysdate) = trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1)  /* Run on first day of the month for previous month */
group by trunc(LAST_DAY(sysdate-1));

insert into usr_wstats_timer values( '22', sysdate );
commit; 



/* Insert Cartons Shipped - Code S */
/* updated 10/9/03 */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'S' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '23', sysdate );
commit;


/* Insert Full Case Cartons Shipped -Code FUL */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'FUL' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '24', sysdate );
commit;

/* Insert Piece Pick Cartons Shipped -Code PCK */

insert into usr_weeklystats(counts,trndte,status)
 select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'PCK' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '25', sysdate );
commit;

/* Insert Full Case Units Shipped -Code FUS */

insert into usr_weeklystats(counts,trndte,status)
select sum(untqty) usr_totctn,trunc(tr.dispatch_dte),'FUS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '26', sysdate );
commit;

/* Insert Piece Pick Units Shipped -Code PUS */

insert into usr_weeklystats(counts,trndte,status)
 select sum(untqty) usr_totctn,trunc(tr.dispatch_dte),'PUS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '27', sysdate );
commit;

/* Insert Full Case $ Shipped -Code FCD */

insert into usr_weeklystats(counts,trndte,status)
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) usr_totctn,trunc(tr.dispatch_dte),'FCD' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '28', sysdate );
commit;

/* Insert Piece Pick $ Shipped -Code PPD */

insert into usr_weeklystats(counts,trndte,status)
 select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) usr_totctn,trunc(tr.dispatch_dte),'PPD' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '29', sysdate );
commit;

/* Insert % of Piece Picks Cartons Shipped - Code PPC */
/* For this script the Piece Pick and Full Case Cartons Shipped Categores */
/* code (PCK, FUL) must have run already.  We can also use the */
/* usr_carton_counts table to get the same numbers */

insert into usr_weeklystats(counts,trndte,status)
select round(pck.counts/nvl(ful.counts,0)*100,2) counts, trunc(sysdate)-1 trndte, 'PPC' status
from (select to_char(trndte,'MON') dte,sum(counts) counts from usr_weeklystats
where trndte between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1)
and  last_day(trunc(sysdate-1)) and status in ('FUL','PCK')
group by to_char(trndte,'MON')) ful,
(select to_char(trndte,'MON') dte, sum(counts) counts from
usr_weeklystats where trndte between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1)
and last_day(trunc(sysdate-1))
and status = 'PCK'
group by to_char(trndte,'MON')) pck
where pck.dte = ful.dte;

insert into usr_wstats_timer values( '30', sysdate );
commit;

/* Insert % of Piece Pick $Value Picked - Code PPV */
/* Codes PPV, PPL and PPU update daily for the entire month. */
/* At the end of the month, the final calculation for the month */
/* should be done. */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1 trndte, round((pcpck.cost/total.cost * 100),2) cost, 'PPV' status
from
(select /*+ ALL_ROWS */ to_char(lstdte,'MON') pckdte, sum(b.untqty * untcst) cost
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1))
              group by to_char(lstdte,'MON')) total,
(select /*+ ALL_ROWS */ to_char(lstdte,'MON') pckdte,  sum(b.untqty * untcst) cost
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1))
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;

insert into usr_wstats_timer values( '31', sysdate );
commit;

/* insert % of Piece Pick Lines Picked - Code PPL */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1, round((pcpck.lines/total.lines *100),2) lines, 'PPL' status
from
(select to_char(lstdte,'MON') pckdte, count(a.ordlin) lines
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) total,
(select to_char(lstdte,'MON') pckdte, count(a.ordlin) lines
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;

insert into usr_wstats_timer values( '32', sysdate );
commit;

/* insert % of Piece Pick Units Picked - PPU */

insert into usr_weeklystats(trndte,counts,status)
select trunc(sysdate) -1, round((pcpck.units/total.units *100),2) units, 'PPU' status
from
(select to_char(lstdte,'MON') pckdte, sum(b.untqty) units
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) total,
(select to_char(lstdte,'MON') pckdte, sum(b.untqty) units
              from prtmst p,pckwrk a,invdtl b
              where a.wrkref = b.wrkref
              AND a.ship_line_id = b.ship_line_id
              AND a.prt_client_id = b.prt_client_id
              AND a.prtnum = b.prtnum
              AND b.prtnum = p.prtnum
              AND a.wrktyp = 'P'
              AND a.appqty  > 0
              AND subucc is null
              AND trunc(b.lstdte) between trunc(ADD_MONTHS(LAST_DAY(sysdate-1),-1)+1) and last_day(trunc(sysdate-1)) 
              group by to_char(lstdte,'MON')) pcpck
where total.pckdte = pcpck.pckdte;

insert into usr_wstats_timer values( '33', sysdate );
commit;


/* delete % Piece Pick Carton Shipped amount from 2 days ago */
/* do not delete on the second day of the month , would wipe out previous month */

delete from usr_weeklystats
where status in ('PPC','PPL','PPU','PPV')
and trunc(trndte) = trunc(sysdate) -2
and trunc(sysdate) != trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1)+1;

insert into usr_wstats_timer values( '34', sysdate );
commit;


/* Insert Total Dollar Value Shipped - Code VS */

insert into usr_weeklystats(counts,trndte,status)
select sum(nvl(ord_line.vc_cust_untcst ,0) * sd.shpqty) dlramt,
trunc(tr.dispatch_dte),'VS' status
 from shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
and (ord_line.prjnum is null or (ord_line.prjnum is not null and ord_line.ordsln = '0000'))
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '35', sysdate );
commit;


/* Insert Total Orders Shipped - Code OS */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ord.ordnum)) counts, trunc(tr.dispatch_dte),'OS' status
 from shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts     ||'' = 'C'
        AND ord.ordtyp                   != 'W'
and (ord_line.prjnum is null or (ord_line.prjnum is not null and ord_line.ordsln = '0000'))
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '36', sysdate );
commit;


/* Get Cartons Received from Domestic Suppliers - Code CRD */

insert into usr_weeklystats(counts,trndte,status) 
select trunc(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRD' status 
           from 
          (select distinct prtmst.untcas untcas,  
            rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum 
            ,rcvlin.trknum,rcvlin.supnum 
             from rcvtrk,rcvinv,rcvlin, prtmst, trlr
              where trlr.trlr_id = rcvtrk.trlr_id
              and rcvtrk.trknum = rcvlin.trknum 
              and rcvlin.trknum = rcvinv.trknum  
              and rcvlin.client_id = rcvinv.client_id  
              and rcvlin.supnum = rcvinv.supnum  
              and rcvlin.invnum = rcvinv.invnum  
              and rcvlin.prtnum = prtmst.prtnum   
              --and rcvtrk.expdte is not null  
              and trlr.trlr_stat = 'C'  
              and trunc(rcvtrk.clsdte) = trunc(sysdate)-1) x  
   where (length(trknum) < 6 or ASCII(upper(substr(trknum,6,1))) between 65 and 90  
              or (supnum not in ('0237','3747','2205','2680','2681','3056',  
                    '3117','3150','3250','4630','5491','5505','5528','6409','7887',  
                    '8595','8675','9467','CY02','CY05') and supnum in  
                 ('0143','1206','1301','1316'  
                   ,'1753','2112','4309','4460',  
                   '5167','5306','5321','5340','5510','5518','5534','9129',  
                '7885','8419','8673','8679','9328')))  
  group by trunc(clsdte);
  
insert into usr_wstats_timer values( '37', sysdate );
commit;






/* Get Cartons Received from International Suppliers - Code CRI */

insert into usr_weeklystats(counts,trndte,status)  
select trunc(sum(rcvqty/decode(untcas,0,1,untcas))),trunc(clsdte),'CRI' status                                
               from
           (select distinct prtmst.untcas untcas,                        
            rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum
            ,rcvlin.trknum,rcvlin.supnum
                 from rcvtrk,rcvinv,rcvlin, prtmst, trlr
                    where trlr.trlr_id = rcvtrk.trlr_id
                    and rcvtrk.trknum = rcvlin.trknum
                    and rcvlin.trknum = rcvinv.trknum
                    and rcvlin.client_id = rcvinv.client_id
                    and rcvlin.supnum = rcvinv.supnum
                    and rcvlin.invnum = rcvinv.invnum
                    and rcvlin.prtnum = prtmst.prtnum
                    --and rcvtrk.expdte is not null
                    and trlr.trlr_stat = 'C'
                    and trunc(rcvtrk.clsdte) =trunc(sysdate)-1) x
         where (length(trknum) >= 6 or trknum like ('03%')
                  or (supnum in ('0237','3747','2205','2680','2681','3056',
                     '3117','3150','3250','4630','5491','5505','5528','6409','7887',
                     '8595','8675','9467','CY02','CY05') and supnum not in
                  ('0143','1206','1301','1316'
                     ,'1753','2112','4309','4460',
                   '5167','5306','5321','5340','5510','5518','5534',
                   '7885','8419','8673','8679','9328','9129')))
                 and ASCII(upper(substr(trknum,6,1))) not between 65 and 90
             group by trunc(clsdte);

insert into usr_wstats_timer values( '38', sysdate );
commit;



/* Get Total Cartons Received (should add up to Intl & Domestic - Code CR */

insert into usr_weeklystats(counts,trndte,status)
 select trunc(sum(rcvqty/decode(untcas,0,1,untcas))), clsdte, 'CR'
         from
         (select distinct prtmst.untcas untcas,
                  rcvlin.prtnum,rcvlin.rcvqty,trunc(rcvtrk.clsdte) clsdte,rcvlin.invnum
                  ,rcvlin.trknum,rcvlin.supnum
                    from rcvtrk,rcvinv,rcvlin, prtmst, trlr
                    where trlr.trlr_id = rcvtrk.trlr_id
                    and rcvtrk.trknum = rcvlin.trknum
                    and rcvlin.trknum = rcvinv.trknum
                    and rcvlin.client_id = rcvinv.client_id
                    and rcvlin.supnum = rcvinv.supnum
                    and rcvlin.invnum = rcvinv.invnum
                    and rcvlin.prtnum = prtmst.prtnum
                    --and rcvtrk.expdte is not null
                    and trlr.trlr_stat = 'C'
                    and trunc(rcvtrk.clsdte) = trunc(sysdate)-1 )
  group by clsdte;

insert into usr_wstats_timer values( '39', sysdate );
commit;

 

/* Get Items(all parts) Received - Code IR */

insert into usr_weeklystats(counts,trndte,status)
select count(prtnum),trunc(clsdte),'IR' status
        from usr_cartons_received_view
    group by trunc(clsdte);

insert into usr_wstats_timer values( '40', sysdate );
commit;

/* Get Units Received - Code URC */

insert into usr_weeklystats(counts,trndte,status)
select sum(idnqty),trunc(clsdte),'URC' status
        from usr_cartons_received_view
    group by trunc(clsdte);

insert into usr_wstats_timer values( '41', sysdate );
commit;

/* Insert Orders Shipped Complete - Code SC */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'SC' status
    from usr_shippedcomplete_view
group by trunc(dispatch_dte);

insert into usr_wstats_timer values( '42', sysdate );
commit;

/* Insert OrdersShippedComplete On Time- Code OTC */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'OTC' status
    from usr_shippedcompleteontime_view
group by trunc(dispatch_dte);

insert into usr_wstats_timer values( '43', sysdate );
commit;

/* Insert Units Shipped Complete On Time - Code UTC */

insert into usr_weeklystats(counts,trndte,status)
select sum(shpqty) counts,trunc(dispatch_dte) trndte,'UTC' status
    from usr_shippedcompleteontime_view
group by trunc(dispatch_dte);

insert into usr_wstats_timer values( '44', sysdate );
commit; 


/* Insert OrdersShippedNotComplete On Time- Code OTN */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(ordnum)) counts,trunc(dispatch_dte) trndte,'OTN' status
    from usr_shippedntcompltontime_view
group by trunc(dispatch_dte);

insert into usr_wstats_timer values( '45', sysdate );
commit;

/* Insert Units Shipped Not Complete On Time - Code UTN */

insert into usr_weeklystats(counts,trndte,status)
select sum(shpqty) counts,trunc(dispatch_dte) trndte,'UTN' status
    from usr_shippedntcompltontime_view
group by trunc(dispatch_dte);

insert into usr_wstats_timer values( '46', sysdate );
commit;

/* Insert # of Items Returned by Customers - Code IRT */

insert into usr_weeklystats(counts,trndte,status)
select count(b.prtnum),trunc(trndte),'IRT' status
    from dlytrn a, prtmst b
    where a.prtnum = b.prtnum
    and a.reacod = 'CUSTRTRN'
    and a.actcod = 'ADJ'
    and a.prtnum != 'LABOR'
    AND a.trndte BETWEEN TRUNC(SYSDATE) - 1 AND TRUNC(SYSDATE)
    and trnqty >= 0
   group by trunc(trndte);

insert into usr_wstats_timer values( '46', sysdate );
commit;

/* Insert # of Units Returned by Customers - Code URT */

insert into usr_weeklystats(counts,trndte,status)
select sum(a.trnqty),trunc(trndte),'URT' status
    from dlytrn a,prtmst b
    where a.prtnum = b.prtnum
    and a.reacod in ('CUSTRTRN','CUSTRTNNS')
    and a.actcod = 'ADJ'
    and a.prtnum != 'LABOR'
    AND a.trndte BETWEEN TRUNC(SYSDATE) - 1 AND TRUNC(SYSDATE)
    and trnqty >0
   group by trunc(trndte);

insert into usr_wstats_timer values( '47', sysdate );
commit;

/* Insert Total Units Ordered - Code 'UNO' */
/* Updated 10/14/03  */

insert into usr_weeklystats(counts,trndte,status)
select sum(a.ordqty) counts,trunc(g.dispatch_dte),'UNO' status
                  from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
                  car_move f,trlr g
                   where x.host_ext_id = d.btcust
                   and x.client_id = d.client_id
                   and d.ordnum = a.ordnum
                   and d.client_id = a.client_id
                   and d.client_id = b.client_id
                   and d.rt_adr_id = c.rt_adr_id 
                   and a.ordnum = b.ordnum
                   and a.ordlin = b.ordlin
                   and a.client_id = b.client_id
                   and a.ordsln = b.ordsln
                   and b.ship_id = c.ship_id
                   and c.stop_id = e.stop_id
                   and e.car_move_id  = f.car_move_id
                   and f.trlr_id  = g.trlr_id
                   and a.client_id ||''= '----'
                   and c.shpsts ||''= 'C'
                   and x.adrtyp = 'CST'
and trunc(g.dispatch_dte) = trunc(sysdate) -1 
group by trunc(g.dispatch_dte);

insert into usr_wstats_timer values( '48', sysdate );
commit;

/* Insert Total Units Shipped - Code 'UNS' */

insert into usr_weeklystats(counts,trndte,status)
select  sum(b.shpqty) counts, trunc(g.dispatch_dte) trndte,'UNS'  status
              from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
              car_move f,trlr g
               where x.host_ext_id = d.btcust
               and x.client_id = d.client_id
               and d.ordnum = a.ordnum
               and d.client_id = a.client_id
               and d.client_id = b.client_id
               and d.rt_adr_id = c.rt_adr_id
               and a.ordnum = b.ordnum
               and a.ordlin = b.ordlin
               and a.client_id = b.client_id
               and a.ordsln = b.ordsln
               and b.ship_id = c.ship_id
               and c.stop_id = e.stop_id
               and e.car_move_id  = f.car_move_id
               and f.trlr_id  = g.trlr_id
               and a.client_id ||''= '----'
               and c.shpsts ||''= 'C'
and (a.prjnum is null or (a.prjnum is not null and a.ordsln = '0000'))
      and trunc(g.dispatch_dte)  = trunc(sysdate) - 1
    group by trunc(g.dispatch_dte);

insert into usr_wstats_timer values( '49', sysdate );
commit;

/* Insert Orders Rolling Over(once at beginning of month) - Code 'ORO' */


insert into usr_weeklystats(counts,trndte,status)
select count(distinct(d.ordnum)) counts,trunc(d.adddte) trndte,'ORO' status
           from ord d,ord_line a,shipment_line b,shipment c
             where d.ordnum = a.ordnum
             and d.client_id = a.client_id
             and d.client_id = b.client_id
             and d.rt_adr_id = c.rt_adr_id
             and a.ordnum = b.ordnum
             and a.ordlin = b.ordlin
             and a.client_id = b.client_id
             and a.ordsln = b.ordsln
             and b.ship_id = c.ship_id
             and a.client_id = '----'
             and c.shpsts != 'C'
             and c.loddte is null
        and to_date(substr(d.adddte,4,3),'MON') < to_date(substr(sysdate,4,3),'MON')
          and trunc(d.adddte) between trunc(ADD_MONTHS(sysdate,-1)) and trunc(sysdate) -1
   and substr(trunc(sysdate),1,2) = '01'
      group by trunc(d.adddte);

insert into usr_wstats_timer values( '50', sysdate );
commit;

/* Insert All Categories into summary table (once every quarter, 2 quarters prior ) */

insert into usr_weeklystatssum(month,count,status)
        select trunc(trndte) month, 
        counts,status 
           from usr_weeklystats 
            where trunc(trndte) between trunc(ADD_MONTHS((sysdate),-6)) and trunc(ADD_MONTHS((sysdate-1),-3)) 
         and substr(trunc(sysdate),1,6) in ('01-JAN','01-APR','01-JUL','01-OCT');

insert into usr_wstats_timer values( '51', sysdate );
commit;



/* Clear out Weeklystats Table (once every quarter, 2 quarters prior) */

delete from usr_weeklystats
where trunc(trndte) between trunc(ADD_MONTHS((sysdate),-6)) and trunc(ADD_MONTHS((sysdate-1),-3))
and substr(trunc(sysdate),1,6) in ('01-JAN','01-APR','01-JUL','01-OCT');

insert into usr_wstats_timer values( '52', sysdate );
commit;



/* Insert Number of BOM Units Produced -Code PUP */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'PUP' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') != 'XBOM'
        and wkosts = 'C' and exists (select 'x' from wkodtl i
        where i.wkonum = wkohdr.wkonum and
        i.prtnum in ('PRODOHLABR','LABOR'))
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);

insert into usr_wstats_timer values( '53', sysdate );
commit;

/* Insert Number of Rework Units Produced - Code RWP */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'RWP' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') != 'XBOM'
        and wkosts = 'C' and not exists (select 'x' from wkodtl i
        where i.wkonum = wkohdr.wkonum and
        i.prtnum in ('PRODOHLABR','LABOR'))
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);

insert into usr_wstats_timer values( '54', sysdate );
commit;


/* Insert Number of XBOM Units Produced - Code XBM */

insert into usr_weeklystats(counts,trndte,status)
select sum(i.prdqty),trunc(i.clsdte),'XBM' status from
    (select distinct wkohdr.wkonum,wkohdr.prtnum,wkohdr.prdqty,trunc(wkohdr.clsdte)
    clsdte from
        wkohdr,wkodtl,prtmst
        where
         wkohdr.client_id = wkodtl.client_id
         and wkohdr.wkonum = wkodtl.wkonum
         and wkohdr.wkorev = wkodtl.wkorev
         and wkohdr.prtnum = prtmst.prtnum
         and nvl(vc_itmcls,'X') = 'XBOM'
        and wkosts = 'C' 
        and trunc(wkohdr.clsdte) =trunc(sysdate) -1) i
  group by trunc(i.clsdte);

insert into usr_wstats_timer values( '55', sysdate );
commit;

/* Insert Number of Pallets Shipped */

insert into usr_weeklystats(counts,trndte,status)
select count(distinct invlod.lodnum) ,trunc(g.dispatch_dte) trndte, 'PAS' status
                      from ord d,ord_line a,shipment_line b,shipment c, stop e,
                       car_move f,trlr g, invlod, invsub, invdtl i
                       where i.ship_line_id = b.ship_line_id
                       and i.subnum = invsub.subnum
                       and invlod.lodnum = invsub.lodnum
                       and d.ordnum = a.ordnum
                       and d.client_id = a.client_id
                       and d.client_id = b.client_id
                       and d.rt_adr_id = c.rt_adr_id
                       and a.ordnum = b.ordnum
                       and a.ordlin = b.ordlin
                       and a.client_id = b.client_id
                       and a.ordsln = b.ordsln
                       and b.ship_id = c.ship_id
                       and c.stop_id = e.stop_id
                       and e.car_move_id  = f.car_move_id
                       and f.trlr_id  = g.trlr_id
                       and a.client_id = '----'
                       and c.shpsts = 'C'
            and trunc(g.dispatch_dte)  = trunc(sysdate) -1
        group by  trunc(g.dispatch_dte);

insert into usr_wstats_timer values( '56', sysdate );
commit;

/* Insert Dollars Received */

insert into usr_weeklystats(counts,trndte,status)
select sum(idnqty * untcst), trunc(rcvtrk.clsdte) clsdte, 'DOR' status
                       from rcvtrk,rcvinv,prtmst,rcvlin,trlr
                       where trlr.trlr_id = rcvtrk.trlr_id
                       and rcvtrk.trknum = rcvlin.trknum
                       and rcvlin.trknum = rcvinv.trknum
                       and rcvlin.client_id = rcvinv.client_id
                       and rcvlin.supnum = rcvinv.supnum
                       and rcvlin.invnum = rcvinv.invnum
                       and rcvlin.prtnum = prtmst.prtnum
                    --and rcvtrk.expdte is not null              /* This line eliminates anything from Dayton B3's */
                    and rcvlin.idnqty <> 0                    /* This eliminates duplicate records */
                    and trlr.trlr_stat = 'C'
                    and rcvinv.invtyp = 'P'
                    and rcvinv.client_id = '----'
         and trunc(rcvtrk.clsdte) = trunc(sysdate) -1
   group by trunc(rcvtrk.clsdte);

insert into usr_wstats_timer values( '57', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select sum(dlytrn.trnqty),trunc(trndte),'TIC' 
 FROM dlytrn
 where (substr(dlytrn.tostol,1,7) = 'OTICKET'
 or dlytrn.tostol='OTREWORK')
 and dlytrn.frstol!=dlytrn.tostol
AND dlytrn.trndte BETWEEN TRUNC(SYSDATE) -1 AND TRUNC(SYSDATE)
group by trunc(trndte);

insert into usr_wstats_timer values( '58', sysdate );
commit;

/* Information for Walmart Shipments */

create table usr_shpqty_stats as
select count(distinct(i.subnum)) usr_totctn, ord.btcust, trunc(tr.dispatch_dte) dte
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FUW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FUO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '59', sysdate );
commit;

truncate table usr_shpqty_stats;
insert into usr_shpqty_stats
 select count(distinct(i.subnum)) usr_totctn,ord.btcust, trunc(tr.dispatch_dte)
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PUW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PUO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '60', sysdate );
commit;

truncate table usr_shpqty_stats;

insert into usr_shpqty_stats
select sum(untqty) usr_totctn,ord.btcust,trunc(tr.dispatch_dte)
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FSW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FSO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '61', sysdate );
commit;

truncate table usr_shpqty_stats;

insert into usr_shpqty_stats
 select sum(untqty) usr_totctn, ord.btcust, trunc(tr.dispatch_dte)
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PSW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PSO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '62', sysdate );
commit;

truncate table usr_shpqty_stats;

insert into usr_shpqty_stats
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) usr_totctn, ord.btcust, trunc(tr.dispatch_dte)
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FCW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'FCO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '63', sysdate );
commit;

truncate table usr_shpqty_stats;

insert into usr_shpqty_stats
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) usr_totctn, ord.btcust, trunc(tr.dispatch_dte)
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PCW'
from usr_shpqty_stats
where btcust in ('121059','140802','179105')
group by dte;

insert into usr_weeklystats(counts,trndte,status)
select sum(usr_totctn), dte, 'PCO' 
from usr_shpqty_stats
where btcust not in ('121059','140802','179105')
group by dte;

insert into usr_wstats_timer values( '64', sysdate );
commit;

truncate table usr_shpqty_stats;

drop table usr_shpqty_stats;

insert into usr_weeklystats(counts,trndte,status)
select count(distinct invlod.lodnum) ,trunc(g.dispatch_dte) trndte, 'DPS' status
                      from ord d,ord_line a,shipment_line b,shipment c, stop e,
                       car_move f,trlr g, invlod, invsub, invdtl i
                       where i.ship_line_id = b.ship_line_id
                       and i.subnum = invsub.subnum
                       and invlod.lodnum = invsub.lodnum
                       and d.ordnum = a.ordnum
                       and d.client_id = a.client_id
                       and d.client_id = b.client_id
                       and d.rt_adr_id = c.rt_adr_id 
                       and d.btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
                       and a.ordnum = b.ordnum
                       and a.ordlin = b.ordlin
                       and a.client_id = b.client_id
                       and a.ordsln = b.ordsln
                       and b.ship_id = c.ship_id
                       and c.stop_id = e.stop_id
                       and e.car_move_id  = f.car_move_id
                       and f.trlr_id  = g.trlr_id
                       and a.client_id = '----'
                       and c.shpsts = 'C'
            and trunc(g.dispatch_dte)  = trunc(sysdate) -1
        group by  trunc(g.dispatch_dte);

insert into usr_wstats_timer values( '65', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select count(distinct invlod.lodnum) ,trunc(g.dispatch_dte) trndte, 'OPS' status
                      from ord d,ord_line a,shipment_line b,shipment c, stop e,
                       car_move f,trlr g, invlod, invsub, invdtl i
                       where i.ship_line_id = b.ship_line_id
                       and i.subnum = invsub.subnum
                       and invlod.lodnum = invsub.lodnum
                       and d.ordnum = a.ordnum
                       and d.client_id = a.client_id
                       and d.client_id = b.client_id
                       and d.rt_adr_id = c.rt_adr_id 
                    and d.btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
                       and a.ordnum = b.ordnum
                       and a.ordlin = b.ordlin
                       and a.client_id = b.client_id
                       and a.ordsln = b.ordsln
                       and b.ship_id = c.ship_id
                       and c.stop_id = e.stop_id
                       and e.car_move_id  = f.car_move_id
                       and f.trlr_id  = g.trlr_id
                       and a.client_id = '----'
                       and c.shpsts = 'C'
            and trunc(g.dispatch_dte)  = trunc(sysdate) -1
        group by  trunc(g.dispatch_dte);

insert into usr_wstats_timer values( '66', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'DCS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
       	AND ord.btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '67', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select count(distinct(i.subnum)) usr_totctn,trunc(tr.dispatch_dte),'WCS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
       	AND ord.btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '68', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) dlramt,
trunc(tr.dispatch_dte),'DDS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
       	AND ord.btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '69', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) dlramt,
trunc(tr.dispatch_dte),'WDS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
       	AND ord.btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1 
group by trunc(tr.dispatch_dte);

insert into usr_wstats_timer values( '70', sysdate );
commit;



create table usr_ordcart_stats as
SELECT o.btcust, COUNT(DISTINCT(sl.ordnum)) num_ords, 
           COUNT(DISTINCT(dtl.subnum)) num_ctns, trunc(sysdate-1) trndte
  FROM FTPMST f, ORD o, PRTMST p, SHIPMENT_LINE sl, INVDTL dtl, DLYTRN dt
 WHERE dt.oprcod = 'UPK'
   AND dt.actcod = 'S PICK'
   AND dt.trndte BETWEEN TRUNC(SYSDATE) - 1 AND TRUNC(SYSDATE)
   AND dtl.dtlnum = dt.dtlnum
   AND sl.ship_line_id = dtl.ship_line_id
   AND p.prtnum = dtl.prtnum
   AND p.prt_client_id = dtl.prt_client_id
   AND o.ordnum = sl.ordnum
   AND o.client_id = sl.client_id
   AND f.ftpcod = p.ftpcod
group by o.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(num_ords), trndte,'DOP'
from usr_ordcart_stats
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;


insert into usr_weeklystats(counts,trndte,status)
select sum(num_ords), trndte,'WOP'
from usr_ordcart_stats
where btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;


insert into usr_weeklystats(counts,trndte,status)
select sum(num_ctns), trndte,'DCP'
from usr_ordcart_stats
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST')  
group by trndte;


insert into usr_weeklystats(counts,trndte,status)
select sum(num_ctns), trndte,'WCP'
from usr_ordcart_stats
where btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_wstats_timer values( '71', sysdate );
commit;

truncate table usr_ordcart_stats;

drop table usr_ordcart_stats;

commit;

create table usr_linepick_info as
SELECT o.btcust, count(sl.ordlin) lines, sum(dtl.untqty) untqty,
           trunc(sysdate-1) trndte
  FROM FTPMST f, ORD o, PRTMST p, SHIPMENT_LINE sl, INVDTL dtl, DLYTRN dt
 WHERE dt.oprcod = 'UPK'
   AND dt.actcod = 'S PICK'
   AND dt.trndte BETWEEN TRUNC(SYSDATE) - 1 AND TRUNC(SYSDATE)
   AND dtl.dtlnum = dt.dtlnum
   AND sl.ship_line_id = dtl.ship_line_id
   AND p.prtnum = dtl.prtnum
   AND p.prt_client_id = dtl.prt_client_id
   AND o.ordnum = sl.ordnum
   AND o.client_id = sl.client_id
   AND f.ftpcod = p.ftpcod
group by o.btcust;


insert into usr_weeklystats (counts, trndte, status)
select sum(lines), trndte, 'DLP'
from usr_linepick_info
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats (counts, trndte, status)
select sum(lines), trndte, 'WLP'
from usr_linepick_info
where btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats (counts, trndte, status)
select sum(untqty), trndte, 'DUP'
from usr_linepick_info
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats (counts, trndte, status)
select sum(untqty), trndte, 'WUP'
from usr_linepick_info
where btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_wstats_timer values( '72', sysdate );
commit;

truncate table usr_linepick_info;

drop table usr_linepick_info;

insert into usr_wstats_timer values( '73', sysdate );
commit;

create table usr_cube_info as
select btcust, round(sum((untqty/untcas)*(cube/1728)),2) qube, trndte
from (select o.btcust, dtl.prtnum, sum(dtl.untqty) untqty, p.untcas, 
(f.caslen * f.caswid * f.cashgt) cube, trunc(sysdate-1) trndte
FROM FTPMST f, ORD o, PRTMST p, SHIPMENT_LINE sl, INVDTL dtl, DLYTRN dt
 WHERE dt.oprcod = 'UPK'
   AND dt.actcod = 'S PICK'
   AND dt.trndte BETWEEN TRUNC(SYSDATE) - 1 AND TRUNC(SYSDATE)
   AND dtl.dtlnum = dt.dtlnum
   AND sl.ship_line_id = dtl.ship_line_id
   AND p.prtnum = dtl.prtnum
   AND p.prt_client_id = dtl.prt_client_id
   AND o.ordnum = sl.ordnum
   AND o.client_id = sl.client_id
   AND f.ftpcod = p.ftpcod
group by o.btcust, dtl.prtnum, p.untcas, 
f.caslen, f.caswid, f.cashgt)
group by btcust, trndte;

insert into usr_weeklystats (counts, trndte, status)
select sum(qube), trndte, 'DBP'
from usr_cube_info
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats (counts, trndte, status)
select sum(qube), trndte, 'WBP'
from usr_cube_info
where btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_wstats_timer values( '74', sysdate );
commit;

truncate table usr_cube_info;

drop table usr_cube_info;

create table flash_temp as
select  sum(b.shpqty) counts, sum(nvl(a.vc_cust_untcst ,0) * b.shpqty) cst, d.btcust,
trunc(g.dispatch_dte) trndte,'UNS'  status
              from adrmst x,ord d,ord_line a,shipment_line b,shipment c, stop e,
              car_move f,trlr g
               where x.host_ext_id = d.btcust
               and x.client_id = d.client_id
               and d.ordnum = a.ordnum
               and d.client_id = a.client_id
               and d.client_id = b.client_id
               and d.rt_adr_id = c.rt_adr_id
               and a.ordnum = b.ordnum
               and a.ordlin = b.ordlin
               and a.client_id = b.client_id
               and a.ordsln = b.ordsln
               and b.ship_id = c.ship_id
               and c.stop_id = e.stop_id
               and e.car_move_id  = f.car_move_id
               and f.trlr_id  = g.trlr_id
               and a.client_id ||''= '----'
               and c.shpsts ||''= 'C'
and (a.prjnum is null or (a.prjnum is not null and a.ordsln = '0000'))
      and trunc(g.dispatch_dte)  = trunc(sysdate) - 1
    group by trunc(g.dispatch_dte), d.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'USD' 
 from flash_temp
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'USO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'USW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'UTD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'UDD' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'UWD' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '75', sysdate );
commit;

truncate table flash_temp;

insert into flash_temp (counts, cst, btcust, trndte,status)
select sum(counts) counts, sum(cst) cst, btcust, dispatch_dte, status from
(select (select count(distinct invdtl.subnum) usr_totctn 
		from shipment_line, invdtl where shipment_line.ship_id=sd.ship_id 
		and shipment_line.ordnum=sd.ordnum 
		and shipment_line.ship_line_id=invdtl.ship_line_id)
 counts, 
sum(nvl(ord_line.vc_cust_untcst ,0) * sd.shpqty) cst, ord.btcust, trunc(tr.dispatch_dte) dispatch_dte,'S'
 status
 from shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
and (ord_line.prjnum is null or (ord_line.prjnum is not null and ord_line.ordsln = '0000'))
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte), ord.btcust, sd.ship_id, sd.ordnum)
group by btcust, dispatch_dte, status;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SD' 
 from flash_temp
where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'SDD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'SOD' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'SWD' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '76', sysdate );
commit;

truncate table flash_temp;

insert into flash_temp (counts,cst, btcust, trndte,status)
select count(distinct(i.subnum)) usr_totctn, sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) cst, ord.btcust, trunc(tr.dispatch_dte),'FUL' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'FD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'FO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'FW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'FDD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'FOD' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'FWD' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '77', sysdate );
commit;

truncate table flash_temp;

insert into flash_temp (counts,cst, btcust, trndte,status)
select count(distinct(i.subnum)) usr_totctn, sum(nvl(ord_line.vc_cust_untcst ,0) * i.untqty) cst,ord.btcust, trunc(tr.dispatch_dte),'PCK' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte), ord.btcust;


insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'PD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'PO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'PW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'PDD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'POD' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'PWD' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '78', sysdate );
commit;

truncate table flash_temp;

insert into flash_temp (counts,cst, btcust, trndte,status)
select count(distinct(ord.ordnum)) counts, sum(nvl(ord_line.vc_cust_untcst ,0) * sd.shpqty) cst,ord.btcust, trunc(tr.dispatch_dte),'OS' status
 from shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts     ||'' = 'C'
        AND ord.ordtyp                   != 'W'
and (ord_line.prjnum is null or (ord_line.prjnum is not null and ord_line.ordsln = '0000'))
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
group by trunc(tr.dispatch_dte),ord.btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'OD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'OO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'OW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'ODD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'OOD' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(cst), trndte, 'OWD' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '79', sysdate );
commit;

truncate table flash_temp;

insert into flash_temp (counts, btcust, trndte,status)
select sum((x.shpqty/x.untcas)*(x.cube/1728)) qube,  btcust, dispatch_dte, 'CUE'
                     from
                     (select distinct shipment.ship_id, i.prtnum, ord.ordnum, ord.btcust, 
                                          ord_line.ordlin,shipment_line.shpqty shpqty, 
                          (ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl i,ftpmst, prtmst, shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND trunc(dispatch_dte) = trunc(sysdate)-1) x
       group by dispatch_dte, btcust;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'CD' 
 from flash_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'CO' 
 from flash_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;


insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'CW' 
 from flash_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '80', sysdate );
commit;

drop table flash_temp;

insert into usr_wstats_timer values( '81', sysdate );
commit;

create table units_temp as
select sum(untqty) counts,ord.btcust, trunc(tr.dispatch_dte) trndte,'FUS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) = 'S'  /* Full cases */ 
group by trunc(tr.dispatch_dte), ord.btcust;

insert into usr_wstats_timer values( '82', sysdate );
commit;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'DFU' 
 from units_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SFU' 
 from units_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'WFU' 
 from units_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '83', sysdate );
commit;

truncate table units_temp;

insert into units_temp(counts,btcust,trndte,status)
 select sum(untqty) usr_totctn,ord.btcust, trunc(tr.dispatch_dte),'PUS' status
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */
group by trunc(tr.dispatch_dte), ord.btcust;


insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'DPU' 
 from units_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SPU' 
 from units_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'WPU' 
 from units_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '84', sysdate );
commit;

truncate table units_temp;

drop table units_temp;

create table weight_temp as 
select x.btcust, sum(x.shpqty * x.vc_grswgt_each) counts,trunc(x.dispatch_dte) trndte, 'WGT' status  
from 
(select distinct shipment.ship_id, i.prtnum, ord.ordnum, ord.btcust, 
  ord_line.ordlin,shipment_line.shpqty shpqty,prtmst.vc_grswgt_each, 
    prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte 
    from invdtl i, prtmst, shipment, shipment_line,  ord, ord_line,  stop, 
         trlr tr,car_move cr 
         where i.ship_line_id = shipment_line.ship_line_id 
        AND prtmst.prtnum = ord_line.prtnum   
        AND ord.ordnum                  = ord_line.ordnum  
        AND ord.client_id               = ord_line.client_id  
        AND ord_line.client_id          = shipment_line.client_id  
        AND ord_line.ordnum             = shipment_line.ordnum  
        AND ord_line.ordlin             = shipment_line.ordlin  
        AND ord_line.ordsln             = shipment_line.ordsln  
        AND shipment_line.ship_id       = shipment.ship_id  
        AND shipment.stop_id                  = stop.stop_id  
        AND stop.car_move_id            = cr.car_move_id   
        AND cr.trlr_id                  = tr.trlr_id  
        AND shipment.shpsts  ||''= 'C'  
        AND ord.ordtyp                   != 'W'  
        AND trunc(tr.dispatch_dte) = trunc(sysdate)-1) x  
  group by x.btcust, trunc(x.dispatch_dte); 

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'DPW' 
 from weight_temp
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'SPW' 
 from weight_temp
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trndte, 'WPW' 
 from weight_temp
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

insert into usr_wstats_timer values( '85', sysdate );
commit;

truncate table weight_temp;

drop table weight_temp;

create table prtlist_temp as (select distinct c.prtnum
from aremst e, locmst d, invlod a, invsub b, invdtl c
where e.fwiflg=1
and e.arecod=d.arecod
and d.stoloc=a.stoloc
and a.lodnum=b.lodnum
and b.subnum=c.subnum
and c.prt_client_id='----');

create table prtcnttemp as
(select nvl(a.vc_status,'0') vc_status, count(*) prtcnt
from prtmst a, prtlist_temp b
where a.prtnum=b.prtnum
group by a.vc_status);

insert into usr_weeklystats (counts,trndte,status)
select count(*), trunc(sysdate), 'FWP'
from prtlist_temp
group by trunc(sysdate);

insert into usr_weeklystats (counts,trndte,status)
select sum(prtcnt), trunc(sysdate),  'FWA'
from prtcnttemp
where vc_status='10';

insert into usr_weeklystats (counts,trndte,status)
select sum(prtcnt), trunc(sysdate),  'FWI'
from prtcnttemp
where vc_status='40';

insert into usr_weeklystats (counts,trndte,status)
select sum(prtcnt), trunc(sysdate),  'FPO'
from prtcnttemp
where vc_status in ('21','22','23');

insert into usr_weeklystats (counts,trndte,status)
select sum(prtcnt), trunc(sysdate),  'FWO'
from prtcnttemp
where vc_status not in ('10','40','21','22','23');

drop table prtlist_temp;
drop table prtcnttemp;

create table usr_linepick_info as
select distinct ord.btcust, pckwrk.ordnum, pckwrk.ordlin
from pckwrk, ord
where pckwrk.ordnum=ord.ordnum
and pckwrk.pckdte between trunc(sysdate-1) and trunc(sysdate)
and pckwrk.wrktyp='P'
and pckwrk.prt_client_id='----'
and pckwrk.appqty>0;

insert into usr_weeklystats(counts,trndte,status)
select count(*), trunc(sysdate-1), 'TPL' 
 from usr_linepick_info;

insert into usr_weeklystats(counts,trndte,status)
select count(*), trunc(sysdate-1), 'RPL' 
 from usr_linepick_info
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST');

insert into usr_weeklystats(counts,trndte,status)
select count(*), trunc(sysdate-1), 'DPL' 
 from usr_linepick_info
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST');

insert into usr_weeklystats(counts,trndte,status)
select count(*), trunc(sysdate-1), 'WPL' 
 from usr_linepick_info
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS');

insert into usr_wstats_timer values( '86', sysdate );
commit;

drop table usr_linepick_info;

create table usr_lineship_info as
 select count(*) counts,btcust, trunc(dispatch_dte) trndte,'PUS' status
 from (select distinct ord_line.ordnum, ord_line.ordlin, ord.btcust, trunc(tr.dispatch_dte) dispatch_dte
from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1) in ('P','C')  /* Piece Pick cases */)
group by trunc(dispatch_dte), btcust;


insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LPR'
 from usr_lineship_info
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LPD'
 from usr_lineship_info
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LPW'
 from usr_lineship_info
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;


commit;
truncate table usr_lineship_info;

insert into usr_lineship_info (counts, btcust, trndte, status)
select count(*) counts,btcust, trunc(dispatch_dte) trndte,'PUS' status
 from (select distinct ord_line.ordnum, ord_line.ordlin, ord.btcust, trunc(tr.dispatch_dte) dispatch_dte
   from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop,
        trlr tr,car_move cr
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND trunc(tr.dispatch_dte) = trunc(sysdate) -1
        AND substr(i.subnum,1,1)= 'S'  /* Full Case */)
group by trunc(dispatch_dte), btcust;


insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LFR'
 from usr_lineship_info
 where btcust in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LFD'
 from usr_lineship_info
 where btcust in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
  and btcust not in (select rtstr1
			from poldat
			where polcod='USR'
			and polvar='DROP-SHIP'
			and polval='DTC-CUST') 
group by trndte;

insert into usr_weeklystats(counts,trndte,status)
select sum(counts), trunc(trndte), 'LFW'
 from usr_lineship_info
 where btcust not in (select rtstr1 from poldat
                        where polcod='USR'
                        and polvar='DROP-SHIP'
                        and polval='CUSTOMERS')
group by trndte;

commit;
truncate table usr_lineship_info;
drop table usr_lineship_info;

insert into usr_wstats_timer values( 'End', sysdate );
commit;
