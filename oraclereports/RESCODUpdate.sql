/* Author:  Al Driver */

/* This script is used to identify locations with reason codes */
/* with shipment id's that are closed. Any identified locations  */
/* should be sent to Renee Bogart before deletion. */

select a.stoloc,substr(a.rescod,1,10) rescod,trunc(b.loddte) shpdte,b.shpsts from
locmst a,shipment b
where a.rescod = b.ship_id
and a.rescod is not null
and b.shpsts = 'C'
and trunc(b.loddte) <= trunc(sysdate) -2
order by shpdte;


update locmst a  
set a.rescod = NULL 
where exists ( 
select 'x' from shipment b 
where b.ship_id = a.rescod 
and b.shpsts = 'C' 
and trunc(b.loddte) <= trunc(sysdate) -2); 
 commit;  
