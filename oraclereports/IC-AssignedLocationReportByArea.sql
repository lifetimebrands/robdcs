/* #REPORTNAME=IC Assigned Location Report By Area */
/* #VARNAM=arecod , #REQFLG=Y */
/* #HELPTEXT= Please enter Area */
/* #HELPTEXT = Requested by Inventory Control March 19, 2003 */

ttitle left print_time center 'IC Assigned Location Report By Area' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a20
column stoloc heading 'Location'
column stoloc format a20
column maxqvl heading 'Max Qvl'
column maxqvl format 999999
column prtnum heading 'Item'
column prtnum format a14
column prtnuma heading 'Replen Item'
column prtnuma format a14
column vc_untpak heading 'Replen Unit Pack'
column vc_untpak format 9999
column minunt heading 'Mininum'
column minunt format 999999
column maxunt heading 'Maximum'
column maxunt format 999999
column untpak heading 'Units/Pack'
column untpak format 9999
column untcas heading 'Units/Case'
column untcas format 999999
column untpal heading 'Units/Pallet'
column untpal format 999999
column untqty heading 'On Hand'
column untqty format 999999
column comqty heading 'Commited Qty'
column comqty format 999999
column reploc heading 'Replen Loc'
column reploc format a14

set linesize 300
set pagesize 1000

select locmst.arecod,
locmst.stoloc,
invsum.prtnum,
rplcfg.prtnum prtnuma,
invsum.untqty,
invsum.comqty,
locmst.maxqvl,
invsum.untpak,
invsum.untcas,
invsum.untpal,
rplcfg.vc_untpak,
rplcfg.minunt,
rplcfg.maxunt
from
rplcfg, invsum, locmst
where
invsum.arecod='&1'
and invsum.stoloc = locmst.stoloc
and invsum.arecod = locmst.arecod
and invsum.stoloc = rplcfg.stoloc
and invsum.prt_client_id = rplcfg.prt_client_id
union
select locmst.arecod,
locmst.stoloc,
'No Inventory' prtnum,
rplcfg.prtnum prtnuma,
0, 0,
locmst.maxqvl,
prtmst.untpak,
prtmst.untcas,
prtmst.untpal,
rplcfg.vc_untpak,
rplcfg.minunt,
rplcfg.maxunt
from rplcfg, prtmst, locmst
where locmst.arecod ='&1'
and rplcfg.stoloc = locmst.stoloc
and rplcfg.arecod = locmst.arecod
and rplcfg.prtnum = prtmst.prtnum
and prtmst.prt_client_id = '----'
and rplcfg.stoloc not in (select invsum.stoloc from invsum
where rplcfg.stoloc = invsum.stoloc 
and rplcfg.prt_client_id = invsum.prt_client_id)
/
