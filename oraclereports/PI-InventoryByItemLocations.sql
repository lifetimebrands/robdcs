/* #REPORTNAME=PI Inventory By Item By Location Report */                 
/* #HELPTEXT= This report lists all inventory by item, location  and quantity */         
/* #HELPTEXT= Requested by Physical Inventory */       
/* #GROUPS=NOONE */

/* This is one of the reports that will be used during the physical inventory */


ttitle left print_time -
center 'PI Inventory By Item and Location  Report ' -                         
right print_date skip 2 -

column prtnum heading 'Item'             
column prtnum format A30      
column stoloc headng 'Location '
column stoloc format a30
column lngdsc heading 'Description'    
column lngdsc format  A30
column untqty heading 'Qty'               
column untqty format 9999990  

set linesize 132
set pagesize 40000

select prtmst.prtnum, rpad (lngdsc, 30, ' '), invlod.stoloc, sum(untqty) untqty
from invdtl, invsub, invlod, locmst, aremst, prtmst, prtdsc
where aremst.fwiflg = '1'
and aremst.arecod  = locmst.arecod
and locmst.stoloc  = invlod.stoloc
and invlod.lodnum  = invsub.lodnum
and invsub.subnum  = invdtl.subnum
and invdtl.prtnum = prtmst.prtnum
and prtdsc.colnam (+) = 'prtnum|prt_client_id'
and prtdsc.locale_id (+) = 'US_ENGLISH'
and prtdsc.colval (+) = prtmst.prtnum||'|----' 
group by prtmst.prtnum, invlod.stoloc, lngdsc
/
