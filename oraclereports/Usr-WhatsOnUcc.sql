/* #REPORTNAME=User What's on a UCC*/
/* #REPORTTYPE=DCSCMD */
/* #HELPTEXT= Displays What is on a UCC*/ 
/* #HELPTEXT= Requested by Order Management */
/* #VARNAM=subucc , #REQFLG=Y */ 


/* decodes are to try and get the right values in the case of a carton split */

set linesize 300

select b.ctnnum, b.srcloc, b.prtnum, decode(b.pckqty, b.appqty, nvl(d.untqty, b.appqty), b.appqty) appqty,
       decode(b.pckqty, b.appqty, nvl(d.untqty, b.pckqty), b.pckqty) pckqty, b.ordnum, b.ship_id
from invdtl d, pckwrk a, pckwrk b, locmst c
where a.subucc='&1'
and a.subnum=b.ctnnum
and c.stoloc(+)=b.srcloc
and d.wrkref (+)= b.wrkref
and d.subnum (+)= b.ctnnum
order by c.trvseq

/
