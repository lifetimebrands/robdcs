/* #REPORTNAME =User Item and Description */
/* #HELPTEXT= Requested by Senior Management */
/* #VARNAM=prtnum , #REQFLG=Y */

column prtnum heading 'Item'
column prtnum format A15
column lngdsc heading 'Description'
column lngdsc format A40

set pagesize 50000;
set linesize 500;


SELECT
rtrim(prtmst.prtnum) prtnum ,
prtdsc.lngdsc lngdsc
from prtmst, prtdsc
where 
prtmst.prt_client_id = '----'
and prtmst.prtnum='&1'
and prtmst.prtnum|| '|----' = prtdsc.colval
and prtdsc.colnam='prtnum|prt_client_id'
and prtdsc.locale_id ='US_ENGLISH'
order by prtmst.prtnum, prtdsc.lngdsc
/

