/* #REPORTNAME=User Shipment by Date Range Sample Report */
/* #HELPTEXT= The Shipped Orders Summary by Date */
/* #HELPTEXT= yeilds a summary of shipped orders.*/
/* #HELPTEXT= It requires a date range entered in the*/
/* #HELPTEXT= form DD-Mon-YYYY.*/
/* #HELPTEXT= Requested by Shipping */

/* #VARNAM=Dispatched-Date1 , #REQFLG=Y */
/* #VARNAM=Dispatched-Date2 , #REQFLG=Y */

ttitle left  print_time -
       center 'User Shipment By Date Range Sample: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column ordnum heading 'Order No.' just center
column ordnum format A12 
column clsdte heading 'Close Date' just center
column clsdte format A12
column late heading 'Late'
column late format A12
column prtnum heading 'Item'
column prtnum format A12
column untqty heading 'Qty'
column untqty format 999999
column vc_extdte heading 'Extension'
column vc_extdte format A12
column stname heading 'Customer Name'just center
column stname format A30
column cityst heading 'Destination' just center
column cityst format A30
column cponum heading 'PO Number' just center
column cponum format A20
column carcod heading 'Shipped VIA' just center
column carcod format A10 
column pronum heading 'PRO Number' just center
column pronum format A20
column usr_totctn heading 'CNT' just center
column usr_totctn format 9999990 
column waybil heading 'WayBill' just center
column waybil format a25
column dlramt heading 'Amt Shipped' just center
column dlramt format 999999.00

set pagesize 5000
set linesize 300


select rtrim(ltrim(sd.ordnum)) ordnum,
       rtrim(ltrim(sh.doc_num)) waybil,
       to_char(tr.dispatch_dte, 'DD-MON-YYYY') clsdte,
       to_char(ord_line.late_shpdte, 'DD-MON-YYYY') late,
       to_char(ord_line.vc_extdte, 'DD-MON-YYYY') vc_extdte,
       ord_line.prtnum,
       i.untqty,
       rtrim(lookup1.adrnam) stname,
       rtrim(lookup1.adrcty)||', '||lookup1.adrstc cityst,
       rtrim(ltrim(ord.cponum)) cponum,
       rtrim(ltrim(sh.carcod)) carcod,
       rtrim(ltrim(sh.track_num)) pronum,
       count(distinct(i.subnum)) usr_totctn,
      (prtmst.untcst*i.untqty) dlramt
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop, adrmst lookup1,
        trlr tr,car_move cr, prtmst
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.st_adr_id               = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND ord_line.prtnum             = prtmst.prtnum
        AND prtmst.prt_client_id          ='----'
        AND sd.ship_id                  = sh.ship_id
        AND sh.stop_id                  = stop.stop_id
        AND stop.car_move_id            = cr.car_move_id
        AND cr.trlr_id                  = tr.trlr_id
        AND sh.shpsts                   = 'C'
        AND ord.ordtyp                   != 'W'
        AND substr(ord.cponum,1,1)      = '='
        AND tr.dispatch_dte between to_date('&&1','DD-MON-YYYY') and
                      to_date('&&2','DD-MON-YYYY')+.99999
group by 
tr.dispatch_dte,
       sd.ordnum,
       ord_line.late_shpdte,
       ord_line.vc_extdte,
       lookup1.adrnam,
       lookup1.adrcty,
       lookup1.adrstc,
       ord.cponum,
       sh.carcod,
       sh.track_num,
       sh.doc_num, prtmst.untcst, i.untqty, ord_line.prtnum
order
    by sd.ordnum,
       lookup1.adrnam;
