/* #REPORTNAME=User File Status Report */
/* #HELPTEXT= This is a report of receipt files DCS5 */
/* #HELPTEXT = Requested by Imports */

ttitle left  print_time -
       center 'User File Status Report' -
       right print_date skip 1 -
       center 'Item: ' &1 skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a15
column trknum heading 'PO #'
column trknum format a10 trunc
column trksts heading 'Status'
column trksts format a10
column supnum heading 'Vendor'
column supnum format a10
column prtnum heading 'Item'
column prtnum format a15
column expqty heading 'Exp|Qty'
column expqty format 9999999999
column indqty heading 'Identified'
column indqty format 9999999999
column rcvqty heading 'Received'
column rcvqty format 9999999999
column arrdte heading 'Arrived'
column arrdte format a12
column invnum heading 'Invoice'
column invnum format a15
column dcs heading 'Environment'
column dcs format a11
column dcs5 heading 'Environment'
column dcs5 format a11
alter session set nls_date_format ='dd-mon-yyyy';

SELECT rcvinv.trknum,
       trlr.trlr_stat, 
       rcvlin.supnum,
       rcvlin.invnum,
       rcvlin.prtnum,
       trlr.arrdte, 
       rcvtrk.expdte,
       sum(rcvlin.expqty) ExpQty,
       sum(rcvlin.idnqty) IndQty,
       sum(rcvlin.rcvqty) rcvqty,
       'DCS 5' dcs5
  FROM rcvlin, rcvinv, rcvtrk, prtmst, supmst,trlr
 WHERE 
   rcvinv.trknum = rcvlin.trknum
   AND rcvinv.supnum = rcvlin.supnum
   AND rcvinv.invnum = rcvlin.invnum
   AND rcvlin.trknum = rcvtrk.trknum
   AND rcvtrk.trlr_id = trlr.trlr_id
   AND rcvlin.supnum = supmst.supnum
   AND rcvlin.prtnum = prtmst.prtnum
   AND prtmst.prt_client_id = '----'
 GROUP
    BY rcvinv.trknum,
       trlr.trlr_stat,
       rcvlin.invnum,
       rcvlin.prtnum,
       rcvlin.supnum,
       trlr.arrdte, rcvtrk.expdte
/
