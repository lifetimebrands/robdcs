/* #REPORTNAME=User What's on a Load*/
/* #REPORTTYPE=DCSCMD */
/* #HELPTEXT= Displays What is on a Load*/ 
/* #HELPTEXT= Requested by Order Management */
/* #VARNAM=lodnum , #REQFLG=Y */ 


column ordnum heading 'Order #'
column ordnum format a20
column ship_id heading 'Ship ID'
column ship_id format a10
column adrnam heading 'Customer'
column adrnam format a30

ttitle left  print_time -
center 'What''s on a Load' -
right print_date skip 2 -

select distinct s.ordnum, s.ship_id, substr(a.adrnam,1,30) adrnam 
from shipment_line s, pckwrk p, adrmst a, ord o, invlod l,
invsub su, invdtl i
where l.lodnum = '&1'
and l.lodnum = su.lodnum
and su.subnum = i.subnum
and i.wrkref = p.wrkref
and p.ordnum = s.ordnum
and p.ship_id = s.ship_id
and s.ordnum=o.ordnum 
and o.client_id = '----'
and o.btcust = a.host_ext_id
/
