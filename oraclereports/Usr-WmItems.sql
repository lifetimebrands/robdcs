/* #REPORTNAME=Walmart Report */
/* #HELPTEXT= This report lists all requested information */
/* #HELPTEXT= on Walmart items. */

ttitle left '&1 ' print_time -
center 'Walmart Items' -
right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column bld3 heading 'Building3'
column bld3 format 99,999,999
column bldg4 heading 'Building4'
column bldg4 format 99,999,999
column pending heading 'Pending'
column pending format 999,999,999
column untcas heading 'UnitCase'
column untcas format 999,999
column untpal heading 'UnitPallet'
column untpal format 999,999
column untpak heading 'UnitPack'
column untpak format 999,999
column invsts heading 'Status'
column invsts format a9
column lotnum heading 'Lot#'
column lotnum format a20
column january heading 'January'
column january format 999,999
column february heading 'February'
column february format 999,999
column march heading 'March'
column march format 999,999
column april heading 'April'
column april format 999,999
column may heading 'May'
column may format 999,999
column june heading 'June'
column june format 999,999
column july heading 'July'
column july format 999,999
column august heading 'August'
column august format 999,999
column september heading 'September'
column september format 999,999
column october heading 'October'
column october format 999,999
column november heading 'November'
column november format 999,999
column december heading 'December'
column december format 999,999
column vc_itmcls heading 'BOM'
column vc_itmcls format a6
column vc_prdcod heading 'PRDCLS'
column vc_prdcod format a6
column lngdsc heading 'Description'
column lngdsc format a30
column total heading 'Total'
column total format 999,999

/* Note: 4/19/05 we changed from using invsum b, to all inventory tables as table b */
/* to get the lotnum */

set linesize 300
set pagesize 600


select rtrim(a.prtnum) prtnum, b.lotnum, c.vc_itmcls, substr(e.lngdsc,1,30) lngdsc, 
c.vc_prdcod,  b.invsts, nvl(d.total,0) total, nvl( d.january,0) january, nvl( d.february,0) february,
nvl(d.march,0) march, nvl(d.april,0) april,
nvl(d.may,0) may,  nvl(d.june,0) june,  nvl(d.july,0) july,  nvl(d.august,0) august,
nvl(d.september,0) september,  nvl(d.october,0) october,
nvl(d.november,0) november,  nvl(d.december,0) december,
sum(nvl(b.untqty - b.comqty,0)) bldg4,
nvl(usrGetPndQty(a.prtnum),0) pending,
nvl(c.untcas,0) untcas,  nvl(c.untpal,0) untpal,  nvl(c.untpak,0) untpak
from usr_wmitems a, prtmst c, usr_totalforecast d, prtdsc e,
(select distinct invdtl.prtnum, invsum.stoloc,invsum.arecod,
                          invdtl.lotnum,
                          invdtl.invsts,
                          invsum.untqty,
                          invsum.comqty
                    from
                        invsum,
                        invlod,
                        invsub,
                        invdtl
                where invsum.prt_client_id = invdtl.prt_client_id
                  and invsum.prtnum = invdtl.prtnum
                  and invdtl.subnum = invsub.subnum
                  and invsub.lodnum = invlod.lodnum
                  and invlod.stoloc = invsum.stoloc
                  and invdtl.prt_client_id = '----'
                 and lotnum = 'NOLOT' ) b
where a.invsts = 'A' 
and a.prtnum = c.prtnum
and a.prtnum = b.prtnum(+)
and a.prtnum = d.prtnum(+)
and a.prtnum||'|----' = e.colval(+)
and e.colnam(+) = 'prtnum|prt_client_id'
and e.locale_id(+) = 'US_ENGLISH'
group by a.prtnum, b.lotnum, c.vc_itmcls, substr(e.lngdsc,1,30), c.vc_prdcod, c.untcas, c.untpal, c.untpak, b.invsts,
d.total, d.january, d.february, d.march, d.april, d.may, d.june,
d.july, d.august, d.september, d.october, d.november, d.december
order by a.prtnum
/
