
/* #REPORTNAME=User Empty Rack Locations Report */
/* #HELPTEXT= Requested by Order Management */


ttitle left print_time center 'User Empty Rack Locations Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a10
column stoloc heading 'Location'
column stoloc format a12

select arecod, stoloc
from locmst where locsts ='E'
and arecod in ('RACKSPCK','RACKCOMP','RACKHIGH','RACKNC','RACKSRES','RACKMED')
order by stoloc
/
