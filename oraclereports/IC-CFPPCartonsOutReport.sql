/* #REPORTNAME=IC-CFPP Cartons Out Report */
/* #HELPTEXT= This report provides details on the number of */
/* #HELPTEXT= cartons scanned out of CFPP.   */
/* #HELPTEXT= Date range must be input in the format DD-MON-YYYY */

/* #VARNAM=begin_dte , #REQFLG=Y */
/* #VARNAM=end_dte , #REQFLG=Y */
 
ttitle left print_time -
		center 'IC - CFPP Cartons Out Report' -
		right print_date skip - 
                center '&&1 '  to  ' &&2' skip 2 

btitle skip1 center 'Page:' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a30
column subnum heading 'CTN#'
column subnum format a20
column qty heading 'Units'
column qty format 999999 
column untcas heading 'Item Casepack'
column untcas format 99999999 
column numctn heading '# Ctns'
column numctn format 999999.99

set linesize 100
set pagesize 5000 

alter session set nls_date_format = 'dd-mon-yyyy HH:MI:SS AM';

select prtnum,sum(trnqty) qty,untcas,subnum,sum(trnqty)/decode(untcas,0,1,untcas) numctn
from
(select distinct dlytrn.prtnum,trnqty,prtmst.untcas,dlytrn.subnum,dlytrn.dtlnum,frstol from prtmst,locmst, dlytrn
    where 
    dlytrn.prtnum = prtmst.prtnum
    and dlytrn.prt_client_id = prtmst.prt_client_id
    and dlytrn.frstol = locmst.stoloc
    and locmst.arecod = 'CFPP'
    and actcod not in ('ADJ','CANPCK')
    and trndte  between '&&1'||' 12:00:00 AM' and '&&2'||' 11:59:59 PM')
group by prtnum,untcas,subnum
union
select prtnum,sum(trnqty) qty,untcas,subnum,sum(trnqty)/decode(untcas,0,1,untcas) ctn
from
(select distinct dlytrn.prtnum,trnqty,prtmst.untcas,dlytrn.subnum,dlytrn.dtlnum,frstol from prtmst@arch,locmst@arch, dlytrn@arch
    where 
    dlytrn.prtnum = prtmst.prtnum
    and dlytrn.prt_client_id = prtmst.prt_client_id
    and dlytrn.frstol = locmst.stoloc
    and locmst.arecod = 'CFPP'
    and actcod not in ('ADJ','CANPCK')
    and trndte between '&&1'||' 12:00:00 AM' and '&&2'||' 11:59:59 PM')
group by prtnum,untcas,subnum
/
