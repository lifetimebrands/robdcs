set linesize 200
set pagesize 500

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=CC Cycle Count By Date Report */
/* #HELPTEXT= This report lists all completed counts by Date and User */
/* #HELPTEXT = Please enter date format at DD-MON-YYYY */ 
/* #HELPTEXT= Requested by Inventory Control  - March 2003 */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */
/* #GROUPS=NOONE */


ttitle  left print_date     print_time center 'CC Cycle Count By Date Report' skip  -
btitle  center 'Page: ' format 999 sql.pno

column adddte heading 'Date '
column adddte format a12
column cnt_usr_id heading 'User'
column cnt_usr_id  format a11
column arecod  heading 'Area'
column arecod format a10
column count heading 'Completed Count'
column count format 9999999


/* first script will return results if the counts have not been closed out. */
/* once they are closed out, the counts should appear in the second script. */
/* records move from cntwrk to cnthst once they are closed. */


select trunc(u.cntdte) adddte, u.cnt_usr_id,b.arecod, count(distinct u.stoloc) count
from locmst b, usr_cnthst u
where u.stoloc = b.stoloc(+)
and u.cnttyp = 'B'
and u.cntmod = 'C' and exists (select 1 from usr_cnthst c where c.cntbat = 
u.cntbat and (c.cnttyp='F' or c.cnttyp='E' and c.untqty >=0)
and c.stoloc=u.stoloc)
and trunc(u.cntdte) between '&&1' and '&&2'
group by trunc(u.cntdte),u.cnt_usr_id,b.arecod
/
