/* #REPORTNAME=IC - Skewer Info */

column supnum heading 'Supplier'
column supnum format a10
column adrnam heading 'Supplier Name'
column adrnam format a35
column prtnum heading 'Part'
column prtnum format a15
column lngdsc heading 'Decsription'
column lngdsc format a30
column invsts heading 'Status'
column invsts format a20
column untqty heading 'Qty.'
column untqty format 999,999,999

set linesize 300
set pagesize 400

select a.supnum, i.adrnam, b.prtnum, substr(g.lngdsc,1,30) lngdsc, substr(j.lngdsc,1,20) invsts, sum(b.untqty) untqty
from rcvlin a, invdtl b, invsub c, invlod d,  prtdsc g, locmst h, adrmst i, dscmst j
where a.rcvkey=b.rcvkey
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and a.prtnum=b.prtnum
and b.prtnum||'|----'=g.colval
and g.colnam='prtnum|prt_client_id'
and g.locale_id='US_ENGLISH'
and (g.lngdsc like '%SKEW%' or g.lngdsc like '%BAMBOO%')
and h.arecod not in ('ADJS','CADJ','SHIP')
and h.stoloc=d.stoloc
and j.colnam='invsts'
and b.invsts=j.colval
and j.locale_id='US_ENGLISH'
and a.supnum=i.host_ext_id(+)
group by a.supnum, i.adrnam, b.prtnum, g.lngdsc, j.lngdsc
union
select '----' supnum, null adrnam, b.prtnum, substr(g.lngdsc,1,30) lngdsc, substr(j.lngdsc,1,20) invsts, sum(b.untqty) untqty
from invdtl b, invsub c, invlod d,  prtdsc g, locmst h, dscmst j
where b.rcvkey is null
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and b.prtnum||'|----'=g.colval
and g.colnam='prtnum|prt_client_id'
and g.locale_id='US_ENGLISH'
and (g.lngdsc like '%SKEW%' or g.lngdsc like '%BAMBOO%')
and h.arecod not in ('ADJS','CADJ','SHIP')
and h.stoloc=d.stoloc
and j.colnam='invsts'
and b.invsts=j.colval
and j.locale_id='US_ENGLISH'
group by b.prtnum, g.lngdsc, j.lngdsc
union
select null supnum, null adrnam, a.prtnum, null lngdsc, null invsts, sum(a.untqty)
from invdtl a, invsub b, invlod c, locmst d
where a.subnum=b.subnum
and b.lodnum=c.lodnum
and c.stoloc=d.stoloc
and d.arecod not in ('ADJS','CADJ','SHIP')
and a.prtnum in (select a.prtnum
from prtmst a, prtdsc b
where a.prtnum||'|----'=b.colval
and b.colnam='prtnum|prt_client_id'
and b.locale_id='US_ENGLISH'
and (b.lngdsc like '%SKEW%' or b.lngdsc like '%BAMBOO%'))
group by a.prtnum
order by prtnum;
