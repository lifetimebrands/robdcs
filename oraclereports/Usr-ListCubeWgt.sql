/* #REPORTNAME=User List Cube & Weight Report */
/* #HELPTEXT= This report lists the cube and weight of  */
/* #HELPTEXT= items by order number or specified part# and qty. */
/* #HELPTEXT= Report requires either an order number */ 
/* #HELPTEXT= or part and quantity. The report will list */ 
/* #HELPTEXT= the order, item, qty, cube and weight. A summary of */
/* #HELPTEXT= the cubes and weights is provided at the end.  */
/* #HELPTEXT= Requested by Senior Management Westbury. */
/* #VARNAM=ordnum , #REQFLG=Y    */
/* #VARNAM=prtnum ,  #REQFLG=Y */
/* #VARNAM=qty ,  #REQFLG=Y */   

-- Author: Al Driver
-- Report provides the cube, weight, qty, order# and item# for
-- an order or specific item. Using the shipment, car_move, trlr and stop tables
-- for orders was not necessary since we are not concerned with 
-- shipment dates. Plus in many cases an order has items that do not ship,
-- which can result in the item not having a link from the ord_line table 
-- to the shipment_line table. The cube and weight are based upon what was ordered,
-- so the order must be in the system. Input by item requires a quantity,
-- otherwise the cube and weight will be zero.
-- Report uses a union to do the calculation for orders or individual parts
-- separately. We used the sum of the ordqty in the first script because
-- sometimes the same item appears on different order lines in the same order
-- and we would lose the quantites if they were the same. 


ttitle left  print_time -
                center 'User List Cube and Weight Report' -
                right print_date skip 5 

btitle skip1 center 'Page:' format 999 sql.pno

column ordnum heading 'Order #'
column ordnum format a20
column prtnum heading 'Item #'
column prtnum format a30
column ordqty heading 'Qty'
column ordqty format 9999999
column cube heading 'Cube'
column cube format 999999.9999 
column weight heading 'Weight'
column weight format 999999.9999 

break on ordnum 
compute sum label "Grand Total" of cube on ordnum
compute sum label "Grand Total" of weight on ordnum

set pagesize 500
set linesize 100

select ord.ordnum ordnum,ord_line.prtnum prtnum ,substr(to_char(sum(ord_line.ordqty)),1,7) ordqty,
       ((ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt)/1728)*(sum(ord_line.ordqty)/prtmst.untcas) cube,
        vc_grswgt_each * sum(ord_line.ordqty) weight
                                          from ftpmst, prtmst,  ord, ord_line
                                          where ftpmst.ftpcod = prtmst.prtnum
                                          AND prtmst.prtnum = ord_line.prtnum
                                          AND ord.ordnum                  = ord_line.ordnum
                                          AND ord.client_id               = ord_line.client_id
                                          AND ord.ordnum = '&&1'
group by ord.ordnum,ord_line.prtnum,caslen,caswid,cashgt,prtmst.untcas,vc_grswgt_each
union
select NULL ordnum, '&&2' prtnum,nvl('&&3',0) ordqty,
     ((ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt)/1728)*(nvl('&&3',0)/prtmst.untcas) cube,
        vc_grswgt_each * nvl('&&3',0) weight
from prtmst,ftpmst
where ftpmst.ftpcod = prtmst.prtnum
and prtmst.prtnum = '&&2'
/
