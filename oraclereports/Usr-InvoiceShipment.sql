/* #REPORTNAME=User Invoice Shipment by SID Report */
/* #HELPTEXT= This report provides invoiced information  */
/* #HELPTEXT= by Shipment.*/
/* #HELPTEXT= Requested by Shipping */

/* #VARNAM=ship_id , #REQFLG=Y */

ttitle left  print_time -
       center 'User Invoice Shipment By SID Range: ' -
       right print_date skip -
btitle skip 1 center 'Page: ' format 999 sql.pno 

column ordnum heading 'Order#'
column ordnum format A20
column prtnum heading 'Item' just center
column prtnum format A12
column shpqty heading 'Qty Shipped' just center
column shpqty format 99,999,999
column untcst heading 'Unit Cost'
column untcst format 9,999.99
column totuntcst heading 'Total Cost'
column totuntcst format 999,999.99

set pagesize 100
break on ordnum 
compute sum label "Total Cost" of totuntcst on ordnum 

select ol.ordnum, ol.prtnum, sd.inpqty, pm.untcst, sum(sd.inpqty * pm.untcst) totuntcst
 from ord_line ol, shipment_line sd, prtmst pm, shipment sh
        where ol.ordnum = sd.ordnum
        and ol.ordlin = sd.ordlin
        and ol.ordsln = sd.ordsln
        and ol.client_id = sd.client_id
        and sd.ship_id = sh.ship_id
        and sh.shpsts ='I'
        and sd.ship_id ='&1'
        and ol.prtnum = pm.prtnum
        and pm.prt_client_id = '----'
group
    by ol.ordnum, ol.prtnum, sd.inpqty, pm.untcst
union
select ol.ordnum, ol.prtnum, sd.stgqty, pm.untcst, sum(sd.stgqty * pm.untcst) totuntcst   
 from ord_line ol, shipment_line sd, prtmst pm, shipment sh
        where ol.ordnum = sd.ordnum
        and ol.ordlin = sd.ordlin
        and ol.ordsln = sd.ordsln
        and ol.client_id = sd.client_id
        and sd.ship_id = sh.ship_id
        and sh.shpsts ='S'
        and sd.ship_id ='&1'
        and ol.prtnum = pm.prtnum
        and pm.prt_client_id = '----'
group
    by ol.ordnum, ol.prtnum, sd.stgqty, pm.untcst
order by 1,2,3
/
