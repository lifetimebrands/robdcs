/* #REPORTNAME=IC-OM Bogus Locations */
/* #HELPTEXT = Requested by Inventory Control */
/* #HELPTEXT = Returns locations starting in OM */
/* #HELPTEXT = that currently have a unit quantity */

set pagesize 500
set linesize 200

ttitle left print_time -
center 'OM Bogus Locations'-
right print_date skip 2 -

column stoloc heading 'Location'
column stoloc format A15
column prtnum heading 'Item #'
column prtnum format A15
column qty heading 'Quanitity'
column qty format 999999
column cost heading 'Cost'
column cost format 999999.99

select a.stoloc stoloc, a.prtnum part, a.untqty qty, (b.untcst * a.untqty) cost
from invloc a, prtmst b
where a.prtnum=b.prtnum
and a.untqty!=0
and a.stoloc like 'OM%'
/
