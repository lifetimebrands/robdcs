/* #REPORTNAME=FIN-Daily Flash Report */
/* #VARNAM=date , #REQFLG=Y */
/* #HELPTEXT= Requested by the Controller */
/* #HELPTEXT= Date must be entered in format DD-MON-YYYY */

/* Author: Al Driver */
/* This report was created to provide summary information */
/* on requested categories.  Report provides day, MTD, QTD and YTD */
/* info for input date, and the same day from a year ago. */
/* The report runs a few scripts with a union to obtain the requested info, */
/* and puts the results in a cursor.  The stats are then taken */
/* from the cursor and put into a table to hold the information. */
/* The info is then printed out in a specific order. */
/* A package (DailyFlash.pkg) was created to obtain the stats */
/* for the year before. For categories where we can not get */
/* last years stats, zeros are substituted. */

alter session set nls_date_format ='DD-MON-YYYY';

set linesize 500
set serveroutput on

declare

c_blankline varchar2(10) := chr(13);
c_col_width number := 13;

stat varchar2(50);
td number;
mt number;
qt number;
yt number;
tdx number;
mtx number;
qtx number;
ytx number;

/* obtain input date and last years date */

l_refdte date:= '&&1';
l_lastdte date:= ADD_MONTHS(l_refdte,-12);

type ordsum is record (status varchar2(50),
                       today number,
		       mtd number,
		       qtd number,
                       ytd number,
                       todayx number,
                       mtdx  number,
                       qtdx number,
                       ytdx number);

type summtab is table of ordsum index by binary_integer;
l_summ summtab;
l_counter number:=0;  /* counter used to input data into specific cells */ 
l_shpcat_col number;

/* Use a union to obtain all the information at once and place in cursor */
/* Get all stored information from KPI Report */

cursor c1 is
select status, UsrDailyFlash.usrGetToday('&&1',status) today, UsrDailyFlash.usrGetMTD('&&1',status) MTD,
UsrDailyFlash.usrGetQTD('&&1',status) QTD,UsrDailyFlash.usrGetYTD('&&1',status) YTD,
UsrDailyFlash.usrGetToday(ADD_MONTHS('&&1',-12),status) today, 
UsrDailyFlash.usrGetMTD(ADD_MONTHS('&&1',-12),status) MTD,
UsrDailyFlash.usrGetQTD(ADD_MONTHS('&&1',-12),status) QTD,
UsrDailyFlash.usrGetYTD(ADD_MONTHS('&&1',-12),status) YTD
from usr_ledgerhdr where status in ('CUR','CUE','UNS','OS','S','VS','URC','CR','RWP','PUP','XBM','UOP',
'PCK','FUL','PAS','DOR','TIC','USD','USO','USW','SD','SO','SW','FD','FO','FW','PD','PO','PW','OD','OO','OW','CD','CO','CW',
'UTD','UDD','UWD','SDD','SOD','SWD','FDD','FOD','FWD','PDD','POD','PWD','ODD','OOD','OWD')
union  /* Obtain Inventory $ Values (4-Wall, Production, Ship-Stage) for the day */
select status, UsrDailyFlash.usrGetToday('&&1',status) today, 0 MTD,
0 QTD,0 YTD,UsrDailyFlash.usrGetToday(ADD_MONTHS('&&1',-12),status) today, 
0 MTD,0 QTD,0 YTD
from usr_ledgerhdr where status in ('IVV','SVV','PVV','FWP','FWA','FWI','FPO','FWO','IS')
union           --select 'SHIP-STAGE', sum(unsamt),0,0,0,0,0,0,0 from usr_openords where status in ('Staged','Printed','Transfer')
select 'Orders' status,count(ordnum),0,0,0,0,0,0,0 /* Obtain Open Orders Information */
from usr_openords
    where trunc(to_date(early_shpdte,'MM/DD/YYYY')) < trunc(sysdate) 
union
select 'Dollars' status, sum(unsamt),0,0,0,0,0,0,0
    from usr_openords
    where trunc(to_date(early_shpdte,'MM/DD/YYYY')) < trunc(sysdate) 
union
select decode(b.status,'Backorder','Pending',b.status),count(a.ordnum),0,0,0,0,0,0,0
              from usr_openords a,usr_openordstatus b
              where a.status(+) = b.status
              and b.status in ('Backorder','Pending','Staged','Released','Ready')
      group by decode(b.status,'Backorder','Pending',b.status)
union  /* Obtain Available Shipping Lanes */
select 'Available' status,count(stoloc),0,0,0,0,0,0,0
from locmst
where rescod is null
and substr(stoloc,1,4) = 'SHIP'
union    /* Obtain Picking Summary information */
select 'Full Case' status,count(b.subnum),0,0,0,0,0,0,0
                               from pckwrk a,invdtl b
                               where a.wrkref = b.wrkref
                                and a.prtnum = b.prtnum
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.wrktyp = 'P'
                                and a.appqty  > 0
                                and a.subucc is not null       /* means its a full case */
                         and trunc(b.lstdte) =  to_date('&&1','DD-MON-YYYY')
union
select 'Piece Pick' status,count(distinct b.subnum),0,0,0,0,0,0,0
                               from pckwrk a,invdtl b
                               where a.wrkref = b.wrkref
                                and a.prtnum = b.prtnum
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.wrktyp = 'P'
                                and a.appqty  > 0
                                and a.subucc is null
                   and trunc(b.lstdte) = to_date('&&1','DD-MON-YYYY')
union         /* Obtain all receiving Summary info */
select 'PALLET',UsrDailyFlash.usrPalRcd('&&1'),UsrDailyFlash.usrPalRcdMTD('&&1'),
    UsrDailyFlash.usrPalRcdQTD('&&1'),UsrDailyFlash.usrPalRcdYTD('&&1'),
    UsrDailyFlash.usrPalRcd(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrPalRcdMTD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrPalRcdQTD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrPalRcdYTD(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Recd',UsrDailyFlash.usrContRcd('&&1'),UsrDailyFlash.usrContRcdMTD('&&1'),
    UsrDailyFlash.usrContRcdQTD('&&1'),UsrDailyFlash.usrContRcdYTD('&&1'),
    UsrDailyFlash.usrContRcd(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdMTD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdQTD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdYTD(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Closed',UsrDailyFlash.usrContClsd('&&1'),UsrDailyFlash.usrContClsdMTD('&&1'),            
    UsrDailyFlash.usrContClsdQTD('&&1'),UsrDailyFlash.usrContClsdYTD('&&1'),            
    UsrDailyFlash.usrContClsd(ADD_MONTHS('&&1',-12)),      
    UsrDailyFlash.usrContClsdMTD(ADD_MONTHS('&&1',-12)),      
    UsrDailyFlash.usrContClsdQTD(ADD_MONTHS('&&1',-12)),      
    UsrDailyFlash.usrContClsdYTD(ADD_MONTHS('&&1',-12))      
   from dual
union
select 'Container Not Closed',UsrDailyFlash.usrContNtClsd('&&1'),UsrDailyFlash.usrContNtClsdMTD('&&1'),         
    UsrDailyFlash.usrContNtClsdQTD('&&1'),UsrDailyFlash.usrContNtClsdYTD('&&1'),          
    UsrDailyFlash.usrContNtClsd(ADD_MONTHS('&&1',-12)),     
    UsrDailyFlash.usrContNtClsdMTD(ADD_MONTHS('&&1',-12)),     
    UsrDailyFlash.usrContNtClsdQTD(ADD_MONTHS('&&1',-12)),     
    UsrDailyFlash.usrContNtClsdYTD(ADD_MONTHS('&&1',-12))     
   from dual
union
select 'Container Recd Int',UsrDailyFlash.usrContRcdI('&&1'),UsrDailyFlash.usrContRcdMTDI('&&1'),
    UsrDailyFlash.usrContRcdQTDI('&&1'),UsrDailyFlash.usrContRcdYTDI('&&1'),
    UsrDailyFlash.usrContRcdI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdMTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdQTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdYTDI(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Closed Int',UsrDailyFlash.usrContClsdI('&&1'),UsrDailyFlash.usrContClsdMTDI('&&1'),
    UsrDailyFlash.usrContClsdQTDI('&&1'),UsrDailyFlash.usrContClsdYTDI('&&1'),
    UsrDailyFlash.usrContClsdI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdMTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdQTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdYTDI(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Not Closed Int',UsrDailyFlash.usrContNtClsdI('&&1'),UsrDailyFlash.usrContNtClsdMTDI('&&1'),
    UsrDailyFlash.usrContNtClsdQTDI('&&1'),UsrDailyFlash.usrContNtClsdYTDI('&&1'),
    UsrDailyFlash.usrContNtClsdI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdMTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdQTDI(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdYTDI(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Recd Dom',UsrDailyFlash.usrContRcdD('&&1'),UsrDailyFlash.usrContRcdMTDD('&&1'),
    UsrDailyFlash.usrContRcdQTDD('&&1'),UsrDailyFlash.usrContRcdYTDD('&&1'),
    UsrDailyFlash.usrContRcdD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdMTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdQTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdYTDD(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Closed Dom',UsrDailyFlash.usrContClsdD('&&1'),UsrDailyFlash.usrContClsdMTDD('&&1'),
    UsrDailyFlash.usrContClsdQTDD('&&1'),UsrDailyFlash.usrContClsdYTDD('&&1'),
    UsrDailyFlash.usrContClsdD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdMTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdQTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdYTDD(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Not Closed Dom',UsrDailyFlash.usrContNtClsdD('&&1'),UsrDailyFlash.usrContNtClsdMTDD('&&1'),
    UsrDailyFlash.usrContNtClsdQTDD('&&1'),UsrDailyFlash.usrContNtClsdYTDD('&&1'),
    UsrDailyFlash.usrContNtClsdD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdMTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdQTDD(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdYTDD(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Recd Tran',UsrDailyFlash.usrContRcdT('&&1'),UsrDailyFlash.usrContRcdMTDT('&&1'),
    UsrDailyFlash.usrContRcdQTDT('&&1'),UsrDailyFlash.usrContRcdYTDT('&&1'),
    UsrDailyFlash.usrContRcdT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdMTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdQTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContRcdYTDT(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Closed Tran',UsrDailyFlash.usrContClsdT('&&1'),UsrDailyFlash.usrContClsdMTDT('&&1'),
    UsrDailyFlash.usrContClsdQTDT('&&1'),UsrDailyFlash.usrContClsdYTDT('&&1'),
    UsrDailyFlash.usrContClsdT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdMTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdQTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContClsdYTDT(ADD_MONTHS('&&1',-12))
   from dual
union
select 'Container Not Closed Tran',UsrDailyFlash.usrContNtClsdT('&&1'),UsrDailyFlash.usrContNtClsdMTDT('&&1'),
    UsrDailyFlash.usrContNtClsdQTDT('&&1'),UsrDailyFlash.usrContNtClsdYTDT('&&1'),
    UsrDailyFlash.usrContNtClsdT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdMTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdQTDT(ADD_MONTHS('&&1',-12)),
    UsrDailyFlash.usrContNtClsdYTDT(ADD_MONTHS('&&1',-12))
   from dual
union  /* Obtain all Warehouse Storage Summary Info */
select 'Storage Use' status, count(stoloc),0,0,0,0,0,0,0 from locmst                     
where
(stoflg =1 or arecod = 'ISTG') and useflg =1
and rescod is null
union  
select 'Empty Rack Use',count(stoloc),0,0,0,0,0,0,0 from locmst                     
where substr(arecod,1,4) = 'RACK'
and stoflg =1 and useflg =1
and pndqvl = 0
and locsts = 'E' 
and not exists(select 'x' from invlod i    /* having not exists in invlod serves same purpose as */
where i.stoloc = locmst.stoloc)            /* adding and curqvl = 0 */
union
select 'Empty ISTG Use',count(stoloc),0,0,0,0,0,0,0 from locmst                     
where substr(arecod,1,4) = 'ISTG'
and useflg =1
and pndqvl = 0
and locsts = 'E' 
and not exists(select 'x' from invlod i
where i.stoloc = locmst.stoloc)
union
select 'Empty Tower Use',count(stoloc),0,0,0,0,0,0,0 from locmst                     
where substr(arecod,1,5) = 'TOWER'
and stoflg =1 and useflg =1
and pndqvl = 0
and locsts = 'E' 
and not exists(select 'x' from invlod i
where i.stoloc = locmst.stoloc)
union
select 'Empty GW Use',count(stoloc),0,0,0,0,0,0,0 from locmst                     
where substr(arecod,1,5) = 'GWALL'
and stoflg =1 and useflg =1
and pndqvl = 0
and locsts = 'E' 
and not exists(select 'x' from invlod i
where i.stoloc = locmst.stoloc)
union
select 'Empty Other Use',count(stoloc),0,0,0,0,0,0,0 from locmst                     
where substr(arecod,1,4) not in ('ISTG','GWAL','TOWE','RACK')
and stoflg =1 and useflg =1
and pndqvl = 0
and locsts = 'E' 
and not exists(select 'x' from invlod i
where i.stoloc = locmst.stoloc)
union
select 'Percentage',round((sum(b.stoloc)-sum(a.stoloc))/sum(b.stoloc) * 100),0,0,0,0,0,0,0
             from(SELECT locmst.arecod,count(locmst.stoloc) stoloc
                  from locmst where
                  (stoflg =1 or substr(arecod,1,4) = 'ISTG') and useflg =1
                  and pndqvl = 0
                  and locsts = 'E'
                  and not exists(select 'x' from invlod i
                  where i.stoloc = locmst.stoloc) group by arecod) a,
               (select locmst.arecod,count(stoloc) stoloc
                      from locmst where (stoflg =1 or substr(arecod,1,4) ='ISTG')  and useflg =1
                      and rescod is null
                      group by arecod) b
               where a.arecod = b.arecod;

/* Create a procedure to print data in specific format and size */

procedure print_line(i_title varchar2,
		     i_col1  number,
		     i_col2  number,
		     i_col3  number,
		     i_col4  number,
		     i_col5  number,
		     i_col6  number,
		     i_col7  number,
                     i_col8  number) is

c_num_fmt number;

begin
  dbms_output.put_line(rpad(i_title,50,' ') ||
		      lpad(to_char(i_col1, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col2, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col3, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col4, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col5, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col6, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col7, c_num_fmt),c_col_width,' ')||' '||
                      lpad(to_char(i_col8, c_num_fmt),c_col_width,' '));
end;

begin

/* initialize table */

  dbms_output.enable(1000000);

  for i in 0..87 loop
    l_summ(i).today := 0;
    l_summ(i).mtd := 0;
    l_summ(i).qtd := 0;
    l_summ(i).ytd := 0;
    l_summ(i).todayx := 0;        
    l_summ(i).mtdx := 0;            
    l_summ(i).qtdx := 0;           
    l_summ(i).ytdx := 0;     
  end loop;


/* open cursor */
/* Place all cursor information into l_summ table */
/* Place information in specified cells to enable control of output */
 

open c1;

for c in 0..87 loop
  
  fetch c1 into stat, td,mt,qt,yt,tdx,mtx,qtx,ytx;

    if stat = 'UNS' then 
     l_counter:=0;     
     l_summ(l_counter).status := 'Total # of Units Shipped';
   elsif stat = 'S' then
     l_counter:=1;
     l_summ(l_counter).status := 'Total # of Cartons Shipped';
  elsif stat = 'OS' then
      l_counter:=2;
      l_summ(l_counter).status := 'Total # of Orders Shipped';
  elsif stat = 'VS' then
      l_counter:=3;
      l_summ(l_counter).status := 'Total Dollars Shipped';
  elsif stat = 'Orders' then
      l_counter:=4;
      l_summ(l_counter).status := '# of past due Open Orders';
   elsif stat = 'Dollars' then
      l_counter:=5;
      l_summ(l_counter).status := 'Total past due Open Dollars';
  elsif stat = 'Pending' then
      l_counter:=6;
      l_summ(l_counter).status := 'Pending Status Orders';
   elsif stat = 'Staged' then
      l_counter:=7;
      l_summ(l_counter).status := 'Staged Status Orders';
   elsif stat = 'Ready' then
      l_counter:=8;
      l_summ(l_counter).status := 'Ready Status Orders';
   elsif stat = 'Released' then
      l_counter:=9;
      l_summ(l_counter).status := 'Released Status Orders';
  elsif stat = 'Available' then
      l_counter:=10;
      l_summ(l_counter).status := 'Available Shipping Lanes';
  elsif stat = 'Full Case' then
      l_counter:=11;
      l_summ(l_counter).status := 'Total Full Case Cartons Picked';
   elsif stat = 'Piece Pick' then
      l_counter:=12;
      l_summ(l_counter).status := 'Total Piece Pick Cartons Picked';
  elsif stat = 'TIC' then
      l_counter:=13;
      l_summ(l_counter).status := 'Total Pieces Ticketed';
  elsif stat = 'URC' then
      l_counter:=14;
      l_summ(l_counter).status := 'Total # of Units Received';
 elsif stat = 'CR' then
      l_counter:=15;
      l_summ(l_counter).status := 'Total # of Cartons Received';
 elsif stat = 'PALLET' then
      l_counter:=16;
      l_summ(l_counter).status := 'Total # of Pallets Received';          
   elsif stat = 'Container Recd' then
      l_counter:=17;
      l_summ(l_counter).status := 'Total # of Containers Recd';
   elsif stat = 'Container Closed' then
      l_counter:=18;
      l_summ(l_counter).status := 'Total # of Containers Closed';
   elsif stat = 'Container Not Closed' then
      l_counter:=19;
      l_summ(l_counter).status := 'Total Containers Recd Not Closed';
  elsif stat = 'RWP' then
      l_counter:=20;
      l_summ(l_counter).status := 'Total Rework Units Produced';
  elsif stat = 'PUP' then
      l_counter:=21;
      l_summ(l_counter).status := 'Total BOM Units Produced';
  elsif stat = 'XBM' then
      l_counter:=22;
      l_summ(l_counter).status := 'Total XBOM Units Produced';  
   elsif stat = 'UOP' then
      l_counter:=23;
      l_summ(l_counter).status := 'Total Production Units';
   elsif stat = 'Storage Use' then
      l_counter:=24;
      l_summ(l_counter).status := 'Available Storage (Location useable)';
   elsif stat = 'Empty Rack Use' then  
      l_counter:=25;
      l_summ(l_counter).status := 'Empty Racks No Pending (Location useable)';  
   elsif stat = 'Empty ISTG Use' then      
      l_counter:=26;
      l_summ(l_counter).status := 'Empty P &'||' Ds No Pending (Location useable)';
   elsif stat = 'Empty Tower Use' then      
      l_counter:=27;
      l_summ(l_counter).status := 'Empty Towers No Pending (Location useable)';
   elsif stat = 'Empty GW Use' then      
      l_counter:=28;
      l_summ(l_counter).status := 'Empty GW No Pending (Location useable)';
   elsif stat = 'Empty Other Use' then      
      l_counter:=29;
      l_summ(l_counter).status := 'Empty Other No Pending (Location useable)';
   elsif stat = 'Percentage' then      
      l_counter:=30;
      l_summ(l_counter).status := 'Percentage of Full Locations';
   elsif stat = 'CUE' then
      l_counter:=31;
      l_summ(l_counter).status := 'Total Cubic Volume Shipped';
   elsif stat = 'CUR' then
      l_counter:=32;
      l_summ(l_counter).status := 'Total Cubic Volume Received';
   elsif stat = 'FUL' then
      l_counter:=33;
      l_summ(l_counter).status := 'Total # of Full Case Cartons Shipped';
  elsif stat = 'PCK' then
      l_counter:=34;
      l_summ(l_counter).status := 'Total # of Piece Pick Cartons Shipped';
  elsif stat = 'IVV' then
      l_counter:=35;
      l_summ(l_counter).status := 'Inventory $ Value (4-Wall)';
  elsif stat = 'PVV' then
      l_counter:=36;
      l_summ(l_counter).status := 'Inventory $ Value (PROD)';
  elsif stat = 'SVV' then
      l_counter:=37;
      l_summ(l_counter).status := 'Inventory $ Value (SHIP-STAGE)';
  elsif stat = 'DOR' then
      l_counter:=38;
      l_summ(l_counter).status := '$ Value of Receipts';
  elsif stat = 'PAS' then
      l_counter:=39;
      l_summ(l_counter).status := 'Number of Pallets Shipped';
  elsif stat = 'USD' then
      l_counter:=40;
      l_summ(l_counter).status := 'Units Shipped DTC';
elsif stat = 'USO' then	
  l_counter:=41;	
  l_summ(l_counter).status := 'Units Shipped Drop Ship';
elsif stat = 'USW' then	
  l_counter:=42;	
  l_summ(l_counter).status := 'Unit Shipped Wholesale';
elsif stat = 'SD' then	
  l_counter:=43;	
  l_summ(l_counter).status := 'Cartons Shipped DTC';
elsif stat = 'SO' then	
  l_counter:=44;	
  l_summ(l_counter).status := 'Cartons Shipped Drop Ship';
elsif stat = 'SW' then	
  l_counter:=45;	
  l_summ(l_counter).status := 'Cartons Shipped Wholesale';
elsif stat = 'FD' then	
  l_counter:=46;	
  l_summ(l_counter).status := 'Full Case Cartons Shipped DTC';
elsif stat = 'FO' then	
  l_counter:=47;	
  l_summ(l_counter).status := 'Full Case Cartons Shipped Drop Ship';
elsif stat = 'FW' then	
  l_counter:=48;	
  l_summ(l_counter).status := 'Full Case Cartons Shipped Wholesale';
elsif stat = 'PD' then	
  l_counter:=49;	
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped DTC';
elsif stat = 'PO' then	
  l_counter:=50;	
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped Drop Ship';
elsif stat = 'PW' then	
  l_counter:=51;	
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped Wholesale';
elsif stat = 'OD' then	
  l_counter:=52;	
  l_summ(l_counter).status := 'Orders Shipped DTC';
elsif stat = 'OO' then	
  l_counter:=53;	
  l_summ(l_counter).status := 'Orders Shipped Drop Ship';
elsif stat = 'OW' then	
  l_counter:=54;	
  l_summ(l_counter).status := 'Orders Shipped Wholesale';
elsif stat = 'CD' then	
  l_counter:=55;	
  l_summ(l_counter).status := 'Cubic Volume Shipped DTC';
elsif stat = 'CO' then	
  l_counter:=56;	
  l_summ(l_counter).status := 'Cubic Volume Shipped Drop Ship';
elsif stat = 'CW' then	
  l_counter:=57;	
  l_summ(l_counter).status := 'Cubic Volume Shipped Wholesale';
elsif stat = 'UTD' then
      l_counter:=58;
      l_summ(l_counter).status := 'Units Shipped DTC $ $';
elsif stat = 'UDD' then
  l_counter:=59;
  l_summ(l_counter).status := 'Units Shipped Drop Ship $';
elsif stat = 'UWD' then
  l_counter:=60;
  l_summ(l_counter).status := 'Unit Shipped Wholesale $';
elsif stat = 'SDD' then
  l_counter:=61;
  l_summ(l_counter).status := 'Cartons Shipped DTC $';
elsif stat = 'SOD' then
  l_counter:=62;
  l_summ(l_counter).status := 'Cartons Shipped Drop Ship $';
elsif stat = 'SWD' then
  l_counter:=63;
  l_summ(l_counter).status := 'Cartons Shipped Wholesale $';
elsif stat = 'FDD' then
  l_counter:=64;
  l_summ(l_counter).status := 'Full Case Cartons Shipped DTC $';
elsif stat = 'FOD' then
  l_counter:=65;
  l_summ(l_counter).status := 'Full Case Cartons Shipped Drop Ship $';
elsif stat = 'FWD' then
  l_counter:=66;
  l_summ(l_counter).status := 'Full Case Cartons Shipped Wholesale $';
elsif stat = 'PDD' then
  l_counter:=67;
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped DTC $';
elsif stat = 'POD' then
  l_counter:=68;
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped Drop Ship $';
elsif stat = 'PWD' then
  l_counter:=69;
  l_summ(l_counter).status := 'Piece Pick Cartons Shipped Wholesale $';
elsif stat = 'ODD' then
  l_counter:=70;
  l_summ(l_counter).status := 'Orders Shipped DTC $';
elsif stat = 'OOD' then
  l_counter:=71;
  l_summ(l_counter).status := 'Orders Shipped Drop Ship $';
elsif stat = 'OWD' then
  l_counter:=72;
  l_summ(l_counter).status := 'Orders Shipped Wholesale $';
elsif stat = 'IS' then
  l_counter:=73;
  l_summ(l_counter).status := 'Number of Items Shipped';
elsif stat = 'FWP' then
  l_counter:=74;
  l_summ(l_counter).status := 'Four Wall Items';
elsif stat = 'FWA' then
  l_counter:=75;
  l_summ(l_counter).status := 'Four Wall Active';
elsif stat = 'FWI' then
  l_counter:=76;
  l_summ(l_counter).status := 'Four Wall Inactive';
elsif stat = 'FPO' then
  l_counter:=77;
  l_summ(l_counter).status := 'Four Wall Phase Out';
elsif stat = 'FWO' then
  l_counter:=78;
  l_summ(l_counter).status := 'Four Wall Other';
   elsif stat = 'Container Recd Int' then
      l_counter:=79;
      l_summ(l_counter).status := 'Total # of Containers Recd Int';
   elsif stat = 'Container Closed Int' then
      l_counter:=80;
      l_summ(l_counter).status := 'Total # of Containers Closed Int';
   elsif stat = 'Container Not Closed Int' then
      l_counter:=81;
      l_summ(l_counter).status := 'Total Containers Recd Not Closed Int';
   elsif stat = 'Container Recd Dom' then
      l_counter:=82;
      l_summ(l_counter).status := 'Total # of Containers Recd Dom';
   elsif stat = 'Container Closed Dom' then
      l_counter:=83;
      l_summ(l_counter).status := 'Total # of Containers Closed Dom';
   elsif stat = 'Container Not Closed Dom' then
      l_counter:=84;
      l_summ(l_counter).status := 'Total Containers Recd Not Closed Dom';
   elsif stat = 'Container Recd Tran' then
      l_counter:=85;
      l_summ(l_counter).status := 'Total # of Containers Recd Tran';
   elsif stat = 'Container Closed Tran' then
      l_counter:=86;
      l_summ(l_counter).status := 'Total # of Containers Closed Tran';
   elsif stat = 'Container Not Closed Tran' then
      l_counter:=87;
      l_summ(l_counter).status := 'Total Containers Recd Not Closed Tran';
end if;

      l_summ(l_counter).today := td;
      l_summ(l_counter).mtd := mt;
      l_summ(l_counter).qtd := qt;
      l_summ(l_counter).ytd := yt;
      l_summ(l_counter).todayx := tdx ;
      l_summ(l_counter).mtdx := mtx;
      l_summ(l_counter).qtdx := qtx;
      l_summ(l_counter).ytdx := ytx;

  end loop;

close c1;

/* print out all headers and statistics */

dbms_output.put_line(c_blankline||rpad(' ',50,' ')||
                       lpad(l_refdte,c_col_width,' ')||' '||
                       lpad('MTD-'||substr(l_refdte,8,4),c_col_width,' ')||' '||
                       lpad('QTD-'||substr(l_refdte,8,4),c_col_width,' ')||' '||
                       lpad('YTD-'||substr(l_refdte,8,4),c_col_width,' ')||' '||
                       lpad('Same Day-'||substr(l_lastdte,8,4),c_col_width,' ')||' '||
                       lpad('MTD-'||substr(l_lastdte,8,4),c_col_width,' ')||' '||
                       lpad('QTD-'||substr(l_lastdte,8,4),c_col_width,' ')||' '||
                       lpad('YTD-'||substr(l_lastdte,8,4),c_col_width,' '));

dbms_output.put_line('Shipping Summary:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));

/* start printing Shipping Summary Info */

for i in 0..3 loop
-- detail lines
  print_line(l_summ(i).status,
	     l_summ(i).today,
	     l_summ(i).mtd,
	     l_summ(i).qtd,
	     l_summ(i).ytd,
	     l_summ(i).todayx,
	     l_summ(i).mtdx,
             l_summ(i).qtdx,
             l_summ(i).ytdx); 

end loop;

dbms_output.put_line(c_blankline);

l_shpcat_col:=4;

for i in 0..5 loop

 print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

dbms_output.put_line(c_blankline);

 print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

dbms_output.put_line(c_blankline);

/* Begin printing Picking Summary info */

dbms_output.put_line('Picking Summary:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));

l_shpcat_col:= 11;

for i in 0..2 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

/* Begin Printing Receiving Summary Info */

dbms_output.put_line(c_blankline);

dbms_output.put_line('Receiving Summary:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));

for i in 0..5 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

/* Begin Printing Production Summary Info */

dbms_output.put_line(c_blankline);

dbms_output.put_line('Production Summary:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));


for i in 0..3 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

/* Begin Printing Warehouse Storage Info */

dbms_output.put_line(c_blankline);

dbms_output.put_line('Warehouse Storage:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));

for i in 0..6 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

/* Begin Printing Cube Information */

dbms_output.put_line(c_blankline);

dbms_output.put_line('Cubic Volume Data:'||rpad(' ',50,' ')||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' ')||' '||
                       lpad(' ',c_col_width,' '));

for i in 1..2 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

dbms_output.put_line(c_blankline);

for i in 1..2 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

dbms_output.put_line(c_blankline);

for i in 1..3 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

print_line('Total Inventory $ Value',
             l_summ(35).today + l_summ(36).today+l_summ(37).today,
             0,0,0,0,0,0,0);

dbms_output.put_line(c_blankline);

for i in 1..2 loop

print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;

dbms_output.put_line(c_blankline);
for i in 0..47 loop
print_line(l_summ(l_shpcat_col).status,
             l_summ(l_shpcat_col).today,
             l_summ(l_shpcat_col).mtd,
             l_summ(l_shpcat_col).qtd,
             l_summ(l_shpcat_col).ytd,
             l_summ(l_shpcat_col).todayx,
             l_summ(l_shpcat_col).mtdx,
             l_summ(l_shpcat_col).qtdx,
             l_summ(l_shpcat_col).ytdx);

l_shpcat_col:= l_shpcat_col + 1;

end loop;


end;
/
