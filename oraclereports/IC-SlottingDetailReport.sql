/* #REPORTNAME=IC Slotting Detail Report */
/* #HELPTEXT= This information is combined with data from */
/* #HELPTEXT =  Demand Solutions and includes units shipped */
/* #HELPTEXT= for the last 12 months.  Requested by Senior Management */

ttitle left print_time center 'IC Slotting Detail Report' -
       right print_date skip 2 
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a20
column lngdsc heading 'Description'
column lngdsc format a30
column untcst heading 'Unit Cost'
column untcst format 999999.999
column available heading 'DCS5 on Hand'
column available format 999999
column vc_prdcod heading 'Prod|Class'
column vc_prdcod format a5
column vc_itmgrp heading 'Category'
column vc_itmgrp format a10
column vc_itmtyp heading 'Type'
column vc_itmtyp format a8
column prtfam heading 'Part Family'
column prtfam format a11
column total heading 'Total'
column total format 999999
column january  heading 'January'
column january format 999999
column february heading 'February '
column february format 999999
column march heading 'March'
column march format 999999
column april heading 'April'
column april format 999999
column may heading 'May'
column may format 999999
column june heading 'June'
column june format 999999
column july heading 'July'
column july format 999999
column august heading 'August'
column august format 999999
column september heading 'September'
column september format 999999
column october heading 'October'
column october format 999999
column november heading 'November'
column november format 999999
column december heading 'December'
column december format 999999
column untpal heading 'Unt/Pallet'
column untpal format 999,999,999
column untcas heading 'Units/Carton'
column untcas format 999,999,999
column untshp heading 'Total Shipped = Past 12 Months'
column untshp format 999,999,999

set linesize 700 
set pagesize 20000

select c.prtnum,substr(e.lngdsc,1,30) lngdsc, c.untcst, usrGetAvailQtyA(c.prtnum) available,
       rpad(c.vc_prdcod,5,' ') vc_prdcod, c.vc_itmgrp, c.vc_itmtyp,rpad(c.prtfam,11,' ') prtfam,
       nvl(a.total,0) total,  nvl(a.january,0) january,
       nvl(a.february,0) february,  nvl(a.march,0) march,  nvl(a.april,0) april,
       nvl(a.may,0) may,  nvl(a.june,0) june, nvl(a.july,0) july,  nvl(a.august,0) august,
       nvl(a.september,0) september,  nvl(a.october,0)
       october,  nvl(a.november,0) november,
       nvl(a.december,0) december, c.untcas,
       c.untpal,nvl(x.shpqty,0) untshp 
      from usr_forecasta a, forecast_temp1 x, invsum b, prtmst c, var_alt_prtnum d, prtdsc e
      where c.prtnum = x.prtnum(+)
      and c.prtnum=a.prtnum(+)
      and c.prtnum = b.prtnum(+)
      and c.prtnum = d.prtnum(+)
      and c.prtnum||'|----' = e.colval
      and e.colnam = 'prtnum|prt_client_id'
      and e.locale_id='US_ENGLISH'
      and c.prt_client_id =b.prt_client_id(+)
      group by c.prtnum, e.lngdsc, c.untcst,c.vc_prdcod,
      c.vc_itmgrp,  c.vc_itmtyp, c.prtfam,
      a.total, a.january, a.february, a.march,
      a.april, a.may, a.june, a.july, a.august,
      a.september, a.october, a.november,
    a.december, c.untcas, c.untpal,x.shpqty
/
