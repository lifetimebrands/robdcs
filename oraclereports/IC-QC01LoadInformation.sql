/* #REPORTNAME=IC - QC01 Load Information */
/* #HELPTEXT= This report lists information */
/* #HELPTEXT= on load numbers currently  */
/* #HELPTEXT= located in QC01 */

ttitle left print_time center 'IC - QC01 Load Information' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format A20
column lodnum heading 'Load Number'
column lodnum format A30
column prtnum heading 'Part Number'
column prtnum format A30
column lotnum heading 'Lot Number'
column lotnum format A20
column untqty heading 'Quantity'
column untqty format 9,999,999
column lstmov heading 'Arrived Date'
column lstmov format date
column trknum heading 'File Number'
column trknum format A20

alter session set nls_date_format ='DD-MON-YYYY HH24:MI:SS';
set linesize 200
set pagesize 200

select invlod.stoloc, invlod.lodnum, invdtl.prtnum, invdtl.lotnum, sum(invdtl.untqty) untqty,
invlod.lstmov, rcvlin.trknum
from invlod, invsub, invdtl, rcvlin, rcvtrk, trlr
where invlod.stoloc='QC01'
and invlod.lodnum=invsub.lodnum
and invsub.subnum=invdtl.subnum
and invdtl.rcvkey=rcvlin.rcvkey
and rcvlin.trknum=rcvtrk.trknum
and rcvtrk.trlr_id=trlr.trlr_id
group by invlod.stoloc, invlod.lodnum, invdtl.prtnum, invdtl.lotnum, invlod.lstmov, rcvlin.trknum
order by invlod.lstmov, invlod.lodnum;
