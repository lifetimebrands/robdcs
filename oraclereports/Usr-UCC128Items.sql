/* #REPORTNAME=User UCC128 Items*/
/* #HELPTEXT= Lists information on items */
/* #HELPTEXT= that are in UCC128 status, */
/* #HELPTEXT= but also exist in Available status. */
/* #HELPTEXT= Requested by QC */


column prtnum heading 'Part Number'
column prtnum format a30
column lotnum heading 'Lot Number'
column lotnum format a20
column invsts heading 'Status'
column invsts format a15
column untqty heading 'Unit Qty.'
column untqty format 999,999,999
column comqty heading 'Committed Qty.'
column comqty format 999,999,999
column avlqty heading 'Available Qty.'
column avlqty format 999,999,999
column pndqty heading 'Pendqty Qty.'
column pndqty format 999,999,999
column expqty heading 'Expected Qty.'
column expqty format 999,999,999
column untcst heading 'Unit Cost'
column untcst format 999,999,999.99

set linesize 200
set pagesize 1000

select a.prtnum, a.lotnum, rtrim(b.lngdsc) invsts, a.untqty, a.comqty, a.avlqty,
a.pndqty, a.expqty, c.untcst
from var_prtqty_view a, dscmst b, prtmst c
where a.invsts='J'
and a.prtnum in (select prtnum from var_prtqty_view where invsts='A')
and a.prtnum=c.prtnum
and c.prt_client_id='----'
and b.colnam='invsts'
and b.locale_id='US_ENGLISH'
and a.invsts=b.colval
order by a.prtnum;
