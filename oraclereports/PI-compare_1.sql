/* #REPORTNAME=PI Cycle Count vs. Identify Detail Report */
/* #HELPTEXT= This report compares counted inventory */
/* #HELPTEXT= against actual inventory */
/* #HELPTEXT= Requested by Inventory Control */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'PI Cycle Count vs. Identify Detail Report' -
right print_date skip 2 -


column stoloc heading 'Location'
column stoloc format A30
column prtnum heading 'Item        '
column prtnum format  A14
column inval1 heading 'Inval      '
column inval1 format A20
column countqty heading 'Count Qty     '
column countqty format 999999999
column actualqty heading 'Identified Qty'
column actualqty format 9999999999

select b.stoloc, b.prtnum, b.inval1, 
      sum (b.cntqty) countqty , sum (id.untqty) actualqty,
      b.cnt_usr_id
 from cntwrk b, invlod il, invsub ib, invdtl id
  where 
  b.cntsts = 'C'
  and b.stoloc = il.stoloc 
  and il.lodnum = ib.lodnum and ib.subnum = id.subnum
group by b.stoloc, b.prtnum, b.inval1, b.cnt_usr_id
having sum (b.cntqty) != sum (id.untqty)
/
