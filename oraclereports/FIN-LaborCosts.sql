/* #REPORTNAME=FIN Labor and Production OverHead Costs Report */
/* #VARNAM=Start-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */
/* #HELPTEXT= This report calculates FIN Labor and Production OverHead Costs. */
/* #HELPTEXT= Date should be entered in dd-mon-yyyy format. */
/* #HELPTEXT = Requested by Controller */

ttitle left  print_time -
       center 'FIN Labor and Production OverHead Costs Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

alter session set nls_date_format ='dd-mon-yyyy';

column tot_cnsqty heading 'Labor Cost'
column tot_cnsqty format 99,999,999.99
column clsdte heading 'Month'
column prtnum heading 'Item'
column prtnum format a15
column clsdte format a14

alter session set nls_date_format ='dd-mon-yyyy';

select sum(a.tot_cnsqty) tot_cnsqty, substr(b.clsdte,4,3) clsdte, a.prtnum 
from wkodtl a , wkohdr b
where a.wkonum = b.wkonum
and a.prtnum = 'LABOR'
and b.clsdte between '&1' and '&2'
and b.wkosts = 'C'
group by substr(b.clsdte,4,3), a.prtnum 
union
select sum(a.tot_cnsqty) tot_cnsqty, substr(b.clsdte,4,3) clsdte, a.prtnum 
from wkodtl a , wkohdr b
where a.wkonum = b.wkonum
and a.prtnum = 'PRODOHLABR'
and b.clsdte between '&1' and '&2'
and b.wkosts = 'C'
group by substr(b.clsdte,4,3), a.prtnum
/
