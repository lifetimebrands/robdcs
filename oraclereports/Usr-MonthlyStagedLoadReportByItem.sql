/* #REPORTNAME=User Monthly Staged Load Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is staged. Requested by Shipping. */ 


column schdte heading 'SchShipDate'
column schdte format a11
column stgdte heading 'StgDte'
column stgdte format a13
column shipid heading 'Ship ID'
column shipid format a14
column prtnum heading 'Item'
column prtnum format a12
column load heading 'Load Number'
column load format a20
column stoloc heading 'Location'
column stoloc format a20
column cpodte heading 'CnlDte'
column cpodte format a13
column cponum heading 'PO #'
column cponum format a18

set linesize 200
set pagesize 5000

alter session set nls_date_format ='DD-MON-YYYY'
/
spool staged.out

select 
       rtrim(ord.cponum) cponum,
       rtrim(sh.ship_id) shipid,
       rtrim(invlod.lodnum) load,
       rtrim(invlod.stoloc) stoloc,
       rtrim(sd.ordnum) ordnum,
       rtrim(pm.untcst) untcst,
       rtrim(ord_line.prtnum) prtnum,
       sd.stgqty,
       sh.stgdte stgdte,
       ord.cpodte cpodte
 from
       ord,
       ord_line,  shipment_line sd,
       shipment sh, invlod,
       invsub, invdtl, locmst lm,
       aremst am, prtmst pm,
       prtdsc
       WHERE
        ord.ordnum                      = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND ord_line.prtnum             = pm.prtnum
        AND pm.prt_client_id            = '----'
        AND sd.ship_id                  = sh.ship_id
        AND sh.shpsts                   in ('S','P')       
        AND invdtl.ship_line_id         = sd.ship_line_id
        AND invdtl.subnum               = invsub.subnum
        AND invsub.lodnum               = invlod.lodnum
        AND invlod.stoloc               = lm.stoloc
        AND lm.arecod                   = am.arecod
        AND am.stgflg                   = 1 
        AND pm.prtnum||'|----'           = prtdsc.colval
        AND prtdsc.colnam               = 'prtnum|prt_client_id'
        AND prtdsc.locale_id            = 'US_ENGLISH'
group
    by
       ord.cponum,
       sh.ship_id,
       sd.ordnum, 
       ord_line.prtnum,
       invlod.lodnum,
       invlod.stoloc,
       sh.shpsts,
       sh.stgdte ,
       sd.stgqty,
       ord.cpodte,
       pm.untcst
/
spool off
exit
