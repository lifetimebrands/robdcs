/* #REPORTNAME=Usr - Trailer Dispatch Information */
/* #HELPTEXT= Returns the ship ID and dispatch */
/* #HELPTEXT= date based on the trailer ID the.*/
/* #HELPTEXT= user enters.*/
/* #VARNAM=Trailer_ID , #REQFLG=Y */


column trlr_id heading 'Trailer ID'
column trlr_id format a10
column ship_id heading 'Shipment ID'
column ship_id format a11
column dispatch_dte heading 'Dispatch Date'
column dispatch_dte format date

alter session set nls_date_format = 'DD-MON-YYYY HH:MI:SS AM';

select a.trlr_id, d.ship_id, a.dispatch_dte
from trlr a, car_move b, stop c, shipment d
where a.trlr_id='&1'
and a.trlr_id=b.trlr_id
and b.car_move_id=c.car_move_id
and c.stop_id=d.stop_id;
