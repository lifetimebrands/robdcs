/* #REPORTNAME = Open Sales Orders For Shipping */
/* #HELPTEXT= Requested by Shipping Department */
/* #HELPTEXT= Returns only ready, staged, */
/* #HELPTEXT= released and loaded status. */
/* #HELPTEXT= Also returns the Ship ID. */ 

column cponum heading 'PO Number'
column cponum format A20
column ordnum heading 'Order'
column ordnum format A10
column ship_id heading 'Ship ID'
column ship_id format A15
column status heading 'Status'
column status format A10
column Ship_To_Name  heading 'Name'
column Ship_To_Name  format A15
column early_shpdte heading 'Early'
column early_shpdte format A11
column Cancel_Date heading 'Late'
column Cancel_Date format A11 
column unsamt heading '$'
column unsamt format '9,999,990.00'
set pagesize 50000;
set linesize 500;

alter session set nls_date_format ='MM/DD/YYYY';


SELECT
        distinct rtrim(ord.cponum) cponum, 
        rtrim(ord.ordnum) ordnum,
        rtrim(shipment.ship_id) ship_id,       
        rtrim(dscmst.lngdsc) status,
        rtrim( substr(lookup1.adrnam,1,15))  Ship_To_Name, 
        rtrim(ord_line.early_shpdte) early_shpdte,    
        rtrim(ord_line.late_shpdte) Cancel_Date,
        decode(shipment_line.linsts, 'I', sum(ord_line.vc_cust_untcst * (shipment_line.stgqty+shipment_line.inpqty)),
        sum(ord_line.vc_cust_untcst*shipment_line.pckqty)) unsamt
       FROM pckbat, stop, dscmst, adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, 
           shipment_line, shipment, ord_line, ord
       WHERE ord.st_adr_id              = lookup1.adr_id
         AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
        and ord.stcust = cstmst.cstnum (+)
        and ord.client_id = cstmst.client_id (+)
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = shipment_line.client_id
        AND ord_line.client_id          ='----'
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.schbat        = pckbat.schbat (+)
        AND shipment_line.ship_id       = shipment.ship_id
        AND shipment.stop_id            = stop.stop_id (+)
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND shipment.shpsts             <> 'B'
        AND shipment.shpsts             <> 'L'
        AND shipment.shpsts             <> 'X'
        GROUP BY ord.cponum, ord.ordnum, dscmst.lngdsc,
        lookup1.adrnam, ord_line.early_shpdte, ord_line.late_shpdte, 
        shipment_line.linsts, shipment.ship_id 
/
