/* #REPORTNAME=IC Daily Adjustments Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=trndte1 , #REQFLG=Y */
/* #VARNAM=reacod , #REQFLG=Y */
/* #HELPTEXT= Please enter date in dd-mon-yyyy format */
/* #HELPTEXT = Requested by Inventory Control */

-- * #GROUPS=NOONE */

ttitle left - 
       center 'IC Daily Adjustments Report ' -
       right h_print_date skip 1 -
       center 'from &1  to &2' skip 2

break on report

compute sum of adjqty on report
compute sum of adjamt on report
set lines 240

column prtnum heading 'Part Number'
column prtnum format a20
column vc_prdcod heading 'Prod Class'
column vc_prdcod format A10
column lngdsc heading 'Description'
column lngdsc format a50
column reacod heading 'Reason'
column reacod format a20
column adjqty heading 'Adj Quantity'
column adjqty format 999,990
column adjamt heading 'Adj Amount'
column adjamt format 99,999,990.00
column trndte heading 'Tran Date'
column trndte format a15
column print_date new_value h_print_date noprint
column start_date new_value h_start_date noprint
column end_date   new_value h_end_date   noprint

alter session set nls_date_format ='dd-mon-yyyy';

select 
       a.prtnum, 
       a.vc_prdcod,
       b.lngdsc,
       c.reacod,
       sum(c.trnqty) adjqty, c.trndte,
       sum(c.trnqty)*nvl(a.untcst,0) adjamt
  from prtdsc b, prtmst a, dlytrn c
where a.prtnum=c.prtnum
and a.prtnum||'|----' = b.colval
 and b.locale_id ='US_ENGLISH'
   and b.colnam = 'prtnum|prt_client_id'
   and c.actcod = 'ADJ' and c.prtnum != 'LABOR'
   and trunc(c.trndte) 
       between 
       '&1' and '&2'
       and c.reacod = '&3'
 group
    by c.trndte, c.reacod, a.prtnum,
       a.vc_prdcod,
       b.lngdsc,
      a.untcst
having sum(c.trnqty) != 0
/
