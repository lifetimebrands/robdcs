/* This script is used to update the usr_pallet_counts table */
/* which is used in the Yearly Receiving Report. */

-- Pallets Received 

insert into usr_pallet_counts(palcnt,clsdte)
select count(distinct invlod.lodnum),trunc(rcvtrk.clsdte)                            
       from trlr,rcvtrk,rcvinv,rcvlin, invsub,invlod,invdtl                                    
       where                                    
       rcvtrk.trknum = rcvlin.trknum 
       and rcvtrk.trlr_id = trlr.trlr_id                                                                    
       and rcvlin.trknum = rcvinv.trknum                                                                     
       and rcvlin.client_id = rcvinv.client_id                                                                     
       and rcvlin.supnum = rcvinv.supnum                                                                     
       and rcvlin.invnum = rcvinv.invnum                                                                     
       and rcvlin.rcvkey = invdtl.rcvkey(+)                                                               
       and invdtl.subnum = invsub.subnum                                                           
       and invsub.lodnum = invlod.lodnum                                                            
       --and rcvtrk.expdte is not null                                                            
       and trlr.trlr_stat = 'C'                                                            
       and trunc(rcvtrk.clsdte) = trunc(sysdate) -1
group by trunc(rcvtrk.clsdte);

commit;

/* insert into usr_pallet_counts(clsdte,palcnt) */
/*    select trunc(a.clsdte),count(distinct b.lodnum) */
/*     from rcvtrk a,dlytrn b */
/*     where a.trknum = b.tostol */
/*     and a.trksts = 'C' */
/*     and b.oprcod = 'CRE' */
/*     and trunc(a.clsdte) = trunc(sysdate) -1 */ 
/*    group by trunc(a.clsdte); */
/* commit; */

-- Pallets Shipped

/* select c.ship_id, count(distinct invlod.lodnum) ,trunc(g.dispatch_dte) trndte */
/*                      from ord d,ord_line a,shipment_line b,shipment c, stop e, */
/*                       car_move f,trlr g, invlod, invsub, invdtl i */
/*                       where i.ship_line_id = b.ship_line_id */
/*                       and i.subnum = invsub.subnum  */
/*                        and invlod.lodnum = invsub.lodnum */
/*                       and d.ordnum = a.ordnum       */
/*                       and d.client_id = a.client_id */
/*                     and d.client_id = b.client_id */
 /*                    and d.rt_adr_id = c.rt_adr_id */
/*                     and a.ordnum = b.ordnum */
 /*                    and a.ordlin = b.ordlin */
  /*                   and a.client_id = b.client_id */
 /*                    and a.ordsln = b.ordsln  */
 /*                    and b.ship_id = c.ship_id */
 /*                    and c.stop_id = e.stop_id */
 /*                    and e.car_move_id  = f.car_move_id */
 /*                    and f.trlr_id  = g.trlr_id */
 /*                    and a.client_id = '----' */
 /*                    and c.shpsts = 'C' */
 /*           and trunc(g.dispatch_dte)  = '03-SEP-2004' */
 /*       group by c.ship_id, trunc(g.dispatch_dte) */

