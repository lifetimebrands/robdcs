
/* #REPORTNAME=IC RACKHIGH Report */
/* #HELPTEXT = Requested by Engineering */


ttitle left print_time center 'IC RACKHIGH Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a10
column stoloc heading 'Location'
column stoloc format a12
column trvseq heading 'Trv. Seq.'
column trvseq format 999999
column curqvl heading 'Current Inv.'
column curqvl format 99
column useflg heading 'Used'
column useflg format a3
column pndqvl heading 'Pending'
column pndqvl format 9999
select arecod, stoloc, trvseq, curqvl, pndqvl, decode(useflg,0,'N','Y') useflg
from locmst
where arecod in ('RACKHIGH')
/
