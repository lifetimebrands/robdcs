/* #REPORTNAME=User Update Lot Number */
/* #REPORTTYPE=DCSCMD */
/* #GROUPS=SYS,SUPR,WHSM,CUSS,CUST */
/* #VARNAM=cponum , #REQFLG=Y */
/* #VARNAM=lotnum , #REQFLG=Y */
/* #HELPTEXT= Requested by Order Management. */

update ord_line a 
set lotnum = '&2' where exists(select b.ordnum from ord b
where a.ordnum=b.ordnum and b.client_id = '----' and b.cponum ='&1')
and exists(select 'x' from ord c
where a.ordnum=c.ordnum and c.client_id = '----' and c.cponum ='&1')
/
commit;
