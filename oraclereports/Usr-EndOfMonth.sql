/* #REPORTNAME=USR- End Of Month Report */
/* #HELPTEXT= This report lists information */
/* #HELPTEXT= on open orders at the end of the */
/* #HELPTEXT= month greater than $5000/customer */

column btcust heading 'Customer'
column btcust format A40
column cponum heading 'PO #'
column cponum format A30
column status heading 'Status'
column status format A10
column amt heading 'Unshipped $s'
column amt format 9,999,999.99
column added heading 'Date Order Recd At Facilitiy'
column added format A28
column early heading 'Requested Ship Date'
column early format A19
column cancel heading 'Cancel Date'
column cancel format A11
column routing heading 'Date Called For Routing'
column routing format A23
column pickup heading 'Date Trucker Expected to Pickup'
column pickup format A31
column comments heading 'Comments'
column comments format A50

alter session set nls_date_format ='MM/DD/YYYY';

set linesize 300
set pagesize 500

select a.custnam btcust, b.cponum cponum, a.status status, a.amt amt, 
to_char(b.adddte) added, to_char(b.early_shpdte) early, 
to_char(b.cancel_date) cancel,
a.routing routing, a.pickup pickup, a.comments comments
from 
(select distinct custnam, status, sum(amt) amt, ' ' routing, ' ' pickup, comments
from
(select distinct b.adrnam custnam, a.cponum cponum, a.status status, sum(a.unsamt) amt, a.adddte added,
a.early_shpdte early, a.cancel_date cancel,
case when a.adddte in (to_char(trunc(sysdate)-1,'MM/DD/YYYY'), to_char(trunc(sysdate)-2,'MM/DD/YYYY'))
then '4 - Received Order < 48 Hours Notice'
     when a.adddte=to_char(trunc(sysdate)-3,'MM/DD/YYYY') then '3- Received Order Between 48 and 72 Hours Notice'
     when a.adddte<to_char(trunc(sysdate)-3,'MM/DD/YYYY') and a.status in ('Pending','Backorder') then '2 - No Inventory Available'
     else ' '
     end comments
from usr_openords a, adrmst b
where a.early_shpdte< (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
and a.btcust=b.host_ext_id
and a.btcust in (select btcust
from usr_openords
where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
group by btcust
having sum(unsamt)>=5000)
group by a.early_shpdte, a.cponum, b.adrnam, a.status, a.cancel_date, a.adddte)
group by custnam, status, comments
having count(cponum)=1
order by custnam, status, comments) a, (select a.cponum, a.status, b.adrnam, a.adddte, a.early_shpdte, a.cancel_date,
sum(a.unsamt) amt from usr_openords a, adrmst b
where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
and a.btcust=b.host_ext_id
group by cponum, status, adddte, early_shpdte, cancel_date, adrnam) b
where a.status=b.status
and a.amt=b.amt
and a.custnam=b.adrnam
union /* to get multiple */
select distinct btcust, case when count(distinct cponum)>1 then 'Various'
else 'cponum'
end cponum, status, sum(amt) amt, case when count(distinct added)>1 then 'Various'
else 'Various'
end added, case when count(distinct early)>1 then 'Various'
else 'Various'
end early, case when count(distinct cancel)>1 then 'Various'
else 'Various'
end cancel, ' ' routing, ' ' pickup, comments
from
(select distinct b.adrnam btcust, a.cponum cponum, a.status status, sum(a.unsamt) amt, a.adddte added, 
a.early_shpdte early, a.cancel_date cancel, 
case when a.adddte in (to_char(trunc(sysdate)-1,'MM/DD/YYYY'), to_char(trunc(sysdate)-2,'MM/DD/YYYY')) 
then '4 - Received Order < 48 Hours Notice'
     when a.adddte=to_char(trunc(sysdate)-3,'MM/DD/YYYY') then '3- Received Order Between 48 and 72 Hours Notice'
     when a.adddte<to_char(trunc(sysdate)-3,'MM/DD/YYYY') and a.status in ('Pending','Backorder') then '2 - No Inventory Available'
     else ' '
     end comments
from usr_openords a, adrmst b
where a.early_shpdte< (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
and a.btcust=b.host_ext_id
and a.btcust in (select btcust
from usr_openords 
where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
group by btcust
having sum(unsamt)>=5000)
group by a.early_shpdte, a.cponum, b.adrnam, a.status, a.cancel_date, a.adddte)
group by btcust, status, comments
having count(cponum)>1
union /* to get sum */
select '' btcust, '' cponum, 'Amt'status, sum(a.unsamt) amt, ''added, ''early, 
''cancel, ''routing,'' pickup, '' comments
from usr_openords a, adrmst b
where a.early_shpdte< (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
and a.btcust=b.host_ext_id
and a.btcust in (select btcust
from usr_openords
where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
group by btcust
having sum(unsamt)>=5000)
union /* to get difference */
select '' btcust, '' cponum, 'Diff' status, sum(b.amt-a.amt), ''added, ''early, 
''cancel, ''routing,'' pickup, '' comments from (select sum(a.unsamt) amt
from usr_openords a, adrmst b
where a.early_shpdte< (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
and a.btcust=b.host_ext_id
and a.btcust in (select btcust
from usr_openords
where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
group by btcust
having sum(unsamt)>=5000)) a, (select sum(unsamt) amt
from usr_openords where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)) b
union /* to get grand total */
select '' btcust, '' cponum, 'Total'status, sum(unsamt) amt, ''added, ''early, 
''cancel, ''routing,'' pickup, '' comments
from usr_openords where early_shpdte < (select rtrim(to_char(trunc(sysdate),'MM/DD/YYYY')) from dual)
order by btcust, status, comments;
