/* #REPORTNAME=User Packaging On Hand Report */
/* #HELPTEXT= This report lists all quantities per specific */
/* #HELPTEXT= items, requested by Packaging                */
/* #HELPTEXT= Requested by Packaging Department             */

 ttitle left '&1' print_time center 'User Packaging On Hand  Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column avlqty heading 'Available'
column avlqty format 999999


select prtnum,  nvl(substr(lngdsc,1,30),'Not Available') lngdsc,
  sum(nvl(((untqty+pndqty) - comqty),0)) avlqty
  from
  (select distinct a.prtnum,b.stoloc,  b.untqty,  b.pndqty,  b.comqty,
   substr(c.lngdsc,1,30) lngdsc, nvl(pckflg,1) pckflg
   from usr_packaging a, invsum b,  dscmst c, locmst d
   where a.prtnum = b.prtnum(+)
   and b.stoloc = d.stoloc(+)
   and b.prt_client_id(+) ='----'
   and b.invsts = c.colval(+)
   and c.colnam(+) ='invsts'
   and c.locale_id (+) ='US_ENGLISH')
   where pckflg = 1
   and (untqty !=0 or untqty is null)
   group by prtnum,  nvl(substr(lngdsc,1,30),'Not Available')
/
