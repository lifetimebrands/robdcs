/* #REPORTNAME=User Staged Loaded Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is staged. Report includes the ship status. Requested by Shipping */ 
ttitle left '&1 ' print_time -
		center 'User Staged Loaded Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column schdte heading 'SchShipDate'
column schdte format a11
column schbat heading 'Sch Batch#'
column schbat format a10
column stgdte heading 'StgDte'
column stgdte format a13
column NUMCTN heading '# Ctns'
column NUMCTN format 999999
column shipid heading 'Ship ID'
column shipid format a14
column stname heading 'Ship To Name'
column stname format a45
column carcod heading 'Carrier'
column carcod format a15
column cpodte heading 'CnlDte'
column cpodte format a13
column extdte heading 'ExtDte'
column extdte format a13
column cponum heading 'PO #'
column cponum format a18
column shpsts heading 'Status'
column shpsts format a10

set linesize 500
set pagesize 5000

alter session set nls_date_format ='DD-MON-YYYY'
/

select sh.entdte schdte,sd.schbat,
       rtrim(lookup1.adrnam) stname,
       rtrim(ord.cponum) cponum,
       rtrim(sh.ship_id) shipid,
       count(distinct invsub.subnum) NUMCTN,
       sh.carcod,
       sh.stgdte stgdte,
       ord.cpodte cpodte,
       ord_line.vc_extdte extdte,decode(shpsts,'L','Loading','S','Staged','X','Transfer') shpsts
 from
       locmst lm,
       aremst am,
       invlod,
       invsub,
       invdtl,
       shipment_line  sd,
       shipment sh,
       ord,
       ord_line,
       dscmst, adrmst lookup1
      WHERE invlod.stoloc = lm.stoloc
   AND lm.arecod                   = am.arecod
        AND am.stgflg                   = 1
        AND invlod.lodnum = invsub.lodnum
        AND invsub.subnum = invdtl.subnum
        AND ord.st_adr_id              = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND invdtl.ship_line_id         = sd.ship_line_id
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND dscmst.colval               = sh.shpsts
        AND sh.shpsts                   in ('S','L','P','X')
group
    by
       sh.entdte,sd.schbat,
       lookup1.adrnam,
       ord.cponum,
       sh.ship_id,
       sh.carcod,
       sh.shpsts,
       sh.stgdte ,
       ord.cpodte,
       ord_line.vc_extdte,decode(shpsts,'L','Loading','S','Staged','X','Transfer')
/
