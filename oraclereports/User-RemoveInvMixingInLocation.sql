/* #REPORTNAME=User Remove Inventory Mixing in Locations  */
/* #VARNAM=Detail#, #REQFLG=Y */
/* #VARNAM=stoloc , #REQFLG=Y */

/* #HELPTEXT= This deletes the inventory mixing in the Location   */
/* #HELPTEXT= Requested by Order Management */
/* #GROUPS=NOONE */

set pagesize 50
set linesize 160


ttitle left print_time -
center 'User Remove  Inventory Moves for Detail &1  and Location &2 ' -
right print_date skip 2 -


delete from invmov where lodnum = '&1' and stoloc ='&2'
;
commit;
