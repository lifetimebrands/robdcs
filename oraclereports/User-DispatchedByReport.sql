/* #REPORTNAME=User Dispatched By Report */
/* #HELPTEXT= This report lists users that dispatch certain shipments. */ 
/* #HELPTEXT= User is required to enter the SID or waybill# */
/* #HELPTEXT= for the dispatched load.  */
/* #HELPTEXT= Requested by Shipping */
/* #GROUPS=NOONE */
/* #VARNAM=ship_id , #REQFLG=Y */
/* #VARNAM=doc_num , #REQFLG=Y */

column ship_id heading 'Ship Id'
column ship_id format a12
column mod_usr_id heading 'User Id'
column mod_usr_id format a30
column doc_num heading 'Waybill#'
column doc_num format a20
column dispatch_dte heading 'Dispatch Date'
column dispatch_dte format a13

set linesize 100

alter session set nls_date_format = 'dd-mon-yyyy';

select distinct shipment.ship_id,trlr.mod_usr_id,shipment.doc_num,trlr.dispatch_dte  
from pckmov, pckwrk, shipment, stop,
car_move, trlr   
where shipment.stop_id  = stop.stop_id 
and stop.car_move_id  = car_move.car_move_id
and car_move.trlr_id
=trlr.trlr_id     and trlr.trlr_stat   in  ('D')
and shipment.loddte is not null
and trlr.dispatch_dte is not null
and shipment.ship_id  =
pckwrk.ship_id     and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
= pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY')
and (shipment.ship_id = '&1' or shipment.doc_num = '&2')
/
