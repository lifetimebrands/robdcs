/* #REPORTNAME=  IC Over Size Pallet Report  */
/* #HELPTEXT= Requested by Inventory Control. */

column prtnum heading 'Item'
column prtnum format a12
column vc_itmgrp heading 'Group'
column vc_itmgrp format a7
column vc_itmtyp heading 'Type'
column vc_itmtyp format a5
column vc_itmcls heading 'Class'
column vc_itmcls format a7
column ftpcod heading 'Footprint'
column ftpcod format a35
column untpak heading 'Unit/|Pack'
column untpak format 999999
column config_untpal heading 'Unit/|Pallet'
column config_untpal format 999999
column cashgt heading 'Height'
column cashgt format 999999
column caslvl heading 'Level'
column caslvl format 999999
column config_untcas heading 'Units/|Case'
column config_untcas format 99999
column config_cases_per_pallet heading 'Cases/|Pallet'
column config_cases_per_pallet format 999999
column config_tiers_per_pallet heading 'Tiers/|Pallet'
column config_tiers_per_pallet format 999999
column config_palhgt heading 'Pallet/|Hgt'
column config_palhgt format 999999
column stoloc heading 'Location'
column stoloc format a10

select   distinct
         rtrim(invsum.stoloc) stoloc,
         rtrim(prtmst.prtnum) prtnum,
         rtrim(prtmst.vc_itmgrp) vc_itmgrp,
         rtrim(prtmst.vc_itmtyp) vc_itmtyp,
         rtrim(prtmst.vc_itmcls) vc_itmcls,
         rtrim(ftpmst.ftpcod) ftpcod,
         prtmst.untpak,
         prtmst.untpal config_untpal,
         prtmst.untcas config_untcas,
         ftpmst.cashgt,
         ftpmst.caslvl,
         (prtmst.untpal/prtmst.untcas) config_cases_per_pallet,
         ceil((prtmst.untpal/prtmst.untcas)/caslvl)  config_tiers_per_pallet,
         ceil(ceil((prtmst.untpal/prtmst.untcas)/caslvl) * ftpmst.cashgt ) config_palhgt
   from  ftpmst,
         prtmst, invsum, locmst, aremst
  where  ftpmst.ftpcod =  prtmst.ftpcod
         and prtmst.prtnum = invsum.prtnum
         and prtmst.prt_client_id = invsum.prt_client_id
         and invsum.stoloc = locmst.stoloc
         and invsum.arecod = aremst.arecod
         and invsum.prt_client_id = '----'
         and  ceil((ceil((prtmst.untpal/prtmst.untcas)/caslvl)) * ftpmst.cashgt)  > 47
         and prtmst.untpal != 0
/                  
