
set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=IC Damaged Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding damaged inventory */
/* #HELPTEXT= Requested by Inventory Control - October 2002 */
/* #GROUPS=NOONE */


ttitle  left print_date     print_time center 'IC Damaged Report' ski  -
btitle  center 'Page: ' format 999 sql.pno

column stoloc heading 'Loc.'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a11
column invsts heading 'Status'
column invsts format a8
column untqty heading 'Qty' 
column untqty format 9999999
column userid heading 'User'
column userid format a10
column cost heading '$'
column cost format 999,999.00
column lngdsc heading 'Description'
column lngdsc format a30
column lstdte heading 'Date'
column lstdte format a12

select   
invdtl.prtnum,
substr(prtdsc.lngdsc,1,30) lngdsc, 
locmst.stoloc,
decode(invdtl.invsts,'D','Damaged') invsts,
rtrim(trunc(invdtl.lstdte)) lstdte,
sum(invdtl.untqty) untqty,
sum(invdtl.untqty) * prtmst.untcst cost, invlod.lst_usr_id 
from
invlod, locmst, invsub, invdtl, prtmst, prtdsc   
where
invlod.stoloc = locmst.stoloc
and locmst.stoloc != 'PERM-ADJ-LOC'
and  invlod.lodnum = invsub.lodnum
and  invsub.subnum = invdtl.subnum
and invdtl.invsts = 'D'
and invdtl.prtnum = prtmst.prtnum
and invdtl.prt_client_id = prtmst.prt_client_id
and prtmst.prtnum|| '|----' = prtdsc.colval
and prtdsc.colnam='prtnum|prt_client_id'
and prtdsc.locale_id ='US_ENGLISH'
group by
invdtl.prtnum, locmst.stoloc,
invdtl.invsts, prtmst.untcst, prtdsc.lngdsc,
invlod.lst_usr_id, invdtl.lstdte
/
