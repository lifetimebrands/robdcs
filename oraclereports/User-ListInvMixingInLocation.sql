/* #REPORTNAME=User List Inventory that would cause mixing in the location */
/* #VARNAM=lodnum , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding inventory mixing in the location  */
/* #HELPTEXT= Requested by Order Management */
/* #GROUPS=NOONE */

set pagesize 50
set linesize 160


ttitle left print_time -
center 'User List Inventory mixing in Location  ' -
right print_date skip 2 -


select im.* from invmov im,
invdtl id, invsub ib
where (im.lodnum = ib.lodnum or im.lodnum = ib.subnum or im.lodnum = id.dtlnum)
and id.subnum = ib.subnum
and ib.lodnum = '&1'
/
