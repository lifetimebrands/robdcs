/* #REPORTNAME= FIN-KPI New */

/* #HELPTEXT= Provides a years worth of KPI Summary */
/* #HELPTEXT= Information by month. Report requires a */
/* #HELPTEXT= date range entered in the form of DD-MON-YYYY.*/
/* #HELPTEXT= Requested by the Controller */

/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */

/* Author: Al Driver */
/* Report uses codes in the usr_ledgerhdr table */
/* to display the appropriate heading and to pass */
/* the status through the package functions. */
/* Report only uses two functions in the KPI.pkg. UsrGetStats, which */
/* returns the summary info for the appropriate month. */
/* And usrGetAvg, which will return the average per Quarter.  This */
/* function is only used for the Inventory Columns, for */
/* which Inventory control requested averages for.  The */
/* report will calculate the average for each quarter based */
/* upon the months the end user inputs.  For example, if the */
/* users range only includes January, then the average for Q1 */
/* will only be for one month. If the range included Jan - Mar, */
/* then the average for Q1 would be calculated for 3 months. */
/* The Report was initially set up to provide a sum of each */
/* quarter, but the Controller said he did not need it. */ 


/* must put serveroutput on to see PL/SQL Output */

set serveroutput on   
set pagesize 500
set linesize 500

alter session set nls_date_format ='DD-MON-YYYY';


declare

/* create a record and table to hold cursor values */

type kpi_records is record
(category usr_ledgerhdr.header%TYPE, 
 jan number,
 feb number,
 mar number,
 Q1  number,
 apr number,
 may number,
 jun number,
 Q2  number,
 jul number,
 aug number,
 sep number,
 Q3  number,
 oct number(30),
 nov number,
 dec number,
 Q4  number);

type kpi_table is table of kpi_records
index by binary_integer;   /* use index by table to increase in size dynamically, */
                          /* do not have to use constructor or extend to add records */

kpi_sum kpi_table;

c_blankline varchar2(20):=chr(13);  /* use carriage return character to skip lines */

l_cell number:=0;      /* used to control cell input for ordered categories */

/* cursor holds data, in order */

cursor c1 is
select distinct b.row_num row_num, b.header header, b.status status,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,1) jan,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,2) feb,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,3) mar,
  nvl(UsrKPI4.usrGetAvgorSum(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,13),0) Q1,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,4) apr,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,5) may,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,6) jun,
  nvl(UsrKPI4.usrGetAvgorSum(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,14),0) Q2,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,7) jul,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,8) aug,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,9) sep,
  nvl(UsrKPI4.usrGetAvgorSum(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,15),0) Q3,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,10) oct,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,11) nov,
  UsrKPI4.usrGetStats(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,12) dec,
  nvl(UsrKPI4.usrGetAvgorSum(to_date('&&1','DD-MON-YYYY'),to_date('&&2','DD-MON-YYYY'),a.status,16),0) Q4
  from usr_weeklystats a,usr_ledgerhdr b
  where a.status(+) = b.status
  and b.status in ('OW','CW','FW','FWD','FW','WFU','PWD','PW','OD','CD','FD','DFU',
'FDD','PD','PDD','OO','CO','FO','SFU','FOD','PO','SPU','POD',
'TIC','RWP','PUP','DPU','WPU','DPW','SPW','WPW','PAS','TPL','RPL','DPL','WPL','LPR','LPD','LPW','LFR','LFD','LFW') 
  order by 1;

/* local procedure to print out categories and values , number format will apply to all values */ 

procedure print_line(i_category varchar2,
                     i_col1  number,
                     i_col2  number,
                     i_col3  number,
                     i_col4  number,
                     i_col5  number,
                     i_col6  number,
                     i_col7  number,
                     i_col8  number,
                     i_col9  number,
                     i_col10  number,
                     i_col11  number,
                     i_col12  number,
                     i_col13  number,
                     i_col14  number,
                     i_col15  number,
                     i_col16  number) is

c_num_fmt number:=999999990.99;

begin

dbms_output.put_line(rpad(i_category,45,' ') ||' '||
lpad(to_char(i_col1,c_num_fmt),12,' ')||' '||lpad(to_char(i_col2,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col3,c_num_fmt),12,' ')||' '||lpad(to_char(i_col4,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col5,c_num_fmt),12,' ')||' '||lpad(to_char(i_col6,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col7,c_num_fmt),12,' ')||' '||lpad(to_char(i_col8,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col9,c_num_fmt),12,' ')||' '||lpad(to_char(i_col10,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col11,c_num_fmt),12,' ')||' '||lpad(to_char(i_col12,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col13,c_num_fmt),12,' ')||' '||lpad(to_char(i_col14,c_num_fmt),12,' ')||' '||
lpad(to_char(i_col15,c_num_fmt),12,' ')||' '||lpad(to_char(i_col16,c_num_fmt),12,' '));

end;


begin

dbms_output.enable(1000000);


/* place all values in the table, in a specified order by using a loop */
/* rows should go in order because they were placed in the cursor in order */

for cur_rec in c1 loop

kpi_sum(l_cell).category:=cur_rec.header;
kpi_sum(l_cell).jan:=cur_rec.jan;
kpi_sum(l_cell).feb:=cur_rec.feb;
kpi_sum(l_cell).mar:=cur_rec.mar;
kpi_sum(l_cell).Q1:=cur_rec.Q1;
kpi_sum(l_cell).apr:= cur_rec.apr;
kpi_sum(l_cell).may:=cur_rec.may;
kpi_sum(l_cell).jun:=cur_rec.jun;
kpi_sum(l_cell).Q2:= cur_rec.Q2;
kpi_sum(l_cell).jul:=cur_rec.jul;
kpi_sum(l_cell).aug:=cur_rec.aug;
kpi_sum(l_cell).sep:=cur_rec.sep;
kpi_sum(l_cell).Q3:=cur_rec.Q3;
kpi_sum(l_cell).oct:= cur_rec.oct;
kpi_sum(l_cell).nov:=cur_rec.nov;
kpi_sum(l_cell).dec:=cur_rec.dec;
kpi_sum(l_cell).Q4:= cur_rec.Q4;

l_cell:=l_cell + 1;

end loop;

/* print headers, exactly like print_line procedure */

dbms_output.put_line(rpad('Performance',45,' ') ||' '||
lpad('January',12,' ')||' '||lpad('February',12,' ')||' '||
lpad('March',12,' ')||' '||lpad('Q1 Avg',12,' ')||' '||
lpad('April',12,' ')||' '||lpad('May',12,' ')||' '||
lpad('June',12,' ')||' '||lpad('Q2 Avg',12,' ')||' '||
lpad('July',12,' ')||' '||lpad('August',12,' ')||' '||
lpad('September',12,' ')||' '||lpad('Q3 Avg',12,' ')||' '||
lpad('October',12,' ')||' '||lpad('November',12,' ')||' '||
lpad('December',12,' ')||' '||lpad('Q4 Avg',12,' '));

dbms_output.put_line(c_blankline);

/* Print cells in order */

for i in 0..3 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

dbms_output.put_line(c_blankline);

for i in 4..9 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

dbms_output.put_line(c_blankline);

for i in 10..12 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

dbms_output.put_line(c_blankline);

for i in 13..15 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

dbms_output.put_line(c_blankline);

for i in 16..21 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

dbms_output.put_line(c_blankline);

for i in 22..27 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

for i in 28..40 loop

print_line(kpi_sum(i).category,
kpi_sum(i).jan,kpi_sum(i).feb,kpi_sum(i).mar,kpi_sum(i).Q1,
kpi_sum(i).apr,kpi_sum(i).may,kpi_sum(i).jun,kpi_sum(i).Q2,
kpi_sum(i).jul,kpi_sum(i).aug,kpi_sum(i).sep,kpi_sum(i).Q3,
kpi_sum(i).oct,kpi_sum(i).nov,kpi_sum(i).dec,kpi_sum(i).Q4);

end loop;

end; 
/

