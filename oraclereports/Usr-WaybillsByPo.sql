/* #REPORTNAME=User Way Bill By PO Report */
/* #VARNAM=cponum , #REQFLG=Y */
/* #HELPTEXT= This report lists ship id, way bill, PO */
/* #HELPTEXT= and order number */
/* #HELPTEXT= Requested by Sandy D.  */
/* #GROUPS=NOONE */

set pagesize 5000
set linesize 160


ttitle left print_time -
center 'User Waybill By Po Report for Purchase Order &1  ' -
right print_date skip 2 -


column ship_id heading 'Ship Id'
column ship_id format A30
column doc_num heading 'Way Bill'
column doc_num format  A30
column ordnum heading 'Order Number'
column ordnum format A20
column cponum heading 'Purchase Order'
column cponum format A20


select a.ship_id, a.doc_num, b.ordnum, ord.cponum
 from shipment a, shipment_line b, invdtl id, invsub iv, invlod il, ord, ord_line
 where a.ship_id=b.ship_id
 and b.ordnum = ord_line.ordnum
 and b.ordsln = ord_line.ordsln
 and b.ordlin = ord_line.ordlin
 and b.client_id = ord_line.client_id
 and ord_line.ordnum = ord.ordnum
 and ord.client_id ='----'
 and ord.cponum ='&1'
 and b.ship_line_id=id.ship_line_id
 and id.subnum=iv.subnum
and iv.lodnum = il.lodnum
union
select a.ship_id, a.doc_num, b.ordnum, ord.cponum
 from shipment@arch  a, shipment_line@arch  b, invdtl@arch  id, invsub@arch  iv, invlod@arch  il, ord@arch ord,
 ord_line@arch ord_line
 where a.ship_id=b.ship_id
 and b.ordnum = ord_line.ordnum
 and b.ordsln = ord_line.ordsln
 and b.ordlin = ord_line.ordlin
 and b.client_id = ord_line.client_id
 and ord_line.ordnum = ord.ordnum
 and ord.client_id ='----'
 and ord.cponum ='&1'
 and b.ship_line_id=id.ship_line_id
 and id.subnum=iv.subnum
and iv.lodnum = il.lodnum
/
