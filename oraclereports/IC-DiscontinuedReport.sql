/* #REPORTNAME=IC Discontinued Items Report */
/* #HELPTEXT= Requested by Inventory Control */


ttitle left print_time center 'IC Discontinued Items Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column vc_prdcod heading 'Prod Code'
column vc_prdcod format a10


SELECT
rtrim(prtmst.prtnum) prtnum ,
substr(prtdsc.lngdsc,1,30) lngdsc,
vc_prdcod
from prtmst, prtdsc
where
prtmst.prt_client_id = '----'
and prtmst.prtnum|| '|----' = prtdsc.colval
and prtdsc.colnam='prtnum|prt_client_id'
and prtdsc.locale_id ='US_ENGLISH'
and substr(vc_prdcod,2,1) ='X'
order by prtmst.prtnum, prtdsc.lngdsc
/
