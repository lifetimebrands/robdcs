/* #REPORTNAME=User Print Archived Consolidated Manifest  */                              
/* #VARNAM=doc_num , #REQFLG=Y */
/* #HELPTEXT= This report lists the consolidated archived  manifest by store */                 
/* #HELPTEXT= Requested by Shipping. */

ttitle left  print_time skip 1 -
left 'Consolidated Archived Manifest - MANIFEST # &1 ' - skip 1  -                           
right print_date skip 2  -

btitle  skip 1 left 'Page: ' format 999 sql.pno

column deptno heading 'DPT #'
column deptno format a3
column stonum heading 'STORE #'
column stonum format a7
column ordnum heading 'ORD#'
column ordnum format a12 
column name heading 'Customer - Store Location'
column name format a58
column totcas heading 'CTNS'
column totcas format 99999
column clcwgt heading 'WT'
column clcwgt format 99999
column cponum heading 'P.O. #'
column cponum format a10
column waybil heading 'MANIFEST #'
column waybil format a20


break on stonum skip 3
compute sum label 'TOTALS.....' of totcas on stonum
compute sum of clcwgt on stonum

select ordnum,
 stonum,
 name,
 cponum,
 deptno,
sum(totcas) totcas,
sum(clcwgt) clcwgt
from usr_manifest_archive_view
where   waybil ='&1'
group by name, stonum,ordnum, cponum, deptno
/
