
/* #REPORTNAME=IC Unassigned Footprint Report */
/* #HELPTEXT= Requested by Inventory Control */


ttitle left print_time center 'IC Unassgned Footprint Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column ftpcod 'Foot Print Code'
column ftpcod format a12

set linesize 200
set pagesize 2000

select prtnum, ftpcod
from prtmst 
where ftpcod ='UNASSIGNED' or ftpcod is null
order by prtnum 
/
