/* #REPORTNAME=User Archived Tracking Numbers by Date */                              
/* #VARNAM=shpdte , #REQFLG=Y */
/* #VARNAM=shpdte1 ,#REQFLG=Y */
/* #HELPTEXT= This report lists tracking numbers and */
/* #HELPTEXT= order numbers from archive */                 
/* #HELPTEXT= Requested by Shipping */

ttitle left  print_time skip 1 -
left 'Archived Tracking Numbers between &1 and &2 ' - skip 1  -                           
right print_date skip 2  -

btitle  skip 1 left 'Page: ' format 999 sql.pno

alter session set nls_date_format = 'dd-mon-yyyy';

column traknm heading 'Tracking #'
column traknm format a25
column ordnum heading 'Order '
column ordnum format a20
column shpdte heading 'Date'
column shpdte format a12


select distinct shipment_line.ordnum, manfst.traknm, manfst.shpdte
from manfst@arch manfst, shipment_line@arch shipment_line 
where manfst.shpdte between '&1' and '&2'
and manfst.ship_id=shipment_line.ship_id
/
