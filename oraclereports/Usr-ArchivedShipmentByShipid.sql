/* #REPORTNAME=User Archived Tracking Numbers by Sid  */                              
/* #VARNAM=ship_id , #REQFLG=Y */
/* #HELPTEXT= This report lists tracking numbers and ship ids from archive */                 
/* #HELPTEXT= Requested by Shipping */

ttitle left  print_time skip 1 -
left 'Archived Tracking Numbers by Sid &1 ' - skip 1  -                           
right print_date skip 2  -

btitle  skip 1 left 'Page: ' format 999 sql.pno

alter session set nls_date_format = 'dd-mon-yyyy';

column ship_id heading 'Ship Id #'
column ship_id format a15
column traknm heading 'Tracking #'
column traknm format a25
column shpdte heading 'Shipped Date'
column shpdte format a12
column weight heading 'Weight'
column weight format 99999

select manfst.ship_id, manfst.traknm, manfst.weight, manfst.shpdte
from manfst@arch manfst
where manfst.ship_id='&1'
order by manfst.ship_id, manfst.shpdte, manfst.traknm
/
