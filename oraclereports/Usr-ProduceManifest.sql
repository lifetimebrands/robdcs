/* #REPORTNAME=User Print Consolidated Manifest  */                              
/* #VARNAM=Trailer , #REQFLG=Y */
/* #HELPTEXT= This report lists the consolidated manifest by store */                 
/* #HELPTEXT= Requested by Shipping */

btitle  skip 1 left 'Page: ' format 999 sql.pno

column ordnum heading 'Vendor Order Number'
column ordnum format a19
column totcas heading 'CTNS'
column totcas format 99999
column clcwgt heading 'Weight'
column clcwgt format 99999.99
column cponum heading 'JCP Purchase Order Number'
column cponum format a26
column waybil heading 'Reference Identification'
column waybil format a24
column volume heading 'Volume'
column volume format 99999.99

select ordnum,
cponum, waybil,
sum(totcas) totcas,
sum(clcwgt) clcwgt,
sum(volume) volume
from usr_manifest_view
where  trlr_id  = '&1'
group by ordnum,cponum, waybil
/
