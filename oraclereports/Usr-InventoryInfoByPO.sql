/* #REPORTNAME=User Inventory Info by PO */
/* #HELPTEXT= Provides total ordered quantity */
/* #HELPTEXT= and avaiable invtory on hand*/
/* #HELPTEXT= Enter date in DD-MON-YYYY form*/

/* #VARNAM=add_date , #REQFLG=Y */
/* #VARNAM=btcust , #REQFLG=Y */

set linesize 200
set pagesize 200

column prtnum heading 'Part Number'
column prtnum format a20
column cponum heading 'PO Number'
column cponum format a20
column ordqty heading 'Ordered Qty.'
column ordqty format 999,999,999
column total heading 'Total On Hand'
column total format 999,999,999
column difference heading 'Total On Hand After'
column difference format 999,999,999

select a.prtnum, a.cponum, a.ordqty, nvl(sum((b.untqty+b.pndqty)-b.comqty),0) total,
(nvl(sum((b.untqty+b.pndqty)-b.comqty),0)-c.ordqty) difference
from
(select ord_line.prtnum, ord.cponum, sum(ord_line.ordqty) ordqty
from ord_line, ord
where ord_line.ordnum=ord.ordnum
and ord_line.client_id='----'
and trunc(ord.adddte)=to_date('&&1','DD-MON-YYYY')
and ord.btcust='&&2'
group by ord_line.prtnum, ord.cponum) a, invsum b, (select ord_line.prtnum, sum(ord_line.ordqty) ordqty
from ord_line, ord
where ord_line.ordnum=ord.ordnum
and ord_line.client_id='----'
and trunc(ord.adddte)=to_date('&&1','DD-MON-YYYY')
and ord.btcust='&&2'
group by ord_line.prtnum) c
where a.prtnum=c.prtnum
and a.prtnum=b.prtnum(+)
and b.invsts (+) ='A'
group by a.prtnum, a.cponum, a.ordqty, c.ordqty
order by a.cponum;
