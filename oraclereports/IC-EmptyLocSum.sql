
/* #REPORTNAME= IC-Empty Locations Summary Report */
/* #HELPTEXT= This report gives a summary of all locations in */
/* #HELPTEXT= Rack, Tower, GWall, ISTG and CFPP , that are */
/* #HELPTEXT= useable, empty, full, pending and available.  */
/* #HELPTEXT= Requested by Inventory Control             */

/* Author: Al Driver */
/* Report provides a summary of locations.  I used a */
/* package in order to obtain the counts for each area. */
/* This report is similar to the IC-Total Locations Summary Report. */
/* I used a union for the subtotals, grandtotal and */
/* different areas to have control over the order that */
/* they appear in the report. Using one script did */
/* not allow me to order the rows as desired */ 

ttitle left print_time -
center 'IC Empty Locations Summary Report  ' -   
right print_date skip 2


column area heading 'Area'
column area format  A5 
column total heading 'Total| Locations'
column total format A15
column useable heading 'Total | Useable'
column useable format A13 
column assigned heading 'Total | Assigned'
column assigned format A13
column perassign heading '%Assigned'
column perassign format 999.99
column full heading 'Total |  Full'
column full format A10
column perfull heading '%Full'
column perfull format 999.99
column empty heading 'Total | Empty'
column empty format A11
column perempty heading '%Empty'
column perempty format 999.99
column pending heading 'Total | Pending'
column pending format A13
column perpend heading '%Pending'
column perpend format 999.99
column available heading 'Total | Available'
column available format A15
column peravl heading '%Available'
column peravl format 999.99 
column aretyp heading 'Area Type'
column aretyp format A15
column rownumbr heading 'Row#'
column rownumbr format 999
column NULL heading '     '
column NULL format A5

set linesize 600
set pagesize 1000

alter session set nls_date_format ='DD-MON-YYYY';

break on area skip 1 


select rownum rownumbr,'RACK' area,arecod aretyp,to_char(UsrLocStatusSum.usrGetAll(arecod)) total ,NULL,
to_char(UsrLocStatusSum.usrGetTotal(arecod)) useable, NULL,to_char(UsrLocStatusSum.usrGetAssigned(arecod))  assigned,
to_char((UsrLocStatusSum.usrGetAssigned(arecod)/decode(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perassign,NULL,
to_char(UsrLocStatusSum.usrGetFull(arecod)) full,
to_char((UsrLocStatusSum.usrGetFull(arecod)/decode(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perfull,NULL,
to_char(UsrLocStatusSum.usrGetEmpty(arecod)) empty,
to_char((UsrLocStatusSum.usrGetEmpty(arecod)/decode(usrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perempty,NULL,
to_char(UsrLocStatusSum.usrGetPending(arecod)) pending,
to_char((UsrLocStatusSum.usrGetPending(arecod)/decode(usrLocStatusSum.usrGetEmpty(arecod)
,0,1,UsrLocStatusSum.usrGetEmpty(arecod)))) * 100  perpend,NULL,
to_char(UsrLocStatusSum.usrGetEmptyAvail(arecod))  available,
to_char(UsrLocStatusSum.usrGetEmptyAvail(arecod)/
UsrLocStatusSum.usrGetTotal(arecod))*100 peravl
from aremst
where arecod ||''in ('RACKHIGH','RACKCOMP','RACKMED','RACKNC','RACKSPCK','RACKSRES','RACKDEAD')
union
select 8 rownumbr,'RACK' area,'RACK-TOTAL' aretyp,to_char(UsrLocStatusSum.usrGetAllTotal('RACK')) total ,NULL,
to_char(UsrLocStatusSum.usrGetAreaTotal('RACK')) useable, NULL,to_char(UsrLocStatusSum.usrGetAssignTotal('RACK')) assigned,
to_char((UsrLocStatusSum.usrGetAssignTotal('RACK')/decode(usrLocStatusSum.usrGetAreaTotal('RACK')
,0,1,usrLocStatusSum.usrGetAreaTotal('RACK')))) * 100 perassign,NULL,
to_char(usrGetFullTotal('RACK')) full,
to_char((usrGetFullTotal('RACK')/decode(usrLocStatusSum.usrGetAreaTotal('RACK')
,0,1,usrLocStatusSum.usrGetAreaTotal('RACK')))) * 100 perfull,NULL,
to_char(usrGetEmptyTotal('RACK')) empty,
to_char((usrGetEmptyTotal('RACK')/decode(usrLocStatusSum.usrGetAreaTotal('RACK')
,0,1,usrLocStatusSum.usrGetAreaTotal('RACK')))) * 100 perempty,NULL,
to_char(usrGetPendingTotal('RACK')) pending,
to_char((usrGetPendingTotal('RACK')/decode(usrGetEmptyTotal('RACK')
,0,1,usrGetEmptyTotal('RACK')))) * 100  perpend,NULL,
to_char(UsrLocStatusSum.usrGetEmptyAvailTotal('RACK'))  available,
to_char(UsrLocStatusSum.usrGetEmptyAvailTotal('RACK')/
UsrLocStatusSum.usrGetAllTotal('RACK'))*100 peravl
from dual
union
SELECT ROWNUM + 8 rownumbr,'TOWER' area,arecod aretyp,TO_CHAR(UsrLocStatusSum.usrGetAll(arecod)) total ,NULL,
TO_CHAR(UsrLocStatusSum.usrGetTotal(arecod)) useable, NULL,TO_CHAR(UsrLocStatusSum.usrGetAssigned(arecod)) assigned,
TO_CHAR((UsrLocStatusSum.usrGetAssigned(arecod)/DECODE(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perassign,NULL,
TO_CHAR(UsrLocStatusSum.usrGetFull(arecod)) FULL,
TO_CHAR((UsrLocStatusSum.usrGetFull(arecod)/DECODE(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perfull,NULL,
TO_CHAR(UsrLocStatusSum.usrGetEmpty(arecod)) empty,
TO_CHAR((UsrLocStatusSum.usrGetEmpty(arecod)/DECODE(usrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perempty,NULL,
TO_CHAR(UsrLocStatusSum.usrGetPending(arecod)) pending,
TO_CHAR((UsrLocStatusSum.usrGetPending(arecod)/DECODE(usrLocStatusSum.usrGetEmpty(arecod)
,0,1,UsrLocStatusSum.usrGetEmpty(arecod)))) * 100  perpend,NULL,
TO_CHAR(UsrLocStatusSum.usrGetEmptyAvail(arecod))  available,
DECODE( UsrLocStatusSum.usrGetTotal(arecod), 0, 0,
TO_CHAR(UsrLocStatusSum.usrGetEmptyAvail(arecod)/
UsrLocStatusSum.usrGetTotal(arecod))*100) peravl
FROM AREMST
WHERE arecod ||''IN ('TOWERFCC','TOWERFCP','TOWERREP')
union
select 12 rownumbr,'TOWER' area,'TOWER-TOTAL' aretyp,to_char(UsrLocStatusSum.usrGetAllTotal('TOWER')) total ,NULL,
to_char(UsrLocStatusSum.usrGetAreaTotal('TOWER')) useable, NULL,to_char(UsrLocStatusSum.usrGetAssignTotal('TOWER')) assigned,
to_char((UsrLocStatusSum.usrGetAssignTotal('TOWER')/decode(usrLocStatusSum.usrGetAreaTotal('TOWER')
,0,1,usrLocStatusSum.usrGetAreaTotal('TOWER')))) * 100 perassign,NULL,
to_char(usrGetFullTotal('TOWER')) full,
to_char((usrGetFullTotal('TOWER')/decode(usrLocStatusSum.usrGetAreaTotal('TOWER')
,0,1,usrLocStatusSum.usrGetAreaTotal('TOWER')))) * 100 perfull,NULL,
to_char(usrGetEmptyTotal('TOWER')) empty,
to_char((usrGetEmptyTotal('TOWER')/decode(usrLocStatusSum.usrGetAreaTotal('TOWER')
,0,1,usrLocStatusSum.usrGetAreaTotal('TOWER')))) * 100 perempty,NULL,
to_char(usrGetPendingTotal('TOWER')) pending,
to_char((usrGetPendingTotal('TOWER')/decode(usrGetEmptyTotal('TOWER')
,0,1,usrGetEmptyTotal('TOWER')))) * 100  perpend,NULL,
to_char(UsrLocStatusSum.usrGetEmptyAvailTotal('TOWER'))  available,
to_char(UsrLocStatusSum.usrGetEmptyAvailTotal('TOWER')/
UsrLocStatusSum.usrGetAllTotal('TOWER'))*100 peravl
from dual
union
select rownum +12 rownumbr,arecod area,arecod aretyp,to_char(UsrLocStatusSum.usrGetAll(arecod)) total ,NULL,
to_char(UsrLocStatusSum.usrGetTotal(arecod)) useable,NULL, to_char(UsrLocStatusSum.usrGetAssigned(arecod)) assigned, 
to_char((UsrLocStatusSum.usrGetAssigned(arecod)/decode(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perassign,NULL,
to_char(UsrLocStatusSum.usrGetFull(arecod)) full,
to_char((UsrLocStatusSum.usrGetFull(arecod)/decode(UsrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perfull,NULL,
to_char(UsrLocStatusSum.usrGetEmpty(arecod)) empty,
to_char((UsrLocStatusSum.usrGetEmpty(arecod)/decode(usrLocStatusSum.usrGetTotal(arecod)
,0,1,usrLocStatusSum.usrGetTotal(arecod)))) * 100 perempty,NULL,
to_char(UsrLocStatusSum.usrGetPending(arecod)) pending,
to_char((UsrLocStatusSum.usrGetPending(arecod)/decode(usrLocStatusSum.usrGetEmpty(arecod)
,0,1,UsrLocStatusSum.usrGetEmpty(arecod)))) * 100  perpend,NULL,
to_char(UsrLocStatusSum.usrGetEmptyAvail(arecod))  available,
to_char(UsrLocStatusSum.usrGetEmptyAvail(arecod)/
UsrLocStatusSum.usrGetTotal(arecod))*100 peravl
from aremst
where arecod ||''in ('CFPP','GWALL','ISTG')
union
select 16 rownumbr,NULL area,'GRAND TOTAL' aretyp ,to_char(usrLocstatussum.usrGetGrandAllTotal()) total,NULL,
to_char(UsrLocStatusSum.usrGetGrandTotal()) total ,NULL, to_char(UsrLocStatusSum.usrGetAssignGrandTotal()) assigned, 
to_char((UsrLocStatusSum.usrGetAssignGrandTotal()/decode(UsrLocStatusSum.usrGetGrandTotal()
,0,1,usrLocStatusSum.usrGetGrandTotal()))) * 100 perassign,NULL,
to_char(UsrLocStatusSum.usrGetFullGrandTotal()) full,
to_char((UsrLocStatusSum.usrGetFullGrandTotal()/decode(usrLocStatusSum.usrGetGrandTotal()
,0,1,usrLocStatusSum.usrGetGrandTotal()))) * 100 perfull,NULL,
to_char(UsrLocStatusSum.usrGetEmptyGrandTotal()) empty,
to_char((UsrLocStatusSum.usrGetEmptyGrandTotal()/decode(usrLocStatusSum.usrGetGrandTotal()
,0,1,usrLocStatusSum.usrGetGrandTotal()))) * 100 perempty,NULL,
to_char(usrlocstatussum.usrGetGrandPendingTotal()) pending,
to_char((UsrLocStatusSum.usrGetGrandPendingTotal()/decode(usrLocStatusSum.usrGetEmptyGrandTotal()
,0,1,UsrLocStatusSum.usrGetEmptyGrandTotal()))) * 100  perpend,NULL,
to_char(UsrLocStatusSum.usrGetEmptyAvailGrandTotal())  available,
to_char(UsrLocStatusSum.usrGetEmptyAvailGrandTotal()/
UsrLocStatusSum.usrGetGrandAllTotal())*100 peravl
from dual
order by 1
/
