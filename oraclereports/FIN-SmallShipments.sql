/* #REPORTNAME=FIN - Small Shipments */
/* #HELPTEXT = Returns orders shipped less then $40 */
/* #HELPTEXT = in a given date range */
/* #VARNAM=Start_Date, #REQFLG=Y */
/* #VARNAM=End_Date, #REQFLG=Y */ 

column adrnam heading 'Customer'
column adrnam format a40
column dispatch_dte heading 'Dispatch Date'
column dispatch_dte format date
column ship_id heading 'Ship ID'
column ship_id format a10
column ordnum heading 'Order #'
column ordnum format a12
column cost heading 'Cost'
column cost format 999,999.99
column cust_cost heading 'Customer Cost'
column cust_cost format 999,999.99

alter session set nls_date_format = 'DD-MON-YYYY';
set pagesize 2000
set linesize 200

select a.adrnam, h.dispatch_dte, d.ship_id, b.ordnum, sum(e.shpqty*i.untcst) cost, 
sum(e.shpqty*c.vc_cust_untcst) cust_cost
from adrmst a, ord b, ord_line c, shipment d, shipment_line e, stop f, car_move g,
trlr h, prtmst i
where b.btcust=a.host_ext_id
and b.ordnum=c.ordnum
and c.client_id='----'
and c.ordnum=e.ordnum
and c.ordlin=e.ordlin
and c.ordsln=e.ordsln
and c.prtnum=i.prtnum
and e.ship_id=d.ship_id
and d.shpsts='C'
and d.stop_id=f.stop_id
and f.car_move_id=g.car_move_id
and g.trlr_id=h.trlr_id
and h.trlr_stat='D'
and h.dispatch_dte between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')+.99
group by a.adrnam, h.dispatch_dte, d.ship_id, b.ordnum
having sum(e.shpqty*i.untcst)<40 and sum(e.shpqty*i.untcst)>0;
