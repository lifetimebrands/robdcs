/* #REPORTNAME=IC-Production Cartons Out Report */
/* #HELPTEXT= This report provides details on cartons coming */
/* #HELPTEXT= out of production. User must select between the  */
/* #HELPTEXT= action codes of FG , SCRAP and RETURN.  */
/* #HELPTEXT= Date range must be input in the format DD-MON-YYYY */

/* #VARNAM=action , #REQFLG=Y */
/* #VARNAM=begin_dte , #REQFLG=Y */
/* #VARNAM=end_dte , #REQFLG=Y */
 
ttitle left print_time -
		center 'IC - Production Cartons Out Report' -
		right print_date skip - 
                center '&&2 '  to  ' &&3' skip 2 

btitle skip1 center 'Page:' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a30
column action heading 'Action'
column action format a10
column qty heading 'Units'
column qty format 999999 
column untcas heading 'Item Casepack'
column untcas format 99999999 
column numctn heading '# Ctns'
column numctn format 999999.99

set linesize 100
set pagesize 5000 

alter session set nls_date_format ='DD-MON-YYYY';

select prtnum, '&&1' action, sum(prdqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,prdqty,wkohdr.prtnum,prtmst.untcas,prdqty/decode(prtmst.untcas,0,1,prtmst.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr,wkodtl,prtmst
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkohdr.prtnum = prtmst.prtnum
    and wkosts = 'C'  
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'FG')
  group by prtnum, untcas
union
select prtnum, '&&1' action, sum(prdqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,prdqty,wkohdr.prtnum,prtmst.untcas,prdqty/decode(prtmst.untcas,0,1,prtmst.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr@arch,wkodtl@arch,prtmst
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkohdr.prtnum = prtmst.prtnum
    and wkosts = 'C'
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'FG')
  group by prtnum, untcas
union
select prtnum, '&&1' action, sum(rpt_scpqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,rpt_scpqty,wkodtl.prtnum,wkodtl.untcas,rpt_scpqty/decode(wkodtl.untcas,0,1,wkodtl.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr,wkodtl
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C' and rpt_scpqty <> 0
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'SCRAP')
  group by prtnum, untcas
union
select prtnum, '&&1' action, sum(rpt_scpqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,rpt_scpqty,wkodtl.prtnum,wkodtl.untcas,rpt_scpqty/decode(wkodtl.untcas,0,1,wkodtl.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr@arch,wkodtl@arch
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C' and rpt_scpqty <> 0
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'SCRAP')
  group by prtnum, untcas
union
select prtnum, '&&1' action, sum(rpt_rtsqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,rpt_rtsqty,wkodtl.prtnum,wkodtl.untcas,rpt_rtsqty/decode(wkodtl.untcas,0,1,wkodtl.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr,wkodtl
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C' and rpt_rtsqty <> 0
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'RETURN')
  group by prtnum, untcas
union
select prtnum, '&&1' action, sum(rpt_rtsqty) qty, untcas, sum(ctn) numctn from
    (select distinct wkohdr.wkonum,rpt_rtsqty,wkodtl.prtnum,wkodtl.untcas,rpt_rtsqty/decode(wkodtl.untcas,0,1,wkodtl.untcas) ctn,
    trunc(clsdte) clsdte
    from wkohdr@arch,wkodtl@arch
    where wkohdr.wkonum = wkodtl.wkonum
    and wkohdr.wkorev = wkodtl.wkorev
    and wkohdr.client_id = wkodtl.client_id
    and wkosts = 'C' and rpt_rtsqty <> 0
   and trunc(clsdte) between '&&2' and '&&3'
   and '&&1' = 'RETURN')
  group by prtnum, untcas
/
