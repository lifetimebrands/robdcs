/* #REPORTNAME=User Policy Replenishment matching Report */
/* #HELPTEXT= This report does the Policy Replen Matching. */
/* #HELPTEXT = Requested by Controller */

ttitle left  print_time -
       center 'CC Policy Replenishment matching Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

set linesize 200
alter session set nls_date_format ='dd-mon-yyyy';

column polval     heading 'Policy Item'
column polval     format a20
column rtstr2     heading 'Policy Location'
column rtstr2     format a20
column rplitem    heading 'Replen Item'
column rplitem    format a20
column rplloc     heading 'Replen Location'
column rplloc     format a20


select substr(polval,6) polval,
substr(rtstr2,1,20) rtstr2, 
a.prtnum rplitem,
a.stoloc rplloc
from poldat, rplcfg a, locmst b
where polcod like 'STORE-ASG%'
and rtstr1 = a.stoloc
and a.stoloc = b.stoloc
and substr(polval,6) != a.prtnum
/

