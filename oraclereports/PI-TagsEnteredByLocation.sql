
set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/* Requested by Senior Management - Jan 03, 2001 */ 

/* #REPORTNAME=PI Tags Entered by Location Report */
/* #VARNAM=stoloc , #REQFLG=Y */
/* #HELPTEXT= This report list entries by location for */
/* #HELPTEXT= Physical Inventory */

ttitle left  print_time Center 'PI Tags Entered by Location Report' skip 2 -
        left print_date skip 1 -
        left 'Location:'  ' &2 ' skip 2 -
btitle  left 'Page: ' format 999 sql.pno

column cntbat heading 'Tag.'
column cntbat format a10
column usr_cnt_version heading 'Rev.#'
column usr_cnt_version format a5
column prtnum heading 'Item'
column prtnum format a11
column lngdsc heading 'Description'
column lngdsc format a35
column cntqty heading 'Units|Qty' 
column cntqty format 999999999
column pend heading 'Status'
column pend format a1

select rtrim(cnthst.prtnum) prtnum, rtrim(prtdsc.lngdsc) lngdsc,
      cnthst.cntqty 
from cnthst,prtdsc 
where 
cnthst.stoloc = '&2'
and cnthst.prtnum||'|----' = prtdsc.colval
and prtdsc.colnam = 'prtnum|prt_client_id'
and dscmst.lngcod='US_ENGLISH'
union
select  rtrim(cntwrk.prtnum) prtnum, 'Pending....' lngdsc,
cntwrk.cntqty
from cntwrk
where cntwrk.stoloc = '&2'



/
