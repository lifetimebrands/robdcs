column prtnum heading 'Part Number'
column prtnum format a20
column lotnum heading 'Lot Number'
column lotnum format a10
column untqty heading 'Qty.'
column untqty format 999,999,999

set pagesize 1000

select a.prtnum, a.lotnum, sum(a.untqty) untqty
from invdtl a, invsub b, invlod c, locmst d, aremst e
where a.prt_client_id='----'
and a.lotnum!='NOLOT'
and a.subnum=b.subnum
and b.lodnum=c.lodnum
and c.stoloc=d.stoloc
and d.arecod=e.arecod
and e.fwiflg=1
group by a.prtnum, a.lotnum
order by a.prtnum;
