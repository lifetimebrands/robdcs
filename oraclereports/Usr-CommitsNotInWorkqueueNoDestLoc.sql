/* #REPORTNAME=User Commits Not in Workqueue - No Destination Location */
/* #HELPTEXT= This report lists all */
/* #HELPTEXT= commits without destination locations */
/* #HELPTEXT= Requested by Senior Management */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 250


ttitle left print_time -
center 'User Commits Not In Workqueue - No Destination Location ' -
right print_date skip 2 -


alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';


column schbat heading 'Batch'
column schbat format A30
column pricod heading 'Code'
column pricod format  A20
column srcare heading 'Source Area'
column srcare format a15
column adddte heading 'Added'
column adddte format A23
column reldte heading 'Released   '
column reldte format A23
column cmpdte heading 'Completed     '
column cmpdte format A12
column wrkref heading 'Work Ref'
column wrkref format a12
column pcksts heading 'Pick Status'
column pcksts format a12
column appqty heading 'Applied'
column appqty format 999999
column pckqty heading 'Picked'
column pckqty format 999999
column prtnum heading 'Item'
column prtnum format a12
column ordnum heading 'Order'
column ordnum format a12
column ctnnum heading 'Carton'
column ctnnum format a12
column wrktyp heading 'Type'
column wrktyp format a12
column srcloc heading 'Source'
column srcloc format a12
column dstloc heading 'Dest. '
column dstloc format a12

select pckbat.schbat, pckbat.pricod, pckwrk.adddte,
        pckbat.reldte, pckbat.cmpdte, pckwrk.wrkref,
        pckwrk.pcksts, pckwrk.appqty,
        pckwrk.pckqty, pckwrk.prtnum,
        pckwrk.ordnum, 
        pckwrk.ctnnum, pckwrk.wrktyp, pckwrk.srcare, pckwrk.srcloc,
        pckwrk.dstloc
   from pckwrk, pckbat
  where pckwrk.schbat = pckbat.schbat
    and pckwrk.appqty < pckwrk.pckqty
    and pckwrk.wrktyp <> 'K'
    and pckbat.cmpdte is null
    and pckwrk.ship_id is null
    and pckwrk.dstloc is  null
order by pckwrk.adddte
/
