/* #REPORTNAME=User Tracking Number Report */
/* #HELPTEXT= This report provides the requested */
/* #HELPTEXT= information on a tracking number.*/
/* #HELPTEXT= It requires a tracking number to be input */
/* #HELPTEXT= Requested by Westbury */

/* #VARNAM=tracking# , #REQFLG=Y */

ttitle left  print_time -
       center 'User Tracking Number Report: ' -
       right print_date skip 2 -

btitle skip 1 center 'Page: ' format 999 sql.pno

column adrnam heading 'Customer Name'
column adrnam format A30
column prtnum heading 'Item #'
column prtnum format A30
column traknum heading 'Tracking #'
column traknum format A30
column weight heading 'Weight'
column weight format 999999
column shpdte heading 'Ship Date'
column shpdte format A11

alter session set nls_date_format = 'DD-MON-YYYY';

set linesize 150 



select distinct substr(a.adrnam,1,30) adrnam, sd.ordnum, i.prtnum prtnum, i.traknm traknum, i.weight ,trunc(tr.dispatch_dte) shpdte            
 from   adrmst a, manfst i,shipment_line sd,shipment sh, stop,
        car_move cr, trlr tr
        where a.adr_id                = sh.rt_adr_id
        AND i.ship_id                 = sd.ship_id
        AND i.carcod                  = cr.carcod  
        AND sd.ship_id                = sh.ship_id  
        AND sh.stop_id                = stop.stop_id  
        AND stop.car_move_id          = cr.car_move_id  
        AND cr.trlr_id                = tr.trlr_id  
        AND i.traknm = '&&1'
union
select distinct substr(a.adrnam,1,30) adrnam, sd.ordnum,i.prtnum prtnum, i.traknm traknum, i.weight ,trunc(tr.dispatch_dte) shpdte
 from   adrmst@arch a, manfst@arch i,shipment_line@arch sd,shipment@arch sh, stop@arch,
        car_move@arch cr, trlr@arch tr
        where a.adr_id                = sh.rt_adr_id
        AND i.ship_id                 = sd.ship_id
        AND i.carcod                  = cr.carcod
        AND sd.ship_id                = sh.ship_id
        AND sh.stop_id                = stop.stop_id
        AND stop.car_move_id          = cr.car_move_id
        AND cr.trlr_id                = tr.trlr_id
        AND i.traknm = '&&1'
/
