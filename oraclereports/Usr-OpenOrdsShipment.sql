truncate table usr_openords_shipment;

insert into usr_openords_shipment(cponum,ordnum,status,btcust,ship_to_name,early_shpdte,
adddte,cancel_date,unsamt,extdte,ptksts,cstcat,shpcat,vc_numbch,ship_id,carcod,schbat)
SELECT
        distinct rtrim(ord.cponum) cponum, 
        rtrim(ord.ordnum) ordnum,     
        decode(rtrim(dscmst.lngdsc),'Staged',decode(vc_print_dte,NULL,rtrim(dscmst.lngdsc),'Printed'),rtrim(dscmst.lngdsc)) status,
        rtrim(ord.btcust) btcust,
        rtrim( substr(lookup1.adrnam,1,30))  Ship_To_Name, 
        rtrim(to_char(ord_line.early_shpdte,'MM/DD/YYYY')) early_shpdte,
        rtrim(ord.adddte) adddte,       
        rtrim(to_char(ord_line.late_shpdte, 'MM/DD/YYYY')) Cancel_Date,
        decode(shipment_line.linsts, 'I', sum(ord_line.vc_cust_untcst * 
       (shipment_line.stgqty+shipment_line.inpqty+shipment_line.pckqty)),
        sum(ord_line.vc_cust_untcst*shipment_line.pckqty)) unsamt,
        rtrim(to_char(ord_line.vc_extdte,'MM/DD/YYYY')) vc_extdte,  decode(cstmst.vc_ptkstk, 0, 'N', 'Y') ptksts,
                        decode(rtrim(ord.btcust),usrGetOpenOrderCustomer(ord.btcust),'FARB',' ') cstcat,
                        decode(sign(trunc(ord_line.early_shpdte)-trunc(sysdate)),
                        -1,'Before Today',
                         decode(to_char(ord_line.early_shpdte,'YYYYMM'),
                         to_char(sysdate,'YYYYMM'),'This Month',
                         'Next Month +')) shpcat,
       ord.vc_numbch, shipment.ship_id, shipment.carcod, shipment_line.schbat
       FROM pckbat, stop, dscmst, adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, 
           shipment_line, shipment, ord_line, ord
       WHERE ord.st_adr_id              = lookup1.adr_id
         AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
        and ord.stcust = cstmst.cstnum (+)
        and ord.client_id = cstmst.client_id (+)
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.schbat        = pckbat.schbat (+)
        AND shipment_line.ship_id       = shipment.ship_id
        AND shipment.stop_id            = stop.stop_id (+)
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled' 
        GROUP BY ord.cponum, ord.ordnum, shipment.ship_id, shipment.carcod, shipment_line.schbat, 
        dscmst.lngdsc,stop.vc_print_dte,   ord.btcust, lookup1.adrnam,
        ord_line.early_shpdte, ord.adddte,
        ord_line.late_shpdte, cstmst.vc_ptkstk, ord_line.vc_extdte, 
        shipment_line.linsts, ord.vc_numbch
union  -- third script grabs orders closed by not dispatched
select cponum,ordnum,status,btcust,adrnam Ship_to_Name,early_shpdte,adddte,Cancel_Date,
sum(vc_cust_untcst*shpqty) unsamt,vc_extdte,ptksts,cstcat,shpcat,vc_numbch, ship_id, carcod, schbat from
(select distinct rtrim(ord.cponum) cponum, rtrim(ord.ordnum) ordnum,  
'Staged-CND' status, rtrim(ord.btcust) btcust,
rtrim(substr(adrnam,1,30))  adrnam,
rtrim(to_char(ord_line.early_shpdte,'MM/DD/YYYY')) early_shpdte,
rtrim(ord.adddte) adddte,
rtrim(to_char(ord_line.late_shpdte,'MM/DD/YYYY')) Cancel_Date,
ord_line.vc_cust_untcst , shipment_line.shpqty, ord_line.prtnum,ord_line.ordlin,
rtrim(to_char(ord_line.vc_extdte,'MM/DD/YYYY')) vc_extdte,
decode(cstmst.vc_ptkstk, 0, 'N', 'Y') ptksts,
decode(rtrim(ord.btcust),usrGetOpenOrderCustomer(ord.btcust),'FARB',' ') cstcat,
decode(sign(trunc(ord_line.early_shpdte)-trunc(sysdate)),
                          -1,'Before Today',
                          decode(to_char(ord_line.early_shpdte,'YYYYMM'),
                          to_char(sysdate,'YYYYMM'),'This Month',
                          'Next Month +')) shpcat,ord.vc_numbch, shipment.ship_id, shipment.carcod, shipment_line.schbat
from adrmst, cstmst, ord, ord_line, shipment_line, pckmov, pckwrk, shipment, stop,
car_move, trlr
where ord.stcust = cstmst.cstnum(+)
and ord.client_id = cstmst.client_id(+)
and ord.st_adr_id = adrmst.adr_id
and ord.ordnum = ord_line.ordnum
and ord_line.ordnum = shipment_line.ordnum
and ord_line.ordlin = shipment_line.ordlin
and ord_line.ordsln = shipment_line.ordsln
and ord_line.client_id = shipment_line.client_id
and shipment_line.ship_id = shipment.ship_id
and shipment.stop_id  = stop.stop_id
and stop.car_move_id  = car_move.car_move_id
and car_move.trlr_id
=trlr.trlr_id     and trlr.trlr_stat   in  ('O','L','H','C','P')
and shipment.loddte is not null
and shipment.ship_id  =
pckwrk.ship_id
and ord.ordnum = pckwrk.ordnum
and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
= pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY'))
group by cponum,ordnum,status,btcust,adrnam,
early_shpdte,adddte,Cancel_Date,vc_extdte,
ptksts,cstcat,shpcat,vc_numbch, ship_id, carcod, schbat;

commit;
