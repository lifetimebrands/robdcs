/* #REPORTNAME=ENG Number of Cartons Processed By Conveyor */
/* #VARNAM=trndte , #REQFLG=Y */

/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding all RF activity by user */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'ENG Number of Cartons Processed By Conveyor ' -
right print_date skip 2 -


column devcod heading 'Device Code'
column devcod format A30
column usr_id heading 'User'
column usr_id format  A20
column vehtyp heading 'Vehicle Type'
column vehtyp format A20
column rftmod heading 'RFT   '
column rftmod format A30
column trndte heading 'Date     '
column trndte format A24
column trnqty heading 'Qty'
column trnqty format 9999999999
column oprcod heading 'Opr Code'
column oprcod format a9
column ref heading 'Movement Ref.'
column ref format 9999999999

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';

select
   dlytrn.usr_id,
  count(distinct(dlytrn.movref)) ref
       FROM dlytrn, rftmst
          WHERE trunc(dlytrn.trndte) = '&1'
           AND  dlytrn.devcod              = rftmst.devcod
           AND  dlytrn.frstol              = 'CONVEYOR'
   group by dlytrn.usr_id
/
