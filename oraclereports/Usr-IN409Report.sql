/* #REPORTNAME=IN409 */
/* #HELPTEXT= Requested by Senior Management */
/* #VARNAM=bomflg , #REQFLG=N */
/* #VARNAM=vc_itmtyp , #REQFLG=N */
/* #VARNAM=vc_itmgrp , #REQFLG=N */
/* #VARNAM=vc_itmcls , #REQFLG=N */

ttitle left  print_time -
       center 'IN409 Report' -
       right print_date skip 2 -
       left 'TopLvl Only Flag: ' h_bomflg skip 1 -

btitle center 'Page: ' sql.pno

column parm_bomflg new_value h_bomflg noprint

column prtnum heading 'Item #'
column prtnum format  a12 trunc
column lngdsc heading 'Description'
column lngdsc format  a20 trunc
column minqty heading 'Min'
column minqty format  99999
column onhqty heading 'OnQty'
column onhqty format  99999
column pndord heading 'Total'
column vc_prdcod heading 'Class'
column vc_prdcod format a6 trunc
column vc_itmcls heading 'BOM'
column vc_itmcls format a6 trunc
column vc_itmtyp heading 'Type'
column vc_itmtyp format a6 trunc
column vc_itmgrp heading 'Group'
column vc_itmgrp format a10
column untcst heading 'Cost'
column untcst format 999.9999
column estsp heading 'Est Sp.'
column estsp format 999.9999
column pndord format  99999
column pstdue heading 'PstDue' just right
column curwk1 heading 'Cur-1Wk' just right
column curwk2 heading 'Cur-2Wk' just right
column curwk3 heading 'Cur-3Wk' just right
column curwk4 heading 'Cur-4Wk' just right
column f_3060 heading '30-60'  just right
column f_ov60 heading '60-FUT' just right
column avlqty heading 'Avail'
column avlqty format 99999
column short heading 'Shortage'
column short format 999999.999

set linesize 600

select 
       prtnum,
       lngdsc,
       decode('&1',null,'0','1') parm_bomflg,
       decode('&2',null,vc_itmtyp,upper('&2')) vc_itmtyp,
       decode('&3',null,vc_itmgrp,upper('&3')) vc_itmgrp,
       decode('&4',null,vc_itmcls,upper('&4')) vc_itmcls,       
       vc_prdcod,
       untcst,
       estsp,
       minqty,
       onhandqty onhqty,
       sum(totpndord) pndord,
       to_char(sum(pastdue),'99990')||decode(sign(sum(pastdue - onhandqty)),1,'*',' ') pstdue,
       to_char(sum(cur_week1),'99990')||decode(sign(sum(pastdue + cur_week1 - onhandqty)),1,'*',' ') curwk1,
       to_char(sum(cur_week2),'99990')||decode(sign(sum(pastdue +
                                               cur_week1 +
                                               cur_week2 -onhandqty)),1,'*',' ') curwk2,
       to_char(sum(cur_week3),'99990')||decode(sign(sum(pastdue +
                                               cur_week1 +
                                               cur_week2 +
                                               cur_week3 - onhandqty)),1,'*',' ') curwk3,
       to_char(sum(cur_week4),'99990')||decode(sign(sum(pastdue +
                                               cur_week1 +
                                               cur_week2 +
                                               cur_week3 +
                                               cur_week4 - onhandqty)),1,'*',' ') curwk4,
       to_char(sum(fut_30_60),'99990')||decode(sign(sum(totpndord - fut_over_60 - onhandqty)),1,'*',' ') f_3060,
       to_char(sum(fut_over_60),'99990')||decode(sign(sum(totpndord - onhandqty)),1,'*',' ') f_ov60,
      (onhandqty - sum(totpndord)) avlqty, ((onhandqty - sum(totpndord)) * (1.9 * untcst))  short
  from prtdsc,
       (select prtmst.prtnum,
               vc_itmcls,
               vc_itmtyp,
               vc_prdcod,
               untcst, (1.9 * untcst) estsp,
               vc_mfgcen,
               vc_itmgrp,
               nvl(reopnt,0) minqty,
               usrIN400.OnHandQtyAll(prtmst.prtnum) onhandqty,
               nvl(sum(ord_line.pckqty),0) totpndord,
               sum(decode(sign(ord_line.early_shpdte - sysdate), -1, ord_line.pckqty, 0)) pastdue,
               sum(decode(sign(ord_line.early_shpdte - sysdate), -1, 0,
		          decode(sign(ord_line.early_shpdte - (sysdate + 7)), -1, ord_line.pckqty, 0))) cur_week1,
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 7)), -1, 0,
		          decode(sign(ord_line.early_shpdte - (sysdate + 14)), -1, ord_line.pckqty, 0))) cur_week2,
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 14)), -1, 0,
		          decode(sign(ord_line.early_shpdte - (sysdate + 21)), -1, ord_line.pckqty, 0))) cur_week3,
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 21)), -1, 0,
		          decode(sign(ord_line.early_shpdte - (sysdate + 28)), -1, ord_line.pckqty, 0))) cur_week4,
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 28)), -1, 0,
		          decode(sign(ord_line.early_shpdte - (sysdate + 60)), -1, ord_line.pckqty, 0))) fut_30_60,
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 60)), -1, 0, ord_line.pckqty)) fut_over_60,
               decode(bomhdr.prtnum,null,'0','1') pcfflg
               from bomhdr, prtmst, ord_line, ord
                    where bomhdr.bomnum(+) = prtmst.prtnum
                    and bomhdr.client_id(+) = prtmst.prt_client_id
                    and prtmst.prtnum = ord_line.prtnum
                    and prtmst.prt_client_id = ord_line.prt_client_id
                    and ord_line.client_id=ord.client_id
                    and ord.ordnum = ord_line.ordnum
                    and ord_line.client_id = '----'
                    and prtmst.prt_client_id = '----'
                    and ord.client_id = '----'
                    and not exists(select 'x' from shipment_line 
                        where ord_line.client_id=shipment_line.client_id
                        and ord_line.ordnum=shipment_line.ordnum
                        and ord_line.ordsln=shipment_line.ordsln
                        and ord_line.ordlin=shipment_line.ordlin)
                        and ('&1' is null or decode(bomhdr.prtnum,null,'0','1') = upper('&1'))
                        and ('&2' is null or rtrim(vc_itmtyp) = upper('&2'))
                        and ('&3' is null or rtrim(vc_itmgrp) = upper('&3'))
                        and ('&4' is null or rtrim(vc_itmcls) = upper('&4'))
 group
            by prtmst.prtnum,
               vc_itmcls,
               vc_itmtyp,
                vc_prdcod,
               untcst,
               vc_mfgcen,
               vc_itmgrp,
               reopnt,
               decode(bomhdr.prtnum,null,'0','1')
union all
   select prtmst.prtnum,
               vc_itmcls,
               vc_itmtyp,
                vc_prdcod,
               untcst, (1.9 * untcst) estsp,
               vc_mfgcen,
               vc_itmgrp,
               nvl(reopnt,0) minqty,
               usrIN400.OnHandQtyAll(prtmst.prtnum) onhandqty,
               nvl(sum(shipment_line.pckqty),0) totpndord,
               sum(decode(sign(ord_line.early_shpdte - sysdate), -1, shipment_line.pckqty, 0)) pastdue,                            
               sum(decode(sign(ord_line.early_shpdte - sysdate), -1, 0,
                          decode(sign(ord_line.early_shpdte - (sysdate + 7)), -1, shipment_line.pckqty, 0))) cur_week1,             
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 7)), -1, 0,
                          decode(sign(ord_line.early_shpdte - (sysdate + 14)), -1, shipment_line.pckqty, 0))) cur_week2,            
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 14)), -1, 0,
                          decode(sign(ord_line.early_shpdte - (sysdate + 21)), -1, shipment_line.pckqty, 0))) cur_week3,           
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 21)), -1, 0,
                          decode(sign(ord_line.early_shpdte - (sysdate + 28)), -1, shipment_line.pckqty, 0))) cur_week4,           
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 28)), -1, 0,
                          decode(sign(ord_line.early_shpdte - (sysdate + 60)), -1, shipment_line.pckqty, 0))) fut_30_60,            
               sum(decode(sign(ord_line.early_shpdte - (sysdate + 60)), -1, 0, shipment_line.pckqty)) fut_over_60,  
    decode(bomhdr.prtnum,null,'0','1') pcfflg
               from bomhdr, shipment_line, prtmst, ord_line
              where bomhdr.bomnum(+) = prtmst.prtnum
    and ord_line.client_id = shipment_line.client_id
              and ord_line.ordnum    = shipment_line.ordnum
              and ord_line.ordlin    = shipment_line.ordlin
              and ord_line.ordsln    = shipment_line.ordsln
              and bomhdr.client_id(+) = prtmst.prt_client_id
              and ord_line.prtnum(+) = prtmst.prtnum
              and ord_line.prt_client_id = '----'
              and ord_line.client_id ='----'
              and prtmst.prt_client_id = '----'
              and ord_line.prt_client_id (+) = prtmst.prt_client_id
              and shipment_line.linsts = 'P'
              and shipment_line.shpqty = 0
  and ('&1' is null or decode(bomhdr.prtnum,null,'0','1') = upper('&1'))
              and ('&2' is null or rtrim(vc_itmtyp) = upper('&2'))
              and ('&3' is null or rtrim(vc_itmgrp) = upper('&3'))
              and ('&4' is null or rtrim(vc_itmcls) = upper('&4'))
         group
            by prtmst.prtnum,
               vc_itmcls,
               vc_itmtyp,
                vc_prdcod,
               untcst,
               vc_mfgcen,
               vc_itmgrp,
               reopnt,
               decode(bomhdr.prtnum,null,'0','1')) prtlst
where
   colnam = 'prtnum|prt_client_id'
   and colval = prtnum||'|----'
   and locale_id ='US_ENGLISH'
   and prtlst.minqty > (prtlst.onhandqty - prtlst.totpndord)
group by prtnum, lngdsc, vc_prdcod, vc_itmtyp, vc_itmgrp, vc_itmcls, untcst, estsp, minqty, onhandqty
/
