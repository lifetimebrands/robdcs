/* #REPORTNAME=UPS/RPS Staged Pallets In Shipping Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, pallet id, ship to customer, */
/* #HELPTEXT= DC Customer and number of cartons for every pallet */
/* #HELPTEXT= that is staged. Requested by Shipping. */ 
ttitle left '&1 ' print_time -
		center 'Ups Rps Staged Pallets In Shipping Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column stgdte heading 'StgDte'
column schdte format a6
column pallet heading 'Pallet'
column pallet format a25
column cponum heading 'PO #'
column cponum format a20
column status heading 'Status'
column status format a10
column stgdte heading 'StgDte'
column stgdte format a6
column stoloc heading 'Location'
column stoloc format a15
column arecod heading 'Area'
column arecod format a4
column NUMCTN heading '# Cartons'
column NUMCTN format 999999
column ordnum heading 'Order Number'
column ordnum format a14
column shipid heading 'Ship ID'
column shipid format a14
column ffname heading 'DC Name'
column ffname format a30
column stname heading 'Ship To Name'
column stname format a30
column carcod heading 'Carrier'
column carcod format a15
column entdte heading 'ShpDte'
column entdte format a6
column cpodte heading 'CnlDte'
column cpodte format a6

set linesize 500
set pagesize 10000


select to_char(sh.stgdte,'YYMMDD')   stgdte,
       rtrim(invlod.lodnum)          pallet,
       rtrim(invlod.stoloc)          stoloc,
       lm.arecod                     arecod,
       rtrim(dscmst.lngdsc)          status,
       rtrim(sh.ship_id)              shipid,
       rtrim(sd.ordnum)              ordnum,
       rtrim(ord.cponum)              cponum,
  to_char(sh.stgdte,'YYMMDD')         stgdte,
       rtrim(invlod.lodnum)          pallet,
       rtrim(invlod.stoloc)          stoloc,
       lm.arecod                     arecod,
       rtrim(dscmst.lngdsc)          status,
       rtrim(sh.ship_id)              shipid,
       rtrim(sd.ordnum)              ordnum,
       rtrim(ord.cponum)              cponum,
       substr(lookup1.adrnam, 1, 30)      ffname,
       count(distinct invsub.subnum) NUMCTN,
      rtrim(sh.carcod)              carcod,
       to_char(sh.entdte, 'YYMMDD')  entdte,
       to_char(ord.cpodte, 'YYMMDD')  cpodte
  from dscmst,
       aremst am,
       locmst lm,
       invlod,
       invsub,
       invdtl,
       shipment_line sd,
       shipment sh,
       ord,
       adrmst lookup1,
       ord_line
 WHERE invlod.stoloc = lm.stoloc
         AND lm.arecod                   = am.arecod
         AND am.stgflg                   = 1
         AND invlod.lodnum               = invsub.lodnum
         AND invsub.subnum               = invdtl.subnum
         AND invdtl.ship_line_id         = sd.ship_line_id
         AND ord.st_adr_id               = lookup1.adr_id
         AND ord.client_id               = '----'
         AND ord.ordnum                  = ord_line.ordnum
     AND ord.client_id               = ord_line.client_id
         AND ord_line.client_id          = sd.client_id
         AND ord_line.ordnum             = sd.ordnum
         AND ord_line.ordlin             = sd.ordlin
         AND ord_line.ordsln             = sd.ordsln
         AND sd.ship_id       = sh.ship_id
         AND dscmst.colnam               = 'shpsts'
         AND dscmst.locale_id            = 'US_ENGLISH'
         AND sh.shpsts                   = dscmst.colval
         AND sh.shpsts                   != 'C'
         AND sh.carcod in ('UPSS','UPSG','UPSCC','RPSS')
group by
       to_char(sh.stgdte,'YYMMDD'),
       invlod.lodnum,
       ord.cponum,
       dscmst.lngdsc,
       lookup1.adrnam,
       invlod.stoloc,
       lm.arecod,
       sd.ordnum,
       sh.ship_id,
       sh.carcod,
       sh.shpsts,
       to_char(sh.entdte,'YYMMDD'),to_char(ord.cpodte, 'YYMMDD')
order by
       to_char(sh.stgdte,'YYMMDD'),
       rtrim(sd.ordnum),
       rtrim(invlod.lodnum)
/
