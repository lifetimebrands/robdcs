/* #REPORTNAME=User Parts Available On Hand Report */
/* #HELPTEXT= This report generates a list of all items */
/* #HELPTEXT= that have an on hand qty and a lot code.*/
/* #HELPTEXT= Requested by Senior Management */

/* Author: Al Driver */
/* Report uses a view to get the proper counts for each item. */
/* Without the use of a view, the counts were off due to a duplication */
/* of lines in the summary table. Some details found in the view */
/* were obtained from the Usr-InventoryByLotNum report. */ 

 ttitle left  print_time center 'User Parts Available On Hand Report ' -
    right print_date skip 2

column prtnum heading 'Item'
column prtnum format a15
column lotnum heading 'Lotcode'
column lotnum format a20
column onhandqty heading 'On Hand Qty'
column onhandqty format 999999
column invsts heading 'Status'
column invsts format a30 
column untcst heading 'Unit Cost'
column untcst format 999999.99

set linesize 200
set pagesize 200

SELECT prtnum, lotnum ,nvl(sum(untqty-comqty),0) onhandqty,untcst,invsts
      from usr_prtnum_code_view
  having sum(untqty- comqty) > 0
  group by prtnum,lotnum,untcst,invsts
/
