/* #REPORTNAME= IC Great Wall FG On-Hand Report*/
/* #HELPTEXT= This report lists the finished good item, committed quantity,*/
/* #HELPTEXT= the great wall location and other rack locations for that item.*/
/* #HELPTEXT = Requested by Inventory Control */

ttitle left print_time center 'IC Great Wall FG On-Hand Report' -
	right print_date skip 2

btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item Number'
column prtnum format a14
column lngdsc heading 'Description'
column lngdsc format a30
column type   heading 'Item Type'
column type   format a10
column comqty heading 'Comm. Qty'
column comqty format 999999
column stoloc heading 'Gwall Location'
column stoloc format a15
column otherloc heading 'Other Locations'
column otherloc format a15


SELECT invsum.prtnum,
substr(prtdsc.lngdsc,1,30) lngdsc,
prtmst.vc_itmtyp type,
invsum.stoloc
FROM invsum, prtmst, prtdsc
WHERE invsum.prtnum||'|----' = prtdsc.colval
AND prtdsc.colnam='prtnum|prt_client_id'
AND prtdsc.locale_id ='US_ENGLISH'
AND invsum.prtnum=prtmst.prtnum
AND invsum.prt_client_id = prtmst.prt_client_id
AND prtmst.prt_client_id = '----'
AND invsum.arecod||''='GWALL'
AND prtmst.vc_itmtyp ='FG'
/
