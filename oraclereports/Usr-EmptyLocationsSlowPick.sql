
/* #REPORTNAME=User Empty Locations Slow Pick Report */
/* #HELPTEXT= Requested by Order Management */


ttitle left print_time center 'User Empty Locations Slow Pick Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a10
column stoloc heading 'Location'
column stoloc format a12

select arecod, stoloc
from locmst where curqvl=0 and pndqvl=0 and useflg = 1
and arecod in ('RACKSPCK')
and not exists(select 'x' from invlod where invlod.stoloc
= locmst.stoloc)
order by stoloc
/
