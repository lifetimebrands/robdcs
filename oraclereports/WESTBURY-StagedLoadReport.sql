/* #REPORTNAME=Westbury Staged Load Report */
/* #HELPTEXT= The Staged Load Report lists, by */
/* #HELPTEXT= order number, PO Number */
/* #HELPTEXT= and Costs associated with the PO */
/* #HELPTEXT= Requested by Westbury */ 
ttitle left '&1 ' print_time -
		center 'Westbury Staged Load Report' -
		right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column cponum heading 'PO #'
column cponum format a18
column ordnum heading 'Order #'
column ordnum format a20
column prtnum heading 'Item'
column prtnum format a15
column stgqty heading 'Staged Qty'
column stgqty format 999,999

set linesize 200
set pagesize 5000


select
       ord.cponum,
       ord.ordnum,
       ord_line.prtnum,
       sd.stgqty 
  from
        ord, ord_line, adrmst lookup1,
        shipment_line  sd, dscmst,
        shipment sh
        WHERE
          ord.st_adr_id                   = lookup1.adr_id
          AND ord.client_id               = lookup1.client_id
         AND ord.ordnum                  = ord_line.ordnum
          AND ord.client_id               = ord_line.client_id
          AND ord_line.client_id          = sd.client_id
          AND ord_line.ordnum             = sd.ordnum
          AND ord_line.ordlin             = sd.ordlin
          AND ord_line.ordsln             = sd.ordsln
          AND sd.ship_id                  = sh.ship_id
          AND dscmst.colnam               = 'shpsts'
          AND dscmst.locale_id            = 'US_ENGLISH'
          AND dscmst.colval               = sh.shpsts
          AND sh.shpsts                   in ('S', 'P','X')
order  
      by
         ord.cponum,
         ord.ordnum, ord_line.prtnum
/
