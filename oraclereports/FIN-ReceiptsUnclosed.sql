/* #REPORTNAME= FIN-Receipts Unclosed Report */
/* #HELPTEXT= This report gives a list of truck receipts */
/* #HELPTEXT=  that have not been closed. */
/* #HELPTEXT= Enter dates in format DD-MON-YYYY . */
/* #HELPTEXT= Requested by The Controller for Receiving */

/* #VARNAM=begin , #REQFLG=Y */
/* #VARNAM=end , #REQFLG=Y */

/* Author: Al Driver */
/* Report provides a detailed listing of container receipts that   */
/* have not been closed. */
/* I used a union for to obtain the numbers in both production */
/* and archive. This report is similar to User-Receipts By Shift. */

set pagesize 1000 
set linesize 450

ttitle left print_time -
center 'FIN - Receipts Unclosed Report  ' -
right print_date skip 2


column trknum heading 'File#'
column trknum format A20 
column vnd_name heading 'Vendor_Name'
column vnd_name format A30
column arrdte heading 'Arrived_Date'
column arrdte format A24
column clsdte heading 'Close_Date'
column clsdte format A24

alter session set nls_date_format ='DD-MON-YYYY';


select
  distinct rcvtrk.trknum trknum,substr(adrmst.adrnam,1,30) vnd_name,
trunc(trlr.arrdte) arrdte,substr(trunc(rcvtrk.clsdte),1,11) clsdte
    from
    trlr, supmst,rcvtrk,rcvinv,rcvlin,adrmst
    where
    supmst.adr_id = adrmst.adr_id(+)
    and supmst.client_id = adrmst.client_id(+)
    and supmst.supnum(+) = rcvinv.supnum
    and supmst.client_id(+) = rcvinv.client_id
    and rcvinv.trknum = rcvtrk.trknum
    and rcvtrk.trlr_id = trlr.trlr_id
    and rcvinv.trknum= rcvlin.trknum
    and rcvinv.supnum = rcvlin.supnum
    and rcvinv.invnum = rcvlin.invnum
    and rcvinv.client_id = rcvlin.client_id
    and rcvlin.trknum = rcvtrk.trknum
    and trunc(trlr.arrdte) between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
    and rcvtrk.clsdte is null
union
select
  distinct rcvtrk.trknum trknum,substr(adrmst.adrnam,1,30) vnd_name,
trunc(rcvtrk.arrdte) arrdte,substr(trunc(rcvtrk.clsdte),1,11) clsdte
    from
    supmst@arch,rcvtrk@arch,rcvinv@arch,rcvlin@arch,adrmst@arch
    where
    supmst.adr_id = adrmst.adr_id(+)
    and supmst.client_id = adrmst.client_id(+)
    and supmst.supnum(+) = rcvinv.supnum
    and supmst.client_id(+) = rcvinv.client_id
    and rcvinv.trknum = rcvtrk.trknum
    and rcvinv.trknum= rcvlin.trknum
    and rcvinv.supnum = rcvlin.supnum
    and rcvinv.invnum = rcvlin.invnum
    and rcvinv.client_id = rcvlin.client_id
    and rcvlin.trknum = rcvtrk.trknum
and rcvtrk.clsdte is null
and not exists(select 'x' from rcvlin x where x.rcvkey = rcvlin.rcvkey)
and trunc(rcvtrk.arrdte) between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
order by 3
/
