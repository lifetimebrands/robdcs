/* #REPORTNAME=ENG - BOM Information */
/* #HELPTEXT = Requested by Engineering */
/* #HELPTEXT = Get BOM information to check */
/* #HELPTEXT = Production Update files. */
/* #HELPTEXT = Enter date in DD-MON-YYYY format */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */  

column wkonum heading 'Work Order'
column wkonum format a20
column clsdte heading 'Close Date'
column clsdte format date
column prtnum heading 'Part Number'
column prtnum format a30
column LaborType heading 'Labor Type'
column LaborType format a11
column bom heading 'Tot. Consumed/BOM'
column bom format a20
column prdqty heading 'Production Qty.'
column prdqty format a20

set pagesize 500
set linesize 200

alter session set nls_date_format ='dd-mon-yyyy';

select a.wkonum, b.clsdte, b.prtnum, a.prtnum LaborType, 
to_char(round(sum(a.tot_cnsqty/a.bom_cnsqty))) bom, to_char(b.prdqty) prdqty
from wkodtl a, wkohdr b
where a.wkonum=b.wkonum
and a.prtnum in ('LABOR','PRODOHLABR')
and b.clsdte between '&1' and '&2'
group by a.wkonum, b.clsdte, b.prtnum, a.prtnum, b.prdqty
order by clsdte;
