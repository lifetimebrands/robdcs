
/* #REPORTNAME=User Rack Med Locations Need to Consolidate Report */
/* #HELPTEXT= This report generates locations that need  */
/* #HELPTEXT= to be consolidated in RackMed, but only list items */
/* #HELPTEXT= with multiple locations. */
/* #HELPTEXT= Requested by Inventory Control */

/* Created by: Al Driver */
/* Report modeled after other Rack Consolidation reports. */
/* Eliminates any part#s listed once,  */
/* and only shows a record if it has a corresponding location with */
/* the same part#, lotnum, invsts and untcas. We modified the main */
/* script to only show items with multiple locations. Then we used */
/* two inline views to compare one table to itself to only */
/* show records that will have a corresponding location to be consolidated into. */

ttitle left print_time center 'User Rack Med Locations Need to Consolidate Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a12
column stoloc heading 'Location'
column stoloc format a12
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Com. Qty'
column comqty format 999999
column prtnum heading 'Item'
column prtnum format a12
column invsts heading 'Invsts'
column invsts format a6
column untpal heading 'Unt. Pal'
column untpal format 999999
column untcas heading 'Unt. Case'
column untcas format 999999
column untpak heading 'Unt. Pak'
column untpak format 999999
column locpak heading 'Location Untpak'
column locpak format 999999
column loccas heading 'Location Untcas'
column loccas format 999999
column fullcase heading 'Actual # of Cases'
column fullcase format 999999.99
column lotnum heading 'Lot Number'
column lotnum format a10
column pcntfull heading 'Percent Full'
column pcntfull format 999.99999

set linesize 500;
set pagesize 5000;

select distinct x.*
from
(select distinct invsum.arecod, invsum.stoloc, invsum.untqty, invsum.comqty, invsum.prtnum,
invdtl.invsts,invdtl.lotnum, invdtl.untcas, prtmst.untpal,(invsum.untqty/invdtl.untcas) fullcase,
(invsum.untqty/prtmst.untpal) pcntfull
      from prtmst, invsum,invlod,invsub,invdtl
      where
       prtmst.prtnum = invsum.prtnum
       and prtmst.prt_client_id = invsum.prt_client_id
       and prtmst.prtnum = invdtl.prtnum
       and prtmst.prt_client_id = invdtl.prt_client_id
       and invsum.stoloc = invlod.stoloc
       and invsum.prtnum = invdtl.prtnum
       and invsum.prt_client_id = invdtl.prt_client_id
       and invlod.lodnum = invsub.lodnum
       and invsub.subnum = invdtl.subnum
       and (invsum.untqty/prtmst.untpal) <= .8
       and (invsum.untqty/prtmst.untpal) <> 0
       and invsum.comqty =0
       and invsum.arecod ||'' = 'RACKMED'
       and exists (select count(a.stoloc)  from invsum a      /* only list items with multiple locations */
       where a.prtnum = invsum.prtnum and a.arecod = 'RACKMED' 
       having count(a.stoloc) > 1)
       order by invsum.prtnum) x, 
(select distinct invsum.arecod, invsum.stoloc,invsum.untqty , invsum.comqty,invsum.prtnum,invdtl.invsts,invdtl.lotnum,
invdtl.untcas, prtmst.untpal,(invsum.untqty/invdtl.untcas) fullcase,
(invsum.untqty/prtmst.untpal) pcntfull
      from prtmst, invsum,invlod,invsub,invdtl
      where prtmst.prtnum = invsum.prtnum
       and prtmst.prt_client_id = invsum.prt_client_id
       and prtmst.prtnum = invdtl.prtnum
       and prtmst.prt_client_id = invdtl.prt_client_id
       and invsum.stoloc = invlod.stoloc
       and invsum.prtnum = invdtl.prtnum
       and invsum.prt_client_id = invdtl.prt_client_id
       and invlod.lodnum = invsub.lodnum
       and invsub.subnum = invdtl.subnum
       and (invsum.untqty/prtmst.untpal) <= .8
       and (invsum.untqty/prtmst.untpal) <> 0
       and invsum.comqty =0
       and invsum.arecod ||'' = 'RACKMED'
       and exists (select count(a.stoloc)  from invsum a
       where a.prtnum = invsum.prtnum and a.arecod = 'RACKMED'
       having count(a.stoloc) > 1)
       order by invsum.prtnum) y
where x.prtnum = y.prtnum            /* check to make sure prtnum,lotnum,invsts and untcas is the same */
and x.lotnum = y.lotnum             /* ,but storage locations are different.  */
and x.invsts = y.invsts
and x.untcas = y.untcas
and x.stoloc != y.stoloc
/
