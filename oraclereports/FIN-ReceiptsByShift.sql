/* #REPORTNAME= FIN-Receipts By Shift Report */
/* #HELPTEXT= This report gives a list of truck receipts by shift. */
/* #HELPTEXT= Enter dates in format DD-MON-YYYY . */
/* #HELPTEXT= Requested by The Controller             */

/* #VARNAM=begin , #REQFLG=Y */
/* #VARNAM=end , #REQFLG=Y */

set pagesize 5000 
set linesize 450

ttitle left print_time -
center 'FIN-Receipts By Shift Report  ' -
right print_date skip 2


/* Author: Al Driver */
/* Report provides a detailed listing of container receipts.  */
/* Created a function usrGetShift to obtain the shift based on the time */
/* the container arrived. This report is similar to Usr-Receipts Unclosed */
/* I used a union to obtain the numbers in both production */
/* and archive. I did not use invsum@arch because it contained */
/* no information. */

column trknum heading 'File#'
column trknum format A20 
column vnd_name heading 'Vendor_Name'
column vnd_name format A30
column arrdte heading 'Arrived_Date'
column arrdte format A24
column clsdte heading 'Close_Date'
column clsdte format A24
column expdte heading 'Expected_Date'
column expdte format A24 
column shift heading 'Shift'
column shift format 999 
column invsts heading 'Invsts'
column invsts format A6 

alter session set nls_date_format ='DD-MON-YYYY HH:MI:SS:AM';


select
  distinct rcvtrk.trknum trknum,substr(adrmst.adrnam,1,30) vnd_name,substr(trunc(rcvtrk.expdte),1,11) expdte,
substr(trunc(trlr.arrdte),1,11) arrdte,
   substr(trunc(rcvtrk.clsdte),1,11) clsdte,
   usrGetShift(to_date(trlr.arrdte,'DD-MON-YYYY HH:MI:SS:AM')) shift,invdtl.invsts invsts
    from
    trlr,supmst,rcvtrk,rcvinv,rcvlin,adrmst,invdtl
    where
    supmst.adr_id = adrmst.adr_id(+)
    and supmst.client_id = adrmst.client_id(+)   
    and supmst.supnum(+) = rcvinv.supnum
    and supmst.client_id(+) = rcvinv.client_id   
    and rcvinv.trknum = rcvtrk.trknum
    and rcvtrk.trlr_id = trlr.trlr_id   
    and rcvinv.trknum= rcvlin.trknum
    and rcvinv.supnum = rcvlin.supnum
    and rcvinv.invnum = rcvlin.invnum 
    and rcvinv.client_id = rcvlin.client_id
    and rcvlin.trknum = rcvtrk.trknum 
    and rcvlin.rcvkey = invdtl.rcvkey
    and rcvlin.prtnum = invdtl.prtnum
and trunc(trlr.arrdte) between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
union
select
  distinct rcvtrk.trknum trknum,substr(adrmst.adrnam,1,30) vnd_name,substr(trunc(rcvtrk.expdte),1,11) expdte,
substr(trunc(rcvtrk.arrdte),1,11) arrdte,
   substr(trunc(rcvtrk.clsdte),1,11) clsdte,
   usrGetShift(to_date(rcvtrk.arrdte,'DD-MON-YYYY HH:MI:SS:AM')) shift,invdtl.invsts invsts
    from
    supmst@arch supmst,rcvtrk@arch rcvtrk,rcvinv@arch rcvinv,rcvlin@arch rcvlin,adrmst@arch adrmst,invdtl@arch invdtl
    where
    supmst.adr_id = adrmst.adr_id(+)
    and supmst.client_id = adrmst.client_id(+)  
    and supmst.supnum(+) = rcvinv.supnum
    and supmst.client_id(+) = rcvinv.client_id
    and rcvinv.trknum = rcvtrk.trknum 
    and rcvinv.trknum= rcvlin.trknum
    and rcvinv.supnum = rcvlin.supnum
    and rcvinv.invnum = rcvlin.invnum 
    and rcvinv.client_id = rcvlin.client_id  
    and rcvlin.trknum = rcvtrk.trknum
    and rcvlin.rcvkey = invdtl.rcvkey   
    and rcvlin.prtnum = invdtl.prtnum
and not exists(select 'x' from rcvlin x where x.rcvkey = rcvlin.rcvkey)
and trunc(rcvtrk.arrdte) between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
/
