/* #REPORTNAME=PI Counted Full Report */
/* #HELPTEXT= This report compares the counted location */
/* #HELPTEXT= against the empty location snapshot data */
/* #HELPTEXT= Requested by Inventory Control */
/* #GROUPS=NOONE */

set pagesize 5000
set linesize 200


ttitle left print_time -
center 'PI Counted Full Report' -
right print_date skip 2 -

column arecod heading 'Area  '
column arecod format a20
column stoloc heading 'Location'
column stoloc format A30
column prtnum heading 'Item '
column prtnum format  A14
column lotnum heading 'Final Lot Code '
column lotnum format A20
column invsts heading 'Final Status'
column invsts format A15
column cntqty heading 'Final Counted'
column cntqty format 999999999
column untqty heading 'Final On Hand'
column untqty format 999999999
column cnt_usr_id heading 'User'
column cnt_usr_id format A14

/* We will have to remove the untqty = 0 clause. Also the qty must be from invdtl, not cntwrk  */ 

select l.arecod, l.stoloc, id.prtnum, id.lotnum, id.invsts, sum(id.untqty) untqty, c.cnt_usr_id
from invdtl id, invsub ib, invlod il, locmst l, cntwrk c
where c.cntsts = 'C'
and not exists (select 1 from cntwrk c2 where c2.stoloc = c.stoloc and c2.untqty > 0)
and c.stoloc = l.stoloc
and l.stoloc = il.stoloc
and il.stoloc = l.stoloc
and il.lodnum = ib.lodnum
and ib.subnum = id.subnum
group by l.arecod, l.stoloc, id.prtnum, id.lotnum, id.invsts, c.cnt_usr_id
order by 1,2
/
