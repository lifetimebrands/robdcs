/* #REPORTNAME=User Priviliges Report */
/* #HELPTEXT= The User Priviliges Report lists all  */
/* #HELPTEXT= priviliges for all the users in the system */
/* #HELPTEXT= No input value is required*/
/* #HELPTEXT= Requested by MIS. */

ttitle left '&1 ' print_time center 'User Priviliges Report' -
	right print_date skip 2 
btitle skip 1 center 'Page: ' format 999 sql.pno

column empnum heading 'Employee'
column empnum format a15
column empname heading 'Name'
column empname format a30
column role_id  heading 'Role'
column role_id format a50
column optnam heading 'Operation'
column optnam format a20

set linesize 200
set pagesize 3000 

select les_usr_role.usr_id empnum,
adrmst.adrnam empname, les_usr_role.role_id
from les_usr_role, adrmst, les_usr_ath
where les_usr_role.usr_id=les_usr_ath.usr_id
and adrmst.adr_id = les_usr_ath.adr_id
and les_usr_ath.usr_sts = 'A'
group by les_usr_role.usr_id, adrmst. adrnam, les_usr_role.role_id
union
select les_opt_ath.ath_id, null,  substr(sys_dsc_mst.mls_text,1,50) mls_text
from les_opt_ath, sys_dsc_mst, les_mnu_opt, les_usr_role, les_usr_ath
where les_opt_ath.ath_id =  les_usr_role.usr_id
and les_mnu_opt.opt_nam = les_opt_ath.opt_nam
and les_opt_ath.ath_typ = 'U'
and les_mnu_opt.opt_nam = sys_dsc_mst.colval
and les_usr_ath.usr_id = les_usr_role.usr_id
and les_usr_ath.usr_sts = 'A'
and sys_dsc_mst.colnam  = 'opt_nam'
and sys_dsc_mst.locale_id = nvl(NULL, 'US_ENGLISH')
and sys_dsc_mst.cust_lvl = (
select max(cust_lvl) cust_lvl
from sys_dsc_mst sdm
where sdm.colnam = sys_dsc_mst.colnam
and sdm.colval = sys_dsc_mst.colval
and sdm.locale_id = sys_dsc_mst.locale_id)
group by les_opt_ath.ath_id, substr(sys_dsc_mst.mls_text,1,50)
/



