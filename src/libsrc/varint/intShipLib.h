/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  Purpose: Miscellaneous common functions for shipping commands 
 *           Note that all the validation libraries log a required
 *           field missing error message.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <applib.h>


long   intValObjectId( char *            tab_name_i,
                       char *            tab_key_name_i,
                       long *            object_id_i,
		       RETURN_STRUCT **  ret_struct_i );

long   intValTablePK( char *            tab_name_i,
                     char *            tab_key_name_i,
                     char *            object_id_i,
		     RETURN_STRUCT **  ret_struct_i );

long   intValCodeMaster( char *            col_name_i,
                         char *            col_val_i,
  		         RETURN_STRUCT **  ret_struct_i );

long   intValAddress( char *            adr_id_i,
  		      RETURN_STRUCT **  ret_struct_i );

char * intCslAddNoDup( char * list_str, char * add_str );

long   intCslValExists( char * list_str, char * find_str );

long   intGetPolicyData( char *            polcod_i,
                         char *            polvar_i,
                         char *            polval_i,
                         char *            rtstr1,
                         char *            rtstr2,
                         long *            rtnum1,
                         long *            rtnum2,
                         double *          rtflt1,
                         double *          rtflt2,
                         RETURN_STRUCT **  ret_data );

