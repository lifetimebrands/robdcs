/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 McHugh Software International, Inc.
 *  Shelton, Connecticut,  U.S.A.
 *  All rights reserved.
 *
 *  Application:   tmint
 *  Created:	07/08/99
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct tm_row_struct {
  char ship_line_id[SHIP_LINE_ID_LEN + 1];
  long pckqty;
  long untcas;
  long untpak;
  moca_bool_t case_splflg;
  moca_bool_t pack_stdflg;

  }  TM_ROW_STRUCT;

static double def_max_volume = -10.0;
static char split_stategy[50];
static int sGetNextLane(void);
static int sGetNextLaneLocation (int *Lane, char *Location);
static void sInitialize(void);

static void sInitialize(void)
{
	long ret_status;
    char sqlBuffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;
	char polval[100];

    if (def_max_volume < 0)
    {
	char polval[100];
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select * from poldat where polcod = 'VAR' and "
			"polvar = 'SHIPMENT-LANE-SPLITTING' and polval = 'DEFAULT-VOLUME' ");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		   {
		   strcpy(polval, sqlGetString(res, row, "polval"));
		   if (!strcmp(polval, "DEFAULT-VOLUME"))
		   	def_max_volume = sqlGetFloat(res, row, "rtflt1");
  		   }
		}
	else			
        {
            /* No policy either, use default */
        def_max_volume = 150000; 
        }
    }

}

/* Consoldiate and split shipment lines based on volume */
LIBEXPORT 
RETURN_STRUCT *varSetLanesForShipmentRR(char *ship_id_i, double *max_volume_i)
{
    int ret_status;
    char ship_id[SHIP_ID_LEN + 1];
    char cons_batch[CONS_BATCH_LEN + 1];
    char sqlBuffer[1000];
    char myLocation[STOLOC_LEN+1];
    double shpvol;
    double cur_volume;
    double palvol;
    mocaDataRes *res;
    mocaDataRow *row;
    mocaDataRes *laneOrderRes;
    mocaDataRow *laneOrderRow;
    long NextLane;
 	
    if (!ship_id_i && !misTrimLen(ship_id_i, SHIP_ID_LEN))
        return (APPMissingArg("ship_id"));

    sInitialize();

    misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);
    sprintf(sqlBuffer,
        "select distinct 1  from var_wavpar_dtl wd, cstmst, ord, shipment_line where ship_id = '%s' "
   	"and shipment_line.client_id = ord.client_id and shipment_line.ordnum = ord.ordnum "
	"and cstnum in (rtcust, stcust, btcust) and cstmst.cstnum = wd.vc_cstnum and rownum < 2"
        " union select 1 from shipment, cardtl where ship_id = '%s' and shipment.carcod = cardtl.carcod "
	"and shipment.srvlvl = cardtl.srvlvl and cardtl.cartyp = 'S'", ship_id, ship_id);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
    {
	sqlFreeResults(res);
        return (srvResults(eOK, NULL));
    }
    sqlFreeResults(res);

    sprintf(sqlBuffer,
        "select distinct vc_palvol  from shipment_line, ord, cstmst where ship_id = '%s' "
   	"and shipment_line.client_id = ord.client_id and shipment_line.ordnum = ord.ordnum "
	"and cstnum in (rtcust, stcust, btcust) and vc_palvol is not null and rownum < 2", ship_id);
    ret_status = sqlExecStr(sqlBuffer, &res);

    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }
   
    if (ret_status == eOK)
   	{
        row = sqlGetRow(res);
	palvol = sqlGetFloat(res, row, "vc_palvol");
 	sqlFreeResults(res);
	}
    else
        {
        palvol = def_max_volume;
        /*NextLane = sGetNextLane();
        sprintf(sqlBuffer,
        "update shipment_line set vc_wave_lane = %d where ship_id = '%s'", NextLane, ship_id);
        ret_status = sqlExecStr(sqlBuffer, NULL);

        return (srvResults(ret_status, NULL)); */
    	} 
   
    sprintf(sqlBuffer,
      "select z.ship_line_id, z.wrkref, z.shpvol from "
      "(select min(ship_line_id) ship_line_id, wrkref,  sum(ctnlen*ctnwid*ctnhgt) shpvol       from ctnmst, pckwrk where "
      "pckwrk.ship_id = '%s' and pcksts = 'P' and wrktyp = 'K' and lodlvl = 'S' and "
      "pckwrk.ctncod = ctnmst.ctncod "
      "group by ship_line_id, wrkref "
      "union "
      "select  ship_line_id, wrktyp, sum((caslen*caswid*cashgt) * (pckwrk.pckqty/pckwrk.untcas)) shpvol"
      " from ftpmst, pckwrk where "
      "  pckwrk.ship_id = '%s' and pcksts = 'P' and wrktyp||'' = 'P' and lodlvl = 'S' and lodflg != 1 and "
      "  pckwrk.ftpcod = ftpmst.ftpcod group by ship_line_id, wrktyp ) z "
      "  where exists (select 1 from shipment_line sl where sl.ship_line_id = z.ship_line_id and " 
      " sl.uc_lanflg = '0' and uc_seqnum is null) ", ship_id, ship_id);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }

    ret_status =  sGetNextLaneLocation (&NextLane, myLocation);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }

    /*NextLane = sGetNextLane(); */
    misTrc(T_FLOW, "Beginning with Lane: %d",NextLane); 
    cur_volume = 0;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	shpvol = sqlGetFloat(res, row, "shpvol");
	
        misTrc(T_FLOW, "Checking Volume: %f against %f", shpvol+cur_volume, palvol); 
	if ((shpvol + cur_volume) > palvol)
	    {
            misTrc(T_FLOW, "Getting Next Lane");
    	    ret_status =  sGetNextLaneLocation (&NextLane, myLocation);
    	    if (ret_status != eOK)
     		{
		sqlFreeResults(res);
        	return (srvResults(ret_status, NULL));
    		}

    	    misTrc(T_FLOW, "New Lane: %d",NextLane); 
	    cur_volume = shpvol;
	    }	
	else
	    cur_volume += shpvol;
	   
        sprintf(sqlBuffer,
        "update shipment_line set vc_wave_lane = %d, uc_lanflg = 0, uc_seqnum = '%s' where ship_line_id = '%s'", NextLane, myLocation,
		sqlGetString(res, row, "ship_line_id"));
        ret_status = sqlExecStr(sqlBuffer, NULL);

        sprintf(sqlBuffer,
        "update locmst set rescod = '%s' where stoloc = '%s'", ship_id, myLocation);
        ret_status = sqlExecStr(sqlBuffer, NULL);

    }
    sqlFreeResults(res);
    return (srvResults(eOK, NULL));
}

static int sGetNextLaneLocation (int *Lane, char *Location)
{
   int ret_status;
   mocaDataRes *res;
   mocaDataRow *row;
   int myLane,i;
   char command[400];
   char myStoloc[STOLOC_LEN+1];

   for (i=0;i<50;i++)
   {
   myLane = sGetNextLane();

   sprintf(command, "select stoloc from locmst where arecod = 'PALLET' and "
		    "stoloc like 'LANE%03d-SLOT%%' and aisle_id = 'LANE%03d' and rescod is null order by stoloc", myLane,
	myLane); 
 
   ret_status = sqlExecStr(command, &res);
   if (ret_status != eOK)
	{
	sqlFreeResults(res);
    	if (ret_status != eDB_NO_ROWS_AFFECTED)
  	   return (ret_status); 
	}
	else
        {
        row = sqlGetRow(res);
	strcpy(Location, sqlGetString(res,row, "stoloc"));
	*Lane = myLane;
	return (eOK);
	}

   }
return (eDB_NO_ROWS_AFFECTED); 
}



static int sGetNextLane(void)
{
    int ret_status;
    char *sqlBuffer1[1000];
    static mocaDataRes *res;
    static mocaDataRow *row;
    mocaDataRes *res1;
    mocaDataRow *row1;
    int RetSeq;
    static char RetLane[STOLOC_LEN+1], NextLane[STOLOC_LEN+1], MaxLane[STOLOC_LEN+1], MinLane[STOLOC_LEN+1];

    int notFound;

    if (!strlen(MaxLane))
       {
       sprintf(sqlBuffer1, "select rtstr1 from poldat where  polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and polval = 'LANE-LIST-RR'  and rtnum1 =("  
	  "select max(rtnum1) from  var_wavpar_hdr, poldat where polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and rtnum1 = vc_wave_lane and polval = 'LANE-LIST-RR' and vc_non_wave_flg = 0) ");
    	ret_status = sqlExecStr(sqlBuffer1, &res);
    	if (ret_status != eOK)
    		{
		sqlFreeResults(res);
        	return (1);
    		}
        row = sqlGetRow(res);
	strcpy(MaxLane, sqlGetString(res, row, "rtstr1"));
	sqlFreeResults(res);

       sprintf(sqlBuffer1, "select rtstr1 from poldat where polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and polval = 'LANE-LIST-RR'  and rtnum1 ="  
	  "(select min(rtnum1) from var_wavpar_hdr, poldat where polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and rtnum1 = vc_wave_lane and polval = 'LANE-LIST-RR' and vc_non_wave_flg = 0) ");
    	ret_status = sqlExecStr(sqlBuffer1, &res);
    	if (ret_status != eOK)
    		{
		sqlFreeResults(res);
        	return (1);
    		}
        row = sqlGetRow(res);
	strcpy(MinLane, sqlGetString(res, row, "rtstr1"));
	sqlFreeResults(res);
       } 

       sprintf(sqlBuffer1, "select rtstr1, rtnum1 from poldat where polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and polval = 'NEXT-LANE-TO-USE-RR' ");
    	ret_status = sqlExecStr(sqlBuffer1, &res);
    	if (ret_status != eOK)
    		{
		sqlFreeResults(res);
        	return (1);
    		}
        row = sqlGetRow(res);
	strcpy(RetLane, sqlGetString(res, row, "rtstr1"));
	RetSeq = sqlGetLong(res, row, "rtnum1");
        sprintf(RetLane, "%d", RetSeq); 
	sqlFreeResults(res);

        if (!strcmp(MaxLane, RetLane))
          sprintf(sqlBuffer1, "update poldat set rtstr1 = '%s', rtnum1 = %s where polcod = 'VAR' and polvar = "
          " 'SHIPMENT-LANE-SPLITTING' and polval = 'NEXT-LANE-TO-USE-RR' ", MinLane, MinLane);
	else
          sprintf(sqlBuffer1, "update poldat set rtnum1 = ( select rtnum1 from poldat where polcod = 'VAR' "
		" and polvar = 'SHIPMENT-LANE-SPLITTING' and polval = 'LANE-LIST-RR' and rtnum1 = (select "
		" nvl(min(rtnum1), %s) from var_wavpar_hdr, poldat where "
		" vc_non_wave_flg = 0 and polcod = 'VAR' and rtnum1 = vc_wave_lane  and polvar = 'SHIPMENT-LANE-SPLITTING' and "
		"polval = 'LANE-LIST-RR' and rtnum1  > %d and rtnum1 not in (select distinct vc_wave_lane from var_wavpar_dtl))) where polcod = 'VAR' and polvar = "
          	" 'SHIPMENT-LANE-SPLITTING' and polval = 'NEXT-LANE-TO-USE-RR' ", MinLane, RetSeq);

    	ret_status = sqlExecStr(sqlBuffer1, NULL);
	return (RetSeq);
} 
