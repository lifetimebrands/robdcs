static const char *rcsid = "$Id: varConfirmBulkLoad.c,v 1.2 2002/01/18 04:37:41 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This function generates a UCC code for each unconfirmed 
 *	subload on the load no. passed, and prints the label for each 
 *  	unconfirmed case. 
 *
 *#END************************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <varerr.h>

LIBEXPORT 
RETURN_STRUCT  *varConfirmBulkLoad( char *lodnum_i ,
				    long *verify_flg_i,
				    char *devcod_i) 
{
    RETURN_STRUCT   *returnData=NULL  ;

    long  casrem ;
    long  cascnt ;
    long  vc_cascnt ;
    long  untqty ;
    long  untcas ;
    long  ret_status ;
    long  status  ;
    long  verify_flg ;
    long  pckqty;
    long  caspckqty; /* kjh 5/29 needed for pckwrk.lodlvl=S */
    long  appqty;

    mocaDataRow   *cur_row ,  *case_row, *row ,*row1; 
    mocaDataRes   *res, *res1, *resdtl ,*ressub, *reswrk;

    char buffer[3500];
    char buffer2[3500];
    char ValueList[3500]; 
    char lodnum[LODNUM_LEN + 1];
    char subnum[SUBNUM_LEN + 1];
    char subnum_old[SUBNUM_LEN + 1];
    char new_subnum[SUBNUM_LEN + 1];
    char prtadr [PRTADR_LEN + 1];
    char ship_id[SHIP_ID_LEN + 1];
    char subucc[UCCCOD_LEN + 1 ];
    char dtlnum[DTLNUM_LEN +1];
    char cons_ship_line_id[SHIP_LINE_ID_LEN+1];	/*MR693kjh cons pik */
    char cons_wrkref[WRKREF_LEN+1];		/*MR693kjh cons pik */
    char new_dtlnum[DTLNUM_LEN +1];
    char new_wrkref[WRKREF_LEN +1] ;
    char wrkref[WRKREF_LEN + 1 ] ;	/* work ref of original load*/
    char ship_line_id[SHIP_LINE_ID_LEN + 1 ] ;	
    char cmbcod[CMBCOD_LEN + 1 ] ;		/*MR693kjh cons pik */
    long i;
    char devcod[DEVCOD_LEN + 1];   

    misTrimcpy(lodnum, lodnum_i, LODNUM_LEN );
    misTrimcpy(devcod, devcod_i, DEVCOD_LEN );
    verify_flg = *verify_flg_i ;

    /* get printer address */

    /* LHSTART - Alex Arifin - 01/17/02 
     * Want to get the printer address 
     * from the RDT device code that
     * we will pass in.
     *
     * Original: 
     *   sprintf(buffer, "select devmst.prtadr"
     *	    " from devmst, locmst, invlod"
     *	    " where devmst.devcod = locmst.devcod"
     *	    " and locmst.stoloc = invlod.stoloc"
     * 	    " and invlod.lodnum = '%s'"	 , 
     *	    lodnum); 
     */

     sprintf(buffer, "select devmst.prtadr"
        " from devmst"
        " where devmst.devcod = '%s'" ,
        devcod);

    /* LHSTOP - Alex Arifin - 01/17/02 */

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status !=eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(eVAR_NO_PRINTER_FOR_LOCATION,"")) ;
    }
    row = sqlGetRow (res);

    if (sqlIsNull (res, row, "prtadr")) {
	return (srvSetupReturn(eVAR_NO_PRINTER_FOR_LOCATION,"")) ;
    }

    strcpy (prtadr, sqlGetString (res, row, "prtadr"));

    sqlFreeResults (res);

    /* check if load contains more than one SKU prdct or different untcas */ 

    sprintf(buffer, "select distinct prtnum, untcas "
	    " from invdtl, invsub"
	    " where invsub.subnum = invdtl.subnum " 
	    " and invsub.lodnum = '%s' ",
	    lodnum);

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status !=eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    
    if (sqlGetNumRows (res) > 1) {
	sqlFreeResults(res);
	return (srvSetupReturn(eVAR_TOO_MANY_PARTS,"")) ;	    
    }

    sqlFreeResults (res);                              

    /* Get ship_id and shp_label type */ 
     
    /* 08/21/2001 Patrick Pape Start
     * MR 626
     * Remove the join to cstmst table. The data selected from cstmst (shplbl)
     * is not used ever. It is selected here but then ignored. According to the
     * MR, "there may be no entry in customer master". If, in fact, the data
     * selected is not used and the table where it is selected from may not
     * have an entry, then there is no need for the table to be present in
     * our query.
     */ 
    sprintf(buffer, "select distinct shipment_line.ship_id "
	    " from invlod, invsub, invdtl, shipment_line "
	    " where invsub.lodnum	  = invlod.lodnum "
	    " and invsub.subnum	  = invdtl.subnum "
	    " and invdtl.ship_line_id   = shipment_line.ship_line_id "
	    " and invlod.lodnum = '%s' " ,
	    lodnum );
    /* 08/21/2001 Patrick Pape Stop */

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status !=eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    if (sqlGetNumRows (res) > 1) {
	sqlFreeResults(res);
	return (srvSetupReturn(eINT_TOO_MANY_SHIP_ID,"")) ;
    }

    row = sqlGetRow (res);

    strcpy (ship_id, sqlGetString(res,row,"ship_id"));

    sqlFreeResults (res);

    /* get unconfirmed cases for the given lodnum */

    sprintf(buffer, "select * "
	    " from invsub "
	    " where invsub.lodnum = '%s' " 
	    " and phyflg = 0 "
	    " and subucc is null " ,
	    lodnum );
    
    ret_status = sqlExecStr(buffer, &ressub) ;

    
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	return (srvSetupReturn(ret_status,"")) ;
    }
    if (ret_status == eDB_NO_ROWS_AFFECTED) {
	if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	return (srvSetupReturn(eVAR_NO_SUBLOAD_FOUND,"")) ;
    }

    /* If in verify mode, then return normal success without doing anything */

    if (verify_flg_i && verify_flg) {
	if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	return (srvSetupReturn(eOK,""));
    }


    for (cur_row = sqlGetRow(ressub); cur_row;cur_row = sqlGetNextRow(cur_row)) {
	strcpy (subnum, sqlGetString(ressub,cur_row,"subnum"));
	memset (subucc, 0, sizeof (subucc));

	/* check if amount of product is a whole number  */
	/* MR693 kjh we are forced to get ship_line_id and wrkref for cons pix */
	/* orig b4MR693
		sprintf(buffer, "select	untqty, untcas ,mod(untqty, untcas) casrem "
		" from invdtl "
		" where	subnum = '%s' " ,
		subnum );
	*/
	/* begin new 4 MR693 kjh */	
	sprintf(buffer, "select	ship_line_id, wrkref, sum(untqty) untqty, "
		" untcas ,mod(sum(untqty), untcas) casrem "
		" from invdtl "
		" where	subnum = '%s' group by ship_line_id, wrkref, untcas " ,
		subnum );
	/* end new 4 MR693 kjh */

	ret_status = sqlExecStr(buffer, &res1) ;

	if (ret_status !=eOK) {
	    if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	    if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	    return (srvSetupReturn(ret_status,"")) ;
	}

	/* MR693 must loop thru the dtls when we have a consolidated pick*/
        for (case_row = sqlGetRow(res1); case_row;
		case_row = sqlGetNextRow(case_row)) {
	  strcpy (cons_ship_line_id, sqlGetString(res1,case_row,"ship_line_id"));
	  strcpy (cons_wrkref, sqlGetString(res1,case_row,"wrkref"));
	
	  /* orig B4MR693 case_row = sqlGetRow(res1);  ...now gotten in for loop*/
	  casrem   = sqlGetLong(res1, case_row, "casrem"); 

	  if (casrem) {
	    if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	    if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	    return (srvSetupReturn(eVAR_NOT_WHOLE_CASES,"")) ;
	  }
	     
	  untqty   = sqlGetLong(res1,case_row,"untqty"); 
	  untcas   = sqlGetLong(res1,case_row,"untcas"); 
	  cascnt   = untqty/untcas ;

	  /* orig b4MR693 sprintf(buffer, "select * from invdtl where subnum='%s' " ,
		subnum );
	  */
	  /* begin new 4 MR693 kjh */	
	    sprintf(buffer, "select * from invdtl "
		" where	subnum='%s' and ship_line_id='%s' and wrkref='%s' " ,
		subnum, cons_ship_line_id, cons_wrkref );
	  /* end new 4 MR693 kjh */

	  ret_status = sqlExecStr(buffer, &resdtl) ;

	  if (ret_status !=eOK) {
	    if (resdtl) sqlFreeResults(resdtl);   /* kjh Aug31 we need to free */
	    if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	    if (ressub) sqlFreeResults(ressub);   /* kjh Aug31 we need to free */
	    return (srvSetupReturn(ret_status,"")) ;
	  }
          row1 = sqlGetRow(resdtl);
          strcpy ( wrkref, sqlGetString(resdtl, row1, "wrkref"));
	  /* MR693 Aug31  kjh i think it is ok to just select by wrkref here */
	  sprintf(buffer, "select * from pckwrk where wrkref ='%s' " , wrkref );

	  ret_status = sqlExecStr(buffer, &reswrk) ;

	  if (ret_status !=eOK) {
	    if (reswrk) sqlFreeResults(reswrk);
	    if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	    if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	    if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	    return (srvSetupReturn(ret_status,"")) ;
	  }

	  row = sqlGetRow(reswrk) ;
	  pckqty  = sqlGetLong(reswrk, row,"pckqty");
	  strcpy(ship_line_id, sqlGetString(reswrk, row,"ship_line_id"));
	  strcpy(cmbcod, sqlGetString(reswrk, row,"cmbcod")); /*MR693 Sep10 */
	  /*kjh must stamp case pick quantity on the exploded lodlvl=S pckwrk*/
	  caspckqty  = sqlGetLong(reswrk, row,"untcas");	/*kjh May29*/
	  /*kjh appqty was never setup in this program...lets use untcas*/
	  appqty  = sqlGetLong(reswrk, row,"untcas");	/*kjh May24*/

	  for (i=0; i<cascnt;i++ ) {
	    /*       GENERATE UCC CODE    */ 

	    sprintf(buffer , "get ucc128 label information where wrkref = '%s' and "
		    "ship_line_id = '%s' and lodlvl = '%s'"
		    "|generate ucc128 identifier where sumflg = 1",
		    wrkref, ship_line_id, LODLVL_SUBLOAD) ;

	    status = srvInitiateCommand (buffer, &returnData);
	    
	    if (status !=eOK) {                   
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(status,"")) ;
	    }

	    res = returnData->ReturnedData;
	    
	    row = sqlGetRow (res) ;

	    strcpy (subucc, sqlGetString(res,row,"subucc"));     

	    srvFreeMemory (SRVRET_STRUCT, returnData);  
			                      	    
	    status = appNextNum (NUMCOD_SUBNUM, new_subnum);
	    if (status != eOK) {
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(status,"")) ;
	    }

	    status = appNextNum (NUMCOD_DTLNUM, new_dtlnum);
	    if (status != eOK) {
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(status,"")) ;
	    }

	    status = appNextNum (NUMCOD_WRKREF, new_wrkref);
	    if (status != eOK) {
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(status,"")) ;
	    }

	    sprintf(ValueList, " '%s','%s','%s','%ld','%ld' " , 
		    new_wrkref,
		    subucc, 
		    LODLVL_SUBLOAD , 
		    caspckqty, 	/* kjh 5/29 make sublevel pix use caseqty*/
		    appqty);
	    		    
	    ret_status = appGenerateTableEntry("pckwrk",
		reswrk,
		sqlGetRow(reswrk),
		" wrkref , subucc, lodlvl , pckqty, appqty" ,
		ValueList);

		/* keep our pckqty's in synch for 1-of calculations */
    	    sprintf(buffer , 
		"update pckwrk set pckqty = pckqty - %ld, appqty = appqty - %ld" 
		" where wrkref = '%s'" ,
		caspckqty, appqty, wrkref) ; 

    	    ret_status = sqlExecStr(buffer, NULL) ;

    	    if (ret_status !=eOK) {
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(ret_status,"")) ;
    	    } 

	    sprintf(ValueList, " '%s','%s','%ld' " , 
		    new_subnum,
		    subucc, 
		    1);
	  	    		    
	    ret_status = appGenerateTableEntry("invsub",
				       ressub,
				       sqlGetRow(ressub),
				       " subnum, subucc, phyflg" ,
				       ValueList);

	    sprintf(ValueList, " '%s', '%s','%s','%ld' " , 
	  	    new_wrkref,	/* kjh 5-29 need to add with new wrkref */
		    new_subnum,
		    new_dtlnum, 
		    appqty);	/* kjh 5-29 this was wrong ...was untqty);*/
	  	    		    
	    ret_status = appGenerateTableEntry("invdtl",
				       resdtl,
				       sqlGetRow(resdtl),
				       " wrkref, subnum, dtlnum, untqty" ,
				       ValueList);
	    
	    /*  issue Produce Shipping Label; print request */

	    sprintf (buffer, "produce shipment label where wrkref='%s' and prtadr = '%s' ",
	  	    new_wrkref, prtadr);	/* kjh 6-25 uses new wrkref */

	    status = srvInitiateCommand (buffer,NULL);

	    if (status !=eOK) {
	      if (reswrk) sqlFreeResults(reswrk);
	      if (resdtl) sqlFreeResults(resdtl); /* kjh Aug31 we need to free */
	      if (res1)   sqlFreeResults(res1);	  /* kjh Aug31 we need to free */
	      if (ressub) sqlFreeResults(ressub); /* kjh Aug31 we need to free */
	      return (srvSetupReturn(status,"")) ;
	    }
	  } /* looping for all cases to be exploded into details */
	} /* MR693 kjh loop for all dtls in a consolidated pik */

	sprintf(buffer , " delete from invsub where subnum = '%s' " ,
			subnum ) ; 

	ret_status = sqlExecStr(buffer, NULL) ;

	if (ret_status !=eOK) {
	  if (reswrk) sqlFreeResults(reswrk);
	  if (resdtl) sqlFreeResults(resdtl);     /* kjh Aug31 we need to free */
	  if (res1)   sqlFreeResults(res1);	    /* kjh Aug31 we need to free */
	  if (ressub) sqlFreeResults(ressub);	    /* kjh Aug31 we need to free */
	  return (srvSetupReturn(ret_status,"")) ;
	} 
	
	/* kjhMay29 destroy original bulk confirm invdtl for the subnum */
	sprintf(buffer , " delete from  invdtl "
			"  where subnum = '%s' " ,
			subnum ) ; 

	ret_status = sqlExecStr(buffer, NULL) ;

	if (ret_status !=eOK) {
	  if (reswrk) sqlFreeResults(reswrk);
	  if (resdtl) sqlFreeResults(resdtl);     /* kjh Aug31 we need to free */
	  if (res1)   sqlFreeResults(res1);	    /* kjh Aug31 we need to free */
	  if (ressub) sqlFreeResults(ressub);	    /* kjh Aug31 we need to free */
	  return (srvSetupReturn(ret_status,"")) ;
	} 
    } /* end looping of subs on load  that is being confirmed */
	
    /* kjhMay29 destroy original bulk confirm pckwrk...replaced by cases*/
    /* oldMay29 sprintf(buffer, "delete from pckwrk where wrkref='%s'",wrkref); */
    /* MR693 Sep10 destroy original bulk confirm pckwrk...replaced by cases     */
    /* MR693 Sep10 we are going by cmbcod since this may be a consolidated pick */
    sprintf(buffer,"delete from pckwrk where cmbcod='%s' and lodlvl='L'",cmbcod); 

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status !=eOK) {
	if (reswrk) sqlFreeResults(reswrk);
	if (resdtl) sqlFreeResults(resdtl);     /* kjh Aug31 we need to free */
	if (res1)   sqlFreeResults(res1);	    /* kjh Aug31 we need to free */
	if (ressub) sqlFreeResults(ressub);	    /* kjh Aug31 we need to free */
	return (srvSetupReturn(ret_status,"")) ;
    } 

    if (reswrk) sqlFreeResults(reswrk);
    if (resdtl) sqlFreeResults(resdtl);
    if (res1)   sqlFreeResults(res1);	    /* kjh Aug31 we need to free */
    if (ressub) sqlFreeResults(ressub);

    return (srvSetupReturn(eOK,""));
}      
