/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /cvs/les/wm/src/libsrc/dcspm/pmlib.h,v $
 *  $Revision: 1.33 $
 *  $Id: pmlib.h,v 1.33 2004/09/23 16:19:57 mzais Exp $
 *
 *  Application:  Intrinsic Library
 *  Created:   31-Jan-1994
 *  $Author: mzais $
 *
 *  Purpose:   Structure defs for PMLIB
 *
 *#END************************************************************************/

#ifndef PMLIB_H
#define PMLIB_H

/* This must be included first */

#include <moca_app.h>

/* Standard C header files */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* WM header files */

#include <moca_app.h>
#include <mocaerr.h>
#include <mcscolwid.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcsgendef.h>
#include <applib.h>

/* Parcel Manifest configuration definitions */

#define PM_INVTID_MANUAL	"MANUAL-PACKAGE"
#define PM_MFSTID_SPLIT		"SPLIT-SHIPMENT"
#define PM_SERVICE_DELIMITER	'.'
#define PM_XMIT_INFO_DELIMITER	','
#define PM_HOST                 "HOST"
#define PM_PORT                 "PORT"

/* Parcel Manifest length definitions */

#define PM_ACTION_CODE_LEN	40
#define PM_FORMAT_CODE_LEN	100
#define PM_PRINTER_CODE_LEN	40
#define PM_SERVICE_CODE_LEN	40
#define PM_STOCK_CODE_LEN       100	

#define PM_BUFFER_LEN		2000
#define PM_CONTEXT_LEN		4000
#define PM_MANIFEST_ID_LEN	MANCLS_LEN  /* 30 */
#define PM_PACKAGE_TYPE_LEN	10
#define PM_PRINTER_PORT_LEN     40	
#define PM_STATUS_TEXT_LEN	80
#define PM_XMIT_COMMAND_LEN	256
#define PM_XMIT_INFO_LIST_LEN	256

/* Parcel Manifest action code definitions */

#define PM_ACTION_ASSIGN_PRINTER		"ASSIGN PRINTER"
#define PM_ACTION_CLOSE_MANIFEST		"CLOSE MANIFEST"
#define PM_ACTION_CREATE_PACKAGE		"CREATE PACKAGE"
#define PM_ACTION_CREATE_LTL_SHIPMENT           "CREATE LTL SHIPMENT"
#define PM_ACTION_GET_NEXT_TRACKING_NUMBER	"GET NEXT TRACKING NUMBER"
#define PM_ACTION_LIST_ASSIGNED_PRINTERS	"LIST ASSIGNED PRINTERS"
#define PM_ACTION_LIST_CLOSED_MANIFESTS		"LIST CLOSED MANIFESTS"
#define PM_ACTION_LIST_CLOSED_PACKAGES		"LIST CLOSED PACKAGES"
#define PM_ACTION_LIST_COUNTRIES		"LIST COUNTRIES"
#define PM_ACTION_LIST_MANIFEST_FORMATS		"LIST MANIFEST FORMATS"
#define PM_ACTION_LIST_OPEN_MANIFESTS		"LIST OPEN MANIFESTS"
#define PM_ACTION_LIST_OPEN_PACKAGES		"LIST OPEN PACKAGES"
#define PM_ACTION_LIST_PACKAGE_FORMATS		"LIST PACKAGE FORMATS"
#define PM_ACTION_LIST_PRINTER_STOCKS		"LIST PRINTER STOCKS"
#define PM_ACTION_LIST_PRINTERS			"LIST PRINTERS"
#define PM_ACTION_LIST_SERVICE_TRANSLATIONS	"LIST SERVICE TRANSLATIONS"
#define PM_ACTION_LIST_SERVICES			"LIST SERVICES"
#define PM_ACTION_PRODUCE_MANIFEST_DOCUMENT	"PRODUCE MANIFEST DOCUMENT"
#define PM_ACTION_PRODUCE_PACKAGE_LABEL		"PRODUCE PACKAGE LABEL"
#define PM_ACTION_RATE_PACKAGE			"RATE PACKAGE"
#define PM_ACTION_RELEASE_PACKAGE		"RELEASE PACKAGE"
#define PM_ACTION_REMOVE_CLOSED_MANIFEST	"REMOVE CLOSED MANIFEST"
#define PM_ACTION_REMOVE_PACKAGE		"REMOVE PACKAGE"
#define PM_ACTION_TRANSMIT_CLOSED_MANIFEST	"TRANSMIT CLOSED MANIFEST"

/* Parcel Manifest inventory identifier type code definitions */

#define PM_INVTYP_SUBNUM	"subnum"
#define PM_INVTYP_SUBUCC	"subucc"
#define PM_INVTYP_CRTNID	"crtnid"
#define PM_INVTYP_PRTNUM	"prtnum"

/* Parcel Manifest package status code definitions */

#define PM_MANSTS_HELD		MANSTS_HELD
#define PM_MANSTS_RELEASED	MANSTS_RELEASED
#define PM_MANSTS_MANIFESTED	MANSTS_MANIFESTED
#define PM_MANSTS_SHIPPED	MANSTS_SHIPPED

/* Parcel Manifest package type code definitions */

#define PM_PAKTYP_DEFAULT	PM_PAKTYP_CUSTOM

#define PM_PAKTYP_BOX		"BOX"
#define PM_PAKTYP_CUSTOM	"CUSTOM"
#define PM_PAKTYP_LETTER	"LETTER"
#define PM_PAKTYP_PAK		"PAK"
#define PM_PAKTYP_TUBE		"TUBE"

/* Parcel Manifest system policy definitions */

#define PM_POL_COD		"PARCEL-MANIFEST"

#define PM_POL_VAR_CONFIG	"CONFIGURATION"
#define PM_POL_CFG_RMTHST	"PARCEL-REMOTE-HOST"
#define PM_POL_CFG_SHNAME	"PARCEL-SHIPPER-NAME"
#define PM_POL_CFG_SYSNAM	"PARCEL-SYSTEM-NAME"
#define PM_POL_CFG_REFCMD	"PACKAGE-REFERENCE-COMMAND"
#define PM_POL_CFG_WGTMUL	"PACKAGE-WEIGHT-MULTIPLIER"

#define PM_POL_VAR_DEFAULT	"DEFAULT"
#define PM_POL_DEF_DEFCTY	"COUNTRY-CODE"
#define PM_POL_DEF_DEFRPT	"REPORT-PREFIX"

#define PM_POL_CAR_XMIT_INFO	"TRANSMIT-INFORMATION"
#define PM_POL_CAR_XMIT_CMD	"TRANSMIT-COMMAND"
#define PM_POL_CAR_MIN_DIM	"MINIMUM-DIMENSIONS"

/* Parcel Manifest system mls text definitions */

#define PM_MLS_MIXPRT		"lbl_mixed_part"

/* Parcel Manifest system data structure definitions */

typedef struct {
    long iniflg;
    /**/
    /* Parcel Manifest system information */
    /**/
    char rmthst [RTSTR1_LEN + 1];
    char shname [RTSTR1_LEN + 1];
    char sysnam [RTSTR1_LEN + 1];
    char dstnam [DSTNAM_LEN + 1];
    char prcl_host [PARVAL_LEN + 1];
    char prcl_port [PARVAL_LEN + 1];
    /**/
    /* Parcel package creation information */
    /**/
    char   refcmd [RTSTR1_LEN + 1];
    double wgtmul;
    /**/
    /* Parcel Manifest system defaults */
    /**/
    char defcty [RTSTR1_LEN + 1];
    char defrpt [RTSTR1_LEN + 1];
    /**/
    /* Parcel Manifest internal operating information */
    /**/
    long mulcar;  /* Multiple DCS carriers mapped to single Parcel Manifest carrier */
    /**/
} PM_CONFIG;

/* Parcel Manifest system data structures */

extern PM_CONFIG pmConfig;

/* Library function prototypes */

long   pmContextStart  (char *contxt);
long   pmContextEnd    (char *contxt);
long   pmContextDouble (char *contxt, double value, char *name);
long   pmContextLong   (char *contxt, long value, char *name);
long   pmContextString (char *contxt, char *value, char *name);
char  *pmFormatDcsCarrierSqlSelectionClause (char *carcod);
char  *pmFormatDcsCarrierSqlSelectionClauseDstcar (char *carcod);
long   pmGetCarrierShipDate (char *shdate, char *dstcar, long movflg);
double pmGetFloat  (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetLong   (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
char  *pmGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetFloatIfSet  (double *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetLongIfSet   (long *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetStringIfSet (char *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetPackageIdInfo (char *carcod, char *mfsmsn, char *crtnid, 
                           char *invtid, 
                           char *wrkref, long opnflg, char *dstnam,
                           char *mansts);
long   pmGetPolicyInfo (char *polvar, char *polval, char *polsub, char *rtstr1, char *rtstr2, long *rtnum1, long *rtnum2, double *rtflt1, double *rtflt2);
long   pmInitialize (void);
long   pmLogMsg (char *header, char *format, ...);
RETURN_STRUCT *pmReturnRemoteHostError (long status_i);
long   pmTranslateFromCarrierCode (char *carcod, char *service_code);
long   pmTranslateFromServiceCode (char *carcod, char *srvlvl, char *service_code);
long   pmTranslateIntoCarrierCode (char *service_code, char *carcod);
long   pmTranslateIntoServiceCode (char *service_code, char *carcod, char *srvlvl);
long   pmTranslateIntoPackageType (char *package_type, char *paktyp);
long   pmVerifyGuiInitialization (char *carcod);
long   pmVerifyMultipleDcsCarriers (void);
long   pmFedexInitialize(char *srvnam);

#endif
