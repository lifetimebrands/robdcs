/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999 Software Architects, Inc.
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /cvs/les/wm/src/libsrc/dcsint/intCmpSrchLib.h,v $
 *  $Revision: 1.2 $
 *
 *  Application:   intCmpSrchlib
 *  Created:       11/28/2000
 *  $Author: mzais $
 *
 *
 *#END************************************************************************/

#ifndef _INTCMPSRCH_H_
#define _INTCMPSRCH_H_

#include <dcsgendef.h>
#include <dcscolwid.h>
#include "intlib.h"

/*
** Defines
*/


#define BOX_ICON   "box.ico"
#define BLANK_ICON ""

/*
** Typedefs
*/
typedef struct CmpSrchNode
               {
                   void *data;
                   struct CmpSrchNode *next;
                   struct CmpSrchNode *prev;
               } CS_NODE;

typedef struct CmpSrchHead
               {
                   long num_nodes;
                   struct CmpSrchNode *first;
                   struct CmpSrchNode *end;
               } CS_FIFO;

/*
** Functions
*/

/*----------------------------------------------------------------------
** The following linked list functions will operate the stack needed for
** the Component Search functionality.  In all cases, the caller is
** responsible for allocating memory for the data node, and for freeing
** the data node after the node is popped from the queue.
**
** Note that the queue currently only operates in a FIFO manner - 
** technically, new nodes are added onto the end of the list,
** and nodes are popped off the front of the list.
----------------------------------------------------------------------*/

/*
** This function will initialize and allocate memory for a queue object.
** It should always be called to initialize the queue.
*/
CS_FIFO *csl_initializeFIFOQueue();

/*
** This function will clear out the queue, freeing all data associated with
** it.  This should always be used to free the memory allocated for the
** list, even if it has zero elements.
*/
void csl_freeFIFOQueue(CS_FIFO *ptr);

/*
** This function will indicate whether the queue is empty or not.
*/
moca_bool_t csl_isFIFOQueueEmpty(CS_FIFO *ptr);

/*
** This function returns the number of nodes on the queue
*/
int csl_numNodesFIFOQueue(CS_FIFO *ptr);

/*
** This function pushes a node onto the queue in FIFO fashion (that is,
** it throws it on the end.
**
** It returns eOK if the operation succeeded, or an error code if it
** fails.
*/
int csl_pushNodeFIFOQueue(CS_FIFO *ptr, void *data);

/*
** This function pops a node from the front of the queue
** It returns an eOK if it is successful, and an error if there's a failure.
** This is to differentiate between a node that exists, but has NULL data,
** and an errored node.
*/
int csl_popNodeFIFOQueue(CS_FIFO *ptr, void **data);

/*
** This function does a misTRC dump of the entire list, and should be used
** for debugging purposes only.
*/
void csl_dumpFIFOQueue(CS_FIFO *ptr);


#endif
