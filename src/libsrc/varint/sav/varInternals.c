static const char *rcsid = "$Id: intInternals.c,v 1.34 2004/08/17 18:47:28 mzais Exp $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mislib.h>
#include <sqllib.h>
#include <srvlib.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

static long _intCheckTruckClosed(char *trknum_i, int rcvmod);

static long _intCheckTruckClosed(char *trknum_i, int rcvmod)
{
    mocaDataRes *res, *res2;
    mocaDataRow *row, *row2;
    long        ret_status;
    char        buffer[200];

    misTrc(T_FLOW, "Checking if the truck is closed");
    sprintf(buffer,
            "select trlr.trlr_stat, "
            "       trlr.trlr_cod "
            "  from rcvtrk, "
            "       trlr "
            " where rcvtrk.trlr_id = trlr.trlr_id "
            "   and rcvtrk.trknum = '%s' ",
            trknum_i);
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        return (ret_status);
    }
    row = sqlGetRow(res);

    /* If it's either Closed/Dispatched, or it's been turned around */
    /* for shipping, we'll consider it closed for receiving and     */
    /* check the policy to see if this is allowed.                  */

    if ((strcmp(sqlGetString(res, row, "trlr_cod"), TRLR_COD_SHIP) == 0) ||
        (strcmp(sqlGetString(res, row, "trlr_stat"), TRLSTS_CLOSED) == 0) ||
        (strcmp(sqlGetString(res, row, "trlr_stat"), TRLSTS_DISPATCHED) == 0))
    {
        /* Truck is closed, can't receive or reverse against it.
         */

        sqlFreeResults(res);
        if (rcvmod == RCVMOD_RECEIVE)
            return (eINT_CANT_RECEIVE_AGAINST_CLOSED_TRUCK);
        else
        {
            /*
             * Whether or not we can reverse against a closed truck
             * is controlled by a policy.  So, let's check the value.
             */

            sprintf(buffer,
                    " select rtnum1 "
                    "  from poldat "
                    " where polcod = '%s' "
                    "   and polvar = '%s' "
                    "   and polval = '%s' ",
                    POLCOD_RECEIVE_INVENTORY,
                    POLVAR_RECEIVE_UNDO,
                    POLVAL_ALLOW_REVERSE_AFTER_CLOSE);

            ret_status = sqlExecStr(buffer, &res2);
            row2 = sqlGetRow(res2);
            if (sqlGetLong(res2, row2, "rtnum1") == BOOLEAN_FALSE)
            {
                sqlFreeResults(res2);
                return (eINT_CANT_REVERSE_AGAINST_CLOSED_TRUCK);
            }
            sqlFreeResults(res2);
        }
    }

    return (eOK);
    sqlFreeResults(res);
}


/*****  Used by intCheckReceiveLine and intRemoveLoad  *****/

RETURN_STRUCT *var_UpdateReceiveLine(char *srcloc_i,
				     char *dstloc_i,
				     char *client_id_i,
				     char *supnum_i,
				     char *invnum_i,
				     char *lodnum_i,
				     char *subnum_i,
				     char *dtlnum_i,
				     char *updmod_i)
{
    static mocaDataRes *AreaRes=NULL;
    static moca_bool_t checkOverRcp, allowBlind, invRcv;
    static moca_bool_t checkRimLin, checkRcvLin;
    mocaDataRes    *res, *invres, *rcvres, *rimres, *rcvlnres;
    mocaDataRow    *row, *invrow, *rcvrow, *AreaRow, *rimrow, *rcvlnrow;
    char            command[2000];
    char            tmpCommand[2000];
    char            srcloc[STOLOC_LEN + 1];
    char            dstare[ARECOD_LEN + 1];
    char            dstloc[STOLOC_LEN + 1];
    char            client_id[CLIENT_ID_LEN + 1];
    char            supnum[SUPNUM_LEN + 1];
    char            invnum[INVNUM_LEN + 1];
    char            lodnum[LODNUM_LEN + 1];
    char            subnum[SUBNUM_LEN + 1];
    char            dtlnum[DTLNUM_LEN + 1];
    char            wrk_dtlnum[DTLNUM_LEN + 1];
    char            new_dtlnum[DTLNUM_LEN + 1];
    char            wrk_client_id[CLIENT_ID_LEN + 20];
    char            wrk_supnum[SUPNUM_LEN + 20];
    char            wrk_invnum[INVNUM_LEN + 20];
    long            untqty;
    double          catchqty;
    long            avaqty, rim_avaqty;
    double          avacatchqty, rim_avacatchqty, tmpCatchQty;
    long            ret_status;
    long            validLines;
    long            curLine;
    long            tmpseqnum;
    moca_bool_t     incrementing;
    moca_bool_t     needRcvKey;
    moca_bool_t     rimFound;
    RETURN_STRUCT  *CurPtr, *RcvPtr, *RetPtr, *RimPtr;

    memset(command, 0, sizeof(command));
    memset(tmpCommand, 0, sizeof(tmpCommand));
    memset(srcloc, 0, sizeof(srcloc));
    memset(dstare, 0, sizeof(dstare));
    memset(dstloc, 0, sizeof(dstloc));
    memset(client_id, 0, sizeof(client_id));
    memset(supnum, 0, sizeof(supnum));
    memset(invnum, 0, sizeof(invnum));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(new_dtlnum, 0, sizeof(new_dtlnum));
    memset(wrk_client_id, 0, sizeof(wrk_client_id));
    memset(wrk_supnum, 0, sizeof(wrk_supnum));
    memset(wrk_invnum, 0, sizeof(wrk_invnum));

    CurPtr = RimPtr = NULL;
   

    /* verify the parms */
    if (srcloc_i && misTrimLen(srcloc_i, STOLOC_LEN))
	strncpy(srcloc, srcloc_i, STOLOC_LEN);

    if (dstloc_i && misTrimLen(dstloc_i, STOLOC_LEN))
	strncpy(dstloc, dstloc_i, STOLOC_LEN);

    /* determine the destination area */
    if (*dstloc)
    {
        sprintf(command, 
                "select arecod "
                "  from locmst "
                " where stoloc = '%s'", 
                dstloc);

        ret_status = sqlExecStr(command, &res);
        if(ret_status != eOK)
        {
            sqlFreeResults(res);
            return APPInvalidArg ("", "dstloc");
        }
        row = sqlGetRow(res);
        strncpy(dstare, sqlGetString(res, row, "arecod"), ARECOD_LEN);
        sqlFreeResults(res);
    }

    /*
     * Try and set up the client Id with the next function.  If
     * 3PL is not installed, then it's OK to get the default client, since
     * all records will be using that anyways. 
     * If 3Pl is installed, then if a client was passed, we'll use it.
     * Note, that appGetclient returns eINVALID_ARGS if the client wasn't 
     * passed, but in our case, that's OK, it just won't be used for the
     * select statements.
     */

    ret_status = appGetClient(client_id_i, client_id);
    if (ret_status == eOK && strlen(client_id))
    {
	sprintf(wrk_client_id, " and client_id = '%s' ", client_id);
    }
    else if ((ret_status != eINVALID_ARGS) && (strlen (client_id_i) > 0))
    {
	return (srvResults (ret_status, NULL));
    }	

    if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
    {
	misTrimcpy(supnum, supnum_i, SUPNUM_LEN);
	sprintf(wrk_supnum, " and supnum = '%s' ", supnum);
    }

    if (invnum_i && misTrimLen(invnum_i, INVNUM_LEN))
    {
	strncpy(invnum, invnum_i, INVNUM_LEN);
	sprintf(wrk_invnum, " and invnum = '%s' ", invnum);
    }

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
	strncpy(lodnum, lodnum_i, LODNUM_LEN);

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
	strncpy(subnum, subnum_i, SUBNUM_LEN);

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
	strncpy(dtlnum, dtlnum_i, DTLNUM_LEN);

    /* Determine if we're incrementing or decrementing */
    incrementing = (strncmp(updmod_i, "INC", 3) == 0);

    /* If we are incrementing and we are not coming from the creation or */
    /* permanent expected receipts location, then get out */
    /* Else, we are decrementing ... so source and dest s/b the same */
    if (incrementing &&
	(strncmp(srcloc, 
		 STOLOC_PERM_CREATE, strlen(STOLOC_PERM_CREATE)) != 0 &&
	 strncmp(srcloc, 
		 STOLOC_PERM_EXPRCP, strlen(STOLOC_PERM_EXPRCP)) != 0))
    {
	return(srvResults(eOK, NULL));
    }

    /* Load the static data */
    if (AreaRes == NULL)
    {
	/* Determine if we have to check for over-receiving */
        /* Note that as of 6.0.0, we will not take catch units into
         * account when checking for over receiving 
         */

        checkOverRcp = checkRimLin = checkRcvLin = 0;
	sprintf(command,
                "select * "
                "  from poldat "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' ",
                POLCOD_PRD_OVRRECV,
                POLVAR_CHECK_POSITION,
                POLVAL_DEFAULT);

	ret_status = sqlExecStr(command, &res);

        if (eOK == ret_status)
        {
            row = sqlGetRow(res);
            /*
             * rtstr1 determines if we check for overreceipt at this 
             * point (i.e., identify) or not.
             *
             * rtstr2 determines how we determine if we have over
             * received.  We can check either by RCVLIN, or RIMLIN,
             * or BOTH.
             */

            if (!strncmp(RTSTR1_OVRRCV_IDENTIFY,
                         sqlGetString(res, row, "rtstr1"),
                         RTSTR1_LEN))
            {
                checkOverRcp = 1;
            }

            if (!strncmp(RTSTR2_OVRRCV_RIMLIN, 
                         sqlGetString(res, row, "rtstr2"),
                         RTSTR2_LEN) ||
                !strncmp(RTSTR2_OVRRCV_BOTH,
                         sqlGetString(res, row, "rtstr2"),
                         RTSTR2_LEN))
            {
                checkRimLin = 1;
            }

            if (!strncmp(RTSTR2_OVRRCV_RCVLIN, 
                         sqlGetString(res, row, "rtstr2"),
                         RTSTR2_LEN) ||
                !strncmp(RTSTR2_OVRRCV_BOTH,
                         sqlGetString(res, row, "rtstr2"),
                         RTSTR2_LEN))
            {
                checkRcvLin = 1;
            }

            /* Make sure they are at least checking by one of the tables */
            if (checkOverRcp && !checkRimLin && !checkRcvLin)
            {
                checkRcvLin = 1;
            }
        }
        sqlFreeResults(res);

	/* Determine if blind receiving is allowed */
	sprintf(command,
                "select 'x' yep "
                "  from poldat "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' "
                "   and (rtstr1 = 'Y' or rtstr1 = '1')",
                POLCOD_RCVTRKOPR,
                POLVAR_MISC,
                POLVAL_RCV_ALLOW_BLIND);
	ret_status = sqlExecStr(command, NULL);
	allowBlind = (ret_status == eOK);

	/* Determine if invoice receiving is on */
	sprintf(command,
                "select 'x' yep "
                "  from poldat "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' "
                "   and (rtstr1 = 'Y' or rtstr1 = '1')",
                POLCOD_RDTALLOPR,
                POLVAR_MISC,
                POLVAL_RDT_MISC_USERCVINV);
	ret_status = sqlExecStr(command, NULL);
	invRcv = (ret_status == eOK);

	/* Get a list of all expected receipt areas */
        /* 5.1 Release - get list of expected receipt areas */
        /* that are not wip_expr areas. */

	sprintf(command,
		"select arecod "
                "  from aremst "
                " where expflg = '%ld'"
                "   and wip_expflg = '%ld' ",
                BOOLEAN_TRUE,
                BOOLEAN_FALSE);

	ret_status = sqlExecStr(command, &AreaRes);
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    return (srvResults(ret_status, NULL));
	}

        /* 
         * Make sure MOCA knows to call this routine to free this 
         * cached memory.
         */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, AreaRes);
    }

    /* See if we will be checking for over-receipt */
    if (checkOverRcp)
        misTrc(T_FLOW, 
               "We will be checking for over-receipt by %s%s%s.",
               checkRimLin ? "RIMLIN" : "",
               checkRimLin && checkRcvLin ? " and " : "",
               checkRcvLin ? "RCVLIN" : "");

    else
        misTrc(T_FLOW, "We will NOT be checking for over-receipt");

    /* Determine the area our truck is in */
    for (AreaRow=sqlGetRow(AreaRes); AreaRow; AreaRow=sqlGetNextRow(AreaRow))
    {
	if (strncmp(misTrim(dstare),
                    sqlGetString(AreaRes, AreaRow, "arecod"),
		    ARECOD_LEN) == 0)
	    break;
    }
    if (AreaRow == NULL)
	return(srvResults(eOK, NULL));

    /* Get the inventory we have to deal with */
    if (strlen(dtlnum))
	sprintf(command,
		"select * "
		"  from invdtl "
		" where invdtl.dtlnum = '%s' ",
		dtlnum);
    else if (strlen(subnum))
	sprintf(command,
		"select * "
		"  from invdtl "
		" where invdtl.subnum = '%s' ",
		subnum);
    else
	sprintf(command,
		"select invdtl.* "
		"  from invdtl, invsub "
		" where invsub.lodnum = '%s' "
		"   and invdtl.subnum = invsub.subnum ",
		lodnum);

    ret_status = sqlExecStr(command, &invres);
    if (eOK != ret_status)
    {
	sqlFreeResults(invres);
	return (srvResults(ret_status, NULL));
    }

    /* Initialize the return structure */
    RetPtr = NULL;
    RetPtr = srvResultsInit(eOK,
                            "dtlnum",    COMTYP_CHAR, DTLNUM_LEN,
                            "trknum",    COMTYP_CHAR, TRKNUM_LEN,
                            "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                            "supnum",    COMTYP_CHAR, SUPNUM_LEN,
                            "invnum",    COMTYP_CHAR, INVNUM_LEN,
                            "invlin",    COMTYP_CHAR, INVLIN_LEN,
                            "invsln",    COMTYP_CHAR, INVSLN_LEN,
                            NULL);

    /* Now loop through the inventory list and process each item */

    memset(wrk_dtlnum, 0, sizeof(wrk_dtlnum));
    for (invrow = sqlGetRow(invres); invrow; invrow = sqlGetNextRow(invrow))
    {
	/* Get a list of candidates to apply quantity to */
	needRcvKey = BOOLEAN_TRUE;
	CurPtr = NULL;


        /* 
         * If we are decrementing, we will already know if this load
         * is assigned to a receipt line by the rcvkey, so go by that
         * rather than trying to find it from the destination location.
         */

        if (incrementing)
        {
            /*
             * Make sure the truck is not closed.
             */

           ret_status = _intCheckTruckClosed(dstloc, RCVMOD_RECEIVE);

           if (ret_status != eOK)
               return (srvResults(ret_status, NULL));
	    sprintf(command,
                    "get matching receive line for identify "
                    "where dtlnum = '%s' "
                    "  and trknum = '%s' "
                    "  %s %s %s ",
                    sqlGetString(invres, invrow, "dtlnum"),
                    dstloc,
                    wrk_client_id,
                    wrk_supnum,
                    wrk_invnum);
        }
        else if (!sqlIsNull(invres, invrow, "rcvkey"))
        {
            sprintf(command, 
                    "list receive invoice lines "
                    " where rcvkey = '%s' ",
                    sqlGetString(invres, invrow, "rcvkey"));

            CurPtr = NULL;
            ret_status = srvInitiateCommand(command, &CurPtr);
            if (ret_status != eOK &&
                ret_status != eSRV_NO_ROWS_AFFECTED &&
                ret_status != eDB_NO_ROWS_AFFECTED)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                return (srvResults(ret_status, NULL));
            }
            res = srvGetResults(CurPtr);
            row = sqlGetRow(res);

            ret_status = _intCheckTruckClosed
                                      (sqlGetString(res, row, "trknum"),
                                      RCVMOD_REVERSE);

            if (ret_status != eOK)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                return (srvResults(ret_status, NULL));
            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);

            sprintf(command,
                    " Select seqnum,    "
                    "        trknum,    "
                    "        client_id, "
                    "        invnum,    "
                    "        supnum,    "
                    "        invlin,    "
                    "        invsln     "
                    "   from rcvlin     "
                    "  where rcvkey = '%s' " ,
                    sqlGetString (invres,invrow,"rcvkey")) ;

            ret_status = sqlExecStr(command, &rcvlnres);
            if (ret_status != eOK )
            {
                sqlFreeResults(rcvlnres);
                return (srvResults(ret_status,NULL));
            }
            rcvlnrow = sqlGetRow(rcvlnres);
		tmpseqnum = sqlGetLong(rcvlnres, rcvlnrow, "seqnum");

            if (tmpseqnum > 0 )
            {
                sprintf(command,
                        " list receive invoice lines "  
                        " where trknum = '%s'  "  
                        "   and client_id = '%s'  "  
                        "   and invnum = '%s'  "  
                        "   and supnum = '%s'  "  
                        "   and invlin = '%s'  "  
                        "   and invsln = '%s'  ",  
                        sqlGetString(rcvlnres, rcvlnrow, "trknum"),
                        sqlGetString(rcvlnres, rcvlnrow, "client_id"),
                        sqlGetString(rcvlnres, rcvlnrow, "invnum"),
                        sqlGetString(rcvlnres, rcvlnrow, "supnum"),
                        sqlGetString(rcvlnres, rcvlnrow, "invlin"),
                        sqlGetString(rcvlnres, rcvlnrow, "invsln"));
            }
            else
            {
                sprintf(command,
                        "list receive invoice lines "  
                        "where rcvkey = '%s' "  ,
                        sqlGetString (invres, invrow, "rcvkey")) ;
            }
            sqlFreeResults(rcvlnres);
        }
        else
            /* There is no receipt associated with this inventory */
            continue;
	
	ret_status = srvInitiateCommand (command, &CurPtr);

	if (eOK != ret_status && 
	    eSRV_NO_ROWS_AFFECTED != ret_status &&
	    eDB_NO_ROWS_AFFECTED != ret_status)
	{
	    if (RetPtr)
	        srvFreeMemory(SRVRET_STRUCT, RetPtr);
	    if (CurPtr) 
	        srvFreeMemory(SRVRET_STRUCT, CurPtr);
            sqlFreeResults(invres);
	    return(srvResults(ret_status, NULL));
	}

	/* If there are no valid lines and we are 
	 * incrementing, create a blind line */
	if (eSRV_NO_ROWS_AFFECTED == ret_status ||
	    eDB_NO_ROWS_AFFECTED == ret_status)
	{
           /*
           ** Can't  clear RetPtr here, because we may very well be using
           ** it later, since there is a continuation path that leads to
           ** publishing a return set using RetPtr.
           ** Changed 04/18/00 AFM PR 4865
	   ** if (RetPtr)
	   **     srvFreeMemory(SRVRET_STRUCT, RetPtr);
           */
	    if (CurPtr) 
	        srvFreeMemory(SRVRET_STRUCT, CurPtr);

            CurPtr = NULL;
		
	    if (!incrementing)
	        continue;  /* Go to the next detail */

	    /* If blind receiving is not allowed, error out */
	    if (!allowBlind)
	    {
	        sqlFreeResults(invres);
                /* Here, we free RetPtr, since we did not do it above.*/
                /* Changed 04/18/00 AFM PR 4865 */
	        if (RetPtr)
	            srvFreeMemory(SRVRET_STRUCT, RetPtr);
		return(srvResults(eINT_BLIND_RCV_NOT_ALLOWED, NULL));
	    }

            /* if invoice receiving is turned off, and the invoice which the */
            /* part belongs to could not be found, then default to the first */
            /* invoice on the truck. */

            if (!invRcv && !misTrimLen (invnum, INVNUM_LEN))
            {

                misTrc(T_FLOW, "Invoice was not passed in, or could not be \n"
                               "determined by part, and invoice receiving is \n"
                               "turned off.\n"
                               "Defaulting to first invoice on truck.");
                CurPtr = NULL;

                sprintf (tmpCommand,
                         "list receive invoice trucks where trknum = '%s'",
                         dstloc);

                ret_status = srvInitiateCommand(tmpCommand, &CurPtr);
                if (ret_status != eOK)
                {
                    if (RetPtr)
                        srvFreeMemory(SRVRET_STRUCT, RetPtr);
                    if (CurPtr)
                        srvFreeMemory(SRVRET_STRUCT, CurPtr);
                    sqlFreeResults(invres);
                    return(srvResults(ret_status, NULL));
                }

	        res = srvGetResults(CurPtr);
	        row = sqlGetRow(res);

                misTrimcpy (invnum, sqlGetString (res, row, "invnum"), 
                            INVNUM_LEN);
                misTrimcpy (supnum, sqlGetString (res, row, "supnum"), 
                            SUPNUM_LEN);
                
            }

            CurPtr = NULL;
	    /* Create the blind line, which will return the line details */
            sprintf(tmpCommand,
                    "create blind receive invoice line "
                    " where trknum = '%s' "
                    "   and client_id = '%s' "
                    "   and supnum = '%s' "
                    "   and invnum = '%s' "
                    "   and dtlnum = '%s' ",
                    dstloc,
                    client_id,
                    supnum,
                    invnum,
                    sqlGetString(invres, invrow, "dtlnum"));

	    ret_status = srvInitiateCommand (tmpCommand, &CurPtr);
	    if (eOK != ret_status)
	    {
	        if (RetPtr)
	            srvFreeMemory(SRVRET_STRUCT, RetPtr);
	        if (CurPtr) 
	            srvFreeMemory(SRVRET_STRUCT, CurPtr);
                sqlFreeResults(invres);
	        return(srvResults(ret_status, NULL));
	    }
	}

	untqty = sqlGetLong(invres, invrow, "untqty");
        catchqty = sqlGetFloat(invres, invrow, "catch_qty");
	res = srvGetResults(CurPtr);
	row = sqlGetRow(res);
	validLines = sqlGetNumRows(res);
	curLine = 1;

        /*
         * Ok, now we go through and apply the qty.  For unit qty, it
         * can get applied to more than one line.
         * This could screw things up for parts with catch qty,
         * because we could apply catch qty across multiple lines.
         * There's really no good way to do this, so our approach
         */
        
	while (untqty > 0 && row)
	{

            avaqty = rim_avaqty = 0;
            avacatchqty = rim_avacatchqty = 0;

	    /* If we are decrementing, no need to check for over-receipt */
	    if (!incrementing)
	    {
	        avaqty = sqlGetLong(res, row, "idnqty");
                avacatchqty = sqlGetFloat(res, row, "idn_cqtch_qty");

                if (checkRimLin)
                {
                    rim_avaqty = avaqty;
                    rim_avacatchqty = avacatchqty;
                }
	    }
	    else if (sqlGetBoolean(res, row, "blind_flg") == BOOLEAN_TRUE)
	    {
		/* Allow all quantity to go onto a blind line */
                /* Similarly, all the catch qty can go onto the same
                 * line.
                 */

	        avaqty = untqty;
                avacatchqty = catchqty;
                if (checkRimLin)
                {
                    rim_avaqty = avaqty;
                    rim_avacatchqty = avacatchqty;
                }
	    }
	    else /* incrementing on a non-blind line */
	    {
                /*
                 * Here's where we could eventually span more than
                 * one line.
                 */

                avaqty = sqlGetLong(res, row, "expqty") - 
                         sqlGetLong(res, row, "idnqty");
                avacatchqty = sqlGetFloat(res, row, "exp_catch_qty") - 
                              sqlGetFloat(res, row, "idn_catch_qty");

                /* If we are checking the rimlin for overage, select it here */
                if (checkRimLin)
                {
                    rimFound = 0;

                    sprintf(command,
                            " list receive invoice master lines "
                            "where supnum      = '%s' "
                            "  and client_id   = '%s' "
                            "  and invnum      = '%s' "
                            "  and invlin      = '%s' "
                            "  and invsln      = '%s' ",
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));

                    ret_status = srvInitiateCommand(command, &RimPtr);

                    if (eOK != ret_status &&
                        eSRV_NO_ROWS_AFFECTED != ret_status && 
                        eDB_NO_ROWS_AFFECTED != ret_status)
                    {
                        if (RimPtr)
                            srvFreeMemory(SRVRET_STRUCT, RimPtr);
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
	                if (CurPtr) 
	                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
                        return (srvResults(ret_status, NULL));
                    }

                    if (eOK == ret_status)
                    {
                        rimFound = 1;
                        rimres = srvGetResults(RimPtr);
                        rimrow = sqlGetRow(rimres);

                        rim_avaqty = sqlGetLong(rimres, rimrow, "expqty") - 
                                     sqlGetLong(rimres, rimrow, "idnqty");
                        rim_avacatchqty = sqlGetFloat(rimres, 
                                                      rimrow, 
                                                      "exp_catch_qty") - 
                                          sqlGetFloat(rimres, 
                                                      rimrow, 
                                                      "idn_catch_qty");
                    }
                }


		/*
		 * Here we have to worry about over-receiving.
		 * If the available quantity is greater than 
                 * what we are bringing in, we're ok, the available 
                 * quantity can be set to that.  If the available 
                 * quanity is less than what we are bringing in,
                 *
		 * We have to determine the over-receipt quantity, UNLESS
		 * this is the last valid line, and we're not checking for 
		 * over-receipt in the IDENTIFY operation.  In this case,
		 * we will apply ALL of the remaining quantity to the last 
		 * valid line.
		 */
		
                if (!checkOverRcp && curLine == validLines)
                {

		    avaqty = untqty;
                    avacatchqty = catchqty;
                    /*
                     * Here we'll just track avaqty since we haven't
                     * encountered the possibility of an overage yet in this
                     * piece of code.
                     */
                    if (checkRimLin)
                    {
                        rim_avaqty = avaqty;
                        rim_avacatchqty = avacatchqty;
                    }
                }
                else
                {
		    avaqty = trnReceiveGetOverReceiveQty(
		                 sqlGetLong(res, row, "expqty"),
				 sqlGetLong(res, row, "idnqty"),
				 sqlGetString(res, row, "supnum"),
				 sqlGetString(res, row, "prtnum"),
				 sqlGetString(res, row, "client_id"));


               	    if (curLine != validLines)
			avaqty = sqlGetLong(res, row, "expqty") -
				 sqlGetLong(res, row, "idnqty");
			

                    /*
                     * If we are calculating rimlin for overage, calculate
                     * how many are available.
                     */
                    if (checkRimLin)
                    {
                        if (rimFound)
                        {
                            rim_avaqty = trnReceiveGetOverReceiveQty(
                                     sqlGetLong(rimres, rimrow, "expqty"),
                                     sqlGetLong(rimres, rimrow, "idnqty"),
                                     sqlGetString(rimres, rimrow, "supnum"),
                                     sqlGetString(rimres, rimrow, "prtnum"),
                                     sqlGetString(rimres, rimrow, "client_id"));
                        }
                        else
                        {
                            rim_avaqty = avaqty;
                        }
                    }
                }

                if (checkRimLin)
                {
                    srvFreeMemory(SRVRET_STRUCT, RimPtr);
                    RimPtr = NULL;
                }

            }

            /* 
             * Set the available quantity to the lessor of the two, if they
             * are both set to non-zero
             */

            if (checkOverRcp)
            {
                if (checkRimLin && checkRcvLin)
                {
                    misTrc(T_FLOW,
                           "Must check both RIMLIN and RCVLIN for overage, "
                           "so basing overage check on the lessor available of "
                           "RCVLIN: %ld and RIMLIN: %ld.",
                           avaqty,
                           rim_avaqty);

                    avaqty = (rim_avaqty < avaqty) ? rim_avaqty : avaqty;
                }
                else if (checkRimLin)
                {
                    misTrc(T_FLOW,
                           "Checking overage against RIMLIN available "
                           "quantity of %ld.",
                           rim_avaqty);
                           
                    avaqty = rim_avaqty;
                }
                else
                {
                    misTrc(T_FLOW,
                           "Checking overage against the RCVLIN available "
                           "quantity of %ld.",
                           avaqty);
                }
            }

	    misTrc(T_FLOW, "Quantity to apply [%ld - catch %f], "
	                   "Available quantity [%ld - catch %f ],"
			   "Line: %s - Subline: %s.", 
			   untqty, catchqty,
                           avaqty, avacatchqty,
			   sqlGetString(res, row, "invlin"),
			   sqlGetString(res, row, "invsln"));

	    /*
	     * The avaqty now represents the amount of available space
	     * left on a receive line.
	     */

	    if (avaqty > 0)
	    {
		if (avaqty < untqty)
		{
                    /*
                     * In this case, if we have catch qty, we're going
                     * to do some guessing, and only update the percentage
                     * of the catch qty that matches the qty that
                     * we are updating.
                     */

                    tmpCatchQty = (avaqty * catchqty) / avaqty;
                    if (tmpCatchQty < avacatchqty)
                        tmpCatchQty = avacatchqty;


                    misTrc(T_FLOW, "Available less than unit qty %d", untqty);
                    sprintf(command,
                            "update rimlin "
                            "   set idnqty = idnqty %s %ld, "
                            "       idn_catch_qty = nvl(idn_catch_qty, 0) %s %f "
                            " where supnum      = '%s' "
                            "   and client_id   = '%s' "
                            "   and invnum      = '%s' "
                            "   and invlin      = '%s' "
                            "   and invsln      = '%s' ",
			    incrementing ? "+" : "-",
                            avaqty,
			    incrementing ? "+" : "-",
                            tmpCatchQty,
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));

                    ret_status = sqlExecStr(command, NULL);
                    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
                    {
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
	                if (CurPtr) 
	                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
                        return (srvResults(ret_status, NULL));
                    }

                    sprintf(command,
                            "update rcvlin "
                            "   set idnqty = idnqty %s %ld, "
                            "       idn_catch_qty = nvl(idn_catch_qty, 0) %s %f " 
                            " where trknum      = '%s' "
                            "   and supnum      = '%s' "
                            "   and client_id   = '%s' "
                            "   and invnum      = '%s' "
                            "   and invlin      = '%s' "
                            "   and invsln      = '%s' "
			    "   and seqnum      = 0",
			    incrementing ? "+" : "-",
                            avaqty,
			    incrementing ? "+" : "-",
                            tmpCatchQty,
                            dstloc,
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));

                    ret_status = sqlExecStr(command, NULL);
                    if (ret_status != eOK)
                    {
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
	                if (CurPtr) 
	                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
                        return (srvResults(ret_status, NULL));
                    }
                    /* Add this to the results structure */

                    if (!strlen(wrk_dtlnum))
                    srvResultsAdd(RetPtr,
		                  sqlGetString(invres, invrow, "dtlnum"),
                                  dstloc,
                                  sqlGetString(res, row, "client_id"),
                                  sqlGetString(res, row, "supnum"),
                                  sqlGetString(res, row, "invnum"),
                                  sqlGetString(res, row, "invlin"),
                                  sqlGetString(res, row, "invsln"));
		    else
			{
                        srvResultsAdd(RetPtr,
		                 wrk_dtlnum,
                                  dstloc,
                                  sqlGetString(res, row, "client_id"),
                                  sqlGetString(res, row, "supnum"),
                                  sqlGetString(res, row, "invnum"),
                                  sqlGetString(res, row, "invlin"),
                                  sqlGetString(res, row, "invsln"));

			if (untqty - avaqty <= 0)
			   memset(wrk_dtlnum, 0, sizeof(wrk_dtlnum));
			}

                    if (curLine < validLines)
			{
 			char FieldList[100];
			char ValueList[100];

			strcpy(FieldList,"dtlnum, untqty");
			ret_status = appNextNum(NUMCOD_DTLNUM, new_dtlnum);
                    	if (ret_status != eOK)
                    	   {
	               	   if (RetPtr)
	                    	srvFreeMemory(SRVRET_STRUCT, RetPtr);
	               	   if (CurPtr) 
	                    	srvFreeMemory(SRVRET_STRUCT, CurPtr);
                           sqlFreeResults(invres);
                           return (srvResults(ret_status, NULL));
                           }

			sprintf(ValueList, "'%s', %d", new_dtlnum, untqty-avaqty);
			ret_status = appGenerateTableEntry("invdtl",
			   invres,
			   invrow,
			   FieldList,
			   ValueList);
                    	if (ret_status != eOK)
                    	   {
	               	   if (RetPtr)
	                    	srvFreeMemory(SRVRET_STRUCT, RetPtr);
	               	   if (CurPtr) 
	                    	srvFreeMemory(SRVRET_STRUCT, CurPtr);
                           sqlFreeResults(invres);
                           return (srvResults(ret_status, NULL));
                           }

			sprintf(command, "update invdtl set untqty = untqty - %d where "
				"dtlnum = '%s'", untqty-avaqty, 
		                  sqlGetString(invres, invrow, "dtlnum"));

                        ret_status = sqlExecStr(command, NULL);
                    	if (ret_status != eOK)
                    	   {
	               	   if (RetPtr)
	                    	srvFreeMemory(SRVRET_STRUCT, RetPtr);
	               	   if (CurPtr) 
	                    	srvFreeMemory(SRVRET_STRUCT, CurPtr);
                           sqlFreeResults(invres);
                           return (srvResults(ret_status, NULL));
                           }
			}
		    untqty = untqty - avaqty;
                    catchqty = catchqty - tmpCatchQty;
		}
		else /* (avaqty >= untqty) */
		{
                    misTrc(T_FLOW, "Available > than unit qty %d", untqty);
                    sprintf(command,
                            "update rimlin "
                            "   set idnqty = idnqty %s %ld, "
                            "       idn_catch_qty = nvl(idn_catch_qty, 0) %s %f "
                            " where supnum      = '%s' "
                            "   and client_id   = '%s' "
                            "   and invnum      = '%s' "
                            "   and invlin      = '%s' "
                            "   and invsln      = '%s' ",
			    incrementing ? "+" : "-",
                            untqty,
			    incrementing ? "+" : "-",
                            catchqty,
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));

                    ret_status = sqlExecStr(command, NULL);
                    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
                    {
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
	                if (CurPtr) 
	                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
                        return (srvResults(ret_status, NULL));
                    }

                    sprintf(command,
                            "update rcvlin "
                            "   set idnqty = idnqty %s %ld, "
                            "       idn_catch_qty = nvl(idn_catch_qty, 0) %s %f "
                            " where trknum      = '%s' "
                            "   and client_id   = '%s' "
                            "   and supnum      = '%s' "
                            "   and invnum      = '%s' "
                            "   and invlin      = '%s' "
                            "   and invsln      = '%s' "
			    "   and seqnum      = 0",
			    incrementing ? "+" : "-",
                            untqty,
			    incrementing ? "+" : "-",
                            catchqty,
                            dstloc,
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));
			
                    ret_status = sqlExecStr(command, NULL);
                    if (ret_status != eOK)
                    {
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
	                if (CurPtr) 
	                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
                        return (srvResults(ret_status, NULL));
                    }

                    /* Add this to the results structure */
                    if (!strlen(wrk_dtlnum))
                    srvResultsAdd(RetPtr,
		                  sqlGetString(invres, invrow, "dtlnum"),
                                  dstloc,
                                  sqlGetString(res, row, "client_id"),
                                  sqlGetString(res, row, "supnum"),
                                  sqlGetString(res, row, "invnum"),
                                  sqlGetString(res, row, "invlin"),
                                  sqlGetString(res, row, "invsln"));
		    else
			{
			needRcvKey = BOOLEAN_TRUE;
                        srvResultsAdd(RetPtr,
		                  wrk_dtlnum,
                                  dstloc,
                                  sqlGetString(res, row, "client_id"),
                                  sqlGetString(res, row, "supnum"),
                                  sqlGetString(res, row, "invnum"),
                                  sqlGetString(res, row, "invlin"),
                                  sqlGetString(res, row, "invsln"));

			}

		    untqty = 0;
                    catchqty = 0;
		}

                /*
		 * If we are incrementing and there is not already a receive 
		 * key associated with this detail, we have to find a line
		 * to associate it with.
		 *
		 * NOTE: The needRcvKey flag makes it so that we only apply
		 * this to the first line that it was identified against.
		 */

		if (incrementing && 
		    /* sqlIsNull(invres, invrow, "rcvkey") && */
		    needRcvKey)
		{
		    misTrc(T_FLOW, "Attempting to find a receive line.");

		    RcvPtr = NULL;
		    sprintf(command,
                            "get matching receive line for receipt "
                            " where dtlnum = '%s' "
                            "   and trknum = '%s' "
                            "   and client_id = '%s' "
                            "   and supnum = '%s' "
                            "   and invnum = '%s' "
                            "   and invlin = '%s' "
                            "   and invsln = '%s' ",
                            strlen(wrk_dtlnum)? wrk_dtlnum: sqlGetString(invres, invrow, "dtlnum"),
                            dstloc,
                            sqlGetString(res, row, "client_id"),
                            sqlGetString(res, row, "supnum"),
                            sqlGetString(res, row, "invnum"),
                            sqlGetString(res, row, "invlin"),
                            sqlGetString(res, row, "invsln"));

	            ret_status = srvInitiateCommand (command, &RcvPtr);
	            if (eOK != ret_status)
		    {
			misTrc(T_FLOW, "Could not find receive line.");
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);

		        if (RcvPtr)
                            srvFreeMemory(SRVRET_STRUCT, RcvPtr);

		        if (CurPtr)
                            srvFreeMemory(SRVRET_STRUCT, CurPtr);

                        sqlFreeResults(invres);

			return(srvResults(ret_status, NULL));
		    }

		    rcvres = srvGetResults(RcvPtr);
		    rcvrow = sqlGetRow(rcvres);

                    sprintf(command,
                            "update invdtl "
                            "   set rcvkey = '%s' "
                            " where dtlnum = '%s'",
                            sqlGetString(rcvres, rcvrow, "rcvkey"),
                            strlen(wrk_dtlnum)? wrk_dtlnum: sqlGetString(invres, invrow, "dtlnum"));

                    if (RcvPtr)
                        srvFreeMemory(SRVRET_STRUCT, RcvPtr);
	
                    ret_status = sqlExecStr(command, NULL);
		    if (eOK != ret_status)
		    {
			misTrc(T_FLOW, "Could not update receive key.");
	                if (RetPtr)
	                    srvFreeMemory(SRVRET_STRUCT, RetPtr);
		        if (CurPtr)
                            srvFreeMemory(SRVRET_STRUCT, CurPtr);
                        sqlFreeResults(invres);
			return(srvResults(ret_status, NULL));
		    }

		    if (strlen(wrk_dtlnum))
			 memset(wrk_dtlnum, 0, sizeof(wrk_dtlnum));
		    needRcvKey = BOOLEAN_FALSE;
		}

	    if (strlen(new_dtlnum))
		strcpy(wrk_dtlnum, new_dtlnum);
	    }

	    row = sqlGetNextRow(row);
	    curLine++;
	}

	if (untqty > 0)
	{
            if (RetPtr)
	       srvFreeMemory(SRVRET_STRUCT, RetPtr);
            if (CurPtr) 
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    sqlFreeResults(invres);
	    if (incrementing)
		return (srvResults(eINT_OVER_RECEIPT_DISALLOWED, NULL));
	    else
		return (srvResults(eOK, NULL));
	}
        if (CurPtr) 
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }
    sqlFreeResults(invres);

    /* returns ok */

    return (RetPtr);
}
