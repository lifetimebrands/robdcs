static const char *rcsid = "$Id: varUnloadShipmentToPrePaperwork.c,v 1.2 2003/01/31 21:15:38 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This function will unload the entire shipment to the 
 *                  staging area and the status will be as if the paperwork
 *                  has not been printed but the stop, car_move and trlr
 *                  will still exist.
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <stdlib.h>



LIBEXPORT 
RETURN_STRUCT  *varUnloadShipmentToPrePaperwork( char *ship_id_i ,
				   char *stoloc_i ) 
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;

    char stoloc[STOLOC_LEN+1];
    char ship_id[SHIP_ID_LEN+1] ;
    char arecod[ARECOD_LEN+1] ;
    char rescod[RESCOD_LEN+1] ;
    char lodnum[LODNUM_LEN + 1] ;
    char stop_id[STOP_ID_LEN + 1] ;

    char buffer[3500] ;

    long ret_status ;
    long stop_count ;
    long car_move_count ;
    
    
    mocaDataRow   *row ;
    mocaDataRes   *res ;

    memset (lodnum, 0, sizeof (lodnum));
    memset (stoloc, 0, sizeof (stoloc));
    memset (ship_id, 0, sizeof (ship_id));
    memset (arecod, 0, sizeof (arecod));
    memset (rescod, 0, sizeof (rescod));
    memset (stop_id, 0, sizeof (stop_id));
    
    
    if ((!ship_id_i || 0 == misTrimLen(ship_id_i, SHIP_ID_LEN)) )
    {
	return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }
    else
    {
        misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN );
    }


    misTrimcpy(stoloc, stoloc_i, STOLOC_LEN );
  
    /* Get the shipment and make sure that it's actually loaded.  */ 

    sprintf(buffer,
		"select *   " 
		"  from shipment "
		" where ship_id = '%s'  ",
		ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    row = sqlGetRow(res);
    strcpy (stop_id, sqlGetString( res, row, "stop_id"), STOP_ID_LEN) ;

    /* Added a small fix to allow the user to unload a shipment that has not
       been dispatched.The check to see if it has ben dispathed is in unload
       shipment. Raman P, MR 4774 */
    if ((strncmp (sqlGetString(res,row,"shpsts"), SHPSTS_LOADED, SHPSTS_LEN ) != 0 )
       && ( strncmp (sqlGetString(res,row,"shpsts"), SHPSTS_LOAD_COMPLETE, SHPSTS_LEN ) != 0 ))
    {
	sqlFreeResults(res);
	return (srvSetupReturn(eINT_INVALID_SHIPMENT,"")) ;
    }

    /* Get the arecod and rescod from the pckmov to determine
     * where the shipment came from and the rescod to see if
     * a location is still assigned to the shipment.
     */ 

    sprintf(buffer,
		"select arecod, seqnum, rescod, stoloc   " 
		"  from pckwrk, pckmov "
		" where ship_id = '%s'  "
		"   and pckwrk.cmbcod = pckmov.cmbcod"
		" order by seqnum desc",
		ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*  Pulling the first record should be sufficient.
    */
    row = sqlGetRow(res);

    strcpy (arecod, sqlGetString( res, row, "arecod"), ARECOD_LEN) ;
    strcpy (rescod, sqlGetString( res, row, "rescod"), RESCOD_LEN) ;

    sprintf(buffer,
		"select stoloc   " 
		"  from locmst "
		" where arecod = '%s'  "
		"   and rescod = '%s'  ",
		arecod, rescod) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }
    else if (ret_status == eDB_NO_ROWS_AFFECTED )
    {
	/*  This means that there is no location allocated to the
         *  rescod and we will have to grab one.
	*/
	CurPtr = NULL;

	sprintf(buffer,
		" list available resource locations where arecod = '%s' ",
		arecod );

	ret_status = srvInitiateCommand (buffer, &CurPtr);
	
	if (ret_status !=eOK) 
	{
	    sqlFreeResults(res);
	    srvFreeMemory(SRVRET_STRUCT, CurPtr );
	    return (srvSetupReturn(ret_status,"")) ;
	}

	/*  Grab the first available location.
	*/
	res = srvGetResults(CurPtr);
        row = sqlGetRow(res);

        strcpy (stoloc, sqlGetString( res, row, "stoloc"), STOLOC_LEN) ;

        srvFreeMemory(SRVRET_STRUCT, CurPtr );

        /*  Allocate the location to the resource code.
        */
        returnData = NULL;

        sprintf(buffer,
		" allocate resource location where arecod = '%s' "
		"  and stoloc = '%s'  and ship_id = '%s' " ,
	    arecod , 
	    stoloc ,
	    ship_id);

        ret_status = srvInitiateCommand (buffer, NULL);
    
        if (ret_status !=eOK) 
        {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

    }
    else
    {
	/*  This means that there is a location is still allocated to the
         *  rescod and we will use that one.
	*/
        row = sqlGetRow(res);
        strcpy (stoloc, sqlGetString( res, row, "stoloc"), STOLOC_LEN) ;
    }


    /*  Find all the loads that are on that shipment.  This assumes
     *  that there is only one shipment on the lodnum...BIG ASSUMPTION.
    */
    sprintf(buffer,
		"select distinct invlod.lodnum   " 
		"  from invlod, invsub, invdtl "
		" where invlod.lodnum = invsub.lodnum  "
		"   and invsub.subnum = invdtl.subnum  "
		"   and invdtl.ship_line_id in "
		"   ( select ship_line_id from shipment_line "
		"      where ship_id = '%s' ) ",
		ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if ((ret_status != eOK) )
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /* Move each load into the new staging lane.
    */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	strcpy (lodnum, sqlGetString( res, row, "lodnum")) ;


	sprintf(buffer,
		" move inventory where lodnum = '%s' "
		"  and dstloc = '%s'  "
		"  and newdst = 0  "
		"  and spcind = '%s'  " ,
		lodnum , 
		stoloc , 
		SPCIND_PRA_REVERSE );

	ret_status = srvInitiateCommand (buffer, NULL);
	
	if (ret_status !=eOK) 
        {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }


    }



    /*******************************************************************/
    /*  Update the shipment shpsts from D->either P or S
     *                  and loddte is blank
     *    DO NOT BLANK THE STOP_ID B/C IT'S STILL VALID.
    */
    sprintf(buffer,
		"update shipment   " 
		"   set shpsts = '%s', loddte = NULL "
		" where ship_id = '%s'  ",
		SHPSTS_STAGED, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /********************************************************************
     *  Update the shipment_line linsts from C->I 
     *                  and shpqty = 0
     *			and stgqty = shpqty
    */
    sprintf(buffer,
		"update shipment_line   " 
		"   set linsts = '%s', stgqty = shpqty, shpqty = 0 "
		" where ship_id = '%s'  ",
		LINSTS_INPROCESS, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /*  Update the ord_line shpqty = 0
    */
    sprintf(buffer,
		" [ select ordnum, ordlin, ordsln, client_id "
		"     from shipment_line "
		"    where ship_id = '%s' ]  | "
		" [ update ord_line   " 
		"  set shpqty = 0 "
		" where ordnum = @ordnum  "
		"   and ordlin = @ordlin  "
		"   and ordsln = @ordsln  "
		"   and client_id  = @client_id   ",
		ship_id) ;

    ret_status = srvInitiateCommand (buffer, NULL);
	
    if (ret_status !=eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /********************************************************************
    /*  Update the pckwrk pcksts from C->R 
    */
    sprintf(buffer,
		"update pckwrk   " 
		"   set pcksts = '%s' "
		" where ship_id = '%s'  ",
		PCKSTS_RELEASED, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /********************************************************************
    /*  Update the pckmov stoloc blank->stoloc that has bee allocated
    */
    sprintf(buffer,
		"update pckmov "
		"   set stoloc = '%s' "
		" where arecod = '%s' "
		"   and cmbcod in ( select cmbcod "
		"      from pckwrk "
		" where ship_id = '%s' ) ",
		stoloc, arecod, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /********************************************************************
    /*  Update the pckbat batsts CMPL->REL
     *			  cmpdte DATE->blank
    */
    sprintf(buffer,
		"update pckbat "
		"   set batsts = '%s', cmpdte = NULL "
		" where schbat = ( select schbat "
		"      from pckwrk "
		" where ship_id = '%s' and rownum < 2 ) ",
		BATSTS_REL, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }


    /********************************************************************
    /*  Leave the car_move, trlr alone.  Update the doc_num and track_num
     *  to be NULL...as if paperwork was never printed.
    */

    /********************************************************************
    /*  Update the stop doc_num, track_num -> blank
     *  If there is more than one shipment on the stop remove the doc_num
     *  and the track_num and unload the shipments to a pre-printed
     *  paperwork state.
    */
        sprintf(buffer,
		"update stop "
		"   set doc_num = NULL, track_num = NULL "
		" where stop_id = '%s' ",
		    stop_id) ;

        ret_status = sqlExecStr(buffer, NULL) ;

        if (ret_status != eOK)
        {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

    	sprintf(buffer, "update trlr set trlr_stat = '%s'"
		    " where trlr_id in "
		    " (select trlr_id from stop, car_move "
		    " where stop_id = '%s' "
		    " and stop.car_move_id = car_move.car_move_id) ",
		    TRLSTS_OPEN, stop_id) ;

    	    ret_status = sqlExecStr(buffer, NULL) ;
    
    	    if (ret_status != eOK) {
		    sqlFreeResults(res);
		    return (srvSetupReturn(ret_status,"")) ;
    	    }


    return (srvSetupReturn(ret_status,"")) ;
}


	

