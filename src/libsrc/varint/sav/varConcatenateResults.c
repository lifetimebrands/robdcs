static const char *rcsid = "$Id: varConcatenateResults.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This function takes in a string that is a select statement
 * 	of a single column and concatenates the results with comma separation. 
 *
 *#END************************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <varerr.h>

LIBEXPORT 
RETURN_STRUCT  *varConcatenateResults( char *colnam_i, 
					char *tblnam_i,
					char *whrcls_i )
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;


    long  ret_status ;
    long  status  ;

    mocaDataRow   *cur_row ,  *case_row, *row ,*row1;
    mocaDataRes   *res, *res1, *resdtl ,*ressub, *reswrk;

    char            datatypes[20];
    char buffer[3500];
    char buffer2[3500];
    char ValueList[3500]; 
    char selstring[300 + 1];
    char rtnstring[3000 + 1];
    char colnam[20 + 1];	/* column name to be retrieved */
    char tblnam[20 + 1];	/* table name to be retrieved */
    char whrcls[100 + 1];	/* where clause to use in select */
    char coldat[20 + 1];	/* column data returned */
    char collst[300 + 1];	/* list of comma separated concat data to return */
    long i;

    CurPtr = NULL;
    memset (coldat, 0, sizeof (coldat));
    memset (collst, 0, sizeof (collst));	/* return list of concatenated data*/

    if ((!colnam_i || 0 == misTrimLen(colnam_i, 20)) ) {
        return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }
    else {
        misTrimcpy(colnam, colnam_i, 20 );
    }

    if ((!tblnam_i || 0 == misTrimLen(tblnam_i, 20)) ) {
        return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }
    else {
        misTrimcpy(tblnam, tblnam_i, 20 );
    }

    if ((!whrcls_i || 0 == misTrimLen(whrcls_i, 100)) ) {
        return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }
    else {
        misTrimcpy(whrcls, whrcls_i, 100 );
    }

    sprintf(buffer, "select %s from %s %s",
		colnam, tblnam, whrcls);

    ret_status = sqlExecStr(buffer, &res) ;

    if ((ret_status != eOK) ) {
        sqlFreeResults(res);
        return (srvSetupReturn(ret_status,"")) ;
    }

    for (cur_row = sqlGetRow(res); cur_row;cur_row = sqlGetNextRow(cur_row)) {
	strcpy (coldat, sqlGetString(res,cur_row,colnam));
	strcat (collst, coldat);
	strcat (collst, ",");
	memset (coldat, 0, sizeof (coldat));
    }
	
    sqlFreeResults(res);

    memset(datatypes, 0, sizeof(datatypes));    
    srvSetupColumns(1);
    i = 0;
    datatypes[i] = srvSetColName(i + 1, "collst", COMTYP_CHAR, 
		misTrimLen(collst,sizeof(collst)));
    i++;

    CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK, datatypes, collst);
    return (CurPtr);      
}
