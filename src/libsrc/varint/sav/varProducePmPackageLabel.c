/*#START***********************************************************************
 *
 * Copyright (c) 2004 RedPrairie Corporation.  All rights reserved.
 * 
 *#END************************************************************************/

#include "pmlib.h"

#define MSG_HDR "varint: (varProducePmPackageLabel) "

LIBEXPORT
RETURN_STRUCT *varProducePmPackageLabel
    (char *crtnid_i, char *invtid_i, char *prnprt_i, char *fmtcod_i,
     long *seqnum_i, long *seqtot_i, long *bndflg_i, char *prtadr_i)
{
    RETURN_STRUCT   *resultData;

    mocaDataRes	    *res;
    mocaDataRow	    *row;

    FILE    *fp_out;

    char    buffer [PM_BUFFER_LEN];
    char    crtnid [CRTNID_LEN + 1];
    char    invtid [LODNUM_LEN + 1];
    char    prnprt [PM_PRINTER_PORT_LEN + 1];
    char    fmtcod [PM_FORMAT_CODE_LEN + 1];
    char    filnam [FILNAM_LEN + 1];
    char    srvcod [PM_SERVICE_CODE_LEN + 1];
    char    carcod [CARCOD_LEN + 1];
    char    srvlvl [SRVLVL_LEN + 1];
    char    mfsmsn [MFSMSN_LEN + 1];
    char    rptnum [NUMCOD_LEN + 1];
    char    varbuf [1024];
    char    dstnam [DSTNAM_LEN + 1];
    char    prtadr [PRTADR_LEN + 1];
    char    loc_id [LOC_ID_LEN + 1];
    char    prtque [PRTQUE_LEN + 1];
    char    mansts [MANSTS_LEN + 1];
    char    *lbldat;

    long    status;
    long    seqnum;
    long    seqtot;
    long    bndflg;
    long    lbllen;
    long    serv_prnt_lblflg;

    /* Load all arguments into internal argument structure */

    memset (crtnid, 0, sizeof(crtnid));
    memset (invtid, 0, sizeof(invtid));
    memset (prnprt, 0, sizeof(prnprt));
    memset (fmtcod, 0, sizeof(fmtcod));
    memset (dstnam, 0, sizeof(dstnam));
    memset (prtadr, 0, sizeof(prtadr));
    memset (srvlvl, 0, sizeof(srvlvl));
    memset (mansts, 0, sizeof(mansts));
    seqnum = 0;
    seqtot = 0;
    bndflg = 0;
    
    if (crtnid_i && strlen (crtnid_i))
        misTrimcpy (crtnid, crtnid_i, sizeof(crtnid));

    if (invtid_i && strlen (invtid_i))
        misTrimcpy (invtid, invtid_i, sizeof(invtid));

    if (prnprt_i && strlen (prnprt_i))
        misTrimcpy (prnprt, prnprt_i, sizeof(prnprt));

    if (fmtcod_i && strlen (fmtcod_i))
        misTrimcpy (fmtcod, fmtcod_i, sizeof(fmtcod));

    if (prtadr_i && strlen (prtadr_i))
        misTrimcpy (prtadr, prtadr_i, sizeof(prtadr));

    if (seqnum_i && *seqnum_i)
	seqnum = *seqnum_i;      

    if (seqtot_i && *seqtot_i)
	seqtot = *seqtot_i;      

    if (bndflg_i && *bndflg_i)
	bndflg = *bndflg_i;      

    /* Get Parcel Manifest package identification information */

    status = pmGetPackageIdInfo (carcod, mfsmsn, crtnid, invtid, NULL, FALSE,
                                 dstnam, mansts);
    if (status != eOK)
    {
	misTrc (T_FLOW, "Error %ld getting package (%s/%s) identification " 
                           "information", status, crtnid, invtid);
	return (srvResults (status, NULL));
    }

    /* Translate DCS carrier code into Parcel Manifest service code */

    status = pmTranslateIntoCarrierCode (srvcod, carcod);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
	misTrc(T_FLOW, "Error %ld translating DCS carrier (%s)", 
                           status, carcod);
	return (srvResults (status, NULL));
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
	misTrc(T_FLOW, "DCS carrier (%s) does not translate into "
                           "Parcel Manifest service code", carcod);
	return (srvResults (status, NULL));
    }

    /* Retrieve carcod & srvlvl */
    sprintf(buffer,
            "select carcod, "
            "       srvlvl "
            "  from manfst "
            " where crtnid = '%s' ",
            crtnid);

    status = sqlExecStr(buffer, &res);
    if (status != eOK)
    {
        sqlFreeResults(res);
        if (status == eDB_NO_ROWS_AFFECTED)
            return (APPInvalidArg(crtnid, "crtnid"));
        else
            return (srvResults(status, NULL));
    }
    row = sqlGetRow(res);
    misTrimcpy(carcod, sqlGetString(res, row, "carcod"), CARCOD_LEN);
    misTrimcpy(srvlvl, sqlGetString(res, row, "srvlvl"), SRVLVL_LEN);
    sqlFreeResults(res);

    /* Figure out the format for the carrier/service level.  It may be passed
     * in, but that is not likely by project teams 
     */

    if (!strlen(fmtcod)) 
    {

        /* Select against the policies to determine the label format */

        sprintf (buffer,
                 "select rtnum2, rtstr2, rtflt1 "
                 "  from poldat "
                 "  where rtstr2 is not null "
                 "    and rtnum1 is not null "
                 "    and polcod = '%s' "
                 "    and polvar = '%s' "
                 "    and polval = '%s'||'_'||'%s' ",
		 POLCOD_SMLPKGOPR,
		 POLVAR_LABELS,
                 carcod,
                 srvlvl);

        status = sqlExecStr (buffer, &res);
        if (status == eDB_NO_ROWS_AFFECTED)
        {
            sprintf (buffer,
                     "select rtnum2, rtstr2, rtflt1 "
                     "  from poldat "
                     "  where rtstr2 is not null "
                     "    and rtnum1 is not null "
                     "    and polcod = '%s' "
                     "    and polvar = '%s' "
                     "    and polval = '%s' ",
                     POLCOD_SMLPKGOPR,
                     POLVAR_LABELS,
                     carcod);

            sqlFreeResults(res);
            status = sqlExecStr (buffer, &res);
            
            if (status == eDB_NO_ROWS_AFFECTED)
            {
                sprintf (buffer,
                         "select rtnum2, rtstr2, rtflt1 "
                         "  from poldat "
                         "  where rtstr2 is not null "
                         "    and rtnum1 is not null "
                         "    and polcod = '%s' "
                         "    and polvar = '%s' "
                         "    and polval = 'DEFAULT' ",
                         POLCOD_SMLPKGOPR,
                         POLVAR_LABELS);
            
                sqlFreeResults(res);
                status = sqlExecStr (buffer, &res);
            }

        }

        if (status != eOK) 
	{
            misTrc(T_FLOW, 
		      "DCS carrier (%s) does not translate into format code", 
		      carcod);
            sqlFreeResults(res);
            return (srvResults (status, NULL));
        }
        row = sqlGetRow (res);

        sprintf (prnprt, "%ld",  sqlGetLong(res, row, "rtnum2"));

        if (!sqlIsNull(res, row, "rtflt1"))
        {
            bndflg = sqlGetLong(res, row, "rtflt1");
        }

        misTrimcpy(fmtcod,
                   sqlGetString(res, row, "rtstr2"), RTSTR2_LEN);

        sqlFreeResults (res);

        misTrc(T_FLOW, "Format code - (%s), Printer Port (%s)", fmtcod, prnprt);
    }

    sprintf(buffer, 
            "select carcod, "
            "       srvlvl "
            "  from manfst "
            " where crtnid = '%s' ",
            crtnid);

    status = sqlExecStr(buffer, &res);
    if (status != eOK)
    {
        sqlFreeResults(res);
        if (status == eDB_NO_ROWS_AFFECTED)
            return (APPInvalidArg(crtnid, "crtnid"));
        else
            return (srvResults(status, NULL));
    }
    row = sqlGetRow(res);
    misTrimcpy(carcod, sqlGetString(res, row, "carcod"), CARCOD_LEN);
    misTrimcpy(srvlvl, sqlGetString(res, row, "srvlvl"), SRVLVL_LEN);
    sqlFreeResults(res);

    /* SI BRP 5-25-2005 Make sure we are using the Tandata system */ 
    sprintf(dstnam, "TANDATA");

    sprintf(buffer,
            "   select serv_prnt_lblflg  "
            "     from carxrf            "
            "    where carcod = '%s' "
            "      and srvlvl = '%s' "
            "      and dstnam = '%s' ",
            carcod,
            srvlvl,
            dstnam);
    
    status = sqlExecStr(buffer, &res);

    if (status != eOK) 
    {
        sqlFreeResults(res);
        if (status == eDB_NO_ROWS_AFFECTED)
            return (APPInvalidArg(carcod, "carcod"));
        else 
            return (srvResults(status, NULL));
    }

    row = sqlGetRow(res);

    serv_prnt_lblflg = sqlGetLong(res, row, "serv_prnt_lblflg");
    sqlFreeResults(res);


    /*
     * If the server prints label is set to true, that means the 
     * destination (Fedex, Fascor, Connectship) will take care of
     * printing the label.
     */

    if (serv_prnt_lblflg == 1)
    {
        if (strcmp(dstnam, DSTNAM_FEDEX) == 0)
        {
            /*
             * Currently, you can not reprint a Fedex label, because
             * the Fedex ship manager does not support it.
             */

            return (srvResults(eFEDEX_DOES_NOT_SUPPORT_SERVICE, NULL));
        }
        else if (strcmp(dstnam, DSTNAM_FASCOR) == 0)
        {
            /* Get label printer to print to based on device */

            /* Format command */
        
            sprintf(buffer,
                    " publish data "
                    "   where prtadr = '%s' "
                    " | "
                    " if (!@prtadr) "
                    " { "
                    "     [select lbl_prtadr prtadr         "
                    "        from devmst                    "
                    "       where devmst.devcod = @@devcod] "
                    " } "
                    " |                                  "
                    " [select prtque                     "
                    "    from prsmst                     "
                    "   where prtadr = @prtadr]          ",
                    prtadr);
        
            status = srvInitiateCommand(buffer, &resultData);

            if (status != eOK)
            {
        	srvFreeMemory (SRVRET_STRUCT, resultData);
                return (srvResults(status, NULL));
            }

            /* Get printer queue */
            res = srvGetResults(resultData);
            row = sqlGetRow(res);

            misTrimcpy(prtque, sqlGetString(res, row, "prtque"),
                       PRTQUE_LEN);

            misTrc(T_FLOW, "Print queue is: %s", prtque);
            srvFreeMemory(SRVRET_STRUCT, resultData);


            /* Obtain the fascor location */

            /* Format command */
        
            sprintf(buffer,
                    " publish data "
                    "   where mfsmsn = '%s' "
                    " | "
                    " [select ordnum, client_id "
                    "    from manfst "
                    "   where mfsmsn = @mfsmsn] catch(-1403)  "
                    " | "
        	    " list pm fascor locations ",
        	    mfsmsn);
        
            /* Invoke command */
        
            status = srvInitiateCommand (buffer, &resultData);
            if (status != eOK)
            {
        	srvFreeMemory (SRVRET_STRUCT, resultData);
                return (srvResults(status, NULL));
            }
        
            /* Get fascor location */
            res = srvGetResults(resultData);
            row = sqlGetRow(res);

            misTrimcpy(loc_id, sqlGetString(res, row, "loc_id"),
                       LOC_ID_LEN);

            srvFreeMemory(SRVRET_STRUCT, resultData);

            /* Produce the fascor package label */

            sprintf(buffer,
        	    " get pm fascor configuration "
                    " |                "
		    " if (@installed = 1) "
		    " { "
                    "     get pm custom where action='%s' "
                    "     |                "
        	    "     remote (@pm_remote_host) produce fascor package label "
                    "         where loc_id='%s' "
                    "         and prtque='%s' "
                    "         and mfsmsn='%s' "
                    "         and srvcod = '%s' "
		    " } ",
        	    PM_ACTION_PRODUCE_PACKAGE_LABEL,
                    loc_id,
                    prtque,
                    mfsmsn,
                    srvcod);
        
            /* Invoke command */
        
            status = srvInitiateCommand (buffer, NULL);
            if (status != eOK)
            {
                return (srvResults(status, NULL));
            }
        }
        else
        {
            /*
             * We are printing a TANDATA label.
             */
        
        
            sprintf(buffer,
                    " publish data "
                    " where service_code = '%s' "
                    "   and mfsmsn = '%s' "
                    "   and prnprt = '%s' "
                    "   and fmtcod = '%s' "
                    "   and seqnum = '%ld' "
                    "   and seqtot = '%ld' "
                    "   and bndflg = '%'d' "
                    " | "
        	    " get pm tandata configuration "
                    " | "
		    " if (@installed = 1) "
		    " { "
                    "     get pm custom where action='%s' "
                    "     | "
        	    "     remote (@pm_remote_host) produce parcel package label "
		    " } ",
        	    srvcod,
                    mfsmsn,
                    prnprt,
                    fmtcod,
                    seqnum,
                    seqtot,
                    bndflg,
        	    PM_ACTION_PRODUCE_PACKAGE_LABEL);
        
            /* Invoke Parcel Manifest command */
        
            status = srvInitiateCommand (buffer, &resultData);
            if (status != eOK)
            {
        	srvFreeMemory (SRVRET_STRUCT, resultData);
                return (srvResults(status, NULL));
            }
        
            /* Get package label file name */
        
            status = appNextNum (NUMCOD_RPTNUM, rptnum);
            if (status != eOK)
            {
        	srvFreeMemory (SRVRET_STRUCT, resultData);
                return (srvResults (status, NULL));
            }
            sprintf (filnam, "%s%sPmPackageLabel_%s.dat",
        	misExpandVars (varbuf, DCS_REPORTS, sizeof (varbuf), NULL),
        	PATH_SEPARATOR_STR, rptnum);
        
            /* Open (create) package label file */
        
            fp_out = fopen (filnam, "wb");
            if (!fp_out)
            {
        	pmLogMsg (MSG_HDR, "Error %ld opening package label file (%s)", 
                                   errno, filnam);

        	srvFreeMemory (SRVRET_STRUCT, resultData);
        	return (srvResults (eSRV_UNEXPECTED_ERROR, NULL));
            }
        
            /* Write package label to package label file */
        
            res = srvGetResults(resultData);
            for (row = sqlGetRow (res); 
                             row; row = sqlGetNextRow (row))
            {
        	lbldat = sqlGetBinaryData(res, row,"label_data");
        	lbllen = sqlGetBinaryDataLen(res, row,"label_data");
        
        	status = fwrite (lbldat, 1, lbllen, fp_out);
        	if (status != lbllen)
        	{
        	    pmLogMsg (MSG_HDR, "Error %ld writing to package label "
                                       "file (%s)", errno, filnam);
        	    fclose (fp_out);
        	    srvFreeMemory (SRVRET_STRUCT, resultData);
        	    return (srvResults (eSRV_UNEXPECTED_ERROR, NULL));
        	}
            }
        
            /* Close package label file */
        
            fclose (fp_out);
          
	    /* SI-BRP 5/3/2005 */
	    /* Standard Product has a bug in this code where */
	    /* it is not returning any data.  For the labels */
	    /* to correctly print from Tandata using the print label */
	    /* trigger, this command must return the filnam */

 	    /* Initialize Moca return data set */
 	    resultData = srvResultsInit (eOK,
 	    	"filnam", COMTYP_CHAR, FILNAM_LEN,
 	    	NULL);

        
 	    /* Move information into Moca return data set */

        
 	    srvResultsAdd (resultData,
		filnam,
		NULL); 

     	    pmLogMsg(MSG_HDR, "Normal successful completion");
            return (resultData);
        }
    }
    else
    {
        sprintf(buffer,
                " publish data "
                "   where prtadr = '%s' "
                " | "
                " if (!@prtadr) "
                " { "
                "     [select lbl_prtadr prtadr "
                "        from devmst                     "
                "       where devmst.devcod = @@devcod] "
                " } "
                " |                                  "
                " [select dstcar srvnam              "
                "    from carxrf,                    "
                "         manfst                     "
                "   where manfst.crtnid = '%s'       "
                "     and manfst.carcod = carxrf.carcod "
                "     and manfst.srvlvl = carxrf.srvlvl "
                "     and rownum < 2]                "
                " |                                  "
                " [select parval lblfmt              "
                "    from prcl_par_data              "
                "   where srvnam = @srvnam           "
                "     and parnam = 'LBLFMT']         "
                " |                                  "
                " get label format                   "
                "     where format = @lblfmt         "
                "       and crtnid = '%s'            "
                " |                                  "
                " produce label                      ",
                prtadr, crtnid, crtnid);
        
        status = srvInitiateCommand(buffer, NULL);

        if (status != eOK)
        {
            return (srvResults(status, NULL));
        }
    }

    return srvResults(eOK, NULL);
}
