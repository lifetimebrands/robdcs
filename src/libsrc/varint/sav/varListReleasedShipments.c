static char    *rcsid = "$Id: varListReleasedShipments.c,v 1.10 2003/08/21 21:40:08 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2000
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  
 *  Roman Barykin 01/16/2002 Added rtcust and cponum as additional input
 *  parameters.
 *
 *  Khairul Zainal 01/23/2002 MR1296, MR1671 - Added vc_extdte, early_shpdte
 *  late_shpdte as additional output.
 *  MR1713 - Wildcard operation for stcust.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <vargendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>

static void _intListRS_AddResults(RETURN_STRUCT ** CurPtr,
				  long arrqty,
				  long pckqty,
				  long appqty,
				  long remqty,
				  mocaDataRes * res,
				  mocaDataRow * row);

static void _processUnalloc(RETURN_STRUCT ** CurPtr,
			    mocaDataRow ** returnHolder,
			    mocaDataRes * res,
			    char *ship_id);

static void sProcessXDock(RETURN_STRUCT ** CurPtr,
			  mocaDataRow ** returnHolder,
			  mocaDataRes * res,
			  char *ship_id,
			  char *curloc);

static RETURN_STRUCT *sSetupEmptyReturn(long ret_status)
{
    RETURN_STRUCT  *CmdRes;

    CmdRes = srvResultsInit(ret_status,
			    "ship_id",   COMTYP_CHAR, SHIP_ID_LEN,
			    "stoloc",    COMTYP_CHAR, STOLOC_LEN,
			    "stcust",    COMTYP_CHAR, STCUST_LEN,
			    "adrnam",    COMTYP_CHAR, ADRNAM_LEN,
			    "carcod",    COMTYP_CHAR, CARCOD_LEN,
			    "srvlvl",    COMTYP_CHAR, SRVLVL_LEN,
			    "arrqty",    COMTYP_CHAR, 14,
			    "appqty",    COMTYP_CHAR, 14,
			    "pckqty",    COMTYP_CHAR, 14,
			    "remqty",    COMTYP_CHAR, 14,
			    "doc_num",   COMTYP_CHAR, DOC_NUM_LEN,
			    "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
			    "ordnum",    COMTYP_CHAR, ORDNUM_LEN,
			    "rtcust",    COMTYP_CHAR, RTCUST_LEN,
			    "cponum",    COMTYP_CHAR, CPONUM_LEN,
//LHSTART-kzainal - initialize or set up empty return result.
			    "early_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			    "late_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			    "vc_extdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
//LHSTOP-kzainal 
			    NULL);

    return (CmdRes);

}

LIBEXPORT 
RETURN_STRUCT *varListReleasedShipments(char *arecod_i, char *stcust_i, 
			   		char *carcod_i, char *srvlvl_i, 
					char *ship_id_i,char *client_id_i,
			       		char *ordnum_i, char *list_type_i,
                                        char *rtcust_i, char *cponum_i)
{

    RETURN_STRUCT  *CurPtr, *ArePtr;
    RETURN_STRUCT  *Cmdres;
    char            buffer[4000];
    long            FlushResults;
    int             ret_status;
    mocaDataRes    *res, *res2;
    mocaDataRow    *row, *returnHolder, *prevRow;
    long            arrqty, pckqty, appqty;
    long            tmp_appqty, tmp_pckqty, tmp_arrqty;
    long	    rem_arrqty = 0;
    char            save_ship_id[SHIP_ID_LEN + 1];
    char            save_curloc[STOLOC_LEN + 1];
    char            arecod[ARECOD_LEN + 1];
    char            stcust_string[100], carcod_string[100], srvlvl_string[100];
    char            ship_id_string[100];
    char            client_id_string[100];
    char            ordnum_string[100];
    char            save_ordkey[CLIENT_ID_LEN + ORDNUM_LEN + 1];
    char            save_cmbcod[CMBCOD_LEN+1];
    char            order_by_clause[RTSTR1_LEN + 100];
    char	    arecod_list[1000];
    char            like_clause[500];
    char            list_type[100];

    char            rtcust_string[100];
    char            cponum_string[100];
    short           passed_in_ordinfo = FALSE;

    SRV_ARGSLIST    *args;
    char            name[ARGNAM_LEN + 1];
    char            dtype;
    void            *data;
    int             oper;
    char            tmpvar[100];

    long            unalloc = 0;
    static short    PolXDockInstalled = FALSE;

    CurPtr = NULL;
    returnHolder = NULL;

    memset(arecod, 0, sizeof(arecod));
    memset(stcust_string, 0, sizeof(stcust_string));
    memset(carcod_string, 0, sizeof(carcod_string));
    memset(srvlvl_string, 0, sizeof(srvlvl_string));
    memset(ship_id_string, 0, sizeof(ship_id_string));
    memset(client_id_string, 0, sizeof(client_id_string));
    memset(ordnum_string, 0, sizeof(ordnum_string));
    memset(like_clause, 0, sizeof(like_clause));
    memset(list_type, 0, sizeof(list_type));

    memset(rtcust_string, 0, sizeof(rtcust_string));
    memset(cponum_string, 0, sizeof(cponum_string));

    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
	misTrimcpy(arecod, arecod_i, ARECOD_LEN);
    if (stcust_i && misTrimLen(stcust_i, ADRNUM_LEN))
    {
	sprintf(stcust_string, " and ord.stcust = '%.*s' ",
		(int) misTrimLen(stcust_i, ADRNUM_LEN), stcust_i);
    }
    if (carcod_i && misTrimLen(carcod_i, CARCOD_LEN))
	sprintf(carcod_string, " and shipment.carcod = '%.*s' ",
		(int) misTrimLen(carcod_i, CARCOD_LEN), carcod_i);
    if (srvlvl_i && misTrimLen(srvlvl_i, SRVLVL_LEN))
	sprintf(srvlvl_string, " and shipment.srvlvl = '%.*s' ",
		(int) misTrimLen(srvlvl_i, SRVLVL_LEN), srvlvl_i);
    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
	sprintf(ship_id_string, " and shipment.ship_id = '%s' ",
		ship_id_i);
    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
	sprintf(client_id_string, " and ord.client_id = '%.*s' ",
		(int) misTrimLen(client_id_i, CLIENT_ID_LEN), client_id_i);
    if (ordnum_i && misTrimLen(ordnum_i, ORDNUM_LEN))
    {
	sprintf(ordnum_string, " and ord.ordnum = '%.*s' ",
		(int) misTrimLen(ordnum_i, ORDNUM_LEN), ordnum_i);
        passed_in_ordinfo = TRUE;
    }
    if (list_type_i && misTrimLen(list_type_i, 100))
	misTrimcpy(list_type, list_type_i, 100);

    if (rtcust_i && misTrimLen(rtcust_i, ADRNUM_LEN))
    {
	sprintf(rtcust_string, " and ord.rtcust = '%.*s' ",
		(int) misTrimLen(rtcust_i, ADRNUM_LEN), rtcust_i);
    }
    if (cponum_i && misTrimLen(cponum_i, CPONUM_LEN))
    {
	sprintf(cponum_string, " and ord.cponum = '%.*s' ",
		(int) misTrimLen(cponum_i, CPONUM_LEN), cponum_i);
        passed_in_ordinfo = TRUE;
    }


    /* 
     * Since the above arguments will only get passed into the function if
     * the operator was an "=", see if the command had a like clause in the
     * where clause, if so, we'll want to use it.
     */

    args = NULL;
    while (eOK == srvEnumerateArgList(&args, name, &oper, &data, &dtype))
    {
	memset (tmpvar, 0, sizeof(tmpvar));

	/* If it's not an equal sign */
	/* We will look for specific column names, because when creating the
	 * where clause, we will have to specify a table name. 
	 */

	if (oper == OPR_LIKE) 
        {
	    if (!misCiStrcmp(name, "ship_id"))
	    {
	        sprintf(tmpvar, " and shipment.%s like '%s' ", 
		                name, (char *)data);
	        strcat(like_clause, tmpvar);
	    }
	    else if (!misCiStrcmp(name, "ordnum"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
                passed_in_ordinfo = TRUE;
	    }
	    else if (!misCiStrcmp(name, "rtcust"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
	    }
	    else if (!misCiStrcmp(name, "cponum"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
                passed_in_ordinfo = TRUE;
	    }
//LHSTART- MR1713 - kzainal - wildcard for stcust.
	    else if (!misCiStrcmp(name, "stcust"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
	    }
//LHSTOP-kzainal 
	}
    }
    srvFreeArgList(args);
    ret_status = eOK;

    /*
    ** Check if cross-docking is installed
    */
    if (appIsInstalled(POLCOD_CROSS_DOCKING))
    {
	PolXDockInstalled = TRUE;
    }

    /* Pull the sort by clause from the policies */
    memset(order_by_clause, 0, sizeof(order_by_clause));
    sprintf(order_by_clause,
	    "order by pckwrk.ship_id, pckmov.arecod, "
	    "pckmov.stoloc, pckwrk.client_id, pckwrk.ordnum");
    sprintf(buffer,
	    "select rtstr1 "
	    "  from poldat "
	    " where polcod = '%s' "
	    "   and polvar = '%s' "
	    "   and polval = '%s' ",
	    POLCOD_SORT_BY_CLAUSES,
	    "LIST",
	    "RELEASED SHIPMENTS");
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
	row = sqlGetRow(res);
	memset(order_by_clause, 0, sizeof(order_by_clause));
	if (misTrimLen((char *) sqlGetValue(res, row, "rtstr1"), RTSTR1_LEN))
	    sprintf(order_by_clause,
		    "order by %.*s ",
		    misTrimLen((char *) sqlGetValue(res, row, "rtstr1"),
			       RTSTR1_LEN),
		    (char *) sqlGetValue(res, row, "rtstr1"));
    }
    sqlFreeResults(res);

    /* If we didn't get a specific arecod to look at, find out what areas
     * we're processing for and build an 'in' list.
     * The list type argument allows us to specify that we want to list
     * released shipments for a specific group of areas.  For example, if
     * want to only list those for Ship Dock Operations, we would set the 
     * type = 'SHIP DOCK'.
     * If no area code was passed in, and no type was passed in, we'll
     * list released shipments for all staging areas.
     */
    if (strlen(arecod) == 0) 
    {
    	ArePtr = NULL;
	memset (arecod_list, 0, sizeof(arecod_list));
	misTrc(T_FLOW, "No area code passed, building a list of areas.");

	if (strlen(list_type))
	{
	    misTrc(T_FLOW, "List type is: %s ", list_type);
	    if (strcmp(list_type, "SHIP DOCK") == 0)
	    {
		misTrc(T_FLOW, "List released shipments for staging areas only");
                sprintf (buffer, 
		         "list shipment staging areas");
	    }
	    else
	    {
		return (APPInvalidArg(list_type, "list_type"));
	    }
	}
	else
	{
	    sprintf(buffer,
		    "list areas where aremst.stgflg = 1");
	}

	ret_status = srvInitiateInline(buffer, &Cmdres);
	if (eOK != ret_status) 
	{
	    srvFreeMemory(SRVRET_STRUCT, Cmdres);
	    CurPtr = sSetupEmptyReturn(ret_status);
	    return (CurPtr);
	}
	res2 = Cmdres->ReturnedData;
	for (row = sqlGetRow(res2); row; row = sqlGetNextRow(row))
	{
	    if (strlen(arecod_list))
		strcat(arecod_list, ",'");
	    else
		strcpy(arecod_list, "'");
	    strcat(arecod_list, sqlGetString (res2, row, "arecod"));
	    strcat(arecod_list, "'");
	}
	srvFreeMemory(SRVRET_STRUCT, Cmdres);
    }
    
    /* And now read the shipments */
    if (strlen(arecod))
    {
        if (passed_in_ordinfo == TRUE)
        {
	   sprintf(buffer,
		"select shipment.ship_id, pckmov.arecod, "
		"       pckmov.stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       pckwrk.pckqty, pckwrk.appqty, "
		"       pckmov.arrqty, pckmov.prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, pckmov.cmbcod, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		" from adrmst, shipment, "
		"      pckmov, pckwrk, ord "
                
                /* MR 2810, Raman Parthasarathy. Dt: 05/22/02 
		"  and pckmov.rescod      = locmst.rescod "
                End of MR 2810 */ 

		"  where  pckmov.arecod      = '%s' "
		"  and pckmov.stoloc is not null "
		"  and pckwrk.cmbcod      = pckmov.cmbcod "
		"  and pckwrk.pcksts||''  = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"   %s %s %s %s %s %s %s %s %s "
		"  and ord.ordnum         = pckwrk.ordnum "
		"  and ord.client_id      = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts||''  = '%s' "
		" %s ",
		arecod,
		PCKSTS_RELEASED, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string,
		like_clause,
		SHPSTS_IN_PROCESS,
		order_by_clause);
        }
        else
        {
	   sprintf(buffer,
		"select shipment.ship_id, pckmov.arecod, "
		"       pckmov.stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       pckwrk.pckqty, pckwrk.appqty, "
		"       pckmov.arrqty, pckmov.prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, pckmov.cmbcod, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		" from adrmst, "
		"      pckwrk, ord, shipment, "
		"      pckmov"
                
                /* MR 2810, Raman Parthasarathy. Dt: 05/22/02 
		"  and pckmov.rescod      = locmst.rescod "
                End of MR 2810 */ 

		"  where  pckmov.arecod      = '%s' "
		"  and pckmov.stoloc is not null "
		"  and pckwrk.cmbcod      = pckmov.cmbcod "
		"  and pckwrk.pcksts      = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"   %s %s %s %s %s %s %s %s %s "
		"  and ord.ordnum         = pckwrk.ordnum "
		"  and ord.client_id      = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts      = '%s' "
		" %s ",
		arecod,
		PCKSTS_RELEASED, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string,
		like_clause,
		SHPSTS_IN_PROCESS,
		order_by_clause);
       
        }
    }
    else
    {
        if (passed_in_ordinfo == TRUE)
        {
	   sprintf(buffer,
		"select shipment.ship_id, pckmov.arecod, "
		"       pckmov.stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       pckwrk.pckqty, pckwrk.appqty, "
		"       pckmov.arrqty, pckmov.prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, pckmov.cmbcod, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		" from adrmst, shipment, "
		"      pckmov, pckwrk, ord "

                /* MR 2810, Raman Parthasarathy. Dt: 05/22/02 
		"  and pckmov.rescod      = locmst.rescod "
                End of MR 2810 */ 

		"  where  pckmov.arecod     in (%s) "
		"  and pckmov.stoloc is not null "
		"  and pckwrk.cmbcod      = pckmov.cmbcod "
		"  and pckwrk.pcksts||''  = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"   %s %s %s %s %s %s %s %s %s "
		"  and ord.ordnum           = pckwrk.ordnum "
		"  and ord.client_id        = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts||''  = '%s' "
		" %s ",
                arecod_list,
		PCKSTS_RELEASED, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string,
		like_clause,
		SHPSTS_IN_PROCESS,
		order_by_clause);
         }
         else
         {
	    sprintf(buffer,
		"select shipment.ship_id, pckmov.arecod, "
		"       pckmov.stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       pckwrk.pckqty, pckwrk.appqty, "
		"       pckmov.arrqty, pckmov.prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, pckmov.cmbcod, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		" from  pckwrk, "
		"       ord, adrmst, shipment, "
		"       pckmov "

                /* MR 2810, Raman Parthasarathy. Dt: 05/22/02 
		"  and pckmov.rescod      = locmst.rescod "
                End of MR 2810 */ 

		"  where  pckmov.arecod     in (%s) "
		"  and pckmov.stoloc is not null "
		"  and pckwrk.cmbcod      = pckmov.cmbcod "
		"  and pckwrk.pcksts      = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"   %s %s %s %s %s %s %s %s %s "
		"  and ord.ordnum           = pckwrk.ordnum "
		"  and ord.client_id        = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts      = '%s' "
		" %s ",
                arecod_list,
		PCKSTS_RELEASED, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string,
		like_clause,
		SHPSTS_IN_PROCESS,
		order_by_clause);
        }
    }
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	CurPtr = sSetupEmptyReturn(eOK);
	return (CurPtr);
    }

    /* Now read the unallocated stuff */
    if (strlen(arecod))
    {
       sprintf(buffer,
		"select pckwrk.ship_id, pckmov.arecod, "
		"       'Unallocated' stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       sum(pckwrk.pckqty) pckqty, 0 appqty, "
		"       0 arrqty, 0 prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		"  from adrmst, pckwrk, ord, shipment, "
		"       pckmov "
		" where pckmov.arecod    = '%s' "
		"  and pckwrk.cmbcod      = pckmov.cmbcod "
		"  and pckwrk.pcksts      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"  and ord.ordnum         = pckwrk.ordnum "
		"  and ord.client_id      = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts      = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"   %s %s %s %s %s %s %s %s "
		" group by pckwrk.ship_id, pckmov.arecod, stoloc, "
		"          pckwrk.client_id, pckwrk.ordnum, pckwrk.untcas, "
		"          pckwrk.pcksts, pckwrk.wrktyp, "
		"          shipment.carcod, shipment.srvlvl, "
		"          ord.stcust, adrmst.adrnam, "
		"          pckwrk.schbat, ord.rtcust, ord.cponum ",
		arecod,
		PCKSTS_PENDING,
		SHPSTS_IN_PROCESS, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string);
    }
    else
    {
       sprintf(buffer,
		"select pckwrk.ship_id, pckmov.arecod, "
		"       'Unallocated' stoloc, pckwrk.client_id, "
		"       pckwrk.ordnum, pckwrk.untcas, "
		"       pckwrk.pcksts, pckwrk.wrktyp, "
		"       shipment.carcod, shipment.srvlvl, "
		"       sum(pckwrk.pckqty) pckqty, 0 appqty, "
		"       0 arrqty, 0 prcqty, "
		"       ord.stcust, adrmst.adrnam, "
		"       pckwrk.schbat, "
		"	pckwrk.client_id||pckwrk.ordnum ordkey, "
                "       ord.rtcust, ord.cponum "
		"  from adrmst, pckwrk, ord, shipment, "
		"       pckmov "
		" where pckmov.arecod     in (%s) "
		"   and pckmov.cmbcod    = pckwrk.cmbcod "
		"  and pckwrk.pcksts      = '%s' "
		"  and shipment.ship_id     = pckwrk.ship_id "
		"  and ord.ordnum         = pckwrk.ordnum "
		"  and ord.client_id      = pckwrk.client_id "
		"  and ord.st_adr_id = adrmst.adr_id "
		"  and shipment.stgdte     is null "
		"  and shipment.shpsts      = '%s' "
		"  and pckwrk.wrktyp      = '%s' "
		"   %s %s %s %s %s %s %s %s "
		" group by pckwrk.ship_id, pckmov.arecod, stoloc, "
		"          pckwrk.client_id, pckwrk.ordnum, pckwrk.untcas, "
		"          pckwrk.pcksts, pckwrk.wrktyp, "
		"          shipment.carcod, shipment.srvlvl, "
		"          ord.stcust, adrmst.adrnam, "
		"          pckwrk.schbat, ord.rtcust, ord.cponum ",
		arecod_list,
		PCKSTS_PENDING,
		SHPSTS_IN_PROCESS, WRKTYP_PICK,
                rtcust_string, cponum_string,
		stcust_string, carcod_string, srvlvl_string,
		ship_id_string, client_id_string, ordnum_string);
    }

    ret_status = sqlExecStr(buffer, &res2);
    if (ret_status != eOK)
	unalloc = 0;
    else
	unalloc = 1;

    memset(save_ship_id, 0, sizeof(save_ship_id));
    memset(save_curloc, 0, sizeof(save_curloc));
    memset(save_ordkey, 0, sizeof(save_ordkey));
    memset(save_cmbcod, 0, sizeof(save_cmbcod));

    appqty = pckqty = arrqty = 0;
    prevRow = NULL;
    for (row = sqlGetRow(res); row; prevRow = row, row = sqlGetNextRow(row))
    {
	FlushResults = 0;
	if (strlen(save_ship_id))
	{
	    if (misTrimStrncmp(save_ship_id, 
		               sqlGetString(res, row, "ship_id"),
			       SHIP_ID_LEN))
	    {
		FlushResults++;
	    }
	}
	else
	{
	    misTrimcpy(save_ship_id, 
		       sqlGetString(res, row, "ship_id"),
		       SHIP_ID_LEN);
	}

	if (strlen(save_curloc))
	{
	    if (strncmp(save_curloc,
			(char *) sqlGetValue(res, row, "stoloc"),
			STOLOC_LEN) != 0)
	    {
		FlushResults++;
	    }
	}
	else
	{
	    strncpy(save_curloc,
		    (char *) sqlGetValue(res, row, "stoloc"), STOLOC_LEN);
	}

	if (strlen(save_ordkey))
	{
	    if (strncmp(save_ordkey,
			(char *) sqlGetValue(res, row, "ordkey"),
			CLIENT_ID_LEN + ORDNUM_LEN) != 0)
	    {
		FlushResults++;
	    }
	}
	else
	{
	    strncpy(save_ordkey,
		    (char *) sqlGetValue(res, row, "ordkey"), 
		    CLIENT_ID_LEN + ORDNUM_LEN);
	}

	if (FlushResults)
	{

	    if (arrqty > appqty)
	    {
		rem_arrqty = arrqty - appqty;
		arrqty = appqty;
	    }

	    _intListRS_AddResults(&CurPtr,
				  arrqty, pckqty, appqty, 0,
				  res, prevRow);

	    if (unalloc)
	    {
		_processUnalloc(&CurPtr,
				&returnHolder,
				res2,
				save_ship_id);
	    }

	    if (PolXDockInstalled)
	    {
		sProcessXDock(&CurPtr,
			      &returnHolder,
			      res2,
			      save_ship_id,
			      save_curloc);
	    }

	    arrqty = 0;
	    pckqty = 0;
	    appqty = 0;
	    memset(save_curloc, 0, sizeof(save_curloc));
	    strncpy(save_curloc,
		    (char *) sqlGetValue(res, row, "stoloc"), STOLOC_LEN);
	    memset(save_ship_id, 0, sizeof(save_ship_id));
	    misTrimcpy(save_ship_id, 
		       sqlGetString(res, row, "ship_id"),
		       SHIP_ID_LEN);
	    memset(save_ordkey, 0, sizeof(save_ordkey));
	    strncpy(save_ordkey,
		    (char *) sqlGetValue(res, row, "ordkey"), 
		    CLIENT_ID_LEN + ORDNUM_LEN);
	}


	tmp_arrqty = sqlGetLong(res, row, "arrqty");
	tmp_appqty = sqlGetLong(res, row, "appqty");
	tmp_pckqty = sqlGetLong(res, row, "pckqty");
	pckqty += tmp_pckqty;
	appqty += tmp_appqty;
	if (strncmp(sqlGetString(res, row, "cmbcod"), 
		    save_cmbcod, CMBCOD_LEN) != 0)
	{
	    memset(save_cmbcod, 0, sizeof(save_cmbcod));
	    strncpy(save_cmbcod, sqlGetString(res, row, "cmbcod"), CMBCOD_LEN);
	    
	    arrqty += tmp_arrqty;
	    rem_arrqty = 0;
	}
	if ((strncmp(sqlGetString(res, row, "cmbcod"), 
		    save_cmbcod, CMBCOD_LEN) == 0)
		&& (rem_arrqty > 0) && (arrqty == 0))
	{
	    arrqty = rem_arrqty;
	}
    }

    _intListRS_AddResults(&CurPtr,
			  arrqty, pckqty, appqty, 0,
			  res, prevRow);

    if (unalloc)
    {
	_processUnalloc(&CurPtr,
			&returnHolder,
			res2,
			save_ship_id);
    }

    if (PolXDockInstalled)
    {
	sProcessXDock(&CurPtr,
		      &returnHolder,
		      res2,
		      save_ship_id,
		      save_curloc);
    }

    sqlFreeResults(res2);
    sqlFreeResults(res);

    return (CurPtr);
}

static void _processUnalloc(RETURN_STRUCT ** CurPtr,
			    mocaDataRow ** returnHolder,
			    mocaDataRes * res,
			    char *ship_id)
{
    mocaDataRow    *row;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	if (misTrimStrncmp(sqlGetString(res, row, "ship_id"),
	                   ship_id,
			   SHIP_ID_LEN) == 0)
	{
	    _intListRS_AddResults(CurPtr, 0, 0, 0,
				  sqlGetLong(res, row, "pckqty"),
				  res, row);
	}
    }
}

static void sProcessXDock(RETURN_STRUCT ** CurPtr,
			  mocaDataRow ** returnHolder,
			  mocaDataRes * res,
			  char *ship_id,
			  char *curloc)
{
    char buffer[4000];
    int ret_status=eOK;
    mocaDataRow *row;

    sprintf(buffer,
	    "select xd.dstare arecod, "
	    "       nvl(xd.dstloc,'%s') stoloc, "
            "       xd.untcas, "
	    "       sh.carcod, sh.srvlvl, "
	    "       xd.schbat, xd.ship_id, "
	    "       o.stcust, xd.ordnum, "
	    "       sum(xd.pckqty - xd.alcqty) pckqty, "
            "       o.rtcust, o.cponum "
	    "  from ord o, shipment sh, xdkwrk xd  "
	    " where xd.ship_id   = sh.ship_id " 
	    "   and xd.ordnum    = o.ordnum "
	    "   and xd.client_id = o.client_id "
	    "   and xd.ship_id   = '%s' "
	    "   and xd.xdktyp    = '%s' "
	    "   and xd.pckqty > xd.alcqty " 
	    " group by xd.dstare, xd.dstloc, "
	    "          xd.untcas, sh.carcod, sh.srvlvl, "
	    "          xd.schbat, xd.ship_id, "
	    "          o.stcust, xd.ordnum, o.rtcust, o.cponum ",
	    curloc, ship_id, XDKTYP_SHIPMENT, curloc);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
    	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    	{
	    if (misTrimStrncmp(sqlGetString(res, row, "ship_id"),
	                   ship_id,
			   SHIP_ID_LEN) == 0)
	    {
	    	_intListRS_AddResults(CurPtr, 0, 0, 0,
				      sqlGetLong(res, row, "pckqty"),
				      res, row);
	    }
    	}
    }
    sqlFreeResults(res);
}

static void _intListRS_AddResults(RETURN_STRUCT **CurPtr,
				  long arrqty,
				  long pckqty,
				  long appqty,
				  long remqty,
				  mocaDataRes * res,
				  mocaDataRow * row)
{
//LHSTART-kzainal - declare variables and pointers 
    char            buffer[4000];
    int             ret_status;
    mocaDataRes    *kres;
    mocaDataRow    *krow;
//LHSTOP-kzainal 

    char            wrk_arrqty[24];
    char            wrk_pckqty[24];
    char            wrk_remqty[24];
    char            wrk_appqty[24];
    static char     datatypes[100];

    sprintf(wrk_arrqty, "%ld", arrqty);
    sprintf(wrk_pckqty, "%ld", pckqty);
    sprintf(wrk_appqty, "%ld", appqty);
    sprintf(wrk_remqty, "%ld", remqty);

    if (*CurPtr == NULL)
    {
        *CurPtr = srvResultsInit(eOK,
				 "ship_id",   COMTYP_CHAR, SHIP_ID_LEN,
				 "stoloc",    COMTYP_CHAR, STOLOC_LEN,
				 "stcust",    COMTYP_CHAR, STCUST_LEN,
				 "adrnam",    COMTYP_CHAR, ADRNAM_LEN,
				 "carcod",    COMTYP_CHAR, CARCOD_LEN,
				 "srvlvl",    COMTYP_CHAR, SRVLVL_LEN,
				 "arrqty",    COMTYP_CHAR, 14,
				 "appqty",    COMTYP_CHAR, 14,
				 "pckqty",    COMTYP_CHAR, 14,
				 "remqty",    COMTYP_CHAR, 14,
				 "doc_num",   COMTYP_CHAR, DOC_NUM_LEN,
			         "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
			         "ordnum",    COMTYP_CHAR, ORDNUM_LEN,
			         "rtcust",    COMTYP_CHAR, RTCUST_LEN,
			         "cponum",    COMTYP_CHAR, CPONUM_LEN,
//LHSTART-kzainal - initialize or set up empty return result.
			         "early_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			         "late_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			         "vc_extdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
//LHSTOP-kzainal 
				 NULL);

    }

//LHSTART-kzainal - get latest early_shpdte, late_shpdte, vc_extdte for each line returned.
    sprintf(buffer,
                "select max(to_char(ord_line.early_shpdte, 'MM/DD/YYYY HH24\:MI\:SS')) early_shpdte, "
                "       max(to_char(ord_line.late_shpdte, 'MM/DD/YYYY HH24\:MI\:SS')) late_shpdte, "
                "       max(to_char(ord_line.vc_extdte, 'MM/DD/YYYY HH24\:MI\:SS')) vc_extdte "
		"  from ord_line "
		" where ord_line.client_id = '----' "
		"   and ord_line.ordnum    = '%s' ",
		sqlGetValue(res, row, "ordnum"));

    ret_status = sqlExecStr(buffer, &kres);
    if (ret_status != eOK)
    {
	//sqlFreeResults(res);
	//sqlFreeResults(kres);
	/* *CurPtr = sSetupEmptyReturn(eOK);
	   return (*CurPtr); */
    }
    krow = sqlGetRow(kres);
//LHSTOP-kzainal

    srvResultsAdd(*CurPtr,
		  sqlGetValue(res, row, "ship_id"),
		  sqlGetValue(res, row, "stoloc"),
		  sqlGetValue(res, row, "stcust"),
		  sqlGetValue(res, row, "adrnam"),
		  sqlGetValue(res, row, "carcod"),
                  sqlGetValue(res, row, "srvlvl"),
		  wrk_arrqty,
		  wrk_appqty,
		  wrk_pckqty,
		  wrk_remqty,
		  sqlGetValue(res, row, "doc_num"),
		  sqlGetValue(res, row, "client_id"),
		  sqlGetValue(res, row, "ordnum"),
                  sqlGetValue(res, row, "rtcust"),
                  sqlGetValue(res, row, "cponum"),
//LHSTART-kzainal - set up return result.
                  sqlGetValue(kres, krow, "early_shpdte"),
                  sqlGetValue(kres, krow, "late_shpdte"),
                  sqlGetValue(kres, krow, "vc_extdte"));

    sqlFreeResults(kres);
//LHSTOP-kzainal 

    return;
}
