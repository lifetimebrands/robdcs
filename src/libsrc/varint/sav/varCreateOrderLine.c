/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 Software Architects, Inc.
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varCreateOrderLine.c,v $
 *  $Revision: 1.2 $
 *
 *  Application:   intlib
 *  Created:       27-Sep-1993
 *  $Author: lh51kz $
 *  
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>


LIBEXPORT
RETURN_STRUCT *varCreateOrderLine(char *client_id_i,
			 	  char *ordnum_i, char *ordlin_i,
			 	  char *ordsln_i, char *prt_client_id_i,
			 	  char *prtnum_i, char *orgcod_i,
			 	  char *revlvl_i, char *lotnum_i,
			 	  char *invsts_i, char *cooinc_i,
			 	  char *coolst_i, 
			 	  long *host_ordqty_i, 
			 	  long *ovramt_i,
			 	  char *ovrcod_i, char *marcod_i,
			 	  moca_bool_t *parflg_i, moca_bool_t *ovaflg_i,
			 	  moca_bool_t *ovpflg_i, moca_bool_t *rpqflg_i,
			 	  moca_bool_t *splflg_i, moca_bool_t *stdflg_i,
			 	  moca_bool_t *bckflg_i,
			 	  char *prcpri_i,
			 	  char *frtcod_i, char *cargrp_i,
			 	  char *carcod_i, char *srvlvl_i,
			 	  moca_bool_t *sddflg_i,
			 	  char *entdte_i,
			 	  char *sales_ordnum_i,
			 	  char *sales_ordlin_i,
			 	  char *cstprt_i,
			 	  char *manfid_i, char *deptno_i,
			 	  char *accnum_i, char *prjnum_i,
			 	  long *untcas_i, long *untpak_i,
			 	  long *untpal_i,
			 	  char *early_shpdte_i, char *late_shpdte_i,
			 	  char *early_dlvdte_i, char *late_dlvdte_i,
				  char *ordinv_i,
				  moca_bool_t *edtflg_i,
				  moca_bool_t *xdkflg_i,
                                  moca_bool_t *atoflg_i,
                                  char *dstare_i, char *dstloc_i,
                                  char *rsvpri_i, long *ordqty_i,
                                  long *rsvqty_i, long *pckqty_i,
                                  char *pckgr1_i, char *pckgr2_i,
                                  char *pckgr3_i, char *pckgr4_i,
                                  int  *trigger_i, char *mode_i)

{
    static int custreqInstalled = 1234;
    static int pckrsvmgrInstalled = 1234;
    static int crossdockInstalled = 1234;

    RETURN_STRUCT *CurPtr;
    mocaDataRes *res, *tempres;
    mocaDataRow *row, *temprow;
    char tablename[100];
    char *columnlist;
    char *valuelist;
    char tmpvar[1000];
    char sqlbuffer[3000];
    char buffer[3000];
    long errcode;
    long ret_status;
    long untcas, untpak, untpal;
    int CheckCustReqs = 1;

    char client_id[CLIENT_ID_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    moca_bool_t carflg = BOOLEAN_NOTSET;
    moca_bool_t bckflg = BOOLEAN_NOTSET;
    moca_bool_t splflg = BOOLEAN_NOTSET; 
    moca_bool_t stdflg = BOOLEAN_NOTSET;
    moca_bool_t parflg = BOOLEAN_NOTSET;
    char manfid[MANFID_LEN+1]; 
    char deptno[DEPTNO_LEN+1]; 
    char cstprt[CSTPRT_LEN+1];
    char ordinv[ORDINV_LEN+1];
    char lotnum[LOTNUM_LEN+1];

    long rsvqty;
    long pckqty;
    long shpqty;

    
    memset(manfid,        0, sizeof(manfid));
    memset(deptno,        0, sizeof(deptno));
    memset(cstprt,        0, sizeof(cstprt));
    memset(client_id,     0, sizeof(client_id));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(ordinv,        0, sizeof(ordinv));
    memset(lotnum,        0, sizeof(lotnum));

    untcas = untpak = untpal = -1;
    shpqty = 0;

    CurPtr = NULL;
    res = NULL;

    /* Check if the customer requirement processing is installed */
    if ( custreqInstalled == 1234 )
	custreqInstalled = appIsInstalled(POLCOD_CUST_REQ_PROC);
   
    /* check if reservation manager is installed */
    if (pckrsvmgrInstalled == 1234)
        pckrsvmgrInstalled = appIsInstalled(POLCOD_PCKRSVMGR);

    if (crossdockInstalled == 1234)
        crossdockInstalled = appIsInstalled(POLCOD_CROSS_DOCKING);

    if (!ordnum_i || misTrimLen(ordnum_i, ORDNUM_LEN) == 0)
	return APPMissingArg("ordnum");

    if (!ordlin_i || misTrimLen(ordlin_i, ORDLIN_LEN) == 0)
	return APPMissingArg("ordlin");

    if (!ordsln_i || misTrimLen(ordsln_i, ORDSLN_LEN) == 0)
	return APPMissingArg("ordsln");

    if (!prtnum_i || misTrimLen(prtnum_i, PRTNUM_LEN) == 0)
	return APPMissingArg("prtnum");

    if (!ordqty_i || *ordqty_i < 0)
	return APPMissingArg("ordqty");

    if (!invsts_i || misTrimLen(invsts_i, INVSTS_LEN) == 0)
	return APPMissingArg("invsts");

    ret_status = appGetClient(client_id_i, client_id);

    if (ret_status != eOK)
	return (srvResults(ret_status, NULL));

    /* make sure that there is a valid order for this line */
    ret_status = appValidateEntity("ord", &res,
	                           "client_id", client_id, COMTYP_STRING, 0,
	                           "ordnum", ordnum_i, COMTYP_STRING, 0,
				   NULL);
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
	sqlFreeResults(res); 
	sprintf (tmpvar, "%s-%s", client_id, ordnum_i);
	return APPInvalidArg(tmpvar,"client_id-ordnum");
    }
    else if (eOK != ret_status)
    {
	sqlFreeResults(res); 
	return (srvResults(ret_status, NULL));
    }

    
    /*validate carrier information */
    row = sqlGetRow(res);
    carflg = sqlGetBoolean(res, row, "carflg");
    sqlFreeResults(res); 

    if (carflg == BOOLEAN_FALSE && (!carcod_i || !srvlvl_i)) 
	return APPMissingArg(carcod_i?"srvlvl":"carcod");


    /* 
       if the prt_client_id was not passed in, set the 
       prt_client_id to the client_id
    */
    if (!prt_client_id_i || misTrimLen(prt_client_id_i, CLIENT_ID_LEN) == 0)
    {
	strncpy(prt_client_id, client_id, CLIENT_ID_LEN);
    }
    else
    {
        misTrc(T_FLOW, "Validating prt_client_id");
        ret_status = appGetClient(prt_client_id_i, prt_client_id);
    }

    /* validate part */
    ret_status = appValidateEntity("prtmst", NULL,
	                           "prtnum", prtnum_i, COMTYP_STRING, 0,
				   "prt_client_id", prt_client_id, COMTYP_STRING, 0,
				   NULL);
    if (ret_status == eDB_NO_ROWS_AFFECTED)
	return APPInvalidArg(prtnum_i, "prtnum");
    else if (ret_status != eOK)
	return (srvResults(ret_status, NULL));


    /*validate carcod */
    if (carcod_i && misTrimLen(carcod_i, CARCOD_LEN))
    {
        ret_status = appValidateEntity("carhdr", NULL,
		                       "carcod", carcod_i, COMTYP_STRING,
				       CARCOD_LEN,
				       NULL);
	if (eDB_NO_ROWS_AFFECTED == ret_status)
	{
	    return APPInvalidArg(carcod_i, "carcod");
	}
	else if (eOK != ret_status)
	{
	    return(srvResults(ret_status, NULL));
	}
    }

    /*validate over allocation code */
    if (ovrcod_i && misTrimLen(ovrcod_i, OVRCOD_LEN))
    {
	ret_status = appValidateEntity("codmst", NULL,
	                               "colnam", "ovrcod", COMTYP_STRING, 0,
				       "codval", ovrcod_i, COMTYP_STRING, 0,
				       NULL);
        if (eDB_NO_ROWS_AFFECTED == ret_status)
	{
	    return APPInvalidArg(ovrcod_i, "ovrcod");
	}
	else if (eOK != ret_status)
	{
	    return(srvResults(ret_status, NULL));
	}

    }


    strcpy(tablename, "ord_line");
    
   
    /* Setup the columns to return */
    columnlist = valuelist = NULL;

    if (misTrimLen(client_id, CLIENT_ID_LEN))
    {

	if (eOK != appBuildInsertList("client_id", client_id, 0,
                              &columnlist, &valuelist))
	{
	    free(valuelist);
	    free(columnlist);
    	    return(srvResults(eNO_MEMORY, NULL));
	}

    }

    if (ordnum_i)
    {
	if (eOK != appBuildInsertList("ordnum", ordnum_i, 0,
                      &columnlist, &valuelist))
	{
    	    free(valuelist);
    	    free(columnlist);
    	    return(srvResults(eNO_MEMORY, NULL));
	}

    }

    if (ordlin_i)
    {
	if (eOK != appBuildInsertList("ordlin", ordlin_i, 0,
              &columnlist, &valuelist))
	{
    	    free(valuelist);
    	    free(columnlist);
    	    return(srvResults(eNO_MEMORY, NULL));
	}
	
    }

    if (ordsln_i)
    {
        if (eOK != appBuildInsertList("ordsln", ordsln_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (prtnum_i)
    {
        if (eOK != appBuildInsertList("prtnum", prtnum_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }

    if (eOK != appBuildInsertList("prt_client_id", prt_client_id, 0,
              &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (orgcod_i)
    {
        if (eOK != appBuildInsertList("orgcod", orgcod_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (revlvl_i)
    {
        if (eOK != appBuildInsertList("revlvl", revlvl_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }

/* LHCSTART Paul W., MR#550, 08/08/01 */  

/* original code
    if (lotnum_i)
    {
        if (eOK != appBuildInsertList("lotnum", lotnum_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    } */
/* LHCEND Paul W., MR#550 08/08/01*/

    if (invsts_i)
    {
        if (eOK != appBuildInsertList("invsts_prg", invsts_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }



    if (cooinc_i)
    {
        if (eOK != appBuildInsertList("cooinc", cooinc_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (coolst_i)
    {
        if (eOK != appBuildInsertList("coolst", coolst_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (host_ordqty_i)
    {
 	sprintf(tmpvar, "%ld", 	*host_ordqty_i);
        if (eOK != appBuildInsertList("host_ordqty", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (ovramt_i)
    {
	sprintf(tmpvar, "%ld", *ovramt_i);
        if (eOK != appBuildInsertList("ovramt", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (ovrcod_i)
    {
        if (eOK != appBuildInsertList("ovrcod", ovrcod_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (marcod_i)
    {
        if (eOK != appBuildInsertList("marcod", marcod_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (ovaflg_i && *ovaflg_i == BOOLEAN_TRUE)
    {
	sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
	sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("ovaflg", tmpvar, 0,
              &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (ovpflg_i && *ovpflg_i == BOOLEAN_TRUE)
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("ovpflg", tmpvar, 0,
              &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (rpqflg_i && *rpqflg_i == BOOLEAN_TRUE)
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("rpqflg", tmpvar, 0,
              &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (prcpri_i && misTrimLen(prcpri_i, PRICOD_LEN) > 0)
    {
        if (eOK != appBuildInsertList("prcpri", prcpri_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }
    else
    {
        if (eOK != appBuildInsertList("prcpri", PRICOD_NORMAL, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (frtcod_i)
    {
        if (eOK != appBuildInsertList("frtcod", frtcod_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (cargrp_i)
    {
        if (eOK != appBuildInsertList("cargrp", cargrp_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
	
    if (carcod_i)
    {
        if (eOK != appBuildInsertList("carcod", carcod_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
	
    if (srvlvl_i)
    {
        if (eOK != appBuildInsertList("srvlvl", srvlvl_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (sddflg_i && *sddflg_i == BOOLEAN_TRUE)
    {
    	sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
    	sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("sddflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
    	free(valuelist);
    	free(columnlist);
    	return(srvResults(eNO_MEMORY, NULL));
    }


    if (entdte_i && misTrimLen(entdte_i, MOCA_STD_DATE_LEN) > 0)
    {
	sprintf(tmpvar, "TO_DATE('%.*s')", MOCA_STD_DATE_LEN, entdte_i);
        if (eOK != appBuildInsertListDBKW("entdte", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
    else
    {
        if (eOK != appBuildInsertListDBKW("entdte", "sysdate", 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
    if (sales_ordnum_i)
    {
        if (eOK != appBuildInsertList("sales_ordnum", sales_ordnum_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (sales_ordlin_i)
    {
        if (eOK != appBuildInsertList("sales_ordlin", sales_ordlin_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }

    if (accnum_i)
    {
        if (eOK != appBuildInsertList("accnum", accnum_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (prjnum_i)
    {
        if (eOK != appBuildInsertList("prjnum", prjnum_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (early_shpdte_i && misTrimLen(early_shpdte_i, MOCA_STD_DATE_LEN) > 0)
    {
        sprintf(tmpvar, "TO_DATE('%.*s')", MOCA_STD_DATE_LEN, early_shpdte_i);
        if (eOK != appBuildInsertListDBKW("early_shpdte", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (late_shpdte_i && misTrimLen(late_shpdte_i, MOCA_STD_DATE_LEN) > 0)
    {
        sprintf(tmpvar, "TO_DATE('%.*s')", MOCA_STD_DATE_LEN, late_shpdte_i);
        if (eOK != appBuildInsertListDBKW("late_shpdte", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (early_dlvdte_i && misTrimLen(early_dlvdte_i, MOCA_STD_DATE_LEN) > 0)
    {
        sprintf(tmpvar, "TO_DATE('%.*s')", MOCA_STD_DATE_LEN, early_dlvdte_i);
        if (eOK != appBuildInsertListDBKW("early_dlvdte", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (late_dlvdte_i && misTrimLen(late_dlvdte_i, MOCA_STD_DATE_LEN) > 0)
    {
        sprintf(tmpvar, "TO_DATE('%.*s')", MOCA_STD_DATE_LEN, late_dlvdte_i);
        if (eOK != appBuildInsertListDBKW("late_dlvdte", tmpvar, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    /*  Start Customer Requirement Check
    */
    if ( custreqInstalled == 1 )
    {
	sprintf(sqlbuffer, 
		"create special handling instructions "
		" where client_id     = '%s' "
		"   and ordnum        = '%s' "
		"   and prtnum        = '%s' "
		"   and prt_client_id = '%s' "
		"   and ordlin        = '%s' "
		"   and ordsln        = '%s' ",
		client_id,
		misTrim(ordnum_i),
		misTrim(prtnum_i),
		misTrim(prt_client_id),
		misTrim(ordlin_i),
		misTrim(ordsln_i));
	
	errcode = srvInitiateCommand (sqlbuffer, NULL);
	if (errcode != eOK) 
	{
            free(valuelist);
            free(columnlist);
	    return(srvResults(errcode, NULL));
	}
    }


    /*  Determine if we need to call to get the customer requirement fields.
    **  If all the customer requirement fields are populated, then there is
    **  no reason to collect the customer requirements.
    */
    if ( custreqInstalled == 1 )
    {
	if ( (manfid_i  && misTrimLen(manfid_i, MANFID_LEN) > 0) &&
	     (deptno_i  && misTrimLen(deptno_i, DEPTNO_LEN) > 0) &&
	     (cstprt_i  && misTrimLen(cstprt_i, CSTPRT_LEN) > 0) &&
	     (bckflg_i) &&
	     (splflg_i) &&
	     (stdflg_i) &&
	     (parflg_i) &&
             (lotnum_i) &&
             (untcas_i  && *untcas_i > 0) &&
	     (untpak_i  && *untpak_i > 0) &&
	     (untpal_i  && *untpal_i > 0) &&
	     (ordinv_i  && misTrimLen(ordinv_i, ORDINV_LEN) > 0) )
	{
	    CheckCustReqs = 0;
	}
    }
    else
    {
	CheckCustReqs = 0;
    }

    /*
    **  If we need to get the customer requirement fields call the command.
    */
    if ( CheckCustReqs == 1 )
    {
	sprintf(sqlbuffer,
                "[select prt_client_id from prtmst where prtnum = '%s'] | "
		"get customer requirements "
		"where prtnum    = '%s' "
		"  and prt_client_id = @prt_client_id "
		"  and client_id = '%s' "
		"  and ordnum    = '%s' ",
		prtnum_i,
                prtnum_i,
		client_id,
		ordnum_i);

	CurPtr = NULL;
	errcode = srvInitiateCommand (sqlbuffer, &CurPtr);
	if (errcode != eOK) 
	{
            free(valuelist);
            free(columnlist);
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    return(srvResults(errcode, NULL));
	}

	/*
	**  Populate any CR field that is blank/null.
	*/
	tempres = srvGetResults(CurPtr);
	temprow = sqlGetRow(tempres);
    }


    if (manfid_i && misTrimLen(manfid_i, MANFID_LEN) > 0)
    {
	strncpy(manfid, manfid_i, MANFID_LEN);
    }
    else if (CheckCustReqs == 1)
    {
	misTrimcpy(manfid, sqlGetString(tempres, temprow,"manfid"),MANFID_LEN);
    }

    if (deptno_i && misTrimLen(deptno_i, DEPTNO_LEN) > 0)
    {
	misTrimcpy(deptno, deptno_i, DEPTNO_LEN);
    }
    else if (CheckCustReqs == 1)
    {
	strncpy(deptno, sqlGetString(tempres, temprow,"deptno"), DEPTNO_LEN);
    }

    if (cstprt_i && misTrimLen(cstprt_i, CSTPRT_LEN) > 0)
    {
	misTrimcpy(cstprt, cstprt_i, CSTPRT_LEN);
    }
    else if (CheckCustReqs == 1)
    {
	misTrimcpy(cstprt, sqlGetString(tempres, temprow,"cstprt"),CSTPRT_LEN);
    }

/* LHCSTART Paul W., MR#550, 08/08/01 */  
    /* if SeamLES send us NOLOT, which means HOST download a blank lotnum */ 
    if (strncmp(lotnum_i, "NOLOT", LOTNUM_LEN) == 0)
    {
       /*if we got NOLOT we will try to find the lotnum from cstprq*/
       misTrimcpy(lotnum, sqlGetString(tempres, temprow, "vc_lotnum"),LOTNUM_LEN);

       /*if we still cannot find anything from cstprq, we will just set it to NOTLOT*/
       if (misTrimLen(lotnum, LOTNUM_LEN) == 0)
       {
          misTrimcpy(lotnum, "NOLOT", LOTNUM_LEN);
       }
    }
    /* if SeamLES send a blank lotnum, which means that it is not lot control*/
    else if (misTrimLen(lotnum_i, LOTNUM_LEN) == 0)
    {
       /*we will just set lotnum to NOLOT*/
       misTrimcpy(lotnum, "NOLOT", LOTNUM_LEN);
    }
    /* if SeamLES send us something other than NOLOT, we will just use it */
/* LHCSTART Khairul Z., MR#942, 11/20/01 */  
    /* else if (strncmp(lotnum_i, "NOLOT", LOTNUM_LEN) != 0 && (misTrimLen(lotnum_i, LOTNUM_LEN) == 0)) */
    /* commented paul's line (else if) above and replaced with (else)
       because it was looking for a null length */ 
    else 
/* LHCEND Khairul Z., MR#942, 11/20/01 */
    {
       /*set lotnum_i to lotnum*/
       misTrimcpy(lotnum, lotnum_i, LOTNUM_LEN);
    }
     
/* LHCEND Paul W., MR#550 08/08/01*/

    if (bckflg_i)
    {
	bckflg = *bckflg_i;
    }
    else if (CheckCustReqs == 1)
    {
	bckflg = sqlGetBoolean(tempres, temprow, "bckflg");
    }

    if (splflg_i)
    {
	splflg = *splflg_i;
    }
    else if (CheckCustReqs == 1)
    {
	splflg = sqlGetBoolean(tempres, temprow, "splflg");
    }

    if (stdflg_i)
    {
	stdflg = *stdflg_i;
    }
    else if (CheckCustReqs == 1)
    {
	stdflg = sqlGetBoolean(tempres, temprow, "stdflg");
    }

    if (parflg_i)
    {
	parflg = *parflg_i;
    }
    else if (CheckCustReqs == 1)
    {
	parflg = sqlGetBoolean(tempres, temprow, "parflg");
    }

    if (untcas_i && *untcas_i >= 0)
    {
	untcas = *untcas_i;
    }
    else if (CheckCustReqs == 1)
    {
	untcas = sqlGetLong(tempres, temprow,"untcas");
    }

    if (untpak_i && *untpak_i >= 0)
    {
	untpak = *untpak_i;
    }
    else if (CheckCustReqs == 1)
    {
	untpak = sqlGetLong(tempres, temprow,"untpak");
    }

    if (untpal_i && *untpal_i >= 0)
    {
	untpal = *untpal_i;
    }
    else if (CheckCustReqs == 1)
    {
	untpal = sqlGetLong(tempres, temprow,"untpal");
    }

    if (ordinv_i && misTrimLen(ordinv_i, ORDINV_LEN) >= 0)
    {
	misTrimcpy(ordinv, ordinv_i, ORDINV_LEN);
    }
    else if (CheckCustReqs == 1)
    {
	misTrimcpy(ordinv, sqlGetString(tempres, temprow, "ordinv"), ORDINV_LEN);
    }

    if ( CheckCustReqs == 1 )
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
        CurPtr = NULL;
    }

    /*
    **  CUSTOMER REQUIREMENT FIELDS
    */

    if (misTrimLen(cstprt, CSTPRT_LEN) > 0)
    {
        if (eOK != appBuildInsertList("cstprt", cstprt, 0, &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }

/* LHCSTART Paul W., MR#550, 08/08/01 */
/* create the update list */
    if (misTrimLen(lotnum, LOTNUM_LEN) > 0) 
    {
        if (eOK != appBuildInsertList("lotnum", lotnum, 0, &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
/* LHCEND Paul W., MR#550 08/08/01*/

    if (misTrimLen(manfid, MANFID_LEN) > 0)
    {
        if (eOK != appBuildInsertList("manfid", manfid, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (misTrimLen(deptno, DEPTNO_LEN))
    {
        if (eOK != appBuildInsertList("deptno", deptno, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (parflg >= 0)
    {
	sprintf(tmpvar, "%d", parflg);
        if (eOK != appBuildInsertList("parflg", tmpvar, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }
    else
    {
	sprintf(tmpvar, "%d", BOOLEAN_TRUE);
        if (eOK != appBuildInsertList("parflg", tmpvar, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }

    }

    if (untcas >= 0)
    {
	sprintf(tmpvar, "%ld", untcas);
        if (eOK != appBuildInsertList("untcas", tmpvar, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (untpak >= 0)
    {
	sprintf(tmpvar, "%ld", untpak);
        if (eOK != appBuildInsertList("untpak", tmpvar, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (untpal >= 0)
    {
	sprintf(tmpvar, "%ld", untpal);
        if (eOK != appBuildInsertList("untpal", tmpvar, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }
    
    if (bckflg == BOOLEAN_TRUE || bckflg == BOOLEAN_NOTSET) 
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("bckflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }


    if (splflg == BOOLEAN_TRUE || splflg == BOOLEAN_NOTSET)
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("splflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }


    if (stdflg == BOOLEAN_TRUE )
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("stdflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (misTrimLen(ordinv, ORDINV_LEN) > 0)
    {
        if (eOK != appBuildInsertList("ordinv", ordinv, 0,
                      &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (edtflg_i && *edtflg_i == BOOLEAN_TRUE )
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("edtflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }
    
    if (xdkflg_i && *xdkflg_i == BOOLEAN_TRUE )
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("xdkflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (atoflg_i && *atoflg_i == BOOLEAN_TRUE )
    {
        sprintf(tmpvar, "%d", BOOLEAN_TRUE);
    }
    else
    {
        sprintf(tmpvar, "%d", BOOLEAN_FALSE);
    }
    if (eOK != appBuildInsertList("atoflg", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    if (dstare_i)
    {
       if (misTrimLen(dstare_i, ARECOD_LEN))
       {
           ret_status = appValidateEntity("aremst", NULL,
                               "arecod", dstare_i, COMTYP_STRING, ARECOD_LEN,
                               NULL);
           if (ret_status != eOK)
           {
               free(valuelist);
               free(columnlist);
               return APPInvalidArg(dstare_i, "dstare");
           }
       }
   
       if (eOK != appBuildInsertList("dstare", dstare_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (dstloc_i)
    {
        if (misTrimLen(dstloc_i, STOLOC_LEN))
        {
            ret_status = appValidateEntity("locmst", NULL,
                                "stoloc", dstloc_i, COMTYP_STRING, 0,
                                NULL);
            if (ret_status != eOK)
            {
                free(valuelist);
                free(columnlist);
                return APPInvalidArg(dstloc_i, "dstloc");
            }
        }
       
	if (eOK != appBuildInsertList("dstloc", dstloc_i, 0,
              &columnlist, &valuelist))
        {
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    if (rsvpri_i)
    {
	sprintf(tmpvar, "%ld", *rsvpri_i);
        if (eOK != appBuildInsertList("rsvpri", tmpvar, 0,
              &columnlist, &valuelist))
        { 
            free(valuelist);
            free(columnlist);
            return(srvResults(eNO_MEMORY, NULL));
        }
    }

    sprintf(tmpvar, "%ld",  *ordqty_i);
    if (eOK != appBuildInsertList("ordqty", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }

    /* manipulate reservation and pickable quantity */
    if (mode_i && misCiStrcmp(mode_i, "SEAMLES") == 0)
    {
        /* mode is SEAMLES */

        /* If the rsvqty is downloaded through Seamles as null, */
        /* we'll be receiving it here as a 0. So, we only want */
        /* to set it to the value being passed if it is > 0. */

        if (rsvqty_i && *rsvqty_i > 0)
        {
            rsvqty = *rsvqty_i;
        }
        else
        {
            if (!pckrsvmgrInstalled)
              rsvqty = *ordqty_i;
            else if ((crossdockInstalled) && (*xdkflg_i == BOOLEAN_TRUE))
              rsvqty = *ordqty_i;
            else
              rsvqty = 0;
        }

        if (pckqty_i && *pckqty_i > -1)
        {
            pckqty = *pckqty_i;
        }
        else
        {
            if (!pckrsvmgrInstalled)
              pckqty = *ordqty_i;
            else if ((crossdockInstalled) && (*xdkflg_i == BOOLEAN_TRUE))
              pckqty = *ordqty_i;
            else
              pckqty = 0;
        }
    }
    else
    {
        /* mode is GUI */
        if (!pckrsvmgrInstalled)
          rsvqty = *ordqty_i;
        else if ((crossdockInstalled) && (*xdkflg_i == BOOLEAN_TRUE))
          rsvqty = *ordqty_i;
        else
          rsvqty = 0;

        if (!pckrsvmgrInstalled)
          pckqty = *ordqty_i;
        else if ((crossdockInstalled) && (*xdkflg_i == BOOLEAN_TRUE))
          pckqty = *ordqty_i;
        else
          pckqty = 0;
    }
   
    sprintf(tmpvar, "%ld",  rsvqty);
    if (eOK != appBuildInsertList("rsvqty", tmpvar, 0,
          &columnlist, &valuelist))
    {  
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }
   
    sprintf(tmpvar, "%ld",  pckqty);
    if (eOK != appBuildInsertList("pckqty", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }
    
    sprintf(tmpvar, "%ld",  shpqty);
    if (eOK != appBuildInsertList("shpqty", tmpvar, 0,
          &columnlist, &valuelist))
    {
        free(valuelist);
        free(columnlist);
        return(srvResults(eNO_MEMORY, NULL));
    }
  
    if (pckgr1_i)
    {
        if (eOK != appBuildInsertList("pckgr1", pckgr1_i, 0,
               &columnlist, &valuelist))
         {
             free(valuelist);
             free(columnlist);
             return(srvResults(eNO_MEMORY, NULL));
         }
    }
    
    if (pckgr2_i)
    {
        if (eOK != appBuildInsertList("pckgr2", pckgr2_i, 0,
               &columnlist, &valuelist))
         {
             free(valuelist);
             free(columnlist);
             return(srvResults(eNO_MEMORY, NULL));
         }
    }
    
    if (pckgr3_i)
    {
        if (eOK != appBuildInsertList("pckgr3", pckgr3_i, 0,
               &columnlist, &valuelist))
         {
             free(valuelist);
             free(columnlist);
             return(srvResults(eNO_MEMORY, NULL));
         }
    }
    
    if (pckgr4_i)
    {
        if (eOK != appBuildInsertList("pckgr4", pckgr4_i, 0,
               &columnlist, &valuelist))
         {
             free(valuelist);
             free(columnlist);
             return(srvResults(eNO_MEMORY, NULL));
         }
    }
  
    if (eOK != appBuildInsertList("mod_usr_id",
                                 osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "",
                                 USR_ID_LEN,
                                 &columnlist, &valuelist))
   {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvResults(eNO_MEMORY, NULL));
   }

   if (eOK != appBuildInsertListDBKW("moddte", "sysdate", 0,
                                 &columnlist, &valuelist))
   {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvResults(eNO_MEMORY, NULL));
   }



    /* Publish out the table, columns, and values for insert by */
    /* intProcessTableInsert (PROCESS TABLE INSERT) */
    
    CurPtr = srvResultsInit(eOK,
	                   "acttyp", COMTYP_CHAR, 1,
			   "tblnam", COMTYP_CHAR, strlen(tablename),
			   "collst", COMTYP_CHAR, strlen(columnlist),
			   "vallst", COMTYP_CHAR, strlen(valuelist),
			   NULL);
    
    srvResultsAdd(CurPtr,
	          ACTTYP_INSERT,
		  tablename,
		  columnlist,
		  valuelist);

    free(columnlist);
    free(valuelist);

    return (CurPtr);
}
