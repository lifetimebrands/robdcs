static const char *rcsid = "$Id: varCreateTriggeredReplenishment.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

/* #include "intlib.h" 	kjh */
/* #include "intRplLib.h"  kjh */

/* kjh added structure def from intRplLib.h for policy */
typedef struct _policyInfo {
    long is_installed;
    long max_created;
    long max_active;
    long topoffByRplcfg;
} POLICY_INFO;

/* 
 * There are some key pieces of info that we care about:
 *   1.  minunt/maxunt/pctflg/maxlocs
 *   2.  replenishments can be configured at an area level or at a location lvl
 *
 *  If configured at an area level:
 *         minunt:  min # of units in the AREA
 *         maxunt:  max # of units to replenish to the AREA (topoff only)
 *  If configured at a location level:
 *         minunt:  min # of units in the location
 *         maxunt:  max # of units to replenish to the location 
 *
 *  If pctflg is set, then quantities ALWAYS apply to a location 
 *  regardless of how the configuration was specified:
 *         minunt:  min % of location used before replenishment occurs
 *         maxunt:  max % of a location that may be used for this part
 *
 *
 *  maxlocs:  only useful when considering area level configs - this limits
 *            number of locations to be used by product.  For example, even
 *            if we are under our min threshold for the area, but we currently
 *            have product spread in more locations than what maxlocs 
 *            specifies, then we will postpone a replenishment due to the
 *            maxloc criteria being violated.
 *
 */


long sint_RplCallGenerateReplenishment( char *arecod, char *stoloc,
				      char *prtnum,
				      char *prt_client_id,
				      char *invsts,
				      long quantity,
				      char *vc_lotnum,	/* kjh */	
				      long vc_untpak, 	/* kjh */
    	    	            	      long match_lotnum_only,	/* kjh */
    	    	            	      long match_untpak_only,	/* kjh */
    	    	            	      long match_none,	/* kjh */
				      RETURN_STRUCT **TmpRes)
{
    char *cptr;
    char buffer[2000];
    char tmpstr[200];
    char distinctBuffer[500];
    mocaDataRes *res, *invres;
    mocaDataRow *row, *invrow;
    
    long ret_status;

    RETURN_STRUCT *CmdRes;

    *TmpRes = NULL;

    /* For grins, we need to make sure we only send stuff to the location
       that can be stored there...the mixing rules at the destination
       can influence what we ask for here...i.e. if we have lot controlled
       part at a location and we don't mix lots, then we'd better be 
       asking for inventory that matches the stuff at the destination */

    sprintf(buffer, 
	    "get inventory mixing rule where stoloc = '%s'", stoloc);
    CmdRes = NULL;

    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, &CmdRes);
	misTrc(T_FLOW, "Error determining mixing rules at stoloc: %s",
	       stoloc);
	return(ret_status);
    }
    res = srvGetResults(CmdRes);
    row = sqlGetRow(res);

    memset(distinctBuffer, 0, sizeof(distinctBuffer));
    if (!sqlIsNull(res, row, "selectlist"))
	strcpy(distinctBuffer, sqlGetString(res, row, "selectlist"));

    if (strlen(distinctBuffer) > 0)
    {
	misTrc(T_FLOW, 
	       "Attempting to determine inventory "
	       "values for allocation request");
	sprintf(buffer,
		"select distinct %s "
		"  from invdtl, invsub, invlod "
		" where invlod.stoloc = '%s' "
		"   and invlod.lodnum = invsub.lodnum "
		"   and invsub.subnum = invdtl.subnum ",
		distinctBuffer, stoloc);
	ret_status = sqlExecStr(buffer, &invres);
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(invres);
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return(ret_status);
	}

	memset(distinctBuffer, 0, sizeof(distinctBuffer));
	if (ret_status == eOK)
	{
	    misTrc(T_FLOW, 
		   "inventory already exists at destination - must request"
		   " specific inventory attributes");

	    invrow = sqlGetRow(invres);
	    /* go through columns and build where clause with values */

	    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	    {
		cptr = sqlGetString(res, row, "column");
		if (strncmp(cptr, "orgcod", 6) == 0 ||
		    strncmp(cptr, "lotnum", 6) == 0 ||
		    strncmp(cptr, "revlvl", 6) == 0)
		{
		    sprintf(tmpstr, "and %s = '%s'",
			    cptr, sqlGetString(invres, invrow, cptr));
		    strcat(distinctBuffer, tmpstr);
		}
		else if (strncmp(cptr, "untpak", 6) == 0 ||
			 strncmp(cptr, "untcas", 6) == 0)
		{
		    sprintf(tmpstr, " and %s = '%d' ",
			    cptr, sqlGetLong(invres, invrow, cptr));
		    strcat(distinctBuffer, tmpstr);
		}
	    }
	}
	else
	{
	    misTrc(T_FLOW, 
		   "No inventory exists at destination - no need"
		   " to request specific attributes");
	}
	sqlFreeResults(invres);
    }

    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    sqlSetSavepoint("before_generate_replenishment");
    if(match_none)
    {	
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' and arecod='%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
	    ALLOCATE_TOPOFF_REPLEN, stoloc, arecod, distinctBuffer);
    }
    else if(match_untpak_only)
    {
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and untpak = %d "	/* kjh */	
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' and arecod='%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_untpak,	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, arecod, distinctBuffer);
    }
    else if(match_lotnum_only)
    {
    	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and lotnum = '%s' "	/* kjh */
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' and arecod='%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_lotnum, 	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, arecod, distinctBuffer);
    }
    else	/* match both */
    {
	sprintf(buffer,
	    "generate replenishment "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' "
	    "   and invsts = '%s' "
	    "   and srcqty = %d "
	    "   and lotnum = '%s' "	/* kjh */
	    "   and untpak = %d "	/* kjh */	
	    "   and pcktyp = '%s' "
	    "   and dstloc = '%s' and arecod='%s' %s ",
	    prtnum, prt_client_id,
	    invsts, quantity, 
 	    vc_lotnum, vc_untpak,	/* kjh */	
	    ALLOCATE_TOPOFF_REPLEN, stoloc, arecod, distinctBuffer);
    }
    CmdRes = NULL;

    ret_status = srvInitiateCommand(buffer, &CmdRes); 
    if (ret_status != eOK)
    {
	sqlRollbackToSavepoint("before_generate_replenishment");
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(ret_status);
    }
    *TmpRes = CmdRes;
    return(eOK);
}

static void sProcessReplen(char *arecod, 
			   char *stoloc,
			   char *prtnum,
			   char *prt_client_id,
			   char *invsts,
			   char *vc_lotnum, 		/* kjh */
			   long *vc_untpak, 		/* kjh */
			   RETURN_STRUCT **CmdRes)
{
    char buffer[2000];
    long ret_status;
    long quantity;
    mocaDataRes *res;
    mocaDataRow *row;
    POLICY_INFO *policyInfo;
    long rplcnt;
    long match_lotnum_only = BOOLEAN_FALSE;	/* kjh */
    long match_untpak_only = BOOLEAN_FALSE;	/* kjh */
    long match_none = BOOLEAN_FALSE;		/* kjh */
    long count_rplcfg;				/* kjh */

    RETURN_STRUCT *TmpRes;

    policyInfo = int_RplGetPolicyInfo(POLVAR_REPLENISHMENT_QTR);

    /* First, we must go out and find what rplcfg records we have
       for this part combination... */

    *CmdRes = NULL;
    match_lotnum_only = BOOLEAN_FALSE;	/* kjh */
    match_untpak_only = BOOLEAN_FALSE;	/* kjh */
    match_none = BOOLEAN_FALSE;	/* kjh */

   sprintf(buffer,
	    "select am.loccod, am.pckcod, rp.* "
	    "  from rplcfg rp, aremst am"
	    " where am.arecod = '%s' "
	    "   and am.arecod = rp.arecod "
	    "   and rp.stoloc = '%s' "
	    "   and rp.prtnum = '%s' "
	    "   and rp.prt_client_id = '%s' "
	    "   and rp.invsts = '%s' "
	    "   and rp.vc_lotnum = '%s' " 		/* kjh */
	    "   and rp.vc_untpak = %ld ", 		/* kjh */
	    arecod, stoloc, prtnum, prt_client_id, invsts, vc_lotnum, vc_untpak); /* kjh */
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	misTrc(T_FLOW, "Failed to select rplcfg by stoloc (matching lotnum,untpak), try by area (with match)");
	
	sprintf(buffer,
		"select am.loccod, am.pckcod, rp.* "
		"  from rplcfg rp, aremst am "
		" where am.arecod = '%s' "
		"   and am.arecod = rp.arecod "
		"   and rp.stoloc is null "
		"   and rp.prtnum = '%s' "
		"   and rp.prt_client_id = '%s' "
		"   and rp.invsts = '%s' "
	    	"   and rp.vc_lotnum = '%s' " 		/* kjh */
	    	"   and rp.vc_untpak = %ld ", 		/* kjh */
		arecod, prtnum, prt_client_id, invsts, vc_lotnum, vc_untpak); /* kjh */
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	  sqlFreeResults(res);

	  /**********************************************************************************/
    	  /* Find number of rplcfg's for part that do not have lot or untpak specified  */
	  /**********************************************************************************/

	  sprintf(buffer,
		"select count(*) count_rplcfg "
		"  from rplcfg "
		" where prtnum = '%s' "
		"   and (vc_lotnum is null or vc_untpak is null) ",
          	prtnum);

	  ret_status = sqlExecStr(buffer, &res);

	  if (ret_status != eOK)
	  {
	      sqlFreeResults(res);
	      misTrc(T_FLOW, 
		"ERROR:%d counting rplcfg records found for part...exiting",ret_status);
	      return;
	  }

	  row = sqlGetRow(res);
    	  count_rplcfg = sqlGetLong(res, row, "count_rplcfg");

    	  /* Free Results */
    	  sqlFreeResults(res);

	  if(count_rplcfg>0)
	  {
	    misTrc(T_FLOW, "Rplcfg exists for part with combo of nulls;try stoloc with null lotnum and null untpak");
   
    	    match_none = BOOLEAN_TRUE;	
	    sprintf(buffer,
	    	"select am.loccod, am.pckcod, rp.* "
	    	"  from rplcfg rp, aremst am"
	    	" where am.arecod = '%s' "
	    	"   and am.arecod = rp.arecod "
	    	"   and rp.stoloc = '%s' "		/* stoloc */
	    	"   and rp.prtnum = '%s' "
	    	"   and rp.prt_client_id = '%s' "
	    	"   and rp.invsts = '%s' "
	    	"   and rp.vc_lotnum is null " 		/* check for lotnum to be null */
	    	"   and rp.vc_untpak is null ", 	/* check for untpak to be null */
	    	arecod, stoloc, prtnum, prt_client_id, invsts); 
    	    ret_status = sqlExecStr(buffer, &res);
    	    if (ret_status != eOK)
    	    {
		sqlFreeResults(res);
		misTrc(T_FLOW, "Failed rplcfg by stoloc (with nulls), try by area (with nulls)");

		sprintf(buffer,
		    "select am.loccod, am.pckcod, rp.* "
		    "  from rplcfg rp, aremst am "
		    " where am.arecod = '%s' "
		    "   and am.arecod = rp.arecod "	/* area */
		    "   and rp.stoloc is null "
		    "   and rp.prtnum = '%s' "
		    "   and rp.prt_client_id = '%s' "
		    "   and rp.invsts = '%s' "
	    	    "   and rp.vc_lotnum is null " 	/* check for lotnum to be null */
	    	    "   and rp.vc_untpak is null ", 	/* check for untpak to be null */
		    arecod, prtnum, prt_client_id, invsts); 
		ret_status = sqlExecStr(buffer, &res);
		if (ret_status != eOK)
		{
	    	    sqlFreeResults(res);
		    misTrc(T_FLOW, "Failed rplcfg by area (with nulls), try by stoloc (match untpak only)");
    	    	    match_none = BOOLEAN_FALSE;	
    	    	    match_untpak_only = BOOLEAN_TRUE;	
   		    sprintf(buffer,
	    		"select am.loccod, am.pckcod, rp.* "
	    		"  from rplcfg rp, aremst am"
	    		" where am.arecod = '%s' "
	    		"   and am.arecod = rp.arecod "
	    		"   and rp.stoloc = '%s' "		/* stoloc */
	    		"   and rp.prtnum = '%s' "
	    		"   and rp.prt_client_id = '%s' "
	    		"   and rp.invsts = '%s' "
	    		"   and rp.vc_lotnum is null " 		/* lotnum is null */
	    		"   and rp.vc_untpak = %ld ", 		/* match untpak */
	    		arecod, stoloc, prtnum, prt_client_id, invsts, vc_untpak);
    	 	    ret_status = sqlExecStr(buffer, &res);
    		    if (ret_status != eOK)
    		    {
			sqlFreeResults(res);
			misTrc(T_FLOW, "Failed rplcfg by stoloc (match untpak), try by area (match untpak only)");
	
			sprintf(buffer,
			    "select am.loccod, am.pckcod, rp.* "
			    "  from rplcfg rp, aremst am "
			    " where am.arecod = '%s' "
			    "   and am.arecod = rp.arecod "	/* area */
			    "   and rp.stoloc is null "
			    "   and rp.prtnum = '%s' "
			    "   and rp.prt_client_id = '%s' "
			    "   and rp.invsts = '%s' "
	    		    "   and rp.vc_lotnum is null " 	/* lotnum is null */
	    		    "   and rp.vc_untpak = %ld ", 	/* match untpak */
			    arecod, prtnum, prt_client_id, invsts, vc_untpak); 
			ret_status = sqlExecStr(buffer, &res);
			if (ret_status != eOK)
			{
	    		    sqlFreeResults(res);
		    	    misTrc(T_FLOW, "Failed rplcfg by area (match untpak), try by stoloc (match lotnum only)");
    	    	            match_untpak_only = BOOLEAN_FALSE;	
    	    	            match_lotnum_only = BOOLEAN_TRUE;
   		    	    sprintf(buffer,
	    			"select am.loccod, am.pckcod, rp.* "
	    			"  from rplcfg rp, aremst am"
	    			" where am.arecod = '%s' "
	    			"   and am.arecod = rp.arecod "
	    			"   and rp.stoloc = '%s' "	/* stoloc */
	    			"   and rp.prtnum = '%s' "
	    			"   and rp.prt_client_id = '%s' "
	    			"   and rp.invsts = '%s' "
	    			"   and rp.vc_lotnum = '%s' " 	/* match lotnum */	
	    			"   and rp.vc_untpak is null ", /* untpak is null */ 
	    			arecod, stoloc, prtnum, prt_client_id, invsts, vc_lotnum);
    	 	    	    ret_status = sqlExecStr(buffer, &res);
    		    	    if (ret_status != eOK)
    		    	    {
				sqlFreeResults(res);
				misTrc(T_FLOW, "Failed rplcfg by stoloc (match lotnum), try by area (match lotnum only)");
	
				sprintf(buffer,
			    		"select am.loccod, am.pckcod, rp.* "
			    		"  from rplcfg rp, aremst am "
			    		" where am.arecod = '%s' "	/* area */
			    		"   and am.arecod = rp.arecod "
			    		"   and rp.stoloc is null "
			    		"   and rp.prtnum = '%s' "
			    		"   and rp.prt_client_id = '%s' "
			    		"   and rp.invsts = '%s' "
	    				"   and rp.vc_lotnum = '%s' " 	/* match lotnum */
	    				"   and rp.vc_untpak is null ", /* untpak is null */
			    		arecod, prtnum, prt_client_id, invsts, vc_lotnum); 
				ret_status = sqlExecStr(buffer, &res);
				if (ret_status != eOK)
				{
	    		    	    sqlFreeResults(res);
	    			    misTrc(T_FLOW, "No rplcfg records found for part...exiting");
	    			    return;
				} /* end area with lotnum only */
			    } /* end stoloc with lotnum only */
			} /* end area with untpak only */
		    } /* end stoloc with untpak only */
		} /* end area with nulls */
	    } /* end stoloc with nulls */
	  }
	  else
	  {
	    misTrc(T_FLOW, "Counted zero rplcfg records found for part...exiting");
	    return;
	  } /* end count of rplcfg for part with either lotnum or untpak being null */
	} /* end area match lot and pak */
    } /* end stoloc match lot and pak */

    /* For a given replenishment record, we really are just concerned 
       with determining how much we need to request to top off (or 
       if we need to top off)... */
    
    misTrc(T_FLOW, "Found replenishment matches...processing. ");
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	/* Go through each record and attempt to process the 
	   replenishment request... */
	/* We need to determine if we are below our threshold... */

	if (int_RplNeedsReplenishment(stoloc,
				      arecod,
				      sqlGetString(res, row, "loccod"),
				      sqlGetString(res, row, "pckcod"),
				      !sqlIsNull(res, row, "stoloc"),
				      sqlGetLong(res, row, "maxloc"),
				      sqlGetLong(res, row, "minunt"),
				      sqlGetLong(res, row, "maxunt"),
				      sqlGetLong(res, row, "pctflg"),
				      prtnum, 
				      prt_client_id,
				      invsts,
				      &quantity))
	{
	    
	    if (policyInfo->max_active > 0)
	    {
		int_RplGetActiveRplCount(&rplcnt);
		
		if (rplcnt >= policyInfo->max_active)
		{
		    sqlFreeResults(res);
		    misTrc(T_FLOW, 
			   "Maximum number of replenishments (%d) "
			   "already outstanding - exiting", 
			   policyInfo->max_active);
		    return;
		}
	    }

	    /* we need to replenish....go for it... */
	    TmpRes = NULL;
	    ret_status = sint_RplCallGenerateReplenishment(arecod, stoloc, 
							  prtnum,
							  prt_client_id,
							  invsts,
							  quantity,
							  vc_lotnum,	/* kjh */	
							  vc_untpak, 	/* kjh */
    	    	            				  match_lotnum_only,	/* kjh */
    	    	            				  match_untpak_only,	/* kjh */
    	    	            				  match_none,	/* kjh */
							  &TmpRes); 
	    if (ret_status != eOK)
	    {
		misTrc(T_FLOW, 
		       "Call to replenish inventory failed: %d",
		       ret_status);
		srvFreeMemory(SRVRET_STRUCT, TmpRes);
	    }
	    else
	    {
		if (*CmdRes == NULL)
		{
		    *CmdRes = TmpRes;
		}
		else
		{
		    srvCombineResults(CmdRes, &TmpRes);
		}
	    }
	}
    }
    return;
}

LIBEXPORT 
RETURN_STRUCT *varCreateTriggeredReplenishment(char *arecod_i, 
					       char *stoloc_i, 
					       char *prtnum_i, 
					       char *prt_client_id_i,
					       char *invsts_i,
					       char *lodnum_i,
					       char *subnum_i,
					       char *dtlnum_i,
					       char *lotnum_i, /* kjh */
					       long *untpak_i) /* kjh */
{

    long ret_status;

    char buffer[2000];
    char stoloc[STOLOC_LEN+1];
    char arecod[ARECOD_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char invsts[INVSTS_LEN+1];
    char lodnum[LODNUM_LEN+1];
    char subnum[SUBNUM_LEN+1];
    char dtlnum[DTLNUM_LEN+1];
    char vc_lotnum[LOTNUM_LEN+1];	/* kjh */
    long vc_untpak=0 ;			/* kjh */

    POLICY_INFO *policyInfo;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes, *TmpRes;

    policyInfo = int_RplGetPolicyInfo(POLVAR_REPLENISHMENT_QTR);

    if (!policyInfo->is_installed)
    {
	misTrc(T_FLOW, "Triggered replenishments not installed - exiting");
	return(srvSetupReturn(eOK, ""));
    }

    memset(stoloc, 0, sizeof(stoloc));
    memset(arecod, 0, sizeof(arecod));
    
    if (stoloc_i)
	strncpy(stoloc, stoloc_i, STOLOC_LEN);
    else
        return APPMissingArg("stoloc");

    if (arecod_i)
	strncpy(arecod, arecod_i, ARECOD_LEN);
    else
    {
	sprintf(buffer,
		"select arecod from locmst where stoloc = '%s'",
		stoloc);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    misTrc(T_FLOW, "Error determining area code!");
	    return(srvSetupReturn(ret_status, ""));
	}
	strncpy(arecod, 
		sqlGetString(res, sqlGetRow(res), "arecod"), ARECOD_LEN);

	sqlFreeResults(res);
    }

    misTrc(T_FLOW, " check if arecod= '%s' is replenishable",arecod);
    /* We should check the area information to make sure we 
       even consider this area for replens... */
    if (!int_RplAreaIsReplenishable(arecod))
    {
	return(srvSetupReturn(eOK, ""));
    }
    misTrc(T_FLOW, " arecod= '%s' must be ok to replenish",arecod);

    /* Ok...we think we need to potentially replenish...let's determine
       what part, etc, that we have...  */

    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(lodnum, 0, sizeof(lodnum));
    memset(subnum, 0, sizeof(subnum));
    memset(dtlnum, 0, sizeof(dtlnum));
    memset(invsts, 0, sizeof(invsts));
    memset(vc_lotnum, 0, sizeof(vc_lotnum)); 	/* kjh */
    if (untpak_i != NULL)
        vc_untpak = *untpak_i;		/* kjh */

    CmdRes = NULL;
    if (prtnum_i && strlen(prtnum_i) && invsts_i && strlen(invsts_i))
    {
	strncpy(prtnum, prtnum_i, PRTNUM_LEN);

        misTrc(T_FLOW, " checking prt_client_id");
	/* Get the part client Id */
	ret_status = appGetClient(prt_client_id_i, prt_client_id);
	if (eOK != ret_status)
	    return (srvSetupReturn(ret_status, ""));

	strncpy(invsts, invsts_i, INVSTS_LEN);
    	
	if (lotnum_i && strlen(lotnum_i) )		/* kjh */
        {	
		strncpy(vc_lotnum, lotnum_i, LOTNUM_LEN);    /* kjh */
    		misTrc(T_FLOW, "passed params vc_lotnum: %s; vc_untpak: %ld",   /* kjh */
	       		vc_lotnum, vc_untpak);
	}

    	misTrc(T_FLOW, "prior to sProcessReplen..." );  /* kjh */

	sProcessReplen(arecod, 
		       stoloc,
		       prtnum,
		       prt_client_id,
		       invsts,
		       vc_lotnum,		/* kjh */
		       vc_untpak,		/* kjh */	
		       &CmdRes);
    	misTrc(T_FLOW, "after  sProcessReplen..." );  /* kjh */
    }
    else 
    {
	if (dtlnum_i && strlen(dtlnum_i))
	{
	    sprintf(buffer,
		    "select prtnum, prt_client_id, invsts, lotnum, untpak " /* kjh */
		    "  from invdtl "
		    " where dtlnum = '%s'",
		    dtlnum_i);
	}
	else if (subnum_i && strlen(subnum_i))
	{
	    sprintf(buffer,
		    "select distinct prtnum, prt_client_id, invsts, lotnum, untpak " /* kjh */
		    "  from invdtl "
		    " where subnum = '%s'",
		    subnum_i);
	}
	else if (lodnum_i && strlen(lodnum_i))
	{
	    sprintf(buffer,
		    "select distinct id.prtnum, id.prt_client_id, id.invsts, "
		    " id.lotnum, id.untpak "			/* kjh */	
		    "  from invdtl id, invsub ivs "
		    " where id.subnum = ivs.subnum "
		    "   and ivs.lodnum = '%s' ",
		    lodnum_i);
	}
	else
	{
            misTrc(T_FLOW, " base expects dtl,sub or lod if no prtnum/invsts");
	    return(srvSetupReturn(eINVALID_ARGS, ""));
	}
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(srvSetupReturn(eOK, ""));
	}

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
	    TmpRes = NULL;
	    sProcessReplen(arecod, 
			   stoloc,
			   sqlGetString(res, row, "prtnum"),
			   sqlGetString(res, row, "prt_client_id"),
			   sqlGetString(res, row, "invsts"),
			   sqlGetString(res, row, "lotnum"),	/* kjh */
			   sqlGetLong(res, row, "untpak"),	/* kjh */
			   &TmpRes);

	    if (CmdRes == NULL)
		CmdRes = TmpRes;
	    else
		srvCombineResults(&CmdRes, &TmpRes);
	}
	sqlFreeResults(res);
    }
    if (CmdRes == NULL)
	return(srvSetupReturn(eOK, ""));
    else
	return(CmdRes);
}


