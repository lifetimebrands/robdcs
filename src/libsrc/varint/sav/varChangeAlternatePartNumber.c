static const char *rcsid = "$Id: varChangeAlternatePartNumber.c,v 1.3 2003/12/03 06:17:43 prod Exp $";
/*#START**********************************************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This functin generates a updatelist and wherelist for an update 
 *                  of a record in the var_ALT_PRTNUM table .Actual Update is done  by
 *                  Process Table Action (MCS Base Component ) 
 *#END***********************************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>



LIBEXPORT 
RETURN_STRUCT *varChangeAlternatePartNumber(char *altnum_i,
				            char *altlot_i,
					    char *prtnum_i)
{
    RETURN_STRUCT *CurPtr;
    char  tablename[100];
    char  tmpvar[1000];
    char  *wherelist;
    char  *updatelist;
    char  altnum[PRTNUM_LEN] ;
    char  altlot[LOTNUM_LEN+1] ;
    char  prtnum[PRTNUM_LEN] ;
    

    CurPtr = NULL;

    if (!altnum_i )
        return (APPMissingArg("altnum"));
    
    misTrimcpy(altnum, altnum_i, PRTNUM_LEN );

    if (!prtnum_i )
    	return (APPMissingArg("prtnum"));
    
    misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN );

    if (!altlot_i )
    	return (APPMissingArg("altlot"));
    
    misTrimcpy(altlot, altlot_i, LOTNUM_LEN );

    strcpy(tablename, "var_alt_prtnum");
    updatelist = wherelist = NULL;

     /* First, let's do the where clause ... */
    sprintf(tmpvar,"altnum  = '%s' and vc_altlot = '%s' ",altnum, altlot);

    if(!misDynStrcat(&wherelist,tmpvar))
    {
	if (wherelist) free(wherelist);
	return(srvSetupReturn(eNO_MEMORY,""));
    }
    

    sprintf(tmpvar, " altnum = '%s',",altnum) ;
    if(!misDynStrcat(&updatelist,tmpvar))
    {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
    }

    sprintf(tmpvar, " vc_altlot = '%s',",altlot) ;
    if(!misDynStrcat(&updatelist,tmpvar))
    {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
    }

    sprintf(tmpvar, " prtnum  = '%s' ",prtnum) ;
    if(!misDynStrcat(&updatelist,tmpvar))
    {
	if (wherelist) free(wherelist);
	if (updatelist) free(updatelist);
	return(srvSetupReturn(eNO_MEMORY, ""));
    }


    /* Publish out the table, columns, and values for insert by */
    /* intProcessTableInsert (PROCESS TABLE INSERT) */

   CurPtr = srvResultsInit(eOK,
                            "acttyp", COMTYP_CHAR, 1,
                            "tblnam", COMTYP_CHAR, strlen(tablename),
                            "updlst", COMTYP_CHAR,
				 updatelist ? strlen(updatelist) : 0,
                            "whrlst", COMTYP_CHAR,
				 wherelist ? strlen(wherelist) : 0,
			    NULL); 


   srvResultsAdd(CurPtr,
		  ACTTYP_UPDATE,
		  tablename,
		  updatelist ? updatelist : "",
		  wherelist ? wherelist : "");

    if (updatelist) free(updatelist);
    if (wherelist) free(wherelist);

    /* Finally, don't forget to free our results set */
   
    return (CurPtr);
}

