/*#START***********************************************************************

 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /cvs/les/wm/src/libsrc/dcsint/intCheckReceiveLine.c,v $
 *  $Revision: 1.8 $
 *
 *  Application:  intlib
 *  Created:      15-Apr-1996
 *  $Author: bgrady $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
#include "intlib.h"

/*
 **     intCheckReceiveLine
 */
LIBEXPORT 
RETURN_STRUCT *varCheckReceiveLine(
			  char *srcloc_i,
			  char *dstloc_i,
			  char *client_id_i,
			  char *supnum_i,
			  char *invnum_i,
			  char *lodnum_i,
			  char *subnum_i,
			  char *dtlnum_i,
                          char *trknum_i)
{
    RETURN_STRUCT *CurPtr;
    char dstloc[STOLOC_LEN + 1];

    /* If this is coming from RF, the destination location could
     * be the expected receipt location.  If that is the case, we 
     * should also have a truck number that we can use
     */

    /* Copy the dest loc first, assume that it is the truck */
    misTrimcpy(dstloc, dstloc_i, STOLOC_LEN);

    if ((trknum_i && misTrimLen(trknum_i, TRKNUM_LEN) > 0) &&
        (strncmp(dstloc_i, STOLOC_PERM_EXPRCP, STOLOC_LEN) == 0))
    {
        misTrimcpy(dstloc, trknum_i, STOLOC_LEN);
    }

    CurPtr = NULL;
    CurPtr = (RETURN_STRUCT *) var_UpdateReceiveLine(srcloc_i,
						     dstloc,
						     client_id_i,
						     supnum_i, 
						     invnum_i,
						     lodnum_i, subnum_i, 
						     dtlnum_i, "INC");


    return (CurPtr);
}
