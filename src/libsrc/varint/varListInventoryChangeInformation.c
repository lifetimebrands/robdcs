static const char *rcsid = "$Id: varListInventoryChangeInformation.c,v 1.27 2005/03/12 03:04:07 mzais Exp $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *  
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <srvlib.h>

#include "intlib.h"
#include "intCmpSrchLib.h"

/* Typedef for Component Search functionality */

typedef struct _data_node
{
    long sub_cmpflg;
    char cmpkey[CMPKEY_LEN+1];
    char sub_cmpkey[CMPKEY_LEN+1];
    char dtlnum[DTLNUM_LEN+1];
} DATA_NODE;

/*------------------------------------------------------------------------*/
/* sProcessDataNode                                                       */
/* The component search logic function was getting too big, so I made it  */
/* modular.                                                               */
/*------------------------------------------------------------------------*/
static int sProcessDataNode(mocaDataRes **ptr, CS_FIFO *queue,
                            DATA_NODE *node)
{
    int ret_stat = eOK;
    mocaDataRes *res = NULL;
    mocaDataRow *row = NULL;
    char sqlbuffer[3000];

    DATA_NODE *newnode = NULL;

    /* If the detail is not NULL, this is actual inventory, and we have */
    /* to query the resultset info ( arecod, stoloc, lodnum,            */
    /* subnum_subucc, img_col ) and append it to the result set.        */
    if (strlen(node->dtlnum) > 0)
    {

       /* Perform SQL query for result set here */
       sprintf(sqlbuffer, 
               "select am.bldg_id, "
               "       lm.arecod, "
               "       lm.stoloc, "
               "       lm.locsts, "
               "       l.lodnum, "
               "       s.subnum, "
               "       d.dtlnum, "
               "       d.prtnum, "
               "       d.prt_client_id, "
               "       d.lotnum, "
               "       d.orgcod, "
               "       d.revlvl, "
               "       d.invsts, "
               "       d.untqty, "
               "       d.catch_qty, "
               "       p.catch_unttyp, "
               "       to_number(d.alcflg) alcflg, "
               "       d.age_pflnam, "
               "       d.fifdte, "
               "       d.mandte, "
               "       d.expire_dte, "
               "       '' trknum, "
               "       '' supnum, "
               "       '' invnum, "
               "       '' wkonum, "
               "       '' client_id, "
               "       '' wkorev, "
               "       '' wkolin, "
               "       to_number(null) seqnum, "
               "       1 kitcmpflg, "
               "       '' ftpcod, "
               "       bm.wh_id, "
               "       0 in_transit_flg "
               "  from bldg_mst bm, "
               "       aremst am, "
               "       locmst lm, "
               "       invlod l, "
               "       invsub s, "
               "       invdtl d, "
               "       prtmst p  "
               " where bm.bldg_id = am.bldg_id "
               "   and am.arecod = lm.arecod "
               "   and lm.stoloc = l.stoloc "
               "   and l.lodnum = s.lodnum "
               "   and s.subnum = d.subnum "
               "   and d.prtnum = p.prtnum "
               "   and d.prt_client_id = p.prt_client_id "
               "   and d.wrkref is null "
               "   and d.ship_line_id is null "
               "   and d.dtlnum = '%s' ",
               node->dtlnum);

        ret_stat = sqlExecStr(sqlbuffer, &res);
        if ((ret_stat != eOK) && (ret_stat != eDB_NO_ROWS_AFFECTED))
        {
            /* We have an error in the query.  Fail out */
            misTrc(T_FLOW, "intListInventorySubload: Failed to query inventory "
                           "with error %ld.  Aborting", ret_stat);
            sqlFreeResults(res);
            return (ret_stat);
        }
        /* If ret_stat is eDB_NO_ROWS_AFFECTED, just go on to the next */
        /* 'if' statement.                                             */
        if (ret_stat == eOK)
        {
            /* We have one detail.  Get the row and add it to the      */
            /* results set.                                            */
            misTrc(T_FLOW, "Row queried. Adding to Resultset");

            /* Add results to mocaDataRes var *ptr here */

            misTrc(T_FLOW, "Combining resultsets");

            ret_stat = sqlCombineResults(ptr, &res);
            if (ret_stat != eOK)
            {
                misTrc(T_FLOW, "Failed to combine moca data result sets "
                               "due to error %ld ", ret_stat);
                sqlFreeResults(res);
                return(ret_stat);
            }
        }

    }

    /* If the the sub_cmpflg is TRUE, then this is a nested component.   */
    /* Find the parent CMPHDR/CMPDTL/(+)INVDTL and push it on the queue. */
    if (node->sub_cmpflg == BOOLEAN_TRUE)
    {
        sprintf(sqlbuffer,
                "select ch.cmpkey cmpkey, ch.sub_cmpflg sub_cmpflg, "
                "       cd.sub_cmpkey sub_cmpkey, d.dtlnum dtlnum "
                "  from cmpdtl cd, cmphdr ch, invdtl d"
                " where cd.cmpkey     = ch.cmpkey "
                "   and cd.cmpkey  =(+) d.cmpkey "
                "   and cd.sub_cmpkey = '%s' ", node->cmpkey);

        ret_stat = sqlExecStr(sqlbuffer, &res);
        if ((ret_stat != eOK) && (ret_stat != eDB_NO_ROWS_AFFECTED))
        {
            /* we have an error.  Abort */
            misTrc(T_FLOW, "intListInventorySubload: SQL query failed with "
                           "error %ld Aborting.", ret_stat);
            sqlFreeResults(res);
            return(ret_stat);
        }
        for (row = sqlGetRow(res); row; row=sqlGetNextRow(row))
        {
            /* There really should only be one row; this loop put here */
            /* for completeness                                        */
            newnode = calloc(1, sizeof(DATA_NODE));
            if (newnode == (DATA_NODE *)NULL)
            {
                misTrc(T_FLOW, "intListInventoryAttributeInformation: "
                               "Memory Error ");
                sqlFreeResults(res); res = NULL;
                csl_freeFIFOQueue(queue);
                ret_stat = eINT_LIST_ERROR;
                break;
            }
            newnode->sub_cmpflg = sqlGetLong(res, row, "sub_cmpflg");
            strcpy(newnode->cmpkey, sqlGetString(res, row, "cmpkey"));
            if (!sqlIsNull(res, row, "sub_cmpkey"))
            {
                strcpy(newnode->sub_cmpkey,
                       sqlGetString(res, row, "sub_cmpkey"));
            }
            if (!sqlIsNull(res, row, "dtlnum"))
            {
                strcpy(newnode->dtlnum, sqlGetString(res, row, "dtlnum"));
            }

            ret_stat = csl_pushNodeFIFOQueue(queue, newnode);
            if (ret_stat != eOK)
            {
                misTrc(T_FLOW, "Failed to allocate memory for list");
                sqlFreeResults(res); res = NULL;
                csl_freeFIFOQueue(queue);
                break;
            }
        }
	sqlFreeResults(res);
    }

    /* If the detail number is NULL, but the sub_cmpflg is FALSE, then   */
    /* this part does not exist in inventory, and we can skip it.  Just  */
    /* continue (which essentially means, do nothing).                   */
    return (ret_stat);
}

/*---------------------------------------------------------------------*/
/* sDoComponentSearch                                                  */
/* This function searches all components for a match, and builds a list*/
/* of invdtl's that match were constructed with components that match  */
/* those parameters.                                                   */
/*---------------------------------------------------------------------*/
static int sDoComponentSearch( mocaDataRes **res, char *parm_stoloc,
                               char *parm_lodnum, char *parm_subnum,
                               char *parm_dtlnum, char *parm_prtnum,
                               char *parm_prt_client_id, char *parm_lotnum,
                               char *parm_orgcod, char *parm_revlvl,
                               char *parm_invsts, char *parm_trknum,
                               char *parm_supnum, char *parm_invnum)
{
    mocaDataRes *res2;
    mocaDataRow *row2;
    CS_FIFO *CmpQueue;

    char sqlbuffer[2048];
    char tmp_sqlbuffer[2048];
    char tmpbuffer[512];
    char tmpbuffer1[512];
    char tmpbuffer2[512];

    int ret_stat = eOK;
    DATA_NODE *node;

    misTrc(T_FLOW, "Entering Component Search");

    memset(tmp_sqlbuffer, 0, sizeof(tmp_sqlbuffer));
    memset(tmpbuffer, 0, sizeof(tmpbuffer));
    memset(tmpbuffer1, 0, sizeof(tmpbuffer1));
    memset(tmpbuffer2, 0, sizeof(tmpbuffer2));
    
    if (!(strlen(parm_prtnum)||strlen(parm_prt_client_id)||strlen(parm_dtlnum)
        ||strlen(parm_subnum)||strlen(parm_lotnum)||strlen(parm_orgcod)
        ||strlen(parm_revlvl)||strlen(parm_invsts)))
    {
        /* If no parms are passed in, just return eOK - the inventory already */
        /* in the list contains all the stuff we'd return.                    */
        /* In fact, the only parms we care about are the ones on the cmpdtl   */
        /* table - the prtnum, prt_client_id, dtlnum, subnum, lotnum, orgcod  */
        /* revlvl, invsts                                                     */
        *res = NULL;
        misTrc(T_FLOW, "No parms passed in - exiting Component Search");
        return(eOK);
    }

    /* Initialize FIFO stack */
    CmpQueue = csl_initializeFIFOQueue();
    

    /* fixed portion of select */
    strcpy(tmp_sqlbuffer,
           "select ch.sub_cmpflg sub_cmpflg, ch.cmpkey cmpkey, "
           "       cd.sub_cmpkey sub_cmpkey, NULL dtlnum "
           "  from cmphdr ch, cmpdtl cd "
           " where ch.cmpkey = cd.cmpkey "
           "   and ch.cmpkey not in (select distinct cmpkey "
           "                           from invdtl "
           "                          where cmpkey is not NULL) "
           " %s "
           "union "
           "select ch.sub_cmpflg sub_cmpflg, ch.cmpkey cmpkey, "
           "       cd.sub_cmpkey sub_cmpkey, d.dtlnum dtlnum "
           "  from cmphdr ch, cmpdtl cd, invdtl d, invsub s, "
           "       invlod l, locmst lm, aremst am, bldg_mst bm "
           " where ch.cmpkey = cd.cmpkey "
           "   and ch.cmpkey = d.cmpkey "
           "   and d.subnum  = s.subnum "
           "   and s.lodnum  = l.lodnum "
           "   and l.stoloc  = lm.stoloc "
           "   and lm.arecod = am.arecod "
           "   and am.bldg_id = bm.bldg_id "
           " %s "
           );

   if (parm_stoloc && strlen(parm_stoloc))
   {
       sprintf(tmpbuffer, "   and l.stoloc like '%s' ", parm_stoloc);
       strcat (tmpbuffer2, tmpbuffer);
   }
 
   if (parm_prtnum && strlen(parm_prtnum))
   {
       sprintf(tmpbuffer, "   and cd.prtnum like '%s' ", parm_prtnum);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_prt_client_id && strlen(parm_prt_client_id))
   {
       sprintf(tmpbuffer, "   and cd.prt_client_id like '%s' ",
               parm_prt_client_id);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_dtlnum && strlen(parm_dtlnum))
   {
       sprintf(tmpbuffer, "   and cd.dtlnum like '%s' ", parm_dtlnum);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }
 
   if (parm_subnum && strlen(parm_subnum))
   {
       sprintf(tmpbuffer, "   and cd.subnum like '%s' ", parm_subnum);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_lotnum && strlen(parm_lotnum))
   {
       sprintf(tmpbuffer, "   and cd.lotnum like '%s' ", parm_lotnum);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_orgcod && strlen(parm_orgcod))
   {
       sprintf(tmpbuffer, "   and cd.orgcod like '%s' ", parm_orgcod);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_revlvl && strlen(parm_revlvl))
   {
       sprintf(tmpbuffer, "   and cd.revlvl like '%s' ", parm_revlvl);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

   if (parm_invsts && strlen(parm_invsts))
   {
       sprintf(tmpbuffer, "   and cd.invsts like '%s' ", parm_invsts);
       strcat (tmpbuffer1, tmpbuffer);
       strcat (tmpbuffer2, tmpbuffer);
   }

    /* Create the sql statement */
    sprintf(sqlbuffer, tmp_sqlbuffer, tmpbuffer1, tmpbuffer2);

    misTrc(T_FLOW, "Processing SQL Statement %s", sqlbuffer);

    /* Execute query */
    ret_stat = sqlExecStr(sqlbuffer, &res2);

    if (ret_stat != eOK)
    {
        sqlFreeResults(res2);
        if (ret_stat == eDB_NO_ROWS_AFFECTED)
        {
            *res = NULL;
            return(eOK);
        }
        misTrc(T_FLOW, "Failed SQL Query with error %ld.",ret_stat);
        return(ret_stat);
    }

    misTrc(T_FLOW, 
           "sDoComponentSearch: %d component possibilities found",
           sqlGetNumRows(res2));

    /* Here, we have the results. */
    /* We queried the fields: sub_cmpflg, cmpkey, sub_cmpkey, dtlnum  */
    /* Loop thru the result set, adding the results to the queue in a */
    /* FIFO fashion                                                   */

    for (row2 = sqlGetRow(res2); row2; row2 = sqlGetNextRow(row2))
    {
        /* We have a dtlnum.... Add to list */
        node = calloc(1,sizeof(DATA_NODE));
        if (node == (DATA_NODE *)NULL)
        {
            misTrc(T_FLOW, "Failed to allocate memory for list");
            sqlFreeResults(res2); res2 = NULL;
            csl_freeFIFOQueue(CmpQueue);
            ret_stat = eINT_LIST_ERROR;
            break;
        }

        node->sub_cmpflg = sqlGetLong(res2, row2, "sub_cmpflg");
        strcpy(node->cmpkey, sqlGetString(res2, row2, "cmpkey"));
        if (!sqlIsNull(res2, row2, "sub_cmpkey"))
            strcpy(node->sub_cmpkey, sqlGetString(res2, row2, "sub_cmpkey"));
        if (!sqlIsNull(res2, row2, "dtlnum"))
            strcpy(node->dtlnum, sqlGetString(res2, row2, "dtlnum"));
        ret_stat = csl_pushNodeFIFOQueue(CmpQueue, node);
        if (ret_stat != eOK)
        {
            misTrc(T_FLOW, "Failed to allocate memory for list");
            sqlFreeResults(res2); res2 = NULL;
            csl_freeFIFOQueue(CmpQueue);
            break;
        }

    }

    sqlFreeResults(res2);

    if (csl_isFIFOQueueEmpty(CmpQueue) == BOOLEAN_TRUE)
        *res = NULL;
    else
    {
        while ((ret_stat = csl_popNodeFIFOQueue(CmpQueue, &node)) == eOK)
        {
            /* This condition should never happen, but if it does, we can */
            /* safely just continue.  The queue functions will prevent    */
            /* an infinite loop.                                          */
            if (node == (DATA_NODE *)NULL)
            {
                misTrc(T_FLOW, "intListInventoryAttributeInformation.c: "
                               "Null node in list while processing queue ");
                continue;
            }

            ret_stat = sProcessDataNode(res, CmpQueue, node);
            if ((ret_stat != eOK) && (ret_stat != eDB_NO_ROWS_AFFECTED))
            {
                /* Any debugging info was already logged */
                break;
            }
            free ((void *)node);
            node = (DATA_NODE *)NULL;
        }
    }

    if (ret_stat == eINT_NO_NODE_IN_LIST)
    {
        /* We don't have any nodes in the list.  Search done. */
        misTrc(T_FLOW, "Component Search completed");

        return(eOK);
    }
    else
    {
        misTrc(T_FLOW, "intListInventoryAttributeInformation: "
                       "Error accessing list: aborting");
        csl_dumpFIFOQueue(CmpQueue);
        csl_freeFIFOQueue(CmpQueue);

        return(ret_stat);
    }
}

static int sFindWkoDetails(mocaDataRes **res,
                           char *stoloc_i,
                           char *lodnum_i,
                           char *prtnum_i,
                           char *prt_client_id_i, 
                           char *lotnum_i,
                           char *orgcod_i,
                           char *revlvl_i,
                           char *invsts_i,
                           char *wkonum_i,
                           char *client_id_i,
                           char *wkorev_i)
{
    mocaDataRow    *tmprow;
    mocaDataRes    *tmpres;

    long status;
    char sqlBuffer[8192];
    char otherBuffer[2048];
    char prtBuffer[1024];
    char tmpBuffer[1024];
    char buildBuffer[1024];

    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char lotnum[LOTNUM_LEN+1];
    char orgcod[ORGCOD_LEN+1];
    char revlvl[REVLVL_LEN+1];
    char invsts[INVSTS_LEN+1];
    char stoloc[STOLOC_LEN+1];
    char lodnum[LODNUM_LEN+1];
    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char client_id[CLIENT_ID_LEN+1];

    int  multiple_rows = BOOLEAN_FALSE;

    misTrc(T_FLOW, "Searching for matching work order details.");

    memset (prtBuffer, 0, sizeof(prtBuffer));
    memset (prtnum, 0, sizeof(prtnum));
    memset (prt_client_id, 0, sizeof(prt_client_id));
    memset (lotnum, 0, sizeof(lotnum));
    memset (orgcod, 0, sizeof(orgcod));
    memset (revlvl, 0, sizeof(revlvl));
    memset (invsts, 0, sizeof(invsts));
    memset (stoloc, 0, sizeof(stoloc));
    memset (lodnum, 0, sizeof(lodnum));
    memset (wkonum, 0, sizeof(wkonum));
    memset (wkorev, 0, sizeof(wkorev));
    memset (client_id, 0, sizeof(client_id));

    /* untcas and untpak should be float type, or it will cause error. */
    sprintf (sqlBuffer, 
	     "select '' bldg_id, "
             "       '' arecod, "
             "       '' stoloc, "
             "       '' locsts, "
             "       '' lodnum, "
             "       '' subnum, "
             "       '' dtlnum, "
             "       prtnum, "
             "       prt_client_id, "
             "       lotnum, "
             "       orgcod, "
             "       revlvl, "
             "       invsts,"
             "       tot_dlvqty  untqty, " 
             "       to_number(null) catch_qty, "
             "       '' catch_unttyp,"
             "       to_number(null) alcflg, "
             "       '' age_pflnam, "
             "       to_date(null) fifdte, "
             "       to_date(null) mandte, "
             "       to_date(null) expire_dte, "
             "       '' trknum, "
             "       '' supnum, "
             "       '' invnum, "
             "       wkonum, "
             "       client_id, "
             "       wkorev, "
             "       wkolin, "
             "       to_number(seqnum) seqnum, "
             "       0 kitcmpflg, "
             "       '' ftpcod, "
             "       0.0 untcas, "
             "       0.0 untpak, "
             "       '' wh_id, "
             "       0 in_transit_flg "
             "  from wkodtl "
             " where tot_dlvqty > 0 "
             "   and linsts != '%s' ",
             WKO_LINSTS_COMPLETE
            );

    if (misTrimLen(wkonum_i, WKONUM_LEN))
    {
        misTrimcpy(wkonum, wkonum_i, WKONUM_LEN);

        memset(client_id, 0, sizeof(client_id));
        status = appGetClient(client_id_i, client_id);
        if (eOK != status)
        {
            memset(client_id, 0, sizeof(client_id));
            status = eOK;
        }
    }

    if (misTrimLen(wkorev_i, WKOREV_LEN))
    {
        misTrimcpy(wkorev, wkorev_i, WKOREV_LEN);
    }

    if (misTrimLen(lodnum_i, LODNUM_LEN))
    {
        /* If we have a load number, we try to find what work order it  */
        /* is tied to.  This can only happen if it's in a LINE stoloc.  */
        /* If it is *not* a LINE stoloc, we can ignore it, because      */
        /* picking won't allow it to be picked if the status has changed*/

        misTrimcpy(lodnum, lodnum_i, LODNUM_LEN);

        sprintf(otherBuffer,
                "select distinct "
                "       il.stoloc, "
                "       id.prtnum, "
                "       id.prt_client_id "
                "  from invlod il, "
                "       invsub ix, "
                "       invdtl id "
                " where il.lodnum = ix.lodnum "
                "   and ix.subnum = id.subnum "
                "   and il.lodnum = '%s' ",
                lodnum);

        status = sqlExecStr(otherBuffer, &tmpres);

        if (eOK != status && eDB_NO_ROWS_AFFECTED != status)
        {
            sqlFreeResults(tmpres);
            return (status);
        }
        if (status == eOK)
        {
            tmprow = sqlGetRow(tmpres);
            strcpy(stoloc, sqlGetString(tmpres, tmprow, "stoloc"));
            strcpy(prtnum, sqlGetString(tmpres, tmprow, "prtnum"));
            strcpy(prt_client_id, 
                   sqlGetString(tmpres, tmprow, "prt_client_id"));

            if (sqlGetNumRows(tmpres) > 1)
            {
                multiple_rows = BOOLEAN_TRUE;
                /* fill the 'prtBuffer' string with an 'in' clause */
                strcpy(prtBuffer, "and prtnum||prt_client_id in (");
                for (tmprow = sqlGetRow(tmpres); 
                     tmprow; 
                     tmprow = sqlGetNextRow(tmprow))
                {
                    sprintf(tmpBuffer, "'%s||%s',",
                            sqlGetString(tmpres,tmprow, "prtnum"),
                            sqlGetString(tmpres,tmprow, "prt_client_id"));
                    strcat(prtBuffer, tmpBuffer); 

                }
                /* clean up the end */
                prtBuffer[strlen(prtBuffer)-1] = '\0';
                strcat (prtBuffer, ") ");
                misTrc(T_FLOW, "assembled prtBuffer string is [%s]", prtBuffer);
            }
            else
            {
                multiple_rows = BOOLEAN_FALSE;
            }

        }
        /* Do nothing on a NO_ROWS_AFFECTED */
        sqlFreeResults(tmpres);
    }

    if (misTrimLen(stoloc_i, STOLOC_LEN) || strlen(stoloc))
    {
        /* If we have the stoloc, we check to see if this stoloc is   */
        /* dedicated to a work order (only possible if its a LINE loc */
        /* Otherwise, we just get the prtnum/prt_client_id combos from  */
        /* the inventory on the load and add that into the mix.         */
        if (strlen(stoloc) == 0)
        {
            /* If the stoloc was filled in above, use it.  Otherwise, take */
            /* the passed-in parm                                          */
            misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);
        }
        
        /* Wkosts could not be 'C', listing closed work order for inventory
         * status change doesn't make sense to user.
         * And if it could not find the result, return out.
         */
        sprintf(otherBuffer,
                "select client_id, "
                "       wkonum, "
                "       wkorev "
                "  from wkohdr "
                " where prcloc = '%s' "
                "   and wkosts != '%s' ",
                stoloc,
                WKOSTS_CLOSED);

        status = sqlExecStr(otherBuffer, &tmpres);

        if (eOK != status && eDB_NO_ROWS_AFFECTED != status)
        {
            misTrc(T_FLOW, "Failed to query wkohdr.  Aborting");
            sqlFreeResults(tmpres);
            return (status);
        }

        if (eDB_NO_ROWS_AFFECTED == status || eSRV_NO_ROWS_AFFECTED == status)
        {
            sqlFreeResults(tmpres);
            return (status);
        }

        if (eOK == status)
        {
            tmprow = sqlGetRow(tmpres);
            /* Over-ride the work order info with these values */
            strcpy(client_id, sqlGetString(tmpres, tmprow, "client_id"));
            strcpy(wkonum, sqlGetString(tmpres, tmprow, "wkonum"));
            strcpy(wkorev, sqlGetString(tmpres, tmprow, "wkorev"));
        }
        sqlFreeResults(tmpres);
    }

    if (multiple_rows || !misTrimLen(lodnum_i, LODNUM_LEN))
    {
        if (misTrimLen(prtnum_i, PRTNUM_LEN) || strlen(prtnum))
        {
            /*
             * Determine client Id.  In non 3pl environment, will return
             * the default client Id.  In 3pl environment, will verify
             * that it is a valid client.  
             */
            if (!strlen(prtnum))
            {
                /* If we've determined partnum from the load, use it, */
                /* not the  passed in values */
                misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
            }

            if (!strlen(prt_client_id))
            {
                status = appGetClient(prt_client_id_i, prt_client_id);
                if (status != eOK)
                {
                    memset(prt_client_id, 0, sizeof(prt_client_id));
                    status = eOK;
                }
                else
                {
                    sprintf(buildBuffer,
                            " and prt_client_id = '%s' ",
                            prt_client_id);
                    strcat (sqlBuffer,buildBuffer);
                }
            }

            if (strlen(prtnum))
            {
                sprintf(buildBuffer, 
                        " and prtnum = '%s' ",
                        prtnum);
                strcat (sqlBuffer,buildBuffer);
            }
        }
    }
    else
    {
       /* There are multiple part numbers from above */
       strcat(sqlBuffer, prtBuffer);
    }
    
    if (misTrimLen(lotnum_i, LOTNUM_LEN))
    {
        sprintf (buildBuffer, " and lotnum = '%s' ", lotnum_i);
        strcat (sqlBuffer, buildBuffer);
    }

    if (misTrimLen(orgcod_i, ORGCOD_LEN))
    {
        sprintf (buildBuffer, " and orgcod= '%s' ", orgcod_i);
        strcat (sqlBuffer, buildBuffer);
    }
       
    if (misTrimLen(revlvl_i, REVLVL_LEN))
    {
        sprintf (buildBuffer, " and revlvl = '%s' ", revlvl_i);
        strcat (sqlBuffer, buildBuffer);
    }

    if (strlen(client_id) > 0)
    {
	sprintf (buildBuffer, " and client_id = '%s' ", client_id);
	strcat (sqlBuffer, buildBuffer);
    }

    if (strlen(wkonum) > 0)
    {
	sprintf (buildBuffer, " and wkonum = '%s' ", wkonum);
	strcat (sqlBuffer, buildBuffer);
    }

    if (strlen(wkorev) > 0)
    {
	sprintf (buildBuffer, " and wkorev = '%s' ", wkorev);
	strcat (sqlBuffer, buildBuffer);
    }

    if (misTrimLen(invsts_i, INVSTS_LEN))
    {
	sprintf (buildBuffer, " and invsts = '%s' ", invsts_i);
	strcat (sqlBuffer, buildBuffer);
    }

    strcat (sqlBuffer,
	    " order by wkonum, client_id, wkorev, wkolin, seqnum ");

    status = sqlExecStr(sqlBuffer, &tmpres);
    
    sqlCombineResults(res, &tmpres);

    sqlFreeResults(tmpres);
        
    return(status);

}

static int sFindIntransitInventory(mocaDataRes **res,
                                   char *lodnum_i, char *subnum_i, 
                                   char *dtlnum_i, char *arecod_i, 
                                   char *bldg_id_i, char *stoloc_i,
                                   char *prtnum_i, char *prt_client_id_i, 
                                   char *lotnum_i, 
                                   char *orgcod_i, char *revlvl_i,
                                   char *invsts_i, char *age_pflnam_i,
                                   char *fr_fifdte_i, char *to_fifdte_i,
                                   char *iidtyp_i, char *fr_serial_i, 
                                   char *to_serial_i, char *trknum_i, 
                                   char *supnum_i, char *invnum_i,
                                   char *client_id_i, char *hldpfx_i, 
                                   char *hldnum_i, char *hldtyp_i, 
                                   char *reacod_i, moca_bool_t *alcflg_i, 
                                   moca_bool_t *shpflg_i, moca_bool_t *dtcflg_i,
                                   char *ftpcod_i)
{
    mocaDataRes *tmpres = NULL;

    char buffer[4096];
    char fromlist[256];
    char rcvcols[256];
    char whereclause[2048];
    char tmpbuf[256];
    long status;
    
    char client_id[CLIENT_ID_LEN + 1];

    misTrc(T_FLOW, "Searching for intransit details.");

    memset(rcvcols, 0, sizeof(rcvcols));
    memset(fromlist, 0, sizeof(fromlist));
    memset(buffer, 0, sizeof(buffer));
    memset(whereclause, 0, sizeof(whereclause));

    /* First, determine which tables we are selecting FROM */
    if ((trknum_i && strlen(trknum_i)) || 
        (supnum_i && strlen(supnum_i)) ||
        (invnum_i && strlen(invnum_i)))
    {
    	sprintf(fromlist, 
                "bldg_mst bm, "
                "aremst am, "
                "locmst lm, " 
                "inv_intransit i, "
                "rcvlin r,"
                "prtmst p");

	sprintf(rcvcols, 
                "r.trknum, "
                "r.supnum, "
                "r.invnum, ");
    }
    else
    {
	sprintf(fromlist, 
		"bldg_mst bm, "
		"aremst am, "
                "locmst lm, "
                "inv_intransit i, "
                "prtmst p ");

        sprintf(rcvcols, 
                "'' trknum, "
                "'' supnum, "
                "'' invnum, ");
    }

    /* 
     * Next, build up the where clause for this first select.
     * The first thing will be 1=1 to make sure we have something
     * to put in there in case they don't pass any selection criteria.
     */

    sprintf(whereclause, " 1 = 1 ");

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
    {
        sprintf(tmpbuf,
                " and i.lodnum = '%.*s' ",
                LODNUM_LEN, lodnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
    {
        sprintf(tmpbuf,
                " and i.subnum = '%.*s' ",
                SUBNUM_LEN, subnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
        sprintf(tmpbuf,
                " and i.dtlnum = '%.*s' ",
                DTLNUM_LEN, dtlnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
    {
        sprintf(tmpbuf,
                " and am.arecod = '%.*s' ",
                ARECOD_LEN, arecod_i);

        strcat(whereclause, tmpbuf);
    }

    if (bldg_id_i && misTrimLen(bldg_id_i, BLDG_ID_LEN))
    {
        sprintf(tmpbuf,
                " and am.bldg_id = '%.*s' ",
                BLDG_ID_LEN, bldg_id_i);

        strcat(whereclause, tmpbuf);
    }

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
    {
        sprintf(tmpbuf,
                " and lm.stoloc = '%.*s' ",
                STOLOC_LEN, stoloc_i);

        strcat(whereclause, tmpbuf);
    }

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
        sprintf(tmpbuf,
                " and i.prtnum = '%.*s' ",
                PRTNUM_LEN, prtnum_i);

        strcat(whereclause, tmpbuf);

        /* Get the part client Id */
        memset(client_id, 0, sizeof(client_id));
        status = appGetClient(prt_client_id_i, client_id);
        if (eOK == status)
        {
            sprintf(tmpbuf,
                    " and i.prt_client_id = '%s' ",
                    client_id);

            strcat(whereclause, tmpbuf);
        }

    }

    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
    {
        sprintf(tmpbuf,
                " and i.lotnum = '%.*s' ",
                LOTNUM_LEN, lotnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
    {
        sprintf(tmpbuf,
                " and i.orgcod = '%.*s' ",
                ORGCOD_LEN, orgcod_i);

        strcat(whereclause, tmpbuf);
    }

    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
    {
        sprintf(tmpbuf,
                " and i.revlvl = '%.*s' ",
                REVLVL_LEN, revlvl_i);

        strcat(whereclause, tmpbuf);
    }

    if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
    {
        sprintf(tmpbuf,
                " and i.invsts = '%.*s' ",
                INVSTS_LEN, invsts_i);

        strcat(whereclause, tmpbuf);
    }

    if (age_pflnam_i && misTrimLen(age_pflnam_i, AGE_PFLNAM_LEN))
    {
        sprintf(tmpbuf,
                " and i.age_pflnam = '%.*s' ",
                AGE_PFLNAM_LEN, age_pflnam_i);

        strcat(whereclause, tmpbuf);
    }

    if (dtcflg_i)
    {
        /* 
         * We will assume that ALL inventory controlled inventory has an
         * aging profile.  This saves a join to the part master.
         */

        if (*dtcflg_i)
            sprintf(tmpbuf, " and i.age_pflnam IS NOT NULL ");
        else
            sprintf(tmpbuf, " and i.age_pflnam IS NULL ");

        strcat(whereclause, tmpbuf);

    }

    if (fr_fifdte_i && misTrimLen(fr_fifdte_i, MOCA_STD_DATE_LEN))
    {
        sprintf(tmpbuf,
                " and i.fifdte >= to_date('%.*s', '%s') ",
                MOCA_STD_DATE_LEN, fr_fifdte_i, MOCA_STD_DATE_FORMAT);

        strcat(whereclause, tmpbuf);
    }

    if (to_fifdte_i && misTrimLen(to_fifdte_i, MOCA_STD_DATE_LEN))
    {
        sprintf(tmpbuf,
                " and i.fifdte <= to_date('%.*s', '%s') ",
                MOCA_STD_DATE_LEN, to_fifdte_i, MOCA_STD_DATE_FORMAT);

        strcat(whereclause, tmpbuf);
    }

    if (fr_serial_i && misTrimLen(fr_serial_i, SUBNUM_LEN))
    {
        if (!iidtyp_i || 
            misTrimLen(iidtyp_i, IIDTYP_LEN) ||
            strncmp(iidtyp_i, IIDTYP_CASES, IIDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and i.subnum >= '%.*s' ",
                    SUBNUM_LEN, fr_serial_i);
        }
        else
        {
            sprintf(tmpbuf,
                    " and i.dtlnum >= '%.*s' ",
                    DTLNUM_LEN, fr_serial_i);
        }

        strcat(whereclause, tmpbuf);
    }

    if (to_serial_i && misTrimLen(to_serial_i, SUBNUM_LEN))
    {
        if (!iidtyp_i || 
            misTrimLen(iidtyp_i, IIDTYP_LEN) ||
            strncmp(iidtyp_i, IIDTYP_CASES, IIDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and i.subnum <= '%.*s' ",
                    SUBNUM_LEN, to_serial_i);
        }
        else
        {
            sprintf(tmpbuf,
                    " and i.dtlnum <= '%.*s' ",
                    DTLNUM_LEN, to_serial_i);
        }

        strcat(whereclause, tmpbuf);
    }

    if ((trknum_i && misTrimLen(trknum_i, TRKNUM_LEN)) ||
        (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN)) ||
        (invnum_i && misTrimLen(invnum_i, INVNUM_LEN)))
    {
        sprintf(tmpbuf,
                " and i.rcvkey = r.rcvkey ");
        strcat(whereclause, tmpbuf);

        if (trknum_i && misTrimLen(trknum_i, TRKNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.trknum = '%.*s' ",
                    TRKNUM_LEN, trknum_i);
            strcat(whereclause, tmpbuf);
        }

        if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.supnum = '%.*s' ",
                    SUPNUM_LEN, supnum_i);
            strcat(whereclause, tmpbuf);
        }

        if (invnum_i && misTrimLen(invnum_i, INVNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.invnum = '%.*s' ",
                    INVNUM_LEN, invnum_i);
            strcat(whereclause, tmpbuf);
        }
    }

    if ((hldpfx_i && misTrimLen(hldpfx_i, HLDPFX_LEN)) ||
        (hldnum_i && misTrimLen(hldnum_i, HLDNUM_LEN)) ||
        (hldtyp_i && misTrimLen(hldtyp_i, HLDTYP_LEN)) ||
        (reacod_i && misTrimLen(reacod_i, REACOD_LEN)) ||
        (alcflg_i) || (shpflg_i))
    {
        /* We're going to have to join in the hold tables */
        sprintf(tmpbuf,
                " and exists "
                " (select 'x' "
                "    from hldmst h, hld_intransit ih "
                "   where h.hldpfx = ih.hldpfx "
                "     and h.hldnum = ih.hldnum "
                "     and ih.dtlnum = i.dtlnum ");

        strcat(whereclause, tmpbuf);

        /* Now add the conditions onto it */
        if (hldpfx_i && misTrimLen(hldpfx_i, HLDPFX_LEN))
        {
            sprintf(tmpbuf,
                    " and ih.hldpfx = '%s' ",
                    hldpfx_i);
            strcat(whereclause, tmpbuf);
        }

        if (hldnum_i && misTrimLen(hldnum_i, HLDNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and ih.hldnum = '%s' ",
                    hldnum_i);
            strcat(whereclause, tmpbuf);
        }

        if (hldtyp_i && misTrimLen(hldtyp_i, HLDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and h.hldtyp = '%s' ",
                    hldtyp_i);
            strcat(whereclause, tmpbuf);
        }

        if (reacod_i && misTrimLen(reacod_i, REACOD_LEN))
        {
            sprintf(tmpbuf,
                    " and ih.reacod = '%s' ",
                    reacod_i);
            strcat(whereclause, tmpbuf);
        }

        if (alcflg_i)
        {
            sprintf(tmpbuf,
                    " and h.alcflg = %ld ",
                    *alcflg_i ? 1 : 0);
            strcat(whereclause, tmpbuf);
        }

        if (shpflg_i)
        {
            sprintf(tmpbuf,
                    " and h.shpflg = %ld ",
                    *shpflg_i ? 1 : 0);
            strcat(whereclause, tmpbuf);
        }

        /* Finally, finish off the sub-clause */
        strcat(whereclause, ")");
    }

    if (ftpcod_i && misTrimLen(ftpcod_i, FTPCOD_LEN))
    {
        sprintf(tmpbuf,
                " and i.ftpcod = '%.*s' ",
                FTPCOD_LEN, ftpcod_i);

        strcat(whereclause, tmpbuf);
    }

    /* Now, build the entire selection string. */

    /* Here the untcas and untpak are multiplied by 1.0
    ** to return a float and provide consistent datatypes
    ** for the sqlCombineResults
    */

    sprintf (buffer, 
             "select am.bldg_id, "
             "       lm.arecod, "
             "       lm.stoloc, "
             "       lm.locsts, "
             "       i.lodnum, "
             "       i.subnum, "
             "       i.dtlnum, "
             "       i.prtnum, "
             "       i.prt_client_id, "
             "       i.lotnum, "
             "       i.orgcod, "
             "       i.revlvl, "
             "       i.invsts, "
             "       i.untqty, "
             "       i.catch_qty, "
             "       p.catch_unttyp,"
             "       to_number('0') alcflg, "
             "       i.age_pflnam, "
             "       i.fifdte, "
             "       i.mandte, "
             "       i.expire_dte, "
             "       %s "
             "       '' wkonum, "
             "       '' client_id, "
             "       '' wkorev, "
             "       '' wkolin, "
             "       to_number(null) seqnum, "
             "       0 kitcmpflg, "
             "       i.ftpcod, "
             "       i.untcas * 1.0 untcas, "  
             "       i.untpak * 1.0 untpak, "  
             "       '' wh_id, "               
             "       1 in_transit_flg "
             "  from %s "
             " where bm.bldg_id = am.bldg_id "
             "   and am.arecod = lm.arecod "
             "   and lm.stoloc = i.trlr_id "
             "   and i.prtnum = p.prtnum "
             "   and i.prt_client_id = p.prt_client_id "
             "   and i.processed_flg = %ld "
             "   and %s "
             " order by i.trlr_id, "
             "          i.prtnum, i.prt_client_id, "
             "          i.lodnum, i.subnum, i.dtlnum ",
             rcvcols,
             fromlist, 
             BOOLEAN_FALSE,
             whereclause);


    status = sqlExecStr(buffer, &tmpres);

    if (eOK == status)
        sqlCombineResults(res, &tmpres);

    sqlFreeResults (tmpres);
        
    return (status);

}

static int sFindInventory(mocaDataRes **res,
                          char *lodnum_i, char *subnum_i, char *dtlnum_i,
                          char *arecod_i, char *bldg_id_i, char *stoloc_i,
                          char *locsts_i,
                          char *prtnum_i, char *prt_client_id_i,
                          char *lotnum_i, char *orgcod_i, char *revlvl_i,
                          char *invsts_i, char *age_pflnam_i,
                          char *fr_fifdte_i, char *to_fifdte_i,
                          char *iidtyp_i,
                          char *fr_serial_i, char *to_serial_i,
                          char *trknum_i, char *supnum_i, char *invnum_i,
                          char *client_id_i, 
                          char *hldpfx_i, char *hldnum_i, char *hldtyp_i,
                          char *reacod_i,
                          moca_bool_t *alcflg_i, moca_bool_t *shpflg_i,
                          moca_bool_t *dtcflg_i, char *ftpcod_i, char *wh_id_i)
{

    char buffer[4096];
    char fromlist[256];
    char rcvcols[256];
    char whereclause[2048];
    char tmpbuf[256];
    long status;
    
    char client_id[CLIENT_ID_LEN + 1];

    misTrc(T_FLOW, "Searching for inventory.");

    memset(rcvcols, 0, sizeof(rcvcols));
    memset(fromlist, 0, sizeof(fromlist));
    memset(buffer, 0, sizeof(buffer));
    memset(whereclause, 0, sizeof(whereclause));

    /* First, determine which tables we are selecting FROM */
    if ((trknum_i && strlen(trknum_i)) || 
        (supnum_i && strlen(supnum_i)) ||
        (invnum_i && strlen(invnum_i)))
    {
    	sprintf(fromlist, 
                "bldg_mst bm, "
		"aremst am, "
                "locmst lm, "
                "invlod l, "
		"invsub s, "
                "invdtl d, "
                "rcvlin r,"
                "prtmst p");

	sprintf(rcvcols, 
                "r.trknum, "
                "r.supnum, "
                "r.invnum, ");
    }
    else
    {
	if (stoloc_i && strlen(stoloc_i))
	{
	    sprintf(fromlist, 
		    "bldg_mst bm, "
		    "aremst am, "
                    "locmst lm, "
		    "invsub s, "
                    "invdtl d, "
                    "invlod l, "
                    "prtmst p ");
	}
	else
	{
	    sprintf(fromlist, 
		    "bldg_mst bm, "
		    "aremst am, "
                    "locmst lm, "
		    "invlod l, "
                    "invsub s, "
                    "invdtl d, "
                    "prtmst p ");
	}
        sprintf(rcvcols, 
                "'' trknum, "
                "'' supnum, "
                "'' invnum, ");
    }

    /* 
     * Next, build up the where clause for this first select.
     * The first thing will be 1=1 to make sure we have something
     * to put in there in case they don't pass any selection criteria.
     */

    sprintf(whereclause, " 1 = 1 ");

    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN))
    {
        sprintf(tmpbuf,
                " and l.lodnum = '%.*s' ",
                LODNUM_LEN, lodnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN))
    {
        sprintf(tmpbuf,
                " and s.subnum = '%.*s' ",
                SUBNUM_LEN, subnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN))
    {
        sprintf(tmpbuf,
                " and d.dtlnum = '%.*s' ",
                DTLNUM_LEN, dtlnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
    {
        sprintf(tmpbuf,
                " and am.arecod = '%.*s' ",
                ARECOD_LEN, arecod_i);

        strcat(whereclause, tmpbuf);
    }

    if (bldg_id_i && misTrimLen(bldg_id_i, BLDG_ID_LEN))
    {
        sprintf(tmpbuf,
                " and am.bldg_id = '%.*s' ",
                BLDG_ID_LEN, bldg_id_i);

        strcat(whereclause, tmpbuf);
    }

    if (stoloc_i && misTrimLen(stoloc_i, STOLOC_LEN))
    {
        sprintf(tmpbuf,
                " and lm.stoloc = '%.*s' ",
                STOLOC_LEN, stoloc_i);

        strcat(whereclause, tmpbuf);
    }

    if (locsts_i && misTrimLen(locsts_i, LOCSTS_LEN))
    {
        sprintf(tmpbuf,
                " and lm.locsts = '%.*s' ",
                LOCSTS_LEN, locsts_i);

        strcat(whereclause, tmpbuf);
    }

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
        sprintf(tmpbuf,
                " and d.prtnum = '%.*s' ",
                PRTNUM_LEN, prtnum_i);

        strcat(whereclause, tmpbuf);

        /* Get the part client Id */
        memset(client_id, 0, sizeof(client_id));
        status = appGetClient(prt_client_id_i, client_id);
        if (eOK == status)
        {
            sprintf(tmpbuf,
                    " and d.prt_client_id = '%s' ",
                    client_id);

            strcat(whereclause, tmpbuf);
        }

    }

    if (lotnum_i && misTrimLen(lotnum_i, LOTNUM_LEN))
    {
        sprintf(tmpbuf,
                " and d.lotnum = '%.*s' ",
                LOTNUM_LEN, lotnum_i);

        strcat(whereclause, tmpbuf);
    }

    if (orgcod_i && misTrimLen(orgcod_i, ORGCOD_LEN))
    {
        sprintf(tmpbuf,
                " and d.orgcod = '%.*s' ",
                ORGCOD_LEN, orgcod_i);

        strcat(whereclause, tmpbuf);
    }

    if (revlvl_i && misTrimLen(revlvl_i, REVLVL_LEN))
    {
        sprintf(tmpbuf,
                " and d.revlvl = '%.*s' ",
                REVLVL_LEN, revlvl_i);

        strcat(whereclause, tmpbuf);
    }

    if (invsts_i && misTrimLen(invsts_i, INVSTS_LEN))
    {
        sprintf(tmpbuf,
                " and d.invsts = '%.*s' ",
                INVSTS_LEN, invsts_i);

        strcat(whereclause, tmpbuf);
    }

    if (age_pflnam_i && misTrimLen(age_pflnam_i, AGE_PFLNAM_LEN))
    {
        sprintf(tmpbuf,
                " and d.age_pflnam = '%.*s' ",
                AGE_PFLNAM_LEN, age_pflnam_i);

        strcat(whereclause, tmpbuf);
    }

    if (dtcflg_i)
    {
        /* 
         * We will assume that ALL inventory controlled inventory has an
         * aging profile.  This saves a join to the part master.
         */

        if (*dtcflg_i)
            sprintf(tmpbuf, " and d.age_pflnam IS NOT NULL ");
        else
            sprintf(tmpbuf, " and d.age_pflnam IS NULL ");

        strcat(whereclause, tmpbuf);

    }

    if (fr_fifdte_i && misTrimLen(fr_fifdte_i, MOCA_STD_DATE_LEN))
    {
        sprintf(tmpbuf,
                " and d.fifdte >= to_date('%.*s', '%s') ",
                MOCA_STD_DATE_LEN, fr_fifdte_i, MOCA_STD_DATE_FORMAT);

        strcat(whereclause, tmpbuf);
    }

    if (to_fifdte_i && misTrimLen(to_fifdte_i, MOCA_STD_DATE_LEN))
    {
        sprintf(tmpbuf,
                " and d.fifdte <= to_date('%.*s', '%s') ",
                MOCA_STD_DATE_LEN, to_fifdte_i, MOCA_STD_DATE_FORMAT);

        strcat(whereclause, tmpbuf);
    }

    if (fr_serial_i && misTrimLen(fr_serial_i, SUBNUM_LEN))
    {
        if (!iidtyp_i || 
            misTrimLen(iidtyp_i, IIDTYP_LEN) ||
            strncmp(iidtyp_i, IIDTYP_CASES, IIDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and s.subnum >= '%.*s' ",
                    SUBNUM_LEN, fr_serial_i);
        }
        else
        {
            sprintf(tmpbuf,
                    " and d.dtlnum >= '%.*s' ",
                    DTLNUM_LEN, fr_serial_i);
        }

        strcat(whereclause, tmpbuf);
    }

    if (to_serial_i && misTrimLen(to_serial_i, SUBNUM_LEN))
    {
        if (!iidtyp_i || 
            misTrimLen(iidtyp_i, IIDTYP_LEN) ||
            strncmp(iidtyp_i, IIDTYP_CASES, IIDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and s.subnum <= '%.*s' ",
                    SUBNUM_LEN, to_serial_i);
        }
        else
        {
            sprintf(tmpbuf,
                    " and d.dtlnum <= '%.*s' ",
                    DTLNUM_LEN, to_serial_i);
        }

        strcat(whereclause, tmpbuf);
    }

    if ((trknum_i && misTrimLen(trknum_i, TRKNUM_LEN)) ||
        (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN)) ||
        (invnum_i && misTrimLen(invnum_i, INVNUM_LEN)))
    {
        sprintf(tmpbuf,
                " and d.rcvkey = r.rcvkey ");
        strcat(whereclause, tmpbuf);

        if (trknum_i && misTrimLen(trknum_i, TRKNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.trknum = '%.*s' ",
                    TRKNUM_LEN, trknum_i);
            strcat(whereclause, tmpbuf);
        }

        if (supnum_i && misTrimLen(supnum_i, SUPNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.supnum = '%.*s' ",
                    SUPNUM_LEN, supnum_i);
            strcat(whereclause, tmpbuf);
        }

        if (invnum_i && misTrimLen(invnum_i, INVNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and r.invnum = '%.*s' ",
                    INVNUM_LEN, invnum_i);
            strcat(whereclause, tmpbuf);
        }
    }

    if ((hldpfx_i && misTrimLen(hldpfx_i, HLDPFX_LEN)) ||
        (hldnum_i && misTrimLen(hldnum_i, HLDNUM_LEN)) ||
        (hldtyp_i && misTrimLen(hldtyp_i, HLDTYP_LEN)) ||
        (reacod_i && misTrimLen(reacod_i, REACOD_LEN)) ||
        (alcflg_i) || (shpflg_i))
    {
        /* We're going to have to join in the hold tables */
        sprintf(tmpbuf,
                " and exists "
                " (select 'x' "
                "    from hldmst h, invhld i "
                "   where h.hldpfx = i.hldpfx "
                "     and h.hldnum = i.hldnum "
                "     and i.dtlnum = d.dtlnum ");

        strcat(whereclause, tmpbuf);

        /* Now add the conditions onto it */
        if (hldpfx_i && misTrimLen(hldpfx_i, HLDPFX_LEN))
        {
            sprintf(tmpbuf,
                    " and i.hldpfx = '%s' ",
                    hldpfx_i);
            strcat(whereclause, tmpbuf);
        }

        if (hldnum_i && misTrimLen(hldnum_i, HLDNUM_LEN))
        {
            sprintf(tmpbuf,
                    " and i.hldnum = '%s' ",
                    hldnum_i);
            strcat(whereclause, tmpbuf);
        }

        if (hldtyp_i && misTrimLen(hldtyp_i, HLDTYP_LEN))
        {
            sprintf(tmpbuf,
                    " and h.hldtyp = '%s' ",
                    hldtyp_i);
            strcat(whereclause, tmpbuf);
        }

        if (reacod_i && misTrimLen(reacod_i, REACOD_LEN))
        {
            sprintf(tmpbuf,
                    " and i.reacod = '%s' ",
                    reacod_i);
            strcat(whereclause, tmpbuf);
        }

        if (alcflg_i)
        {
            sprintf(tmpbuf,
                    " and h.alcflg = %ld ",
                    *alcflg_i ? 1 : 0);
            strcat(whereclause, tmpbuf);
        }

        if (shpflg_i)
        {
            sprintf(tmpbuf,
                    " and h.shpflg = %ld ",
                    *shpflg_i ? 1 : 0);
            strcat(whereclause, tmpbuf);
        }

        /* Finally, finish off the sub-clause */
        strcat(whereclause, ")");
    }

    if (ftpcod_i && misTrimLen(ftpcod_i, FTPCOD_LEN))
    {
        sprintf(tmpbuf,
                " and d.ftpcod = '%.*s' ",
                FTPCOD_LEN, ftpcod_i);

        strcat(whereclause, tmpbuf);
    }

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
    {
        sprintf(tmpbuf,
                " and bm.wh_id = '%.*s' ",
                WH_ID_LEN, wh_id_i);

        strcat(whereclause, tmpbuf);
    }

    /* Now, build the entire selection string. */

    /* Here the untcas and untpak are multiplied by 1.0
    ** to return a float and provide consistent datatypes
    ** for the sqlCombineResults
    */
    
    sprintf (buffer, 
             "select am.bldg_id, "
             "       lm.arecod, "
             "       lm.stoloc, "
             "       lm.locsts, "
             "       l.lodnum, "
             "       s.subnum, "
             "       d.dtlnum, "
             "       d.prtnum, "
             "       d.prt_client_id, "
             "       d.lotnum, "
             "       d.orgcod, "
             "       d.revlvl, "
             "       d.invsts, "
             "       d.untqty, "
             " nvl((select sum(ivs.comqty) from invsum ivs where ivs.stoloc = l.stoloc and ivs.prtnum = d.prtnum and ivs.invsts = d.invsts),0)  comqty, "
             "       d.ship_line_id, "
             "       d.catch_qty, "
             "       p.catch_unttyp,"
             "       to_number(d.alcflg) alcflg, "
             "       d.age_pflnam, "
             "       d.fifdte, "
             "       d.mandte, "
             "       d.expire_dte, "
             "       %s "
             "       '' wkonum, "
             "       '' client_id, "
             "       '' wkorev, "
             "       '' wkolin, "
             "       to_number(null) seqnum, "
             "       0 kitcmpflg, "
             "       d.ftpcod, "
             "       d.untcas * 1.0 untcas, "
             "       d.untpak * 1.0 untpak, "
             "       bm.wh_id, "
             "       0 in_transit_flg "
             "  from %s "
             " where bm.bldg_id = am.bldg_id "
             "   and am.arecod = lm.arecod "
             "   and lm.stoloc = l.stoloc "
             "   and l.lodnum = s.lodnum "
             "   and s.subnum = d.subnum "
             "   and d.prtnum = p.prtnum"
             "   and d.prt_client_id = p.prt_client_id"
             "   and %s "
             "   and not exists "
             "       (select trlr.stoloc "
             "          from trlr "
             "         where trlr.stoloc = lm.stoloc "
             "           and trlr.trlr_stat = '%s' ) "
             " order by bm.wh_id, am.arecod, lm.stoloc, "
             "          d.prtnum, d.prt_client_id, "
             "          l.lodnum, s.subnum, d.dtlnum ",
             rcvcols,
             fromlist, 
             whereclause,
             TRLSTS_DISPATCHED);


    status = sqlExecStr(buffer, res);
        
    return (status);

}

LIBEXPORT 
RETURN_STRUCT *varListInventoryChangeInformation(char *lodnum_i,
                                                 char *subnum_i,
                                                 char *dtlnum_i,
                                                 char *arecod_i,
                                                 char *bldg_id_i,
                                                 char *stoloc_i,
                                                 char *locsts_i,
                                                 char *prtnum_i,
                                                 char *prt_client_id_i,
                                                 char *lotnum_i,
                                                 char *orgcod_i,
                                                 char *revlvl_i,
                                                 char *invsts_i,
                                                 char *age_pflnam_i,
                                                 char *fr_fifdte_i,
                                                 char *to_fifdte_i,
                                                 char *iidtyp_i,
                                                 char *fr_serial_i,
                                                 char *to_serial_i,
                                                 char *trknum_i,
                                                 char *supnum_i,
                                                 char *invnum_i,
                                                 char *wkonum_i,
                                                 char *client_id_i,
                                                 char *wkorev_i,
                                                 char *hldpfx_i,
                                                 char *hldnum_i,
                                                 char *hldtyp_i,
                                                 char *reacod_i,
                                                 moca_bool_t *alcflg_i,
                                                 moca_bool_t *shpflg_i,
                                                 moca_bool_t *dtcflg_i,
                                                 moca_bool_t *incwko_i,
                                                 moca_bool_t *srch_comps_flg_i,
                                                 moca_bool_t *initflg_i,
                                                 char *ftpcod_i,
                                                 char *wh_id_i,
                                                 moca_bool_t *show_in_transit_flg_i)
{
    RETURN_STRUCT  *RetPtr;
    mocaDataRes    *res = NULL;
    mocaDataRes    *cmpres = NULL;
    mocaDataRes    *wkores = NULL;
    mocaDataRes    *intr_res = NULL;

    char prt_client_id[CLIENT_ID_LEN + 1];
    
    long status;    

    memset(prt_client_id,0,sizeof(prt_client_id));

    /*
     * If we're in initialization mode, as may be the case when called from
     * the GUI, just return back an empty result set.
     */

    if (initflg_i && *initflg_i == BOOLEAN_TRUE)
    {
        RetPtr = srvResultsInit (eOK,
                                 "bldg_id", COMTYP_CHAR, BLDG_ID_LEN,
                                 "arecod", COMTYP_CHAR, ARECOD_LEN,
                                 "stoloc", COMTYP_CHAR, STOLOC_LEN,
                                 "locsts", COMTYP_CHAR, LOCSTS_LEN,
                                 "lodnum", COMTYP_CHAR, LODNUM_LEN,
                                 "subnum", COMTYP_CHAR, SUBNUM_LEN,
                                 "dtlnum", COMTYP_CHAR, DTLNUM_LEN,
                                 "prtnum", COMTYP_CHAR, PRTNUM_LEN,
                                 "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                                 "lotnum", COMTYP_CHAR, LOTNUM_LEN,
                                 "orgcod", COMTYP_CHAR, ORGCOD_LEN,
                                 "revlvl", COMTYP_CHAR, REVLVL_LEN,
                                 "invsts", COMTYP_CHAR, INVSTS_LEN,
                                 "untqty", COMTYP_INT, sizeof(long),
                                 "catch_qty", COMTYP_FLOAT, sizeof(double),
                                 "catch_unttyp", COMTYP_CHAR, CATCH_UNTTYP_LEN,
                                 "alcflg", COMTYP_BOOLEAN, sizeof(moca_bool_t),
                                 "age_pflnam", COMTYP_CHAR, AGE_PFLNAM_LEN,
                                 "fifdte", COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                                 "mandte", COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                                 "expire_dte", COMTYP_DATTIM, MOCA_STD_DATE_LEN,
                                 "trknum", COMTYP_CHAR, TRKNUM_LEN,
                                 "supnum", COMTYP_CHAR, SUPNUM_LEN,
                                 "wkonum", COMTYP_CHAR, WKONUM_LEN,
                                 "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                                 "wkorev", COMTYP_CHAR, WKOREV_LEN,
                                 "wkolin", COMTYP_CHAR, WKOLIN_LEN,
                                 "seqnum", COMTYP_INT, sizeof(long),
                                 "kitcmpflg", COMTYP_BOOLEAN, sizeof(moca_bool_t),
                                 "ftpcod", COMTYP_CHAR, FTPCOD_LEN,
                                 "untcas", COMTYP_INT, sizeof(long),
                                 "untpak", COMTYP_INT, sizeof(long),
                                 "wh_id", COMTYP_CHAR, WH_ID_LEN,
                                 "in_transit_flg", COMTYP_BOOLEAN,
                                     sizeof(moca_bool_t),
                                 NULL);

        return (RetPtr);
    }


    /* If the wkonum is entered, then just list all the wkodtl information
     * It will not get inventory this time.
     */
    if (wkonum_i && misTrimLen(wkonum_i, WKONUM_LEN))
    {
        status = sFindWkoDetails(&res,
                                 stoloc_i ? stoloc_i : "",
                                 lodnum_i ? lodnum_i : "",
                                 prtnum_i ? prtnum_i : "",
                                 prt_client_id_i, 
                                 lotnum_i ? lotnum_i : "",
                                 orgcod_i ? orgcod_i : "",
                                 revlvl_i ? revlvl_i : "",
                                 invsts_i ? invsts_i : "",
                                 wkonum_i ? wkonum_i : "",
                                 client_id_i,
                                 wkorev_i ? wkorev_i : "");

        if ((eOK != status) &&
            (eDB_NO_ROWS_AFFECTED != status) &&
            (eSRV_NO_ROWS_AFFECTED != status))
        {
            sqlFreeResults (res);
            return (srvResults (status, NULL));
        }
    }
    else
    {

        /*
         * First do the main select to get inventory according to our
         * parameters.
         */

        status = sFindInventory(&res,
                                lodnum_i, subnum_i, dtlnum_i,
                                arecod_i, bldg_id_i, stoloc_i,
                                locsts_i, prtnum_i, prt_client_id_i,
                                lotnum_i, orgcod_i, revlvl_i,
                                invsts_i, age_pflnam_i,
                                fr_fifdte_i, to_fifdte_i,
                                iidtyp_i,
                                fr_serial_i, to_serial_i,
                                trknum_i, supnum_i, invnum_i,
                                client_id_i,
                                hldpfx_i, hldnum_i, hldtyp_i,
                                reacod_i,
                                alcflg_i, shpflg_i, dtcflg_i,
                                ftpcod_i, wh_id_i);

        if ((eOK != status) &&
            (eDB_NO_ROWS_AFFECTED != status) &&
            (eSRV_NO_ROWS_AFFECTED != status))
        {
            sqlFreeResults (res);
            return (srvResults (status, NULL));
        }

        /*
         * If we're to show intransit inventory, then look of intransit
         * inventory.
         * If we're looking for a specific detail number, and we didn't
         * find it in warehouse inventory, we're also going to check intransit 
         * inventory as well, 
         */

        if ((show_in_transit_flg_i && *show_in_transit_flg_i == BOOLEAN_TRUE) ||
            (dtlnum_i && 
            ((eDB_NO_ROWS_AFFECTED == status) ||
             (eSRV_NO_ROWS_AFFECTED == status))))
        {
            status = sFindIntransitInventory(&intr_res,
                                lodnum_i, subnum_i, dtlnum_i,
                                arecod_i, bldg_id_i, stoloc_i,
                                prtnum_i, prt_client_id_i,
                                lotnum_i, orgcod_i, revlvl_i,
                                invsts_i, age_pflnam_i,
                                fr_fifdte_i, to_fifdte_i,
                                iidtyp_i,
                                fr_serial_i, to_serial_i,
                                trknum_i, supnum_i, invnum_i,
                                client_id_i, 
                                hldpfx_i, hldnum_i, hldtyp_i,
                                reacod_i,
                                alcflg_i, shpflg_i, dtcflg_i,
                                ftpcod_i);

            if ((eOK != status) &&
                (eDB_NO_ROWS_AFFECTED != status) &&
                (eSRV_NO_ROWS_AFFECTED != status))
            {
                sqlFreeResults (res);
                sqlFreeResults (intr_res);
                return (srvResults (status, NULL));
            }

            if (eOK == status)
            {
                status = sqlCombineResults(&res, &intr_res);
                if (eOK != status)
                {
                    sqlFreeResults (res);
                    sqlFreeResults (intr_res);
                    return (srvResults (status, NULL));
                }
            }
            sqlFreeResults(intr_res);
        }

        /* 
         * If the INCL_WKO_FLG is set, then find the work orders matching the 
         * parameters 
         */

        if (incwko_i && *incwko_i == BOOLEAN_TRUE)
        {
            status = sFindWkoDetails(&wkores,
                                     stoloc_i ? stoloc_i : "",
                                     lodnum_i ? lodnum_i : "",
                                     prtnum_i ? prtnum_i : "",
                                     prt_client_id_i, 
                                     lotnum_i ? lotnum_i : "",
                                     orgcod_i ? orgcod_i : "",
                                     revlvl_i ? revlvl_i : "",
                                     invsts_i ? invsts_i : "",
                                     wkonum_i ? wkonum_i : "",
                                     client_id_i,
                                     wkorev_i ? wkorev_i : "");

            if ((eOK != status) &&
                (eDB_NO_ROWS_AFFECTED != status) &&
                (eSRV_NO_ROWS_AFFECTED != status))
            {
                sqlFreeResults (res);
                sqlFreeResults (wkores);
                return (srvResults (status, NULL));
            }

            if (eOK == status)
            {
                status = sqlCombineResults(&res, &wkores);
                if (eOK != status)
                {
                    sqlFreeResults (res);
                    sqlFreeResults (wkores);
                    return (srvResults (status, NULL));
                }
            }
            sqlFreeResults(wkores);
        }

        /* 
         * If the Search Components Flag is set, we must recursively search
         * thru the component header and detail tables in order to find any
         * inventory that was constructed using the query criteria.
         *
         * If they didn't pass anything in, don't search for components 
         */

        if (srch_comps_flg_i && 
            *srch_comps_flg_i == BOOLEAN_TRUE && 
            ((stoloc_i && strlen(stoloc_i)) ||
             (lodnum_i && strlen(lodnum_i)) ||
             (subnum_i && strlen(subnum_i)) ||
             (dtlnum_i && strlen(dtlnum_i)) ||
             (prtnum_i && strlen(prtnum_i)) ||
             (lotnum_i && strlen(lotnum_i)) ||
             (orgcod_i && strlen(orgcod_i)) ||
             (revlvl_i && strlen(revlvl_i)) ||
             (invsts_i && strlen(invsts_i)) ||
             (trknum_i && strlen(trknum_i)) ||
             (supnum_i && strlen(supnum_i))))
        {
            /* prt_client_id is necessary parameter. if it is NULL,
             * it will occur error. 
             * If it is not a 3PL environment, it will get the default
             * value.
             */
            status = appGetClient(prt_client_id_i, prt_client_id);
            if (eOK != status)
            {
                memset(prt_client_id, 0, sizeof(prt_client_id));
            }

            status = sDoComponentSearch(&cmpres, 
                                        stoloc_i ? stoloc_i : "",
                                        lodnum_i ? lodnum_i : "",
                                        subnum_i ? subnum_i : "",
                                        dtlnum_i ? dtlnum_i : "",
                                        prtnum_i ? prtnum_i : "",
                                        prt_client_id,
                                        lotnum_i ? lotnum_i : "",
                                        orgcod_i ? orgcod_i : "",
                                        revlvl_i ? revlvl_i : "",
                                        invsts_i ? invsts_i : "",
                                        trknum_i ? trknum_i : "",
                                        supnum_i ? supnum_i : "",
                                        invnum_i ? invnum_i : "");

            if ((eOK != status) &&
                (eDB_NO_ROWS_AFFECTED != status) &&
                (eSRV_NO_ROWS_AFFECTED != status))
            {
                sqlFreeResults (res);
                sqlFreeResults (cmpres);
                return (srvResults (status, NULL));
            }

            if (eOK == status)
            {
                status = sqlCombineResults(&res, &cmpres);
                if (eOK != status)
                {
                    sqlFreeResults (res);
                    sqlFreeResults (cmpres);
                    return (srvResults (status, NULL));
                }
            }
            sqlFreeResults(cmpres);
        }
    }

    /*
     * If we make it all the way to this point, then we're going to send
     * back either eDB_NO_ROWS_AFFECTED or eOK based on the number of
     * rows in the final result set.
     */

    if (sqlGetNumRows(res) == 0)
        status = eDB_NO_ROWS_AFFECTED;
    else
        status = eOK;

    return(srvAddSQLResults(res, status));

}
