static const char *rcsid = "$Id: intAllocatePickGroup.c,v 1.58.4.2 2004/11/11 15:17:34 mzais Exp $";
/*#START***********************************************************************
 *  Copyright (c) 2004 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>

#include "intlib.h"

typedef struct _WorkingStruct 
{
    char	prtnum[PRTNUM_LEN+1];
    char	prt_client_id[CLIENT_ID_LEN+1];
    char	orgcod[ORGCOD_LEN+1];
    char	revlvl[REVLVL_LEN+1];
    char	lotnum[LOTNUM_LEN+1];
    char	invsts_prg[INVSTS_PRG_LEN+1];
    char	dstare[ARECOD_LEN+1];
    char	dstloc[STOLOC_LEN+1];
    long	pckqty;
    char    client_id[CLIENT_ID_LEN + 1];
    char	ordnum[ORDNUM_LEN+1];
    char	ordlin[ORDLIN_LEN+1];
    char	ordsln[ORDSLN_LEN+1];
    char	stcust[ADRNUM_LEN+1];
    char	rtcust[ADRNUM_LEN+1];
    char	carcod[CARCOD_LEN+1];
    char	srvlvl[SRVLVL_LEN+1];
    char	marcod[MARCOD_LEN+1];
    char	ordtyp[ORDTYP_LEN+1];
    moca_bool_t    parflg;
    moca_bool_t    rpqflg;
    moca_bool_t    splflg;
    moca_bool_t    stdflg;
    moca_bool_t    frsflg;
    moca_bool_t    non_alc_flg;
    moca_bool_t    xdkflg;
    moca_bool_t    xdk_inserted;
    char	entdte[DB_STD_DATE_LEN+1];
    char	concod[CONCOD_LEN+1];
    char	ship_id[SHIP_ID_LEN+1];
    char    ship_line_id[SHIP_LINE_ID_LEN+1];
    char	stop_id[STOP_ID_LEN+1];
    char	prcpri[PRCPRI_LEN+1];
    long	untpal;
    long	untcas;
    double	casvol;
    long    alccas;
    long    alcpak;
    long    alcpal;
    long        min_shelf_hrs;
    char        frsdte[MOCA_STD_DATE_LEN + 1];
    long    partial_case;
    struct	_WorkingStruct *prev;
    struct	_WorkingStruct *next;
} WORKING_STRUCTURE;



/*
 * The following structure holds a list of failed allocation
 * attempts - meaning we could not allocate what was in this
 * list.  This fits in to the PIP allocation method and is
 * used to speed the allocation process.
 */
typedef struct failed_allocs
{
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char allocate_type[40];
    char revlvl[REVLVL_LEN+1];
    char lotnum[LOTNUM_LEN+1];
    char orgcod[ORGCOD_LEN+1];
    char invsts_prg[INVSTS_PRG_LEN+1];
    char dstloc[STOLOC_LEN+1];
    char dstare[ARECOD_LEN+1];
    long splflg;
    long untcas;
    long untpak;
    long untpal;
    long frsflg;
    long non_alc_flg;
    long min_shelf_hrs;
    char frsdte[MOCA_STD_DATE_LEN + 1];

    struct failed_allocs *next;
} FAILED_ALLOCS;

static long sAddToFailureList(FAILED_ALLOCS **FailedAllocHead,
			      char *prtnum,
			      char *prt_client_id,
			      char *orgcod,
			      char *revlvl,
			      char *lotnum,
			      char *invsts_prg,
			      char *dstare,
			      char *dstloc,
			      long splflg,
			      long untcas,
			      long untpak, 
			      long untpal,
                              long frsflg,
                              long non_alc_flg,
                              long min_shelf_hrs,
                              char *frsdte)
{
    FAILED_ALLOCS *fa_p;

    fa_p = (FAILED_ALLOCS *)calloc(1, sizeof(FAILED_ALLOCS));
    if (fa_p == NULL)
	return(eNO_MEMORY);

    if (prtnum)	strcpy(fa_p->prtnum, prtnum);
    if (prt_client_id)	strcpy(fa_p->prt_client_id, prt_client_id);
    if (orgcod)	strcpy(fa_p->orgcod, orgcod);
    if (revlvl)	strcpy(fa_p->revlvl, revlvl);
    if (lotnum)	strcpy(fa_p->lotnum, lotnum);
    if (invsts_prg) strcpy(fa_p->invsts_prg, invsts_prg);
    if (dstare)	strcpy(fa_p->dstare, dstare);
    if (dstloc)	strcpy(fa_p->dstloc, dstloc);
    fa_p->splflg = splflg;
    fa_p->untcas = untcas;
    fa_p->untpak = untpak;
    fa_p->untpal = untpal;
    fa_p->frsflg = frsflg;
    fa_p->non_alc_flg = non_alc_flg;
    fa_p->min_shelf_hrs = min_shelf_hrs;
    if (frsdte) strcpy(fa_p->frsdte, frsdte);

    if (*FailedAllocHead)
	fa_p->next = *FailedAllocHead;

    *FailedAllocHead = fa_p;

    return(eOK);
    
}
/* Returns TRUE if there is a previous allocation like this 
 * one that has failed (meaning this one is likely to fail as well).
 */
static moca_bool_t sWillAllocFail(FAILED_ALLOCS *FHead,
				  WORKING_STRUCTURE *request)
{
    FAILED_ALLOCS *fa;

    if (FHead == NULL)
	return(BOOLEAN_FALSE);

    misTrc(T_FLOW, "Optimizing against failed allocation queue");

    for (fa = FHead; fa; fa = fa->next)
    {
	if (strcmp(fa->prtnum, request->prtnum) == 0 &&
	    strcmp(fa->prt_client_id, request->prt_client_id) == 0 &&
	    strcmp(fa->lotnum, request->lotnum) == 0 &&
	    strcmp(fa->revlvl, request->revlvl) == 0 &&
	    strcmp(fa->orgcod, request->orgcod) == 0 &&
	    strcmp(fa->dstare, request->dstare) == 0 &&
	    strcmp(fa->dstloc, request->dstloc) == 0 &&
	    strcmp(fa->invsts_prg, request->invsts_prg) == 0 &&
	    fa->splflg == request->splflg &&
	    fa->untcas == request->alccas &&
	    fa->untpak == request->alcpak &&
	    fa->untpal == request->alcpal &&
            fa->frsflg == request->frsflg &&
            fa->non_alc_flg == request->non_alc_flg &&
            fa->min_shelf_hrs == request->min_shelf_hrs &&
            strcmp(fa->frsdte, request->frsdte) == 0)
	{
	    misTrc(T_FLOW, 
		   "Found previous allocation attempt which already failed - "
		   "SKIPPING inventory lookup!");
	    return(BOOLEAN_TRUE);
	}
    }
    
    misTrc(T_FLOW, "Streaming optimization complete - no savings found");
    return(BOOLEAN_FALSE);
}

static void sFreeFailedAllocList(FAILED_ALLOCS *head)
{
    FAILED_ALLOCS *fa, *last;

    last = NULL;
    for (fa = head; fa; last = fa, fa = fa->next)
    {
	if (last)
	    free(last);
    }

    if (last)
	free(last);
    return;
}

static void sFreeWorkingList(WORKING_STRUCTURE *head)
{
    WORKING_STRUCTURE *lastptr, *tmpptr;

    lastptr = NULL;
    for (tmpptr = head; tmpptr; lastptr = tmpptr, tmpptr = tmpptr->next) 
    {
	if (lastptr)
	    free (lastptr);
    }
    if (lastptr)
	free(lastptr);

    return;
}

LIBEXPORT
RETURN_STRUCT *varAllocatePickGroup(char *pcktyp_i, 
                           	    char *pcksts_i, 
				    char *ship_id_i,
			   	    char *ship_line_id_i,
			   	    char *client_id_i, 
				    char *ordnum_i,
			   	    char *pckgr1_i, char *pckgr2_i, 
				    char *pckgr3_i, char *pckgr4_i,
			   	    char *carcod_i, char *srvlvl_i, 
				    char *pricod_i,
			   	    char *consby_i, 
			   	    char *restyp_i, 
			   	    char *resval_i,
			   	    char *arecod_i, 
			   	    char *dstare_i, 
				    char *dstloc_i,
			   	    char *schbat_i, 
				    char *lblseqstrategy,
			   	    moca_bool_t *comflg_i, 
				    char *batcod_i,
				    moca_bool_t *rrlflg_i,
                                    char *pipcod_i)
{
    long ret_status;
    RETURN_STRUCT *CurPtr=NULL, *AllPtrs=NULL;
    mocaDataRes *res=NULL, *res1=NULL, *tempres=NULL;
    mocaDataRes *rplres;
    mocaDataRow *rplrow;
    mocaDataRow *row=NULL, *row1=NULL, *temprow=NULL;
    char sqlbuffer[3000];
    long errcode;
    char tmpbuf[50];
    char tmpbuf2[200];
    char pricod_buf[100];
    char saved_ship_id[SHIP_ID_LEN+1];
    char pipcod[PIPCOD_LEN+1];
    long loop_cnt = 0;

    FAILED_ALLOCS *FailedAllocHead = NULL;

    char where_clause[2500];
    char temp_clause[300];
    char table_name[20];
    char work_schbat[SCHBAT_LEN+1];
    char batcod[BATCOD_LEN+1];
    char work_concod[CONCOD_LEN+1];
    char *all_schbat;
    char *all_ship_id;
    char *all_ship_line_id;
    char *all_stcust;
    char *all_rtcust;
    char *all_client_id;
    char *all_ordnum;
    char *all_ordlin;
    char *all_ordsln;
    char *all_concod;
    char *all_segqty;
    char *all_dstare;
    char *all_dstloc;
    long all_pckqty;
    long rpckqty = 0;
    static long pallet_volume;
    long num_segments;
    WORKING_STRUCTURE	*Top, *ptr, *ShipTop;
    WORKING_STRUCTURE	*lastptr, *tmpptr;
    char dstare[ARECOD_LEN+1];
    char dstloc[STOLOC_LEN+1];
    moca_bool_t comflg;
    moca_bool_t rrlflg;
    char pcksts[PCKSTS_LEN+1];
    char xdkref[XDKREF_LEN+1];
    
    char consby_in[50];
    static short BreakShipids;
    static short PolXDockInstalled = FALSE;
    static short PolPIProcessing = FALSE;
    short WriteXDock;
    short running_pip_in_parallel;
    long xdk_created = FALSE;
    long add_to_failure;
    long setForXDock = 0;

    ShipTop = 0;
    lastptr = 0;

    memset (batcod, 0,sizeof(batcod));
    if (batcod_i && misTrimLen(batcod_i,BATCOD_LEN))
      misTrimcpy(batcod, batcod_i, BATCOD_LEN);

    memset(pcksts, 0, sizeof(pcksts));
    if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
	misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);

    /* Format the incoming variables */
    memset(dstare,'\0',sizeof(dstare));
    if (dstare_i && misTrimLen(dstare_i,ARECOD_LEN))
	misTrimcpy(dstare,dstare_i,ARECOD_LEN);
    
    memset(dstloc,'\0',sizeof(dstloc));
    if (dstloc_i && misTrimLen(dstloc_i,STOLOC_LEN))
	misTrimcpy(dstloc,dstloc_i,STOLOC_LEN);

    comflg = BOOLEAN_NOTSET;
    if (comflg_i)
        comflg = *comflg_i;
    else
    {
        /* 
         * If comflg is passed in, then use the passed value.
         * or get the value from policy.
         * In the past, many project teams customized the allocate
         * pick group and hard-coded it to pass a 1 into the C function
         * for the comflg.
         * This policy will prevent those types of customizations.
         * The purpose of this flag is to indicate whether or not 
         * commits occur during allocation, and is often turned on to
         * enhance performance.
         */
        sprintf(sqlbuffer,
                "select rtnum1 from poldat "
                "where polcod = '%s' "
                "  and polval = '%s' "
                "  and polvar = '%s' ",
                POLCOD_ALLOCATE_INV,
                POLVAR_MISC,
                POLVAL_COMMIT_DURING_ALLOC);
        errcode = sqlExecStr(sqlbuffer,&res);
        if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED) 
        {
            sqlFreeResults(res);
            return(srvResults(errcode, NULL));
        }
        else if (errcode == eOK)
        {
            row = sqlGetRow(res);
	        comflg = sqlGetLong(res,row,"rtnum1");
            sqlFreeResults(res);
        }
    }


    rrlflg = BOOLEAN_NOTSET;
    if (rrlflg_i)
        rrlflg = *rrlflg_i;

    memset(consby_in, 0, sizeof(consby_in));
    if (consby_i && misTrimLen(consby_i, 10))
	strncpy(consby_in, consby_i, 
		misTrimLen(consby_i, sizeof(consby_in)-1));

    memset(table_name,0,sizeof(table_name));
    strcpy(table_name,"sl");

    memset(where_clause,0,sizeof(where_clause));
    if ((pckgr1_i) && strlen(pckgr1_i)) 
    {
	sprintf(where_clause,"%s.pckgr1 = '%s' ",table_name,pckgr1_i);
    }
    else
    {
	/*
	 * Either a pick group or schedule batch is required.
	 */

	if (!schbat_i || misTrimLen(schbat_i, SCHBAT_LEN) == 0)
            return (APPMissingArg("pckgr1"));
    }
    if ((pckgr2_i) && strlen(pckgr2_i)) 
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause,"%s.pckgr2 = '%s' ",table_name,pckgr2_i);
	else 
	{
	    sprintf(temp_clause,"and %s.pckgr2 = '%s' ",table_name,pckgr2_i);
	    strcat(where_clause, temp_clause);
	}
    }
    if ((pckgr3_i) && strlen(pckgr3_i)) 
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause,"%s.pckgr3 = '%s' ",table_name,pckgr3_i);
	else 
	{
	    sprintf(temp_clause,"and %s.pckgr3 = '%s' ",table_name,pckgr3_i);
	    strcat(where_clause, temp_clause);
	}
    }
    if ((pckgr4_i) && strlen(pckgr4_i)) 
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause,"%s.pckgr4 = '%s' ",table_name,pckgr4_i);
	else 
	{
	    sprintf(temp_clause,"and %s.pckgr4 = '%s' ",table_name,pckgr4_i);
	    strcat(where_clause, temp_clause);
	}
    }

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause, "%s.ship_id = '%s' ", table_name, ship_id_i);
	else 
	{
	    sprintf(temp_clause, 
		    "and %s.ship_id = '%s' ", 
		    table_name, 
		    ship_id_i);
	    strcat(where_clause, temp_clause);
	}
    }

    if (ship_line_id_i && misTrimLen(ship_line_id_i, SHIP_LINE_ID_LEN))
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause,
		    "%s.ship_line_id = '%s' ",
		    table_name,
		    ship_line_id_i);
	else 
	{
	    sprintf(temp_clause,
		    "and %s.ship_line_id = '%s' ",
		    table_name,
		    ship_line_id_i);
	    strcat(where_clause, temp_clause);
	}
    }

    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
    {
	if (strlen(where_clause)==0)
	    sprintf(where_clause,"%s.schbat = '%s' ",table_name,schbat_i);
	else 
	{
	    sprintf(temp_clause,"and %s.schbat = '%s' ",table_name,schbat_i);
	    strcat(where_clause, temp_clause);
	}
    }

    if (strlen(where_clause)==0) 
    {
	misTrc(T_FLOW, "Missing data to query shipemnts with");
	return(srvResults(eINVALID_ARGS, NULL));
    }

    /* The strncmp checks to make sure if we got a consby, it's one 
     * of the following values.  However, if we didn't get one at all
     * we get the message 'Invalid value [] for consby.  Therefore,
     * we will check first to see if it is null, so we can give a 
     * better message.  If we ever decide that consby is not needed,
     * we should remove the next if, and fix the if with the strncmp
     * to ignore a null value.
     */

    if (strlen(consby_in) == 0)
        return (APPMissingArg("consby"));

    if (misCiStrncmp(consby_in,"ship_id",7) !=0 &&
	misCiStrncmp(consby_in,"stop_id",7) !=0 &&
	misCiStrncmp(consby_in,"rtcust",6) !=0 &&
	misCiStrncmp(consby_in,"stcust",6) != 0 &&
	misCiStrncmp(consby_in,"all",3) != 0)
	return (APPInvalidArg(consby_in, "consby"));

    /* Added as a point to trigger off of */
    CurPtr = NULL;
    errcode = srvInitiateCommandFormat (NULL,
	    "register pick group allocation "
	    " where pckgr1 = '%s' "
	    "   and pckgr2 = '%s' "
	    "   and pckgr3 = '%s' "
	    "   and pckgr4 = '%s' "
	    "   and schbat = '%s' ",
	    pckgr1_i ? pckgr1_i : "",
	    pckgr2_i ? pckgr2_i : "",
	    pckgr3_i ? pckgr3_i : "",
	    pckgr4_i ? pckgr4_i : "",
	    schbat_i ? schbat_i : "");

    if (errcode != eOK)
	return (srvResults(errcode, NULL));

    running_pip_in_parallel = FALSE;
    if (appIsInstalled(POLCOD_PRE_INV_PICKING))
    {
        PolPIProcessing = TRUE;
        sprintf(sqlbuffer,
                "select 1 from poldat "
                "where polcod = '%s' "
                " and polval = '%s' "
                " and polvar = '%s' "
                " and rtnum1 = 1",
                POLCOD_PRE_INV_PICKING,
                POLVAL_PARALLEL_ALLOC,
                POLVAR_OPERATING_MODE);
        errcode = sqlExecStr(sqlbuffer,NULL);
        if (errcode == eOK)
        {
            running_pip_in_parallel = TRUE;
        }
    }

    if (pallet_volume < 1) 
    {
	pallet_volume = 120960;
	sprintf(sqlbuffer,
		"select rtnum1 from poldat "
		"where polcod = '%s' "
		" and polval = '%s' ",
		POLCOD_PROC_PICK_REL,
		POLVAL_REL_PALLET_VOLUME);
	errcode = sqlExecStr(sqlbuffer,&res);
	if (errcode == eOK) 
	{
	    row = sqlGetRow(res);
	    pallet_volume = sqlGetLong(res,row,"rtnum1");
	}
	if (res)    sqlFreeResults(res);

	/*
	** Check if cross docking is installed
	*/
	if (appIsInstalled(POLCOD_CROSS_DOCKING))
	{
	    PolXDockInstalled = TRUE;
	}
    }

    /* If we got a carrier code sent in, then use it to update */
    if ((carcod_i) && strlen(carcod_i)) 
    {
	sprintf(sqlbuffer,
		"update shipment "
		"   set carcod = '%s' ",
		carcod_i);
        /* 
         * Make sure we only update the service level if it also 
         * get passed.
         */

        memset(tmpbuf2, 0, sizeof(tmpbuf2));
        if ((srvlvl_i) && misTrimLen(srvlvl_i, SRVLVL_LEN))
        {
            sprintf(tmpbuf2, ", srvlvl = '%s' ", srvlvl_i);
            strcat(sqlbuffer, tmpbuf2);
        }

	sprintf(tmpbuf2,
		" where (ship_id) in "
		" (select distinct ship_id "
		"    from shipment_line sl where %s ) ",
		where_clause);
        strcat(sqlbuffer, tmpbuf2);

	errcode = sqlExecStr(sqlbuffer,NULL);
	if (errcode != eOK) 
	    return(srvResults(errcode, NULL));
    }

    memset (pipcod, 0, sizeof(pipcod));
    if ((pipcod_i) && strlen(pipcod_i)) 
    {
        strncpy (pipcod, pipcod_i, PIPCOD_LEN);
    }

    /* If we got a release remaining flag sent in, then use it to update */
    if (rrlflg != BOOLEAN_NOTSET) 
    {
	sprintf(sqlbuffer,
		"update shipment "
		"   set rrlflg = '%ld' "
		" where (ship_id) in "
		" (select distinct ship_id "
		"    from shipment_line sl where %s ) ",
		rrlflg,
		where_clause);
	errcode = sqlExecStr(sqlbuffer,NULL);
	if (errcode != eOK) 
	    return(srvResults(errcode, NULL));
    }

    /* Get any pckqty from rplwrk by the pick group created where clause */
    sprintf(sqlbuffer,
	    "select r.ship_line_id, sum(r.pckqty) rpl_pckqty "
	    "  from rplwrk r, shipment_line sl "
	    " where %s "
	    "   and r.ship_line_id = sl.ship_line_id "
	    " group by r.ship_line_id",
	    where_clause);
    errcode = sqlExecStr(sqlbuffer, &rplres);
    if (errcode != eOK && errcode != eDB_NO_ROWS_AFFECTED) 
    {
	sqlFreeResults(rplres);
	return(srvResults(errcode, NULL));
    }

    /* Select all of the information from the shipment and order line tables */
    /* by the pick group created where clause */
    /* Add to_number to select below because nvl(sl.pckqty,0) gets stored as */
    /* different datatypes in sqlserver and oracle.  See PR 33318 */

    sprintf(sqlbuffer,
	    "select sl.client_id, sl.ordnum, "
            "       sl.prcpri, to_number(nvl(sl.pckqty,0)) pckqty, "
	    "       sl.ordlin, sl.ordsln, sl.ship_line_id, "
	    "       sh.carcod, sh.srvlvl, sh.dstare, "
	    "       sh.dstloc, sh.ship_id, sh.doc_num, "
	    "       o.stcust, o.rtcust, o.ordtyp, "
	    "       ol.prtnum, ol.prt_client_id, "
	    "       ol.orgcod, ol.revlvl, ol.lotnum, "
	    "       ol.marcod, ol.invsts_prg, ol.splflg, "
	    "       ol.parflg, ol.rpqflg, ol.stdflg, "
	    "       ol.xdkflg, ol.entdte, "
            "       ol.frsflg, ol.non_alc_flg, "
            "       nvl(ol.min_shelf_hrs, -1) min_shelf_hrs, "
	    "	    nvl(ol.untcas, 0) alccas, "
	    "       nvl(ol.untpak, 0) alcpak, " 
	    "       nvl(ol.untpal, 0) alcpal, " 
	    "       p.untpal, p.untcas, sh.stop_id, "
	    "	    (f.caslen * f.cashgt * f.caswid) casvol "
	    "       from ftpmst f, prtmst p, shipment sh, shipment_line sl, "
	    "            ord o, ord_line ol "
	    "       where %s "
	    "         and nvl(sl.pckqty,0) > 0 "
	    "         and sl.linsts    in ('%s','%s') "
	    "         and sh.ship_id          = sl.ship_id "
	    "         and sh.stgdte is null "
	    "         and sl.client_id        = o.client_id "
	    "         and sl.ordnum           = o.ordnum "
	    "         and sl.client_id        = ol.client_id "
	    "         and sl.ordnum           = ol.ordnum "
	    "         and sl.ordlin           = ol.ordlin "
	    "         and sl.ordsln           = ol.ordsln "
	    "         and ol.prtnum           = p.prtnum "
	    "         and ol.prt_client_id    = p.prt_client_id "
	    "         and p.ftpcod            = f.ftpcod "
	    "       order by o.stcust, sh.carcod, sh.srvlvl, "
	    "	             sl.ship_id, "
	    "                sl.client_id, "
	    "	             sl.ordnum, ol.prtnum, "
	    "                ol.prt_client_id "
	    "     for update of sl.linsts",
	    where_clause,
	    LINSTS_PENDING,
	    LINSTS_INPROCESS);
    
    errcode = sqlExecStr(sqlbuffer,&res);
    if (errcode != eOK) 
    {
	sqlFreeResults(res);
	sqlFreeResults(rplres);
	return(srvResults(errcode, NULL));
    }
    if (sqlGetNumRows(rplres) > 0)
    {
	/* We have to take into consideration any outstanding replenishment
	 * quantity and decrement against matching ship_line_id's
	 */
	misTrc(T_FLOW, 
	       "Attempting to adjust pckqty for %d lines "
	       "which have outstanding replenishments",
	       sqlGetNumRows(rplres));

	for (rplrow = sqlGetRow(rplres); rplrow; 
	     rplrow = sqlGetNextRow(rplrow))
	{
	    long found;
	    char ship_line_id[SHIP_LINE_ID_LEN+1];

	    memset(ship_line_id, 0, sizeof(ship_line_id));
	    strncpy(ship_line_id, 
		    sqlGetString(rplres, rplrow, "ship_line_id"), 
		    SHIP_LINE_ID_LEN);

	    found = 0;
	    for (row = sqlGetRow(res); row && !found; row = sqlGetNextRow(row))
	    {
		if (strncmp(ship_line_id, 
			    sqlGetString(res, row, "ship_line_id"),
			    SHIP_LINE_ID_LEN) == 0)
		{
		    long pckqty;
		    long r_pckqty;

		    pckqty = sqlGetLong(res, row, "pckqty");
		    r_pckqty = sqlGetLong(rplres, rplrow, "rpl_pckqty");

		    misTrc(T_FLOW,
			   "Decrementing original pckqty of %d "
			   "by %d due to replens",
			   pckqty, r_pckqty);

		    pckqty -= r_pckqty;

		    if (pckqty < 0)
		    {
			misTrc(T_FLOW,
			       "Warning: adjusted pick qty "
			       "for ship_line_id(%s) is < 0, (%d)",
			       ship_line_id, pckqty);
			misTrc(T_FLOW, 
			       "Resetting to zero");
			pckqty = 0;
		    }
		    found = 1;

		    sqlSetFloat(res, row, "pckqty", (double) pckqty);
		    misTrc(T_FLOW,
			   "Reset pick quantity - value is now: %d",
			   sqlGetLong(res, row, "pckqty"));
		}
	    }
	}
    }
    sqlFreeResults(rplres);
	
    /* Load them up into our structure */
    Top = NULL;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) 
    {
	/* Build the linked list */
	if (Top == NULL) 
	{
	    Top = (WORKING_STRUCTURE *) calloc(1, sizeof(WORKING_STRUCTURE));
	    if (Top == NULL)
		return(srvResults(eNO_MEMORY, NULL));
	    ptr = Top;
	    memset(ptr,0,sizeof(WORKING_STRUCTURE));
	}
	else 
	{
	    ptr = (WORKING_STRUCTURE *) calloc(1, sizeof(WORKING_STRUCTURE));
	    if (ptr == NULL)
		return(srvResults(eNO_MEMORY, NULL));
	    memset(ptr,0,sizeof(WORKING_STRUCTURE));
	    ptr->prev = lastptr;
	    lastptr->next = ptr;
	}
	lastptr = ptr;

	/* Now load up the structure */
	strncpy(ptr->prtnum,sqlGetString(res,row,"prtnum"),PRTNUM_LEN);
	strncpy(ptr->prt_client_id,sqlGetString(res,row,"prt_client_id"),
							       CLIENT_ID_LEN);
	strncpy(ptr->orgcod,sqlGetString(res,row,"orgcod"),ORGCOD_LEN);
	strncpy(ptr->revlvl,sqlGetString(res,row,"revlvl"),REVLVL_LEN);
	strncpy(ptr->lotnum,sqlGetString(res,row,"lotnum"),LOTNUM_LEN);
	strncpy(ptr->ordtyp,sqlGetString(res,row,"ordtyp"),ORDTYP_LEN);
	strncpy(ptr->invsts_prg,
                sqlGetString(res,row,"invsts_prg"),
                INVSTS_PRG_LEN);
	ptr->pckqty = sqlGetLong (res,row,"pckqty");
	strncpy(ptr->client_id, 
		     sqlGetString(res,row,"client_id"),CLIENT_ID_LEN);
	strncpy(ptr->prcpri,sqlGetString(res,row,"prcpri"),PRCPRI_LEN);
	strncpy(ptr->ordnum,sqlGetString(res,row,"ordnum"),ORDNUM_LEN);
	strncpy(ptr->ordlin,sqlGetString(res,row,"ordlin"),ORDLIN_LEN);
	strncpy(ptr->ordsln,sqlGetString(res,row,"ordsln"),ORDSLN_LEN);
	strncpy(ptr->stcust,sqlGetString(res,row,"stcust"),STCUST_LEN);
	strncpy(ptr->rtcust,sqlGetString(res,row,"rtcust"),RTCUST_LEN);
	strncpy(ptr->carcod,sqlGetString(res,row,"carcod"),CARCOD_LEN);
	strncpy(ptr->srvlvl,sqlGetString(res,row,"srvlvl"),SRVLVL_LEN);
	strncpy(ptr->marcod,sqlGetString(res,row,"marcod"),MARCOD_LEN);
	ptr->parflg = sqlGetBoolean(res,row,"parflg");
	ptr->rpqflg = sqlGetBoolean(res,row,"rpqflg");
	ptr->splflg = sqlGetBoolean(res,row,"splflg");
	ptr->stdflg = sqlGetBoolean(res,row,"stdflg");
	ptr->xdkflg = sqlGetBoolean(res,row,"xdkflg");
	strncpy(ptr->entdte,sqlGetValue(res,row,"entdte"),DB_STD_DATE_LEN);
	ptr->untpal = sqlGetLong(res,row,"untpal");
	if (ptr->untpal == 0)	ptr->untpal = 1;
	ptr->untcas = sqlGetLong(res,row,"untcas");
	if (ptr->untcas == 0)	ptr->untcas = 1;
	ptr->casvol = sqlGetFloat(res,row,"casvol");
	strncpy(ptr->ship_id, sqlGetString(res,row,"ship_id"), SHIP_ID_LEN);
	strncpy(ptr->ship_line_id, sqlGetString(res,row,"ship_line_id"), SHIP_LINE_ID_LEN);
	ptr->alccas = sqlGetLong(res,row,"alccas");
	ptr->alcpak = sqlGetLong(res,row,"alcpak");
	ptr->alcpal = sqlGetLong(res,row,"alcpal");
        ptr->frsflg = sqlGetBoolean(res, row, "frsflg");
        ptr->non_alc_flg = sqlGetBoolean(res, row, "non_alc_flg");
        ptr->min_shelf_hrs = sqlGetLong(res, row, "min_shelf_hrs");
	/* we'll just initialize the xdk_inserted member to be false */
	ptr->xdk_inserted = BOOLEAN_FALSE;

	/*  If the standard case flag is set (Y), and the allocate case value
	**  (untcas on the detail) is zero reset the alccas to the 
	**  untcas value from the prtmst.
	    TODO:  Does stdflg include the inner pack and pallet also???4-13-98
	*/
	if (ptr->stdflg == BOOLEAN_TRUE && (ptr->alccas == 0))
	{
	    ptr->alccas = sqlGetLong(res,row,"untcas");
	}

        /* 
         * If we are doing freshness date processing, get the freshness
         * date to use.  It is important for our calculations on the number
         * of segments, etc.
         *
         * This function will set the ptr->frsdte member of the structure.
         */
        if (ptr->frsflg)
        {
            ret_status = trnGetCstFrsDte(ptr->prtnum,
                                         ptr->client_id,
                                         ptr->stcust,
                                         ptr->rtcust,
                                         ptr->frsdte);
            if (eOK != ret_status)
            {
                /* Exit out of here */
                sqlFreeResults(res);
		sFreeWorkingList(Top);
                return(srvResults(ret_status, NULL));
            }
        }

	/*
	*  If xdocking and we've assigned a dstloc (stage lane) for 
	*  this shipment, we need to send all the lines to it.
        *  Note this only matters if the lines are set up for cross-docking.
        *  The setForXDock helps keep track of whether we may need to 
        *  eventually update the dstloc on the shipment line, because
        *  event though cross-docking is installed, we will only want
        *  to update the shipment line if the xdkflg on the shipment line
        *  is true.
	*/
                
        setForXDock = 0;
	if (PolXDockInstalled)
	{
            /*
             * Need to handle where cross docking flag is set, bug
             * we don't yet have the dstloc assigned.
             */

	    sprintf(sqlbuffer,
		    "select sh.dstloc, l.arecod "
		    "  from shipment sh, locmst l, "
		    "       shipment_line sl, ord_line ol "
		    " where sh.ship_id = '%s' "
		    "   and ol.xdkflg  = '%d' "	
		    "   and sh.dstloc    = l.stoloc(+) "
		    "   and sh.ship_id   = sl.ship_id "
		    "   and sl.client_id = ol.client_id "
		    "   and sl.ordnum    = ol.ordnum "
		    "   and sl.ordlin    = ol.ordlin "
		    "   and sl.ordsln    = ol.ordsln ",
		    ptr->ship_id, BOOLEAN_TRUE);

	    errcode = sqlExecStr(sqlbuffer, &res1);
	    if (errcode == eOK)
	    {
                /*
                 * This is a cross-dockable shipment.  If it already has
                 * a dstloc assigned, let's use it.
                 */
		row1 = sqlGetRow(res1);
                if (!sqlIsNull(res1, row1, "dstloc"))
                {
		    misTrimcpy(ptr->dstare,
		  	       sqlGetString(res1,row1,"arecod"), ARECOD_LEN);
		    misTrimcpy(ptr->dstloc,
			       sqlGetString(res1,row1,"dstloc"), STOLOC_LEN);
                    if (strlen(ptr->dstloc))
                        misTrc(T_FLOW, "Dest Location assigned to shipment: %s",
                           ptr->dstloc);
                }
                setForXDock = 1;
	    }
	    sqlFreeResults(res1);
	}

	/* If we got a destination area or location ... 
	/*	 then take the arguments 
        /*       else try to use ones already set on the shipment
	/* Otherwise, look them up using the GET ORDER DESTINATION command */
	if (strlen(dstare) || strlen(dstloc)) 
	{
            misTrc(T_FLOW, "Destination area %s and location %s were passed",
                   strlen(dstare) ? dstare : "",
                   strlen(dstloc) ? dstloc : "");
            /*
             * At this point, ptr->dstare is only set if cross docking
             * was installed and there are order lines that are set up
             * for cross docking on the shipment.  If installed, and 
             * this value is set, we have to use it since we may have 
             * other shipment lines
             */

            if (PolXDockInstalled)
            {
                /*
                 * If the user passed something that doesn't match
                 * where we are cross docking to, give them an error.
                 */

		if ((strlen(ptr->dstloc) && strlen(dstloc) &&
		    strncmp(dstloc, ptr->dstloc, STOLOC_LEN)) ||
		    (strlen(ptr->dstare) && strlen(dstare) &&
		     strncmp(dstare, ptr->dstare, ARECOD_LEN)))
	        {
                    misTrc(T_FLOW, "Cross docking is set for shipment, and "
                           "dstare/dstloc passed do not match values "
                           "on the shipment. ");
		    sqlFreeResults (res);
		    sFreeWorkingList(Top);
		    return(srvResults(eINT_PARM_MISMATCH, NULL));
	        }
            }

            /*
             * If we are at this point, if a dstare and dstloc were passed
             * we can use them because either we are not set up for
             * cross docking, or we passed the above check that verified
             * they were equal.
             */

            if (strlen(dstare)) 
            { 
                misTrc(T_FLOW, "Using passed in dstare: %s", dstare);
	        strcpy(ptr->dstare,dstare);
            }

	    if (strlen(dstloc)) 
            {
                misTrc(T_FLOW, "Using passed in dstloc: %s", dstloc);
	        strcpy(ptr->dstloc,dstloc);
            }                         
	}
	else
	{
            /*
             * They weren't passed. 
             * If they weren't already set by cross docking, see if they 
             * already exist on the  shipment. 
             */

	    if (strlen(ptr->dstloc)==0)
            {
		strcpy(ptr->dstloc, sqlGetString(res,row,"dstloc"));
                if (strlen(ptr->dstloc))
                    misTrc(T_FLOW, 
                           "Using dstloc: %s already assigned to shipment",
                           sqlGetString(res, row, "dstloc"));
            }
	    if (strlen(ptr->dstare)==0) 
            {
		strcpy(ptr->dstare, sqlGetString(res,row,"dstare"));
                if (strlen(ptr->dstare))
                    misTrc(T_FLOW, 
                           "Using dstare: %s already assigned to shipment",
                           sqlGetString(res, row, "dstare"));
            }
	}

	/* if we just have the dstloc, get the dstare */
        if ((strlen(ptr->dstloc)) && strlen(ptr->dstare) == 0)
        {
	    sprintf(sqlbuffer,
		    "select arecod "
		    "  from locmst "
		    " where stoloc = '%s' ",
		    ptr->dstloc); 

	    errcode = sqlExecStr(sqlbuffer, &res1);
	    if (errcode == eOK)
	    {
	        row1 = sqlGetRow(res1);
		misTrimcpy(ptr->dstare,
			   sqlGetString(res1,row1,"arecod"), ARECOD_LEN);
	    }
	    sqlFreeResults(res1);
        }

        /*
         * If cross docking is installed, we need to update the shipment
         * with the dstare and dstloc specified, because they are used
         * on down the line by cross-docking.  Otherwise, sending the
         * dstare and dstloc at allocation is a 1-time thing, so we won't
         * update the values (in case they were sent by the host or something.
         */

	if ((PolXDockInstalled) && setForXDock && strlen(ptr->dstloc)) 
        {
    	    /* Update shipment with these dstare and dstloc values.
             * We'll only do this if cross docking is installed.
             * Otherwise, we'll consider the values a temporary for
             * this allocation attempt (unless they were set on the
             * shipment already, in which case we don't have to bother
             * with an update).
             */
	    sprintf(sqlbuffer,
		"update shipment "
		"   set dstare  = '%s', "
		"       dstloc  = '%s' "
		" where ship_id = '%s' ",
		ptr->dstare, 
                ptr->dstloc,
		ptr->ship_id);
	    errcode = sqlExecStr(sqlbuffer,NULL);
	    if (errcode != eOK) 
	    {
		sqlFreeResults (res);
		sFreeWorkingList(Top);
	    	return(srvResults(errcode, NULL));
	    }
	}

        /*
         * At this point, if we have a dstloc determined, we need
         * to allocate the resource location for it.
         * (since we either have it due to cross-docking, it's on
         * the shipment, or the user specifically specified it.)
         * Otherwise, it won't get allocated until pick release.
         */

        if (strlen(ptr->dstloc))
        {
	    errcode = srvInitiateCommandFormat (NULL,
		    "allocate resource location "
		    " where stoloc  = '%s' "
		    "   and ship_id = '%s' ",
		    ptr->dstloc,
		    ptr->ship_id);
	    if (errcode != eOK) 
	    {
		sqlFreeResults (res);
		sFreeWorkingList(Top);
		return(srvResults(errcode, NULL));
	    }
        }

        /*
         * It wasn't set on the shipment, and it wasn't passed in,
         * so let's try to determine the order's destination.
         */

	if (strlen(ptr->dstare)==0)
	{
	    CurPtr = NULL;
	    errcode = srvInitiateCommandFormat (&CurPtr,
		    "get order destination "
		    " where ship_id      = '%s' "
		    "   and ship_line_id = '%s' "
		    "   and client_id    = '%s' "
		    "   and ordnum       = '%s' "
		    "   and ordlin       = '%s' and ordsln = '%s' "
		    "   and carcod       = '%s' and srvlvl = '%s' "
		    "   and marcod       = '%s' and ordtyp = '%s' ",
		    ptr->ship_id,
		    ptr->ship_line_id,
		    ptr->client_id,
		    ptr->ordnum,
		    ptr->ordlin,
		    ptr->ordsln,
		    ptr->carcod,
		    ptr->srvlvl,
		    ptr->marcod,
		    ptr->ordtyp);
	    if (errcode != eOK) 
	    {
		srvFreeMemory(SRVRET_STRUCT, CurPtr);
		sqlFreeResults (res);
		sFreeWorkingList(Top);
		return(srvResults(errcode, NULL));
	    }
            tempres = srvGetResults(CurPtr);
	    temprow = sqlGetRow(tempres);
	    strncpy(ptr->dstare,
		    sqlGetString(tempres, temprow,"dstare"),ARECOD_LEN);
            if (!sqlIsNull(tempres, temprow, "dstloc"))
            {
	        strncpy(ptr->dstloc,
		        sqlGetString(tempres, temprow, "dstloc"),STOLOC_LEN);
            }
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	}

/* MF */ 

	misTrc(T_FLOW, "pckqty %d", ptr->pckqty ); 
        misTrc(T_FLOW, "alccas %d", ptr->alccas );
        misTrc(T_FLOW, "untcas %d", ptr->untcas );
        misTrc(T_FLOW, "formula: %d", (ptr->alccas?ptr->alccas:ptr->untcas));

        if (ptr->pckqty % (ptr->alccas?ptr->alccas:ptr->untcas)) 
            misTrc(T_FLOW, "MOD CASE");

        if (ptr->pckqty < (ptr->alccas?ptr->alccas:ptr->untcas))
        {
	    misTrc(T_FLOW, "pckqty < case_quantity");
             ptr->partial_case = TRUE; 
	}
        else 
	  if (ptr->pckqty % (ptr->alccas?ptr->alccas:ptr->untcas)) 
	  {
	    misTrc(T_FLOW, "MOD CASE");
	    tmpptr = (WORKING_STRUCTURE *) calloc(1, sizeof(WORKING_STRUCTURE));
	    if (tmpptr == NULL)
		return(srvResults(eNO_MEMORY, NULL));
	    memcpy(tmpptr, ptr, sizeof(WORKING_STRUCTURE));
	    tmpptr->prev = ptr;
	    ptr->next = tmpptr;
	    lastptr = tmpptr;
	    tmpptr->pckqty = ptr->pckqty % (ptr->alccas?ptr->alccas:ptr->untcas);
	    ptr->pckqty = ptr->pckqty - (ptr->pckqty % (ptr->alccas?ptr->alccas:ptr->untcas));
            tmpptr->partial_case = TRUE; 
	    tmpptr->next = NULL;
	  }

    }
    sqlFreeResults(res);

   
    memset(work_schbat,0,sizeof(work_schbat));
    if (schbat_i && misTrimLen(schbat_i,SCHBAT_LEN)) 
    {
	misTrimcpy(work_schbat, schbat_i, SCHBAT_LEN);
    }
    else
    {
        /*
         * The following situation can occur when running allocate pick group
         * with comflg = 1
         *
         * 1.  Allocate pick group is started, and a new schbat is created
         *     for the pick group.
         * 2.  The pick batch record is created in a state of IN-PROCESS
         * 3.  allocate inventory runs and fails with a DEADLOCK (-60)
         * 4.  allocate pick group exits with a return status of -60
         * 5.  MOCA detects the deadlock, and reruns the command according
         *     to design
         * 
         * In the above scenario, if comflg is 1, then it is quite possible
         * that part of the pick group is already created and committed.  In 
         * this case, when MOCA reruns the command, we don't want another 
         * schedule batch created.  This would result in a pick group that is 
         * split over multiple pick batches.  
         * We'll try to make use of the static schedule batch set by 
         * trnSetSchbat later on in this code.  When allocate pick group 
         * completes, trnClearSchbat is called to clear the static schedule 
         * batch.  In theory, if this function exists prematurely, then the 
         * static schedule batch variable should never have been cleared.  To 
         * safeguard against using an old schbat, we'll also check to make
         * sure the static schedule batch is still in process.
         */

        misTrimcpy(work_schbat, trnGetSchbat(), SCHBAT_LEN);

        if (misTrimLen(work_schbat, SCHBAT_LEN) > 0)
        {
            sprintf(sqlbuffer,
                    "select 'x' "
                    "  from pckbat "
                    " where schbat = '%s' "
                    "   and batsts = '%s' ",
                    work_schbat,
                    BATSTS_ALLOC_IN_PROC);

            errcode = sqlExecStr (sqlbuffer, NULL);
            if (eOK != errcode) 
            {
                /* Create the next schedule batch */
	        errcode = appNextNum(NUMCOD_SCHBAT, work_schbat);
	        if (errcode != eOK) 
	        {
	            /* Free up our working list */
	            sFreeWorkingList(Top);
	            return(srvResults(errcode, NULL));
	        }
            }
        }
        else
        {
            /* Create the next schedule batch */
            errcode = appNextNum(NUMCOD_SCHBAT, work_schbat);
            if (errcode != eOK) 
            {
                /* Free up our working list */
                sFreeWorkingList(Top);
                return(srvResults(errcode, NULL));
            }
        }
    }
    
    /* And if the batch doesn't already exist, then create it ... */
    sprintf(sqlbuffer,
	    "select cmpdte, "
	    "       batsts "
	    "  from pckbat "
	    " where schbat = '%s' ",
	    work_schbat);

    errcode = sqlExecStr(sqlbuffer,&res);
    if (errcode != eOK) 
    {
	sqlFreeResults(res);

	memset(temp_clause,'\0',sizeof(temp_clause));
	if (arecod_i && restyp_i && resval_i && 
		misTrimLen(arecod_i,strlen(arecod_i)) &&
		misTrimLen(restyp_i, strlen(restyp_i)) &&
		misTrimLen(resval_i, strlen(resval_i)))
	    sprintf(temp_clause," and arecod='%s' and restyp='%s' and "
		    " resval='%s' ",arecod_i,restyp_i,resval_i);

	memset(tmpbuf, 0, sizeof(tmpbuf));
	if (strlen(batcod))
	{
	  sprintf (tmpbuf, " and batcod = '%s' ", batcod);
	}
	
        /* See if we need to include a priority code in the pckbat record. */
        memset(pricod_buf, 0, sizeof(pricod_buf));
        if ((pricod_i && strlen(pricod_i)))
            sprintf(pricod_buf, "  and pricod = '%s'", pricod_i);

	CurPtr = NULL;
	errcode = srvInitiateCommandFormat (NULL,
		    "create pick batch "
		    " where schbat = '%s'  "
		    "   and batsts = '%s' "
		    " %s "
		    "   and wave_prc_flg = '%ld' "
		    " %s "
		    " and lblseq='%s' %s ",
		    work_schbat,
		    BATSTS_ALLOC_IN_PROC,
		    pricod_buf,
		    BOOLEAN_FALSE,
		    temp_clause,
		    lblseqstrategy ? lblseqstrategy : "",
		    tmpbuf);
	if ((errcode != eOK && errcode != eDB_UNIQUE_CONS_VIO) ||
	    (errcode == eDB_UNIQUE_CONS_VIO &&
	    (!schbat_i || misTrimLen(schbat_i,SCHBAT_LEN) == 0))) 
	{
	    /* Free up our working list */
	    lastptr = NULL;
	    sFreeWorkingList(Top);
	    return(srvResults(errcode, NULL));
	}
    }
    else 
    {
	row = sqlGetRow(res);
	/*
	 * Make sure the batch looks like it's inprocess, and shows that we
	 * have allocated something. (may currently be in a PLAN state).
	 */

        /* See if we need to include a priority code in the pckbat record. */
        memset(pricod_buf, 0, sizeof(pricod_buf));
        if ((pricod_i && strlen(pricod_i)))
            sprintf(pricod_buf, "      pricod = '%s',", pricod_i);

	if (!sqlIsNull(res,row,"cmpdte") || 
            strcmp(sqlGetString(res, row, "batsts"), BATSTS_ALOC) ||
            strcmp(sqlGetString(res, row, "batsts"), BATSTS_ALLOC_IN_PROC)) 
	{
	    sprintf(sqlbuffer,
		    "update pckbat "
		    "   set cmpdte = '', "
                    " %s "
		    "       batsts = '%s' "
		    " where schbat = '%s' ",
                    pricod_buf,
		    BATSTS_ALLOC_IN_PROC,
		    work_schbat);
	    errcode = sqlExecStr(sqlbuffer,NULL);
	    if (errcode != eOK) 
	    {
		sqlFreeResults(res);
		/* Free up our working list */
		sFreeWorkingList(Top);
		return(srvResults(errcode, NULL));
	    }
	}
	sqlFreeResults(res);
    }

    /*
     * And now, let's set the schbat for allocate inventory.
     * By doing this, we will prevent allocate inventory from making
     * repeated calls to create pick batch, which is a performace save.
     * Since all subsequenct calls to allocate inventory will get called for
     * the same schedule batch, and since, from above, we know the schbat is
     * created, we will tell allocate inventory to not even try creating the
     * schbat.
     */

    misTrc(T_FLOW, "Schedule batch for pick group is %s", work_schbat);
    trnSetSchbat(work_schbat);

/******** I don't think the following is needed...the consolidated pick
 ******** logic gets handled below where we build the call for allocate  
    for (ptr = Top; ptr; ptr = ptr->next) 
    {
	if ((misCiStrncmp(consby_in,"ship_id",7)==0) ||
	    (misCiStrncmp(consby_in,"stop_id",7)==0) ||
	    (misCiStrncmp(consby_in,"rtcust",6)==0) ||
	    (misCiStrncmp(consby_in,"stcust",6)==0))
	{
	    memset(work_concod,0,sizeof(work_concod));
	    errcode = appNextNum(NUMCOD_CONCOD, work_concod);
	    if (errcode != eOK) 
	    {
		sFreeWorkingList(Top);
		return(srvResults(errcode, NULL));
	    }
	}
	strncpy(ptr->concod,work_concod,CONCOD_LEN);
    }
************************************************************************/

    memset(saved_ship_id, 0, sizeof(saved_ship_id));
    AllPtrs=NULL;
    row=NULL;
    for (ptr = Top; ptr; ptr = ptr->next) 
    {
	++loop_cnt;

	/* First, count up the number of segments we will have for this call */
	num_segments = 0;
	for (tmpptr = ptr; tmpptr; tmpptr = tmpptr->next) 
	{
	    if (tmpptr->pckqty > 0)
	    {
		WriteXDock = TRUE;
		if ((PolXDockInstalled) &&
		    (strcmp(pcktyp_i, ALLOCATE_SHIPMENT_ONLY)) &&
		    (tmpptr->xdkflg == BOOLEAN_TRUE))
		{
		    /*
		    ** A XDock work record should only exist if some 
		    ** of the pckqty couldn't be cross-docked so
		    ** go ahead and allocate it
		    */
		    sprintf(sqlbuffer,
			    "select pckqty "
			    "  from xdkwrk "
			    " where ship_line_id = '%s' ",
			    tmpptr->ship_line_id);

		    errcode = sqlExecStr(sqlbuffer, &res);
		    if (errcode == eOK)
		    {
			WriteXDock = FALSE;
			row = sqlGetRow(res);
			misTrc(T_FLOW, "Cross-dock work already exists for "
				"pckqty %ld - Wanted to add pckqty %ld. ",
			        sqlGetLong(res, row, "pckqty"), tmpptr->pckqty);

		        /* And zero it out so we don't run across it again */
		        tmpptr->pckqty = 0;
		        tmpptr->xdk_inserted = BOOLEAN_TRUE;
		    }
		    sqlFreeResults(res);
		}
		else
		{
		    WriteXDock = FALSE;
		}

		if (WriteXDock)
		{
		    memset(xdkref, 0, sizeof(xdkref));
		    if (errcode = appNextNum(NUMCOD_XDKREF, xdkref) != eOK)
			return (srvResults(errcode, NULL));

		    sprintf(sqlbuffer,
			    "insert into xdkwrk "
			    "(xdkref, xdktyp, dstare, prtnum, adddte,"
			    " prt_client_id, invsts_prg, pckqty, xdkqty, "
			    " alcqty, lotnum, orgcod, revlvl, untcas, "
			    " untpak, untpal, ship_id, ship_line_id, "
			    " client_id, ordnum, ordlin, ordsln, "
			    " dstloc, schbat, stcust, rpqflg, splflg) "
			    " values "
			    " ('%s', '%s', '%s', '%s', "
			    "  to_date('%s', 'YYYYMMDDHH24MISS'), "
			    "  '%s', '%s', '%ld', '%ld', '%ld', "
			    "  '%s', '%s', '%s', '%ld', '%ld', "
			    "  '%ld', '%s', '%s', '%s', '%s', "
			    "  '%s', '%s', '%s', '%s', '%s', '%ld', '%ld') ",
			    xdkref, XDKTYP_SHIPMENT, 
			    tmpptr->dstare, tmpptr->prtnum, tmpptr->entdte,
			    tmpptr->prt_client_id, tmpptr->invsts_prg,
			    tmpptr->pckqty, 0, 0, tmpptr->lotnum,
			    tmpptr->orgcod, tmpptr->revlvl, tmpptr->alccas,
			    tmpptr->alcpak, tmpptr->alcpal, tmpptr->ship_id,
			    tmpptr->ship_line_id, tmpptr->client_id,
			    tmpptr->ordnum, tmpptr->ordlin, tmpptr->ordsln,
			    tmpptr->dstloc, work_schbat, tmpptr->stcust,
			    tmpptr->rpqflg, tmpptr->splflg);

		    errcode = sqlExecStr(sqlbuffer, NULL);
		    if (errcode != eOK && errcode != eDB_UNIQUE_CONS_VIO)
			return (srvResults(errcode, NULL));

		    /* And zero it out so we don't run across it again */
		    tmpptr->pckqty = 0;
		    tmpptr->xdk_inserted = BOOLEAN_TRUE;

		    /*
		    ** Indicate that at least one cross dock entry was created
		    */
		    xdk_created = TRUE;
		}

		if (((misCiStrncmp(consby_in,"ship_id",7)==0 &&
		      strncmp(tmpptr->ship_id,ptr->ship_id, SHIP_ID_LEN)==0) ||
		     (misCiStrncmp(consby_in,"stop_id",7)==0 &&
		      strncmp(tmpptr->stop_id,ptr->stop_id, STOP_ID_LEN)==0) ||
		     (misCiStrncmp(consby_in,"stcust",6)==0 &&
		      strncmp(tmpptr->stcust,ptr->stcust,STCUST_LEN)==0) ||
		     (misCiStrncmp(consby_in,"all",3) == 0) ||
		     (misCiStrncmp(consby_in,"rtcust",6)==0 &&
		      strncmp(tmpptr->rtcust,ptr->rtcust,RTCUST_LEN)==0)) &&
		    strncmp(tmpptr->prtnum,ptr->prtnum,PRTNUM_LEN)==0 &&
		    strncmp(tmpptr->prt_client_id,ptr->prt_client_id,
			    CLIENT_ID_LEN)==0 &&
		    strncmp(tmpptr->orgcod,ptr->orgcod,ORGCOD_LEN)==0 &&
		    strncmp(tmpptr->revlvl,ptr->revlvl,REVLVL_LEN)==0 &&
		    strncmp(tmpptr->lotnum,ptr->lotnum,LOTNUM_LEN)==0 &&
		    strncmp(tmpptr->invsts_prg,
                            ptr->invsts_prg,
                            INVSTS_PRG_LEN)==0 &&
		    tmpptr->splflg == ptr->splflg &&
		    tmpptr->alccas == ptr->alccas &&
		    tmpptr->alcpak == ptr->alcpak &&
		    tmpptr->alcpal == ptr->alcpal &&
                    tmpptr->frsflg == ptr->frsflg &&
                    tmpptr->non_alc_flg == ptr->non_alc_flg &&
                    tmpptr->min_shelf_hrs == ptr->min_shelf_hrs &&
                    strncmp(tmpptr->frsdte,ptr->frsdte,MOCA_STD_DATE_LEN)==0)
			num_segments++;
	    }

	    if (ptr->partial_case == TRUE)
		break;

 	    if (ptr->splflg == BOOLEAN_FALSE)
		break;    /*  If the ptr is "do not split cases", allocate
			  **  that line by it's self.
			  */
            if (ptr->non_alc_flg == BOOLEAN_TRUE)
                break;    /*  If the ptr is for a non-allocation line,
                          **  we're not allocating it.
                          */
	}
	if (num_segments == 0)	continue;
	
	/* Now, make space for everybody going in to trnAllocate */
	all_ship_id = (char *)calloc(1, (SHIP_ID_LEN+2)*num_segments);
	all_ship_line_id = (char *)calloc(1, (SHIP_LINE_ID_LEN+2)*num_segments);
	all_schbat = (char *)calloc(1, (SCHBAT_LEN+2)*num_segments);
	all_stcust = (char *)calloc(1, (ADRNUM_LEN+2)*num_segments);
	all_rtcust = (char *)calloc(1, (ADRNUM_LEN+2)*num_segments);
	all_client_id = (char *)calloc(1, (CLIENT_ID_LEN+2)*num_segments);
	all_ordnum = (char *)calloc(1, (ORDNUM_LEN+2)*num_segments);
	all_ordlin = (char *)calloc(1, (ORDLIN_LEN+2)*num_segments);
	all_ordsln = (char *)calloc(1, (ORDSLN_LEN+2)*num_segments);
	all_concod = (char *)calloc(1, (CONCOD_LEN+2)*num_segments);
	all_segqty = (char *)calloc(1, (6+2)*num_segments);
	all_dstare = (char *)calloc(1, (ARECOD_LEN+2)*num_segments);
	all_dstloc = (char *)calloc(1, (STOLOC_LEN+2)*num_segments);
	if (all_ship_id       == NULL ||
	    all_ship_line_id  == NULL ||
	    all_stcust        == NULL ||
	    all_rtcust        == NULL ||
	    all_client_id     == NULL ||
	    all_ordnum        == NULL ||
	    all_ordlin        == NULL ||
	    all_ordsln        == NULL ||
	    all_concod        == NULL ||
	    all_segqty        == NULL ||
	    all_dstare        == NULL ||
	    all_dstloc        == NULL )
	{
	    /* Free up our working list */
	    sFreeWorkingList(Top);
	    if (all_ship_id) free (all_ship_id);
	    if (all_ship_line_id) free (all_ship_line_id);
	    if (all_schbat) free (all_schbat);
	    if (all_stcust) free (all_stcust);
	    if (all_rtcust) free (all_rtcust);
	    if (all_client_id) free (all_client_id);
	    if (all_ordnum) free (all_ordnum);
	    if (all_ordlin) free (all_ordlin);
	    if (all_ordsln) free (all_ordsln);
	    if (all_concod) free (all_concod);
	    if (all_segqty) free (all_segqty);
	    if (all_dstare) free (all_dstare);
	    if (all_dstloc) free (all_dstloc);
	    return(srvResults(eNO_MEMORY, NULL));
	}
	all_pckqty = 0;

	memset(work_concod, 0, sizeof(work_concod));
	/* Now, loop through and build the paramters with spaces in between */
	for (tmpptr = ptr; tmpptr; tmpptr = tmpptr->next) 
	{ 
		if (tmpptr->pckqty > 0 &&
		((misCiStrncmp(consby_in,"ship_id",7)==0 &&
		  strncmp(tmpptr->ship_id, ptr->ship_id, SHIP_ID_LEN)==0) ||
		 (misCiStrncmp(consby_in,"stop_id",7)==0 &&
		  strncmp(tmpptr->stop_id, ptr->stop_id, STOP_ID_LEN)==0) ||
		 (misCiStrncmp(consby_in,"all",3)==0) ||
		 (misCiStrncmp(consby_in,"stcust",6)==0 &&
		  strncmp(tmpptr->stcust,ptr->stcust,STCUST_LEN)==0) ||
		 (misCiStrncmp(consby_in,"rtcust",6)==0 &&
		  strncmp(tmpptr->rtcust,ptr->rtcust,RTCUST_LEN)==0)) &&
		strncmp(tmpptr->prtnum,ptr->prtnum,PRTNUM_LEN)==0 &&
		strncmp(tmpptr->prt_client_id,ptr->prt_client_id,
						CLIENT_ID_LEN)==0 &&
		strncmp(tmpptr->orgcod,ptr->orgcod,ORGCOD_LEN)==0 &&
		strncmp(tmpptr->revlvl,ptr->revlvl,REVLVL_LEN)==0 &&
		strncmp(tmpptr->lotnum,ptr->lotnum,LOTNUM_LEN)==0 &&
		strncmp(tmpptr->invsts_prg,
                        ptr->invsts_prg,
                        INVSTS_PRG_LEN)==0 &&
		tmpptr->splflg == ptr->splflg &&
		tmpptr->alccas == ptr->alccas &&
		tmpptr->alcpak == ptr->alcpak &&
		tmpptr->alcpal == ptr->alcpal )
	    {
		if (ptr != tmpptr && tmpptr->partial_case)
		    continue;

		if (strlen(work_concod) == 0)
		    appNextNum(NUMCOD_CONCOD, work_concod);

		strcat (all_schbat, work_schbat);
		strcat (all_schbat, CONCAT_CHAR);

		strcat (all_ship_id, tmpptr->ship_id);
		strcat (all_ship_id, CONCAT_CHAR);
		strcat (all_ship_line_id, tmpptr->ship_line_id);
		strcat (all_ship_line_id, CONCAT_CHAR);
		if (strlen(tmpptr->stcust))
		{
		    strcat (all_stcust, tmpptr->stcust);
		    strcat (all_stcust, CONCAT_CHAR);
		}
		if (strlen(tmpptr->rtcust))
		{
		    strcat (all_rtcust, tmpptr->rtcust);
		    strcat (all_rtcust, CONCAT_CHAR);
		}
		strcat (all_client_id, tmpptr->client_id);
		strcat (all_client_id, CONCAT_CHAR);
		strcat (all_ordnum, tmpptr->ordnum);
		strcat (all_ordnum, CONCAT_CHAR);
		strcat (all_ordlin, tmpptr->ordlin);
		strcat (all_ordlin, CONCAT_CHAR);
		strcat (all_ordsln, tmpptr->ordsln);
		strcat (all_ordsln, CONCAT_CHAR);
		strcat (all_concod, work_concod);
		strcat (all_concod, CONCAT_CHAR);
		sprintf (temp_clause, "%6ld", tmpptr->pckqty);
		strcat (all_segqty, temp_clause);
		strcat (all_segqty, CONCAT_CHAR);
		if (strlen(tmpptr->dstare))
		{
		    strcat (all_dstare, tmpptr->dstare);
		    strcat (all_dstare, CONCAT_CHAR);
		}
                else
                {
                    /*I used a blank space to allow for blank entries here
                      and in dstloc. Cant use the empty string because
                      strtoc will ignore those in trnAllocInv */
                    strcat (all_dstare, " ");
                    strcat (all_dstare, CONCAT_CHAR);
                }
                if (strlen(tmpptr->dstloc))
                {
                    strcat (all_dstloc, tmpptr->dstloc);
                    strcat (all_dstloc, CONCAT_CHAR);
                }
                else
                {
                    strcat (all_dstloc, " ");
                    strcat (all_dstloc, CONCAT_CHAR);
                }

		/* Add to the total pick quantity */
		all_pckqty += tmpptr->pckqty;

		/* And zero it out so we don't run across it again */
		tmpptr->pckqty = 0;

	    }

	    if (ptr->partial_case == TRUE)
		break;

	    /*  If the split case flag is set to NO (do not break cases just
	    **  to complete a line), allocate that line by it's self.
	    */
	    if (ptr->splflg == BOOLEAN_FALSE)
		break;

            /*  If the non allocatable flag is set to True,
            **  don't allocate it.
            */
            if (ptr->non_alc_flg == BOOLEAN_TRUE)
                break;
	}

	/* Let's naw off our last comma */
	misTrim(all_segqty);
	misTrim(all_concod);
	misTrim(all_ordsln);
	misTrim(all_ordlin);
	misTrim(all_client_id);
	misTrim(all_ordnum);
	misTrim(all_stcust);
	misTrim(all_rtcust);
	misTrim(all_ship_id);
	misTrim(all_ship_line_id);
	misTrim(all_schbat);
	if (strlen(all_segqty) > 0)
	    all_segqty[strlen(all_segqty)-1]='\0';
	if (strlen(all_concod) > 0)
	    all_concod[strlen(all_concod)-1]='\0';
	if (strlen(all_ordsln) > 0)
	    all_ordsln[strlen(all_ordsln)-1]='\0';
	if (strlen(all_ordlin) > 0)
	    all_ordlin[strlen(all_ordlin)-1]='\0';
	if (strlen(all_client_id) > 0)
	    all_client_id[strlen(all_client_id) - 1]='\0';
	if (strlen(all_ordnum) > 0)
	    all_ordnum[strlen(all_ordnum)-1]='\0';
	if (strlen(all_stcust) > 0)
	    all_stcust[strlen(all_stcust)-1]='\0';
	if (strlen(all_rtcust) > 0)
	    all_rtcust[strlen(all_rtcust)-1]='\0';
	if (strlen(all_ship_id) > 0)
	    all_ship_id[strlen(all_ship_id)-1]='\0';
	if (strlen(all_ship_line_id) > 0)
	    all_ship_line_id[strlen(all_ship_line_id)-1]='\0';
	if (strlen(all_schbat) > 0)
	    all_schbat[strlen(all_schbat)-1]='\0';
	if (strlen(all_dstare) > 0)
	    all_dstare[strlen(all_dstare)-1]='\0';
        if (strlen(all_dstloc) > 0)
            all_dstloc[strlen(all_dstloc)-1]='\0';

	/* don't allocate if a xdkwrk was inserted */
	if (ptr->xdk_inserted == BOOLEAN_TRUE)
	    continue;

        /* If we have a non-allocatable line, we don't need to 
         * do anything at this point. 
         */

        if (ptr->non_alc_flg == BOOLEAN_TRUE)
        {
            misTrc(T_FLOW, "Skipping allocation - this is a non-allocatable "
                           "line!");
            ret_status = eOK;
            add_to_failure = 0;

            continue;
        }
	if (strncmp(saved_ship_id, ptr->ship_id, SHIP_ID_LEN) != 0)
	{
	    if (comflg == BOOLEAN_TRUE)  sqlCommit();
	    strcpy(saved_ship_id, ptr->ship_id);
	}

	/*
	 * Build up a suffix for the status file name consisting of 
	 * the pick group, part number and sequence number, in case
	 * the part number gets allocated more than once in the pick
	 * group.
	 */

	memset(tmpbuf, 0, sizeof(tmpbuf));
	sprintf(tmpbuf, "-%s-%s-%ld", pckgr1_i, ptr->prtnum, loop_cnt);

	add_to_failure = 0;
        
	/* Change to srvInitiateCommandFormat to avoid memory leaks. */
	ret_status = srvInitiateCommandFormat(&CurPtr,
		"allocate inventory "
		"   where pcktyp='%s' "
		"     and prtnum='%s' "
		"     and prt_client_id='%s' "
		"     and pckqty='%ld'  and orgcod='%s' "
		"     and revlvl='%s' and lotnum='%s' "
		"     and invsts_prg='%s' "
		"     and schbat='%s' and ship_id='%s'  "
		"     and ship_line_id='%s' "
		"     and client_id='%s' "
		"     and ordnum='%s'  "
		"     and stcust='%s' "
		"     and rtcust='%s'  "
		"     and ordlin='%s' and ordsln='%s' "
		"     and concod='%s' and segqty='%s' "
		"     and dstare='%s' "
		"     and pcksts='%s' and splflg='%ld' "
		"     and dstloc='%s' and untcas='%ld' "
		"     and untpak='%ld'  and untpal='%ld' "
                "     and pipcod = '%s' "
                "     and min_shelf_hrs = %ld "
                "     and frsflg = %ld "
                "     and frsdte = '%s' "
		"     and trace_suffix = '%s' "
		"     and skip_invlkp = '%ld'",
		pcktyp_i?pcktyp_i:"",
		ptr->prtnum,
		ptr->prt_client_id,
		all_pckqty,ptr->orgcod,ptr->revlvl,ptr->lotnum,
		ptr->invsts_prg,all_schbat,all_ship_id,
		all_ship_line_id,
		all_client_id,
		all_ordnum,
		all_stcust,all_rtcust,all_ordlin,all_ordsln,all_concod,
		all_segqty,all_dstare,
		pcksts,ptr->splflg,
		all_dstloc,
		ptr->alccas, ptr->alcpak, ptr->alcpal,
                pipcod,
                ptr->min_shelf_hrs,
                ptr->frsflg,
                ptr->frsdte,
		tmpbuf,
		(PolPIProcessing ? sWillAllocFail(FailedAllocHead, ptr) :
                                 BOOLEAN_FALSE));

	if (ret_status == eDB_NO_ROWS_AFFECTED ||
	    ret_status == eINT_NO_INVENTORY)
	    add_to_failure = 1;
	
	if (ret_status == eOK || ret_status == eINT_CANT_OVRALLOC)
	{
	    mocaDataRes *ares;
	    mocaDataRow *arow;
	    long pipFound = 0;
	    long pipPos;

	    if (ret_status == eOK)
	    {

	    	ares = srvGetResults(CurPtr);
	    	arow = sqlGetRow(ares);

	    	pipPos = sqlFindColumn(ares, "pipflg");

	    	if (pipPos > 0)
	    	{
			for (arow = sqlGetRow(ares); arow && !pipFound; 
			     arow = sqlGetNextRow(arow))
			{
		    	if (!sqlIsNullByPos(ares, arow, pipPos) &&
			sqlGetLongByPos(ares, arow, pipPos) > 0)
			pipFound = 1;
			}
	    	}
	    	if (pipFound || sqlGetNumRows(ares) == 0)
			add_to_failure = 1;
	     }

	     else
		{
                misTrc(T_FLOW, "*** Forcing OverAllocate Error Back to eOK (temporary workaround) ***");
		ret_status = eOK;
		}
	}

	if (add_to_failure)
	{
	    /* We failed to allocate - stash this off in our
	     * failed allocation list so that we can more intelligently
	     * call allocate inventory
	     */
	    ret_status = sAddToFailureList(&FailedAllocHead,
					   ptr->prtnum,
					   ptr->prt_client_id,
					   ptr->orgcod,
					   ptr->revlvl,
					   ptr->lotnum,
					   ptr->invsts_prg,
					   ptr->dstare,
					   ptr->dstloc,
					   ptr->splflg,
					   ptr->alccas,
					   ptr->alcpak, 
					   ptr->alcpal,
                                           ptr->frsflg,
                                           ptr->non_alc_flg,
                                           ptr->min_shelf_hrs,
                                           ptr->frsdte);

	}


	if (all_schbat) free (all_schbat);
	if (all_ship_id) free (all_ship_id);
	if (all_ship_line_id) free (all_ship_line_id);
	if (all_stcust) free (all_stcust);
	if (all_rtcust) free (all_rtcust);
	if (all_client_id) free (all_client_id);
	if (all_ordnum) free (all_ordnum);
	if (all_ordlin) free (all_ordlin);
	if (all_ordsln) free (all_ordsln);
	if (all_concod) free (all_concod);
	if (all_segqty) free (all_segqty);
	if (all_dstare) free (all_dstare);
	if (all_dstloc) free (all_dstloc);

	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	{
	    /* Free up our working list */
	    sFreeWorkingList(Top);
	    srvFreeMemory(SRVRET_STRUCT,CurPtr);
	    if (AllPtrs)
		srvFreeMemory(SRVRET_STRUCT, AllPtrs);
	    return(srvResults(ret_status, NULL));
	}
 
	if (ret_status == eOK) 
	{
	    if (CurPtr->rows == 0) 
	    {
		srvFreeMemory(SRVRET_STRUCT, CurPtr);
		CurPtr = NULL;
	    }
	    else
	    {
		if (AllPtrs)
		{
		    ret_status = srvCombineResults (&AllPtrs,&CurPtr);
		    if (ret_status != eOK)
		    {
			srvFreeMemory(SRVRET_STRUCT, AllPtrs);
			srvFreeMemory(SRVRET_STRUCT, CurPtr);
			return(srvResults(ret_status, NULL));
		    }
		}
		else
		    AllPtrs = CurPtr;
	    }
	}
        /* PR 30609. LN - 8/20/2003
         * This next line had been commented out at one time when it was
         * causing issues in Sql Server sites where dirty reads were enabled
         * because the pick release manager would then start processing
         * these lines even though the schedule batch was not finished 
         * allocating.
         * These problems were fixed under PR 29254. As a result,
         * we're going to enable this incremental commit to speed up
         * processing.
         */
        
	if (comflg == BOOLEAN_TRUE)  sqlCommit();   
    }

    sFreeFailedAllocList(FailedAllocHead);

    /* Set the batsts to "allocation complete" */
    sprintf(sqlbuffer,
            "update pckbat "
            "   set batsts = '%s' "
            " where schbat = '%s' ",
            BATSTS_ALOC,
            work_schbat);
    errcode = sqlExecStr(sqlbuffer,NULL);
    if (errcode != eOK)
    {
        /* Not the biggest crime in the world, let the error go. */
        misTrc(T_FLOW, "Problem updating the pckbat.  Look into the error!");
    }

    /* Deal with the preinventory picks right here */
    if (work_schbat && misTrimLen(work_schbat, SCHBAT_LEN) &&
        !running_pip_in_parallel)
    {

        /* This is the part that we swing back and select all the pre-inventory
           pick work entries for the schedule batch and create replenishments
           to pull the product forward. */
    
        ret_status = srvInitiateInlineFormat(NULL,
		"process pip for schedule batch"
                " where schbat = '%s' "
                "   and comflg = %ld "
                "   and pcktyp = '%s' "
                "   and pip_pcksts = '%s' ",
		work_schbat,
                comflg_i,
                pcktyp_i?pcktyp_i:"",
                pcksts);

        /* Bomb out if this returns an error */
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	{
	    /* Free up our working list */
	    sFreeWorkingList(Top);
	    if (AllPtrs)
		srvFreeMemory(SRVRET_STRUCT, AllPtrs);
	    return(srvResults(ret_status, NULL));
        }
    }

    if (AllPtrs == NULL)
    {
	/* see above comment regarding replenishment manager... */
	if (strcmp(pcktyp_i, ALLOCATE_PICKNREPLEN) == 0 ||
	    strcmp(pcktyp_i, ALLOCATE_PICKNREPLENNSHIP) == 0 ||
	    xdk_created == TRUE)
	    {
                CurPtr = srvResults(eOK,
                                    "schbat", COMTYP_CHAR, SCHBAT_LEN, 
                                        work_schbat,
                                    NULL);
	    }
	else
	{
		CurPtr = srvResults(eDB_NO_ROWS_AFFECTED, NULL);	
	}	
    }
    else 
	CurPtr=AllPtrs;

    /* Free up our working list */
    sFreeWorkingList(Top);

    /*
     * Now, make sure we clear the schbat for the next
     * call to allocate pick group.  This will also prevent garbage from 
     * being in the field in case allocate inventory gets called outside of
     * this program. (Like for move requests)
     */

    trnClearSchbat();


    return(CurPtr);
}
