static char    *rcsid = "$Id: varpckProcessOrderReservation.c 103521 2004-04-06 12:26:16Z mzais $";
/*#START***********************************************************************
 *  RedPrairie Corporation
 *  Copyright 2003
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "pckRSVlib.h"

typedef struct _resv_set
{
    char ordnum[ORDNUM_LEN+1];
    char ordlin[ORDLIN_LEN+1];
    char ordsln[ORDSLN_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    moca_bool_t married;

    long rsv_demand;
    long rsv_granted;
    struct _resv_set *next;
} RESV_SET;

typedef struct _resv_group 
{
    char stcust[STCUST_LEN+1];
    
    long rsv_demand;
    long rsv_granted;
    long addqty;

    RESV_SET *orders;
    long order_count;
    
    struct _resv_group *next;
} RESV_GROUP;


typedef struct _resv_main
{
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char invsts_prg[INVSTS_PRG_LEN+1];
    long rsvpri;
    
    long rsv_demand;   /* The summarized amount needed */
    long rsv_granted;  /* The amount granted */
    
    long addqty;
    long min_rsvqty;

    long processed;

    RESV_GROUP *groups;
    long group_count;

    struct _resv_main *next;
} RESV_MAIN;


typedef struct _invsts_list 
{
    char invsts[INVSTS_LEN+1];
    struct _invsts_list *next;
} INVSTS_LIST;

typedef struct _invsts_prg
{
    char invsts_prg[INVSTS_PRG_LEN+1];

    INVSTS_LIST *sts;
    struct _invsts_prg *next;
} INVSTS_PRG;

static INVSTS_PRG *ProgressionList = NULL;

typedef struct _inventory_list
{
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char invsts[INVSTS_LEN+1];
    long untqty;

    long rsvqty;
    long inpqty;
    long pckqty;
    long stgqty;
    long wkoqty;

    struct _inventory_list *next;
} INVENTORY_LIST;

typedef struct _ord_list
{
    char ordnum[ORDNUM_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    long married;
    struct _ord_list *next;
} ORD_LIST;

/*------------------------------------------------------------------------
 *      I N V E N T O R Y   F U N C T I O N S  --  S T A R T
 *------------------------------------------------------------------------
 * The following set of functions are used to reconcile how much 
 * available inventory we have to use for reservations.
 *
 *  sLoadInventory - loads initial list of inventory
 *  sReconcileShipmentInventory - handles inventory for shipments
 *  sReconcileReservedInventory - handles already reserved inventory
 *  sReconcileWorkOrderInventory - handles inventory for work orders
 *------------------------------------------------------------------------
 */
static void sConsumeInventory(long *ip_qty_to_increment,
			      long *qty_needed,
			      long *avail_qty)
{
    if (*avail_qty < *qty_needed)
    {
	*ip_qty_to_increment += (*avail_qty);
	*qty_needed -= (*avail_qty);
	*avail_qty = 0;
    }
    else
    {
	*ip_qty_to_increment += (*qty_needed);
	*avail_qty -= (*qty_needed);
	*qty_needed = 0;
    }
    return;
}

static void sFreeProgressionList()
{
    INVSTS_LIST *s, *slast;
    INVSTS_PRG *p, *plast;

    plast = NULL;
    slast = NULL;

    for (p = ProgressionList; p; plast = p, p = p->next)
    {
	if (plast)
	{
	    slast = NULL;
	    for (s = plast->sts; s; slast = s, s = s->next)
	    {
		if (slast)
		{
		    free(slast);
		    slast = NULL;
		}
	    }
	    if (slast)
		free(slast);

	    free(plast);
	}
    }
    if (plast)
    {
	slast = NULL;
	for (s = plast->sts; s; slast = s, s = s->next)
	{
	    if (slast)
	    {
		free(slast);
		slast = NULL;
	    }
	}
	if (slast)
	    free(slast);
	
	free(plast);
    }
    ProgressionList = NULL;

    return;
}


static INVSTS_PRG *sFindProgression(char *invsts_prg)
{
    INVSTS_PRG *prg_p;
    INVSTS_LIST *sts_p;
    char buffer[1000];
    long ret_status;
    
    mocaDataRow *row;
    mocaDataRes *res;

    static long progression_registered = -1;

    for (prg_p = ProgressionList; prg_p; prg_p = prg_p->next)
    {
	if (misCiStrncmp(prg_p->invsts_prg, invsts_prg, INVSTS_PRG_LEN) == 0)
	    return(prg_p);
    }
    
    sprintf(buffer,
	    "select invsts from prgmst where invsts_prg = '%s'",
	    invsts_prg);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return(NULL);
    }
    
    prg_p = (INVSTS_PRG *) calloc(1, sizeof(INVSTS_PRG));
    strcpy(prg_p->invsts_prg, invsts_prg);
    
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	sts_p = (INVSTS_LIST *) calloc (1, sizeof(INVSTS_LIST));
	strncpy(sts_p->invsts,
		sqlGetString(res, row, "invsts"), INVSTS_LEN);

	sts_p->next = prg_p->sts;
	prg_p->sts = sts_p;
    }
    
    sqlFreeResults(res);
    prg_p->next = ProgressionList;
    ProgressionList = prg_p;
    
    if (progression_registered < 0)
    {
	progression_registered = 1;
        misFlagCachedMemory((OSFPTR) sFreeProgressionList, NULL);
    }

    return(prg_p);
}


static moca_bool_t sProgressionIsMatch(char *invsts_prg,
				       char *invsts)
{
    INVSTS_PRG *prg;
    INVSTS_LIST *sts;

    prg = sFindProgression(invsts_prg);

    if (!prg)
    {
	pckrsv_WriteTrc("   FAILED to load status progression: %s",
			invsts_prg);
	return(MOCA_FALSE);
    }
	
    for (sts = prg->sts; sts; sts = sts->next)
    {
	if (misCiStrncmp(sts->invsts, invsts, INVSTS_LEN) == 0)
	    return(MOCA_TRUE);
    }
    return(MOCA_FALSE);
}

static void sReconcileInventory(INVENTORY_LIST *InvList,
				char *invsts_prg,
				long rsvqty_i,
				long inpqty_i,
				long pckqty_i,
				long stgqty_i,
				long wkoqty_i)
{
    INVENTORY_LIST *ip;

    long rsvqty;
    long inpqty;
    long pckqty;
    long stgqty;
    long wkoqty;

    rsvqty = (rsvqty_i < 0 ? 0 : rsvqty_i);
    inpqty = (inpqty_i < 0 ? 0 : inpqty_i);
    pckqty = (pckqty_i < 0 ? 0 : pckqty_i);
    stgqty = (stgqty_i < 0 ? 0 : stgqty_i);
    wkoqty = (wkoqty_i < 0 ? 0 : wkoqty_i);
    

    for (ip = InvList; ip; ip = ip->next)
    {
	long total_used;
	long total_need;
	long avail_qty;

	total_used = ip->rsvqty + ip->inpqty + 
	    ip->pckqty + ip->stgqty + ip->wkoqty;

	total_need = rsvqty + inpqty + 
	    pckqty + stgqty + wkoqty;
       
	if (total_need == 0)
	    return;

	avail_qty = ip->untqty - total_used;

	if (avail_qty > 0)
	{
	    if (sProgressionIsMatch(invsts_prg, ip->invsts))
	    {
		if (rsvqty > 0)
		{
		    sConsumeInventory(&(ip->rsvqty),
				      &rsvqty,
				      &avail_qty);
		}
		
		if (avail_qty <= 0)
		    continue;

		if (inpqty > 0)
		{
		    sConsumeInventory(&(ip->inpqty),
				      &inpqty,
				      &avail_qty);
		}

		if (avail_qty <= 0)
		    continue;

		if (pckqty > 0)
		{
		    sConsumeInventory(&(ip->pckqty),
				      &pckqty,
				      &avail_qty);
		}

		if (avail_qty <= 0)
		    continue;

		if (stgqty > 0)
		{
		    sConsumeInventory(&(ip->stgqty),
				      &stgqty,
				      &avail_qty);
		}

		if (avail_qty <= 0)
		    continue;


		if (wkoqty > 0)
		{
		    sConsumeInventory(&(ip->wkoqty),
				      &wkoqty,
				      &avail_qty);
		}

		if (avail_qty <= 0)
		    continue;

		if (stgqty > 0)
		{
		    sConsumeInventory(&(ip->stgqty),
				      &stgqty,
				      &avail_qty);
		}

	    }
 	}
    }
    return;
} 

/*
 * sReconcileReservedInventory
 *
 * Reconcile outstanding reservations against available inventory
 *
 */
static long sReconcileReservedInventory(char *prtnum,
					char *prt_client_id,
					INVENTORY_LIST *InvList)
{

    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;

    sprintf(buffer,
	    "get reserved quantity for part reservation"
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' ",
	    prtnum, prt_client_id);

    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	pckrsv_WriteTrc("Error %d getting outstanding reservations",
			ret_status);
	return(ret_status);
    }

    res = srvGetResults(CmdRes);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	sReconcileInventory(InvList,
			    sqlGetString(res, row, "invsts_prg"),
			    sqlGetLong(res, row, "rsvqty"),
			    0,  /* inpqty */
			    0,  /* pckqty */
			    0,  /* stgqty */
			    0); /* wkoqty */

    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return(eOK);
}
/*
 * sReconcileWorkOrderInventory
 *
 * Reconcile outstanding work orders against available inventory
 *
 */
static long sReconcileWorkOrderInventory(char *prtnum,
					 char *prt_client_id,
					 INVENTORY_LIST *InvList)
{

    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;

    sprintf(buffer,
	    "get work order quantity for part reservation"
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' ",
	    prtnum, prt_client_id);

    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	pckrsv_WriteTrc("Error %d getting outstanding reservations",
			ret_status);
	return(ret_status);
    }

    res = srvGetResults(CmdRes);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	sReconcileInventory(InvList,
			    sqlGetString(res, row, "invsts_prg"),
			    0,  /* rsvqty */
			    0,  /* inpqty */
			    0,  /* pckqty */
			    0,  /* stgqty */
			    sqlGetLong(res, row, "wkoqty")); /* wkoqty */

    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return(eOK);
}

/*
 * sReconcileShipmentInventory
 *
 * Reconcile outstanding reservations against available inventory
 *
 */
static long sReconcileShipmentInventory(char *prtnum,
					char *prt_client_id,
					INVENTORY_LIST *InvList)
{

    char buffer[2000];
    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;

    sprintf(buffer,
	    "get shipment quantities for part reservation"
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s' ",
	    prtnum, prt_client_id);

    ret_status = srvInitiateInline(buffer, &CmdRes);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	pckrsv_WriteTrc("Error %d getting outstanding shipment",
			ret_status);
	return(ret_status);
    }

    res = srvGetResults(CmdRes);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	sReconcileInventory(InvList,
			    sqlGetString(res, row, "invsts_prg"),
			    0,
			    sqlGetLong(res, row, "inpqty"),  /* inpqty */
			    sqlGetLong(res, row, "pckqty"),  /* pckqty */
			    sqlGetLong(res, row, "stgqty"),  /* stgqty */
			    0); /* wkoqty */

    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return(eOK);
}

static void sFreeInventoryList(INVENTORY_LIST **InvList)
{
    INVENTORY_LIST *ip, *last_ip;

    if (*InvList ==  NULL)
	return;

    last_ip = NULL;
    for (ip = *InvList; ip; last_ip = ip, ip = ip->next)
    {
	if (last_ip)
	    free(last_ip);
    }
    if (last_ip)
	free(last_ip);

    *InvList = NULL;
    return;
}

static long sLoadInventory(char *prtnum,
			   char *prt_client_id,
			   INVENTORY_LIST **Inv)
{

    INVENTORY_LIST *ip, *head;
    mocaDataRes *res;
    mocaDataRow *row;
    RETURN_STRUCT *CmdRes;
    
    long ret_status;
    char buffer[1000];

    *Inv = NULL;
    head = NULL;

    sprintf(buffer,
	    "get reservable inventory quantity "
	    " where prtnum = '%s' "
	    "   and prt_client_id = '%s'",
	    prtnum, prt_client_id);

    ret_status = srvInitiateInline(buffer, &CmdRes);

    if (ret_status != eOK)
    {
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(ret_status);
    }

    res = srvGetResults(CmdRes);
    
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	ip = (INVENTORY_LIST *) calloc(1, sizeof(INVENTORY_LIST));

	if (ip == NULL)
	{
	    sFreeInventoryList(&head);
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    return(eNO_MEMORY);
	}

	strcpy(ip->prtnum, prtnum);
	strcpy(ip->prt_client_id, prt_client_id);

	ip->untqty = sqlGetLong(res, row, "untqty");
	strncpy(ip->invsts, sqlGetString(res, row, "invsts"), INVSTS_LEN);

	ip->next = head;
	head = ip;
    }

    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    *Inv = head;
    return(eOK);
}
    



/*------------------------------------------------------------------------
 *      I N V E N T O R Y   F U N C T I O N S  --  E N D
 *------------------------------------------------------------------------
 */

/*
 * sMarkAsProcessed - used to mark an individual list item as being 
 *                    processed
 */
static void sMarkAsProcessed(RESV_MAIN *rp)
{
    if (rp->processed == 1)
	return;

    rp->processed = 1;

    return;
}
/*
 * sMarkPartProcessed - used to mark remaining list elements for part 
 *                      as processed.  This is passed a list element to
 *                      start with and all elements following it are 
 *                      processed.
 */
static void sMarkPartProcessed(RESV_MAIN *start,
			       char *prtnum,
			       char *prt_client_id)
{
    RESV_MAIN *rp;

    for (rp = start; rp; rp = rp->next)
    {
	if (rp->processed)
	    continue;
	
	if (misCiStrncmp(prtnum, rp->prtnum, PRTNUM_LEN) == 0 &&
	    misCiStrncmp(prt_client_id, rp->prt_client_id, CLIENT_ID_LEN) == 0)
	{
	    sMarkAsProcessed(rp);
	}
    }
    return;
}

/*------------------------------------------------------------------------
 *      R E S E R V A T I O N  M A N A G E M E N T   F U N C T I O N S
 *------------------------------------------------------------------------
 * The following block of functions are used to manage our list of
 * reservation requirements.  The "list" is actually nested a few 
 * layers deep and grouped according to how inventory should be handed
 * out.  Each RESV_MAIN node should be attempted to be given 100% of 
 * it's demand.  Each RESV_GROUP under a RESV_MAIN node should have 
 * quantity distributed equally to each node.  Each RESV_SET
 * under a RESV_GROUP should have quantity also handed out equally.
 */
static void sFreeReservations(RESV_MAIN *resv)
{
    RESV_MAIN *rp, *last_rp;
    RESV_GROUP *rg, *last_rg;
    RESV_SET *rs, *last_rs;

    last_rp = NULL;
    last_rg = NULL;
    last_rs = NULL;
    for (rp = resv; rp; last_rp = rp, rp = rp->next)
    {
	if (last_rp)
	    free(last_rp);

	last_rg = NULL;
	for (rg = rp->groups; rg; last_rg = rg, rg = rg->next)
	{
	    if (last_rg)
		free(last_rg);

	    last_rs = NULL;
	    for (rs = rg->orders; rs; last_rs = rs, rs = rs->next)
	    {
		if (last_rs)
		    free(last_rs);
	    }
	    if (last_rs)
		free(last_rs);
	}
	if (last_rg)
	    free(last_rg);
    }
    if (last_rp)
	free(last_rp);

    return;
}

static RESV_MAIN *sFindMainReservationForAdd(RESV_MAIN *Start,
					     char *prtnum,
					     char *prt_client_id,
					     char *invsts_prg,
					     long rsvpri,
					     RESV_MAIN **last_rp)
{
    RESV_MAIN *rmp, *last_rmp;
    long rsvpri_threshold;

    /* 
     * Anything above the threshold gets quantity divvied by 
     * amongst the reservation groups...anything below or equal 
     * to the threshold gets a 100% fill attempt
     *
     * What that means is that we will create a new "main" 
     * group for all our priority <= threshold, whereas we'll
     * pile in multiple groups under a given main if the quantity
     * should get divvied amongst them.
     */
    rsvpri_threshold = pckrsv_GetRsvpriThreshold();

    if (rsvpri <= rsvpri_threshold)
    {
	for (rmp = Start; rmp; last_rmp = rmp, rmp = rmp->next)
	    ;
	*last_rp = last_rmp;
	return(NULL);
    }

    /*
     * We're not a priority reservation, go ahead and find our 
     * matching and return
     */
    for (rmp = Start; rmp; last_rmp = rmp, rmp = rmp->next)
    {
	if (rmp->rsvpri == rsvpri &&
	    misCiStrncmp(rmp->prtnum, prtnum, PRTNUM_LEN) == 0 &&
	    misCiStrncmp(rmp->prt_client_id, prt_client_id, PRTNUM_LEN) == 0 &&
	    misCiStrncmp(rmp->invsts_prg, invsts_prg, INVSTS_PRG_LEN) == 0)
	{
	    return(rmp);
	}
    }
    *last_rp = last_rmp;
    return(NULL);
}
static RESV_GROUP *sFindReservationGroup(RESV_MAIN *rp,
					 char *stcust)
{

    RESV_GROUP *rgp;

    for (rgp = rp->groups; rgp; rgp = rgp->next)
    {
	if (misCiStrncmp(rgp->stcust, stcust, STCUST_LEN) == 0)
	    return(rgp);
    }
    return(NULL);
}


static long sAddReservation(RESV_MAIN **Resv,
			    char *prtnum, 
			    char *prt_client_id,
			    char *stcust,
			    char *ordnum,
			    char *ordlin,
			    char *ordsln,
			    char *client_id,
			    char *invsts_prg,
			    long rsvpri,
			    long min_rsvqty,
			    long ordqty,
			    long shpqty,
			    long rsvqty,
			    moca_bool_t married,
			    RESV_MAIN **last_node_added)
{
    RESV_MAIN *rp, *last_rp;
    RESV_GROUP *rgp;
    RESV_SET *rsp;

    long demand_qty;
    
    last_rp = rp = NULL;

    demand_qty = ordqty - shpqty - rsvqty;
    /*
     * Our first job is to identify the main reservation to add
     * this under.
     */
    if (*Resv)
    {
	/*
	 * Find the reservation.  If we have a list, but the
	 * reservation is not found, then this function will 
	 * also return a pointer to the last node looked at...
	 * this allows for a quick add to the end of the list
	 */
    	rp = sFindMainReservationForAdd(*last_node_added,
					prtnum,
					prt_client_id,
					invsts_prg,
					rsvpri,
					&last_rp);
    }
    else 
    {
	rp = NULL;
    }

    if (rp == NULL)
    {
	rp = (RESV_MAIN *) calloc(1, sizeof(RESV_MAIN));
	if (rp == NULL)
	    return(eNO_MEMORY);

	misTrimcpy(rp->prtnum, prtnum, PRTNUM_LEN);
	misTrimcpy(rp->prt_client_id, prt_client_id, CLIENT_ID_LEN);
	misTrimcpy(rp->invsts_prg, invsts_prg, INVSTS_PRG_LEN);
	rp->rsvpri = rsvpri;
	rp->min_rsvqty = min_rsvqty;

	if (last_rp)
	    last_rp->next = rp;
	else
	    *Resv = rp;

	*last_node_added = rp;
    }
    rp->rsv_demand += demand_qty;
    
    /*
     * Ok...now we've got a reservation...now we just need to add
     * in the group/set
     */
    rgp = sFindReservationGroup(rp,
				stcust);
    if (rgp == NULL)
    {
	rgp = (RESV_GROUP *)calloc(1, sizeof(RESV_GROUP));
	if (rgp == NULL)
	    return(eNO_MEMORY);

	misTrimcpy(rgp->stcust, stcust, STCUST_LEN);

	rgp->next = rp->groups;
	rp->groups = rgp;
	
	rp->group_count++;
    }

    rgp->rsv_demand += demand_qty;

    /* Now add the order to the reservation set */

    rsp = (RESV_SET *)calloc(1, sizeof(RESV_SET));
    misTrimcpy(rsp->ordnum, ordnum, ORDNUM_LEN);
    misTrimcpy(rsp->ordlin, ordlin, ORDLIN_LEN);
    misTrimcpy(rsp->ordsln, ordsln, ORDSLN_LEN);
    misTrimcpy(rsp->client_id, client_id, CLIENT_ID_LEN);
    rsp->rsv_demand = demand_qty;
    rsp->married = married;

    rsp->next = rgp->orders;
    rgp->orders = rsp;
    
    return(eOK);
}

/*
 * sLoadReservationRequirements
 *
 * This routine loads the results from the command:
 *   "get order reservation requirements"
 * which should return the following columns:
 *    ordnum, client_id, ordlin, ordsln, prtnum, prt_client_id,
 *    shpqty, ordqty, rsvqty, rsvpri, marcod, bckflg, invsts_prg
 * Rows should be returned in the order they are to be processed.
 */
static long sLoadReservationRequirements(RESV_MAIN **Reservations,
					 RETURN_STRUCT *CmdRes)
{
    mocaDataRes *res;
    mocaDataRow *row;
    RESV_MAIN *last_node;
    long ret_status;
    long count;
    long married;

    res = srvGetResults(CmdRes);

    last_node = NULL;

    for (count = 0, row = sqlGetRow(res); row; 
	 count++, row = sqlGetNextRow(row))
    {
	if (!sqlIsNull(res, row, "marcod") ||
	    misCiStrncmp(sqlGetString(res, row, "ordsln"),
                         pckrsv_GetSublinFiller(), ORDSLN_LEN) != 0)
	{
	    married = 1;
	}
	else
	{
	    married = 0;
	}

	ret_status = sAddReservation(Reservations,
				     sqlGetString(res, row, "prtnum"),
				     sqlGetString(res, row, "prt_client_id"),
				     sqlGetString(res, row, "stcust"),
				     sqlGetString(res, row, "ordnum"),
				     sqlGetString(res, row, "ordlin"),
				     sqlGetString(res, row, "ordsln"),
				     sqlGetString(res, row, "client_id"),
				     sqlGetString(res, row, "invsts_prg"),
				     sqlGetLong(res, row, "rsvpri"),
				     (!sqlIsNull(res, row, "min_rsvqty") ?
				      sqlGetLong(res, row, "min_rsvqty") : 1),
				     sqlGetLong(res, row, "ordqty"),
				     sqlGetLong(res, row, "shpqty"),
				     sqlGetLong(res, row, "rsvqty"),
				     married,
				     &last_node);
	if (ret_status != eOK)
	{
	    misTrc(T_FLOW, "Error loading reservation requirements (%d)",
		   ret_status);
	    return(ret_status);
	}
    }

    pckrsv_WriteTrc("  Loaded %d reservation requirements", count);
    return(eOK);

}

RESV_MAIN *sGetNextReservation(RESV_MAIN *rp)
{
    for (;rp; rp = rp->next)
    {
	if (!rp->processed)
	    return(rp);
    }
    return(NULL);
}
/*------------------------------------------------------------------------
 *      R E S E R V A T I O N  M A N A G E M E N T   F U N C T I O N S
 *                             E   N   D  
 *------------------------------------------------------------------------
 */

static void sDistributeReservation(RESV_MAIN *rm)
{
    long done;
    long need;
    long rem;
    RESV_GROUP *rg;
    RESV_SET *rs;

    if (rm->addqty <= 0)
	return;

    if (rm->rsv_demand <= rm->addqty)
    {
	/*
	 * We can just blast the quantity out to each group/order
	 */
	for (rg = rm->groups; rg; rg = rg->next)
	{
	    for (rs = rg->orders; rs; rs = rs->next)
	    {
		rs->rsv_granted = rs->rsv_demand;
		rem = rs->rsv_demand % rm->min_rsvqty;
		
		/* level out for min_rsvqty */
		if (rem > 0)
		    rs->rsv_granted -= rem;
	    }
	}
	rm->addqty = 0;
	return;
    }

    /*
     * We don't have enough to go around...this means that 
     * we have to be "fair" about how we dole this out...
     *
     * The way we do this is relatively straightforward...
     *
     *  1.  Take the "addqty" from the MAIN reservation
     *      and distribute that quantity to the reservation
     *      groups.
     *  2.  As quantity is distributed, the MAIN addqty is 
     *      decremented
     *  3.  Once all of the main addqty is handed out, then
     *      we'll go to the individual reservation groups
     *      and distribute that quantity out to orders
     */

	 
    done = 0;
    while (rm->addqty > 0 && rm->addqty >= rm->min_rsvqty && !done)
    {
	done = 1;
	for (rg = rm->groups; rg && rm->addqty > 0; rg = rg->next)
	{
	    need = rg->rsv_demand - rg->rsv_granted - rg->addqty;
	    if (need >= rm->min_rsvqty && rm->addqty >= rm->min_rsvqty)
	    {
		rg->addqty += rm->min_rsvqty;
		rm->addqty -= rm->min_rsvqty;
		done = 0; /*this line has been added to allow partial quantity reservations*/
	    }
	}
    }
    /*
     * The quantity has now all been handed out to the group level 
     * ...now we have to distribute each group quantity out to the 
     * the reservation sets (orders)
     */
	 
	 
    for (rg = rm->groups; rg; rg = rg->next)
    {
	
	while (rg->addqty >= rm->min_rsvqty && !done)
	{
	    done = 1;
	    for (rs = rg->orders; rs && rg->addqty > 0; rs = rs->next)
	    {
		need = rs->rsv_demand - rs->rsv_granted;
		if (need >= rm->min_rsvqty)
		{
		    rs->rsv_granted += rm->min_rsvqty;
		    rg->addqty -= rm->min_rsvqty;
		    done = 0;
		}
	    }
	}
    }
    return;
}
static moca_bool_t sPartMatches(RESV_MAIN *rp, INVENTORY_LIST *ip)
{
    if (misCiStrncmp(ip->prtnum, rp->prtnum, PRTNUM_LEN) == 0 &&
	misCiStrncmp(ip->prt_client_id, rp->prt_client_id, CLIENT_ID_LEN) == 0)
	return(MOCA_TRUE);
    else
	return(MOCA_FALSE);
}
static long sAssignReservations(RESV_MAIN *start_rp,
				INVENTORY_LIST *Ilist)
{
    RESV_MAIN *start, *rp;
    INVENTORY_LIST *ip;
    long qty_needed, qty_avail;

    start = start_rp;

    for (rp = start_rp; rp; rp = rp->next)
    {
	if (rp->processed == 1)
	    continue;
	
	if (!sPartMatches(rp, Ilist))
	    continue;

	qty_needed = rp->rsv_demand - rp->rsv_granted;

	for (ip = Ilist; ip && qty_needed > 0; ip = ip->next)
	{
	    if (!sProgressionIsMatch(rp->invsts_prg, ip->invsts))
		continue;

	    qty_avail = ip->untqty - ip->rsvqty - ip->inpqty - ip->pckqty
		- ip->stgqty - ip->wkoqty;

	    if (qty_avail <= 0)
	    {
		pckrsv_WriteTrc("     Part: %s, Client: %s, Status: %s "
				"is FULLY RESERVED\n"
				"       No inventory available "
				"to apply to reservations.",
				ip->prtnum,
				ip->prt_client_id,
				ip->invsts);
		pckrsv_WriteTrc("       Qty: %d, rsv: %d, inp: %d, "
				"pck: %d, stg: %d",
				ip->untqty, ip->rsvqty, ip->inpqty, ip->pckqty,
				ip->stgqty);
		continue;
	    }

	    
	    if (qty_needed <= qty_avail)
	    {
		pckrsv_WriteTrc("     Quantity available is %d "
				"- using %d of that", 
				qty_avail, qty_needed);
		rp->addqty = qty_needed;
		rp->rsv_granted += qty_needed;
		ip->rsvqty += qty_needed;
		qty_needed = 0;
	    }
	    else
	    {
		pckrsv_WriteTrc("     Quantity available is %d "
				"- using %d of that", 
				qty_avail, qty_avail);
		rp->addqty = qty_avail;
		qty_needed -= qty_avail;
		ip->rsvqty += qty_avail;
		rp->rsv_granted += qty_avail;
	    }
	    sDistributeReservation(rp);
	}
	sMarkAsProcessed(rp);
    }
    sMarkPartProcessed(start_rp,
		       Ilist->prtnum,
		       Ilist->prt_client_id);

    return(eOK);
}

static long sProcessReservation(RESV_MAIN *rp)
{
    long ret_status;
    INVENTORY_LIST *InventoryList;

    pckrsv_WriteTrc("\n   Loading inventory for part: %s, client: %s",
		    rp->prtnum, rp->prt_client_id);

    InventoryList = NULL;
    ret_status = sLoadInventory(rp->prtnum, 
				rp->prt_client_id,
				&InventoryList);
    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("Error %d loading inventory for "
			"part: %s, client: %s - skipping part",
			ret_status, rp->prtnum, rp->prt_client_id);
	
	sMarkPartProcessed(rp, 
			   rp->prtnum, 
			   rp->prt_client_id);
	return(ret_status);
    }
    
    /*
     * Next, we pull all existing reservations and account for
     * that from the inventory we have.
     */
    pckrsv_WriteTrc("   Loading existing reservations"
		    " for part: %s, client: %s",
		    rp->prtnum, rp->prt_client_id);
    
    ret_status = sReconcileReservedInventory(rp->prtnum,
					     rp->prt_client_id,
					     InventoryList);
	
    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("Error %d loading reservation information for "
			"part: %s, client: %s",
			ret_status, rp->prtnum, rp->prt_client_id);
	sFreeInventoryList(&InventoryList);
	sMarkPartProcessed(rp, 
			   rp->prtnum, 
			   rp->prt_client_id);
	return(ret_status);
    }
    
    /*
     * Next, we pull any orders already committed to shipments
     * and account for that from the inventory we have.
     *
     */

    pckrsv_WriteTrc("   Loading shipment information "
		    " for part: %s, client: %s",
		    rp->prtnum, rp->prt_client_id);

    ret_status = sReconcileShipmentInventory(rp->prtnum,
					     rp->prt_client_id,
					     InventoryList);

    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("Error %d loading shipment information for "
			"part: %s, client: %s",
			ret_status, rp->prtnum, rp->prt_client_id);
	sFreeInventoryList(&InventoryList);
	sMarkPartProcessed(rp, 
			   rp->prtnum, 
			   rp->prt_client_id);
	return(ret_status);
    }

    /*
     *
     * Next, we pull any work orders which have inventory 
     * committed to them and account for that from the inventory we have
     *
     */
    pckrsv_WriteTrc("   Loading work order information "
		    " for part: %s, client: %s\n",
		    rp->prtnum, rp->prt_client_id);
    ret_status = sReconcileWorkOrderInventory(rp->prtnum,
					      rp->prt_client_id,
					      InventoryList);

    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("Error %d loading work order information for "
			"part: %s, client: %s",
			ret_status, rp->prtnum, rp->prt_client_id);
	sFreeInventoryList(&InventoryList);
	sMarkPartProcessed(rp, 
			   rp->prtnum, 
			   rp->prt_client_id);
	return(ret_status);
    }
	
    /*
     * Now we can assign out the remaining quantity that we have
     */
    sAssignReservations(rp,
			InventoryList);
    
    /* All done with the inventory loaded... */
    sFreeInventoryList(&InventoryList);
    
    return(eOK);
}

static long sLevelMarriedLines(ORD_LIST *olist, moca_bool_t commit_flg)
{
    char buffer[2000];
    long ret_status;

    ORD_LIST *op;


    for (op = olist; op; op = op->next)
    {
	if (!op->married)
	    continue;

	pckrsv_WriteTrc("  Leveling married lines for order: %s, client: %s",
			op->ordnum, op->client_id);

	sprintf(buffer,
		"process married line leveling for reservation "
		"  where ordnum = '%s' "
		"    and client_id = '%s' ",
		op->ordnum,
		op->client_id);
	
	ret_status = srvInitiateInline(buffer, NULL);
	if (ret_status != eOK)
	{
	    pckrsv_WriteTrc("   Failed (%d) attempting to level lines",
			    ret_status);
	    return(ret_status);
	}
	if (commit_flg)
	    sqlCommit();
    }
    return(eOK);
}

static void sFreeOrderList(ORD_LIST *OrdList)
{
    ORD_LIST *op, *last_op;

    if (OrdList == NULL)
	return;

    last_op = NULL;
    for (op = OrdList; op; last_op = op, op = op->next)
    {
	if (last_op)
	    free(last_op);
    }
    if (last_op)
	free(last_op);

    return;
}

static long sAddOrderToList(char *ordnum, 
			    char *client_id,
			    moca_bool_t married,
			    ORD_LIST **olist)
{
    ORD_LIST *op;

    if (!*olist)
    {
	op = (ORD_LIST *) calloc(1, sizeof(ORD_LIST));
	if (!op)
	    return(eNO_MEMORY);
	strncpy(op->ordnum, ordnum, ORDNUM_LEN);
	strncpy(op->client_id, client_id, CLIENT_ID_LEN);
	op->married = married;
	*olist = op;
    }
    else
    {
	for (op = *olist; op; op = op->next)
	{
	    if (misCiStrncmp(op->ordnum, ordnum, ORDNUM_LEN) == 0 &&
		misCiStrncmp(op->client_id, client_id, CLIENT_ID_LEN) == 0)
	    {
		/* If any lines married, make sure we show married */
		if (married)
		    op->married = married;

		/* Found it...just get out */
		return(eOK);
	    }
	}
	op = (ORD_LIST *) calloc(1, sizeof(ORD_LIST));	
	if (!op)
	    return(eNO_MEMORY);
	strncpy(op->ordnum, ordnum, ORDNUM_LEN);
	strncpy(op->client_id, client_id, CLIENT_ID_LEN);
	op->married = married;
	op->next = *olist;
	*olist = op;
    }
    return(eOK);
}

static long sWriteReservations(RESV_MAIN *rp, 
			       ORD_LIST **olist,
			       moca_bool_t no_results_flg,
			       moca_bool_t comflg)
{
    long ret_status;
    char buffer[2000];
    RETURN_STRUCT *CmdRes;
    
    RESV_GROUP *rg; 
    RESV_SET *rs;

    for (rg = rp->groups; rg; rg = rg->next)
    {
	for (rs = rg->orders; rs; rs = rs->next)
	{
	    if (rs->rsv_granted > 0)
	    {
		sprintf(buffer,
			"write order line reservation "
			" where ordnum = '%s' "
			"   and client_id = '%s' "
			"   and ordlin = '%s' "
			"   and ordsln = '%s' "
			"   and add_rsvqty = %d ",
			rs->ordnum, rs->client_id, rs->ordlin,
			rs->ordsln, rs->rsv_granted);

		CmdRes = NULL;
		ret_status = srvInitiateInline(buffer, NULL);
		if (ret_status != eOK)
		{
		    pckrsv_WriteTrc("Failed %d, writing line reservation",
				    ret_status);
		    return(ret_status);
		}
		if (rs->married || !no_results_flg)
		{
		    sAddOrderToList(rs->ordnum, 
				    rs->client_id,
				    rs->married, 
				    olist);
		}

		if (comflg)
		    sqlCommit();
	    }
	}
    }
    return(eOK);
}

LIBEXPORT 
RETURN_STRUCT *varpckProcessOrderReservation(char *ordnum_i,
					  char *client_id_i,
					  char *prtnum_i,
					  char *prt_client_id_i,
					  moca_bool_t *keep_existing_flg_i,
					  moca_bool_t *comflg_i,
					  moca_bool_t *no_results_flg_i)
{
    long ret_status;
    char buffer[2000];
    char ordnum[ORDNUM_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    RETURN_STRUCT *CmdRes;
    RESV_MAIN *Reservations, *rp;
    ORD_LIST *ordList, *op;
    moca_bool_t no_results_flg;
    moca_bool_t comflg;

    comflg = (comflg_i ? *comflg_i : 0);
    no_results_flg = (no_results_flg_i ? *no_results_flg_i : 0);

    pckrsv_OpenTrc(comflg);

    ordList = NULL;

    memset(ordnum, 0, sizeof(ordnum));
    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(client_id, 0, sizeof(client_id));

    Reservations = NULL;
    if (!pckrsv_isInstalled())
    {
	pckrsv_CloseTrc();
	return(srvResults(eRSV_NOT_INSTALLED, NULL));
    }

    if (ordnum_i && misTrimLen(ordnum_i, ORDNUM_LEN))
	misTrimcpy(ordnum, ordnum_i, ORDNUM_LEN);
    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
    {
	misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN);
	
	ret_status = appGetClient(prt_client_id_i, prt_client_id);
	if (strlen(prt_client_id) == 0)
	{
	    pckrsv_WriteTrc("We got a prtnum with no prt_client_id "
			    "in a 3PL env - error");
	    pckrsv_CloseTrc();
	    return(APPMissingArg("prt_client_id"));
	}
    }

    if (strlen(ordnum))
    {
	pckrsv_WriteTrc("  Reserving against order: %s, client: %s", 
			ordnum, client_id);
    }

    if (strlen(prtnum))
    {
	pckrsv_WriteTrc("  Reserving against prt: %s, client: %s", 
			prtnum, prt_client_id);
    }

    if (!keep_existing_flg_i || *keep_existing_flg_i == MOCA_FALSE)
    {
	pckrsv_WriteTrc("  Clearing existing reservations");

	sprintf(buffer, "clear existing order reservations");

	CmdRes = NULL;
	ret_status = srvInitiateInline(buffer, &CmdRes);
	if (ret_status != eOK && 
	    ret_status != eDB_NO_ROWS_AFFECTED &&
	    ret_status != eSRV_NO_ROWS_AFFECTED)
	{
	    pckrsv_WriteTrc(" ERROR %d clearing pick reservations",
			    ret_status);
	    pckrsv_CloseTrc();
	    return(CmdRes);
	}
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
    }
    else
    {
	pckrsv_WriteTrc("  Keeping all existing reservations");
    }



    /* Get an ordered list of orders to reserve against */
    sprintf(buffer,
	    "get order reservation requirements"); 
    CmdRes = NULL;
    ret_status = srvInitiateInline(buffer, &CmdRes);
    
    if (ret_status != eOK)
    {
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    srvFreeMemory(SRVRET_STRUCT, CmdRes);
	    pckrsv_WriteTrc("No reservations required - exiting");
	    return(srvResults(eOK, NULL));
	}
	else
	{
	    pckrsv_WriteTrc(" ERROR %d getting pick "
			    "reservations requirements",
			    ret_status);
	    
	    pckrsv_CloseTrc();
	    return(CmdRes);
	}
    }

    pckrsv_WriteTrc("  Loading reservation requirements into memory");
    ret_status = sLoadReservationRequirements(&Reservations, CmdRes);
    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("ERROR %d loading requirements - exiting",
			ret_status);
	pckrsv_CloseTrc();
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return(srvResults(ret_status, NULL));
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);


    /* First process all the reservations and queue up our writes */
    for (rp = sGetNextReservation(Reservations); 
	 rp; rp = sGetNextReservation(rp))
    {
	sProcessReservation(rp);
    }

    /* Now go off and write the reservations */
    pckrsv_WriteTrc("  Writing reservations");
    for (rp = Reservations; rp; rp = rp->next)
    {
	sWriteReservations(rp,
			   &ordList,
			   no_results_flg,
			   comflg);
    }

    sFreeReservations(Reservations);

    pckrsv_WriteTrc("  Performing line leveling for married code processing ");

    ret_status = sLevelMarriedLines(ordList, comflg);
    if (ret_status != eOK)
    {
	pckrsv_WriteTrc("ERROR %d leveling married codes",
			ret_status);
	pckrsv_CloseTrc();
	sFreeOrderList(ordList);
	return(srvResults(ret_status, NULL));
    }

    /* Since this component may publish out multiple rows,
     * and we often need something to trigger off of from this component
     * just once each time it runs, we're going to call a "register" 
     * component here.
     */

    sprintf(buffer,
            " register process order reservation ");

    ret_status = srvInitiateCommand(buffer, NULL);
    if ((ret_status != eOK) &&
        (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        pckrsv_WriteTrc("ERROR %d registering order reservation",
                        ret_status);
        pckrsv_CloseTrc();
        sFreeOrderList(ordList);
        return (srvResults(ret_status, NULL));
    }


    pckrsv_WriteTrc("\n\n****  Reservation process complete - "
		    "publishing results ****");
    
    if (no_results_flg)
    {
	CmdRes = srvResults(eOK, NULL);
    }
    else
    {
	CmdRes = srvResultsInit(eOK,
				"ordnum", COMTYP_CHAR, ORDNUM_LEN,
				"client_id", COMTYP_CHAR, CLIENT_ID_LEN,
				NULL);
	
	for (op = ordList; op; op = op->next)
	{
	    srvResultsAdd(CmdRes, op->ordnum, op->client_id);
	}
    }

    sFreeOrderList(ordList);
    
    return(CmdRes);

}
