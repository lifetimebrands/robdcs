static const char *rcsid = "$Id: lmProcessLMSIdentificationEvent.c,v 1.3 2001/04/27 15:29:45 verdeyen Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "lmlib.h"

LIBEXPORT 
RETURN_STRUCT *varProcessLMSIdentificationEvent(char *oprcod_i,
						char *invtyp_i,
						char *dtlnum_i,
						char *subnum_i,
						char *lodnum_i,
						char *usr_id_i, 
						char *dstare_i,
						char *dstloc_i,
						char *srcare_i,
						char *srcloc_i) 
{
    long ret_status;

    char buffer[2000];
    char subselect[2000];
    char *p;

    char oprcod[OPRCOD_LEN+1];
    char invtyp[INVTYP_LEN+1];


    LMS_TRANSACTION_INFO lms;
    mocaDataRes *res;
    mocaDataRow *row;
    static int isInstalled = -1;
    long casqty;
    double untwgt, untvol;

    char locale_id[LOCALE_ID_LEN+1];

    if (isInstalled == -1)
    {
	if (appIsInstalled(POLCOD_LMS))
	    isInstalled = 1;
	else
	    isInstalled = 0;
    }
    
    if (!isInstalled)
    {
	misTrc(T_FLOW, "Skipping LMS processing as option is not installed. ");
	return(srvResults(eOK, NULL));
    }

    memset(&lms, 0, sizeof(lms));

    /*
    ** -------------------------------------------------- 
    ** Place the inbound data into the local char. arrays
    ** -------------------------------------------------- 
    */
    
    memset(oprcod, 0, sizeof(oprcod));
    if (oprcod_i && misTrimLen(oprcod_i, OPRCOD_LEN) > 0) 
    {
	misTrimcpy(lms.oprcod, oprcod_i, OPRCOD_LEN);
    }
    
    memset(invtyp, 0, sizeof(invtyp));
    if (invtyp_i && misTrimLen(invtyp_i, INVTYP_LEN) > 0) 
    {
	misTrimcpy(invtyp, invtyp_i, INVTYP_LEN);
    }
    
    /*
    ** -------------------------------------------------- 
    ** set untqty to 0 if inbound value is null.
    ** -------------------------------------------------- 
    */
    
    if (dtlnum_i && misTrimLen(dtlnum_i, DTLNUM_LEN) > 0) 
    {
	misTrimcpy(lms.dtlnum, dtlnum_i, DTLNUM_LEN);
    }
    if (subnum_i && misTrimLen(subnum_i, SUBNUM_LEN) > 0) 
    {
	misTrimcpy(lms.subnum, subnum_i, SUBNUM_LEN);
    }
    if (lodnum_i && misTrimLen(lodnum_i, LODNUM_LEN) > 0) 
    {
	misTrimcpy(lms.lodnum, lodnum_i, LODNUM_LEN);
    }

    lm_GetUserId(usr_id_i, lms.usr_id);

    if (lm_IsFromAutomation(lms.usr_id))
    {
	misTrc(T_FLOW, "Skipping identification event from automation");
	return(srvResults(eOK, NULL));
    }
   
    if (dstloc_i && misTrimLen(dstloc_i, STOLOC_LEN) > 0) 
	misTrimcpy(lms.dstloc, dstloc_i, STOLOC_LEN);

    if (dstare_i && misTrimLen(dstare_i, ARECOD_LEN) > 0)
	misTrimcpy(lms.dstare, dstare_i, ARECOD_LEN);

    if (srcloc_i && misTrimLen(srcloc_i, STOLOC_LEN) > 0) 
	misTrimcpy(lms.srcloc, srcloc_i, STOLOC_LEN);

    if (srcare_i && misTrimLen(srcare_i, ARECOD_LEN) > 0)
	misTrimcpy(lms.srcare, srcare_i, ARECOD_LEN);

    /*
    ** ---------------------------------------------------------- 
    ** Suppress IE for workstation activities unless specifically
    ** requested (oprcod not null). 
    ** ---------------------------------------------------------- 
    */

    if ((p = osGetVar(LESENV_DEVCOD)) != NULL)
	misTrimcpy(lms.devcod, p, DEVCOD_LEN);



    if (lm_IsWorkstation(lms.devcod) && 
	misTrimLen(lms.oprcod, OPRCOD_LEN) == 0)
    {
	misTrc(T_FLOW, 
	       "Got identification event from workstation"
	       " with no oprcod specified...skipping");
	return(srvResults(eOK, NULL));
    }
    /*
    ** ---------------------------------------------------------- 
    ** Make sure that the IE is only occurring for DSTLOC values 
    ** within the expected receipt area (EXPFLG = 'Y'). Glomm 
    ** onto the DSTARE simultaneously.  
    ** ---------------------------------------------------------- 
    */
    sprintf(buffer,
	    "select l.arecod, l.stoloc, a.expflg, a.wip_expflg "
	    "  from invlod il, locmst l, aremst a"
            " where l.stoloc = il.stoloc "
            "   and il.lodnum = '%s'" 
 	    "   and l.arecod = a.arecod"
	    "   and %ld in (a.expflg, a.wip_expflg) ", 
	    lms.lodnum,
	    BOOLEAN_TRUE);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        return(srvResults(eOK, NULL));
    }

    row = sqlGetRow(res);

    if (sqlGetBoolean(res, row, "wip_expflg"))
	strcpy(lms.actcod, ACTCOD_IDENTIFICATION_WO);
    else
	strcpy(lms.actcod, ACTCOD_IDENTIFICATION);

    if (strlen(lms.srcloc) == 0)
	strncpy(lms.srcloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);

    if (strlen(lms.dstloc) == 0)
	strncpy(lms.dstloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
    
    if (strlen(lms.srcare) == 0)
	strncpy(lms.srcare, sqlGetString(res, row, "arecod"), ARECOD_LEN);

    if (strlen(lms.dstare) == 0)
	strncpy(lms.dstare, sqlGetString(res, row, "arecod"), ARECOD_LEN);

    sqlFreeResults(res);

    if (misTrimLen(lms.dtlnum, DTLNUM_LEN))
    {
	sprintf(subselect,
		"(select '%s' dtlnum from dual)", lms.dtlnum);
    }
    else if (misTrimLen(lms.subnum, SUBNUM_LEN))
    {
	sprintf(subselect,
		"(select dtlnum from invdtl where subnum = '%s')", 
		lms.subnum);
    }
    else if (misTrimLen(lms.lodnum, LODNUM_LEN))
    {
	sprintf(subselect,
		"(select id.dtlnum "
		"   from invdtl id, invsub ivs"
		"  where id.subnum = ivs.subnum "
		"    and ivs.lodnum = '%s')",
		lms.lodnum);
    }
    if (strlen(subselect))
    {
	sprintf(buffer,
		"select sum(id.untqty) untqty, id.untcas, pm.lodlvl, "
		"       id.prtnum, id.prt_client_id, "
		"       pm.grswgt," 
		"       fm.caslen * fm.cashgt * fm.caswid casvol, "
		"       sum((pm.grswgt * (id.untqty/id.untcas))) untwgt, "
		"       sum(((fm.caslen * fm.cashgt * fm.caswid) * "
		"           (id.untqty/id.untcas))) untvol "
		"  from ftpmst fm, prtmst pm, invdtl id "
		" where id.dtlnum in %s"
		"   and id.prtnum = pm.prtnum "
		"   and id.prt_client_id = pm.prt_client_id "
		"   and id.ftpcod = fm.ftpcod "
		" group by id.prtnum, id.prt_client_id, id.untcas, pm.lodlvl,"
		"       pm.grswgt, "
		"       fm.caslen * fm.cashgt * fm.caswid ",
		subselect);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(srvResults(ret_status, NULL));
	}
	else
	{
	    row = sqlGetRow(res);
	    casqty = 
		sqlGetLong(res, row, "untqty") / sqlGetLong(res, row, "untcas");
	    untwgt = sqlGetFloat(res, row, "untwgt");
	    untvol = sqlGetFloat(res, row, "untvol");
	    lms.untwgt = sqlGetFloat(res, row, "grswgt");
	    lms.untvol = sqlGetFloat(res, row, "casvol");
	    if (strcmp(LODLVL_LOAD, sqlGetString(res, row, "lodlvl")) == 0)
	    {
		lms.palqty = 1;
	    }
	    else if (strcmp(LODLVL_SUBLOAD, 
			    sqlGetString(res, row, "lodlvl")) == 0)
	    {
		lms.casqty = 
		    sqlGetLong(res, row, "untqty") / 
		    sqlGetLong(res, row, "untcas");
	    }
	    else
		lms.untqty = sqlGetLong(res, row, "untqty");

		    
	    misTrimcpy(lms.prtnum, 
		       sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
	    misTrimcpy(lms.prt_client_id, 
                       sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
	    sqlFreeResults(res);
	}
    }
    else 
    {
	misTrc(T_FLOW, "Didn't receive info for ID");
	return(srvResults(eINVALID_ARGS, NULL));
    }

    
    /*
    ** -------------------------------------------------------- 
    ** Get the LNGDSC for the PRTNUM if the PRTNUM is not null.
    ** -------------------------------------------------------- 
    */

    if (misTrimLen(lms.prtnum, PRTNUM_LEN))
    {
        /*  Determine the locale.  Note: the locale id of the session is
            used, and it shouldn't be.  We can get away with it for now since
            CALM is not internationalized.                                */

        strcpy (locale_id,
		osGetVar(LESENV_LOCALE_ID) ? osGetVar(LESENV_LOCALE_ID) : "");
	
	sprintf(buffer,
		"select lngdsc prtdsc "
		"  from prtdsc "
                " where colnam = '%s' " 
		"   and colval = '%-.*s|%-.*s' "
		"   and locale_id = '%-.*s' ",
                PRTDSC_COLNAM,
                PRTNUM_LEN, lms.prtnum, CLIENT_ID_LEN, lms.prt_client_id,
                LOCALE_ID_LEN, locale_id);

	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
	    strcpy(lms.prtdsc, lms.prtnum);
	}
	else
	{
	    row = sqlGetRow(res); 
	    misTrimcpy(lms.prtdsc, 
		       sqlGetString(res, row, "prtdsc"), LNGDSC_LEN);
	}
    sqlFreeResults(res);
    }

    strcpy(lms.lmscod, LMSCOD_DIRECT);
    strcpy(lms.lmstyp, LMSTYP_OBTAIN);
    lms.palqty = 0;
    lms.casqty = casqty;

    ret_status = lm_WriteLMSActuals(&lms);

    lms.palqty = 1;
    lms.casqty = 0;
    lms.untwgt = untwgt;
    lms.untvol = untvol;
    strcpy(lms.lmstyp, LMSTYP_PLACE);

    ret_status = lm_WriteLMSActuals(&lms);

    return(srvResults(ret_status, NULL));
    
}
