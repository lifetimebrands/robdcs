static const char *rcsid = "$Id: varGetBuildLoadSlotInfo.c,v 1.7 2002/02/28 13:57:41 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION: This function is part of the palletizing on RF for Lifetime
 *		 Hoan Gap 12.01.  Basically it performs the activity of
 * 		 building the load in the slot corresponding with the 
 * 	         sorter lane.  It uses policies to keep track of the slot 
 *		 usage.
 * 
 *		 Normally more than one build location can be used for 
 *		 building one shipment at a time, however, an exception 
 *		 is made for replenishment cases and ticketed/non small 
 *		 packages will always be built seperately to handle these
 *		 without mixing with other cases. 
 *	
 *	7/2/01 kjh fixed from cstprq.vc_prenum to instead properly use vc_pretck
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <vargendef.h>
#include <common.h>
#include <stdlib.h>

LIBEXPORT 
RETURN_STRUCT  *varGetBuildLoadSlotInfo( char *subnum_i ,
				   	 long *laneloc_i) 
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;

    char laneloc[STOLOC_LEN+1];
    char ship_id[SHIP_ID_LEN+1] ;
    char prtnum[PRTNUM_LEN+1] ;
    char dstcar[DSTCAR_LEN+1];
    char buffer[3500] ;
    char lodnum[LODNUM_LEN + 1] ;
    char subnum[SUBNUM_LEN + 1] ;
    char carcod[CARCOD_LEN]  = "";
    char wkonum[WKONUM_LEN + 1];

    char cas_rem[10]= "";
    char est_pal[10]= "";
    char slotno1[3] = "";
    char slotno2[3] = "";
    char slotno3[3] = "";
    char slotno4[3] = "";
    char slotno5[3] = "";
    
    long untcas ;
    long ret_status ;
    long status ;
    long last_case = 0;
    long tkt_flg = 0 ;

    mocaDataRow   *row ;
    mocaDataRes   *res ;

    long new_slot_flag;

    memset(laneloc,0,sizeof(laneloc));
    memset(ship_id,0,sizeof(ship_id));
    memset(prtnum,0,sizeof(prtnum));
    memset(dstcar,0,sizeof(dstcar));
    memset(lodnum,0,sizeof(lodnum));
    memset(subnum,0,sizeof(subnum));
    memset(wkonum,0,sizeof(wkonum));

    new_slot_flag = 0;

    CurPtr = srvResultsInit(eOK,
			     "prtnum" , COMTYP_CHAR, PRTNUM_LEN,
			     "cas_rem", COMTYP_CHAR, 8,
			     "est_pal", COMTYP_CHAR, 8,
			     "ship_id", COMTYP_CHAR, SHIP_ID_LEN , 
			     "slotno1", COMTYP_CHAR, 2,
			     "slotno2", COMTYP_CHAR, 2,
			     "slotno3", COMTYP_CHAR, 2,
			     "slotno4", COMTYP_CHAR, 2,
			     "slotno5", COMTYP_CHAR, 2,
			     "new_slot_flag", COMTYP_INT , 1 ,
			     "last_case", COMTYP_INT , 1 ,
			     NULL);

    misTrimcpy(subnum, subnum_i, LODNUM_LEN );
    misTrimcpy(laneloc, laneloc_i, STOLOC_LEN );
  
    /* get information on the case . PART no. and Cases Left  */ 
    sprintf(buffer,
	    "select invdtl.prtnum "
	    "  from invsub , invdtl "
	    " where invsub.subnum = invdtl.subnum "
	    "   and invsub.subnum = '%s' " ,
	    subnum ); 
    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }
    row = sqlGetRow (res) ;
    strcpy (prtnum, sqlGetString(res,row,"prtnum")) ;
    sqlFreeResults(res);


    /* Roman Barykin added a check if it's a work order case */
    sprintf(buffer,
    "select max(pckwrk.wkonum) wkonum     "
    "  from pckwrk, invsub                "
    " where invsub.subucc = pckwrk.subucc "
    "   and invsub.subnum = '%s'          ",
    subnum);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	  sqlFreeResults(res);
	  return (srvSetupReturn(ret_status,""));
    }
    row = sqlGetRow (res);
    strcpy (wkonum, sqlGetString(res,row,"wkonum"));
    sqlFreeResults(res);
 
    if(misTrimLen(wkonum, WKONUM_LEN) > 0)
    {
         /*  Work order logic to return the slot used for work order */
  	 /*  If no slot is currently used, set new slot to YES       */
         misTrc(T_FLOW, "##### Palatizing work order case. #####");
 	 strcpy(ship_id, " ") ;
         strcpy(cas_rem, "WRKORD");
	
	 sprintf(buffer,
	 "select distinct to_char(poldat.srtseq) slotno  "
	 "  from pckwrk, poldat, invlod, invsub, invdtl  "
	 " where poldat.polcod = 'VAR'                   "
	 "   and poldat.polvar = 'SHIP-STAGE-BUILD-LOAD' "
	 "   and poldat.polval = '%s'                    " 
	 "   and invlod.stoloc = poldat.rtstr1 "
	 "   and invlod.lodnum = invsub.lodnum "
	 "   and invsub.subnum = invdtl.subnum "
	 "   and invdtl.ship_line_id is null   "
         "   and invsub.subucc = pckwrk.subucc "
         "   and pckwrk.wkonum = '%s'          ",
	 laneloc,wkonum);

	 ret_status = sqlExecStr(buffer, &res) ;
	 if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	      sqlFreeResults(res);
	      return (srvSetupReturn(ret_status,"")) ;
	 }

	 if (ret_status == eDB_NO_ROWS_AFFECTED) {  
	     new_slot_flag = 1 ;
	     srvResultsAdd(CurPtr, prtnum, cas_rem, est_pal, ship_id, slotno1,
    	        slotno2,slotno3,slotno4, slotno5, new_slot_flag, last_case); 
	     return (CurPtr) ;
	 }
	
	 /* work order slot found */
	 row = sqlGetRow (res) ;
	 strcpy (slotno1, sqlGetString(res,row,"slotno")) ;
	 sqlFreeResults(res);

	 /* see if there are more slots available  */ 
	 row = sqlGetNextRow (row); 

	 if (row) {
	    strcpy (slotno2, sqlGetString(res,row,"slotno")) ;
	    row = sqlGetNextRow (row);

	    if (row) {
		strcpy (slotno3, sqlGetString(res,row,"slotno")) ;
		row = sqlGetNextRow (row);

	    	if (row) {
		    strcpy (slotno4, sqlGetString(res,row,"slotno")) ;
		    row = sqlGetNextRow (row);

		    if (row) {
			strcpy (slotno5, sqlGetString(res,row,"slotno")) ;
			sqlFreeResults(res);
		    }
		}
	    }
	 }

	 srvResultsAdd(CurPtr, prtnum,	cas_rem, est_pal, ship_id, slotno1,
		    slotno2,slotno3,slotno4,slotno5, new_slot_flag, last_case); 
	 return (CurPtr) ;
    }

    /*  see if its a replenishment case */ 
    /*  If inventory exists for the passed-in subnum (which it must if we   */
    /*  have gotten this far), check to see if a pckwrk record exists for   */
    /*  it. If it doesn't, we are assuming that it is a replenishment case. */
    /*  If it isn't, RF palletizing will catch it because it will not       */
    /*  be in a valid location (lane loc, conveyor)                         */
    sprintf(buffer,
    "select pckwrk.wrktyp                 "
    "  from pckwrk, invsub                "
    " where invsub.subucc = pckwrk.subucc "
    "   and invsub.subnum = '%s'          ",
    subnum);

    ret_status = sqlExecStr(buffer, &res);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,""));
    }

    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {  
        /*  Replenish case logic to return the slot used for replenishment */
        /*  If no slot is currently used, set new slot to YES              */
        misTrc(T_FLOW, "##### Palatizing replenishment pick. #####");
        strcpy(ship_id, " ") ;
        strcpy(cas_rem, "REPLEN");
	
 	sprintf(buffer,
	"select distinct to_char(poldat.srtseq) slotno  "
	"  from pckwrk, poldat, invlod, invsub, invdtl  "
	" where poldat.polcod = 'VAR'                   "
	"   and poldat.polvar = 'SHIP-STAGE-BUILD-LOAD' "
	"   and poldat.polval = '%s'             " 
	"   and invlod.stoloc = poldat.rtstr1    "
	"   and invlod.lodnum = invsub.lodnum    "
	"   and invsub.subnum = invdtl.subnum    "
	"   and invdtl.ship_line_id is null      "
        "   and invsub.subucc = pckwrk.subucc(+) "
        "   and pckwrk.wkonum is null            ",
	laneloc);

	ret_status = sqlExecStr(buffer, &res) ;
	if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	if (ret_status == eDB_NO_ROWS_AFFECTED) {  
	    new_slot_flag = 1 ;
	    srvResultsAdd(CurPtr, prtnum, cas_rem, est_pal, ship_id, slotno1,
  		slotno2,slotno3,slotno4, slotno5, new_slot_flag, last_case); 
	    return (CurPtr) ;
	}
	
	/* replenish slot found */
	row = sqlGetRow (res) ;
	strcpy (slotno1, sqlGetString(res,row,"slotno")) ;
	sqlFreeResults(res);

	/* see if there are more slots available  */ 
	row = sqlGetNextRow (row) ; 

	if (row) {
	    strcpy (slotno2, sqlGetString(res,row,"slotno")) ;
	    row = sqlGetNextRow (row);

	    if (row) {
		strcpy (slotno3, sqlGetString(res,row,"slotno")) ;
		row = sqlGetNextRow (row);

	    	if (row) {
		    strcpy (slotno4, sqlGetString(res,row,"slotno")) ;
		    row = sqlGetNextRow (row);

		    if (row) {
			strcpy (slotno5, sqlGetString(res,row,"slotno")) ;
			sqlFreeResults(res);
		    }
		}
	    }
	}

	srvResultsAdd(CurPtr, prtnum,	cas_rem, est_pal, ship_id, slotno1,
	slotno2,slotno3,slotno4,slotno5, new_slot_flag, last_case); 
	return (CurPtr) ;
   }
    
    /* its not a replenishment or work order, */
    /* so its safe to pull out shipment id    */ 
    sprintf(buffer,
	    "select distinct shipment_line.ship_id , "
	    "       shipment.carcod   "
	    "  from invsub, invdtl, shipment_line, shipment  "
	    " where invsub.subnum  = invdtl.subnum "
	    "   and invdtl.ship_line_id  = shipment_line.ship_line_id " 
	    "   and shipment_line.ship_id = shipment.ship_id " 
	    "   and invsub.subnum = '%s' " ,
	    subnum );

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    row = sqlGetRow (res) ;
    strcpy (ship_id, sqlGetString(res,row,"ship_id")) ; 
    strcpy (carcod, sqlGetString(res,row,"carcod")) ; 

    /* now if this carcod is found in CARXRF , it is a small package */ 

    sprintf(buffer,
	    " select carcod, dstcar  "
	    "   from carxrf "
	    "  where carcod = '%s' "
	    "    and not exists (select 'x' "
	    "                      from poldat "
	    "                     where polcod = 'VAR' "
	    "                       and polvar = '%s' "
	    "                       and polval = '%s' "
	    "                       and rtstr1 = '%s') ",
	    carcod, POLVAR_FLUID_LOAD_LTL, POLVAL_CARRIERS, carcod);

    ret_status = sqlExecStr(buffer, &res) ;
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    if (ret_status == eOK) {
	row = sqlGetRow (res) ;
	strcpy (dstcar, sqlGetString(res,row,"dstcar")) ;
	sqlFreeResults(res);

	/* its a small package */ 
	/* see if it or any of the part in the shipment  is ticketed */ 

	sprintf(buffer, 
		" select 1  "
		"   from shipment, shipment_line, ord_line, cstmst, cstprq "
		"  where shipment_line.client_id = ord_line.client_id "
		"    and shipment_line.ordnum    = ord_line.ordnum "
		"    and shipment_line.ship_id   = shipment.ship_id  "
		"    and shipment.rt_adr_id      = cstmst.adr_id "
		"    and cstmst.client_id        = cstprq.client_id  "
		"    and cstmst.cstnum           = cstprq.cstnum "
		"    and ord_line.prt_client_id  = cstprq.prt_client_id  "
		"    and ord_line.prtnum         = cstprq.prtnum  "
		"    and shipment_line.ship_id   = '%s' "
		"    and cstprq.vc_pretck is not null " ,
		ship_id ) ;
	ret_status = sqlExecStr(buffer, &res) ;
	if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

	if (ret_status == eDB_NO_ROWS_AFFECTED) {
	    /*   Case does not belong to a ticketed shipment */ 
	    sprintf(buffer,
	    "select  distinct to_char(poldat.srtseq)  slotno   "
	    " from poldat,invlod,invsub,invdtl,shipment_line,shipment,carxrf "
	    "where poldat.rtstr1         = invlod.stoloc "
	    " and invlod.lodnum         = invsub.lodnum "
	    " and invsub.subnum         = invdtl.subnum "
	    " and invdtl.ship_line_id   = shipment_line.ship_line_id  "
	    " and shipment_line.ship_id = shipment.ship_id "
	    " and shipment.carcod       = carxrf.carcod "
	    " and carxrf.dstcar = '%s' "
	    " and poldat.polcod ='VAR' "
	    " and poldat.polvar ='SHIP-STAGE-BUILD-LOAD' "
	    " and poldat.polval = '%s' "
	    " and not exists ( "
	      " select distinct to_char(poldat.srtseq) slotno   "
	      " from poldat pol2nd, invlod, invsub, invdtl, "
	           " shipment_line, shipment, carxrf, cstmst, ord_line, "
		   "cstprq "
	    	" where pol2nd.rtstr1           = invlod.stoloc  "
	    	" and invlod.lodnum         = invsub.lodnum  " 
	    	" and invsub.subnum         = invdtl.subnum  " 
	    	" and invdtl.ship_line_id   = shipment_line.ship_line_id  "
	    	" and shipment_line.ship_id = shipment.ship_id  "  
	    	" and shipment.carcod       = carxrf.carcod "
	    	" and shipment.rt_adr_id    = cstmst.adr_id "
	    	" and shipment_line.client_id = ord_line.client_id "
	    	" and shipment_line.ordnum    = ord_line.ordnum "
	    	" and cstmst.client_id        = cstprq.client_id  "   
	    	" and cstmst.cstnum           = cstprq.cstnum "
	    	" and ord_line.prt_client_id  = cstprq.prt_client_id  "
	    	" and ord_line.prtnum         = cstprq.prtnum  "
	    	" and pol2nd.polcod = 'VAR' "
	    	" and pol2nd.polvar = 'SHIP-STAGE-BUILD-LOAD' "
	    	" and pol2nd.polval = '%s' "
	    	" and cstprq.vc_pretck is not NULL "
	    	" and pol2nd.srtseq=poldat.srtseq) ",
	    	dstcar ,
	    	laneloc, 
	    	laneloc);

		/* St12.1 (Nov 1, 2001) Set PARCEL text */
		strcpy(cas_rem, "PARCEL") ;
	}

	else {
	    /*   Case belongs to a ticketed shipment */ 
	    sprintf(buffer,
		    "select  distinct to_char(poldat.srtseq) slotno   "
		    "from poldat, invlod, invsub, invdtl, shipment_line, "
		    " shipment, carxrf,  cstmst,  ord_line,   cstprq "
		    "where poldat.rtstr1         = invlod.stoloc "
		    "  and invlod.lodnum         = invsub.lodnum " 
		    "  and invsub.subnum         = invdtl.subnum " 
		    "  and invdtl.ship_line_id   = shipment_line.ship_line_id "
		    "  and shipment_line.ship_id = shipment.ship_id "   
		    "  and shipment.carcod       = carxrf.carcod "  
		/* kjh Jun27 fixed was bad join to shipment_line.adr_id */ 
		    "  and shipment.rt_adr_id    = cstmst.adr_id "   
		    "  and shipment_line.client_id = ord_line.client_id "
		    "  and shipment_line.ordnum    = ord_line.ordnum "
		    "  and cstmst.client_id        = cstprq.client_id "    
		    "  and cstmst.cstnum           = cstprq.cstnum "       
		    "  and ord_line.prt_client_id  = cstprq.prt_client_id "
		    "  and ord_line.prtnum         = cstprq.prtnum "
		    "  and poldat.polcod = 'VAR' "
		    "  and poldat.polvar = 'SHIP-STAGE-BUILD-LOAD' "
		    "  and poldat.polval = '%s'    "
		    "  and cstprq.vc_pretck is not NULL ", 
		    laneloc );

		    /* St12.1 (Nov 1, 2001) Set PARCEL text */
		    strcpy(cas_rem, "PAR-TKT") ;
	}

	ret_status = sqlExecStr(buffer, &res) ;

	if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}


	/* St12.1 (Nov 1, 2001) Move where PARCEL text is set
	** strcpy(cas_rem, "PARCEL") ;
	*/

/*	strcpy(ship_id, " ") ;*/
	
	if (ret_status == eDB_NO_ROWS_AFFECTED) {  
	    
	    new_slot_flag = 1 ;
	    srvResultsAdd(CurPtr, prtnum, cas_rem, est_pal, ship_id, slotno1,
			      slotno2,slotno3,slotno4, slotno5, new_slot_flag, last_case); 
	    return (CurPtr) ;
	}


	/* small package slot found */

	row = sqlGetRow (res) ;
	strcpy (slotno1, sqlGetString(res,row,"slotno")) ;
	sqlFreeResults(res);

	/* see if there are more slots available  */ 

	row = sqlGetNextRow (row) ; 

	if (row) {
	    strcpy (slotno2, sqlGetString(res,row,"slotno")) ;
	    row = sqlGetNextRow (row);

	    if (row) {
		strcpy (slotno3, sqlGetString(res,row,"slotno")) ;
		row = sqlGetNextRow (row);

	    	if (row) {
		    strcpy (slotno4, sqlGetString(res,row,"slotno")) ;
		    row = sqlGetNextRow (row);

		    if (row) {
			strcpy (slotno5, sqlGetString(res,row,"slotno")) ;
			sqlFreeResults(res);
		    }
		}
	    }
	}

	srvResultsAdd(CurPtr, prtnum, cas_rem, est_pal,	ship_id, slotno1,
			  slotno2,slotno3,slotno4, slotno5,	new_slot_flag, last_case); 
	return (CurPtr) ;
    }

    /*  We have the ship_id 
	and we know that its not a replenish or small package case.  */ 

    sprintf(buffer,
	    " get shipping staging shipment build cases left "
	    "where subnum = '%s' " , 
	    subnum);

    ret_status = srvInitiateCommand (buffer, &returnData);

    if (ret_status !=eOK) {                   
	sqlFreeResults(res);
	return (srvSetupReturn(status,"")) ;
    }

    res = returnData->ReturnedData;
    row = sqlGetRow (res) ;
    sprintf (cas_rem, "%ld", sqlGetLong(res,row,"cas_rem")); 
    sqlFreeResults(res);

    if ( sqlGetLong(res,row,"cas_rem") == 1 ) 
	last_case =1 ;
   
    sprintf(buffer,
	    " get shipment pick estimate  "
	    " WHERE  ship_line_id = '' and ship_id = '%s' | "
	    "[select @estpal  est_pal from dual ] " ,
	    ship_id) ;

    ret_status = srvInitiateCommand (buffer, &returnData);

    if (ret_status !=eOK) {                   
	sqlFreeResults(res);
	return (srvSetupReturn(status,"")) ;
    }

    res = returnData->ReturnedData;
    row = sqlGetRow (res) ;
    sprintf (est_pal, "%ld", sqlGetLong(res,row,"est_pal")); 
    sqlFreeResults(res);
    
    sprintf(buffer,
    "select distinct to_char(poldat.srtseq)  slotno "
    "  from pckwrk, pckmov, shipment_line, invsub s, "
    "       invsub, invdtl, invlod, poldat " 
    " where poldat.polcod ='VAR'                    "
    "   and poldat.polvar ='SHIP-STAGE-BUILD-LOAD'  "
    "   and poldat.polval = '%s'          " 
    "   and invlod.stoloc = poldat.rtstr1 "
    "   and invlod.lodnum = invsub.lodnum "
    "   and invsub.subnum = invdtl.subnum "
    "   and invdtl.ship_line_id  = shipment_line.ship_line_id " 
    "   and shipment_line.ship_id = '%s'  " 
    "   and s.subnum = '%s'               "
    "   and s.subucc = pckwrk.subucc      "
    "   and pckwrk.cmbcod = pckmov.cmbcod "
    "   and pckmov.seqnum = (select max(pckmov3.seqnum)             "
    "                          from pckmov pckmov3                  "
    "                         where pckmov3.cmbcod = pckwrk.cmbcod) "
    "   and pckmov.stoloc = (select min(pckmov1.stoloc)         "
    "           from pckmov pckmov1, pckwrk pckwrk1, invsub invsub1 "
    "                         where invsub1.lodnum = invlod.lodnum  "
    "                           and invsub1.subucc = pckwrk1.subucc "
    "                           and pckwrk1.cmbcod = pckmov1.cmbcod "
    "                           and pckmov1.seqnum = (select max(pckmov2.seqnum) "
    "                                                   from pckmov pckmov2 "
    "                                                  where pckmov2.cmbcod = pckwrk1.cmbcod)) ",
    laneloc, ship_id, subnum);

    ret_status = sqlExecStr(buffer, &res) ;
    
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    if (ret_status == eDB_NO_ROWS_AFFECTED) {  
	new_slot_flag = 1 ;
	    	
	srvResultsAdd(CurPtr, prtnum,	cas_rem, est_pal,	ship_id, slotno1,
			  slotno2,slotno3,slotno4,slotno5,new_slot_flag, last_case); 
	return (CurPtr) ;
    }

    /*  slot found */

    row = sqlGetRow (res) ;
    strcpy (slotno1, sqlGetString(res,row,"slotno")) ;
    sqlFreeResults(res);

    /* see if there are more slots available  */ 

    row = sqlGetNextRow (row) ; 

    if (row) {
	strcpy (slotno2, sqlGetString(res,row,"slotno")) ;
	row = sqlGetNextRow (row);

	if (row) {
	    strcpy (slotno3, sqlGetString(res,row,"slotno")) ;
	    row = sqlGetNextRow (row);

	    if (row) {
		strcpy (slotno4, sqlGetString(res,row,"slotno")) ;
		row = sqlGetNextRow (row);

		if (row) {
		    strcpy (slotno5, sqlGetString(res,row,"slotno")) ;
		    sqlFreeResults(res);
		}
	    }
	}
    }

    srvResultsAdd(CurPtr, prtnum, cas_rem, est_pal, ship_id, slotno1,
		      slotno2,slotno3,slotno4, slotno5, new_slot_flag, last_case); 
    return (CurPtr) ;
}
