/*#START************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION:  This function recreates the policies and locations
 *  	when a lane is added or modified (for MR1557) -
 *      a) the area code of of a fluid loaded lane is set to FSTG
 *      b) the area code of of a non-fluid loaded lane is set to PALLET 
 *      c) the VAR policy telling the number of slots for the lane 
 *		is used to add the corresponding slot locations. If
 *		this policy is absent, it is readded as a default of 5.
 *      d) the VAR policies that tell the the names of the slots 
 *		are added.
 *#END*************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT 
long *varResetWaveLane (long vc_wave_lane_i,
		        long fluid_lane_i)
{
    char  tmpvar[1000];
    long  vc_wave_lane;
    long  ret_status;
    mocaDataRes *res;
    mocaDataRow *row;
    long num_slots;		/* MR1557 max slots from policy */
    long lane_length;  		/* MR1557 length of lane name */
    long count_lane;   		/* MR1557 check for lane policies */
    char lane_name[STOLOC_LEN+1]; /*MR1557 */
    long fluid_lane = BOOLEAN_FALSE; /*MR1557 flag for changing areacode*/
    long sltidx; 		/* MR1557 index for looping of slots*/

    memset(lane_name,0,sizeof(lane_name));	/* MR1557 */
    misLogInfo(" in varResetWaveLane...vc_wave_lane_i=%ld,fluid_lane_i=%ld",
	vc_wave_lane_i, fluid_lane_i);
    if (vc_wave_lane_i == 0) {
	return (eOK);
    }
    vc_wave_lane = vc_wave_lane_i;
    
    if (fluid_lane_i == 0) {
	fluid_lane=BOOLEAN_FALSE; 	/* must know if fluid for locmst*/
    }
    else {
    	fluid_lane=fluid_lane_i;
    }

    sprintf(tmpvar, "select rtnum1 num_slots from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval = 'NUMBER-OF-SLOTS'");
    ret_status = sqlExecStr(tmpvar,&res);
    if (ret_status != eOK) { /* if policy is absent...add a default of 5*/
	sqlFreeResults(res);
	num_slots=5;
	sprintf(tmpvar, "insert into poldat "
                " (polcod,polvar,polval,srtseq,rtstr1,rtstr2, "
                " rtnum1, rtnum2, moddte, mod_usr_id) "
                " values('VAR', 'SHIP-STAGE-BUILD-LOAD', "
                " 'NUMBER-OF-SLOTS',1,null,null,%ld,null,sysdate,'%s')",
		num_slots,		
		osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID):"");	
	ret_status = sqlExecStr(tmpvar,NULL);
    }
    else {
      row = sqlGetRow(res);
      num_slots = sqlGetLong(res, row, "num_slots");
      sqlFreeResults(res);
    }

    /* if we have any policies already for this lane -- rewrite them*/
    sprintf(tmpvar, "select count(*) count_lane from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval like '%%'||lpad(%ld,3,'0') ", vc_wave_lane);
    ret_status = sqlExecStr(tmpvar,&res);
    if (ret_status != eOK) {
      sqlFreeResults(res);
      misLogInfo(" MR1557dbgReset: prob counting policies...quit");
      return (eOK) ;
    }
    row = sqlGetRow(res);
    count_lane = sqlGetLong(res, row, "count_lane");
    sqlFreeResults(res);

    if(count_lane > 0) {   /* delete the policies so we can rewrite them*/
      sprintf(tmpvar, "select min(polval) lane_id, "
                " length(rtrim(min(polval),'0123456789 ')) lane_len "
                " from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval like '%%'||lpad(%ld,3,'0') ", vc_wave_lane);
      ret_status = sqlExecStr(tmpvar,&res);
      row = sqlGetRow(res);
      lane_length=sqlGetLong(res,row,"lane_len");
      misTrimcpy(lane_name,sqlGetString(res,row,"lane_id"),lane_length);
      sqlFreeResults(res);

      sprintf(tmpvar, "delete from poldat "
                " where polcod='VAR' and polvar='SHIP-STAGE-BUILD-LOAD' "
                " and polval = '%s'||lpad(%ld,3,'0') ", 
		lane_name, vc_wave_lane);
      ret_status = sqlExecStr(tmpvar,NULL);
      if (ret_status != eOK) {
        misLogInfo(" MR1557dbgReset: prob deleting policies");
      }
    }
    else { 
      sprintf(lane_name,"LANE"); /* if no policies, we assume lane name */
      lane_length=4;
    }

    /* if we have any locations already for this lane -- rewrite them*/
    sprintf(tmpvar, "select count(*) count_lane from locmst "
                " where stoloc like '%s'||lpad(%ld,3,'0')||'%%'", 
		lane_name,vc_wave_lane);
    ret_status = sqlExecStr(tmpvar,&res);
    if (ret_status != eOK) {
      sqlFreeResults(res);
      misLogInfo(" MR1557dbgReset: prob counting locations...quit");
      return (ret_status) ;
    }
    row = sqlGetRow(res);
    count_lane = sqlGetLong(res, row, "count_lane");
    sqlFreeResults(res);

    if(count_lane > 0) {   /* delete the locations so we can rewrite them*/
      sprintf(tmpvar, "delete from locmst "
                " where stoloc like '%s'||lpad(%ld,3,'0')||'%%' "
		" and arecod in ('FSTG','PALLET','OPALLET') ",
		lane_name,vc_wave_lane);
      ret_status = sqlExecStr(tmpvar,NULL);
      if (ret_status != eOK) {
        misLogInfo(" MR1557dbgReset: prob deleting locations...continuing");
      }
    }

    /* now we rewrite the LANE location */
    if(fluid_lane) 	/* MR1557 fluid has area code of FSTG */
        sprintf(tmpvar, "insert into locmst "
		"(stoloc,arecod,locsts,velzon,wrkzon, "
		" maxqvl,curqvl,pndqvl,numcnt,useflg,repflg) " 
		" values ('%s'||lpad(%ld,3,'0'),'FSTG','E','A','SHIP', "
		" 99999,0,0,0,1,1) ",
                lane_name,vc_wave_lane);
    else 		/* MR1557 non-fluid has loc area code of PALLET */
        sprintf(tmpvar, "insert into locmst "
		"(stoloc,arecod,locsts,velzon,wrkzon, "
		" maxqvl,curqvl,pndqvl,numcnt,useflg,repflg) " 
		" values ('%s'||lpad(%ld,3,'0'),'PALLET','E','A','SHIP', "
		" 99999,0,0,0,1,1) ",
                lane_name,vc_wave_lane);
    ret_status = sqlExecStr(tmpvar, NULL) ;
    if (ret_status != eOK) {
      return (ret_status);
    }
   
    /* loop through the num_slots */ 
    for (sltidx = 1; sltidx <= num_slots; sltidx++)
    {
      misLogInfo(" MR1557dbgResetMx: vc_wave_lane=%ld,lane_name=%s,sltidx=%ld",
                vc_wave_lane,lane_name,sltidx);
      /* now we rewrite the LANE-slot policy and locations */
      sprintf(tmpvar, "insert into poldat "
        " (polcod,polvar,polval,srtseq,rtstr1,rtstr2, "
        " rtnum1, rtnum2, moddte, mod_usr_id) "
        " values('VAR', 'SHIP-STAGE-BUILD-LOAD', "
        " '%s'||lpad(%ld,3,'0'), %ld, "
        " '%s'||lpad(%ld,3,'0')||'-SLOT'||lpad(%ld,2,'0'), "
        " '%s'||lpad(%ld,3,'0')||'-SLOT'||lpad(%ld,2,'0')||'V', "
        " null,null,sysdate,'%s')",
        lane_name, vc_wave_lane, sltidx,
        lane_name, vc_wave_lane, sltidx,
        lane_name, vc_wave_lane, sltidx,
        osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID):"");
      ret_status = sqlExecStr(tmpvar,NULL);
        
      sprintf(tmpvar, "insert into locmst "	/* the input side */
	"(stoloc,arecod,locsts,velzon,wrkzon, "
	" maxqvl,curqvl,pndqvl,numcnt,useflg,repflg) " 
	" values ('%s'||lpad(%ld,3,'0')||'-SLOT'||lpad(%ld,2,'0'), "
	" 'PALLET','E','A','SHIP', "
	" 99999,0,0,0,1,1) ",
        lane_name,vc_wave_lane,sltidx);
      ret_status = sqlExecStr(tmpvar,NULL);

      sprintf(tmpvar, "insert into locmst "	/* the output side */
	"(stoloc,arecod,locsts,velzon,wrkzon, "
	" maxqvl,curqvl,pndqvl,numcnt,useflg,repflg) " 
	" values ('%s'||lpad(%ld,3,'0')||'-SLOT'||lpad(%ld,2,'0')||'V', "
	" 'OPALLET','E','A','SHIP', "
	" 99999,0,0,0,1,1) ",
        lane_name,vc_wave_lane,sltidx);
      ret_status = sqlExecStr(tmpvar,NULL);
    }
    misLogInfo(" exiting varResetWaveLane...");
    return (eOK) ;
}
