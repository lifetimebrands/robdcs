static const char *rcsid = "$Id: varGetShippingStagingShipmentBuildCasesLeft.c,v 1.6 2004/09/12 15:54:35 prod Exp $"; 
/*#START*********************************************************************** 
 *  McHugh Software International 
 *  Copyright 2001 
 *  Waukesha, Wisconsin,  U.S.A. 
 *  All rights reserved 
 *  DESCRIPTION  :  This function verifies that there is only one shipment 
 *		    connected to the identifier (load or sub).  It then  
 *		    counts the number of cases expected for the shipment 
 *		    vs the number of cases built onto loads so far and  
 *		    returns the number of cases left to be build for the 
 *		    shipment. 
 *#END************************************************************************/ 
 
#include <moca_app.h>
#include <stdio.h> 
#include <applib.h> 
#include <dcsgendef.h> 
#include <dcscolwid.h> 
#include <dcserr.h> 
#include <varerr.h> 
 
LIBEXPORT  
RETURN_STRUCT  *varGetShippingStagingShipmentBuildCasesLeft( char *lodnum_i , 
							     char *subnum_i)  
{ 
    RETURN_STRUCT   *CurPtr, *returnData=NULL; 
    long pck_cas_count; 
    long case_on_cnvr; 
    long case_on_lanes; 
    long case_unpicked; 
    long ret_status ; 
    long status  ; 
    long ship_id_found_flag = 0 ; 
     
    mocaDataRow   *row ; 
    mocaDataRes   *res ; 
 
    char buffer[3500] ; 
    char lodnum[LODNUM_LEN + 1] ; 
    char subnum[SUBNUM_LEN + 1] ; 
    char ship_id[SHIP_ID_LEN + 1] ; 
       
    if (!lodnum_i && !subnum_i)  
    	return(srvSetupReturn(eINVALID_ARGS, "")); 
     
    if (lodnum_i) { 
	misTrimcpy(lodnum, lodnum_i, LODNUM_LEN ); 
 
	sprintf(buffer, 
	    "select distinct shipment_line.ship_id " 
	    " from invlod, invsub, invdtl, shipment_line" 
	    " where invsub.lodnum = invlod.lodnum " 
	    " and invsub.subnum = invdtl.subnum " 
	    " and invdtl.ship_line_id  = shipment_line.ship_line_id " 
	    " and invlod.lodnum = '%s' " , 
	    lodnum ); 
 
	ret_status = sqlExecStr(buffer, &res); 
 
	if (ret_status !=eOK) { 
	    sqlFreeResults(res); 
	    return (srvSetupReturn(ret_status,"")); 
	} 
 
	if (sqlGetNumRows (res) > 1) { 
	    sqlFreeResults(res); 
	    return (srvSetupReturn(eINT_TOO_MANY_SHIP_ID,"")); 
	} 
 
	row = sqlGetRow (res); 
 
	strcpy (ship_id, sqlGetString(res,row,"ship_id")); 
	ship_id_found_flag = 1; 
	sqlFreeResults(res); 
    }  
 
    if (subnum_i) { 
	misTrimcpy(subnum, subnum_i, SUBNUM_LEN ); 
	/* We need to get SHIP_ID only if we could not get it through LODNUM */ 
	if ( ship_id_found_flag == 0) {  
	    sprintf(buffer, 
		"select distinct shipment_line.ship_id " 
		" from invsub, invdtl, shipment_line" 
		" where invsub.subnum	 = invdtl.subnum " 
		" and invdtl.ship_line_id  = shipment_line.ship_line_id " 
		" and  invsub.subnum = '%s' ", 
		subnum ); 
	    ret_status = sqlExecStr(buffer, &res) ; 
 
	    if (ret_status !=eOK) { 
		sqlFreeResults(res); 
		return (srvSetupReturn(ret_status,"")) ; 
	    } 
 
	    if (sqlGetNumRows (res) > 1) { 
		sqlFreeResults(res); 
		return (srvSetupReturn(eINT_TOO_MANY_SHIP_ID,"")); 
	    } 
 
	    row = sqlGetRow (res); 
	    strcpy (ship_id, sqlGetString(res,row,"ship_id")); 
	    sqlFreeResults(res); 
	} 
    }  
 
#if 0 
/* 08/06/2001 Patrick Pape Start 
   Instead of counting the total cases for the shipment and then subtracting 
   out the cases staged and cases slotted, we are going to just count the 
   number of cases not picked plus the number of cases on the conveyor plus 
   the number of cases in LANEXXX locations (not slot or virtual slot locations). 
   This will give us an accurate count of how many cases are remaining. 
*/  
 
    sprintf(buffer, 
	"select nvl(sum( ceil(pckqty/untcas)),0)  pck_cas_count " 
	" from pckwrk " 
	" where ship_id = '%s' " 
	" and lodlvl  = '%s' ", 
	ship_id, 
	LODLVL_SUBLOAD); 
    ret_status = sqlExecStr(buffer, &res) ; 
 
    if (ret_status !=eOK) { 
	sqlFreeResults(res); 
	return (srvSetupReturn(ret_status,"")); 
    } 
 
    row = sqlGetRow (res); 
    pck_cas_count = sqlGetLong(res, row, "pck_cas_count") ; 
 
#endif /* #if 0 */ 
     
    /* Get the number of cases on the conveyor */ 

    sprintf(buffer, 
	"select count (distinct invdtl.subnum)  case_on_cnvr" 
	" from invdtl, shipment_line,invsub, invlod, locmst " 
	"where invdtl.ship_line_id = shipment_line.ship_line_id " 
	" and invdtl.subnum = invsub.subnum " 
	" and invsub.lodnum = invlod.lodnum " 
	" and invlod.stoloc = locmst.stoloc " 
        " and locmst.arecod = 'CNVR'        " 
	" and shipment_line.ship_id||'' = '%s' ", 
	ship_id ); 
 
    ret_status = sqlExecStr(buffer, &res) ; 
 
    if (ret_status !=eOK) { 
	sqlFreeResults(res); 
	return (srvSetupReturn(ret_status,"")) ; 
    } 
 
    row = sqlGetRow (res); 
    case_on_cnvr = sqlGetLong(res, row, "case_on_cnvr") ; 
     
    sqlFreeResults(res); 
 
    /* Get the number of cases in a LANEXXX location */ 
    /* The original one is a high io select. I have
       modified it. If however this affects performance
       adversely, we will revert back to the old one . Raman P 
       Here is the original. 
    sprintf (buffer,
        "select count(distinct invsub.subnum) case_on_lanes     "
        "  from shipment_line, invdtl, invsub, invlod, poldat   "
        " where poldat.polcod = 'VAR'                           "
        "   and poldat.polvar = 'SHIP-STAGE-BUILD-LOAD'         "
        "   and poldat.polval = invlod.stoloc                   "
        "   and invlod.lodnum = invsub.lodnum                   "
        "   and invsub.subnum = invdtl.subnum                   "
        "   and invdtl.ship_line_id = shipment_line.ship_line_id"
        "   and shipment_line.ship_id||'' = '%s'                ",
        ship_id);
    */
    /* Modified by Raman Parthasarathy */
    sprintf (buffer,
      "select count(distinct invdtl.subnum) case_on_lanes      "
      "  from  poldat, invlod, invsub,  invdtl,   shipment_line        "
      " where shipment_line.ship_id = '%s'                     "
      "   and shipment_line.ship_line_id = invdtl.ship_line_id "
      "   and invdtl.subnum = invsub.subnum                    "
      "   and invsub.lodnum = invlod.lodnum                    "
      "   and invlod.stoloc = poldat.polval                    "
      "   and poldat.polcod = 'VAR'                            "
      "   and poldat.polvar||'' = 'SHIP-STAGE-BUILD-LOAD'      ",
      ship_id);
 
    ret_status = sqlExecStr(buffer, &res) ; 
 
    if (ret_status !=eOK) { 
	sqlFreeResults(res); 
	return (srvSetupReturn(ret_status,"")) ; 
    } 
    row = sqlGetRow (res); 
    case_on_lanes = sqlGetLong(res, row, "case_on_lanes") ; 
 
    /* Get the number of unpicked cases */ 
 
    sprintf( buffer, 
	"select count( distinct pckwrk.subucc ) case_unpicked " 
	" from pckwrk " 
	" where ship_id = '%s' " 
	" and lodlvl = '%s' " 
	" and pckqty > appqty ", 
	ship_id, LODLVL_SUBLOAD ); 
 
    ret_status = sqlExecStr(buffer, &res); 
 
    if (ret_status !=eOK) { 
	sqlFreeResults(res); 
	return (srvSetupReturn(ret_status,"")); 
    } 
 
    row = sqlGetRow (res); 
    case_unpicked = sqlGetLong(res, row, "case_unpicked"); 
 
    /* return  cases left to build */ 
    CurPtr = srvResultsInit(eOK,"cas_rem",COMTYP_INT,10,NULL); 
    srvResultsAdd(CurPtr,case_on_cnvr + case_on_lanes + case_unpicked); 
    return ( CurPtr ) ; 
}
