/*#START***********************************************************************
 *  McHugh Software
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
/* #include "intlib.h"  rp, dt: 03/13/02  */

/*
 **   varAllocateCancelledPicks looks at the canpck table for the cangrp
 **   passed in.  If those entries have not been successfully processed, 
 **   this function will check the cancod table for the settings associated
 **   with the reason code and attempt to reallocate (or not) depending
 **   upon the setttings.
 */
LIBEXPORT 
RETURN_STRUCT *varAllocateCancelledPicks(char *cangrp_i)
{
    long status = eERROR;
    RETURN_STRUCT *CurPtr, *AllocPtr;
    mocaDataRes *res, *ReasonRes;
    mocaDataRow *row, *ReasonRow, *AllocRow;
    char buffer[5000];
    char cangrp[CANGRP_LEN + 1];
    char alctyp[ALCTYP_LEN + 1];
    short attempt_to_allocate;
    long total_allocated;
    
    CurPtr = NULL; 
    
    memset(cangrp, 0, sizeof(cangrp));
    if (cangrp_i && misTrimLen(cangrp_i, CANGRP_LEN) != 0)
    {
	strncpy(cangrp, cangrp_i, misTrimLen(cangrp_i, CANGRP_LEN));
    }

    /* Select everything with that cangrp that has 
      not already been reallocated */
    sprintf(buffer,
	    "select wrkref, wrktyp, "
	    "       NVL(remqty, 0) remqty, cancod "
	    " from canpck "
	    " where cangrp = '%s' ",
	    cangrp);

    status = sqlExecStr(buffer, &res);
    if (status != eOK)
    {
	sqlFreeResults(res);
	CurPtr = (RETURN_STRUCT *) srvSetupReturn(status, "");
	return (CurPtr);
    }

    /* Now, loop through each of them */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	attempt_to_allocate = TRUE;

	/* Check the work type to see if we should try to reallocate, 
	   only try to reallocate if it is a replenishment or pick */
	if (strncmp(sqlGetString(res, row, "wrktyp"),
		    WRKTYP_KIT, WRKTYP_LEN) == 0)
	{
	    attempt_to_allocate = FALSE;
	    misTrc(T_FLOW, 
		   "Work type is not pick or replen, skipping allocate!");
	}

	/* Make sure that there is something left to allocate */
	if (sqlGetLong(res, row, "remqty") <= 0)
	{
	    attempt_to_allocate = FALSE;
	    misTrc(T_FLOW, "canpck.remqty is zero, skipping allocation!");
	}

	/* Check the reason code to see if we should try to reallocate, 
	   the default behavior was to NOT reallocate so if the reason
	   is not on the cancel code master, fall back to the default */
	if (!sqlIsNull(res, row, "cancod"))
	{
	    sprintf(buffer,
		    "select * from cancod "
		    " where codval = '%s' ",
		    sqlGetString(res, row, "cancod"));
	}
	else
	{
	    misTrc(T_FLOW, "cancod not specified, using default cancod!");
	    sprintf(buffer,
		    "select * from cancod "
		    " where defflg = '%ld' "
		    " and rownum = 1 ", BOOLEAN_TRUE);
	}
	status = sqlExecStr(buffer, &ReasonRes);

	memset(alctyp, 0, sizeof(alctyp));
	if (status != eOK)
	{
	    attempt_to_allocate = FALSE;
	    misTrc(T_FLOW, "cancod is missing/no default cancod defined!");
	    misTrc(T_FLOW, "Allocation will NOT be attempted!");
	}
	else
	{
	    ReasonRow = sqlGetRow(ReasonRes);

	    if (sqlGetBoolean(ReasonRes, ReasonRow, "reaflg") != BOOLEAN_TRUE)
	    {
		misTrc(T_FLOW, "cancod.reaflg != 1, skipping allocation!");
		attempt_to_allocate = FALSE;
	    }
	    else
	    {
		strncpy(alctyp, sqlGetString(ReasonRes, ReasonRow, "alctyp"),
			ALCTYP_LEN);
		misTrim(alctyp);
	    }
	}
	sqlFreeResults(ReasonRes);

	if (attempt_to_allocate)
	{
	    misTrc(T_FLOW, 
		   "Calling allocate - wrkref: %s, cangrp: %s, alctyp: %s",
		   sqlGetString(res, row, "wrkref"),
		   cangrp, alctyp);

	    sqlSetSavepoint("before_realloc_activity");
	    sprintf(buffer,
		    " [ select * from canpck "
		    "    where wrkref = '%s' "
		    "      and cangrp = '%s' ] "
		    " |  "
		    " if (@wrktyp = 'P')"
		    " { "
		    "     if (@ordnum) "
		    "     { "
		    "         [ select untpal, untcas, untpak, "
		    "                 orgcod, lotnum, revlvl, "
		    "                 splflg, stdflg, NULL xdkflg, ordinv "
		    "            from ord_line ol"
		    "           where ol.client_id = @client_id "
		    "             and ol.ordnum = @ordnum "
		    "             and ol.ordlin = @ordlin "
		    "             and ol.ordsln = @ordsln ] "
		    "     }"
		    "     else "
		    "     { "
		    "         if (@wkonum) "
		    "         { "
		    "             [select untcas, untpak, orgcod, lotnum, "
		    "                     revlvl, NULL xdkflg "
		    "                from wkodtl "
		    "               where wkonum = @wkonum "
		    "                 and wkorev = @wkorev "
		    "                 and wkolin = @wkolin "
		    "                 and client_id = @client_id ] "
		    "         } "
		    "     } "
		    " }"
		    " | [select @srcloc save_location, useflg oldflg "
		    "      from locmst "
		    "     where stoloc = @srcloc ] "
                    /* MR 2098, Raman P, Dt: 03/23/02 
                       Check the Held Pick count to decide what the pcksts will be.
                    */
		    " | [select varGetHeldPickCount (@schbat) cntrel from dual] "
                    " | if (@cntrel > 0) {  " 
	            " publish data  "
                    " where pcksts='%s' "
		    "       and srcloc = '' "
		    "       and stoloc = '' "
		    "       and srcare = '' "
                    " } "
                    " else {  "
                    /* End MR 2098, Raman P, dt: 03/23/02 */
                    "   publish data "
		    "     where pcksts = '' "
		    "       and srcloc = '' "
		    "       and stoloc = '' "
		    "       and srcare = '' "
                    " } "
		    " | [update locmst "
		    "       set useflg = %ld "
		    "     where stoloc = @save_location ] "
		    " | allocate inventory "
		    "      where pipcod = 'N' "
                    "        and pcktyp = '%s' "
		    "        and segqty = @remqty "
		    "        and pckqty = @remqty "
		    " | [update locmst "
		    "       set useflg = @oldflg "
		    "     where stoloc = @save_location ] ",
		    sqlGetString(res, row, "wrkref"),
		    cangrp,
                    /* MR 2098, Raman P, Dt: 03/23 */
                    PCKSTS_HOLD, 
                    /* End MR 2098, Raman P, Dt: 03/23 */
		    BOOLEAN_FALSE,
		    alctyp);

	    AllocPtr = NULL;
	    misTrc(T_FLOW, "%s", buffer);
	    status = srvInitiateCommand(buffer, &AllocPtr);

	    if (status != eOK)
	    {
		misTrc(T_FLOW, "Allocate Failed (%ld), continuing...", status);
		sqlRollbackToSavepoint("before_realloc_activity");
	    }
	    else
	    {
		/* Spin through the results and sum up the total that 
		   was allocated */
		total_allocated = 0;

		for (AllocRow = sqlGetRow(AllocPtr->ReturnedData);
		     AllocRow; AllocRow = sqlGetNextRow(AllocRow))
		{

		    total_allocated +=
			sqlGetLong(AllocPtr->ReturnedData, AllocRow, "pckqty");

		    /* Publish the wrkref's that were generated along with the 
		       original wrkref */
		    if (!CurPtr)
		    {
			
			CurPtr = srvResultsInit(eOK,
						"oldref", 
						COMTYP_CHAR, WRKREF_LEN,
						"wrkref", 
						COMTYP_CHAR, WRKREF_LEN,
						NULL);
			srvResultsAdd(CurPtr,
				      sqlGetString(res, row, "wrkref"),
				      sqlGetString(AllocPtr->ReturnedData,
						   AllocRow, "wrkref"));
		    }
		    else
		    {
			srvResultsAdd(CurPtr,
				      sqlGetString(res, row, "wrkref"),
				      sqlGetString(AllocPtr->ReturnedData,
						   AllocRow, "wrkref"));

		    }

		    /* Update the canpck table  and decrement 
		     * the remqty by the allocated quantity 
		     */
		    sprintf(buffer,
			    "update canpck "
			    "   set remqty = remqty - %ld "
			    " where wrkref = '%s' "
			    " and cangrp = '%s' ",
			    total_allocated,
			    sqlGetString(res, row, "wrkref"),
			    cangrp);

		    status = sqlExecStr(buffer, NULL);

		    if (status != eOK)
		    {
			misTrc(T_FLOW, 
			       "Failed to update the "
			       "canpck.remqty, aborting!");
			srvFreeMemory(SRVRET_STRUCT, AllocPtr);
			sqlFreeResults(res);
			CurPtr = (RETURN_STRUCT *) srvSetupReturn(status, "");
			return (CurPtr);
		    }
		}
	    }
	    srvFreeMemory(SRVRET_STRUCT, AllocPtr);
	}
	
    }
    sqlFreeResults(res);

    if (!CurPtr) CurPtr = srvSetupReturn(eOK, "");
    return (CurPtr);
}
