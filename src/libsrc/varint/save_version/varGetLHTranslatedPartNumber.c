static const char *rcsid = "$Id: varGetLHTranslatedPartNumber.c,v 1.2 2003/12/03 06:17:45 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2003
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <common.h>
/* #include "intlib.h"  */ 

LIBEXPORT 
RETURN_STRUCT *varGetLHTranslatedPartNumber(char *input_val_i,
                                 char *input_lot_i,
				 char *match_prt_i,
				 char *prt_client_id_i)
{
    long            outrow;
    long ret_status;
    long is_vc_innpck = 0;
    RETURN_STRUCT *CurPtr, *returnHolder;
    char sqlBuffer[500];
    char tmpbuf[500];
    mocaDataRes *Res, *Res2; /* Added Res2 for Mr 5471, Raman P */
    mocaDataRow *Row, *RetRow;
    char input_val[PRTNUM_LEN + UPCCOD_LEN + 1];
    char match_prt[PRTNUM_LEN + 1];
    char comp_prt[PRTNUM_LEN + 1];
    char prt_client_id[CLIENT_ID_LEN + 1];
    char input_lot[LOTNUM_LEN + 1];
    long is_altprt = FALSE;
    long part_found = FALSE;

    char seltyp[DB_COL_NAME_LEN + 1];
    static short loaded;
    static char fldidn[3][FLDIDN_LEN + 1];
    static moca_bool_t sidflg[3];
    char *newselector;
    char *selectSegment;
    char where_clause[2500];
    short loop;

    Res = 0;
    Row = RetRow = 0;
    CurPtr = returnHolder = NULL;

    if (!input_val_i || misTrimLen(input_val_i, PRTNUM_LEN + UPCCOD_LEN) == 0)
	return (srvSetupReturn(eINVALID_ARGS, ""));
    
    if (!input_lot_i || misTrimLen(input_lot_i, LOTNUM_LEN) == 0)
	return (srvSetupReturn(eINVALID_ARGS, ""));

    memset(input_val, 0, sizeof(input_val));
    memset(input_lot, 0, sizeof(input_lot));
    memset(match_prt, 0, sizeof(match_prt));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(where_clause, 0, sizeof(where_clause));
    memset(tmpbuf, 0, sizeof(tmpbuf));

    strncpy(input_val, input_val_i,
	    misTrimLen(input_val_i, PRTNUM_LEN + UPCCOD_LEN));

    strncpy(input_lot, input_lot_i, misTrimLen(input_lot_i, LOTNUM_LEN));

    if (match_prt_i && misTrimLen(match_prt_i, PRTNUM_LEN))
	strncpy(match_prt, match_prt_i, misTrimLen(match_prt_i, PRTNUM_LEN));

    if (prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN)){
	sprintf(where_clause, " and prt_client_id = '%s' ", prt_client_id_i);
	strncpy(prt_client_id, prt_client_id_i,
				misTrimLen(prt_client_id_i, CLIENT_ID_LEN));
    }

    if (!loaded)
    {
	/* Add the client_id if appropriate */
	if (strlen(where_clause)>0){
	    sprintf(tmpbuf, " and client_id = '%s'", prt_client_id);
	    strcat (sqlBuffer, tmpbuf);
	}
	
	for (loop = 0; loop <= 2; loop++)
	{

        /* TODO - Replace commented out logic dealing with varval with new
         * replacement code once new way to store barcode prefixes is determined.
         * Steve Hanchar - 7-Jun-1999
         *
         */
            sprintf(seltyp, loop == 0 ? "prtnum" :
                    (loop == 1 ? "upccod" : "ndccod"));
            sprintf(sqlBuffer,
             "list field identifiers "
		"where var_nam='%s' %s",
		seltyp, tmpbuf);

	    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
            {
		if (CurPtr) srvFreeMemory(SRVRET_STRUCT, CurPtr);
		CurPtr=NULL;
                return ((RETURN_STRUCT *) srvSetupReturn(ret_status, ""));
            }
            if (ret_status == eOK)
            {
		Res = CurPtr->ReturnedData;
                Row = sqlGetRow(Res);
                strncpy(fldidn[loop],sqlGetValue(Res,Row,"fld_idn"),FLDIDN_LEN);
                misTrim(fldidn[loop]);
                sidflg[loop]= sqlGetBoolean(Res, Row, "sid_flg");
            }
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    CurPtr=NULL;
	    Res=NULL;

	    /************************************************
	     * Note: The following logic is/was a patch designed to disable 
	     * the barcode prefix logic until the replacement logic is/was
             * determined.
             *
             * memset (fldidn [loop], 0, FLDIDN_LEN);
             * sidflg [loop] = 'N';
	     ************************************************/
	}
	loaded = TRUE;
    }

    /* New field identifier logic */
    newselector = (char *) calloc(1, strlen(input_val) + 1);
    if ((strlen(fldidn[0])) && (sidflg[0] != BOOLEAN_TRUE) &&
	(strncmp(fldidn[0], input_val, strlen(fldidn[0])) == 0))
	strcpy(newselector, &input_val[strlen(fldidn[0])]);
    else
	strcpy(newselector, input_val);

    /* Just read all matching parts based on part number, UPC, and NDC */
    /* TODO:  The use of prt_client_id may have to be check to see if it was
     *        passed in.  If it was not, return everything and let the calling
     *        program handle it.
    */
    /* If the prt_client_id was passed into the command, use it with part
     * number, upc code and the ndc code.  Otherwise, expect multiple return
     * values.
    */
    sprintf(sqlBuffer,
	    "select prtnum, upccod, ndccod, prt_client_id "
	    "  from prtmst "
	    " where prtnum = '%s' ",
	    newselector);
    
    ret_status = sqlExecStr(sqlBuffer, &Res);

    /*
     * LHCSTART 01.03
     * The UPC on the carton has the vendor number and pack size
     * in front of the part number. A "#" delimits the fields.
     * Strip off everything in front of the "#" and try again.  
     */
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
	selectSegment = strchr(newselector, '#');

	if (selectSegment) 
        {
	    selectSegment++;
	    sprintf(sqlBuffer,
    	    "select prtnum, upccod, ndccod, prt_client_id"
    	    "  from prtmst "
            " where prtnum = '%s' " ,
   	    selectSegment);
            
  	    ret_status = sqlExecStr(sqlBuffer, &Res);
            if (ret_status == eDB_NO_ROWS_AFFECTED)
	    {
                sqlFreeResults(Res);
	        sprintf(sqlBuffer, "select prtmst.prtnum, upccod, ndccod, "
			"  prt_client_id "
    		        "  from prtmst, var_alt_prtnum "
    		        " where altnum = '%s' and vc_altlot = '%s' and prtmst.prtnum = var_alt_prtnum.prtnum", 
		        selectSegment, input_lot);
   	        ret_status = sqlExecStr(sqlBuffer, &Res);
                /* If it is an alternate, then set the flag to TRUE */
                if (ret_status == eOK)
                {
                    is_altprt = TRUE;
                }
	    }
            /* Raman P, MR 5471 */
            else
            {
               part_found = TRUE;
            }
	}
    }
    /* Raman P, For MR 5471 */ 
    else
    {
       part_found = TRUE;
    }
    
   /*
    * Lifetime specific - some part numbers have an alternate. This alternate
    * is maintained in a seperate table. */
   if (is_altprt != TRUE)
   {
       sprintf(sqlBuffer, "select prtmst.prtnum, upccod, ndccod, "
	       "  prt_client_id "
    	       "  from prtmst, var_alt_prtnum "
    	       " where altnum = '%s' and vc_altlot = '%s' and prtmst.prtnum = var_alt_prtnum.prtnum", 
	       newselector, input_lot);
       ret_status = sqlExecStr(sqlBuffer, &Res2);
       if (ret_status == eOK)
       {
          is_altprt = TRUE;
          /* Copy the result set into Res */
          Res = Res2;
       }
       else  /* Not found */
       {
          if (part_found == TRUE)
          {
             ret_status = eOK;
          }
       }
   }
   /* LHCEND 01.03 */

   if (ret_status == eDB_NO_ROWS_AFFECTED)
   {
	sqlFreeResults(Res);
	if ((strlen(fldidn[1])) && (sidflg[1] != BOOLEAN_TRUE) &&
	    (strncmp(fldidn[1], input_val, strlen(fldidn[1])) == 0))
	    strcpy(newselector, &input_val[strlen(fldidn[1])]);
	else
	    strcpy(newselector, input_val);

    /* TODO:  SHOULD THE PRT_CLIENT_ID BE USED HERE???????????
     *        The use of prt_client_id may have to be check to see if it was
     *        passed in.  If it was not, return everything and let the calling
     *        program handle it.
    */
    /* If the prt_client_id was passed into the command, use it with part
     * number, upc code and the ndc code.  Otherwise, expect multiple return
     * values.
    */

	sprintf(sqlBuffer,
		"select prtnum, upccod, ndccod, prt_client_id "
		"  from prtmst "
		" where upccod = '%s' ",
		newselector);
	if (strlen(where_clause)>0)
	    strcat (sqlBuffer, where_clause);

	ret_status = sqlExecStr(sqlBuffer, &Res);
	if (ret_status == eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(Res);
	    if ((strlen(fldidn[2])) && (sidflg[2] != BOOLEAN_TRUE) &&
		(strncmp(fldidn[2], input_val, strlen(fldidn[2])) == 0))
		strcpy(newselector, &input_val[strlen(fldidn[2])]);
	    else
		strcpy(newselector, input_val);

    /* TODO:  SHOULD THE PRT_CLIENT_ID BE USED HERE???????????
     *        The use of prt_client_id may have to be check to see if it was
     *        passed in.  If it was not, return everything and let the calling
     *        program handle it.
    */
    /* If the prt_client_id was passed into the command, use it with part
     * number, upc code and the ndc code.  Otherwise, expect multiple return
     * values.
    */
	    sprintf(sqlBuffer,
		    "select prtnum, upccod, ndccod, prt_client_id "
		    "  from prtmst "
		    " where ndccod = '%s' ",
		    newselector);
	    if (strlen(where_clause)>0)
		strcat (sqlBuffer, where_clause);

	    ret_status = sqlExecStr(sqlBuffer, &Res);
	}
    }
    if (ret_status != eOK)
    {
	sqlFreeResults(Res);
	/* We need to add vc_innpck here*/
	sprintf(sqlBuffer,
	"select prtnum, upccod, ndccod, prt_client_id "
	" from prtmst "
	" where vc_innpck = '%s' ", newselector);
	if (strlen(where_clause)>0)
	    strcat (sqlBuffer, where_clause);
        ret_status = sqlExecStr(sqlBuffer,&Res);
	is_vc_innpck = 1;
	if (ret_status != eOK)
	     {
	     CurPtr = (RETURN_STRUCT *) srvSetupReturn(ret_status, "");
	     return (CurPtr);
	     }
    }

    /* Now loop through and add to the result set if we didn't specify */
    /* a desired matching part number or if we match the requested one */

    /*  If the part client id was not passed in, this will find and return
     *  all the matches b/w the match_prt and the prtnum selected regardless
     *  of the client id.
    **/

    outrow = 0;
    for (Row = sqlGetRow(Res); Row; Row = sqlGetNextRow(Row))
    {
	memset(comp_prt, 0, sizeof(comp_prt));
	strncpy(comp_prt,
		(char *) sqlGetValue(Res, Row, "prtnum"),
	  misTrimLen((char *) sqlGetValue(Res, Row, "prtnum"), PRTNUM_LEN));
	if ((strlen(match_prt) == 0) ||
	    (strlen(match_prt) > 0 &&
	     strcmp(match_prt, comp_prt) == 0))
	{
	    if(outrow == 0)
	    {
	    outrow++;
	    CurPtr = srvResultsInit(eOK,
			     "inpval", COMTYP_CHAR, PRTNUM_LEN + UPCCOD_LEN,
			     "inplot", COMTYP_CHAR, LOTNUM_LEN,
			     "actprt", COMTYP_CHAR, PRTNUM_LEN,
			     "actlot", COMTYP_CHAR, LOTNUM_LEN,
			     "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
			     "vc_innpck", COMTYP_INT, sizeof(long),
			     NULL);

	    }
            if (is_altprt == TRUE)
            {
	        srvResultsAdd(  CurPtr,
			    newselector,
                            input_lot,
			    (char *) sqlGetValue(Res, Row, "prtnum"), 
                            "NOLOT", 
                            (char *) sqlGetValue(Res, Row, "prt_client_id"), 
                            (long) is_vc_innpck);
            }
            else
            {
	        srvResultsAdd(  CurPtr,
			    newselector,
                            input_lot,
			    (char *) sqlGetValue(Res, Row, "prtnum"), 
                            input_lot,
                            (char *) sqlGetValue(Res, Row, "prt_client_id"), 
                            (long) is_vc_innpck);
            }
	}
    }

    sqlFreeResults(Res);
    if (Res2)
    { 
       sqlFreeResults(Res2);
    }
    free(newselector);

    /* We may come through a result set without matching any parts */
    if (!CurPtr)
	CurPtr = (RETURN_STRUCT *) srvSetupReturn(eDB_NO_ROWS_AFFECTED, "");

    return (CurPtr);
}

