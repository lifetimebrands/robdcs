static const char *rcsid = "$Id: varGetNextLabel.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>


#define STX                     2
#define ETX                     3
#define ENQ                     5
#define ACK                     6
#define SEL                     7
#define DLE                     16
#define FF                      12
#define CR                      13
#define NAK                     21
#define ETB                     23
#define CAN                     24
#define EM                      25
#define ESC                     27
#define US                      31
#define SOH                     65

#define DEL                     127

#define RES                     'Z'
#define AFF                     'Y'
#define NEG                     'X'

static void     set_print_i8000(char *cmd);
static void     set_data_i8000(char *cmd, char *data);
static void     set_print_zebra(char *cmd);
static void     set_data_zebra(char *cmd, char *data);

static long	BuildPrinterList(char *prtadr, char *netlistadr);
/*
 * intGetNextLabel will get the next label from the prswrk
 * for the net/ptr combination.
 */
LIBEXPORT 
RETURN_STRUCT *varGetNextLabel(char *prtadr_i, char *fldsep_i, long *lookahead_i)

{
    RETURN_STRUCT  *CurPtr;
    long            ret_status;
    long            i;
    long            lblqty;
    char            buffer[2000];
    char            prtadr[PRTADR_LEN + 1];
    char            format[LBLFMT_LEN + 1];
    char            lbdata[2500];
    char            prttyp[PRTTYP_LEN + 1];
    char            fmtlbl[2500];
    char            datatypes[50];
    char            netadrlist[500];
    long            prsreq;
    mocaDataRes     *res;
    mocaDataRow    *row;

    /* Validate prtadr */

    memset(prtadr, 0, sizeof(prtadr));
    if (prtadr_i && misTrimLen(prtadr_i, PRTADR_LEN))
	strncpy(prtadr, prtadr_i, misTrimLen(prtadr_i, PRTADR_LEN));
    else
	return (srvSetupReturn(eINVALID_ARGS, ""));

    /* Select to see if the printer is re-routed */
    sprintf(buffer,
	    "select 'X' "
	    "from prsmst "
	    "where prtadr = '%s'"
	    "  and rerprt is not null ",
	    prtadr);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
	CurPtr = (RETURN_STRUCT *) srvSetupReturn(eINT_PTR_IS_REROUTED, "");
	return (CurPtr);
    }

   /*
    * Build a list of printer address to use in further selects
    * BuildPrintList is a recursive function that will find the
    * reroute printers for the printer, then the reroute printers for
    * the reroute printers, then the reroute printers for the reroute
    * printers of the reroute printers ...
    */

     memset (netadrlist, 0, sizeof netadrlist);
     sprintf(netadrlist, "'%s'", prtadr);
     ret_status = BuildPrinterList (prtadr, netadrlist);
     if ( (ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED) )
     {
          return srvSetupReturn(ret_status, "");
     }
		
    if (lookahead_i && *lookahead_i)  
    {
	/* Look for the second label */

        sprintf(buffer, " select prsreq from prswrk where 1=2 ");
    }
    else 
    {
	/* Just look for the first one back */
        sprintf(buffer,

	    "  select w.prsreq, w.lblqty, w.lbdata, w.lblfmt, m.prttyp "
	    "    from prswrk w, prsmst m "
	    "   where w.prtadr = m.prtadr "
		"   and prsreq = (select min(pw.prsreq) from prswrk pw "
 		"                   where w.prtadr=pw.prtadr ) " 
		"  and w.prtadr in (%s)",
	    netadrlist);
           
	}

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return srvSetupReturn(ret_status, "");
    }

    row = sqlGetRow(res);

    /* Get next label information */

    memset(prttyp, 0, sizeof(prttyp));
    memset(lbdata, 0, sizeof(lbdata));
    memset(format, 0, sizeof(format));
    lblqty = 0;

    prsreq = sqlGetLong (res, row, "prsreq");
    strncpy(prttyp, misTrim((char *) 
	sqlGetValue(res, row, "prttyp")), 
	PRTTYP_LEN);
    strncpy(lbdata, 
	misTrim((char*)sqlGetValue(res,row,"lbdata")),
	LBDATA_LEN);
    strncpy(format, 
	misTrim((char*)sqlGetValue(res,row,"lblfmt")),
	LBLFMT_LEN);
    lblqty = sqlGetLong(res, row, "lblqty");

    sqlFreeResults(res);

    /* Just a little firewall here */
    if (lblqty <= 0)
	lblqty = 1;

    /* Build the data string for the printer */
    memset(fmtlbl, 0, sizeof(fmtlbl));

    if (strncmp(format, LBLFMT_GENDOC, sizeof (format)) == 0)
    {
        strncpy(fmtlbl, lbdata, sizeof (fmtlbl));
    }
    else if (strncmp(format, LBLFMT_PASDOC, sizeof (format)) == 0)
    {
        strncpy(fmtlbl, lbdata, sizeof (fmtlbl));
    }
    else if (strncmp(prttyp, PRTTYP_INTERMEC, PRTTYP_LEN) == 0)
    {
	set_data_i8000(fmtlbl, lbdata);
	set_print_i8000(fmtlbl);
    }
    else
    {
	set_data_zebra(fmtlbl, lbdata);
	set_print_zebra(fmtlbl);
    }

    /* Setup the Return and let it go */
    memset(datatypes, 0, sizeof(datatypes));

    /* Pass in the prsreq so the cancel can return it and be sure we are 
       deleting the record */
    srvSetupColumns(4);		/* Header indicates x columns */
    i = 0;
    datatypes[i] = srvSetColName(i + 1, "prsreq", COMTYP_INT, sizeof(long));
    i++;
    datatypes[i] = srvSetColName(i + 1, "fmtlbl", COMTYP_CHAR, sizeof(fmtlbl));
    i++;
    datatypes[i] = srvSetColName(i + 1, "format", COMTYP_CHAR, LBLFMT_LEN);
    i++;
    datatypes[i] = srvSetColName(i + 1, "lblqty", COMTYP_INT, sizeof(long));
    i++;

    CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK,
					      datatypes,
					      prsreq,
					      fmtlbl,
					      format,
					      lblqty);

    return (CurPtr);
}
static void set_data_i8000(char *cmd, char *data)
{
    char            tmpstr[50];
    char            temp_mesg[3000];
    char            buffer[3000];
    long            buf_len;
    long            i;
    long            field;

    field = 0;

    memset(buffer, 0, sizeof(buffer));
    strcpy(buffer, data);

    memset(temp_mesg, 0, sizeof(temp_mesg));

    sprintf(tmpstr, "%cF%ld%c", ESC, field, DEL);
    strcat(temp_mesg, tmpstr);
    field++;

    buf_len = strlen(buffer) - 1;
    while ((buf_len > 0) &
	   (buffer[buf_len] != '~'))
    {
	buffer[buf_len] = 0;
	buf_len--;
    }

    buf_len = strlen(buffer);
    for (i = 0; i < buf_len; i++)
    {
	if (buffer[i] == '~')	/* if start of new field  */
	{
	    if (i != buf_len - 1)
	    {
		sprintf(tmpstr, "%cF%ld%c", ESC, field, DEL);
		strcat(temp_mesg, tmpstr);
		field++;
	    }
	}
	else
	{
	    temp_mesg[strlen(temp_mesg)] = buffer[i];
	}
    }

    strcat(cmd, temp_mesg);

    return;
}

static void set_print_i8000(char *cmd)
{
    char            temp_mesg[20];

    memset(temp_mesg, 0, sizeof(temp_mesg));
    temp_mesg[0] = ETB;
    temp_mesg[1] = 12;		/* put out a form feed */
    temp_mesg[2] = ETX;		/* put on an EOT */

    strcat(temp_mesg, "[****]");

    strcat(cmd, temp_mesg);

    return;
}
static void set_data_zebra(char *cmd, char *data)
{
    char            tmpstr[50];
    char            temp_mesg[3000];
    char            buffer[3000];
    long            buf_len;
    long            i;
    long            field;

    field = 1;

    memset(buffer, 0, sizeof(buffer));
    strcpy(buffer, data);

    memset(temp_mesg, 0, sizeof(temp_mesg));

    sprintf(tmpstr, "^FS^FN%ld^FD", field);
    strcat(temp_mesg, tmpstr);
    field++;

    buf_len = strlen(buffer) - 1;
    while ((buf_len > 0) &
	   (buffer[buf_len] != '~'))
    {
	buffer[buf_len] = 0;
	buf_len--;
    }

    buf_len = strlen(buffer);
    for (i = 0; i < buf_len; i++)
    {
	if (buffer[i] == '~')
	{
	    if (i != buf_len - 1)
	    {
		sprintf(tmpstr, "^FS^FN%ld^FD", field);
		strcat(temp_mesg, tmpstr);
		field++;
	    }
	}
	else
	{
	    temp_mesg[strlen(temp_mesg)] = buffer[i];
	}
    }

    strcat(cmd, temp_mesg);

    return;
}
static void set_print_zebra(char *cmd)
{
    char            temp_mesg[20];

    memset(temp_mesg, 0, sizeof(temp_mesg));

    strcat(temp_mesg, "^FS^XZ");

    strcat(cmd, temp_mesg);

    return;
}


/* This is a recursive functino to add the reroute printers wing example, if prtadr is A, then B, C, D, E, F, G are added to
 * the list.  B and C are reroute printers for A.  D is a reroute printer for B.
 * E is a reroute printer for C.  F and G are reroute printers for E.
 * 
 *      prtadr          rerprt
 *      ------          ------
 *      A               NULL
 *      B               A
 *      C               A
 *      D               B
 *      E               C
 *      F               E
 *      G               E
 */


 static long BuildPrinterList (char *prtadr, char *netadrlist)
{

    long        ret_status;

    char        buffer[400];
    char        tmp_prtadr[PRTADR_LEN+1];

    mocaDataRes	*res;
    mocaDataRow *row;

    
    sprintf(buffer,
	"select prtadr from prsmst "
	" where rerprt = '%s' ",
	prtadr);
    
    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return(ret_status);
    }

    for (row = sqlGetRow(res); row; row=sqlGetNextRow(row))
    { 
	misTrimcpy(tmp_prtadr, (char *) sqlGetValue(res,row,"prtadr"),
		PRTADR_LEN);

	/* If not in printer list, put it in the list */
	sprintf(buffer, ",'%s'", tmp_prtadr);
	if (strstr(netadrlist,buffer) == 0)
	{
	    strcat(netadrlist, buffer);
					
	/* Recursive call to add to the list any reroute printers for
	*          * this re-route printer */
	    ret_status = BuildPrinterList (tmp_prtadr, netadrlist);
	    if ( (ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED) )
	    {
	    	sqlFreeResults(res);
	    	return(ret_status);
	    }
        }
    }

    sqlFreeResults(res);
    res = NULL;

    return (eOK);
}

