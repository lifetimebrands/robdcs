static char RCS_Id[] = "$Id: baseCreateAddress.c,v 1.9 2003/05/29 13:16:07 mabini Exp $";
/*#START***********************************************************************
 *
 *  $Source: /cvs/les/sal/src/libsrc/salbase/baseCreateAddress.c,v $
 *  $Revision: 1.9 $
 *  $Author: mabini $
 *
 *  Description:
 *
 *  $Copyright-Start$
 *
 *  Copyright (c) 2002
 *  RedPrairie Corporation
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by RedPrairie Corporation.
 *
 *  RedPrairie Corporation assumes no responsibility for the use of the
 *  software described in this document on equipment which has not been
 *  supplied or approved by RedPrairie Corporation.
 *
 *  $Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mislib.h>
#include <sqllib.h>
#include <srvlib.h>

#include <applib.h>
#include <salerr.h>
#include <salcolwid.h>
#include <salgendef.h>

LIBEXPORT 
RETURN_STRUCT *varCreateAddress(char *adr_id_i,
                                 char *adrtyp_i, 
                                 char *adrnam_i,
                                 char *client_id_i,
                                 char *host_ext_id_i,
                                 char *adrln1_i, 
                                 char *adrln2_i, 
                                 char *adrln3_i,
                                 char *adrcty_i, 
                                 char *adrstc_i, 
                                 char *adrpsz_i,
                                 char *ctry_name_i, 
                                 char *rgncod_i, 
                                 char *phnnum_i,
                                 char *faxnum_i, 
                                 char *attn_name_i, 
                                 char *attn_tel_i, 
                                 char *cont_name_i, 
                                 char *cont_tel_i, 
                                 char *cont_title_i, 
                                 moca_bool_t *rsaflg_i,
                                 moca_bool_t *temp_flg_i, 
                                 char *last_name_i,
                                 char *first_name_i, 
                                 char *honorific_i,
                                 char *adr_district_i,
                                 char *web_adr_i,
                                 char *email_adr_i,
                                 char *pagnum_i,
                                 char *locale_id_i)
{
    RETURN_STRUCT *CurPtr, *TmpPtr;
    char tablename[100];
    char *columnlist;
    char *valuelist;
    char adr_id[ADR_ID_LEN + 1];
    long ret_status;
    long param_count;
    char tmpstr[100];
    char client_id[CLIENT_ID_LEN + 1];
    char buffer[500];

    CurPtr = NULL;

    memset(adr_id, 0, sizeof(adr_id));
    
    param_count = 0;

    /*
     * If an address Id was not passed in, get a new one, since it is a 
     * system generated number.
     */
     
    if (!adr_id_i || misTrimLen(adr_id_i, ADR_ID_LEN) == 0)
    {   
        ret_status = appNextNum(NUMCOD_ADR_ID, adr_id);
        if (ret_status != eOK)
            return (srvResults(ret_status, NULL));
    }   
    else
    {   
        misTrimcpy(adr_id, adr_id_i, ADR_ID_LEN);
    }   
    
    strcpy(tablename, "adrmst");
    columnlist = valuelist = NULL;

    if (eOK != appBuildInsertList("adr_id", adr_id, ADR_ID_LEN, 
                      &columnlist, &valuelist))
    {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvResults(eNO_MEMORY, NULL));
    }

    if (adrnam_i)
    {
        param_count++;

        if (eOK != appBuildInsertList("adrnam", adrnam_i, ADRNAM_LEN, 
                          &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }
    else
    {
        if (columnlist) free(columnlist);

        if (valuelist) free(valuelist);
            /* NOTE!!!  This must be eINVALID_ARGS.  */
            /* Programs are expecting it back in this status */
            return (srvResults(eINVALID_ARGS, NULL));
    }

    if (client_id_i || host_ext_id_i)
    {
        /* Don't increment param_count for the client because it will always 
         * be passed when called from other functions (like create customer).
         */

	/* BRP 4/21/2005 - Set the client id if it is passed in because there
	 * is a chance that if the adrtyp is not passed in the client_id will 
         * not be set correctly.
         */
	if (client_id_i)
	{
	    misTrc(T_FLOW, "Setting Client ID: %s", client_id_i);
	    misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);
	    misTrc(T_FLOW, "Set Client ID to : %s", client_id);

	}

        /* only adrtyp of USR can have client_id = NULL */
        if (adrtyp_i && !strcmp(adrtyp_i, ADRTYP_USR) == 0)
        {
	    misTrc(T_FLOW, "adrtyp: %s", adrtyp_i);
            ret_status = appGetClient(client_id_i, client_id);
        
            /*
             * We have to check for NO_ROWS_AFFECTED, because this routine may
             * have been called from intCreateClient.  In this case, the client
             * record won't exist yet.
             */

            if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
                return (srvResults(ret_status, NULL));
        }
        else
        {
	    misTrc(T_FLOW, "adrtyp is null");
            memset(client_id, 0, sizeof(client_id)) ;

	    /* BRP 4/21/2005 - If the client id was passed in, 
	     * make sure we set it here 
             */
	    if (client_id_i)
	    {
	        misTrc(T_FLOW, "Setting Client ID: %s", client_id_i);
	        misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);
	        misTrc(T_FLOW, "Set Client ID to : %s", client_id);
    
	    }
        }

        if (eOK != appBuildInsertList("client_id", client_id, CLIENT_ID_LEN, 
                          &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if ((host_ext_id_i) && (misTrimLen(host_ext_id_i, HOST_EXT_ID_LEN) > 0))
    {
        /* First, check to make sure there isn't already one out there */

        sprintf(buffer,
                "select 1 "
                "  from adrmst "
                " where host_ext_id = '%s' "
                "   and client_id   = '%s' ",
                host_ext_id_i,
                client_id);

        ret_status = sqlExecStr(buffer, NULL);
        if (eOK == ret_status)   /* One already exists */
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (APPCanNotCreate("tbl_adrmst", "errDupAdrIdx"));
        }
        else if (eDB_NO_ROWS_AFFECTED != ret_status)   
            /* Another error occurred */
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(ret_status, NULL));
        }

        param_count++;

        if (eOK != appBuildInsertList("host_ext_id", host_ext_id_i, 
                                      HOST_EXT_ID_LEN, 
                          &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }
    else
    {
        /* If they're not passing us a host external id, then we're */
        /* going to default the adr_id into this field. This is so that */
        /* Lens can use the host_ext_id to provide visibility to all */
        /* objects, even those that are tied to addresses that did not */
        /* include a host_ext_id value. For example, shipments and 
        /* orders that might have been created using an address that was */
        /* manually entered and did not have a host_ext_id.          */

        param_count++;

        if (eOK != appBuildInsertList("host_ext_id", adr_id,
                                      HOST_EXT_ID_LEN,
                          &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }


    if (adrtyp_i)
    {   
        /*
         * Do not increase the param_count here, because anything intiating
         * create address inline will probably send in and adrtyp, and
         * we really only want to create an address if we have specific address
         * information.
         */

        if (eOK != appBuildInsertList("adrtyp", adrtyp_i, ADRTYP_LEN, 
                          &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adrln1_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrln1", adrln1_i, ADRLN1_LEN, 
                    &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adrln2_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrln2", adrln2_i, ADRLN2_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adrln3_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrln3", adrln3_i, ADRLN3_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adrcty_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrcty", adrcty_i, ADRCTY_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adrstc_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrstc", adrstc_i, ADRSTC_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
    }
    }

    if (adrpsz_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adrpsz", adrpsz_i, ADRPSZ_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (ctry_name_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("ctry_name", ctry_name_i, CTRY_NAME_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (rgncod_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("rgncod", rgncod_i, RGNCOD_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (phnnum_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("phnnum", phnnum_i, PHNNUM_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (faxnum_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("faxnum", faxnum_i, FAXNUM_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (attn_name_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("attn_name", attn_name_i, ATTN_NAME_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (attn_tel_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("attn_tel", attn_tel_i, PHNNUM_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (cont_name_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("cont_name", cont_name_i, CONT_NAME_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (cont_tel_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("cont_tel", cont_tel_i, PHNNUM_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (cont_title_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("cont_title", cont_title_i, CONT_TITLE_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    sprintf(tmpstr, "%d", rsaflg_i ? *rsaflg_i : BOOLEAN_FALSE);
    if (eOK != appBuildInsertList("rsaflg", tmpstr, FLAG_LEN, 
                      &columnlist, &valuelist))
    {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvResults(eNO_MEMORY, NULL));
    }

    /*
     * If the temporary flag is not specified, default it to false.
     */

    if (temp_flg_i)
    {
        sprintf(tmpstr, "%d", *temp_flg_i);
        if (eOK != appBuildInsertList("temp_flg", tmpstr, FLAG_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }
    else
    {
        sprintf (tmpstr, "%d", BOOLEAN_FALSE);
        if (eOK != appBuildInsertList("temp_flg", tmpstr, FLAG_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (last_name_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("last_name", last_name_i, LAST_NAME_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (first_name_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("first_name", first_name_i, 
                                      FIRST_NAME_LEN, 
                             &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (honorific_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("honorific", honorific_i, HONORIFIC_LEN, 
                   &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (adr_district_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("adr_district", adr_district_i, 
                                       ADR_DISTRICT_LEN, 
                           &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (web_adr_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("web_adr", web_adr_i, 
                                           WEB_ADR_LEN, 
                           &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (email_adr_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("email_adr", email_adr_i, 
                                           EMAIL_ADR_LEN, 
                           &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (pagnum_i)
    {
        param_count++;
        if (eOK != appBuildInsertList("pagnum", pagnum_i, 
                                           PHNNUM_LEN, 
                                 &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (locale_id_i)
    {
        /* Make sure it is a valid Locale */
        memset (buffer, 0, sizeof(buffer));
        sprintf(buffer,
                "list locale where locale_id = '%s'",
                locale_id_i);

        TmpPtr = NULL;
        ret_status = srvInitiateInline(buffer, &TmpPtr);
        srvFreeMemory(SRVRET_STRUCT, TmpPtr);
        TmpPtr = NULL;

        if (eOK != ret_status)
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return APPInvalidArg(locale_id_i, "locale_id");
        }

        if (eOK != appBuildInsertList("locale_id", locale_id_i, 
                                           LOCALE_ID_LEN, 
                           &columnlist, &valuelist))
        {
            if (columnlist) free(columnlist);
            if (valuelist) free(valuelist);
            return (srvResults(eNO_MEMORY, NULL));
        }
    }

    if (0 == param_count)
    {
        /* What's an address if there's no information to it??? */
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvResults(eINVALID_ARGS, NULL));
    }

    /*
     * Publish out the table, columns, and values for insert by 
     * intProcessTableInsert (PROCESS TABLE INSERT).
     * We need to make sure to publish the address Id, because several
     * commands may have initiated this command to create an address, and
     * will need the address Id to tie back to the entity that was originally
     * created.
     */

    CurPtr = srvResultsInit(eOK,
                            "acttyp",    COMTYP_CHAR, 1,
                            "tblnam",    COMTYP_CHAR, strlen(tablename),
                            "collst",    COMTYP_CHAR, strlen(columnlist),
                            "vallst",    COMTYP_CHAR, strlen(valuelist),
                            "adr_id",    COMTYP_CHAR, ADR_ID_LEN,
                            NULL);

    srvResultsAdd(CurPtr,
                  ACTTYP_INSERT,
                  tablename,
                  columnlist,
                  valuelist,
                  adr_id);

    free(columnlist);
    free(valuelist);

    return (CurPtr);
}
