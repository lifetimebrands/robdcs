static const char *rcsid = "$Id: varGenerateBILNumber.c,v 1.2 2004/05/23 20:56:47 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <varerr.h>
#include <common.h>

LIBEXPORT 
RETURN_STRUCT *varGenerateBILNumber(char *numcod_i,
                                    char *ship_id_i)
{
    RETURN_STRUCT *CurPtr;

    long  return_status;
    long  ret_status ;
    char  buffer[2000];
    int   seq;
    int   ii;
    int   sum1 = 0; 
    int   sum2 = 0;
    int   totsum = 0;
    int   chksum = 0;
    int   CheckSum;
    char  TempString[3];
    char  temp[100];
    char  numcod[NUMCOD_LEN+1];
    char  doc_num[DOC_NUM_LEN+1];

    mocaDataRes   *res;
    mocaDataRow	  *row;
    RETURN_STRUCT *CmdRes;

/*
    if (!numcod_i || misTrimLen(numcod_i, NUMCOD_LEN) == 0)
	return APPMissingArg("numcod");
*/
    /* 
     * Get the prefix for the Bill of Lading Number 
     * If the ship_id is not passed in, use the policy
     */

    if (!ship_id_i || misTrimLen(ship_id_i, SHIP_ID_LEN) == 0)
    {
        sprintf( buffer,
               " select rtstr1  "
    	       "   from poldat  "
    	       "  where polcod = 'SYSTEM-INFORMATION' " 
    	       "    and polvar = 'FACILITY-PREFIX-CODE' " 
    	       "    and polval = 'PREFIX' ");
    }
    else
    {
        /*
         * Should only be one bill to customer per shipment, but 
         * if this is not the case, we'll grab the min leading character
         */
        sprintf( buffer,
                " select decode(substr(o.btcust,1,1), 'L', '045908', "
                "                                     'H', '024131', "
                "                                     'R', '015388', "
                "                                     '000000')  prefix "
                "   from ord o, shipment_line sl, shipment s "
                "   where o.ordnum = sl.ordnum "
                "     and o.client_id = sl.client_id "
                "     and s.ship_id = sl.ship_id "
                "     and o.btcust is not null "
                "     and s.ship_id = '%s' " 
                "     and rownum < 2 ",
                ship_id_i);
    }

    ret_status = sqlExecStr(buffer, &res) ;
    
    if (ret_status !=eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(eOK,""));
    }

    /* Now get the correct UCC prefix for customer */      
    memset (doc_num, 0, sizeof(doc_num));

    row = sqlGetRow(res);

    if (!ship_id_i || misTrimLen(ship_id_i, SHIP_ID_LEN) == 0)
    {
        strcpy(doc_num, sqlGetString(res, row, "rtstr1") );
    }
    else
    {
        strcpy(doc_num, sqlGetString(res, row, "prefix") );
    }

    /* Get Next Sequential Number */
    sprintf( buffer, " select %s.nextval nextval from dual", numcod_i);
    ret_status = sqlExecStr(buffer, &res) ;
    
    if (ret_status !=eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,""));
    }

    row = sqlGetRow(res);
    seq = sqlGetLong(res, row, "nextval");
    sprintf(TempString,  "%10.10ld", seq);
    strcat(doc_num, TempString );
    sqlFreeResults(res);

    misTrc(T_FLOW, "Docnum=%s, seq=%ld ship_id=%s", doc_num, seq, ship_id_i);

    /* Here is an example...
       company prefix 123456
       serialized number 7890123456
       ....
       uccnum = 1234567890123456
       Step 1) Starting at position 1, add up the numbers in the odd
       numbered positions.  1+3+5+7+9+1+3+5 = 34
       Step 2) Starting at position 2, add up the numbers in the even
       numbered positions. 2+4+6+8+0+2+4+6 = 32
       Step 3) Multiply this result by 3 (96)
       Step 4) Add the results of step 1 and 3. 34 + 96 = 120
       Step 5) The check digit is the smallest number, when added to the 
       value obtained in Step 4 provides a number which is a multiple
       of 10. 120 + ? = 130.
       Step 6) The moculo 10 - check digit for this uccnum is 0. 
       doc_num = 12345678901234560
     */

    /* Step 1 */
    memset(TempString, 0, sizeof(TempString));
    for (ii = 0; ii < ((int)strlen(doc_num) -1); ii = ii + 2)
    {
	TempString[0] = doc_num[ii] ;
	sum1 = sum1 + atoi(TempString);
    }


    /* Step 2 */
    for (ii = 1; ii < (int)strlen(doc_num) ; ii = ii + 2)
    {
	TempString[0] = doc_num[ii] ;
	sum2 = sum2 + atoi(TempString);
    }

    misTrc(T_FLOW, "sum1 = %ld, sum2 = %ld", sum1, sum2);

    /* Step 3 */
    sum2 *= 3;

    /* Step 4 */
    totsum = sum1 + sum2;

    /* Step 5 */
    chksum = (10 - (totsum % 10)) % 10;

    sprintf(temp, "%ld", chksum);
    strcat(doc_num, temp);
    misTrc(T_FLOW, "doc_num = %s", doc_num);

    CmdRes = srvResultsInit(eOK,
                                "nxtnum", COMTYP_CHAR, strlen(doc_num),
				NULL);
    srvResultsAdd(CmdRes,
		      doc_num);
    return(CmdRes);
}
