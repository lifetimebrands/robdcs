/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 Software Architects, Inc.
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varListClosedPmManifests.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application:
 *  Created:
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

//#include "pmlib.h"
/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varListClosedPmManifests.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Id: varListClosedPmManifests.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $
 *
 *  Application:  Intrinsic Library
 *  Created:   31-Jan-1994
 *  $Author: lh51sh $
 *
 *  Purpose:   Structure defs for PMLIB
 *
 *#END************************************************************************/

#ifndef PMLIB_H
#define PMLIB_H

/* This must be included first */

#include <moca_app.h>

/* Standard C header files */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* WM header files */

#include <moca_app.h>
#include <mocaerr.h>
#include <mcscolwid.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcsgendef.h>
#include <applib.h>

/* Parcel Manifest configuration definitions */

#define PM_INVTID_MANUAL	"MANUAL-PACKAGE"
#define PM_MFSTID_SPLIT		"SPLIT-SHIPMENT"
#define PM_SERVICE_DELIMITER	'.'
#define PM_XMIT_INFO_DELIMITER	','

/* Parcel Manifest length definitions */

#define PM_ACTION_CODE_LEN	40
#define PM_FORMAT_CODE_LEN	100
#define PM_PRINTER_CODE_LEN	40
#define PM_SERVICE_CODE_LEN	40
#define PM_STOCK_CODE_LEN       100	

#define PM_BUFFER_LEN		2000
#define PM_CONTEXT_LEN		4000
#define PM_MANIFEST_ID_LEN	MANCLS_LEN  /* 30 */
#define PM_PACKAGE_TYPE_LEN	10
#define PM_PRINTER_PORT_LEN     40	
#define PM_STATUS_TEXT_LEN	80
#define PM_XMIT_COMMAND_LEN	256
#define PM_XMIT_INFO_LIST_LEN	256

/* Parcel Manifest action code definitions */

#define PM_ACTION_ASSIGN_PRINTER		"ASSIGN PRINTER"
#define PM_ACTION_CLOSE_MANIFEST		"CLOSE MANIFEST"
#define PM_ACTION_CREATE_PACKAGE		"CREATE PACKAGE"
#define PM_ACTION_GET_NEXT_TRACKING_NUMBER	"GET NEXT TRACKING NUMBER"
#define PM_ACTION_LIST_ASSIGNED_PRINTERS	"LIST ASSIGNED PRINTERS"
#define PM_ACTION_LIST_CLOSED_MANIFESTS		"LIST CLOSED MANIFESTS"
#define PM_ACTION_LIST_CLOSED_PACKAGES		"LIST CLOSED PACKAGES"
#define PM_ACTION_LIST_COUNTRIES		"LIST COUNTRIES"
#define PM_ACTION_LIST_MANIFEST_FORMATS		"LIST MANIFEST FORMATS"
#define PM_ACTION_LIST_OPEN_MANIFESTS		"LIST OPEN MANIFESTS"
#define PM_ACTION_LIST_OPEN_PACKAGES		"LIST OPEN PACKAGES"
#define PM_ACTION_LIST_PACKAGE_FORMATS		"LIST PACKAGE FORMATS"
#define PM_ACTION_LIST_PRINTER_STOCKS		"LIST PRINTER STOCKS"
#define PM_ACTION_LIST_PRINTERS			"LIST PRINTERS"
#define PM_ACTION_LIST_SERVICE_TRANSLATIONS	"LIST SERVICE TRANSLATIONS"
#define PM_ACTION_LIST_SERVICES			"LIST SERVICES"
#define PM_ACTION_PRODUCE_MANIFEST_DOCUMENT	"PRODUCE MANIFEST DOCUMENT"
#define PM_ACTION_PRODUCE_PACKAGE_LABEL		"PRODUCE PACKAGE LABEL"
#define PM_ACTION_RATE_PACKAGE			"RATE PACKAGE"
#define PM_ACTION_RELEASE_PACKAGE		"RELEASE PACKAGE"
#define PM_ACTION_REMOVE_CLOSED_MANIFEST	"REMOVE CLOSED MANIFEST"
#define PM_ACTION_REMOVE_PACKAGE		"REMOVE PACKAGE"
#define PM_ACTION_TRANSMIT_CLOSED_MANIFEST	"TRANSMIT CLOSED MANIFEST"

/* Parcel Manifest inventory identifier type code definitions */

#define PM_INVTYP_SUBNUM	"subnum"
#define PM_INVTYP_SUBUCC	"subucc"
#define PM_INVTYP_CRTNID	"crtnid"
#define PM_INVTYP_PRTNUM	"prtnum"

/* Parcel Manifest package status code definitions */

#define PM_MANSTS_HELD		MANSTS_HELD
#define PM_MANSTS_RELEASED	MANSTS_RELEASED
#define PM_MANSTS_MANIFESTED	MANSTS_MANIFESTED
#define PM_MANSTS_SHIPPED	MANSTS_SHIPPED

/* Parcel Manifest package type code definitions */

#define PM_PAKTYP_DEFAULT	PM_PAKTYP_CUSTOM

#define PM_PAKTYP_BOX		"BOX"
#define PM_PAKTYP_CUSTOM	"CUSTOM"
#define PM_PAKTYP_LETTER	"LETTER"
#define PM_PAKTYP_PAK		"PAK"
#define PM_PAKTYP_TUBE		"TUBE"

/* Parcel Manifest system policy definitions */

#define PM_POL_COD		"PARCEL-MANIFEST"

#define PM_POL_VAR_CONFIG	"CONFIGURATION"
#define PM_POL_CFG_RMTHST	"PARCEL-REMOTE-HOST"
#define PM_POL_CFG_SHNAME	"PARCEL-SHIPPER-NAME"
#define PM_POL_CFG_SYSNAM	"PARCEL-SYSTEM-NAME"
#define PM_POL_CFG_REFCMD	"PACKAGE-REFERENCE-COMMAND"
#define PM_POL_CFG_WGTMUL	"PACKAGE-WEIGHT-MULTIPLIER"
#define PM_POL_CFG_TRLTYP	"MANIFEST-TRAILER-TYPE"

#define PM_POL_VAR_DEFAULT	"DEFAULT"
#define PM_POL_DEF_DEFCTY	"COUNTRY-CODE"
#define PM_POL_DEF_DEFRPT	"REPORT-PREFIX"

#define PM_POL_CAR_XMIT_INFO	"TRANSMIT-INFORMATION"
#define PM_POL_CAR_XMIT_CMD	"TRANSMIT-COMMAND"

/* Parcel Manifest system mls text definitions */

#define PM_MLS_MIXPRT		"lbl_mixed_part"

/* Parcel Manifest system data structure definitions */

typedef struct {
    long iniflg;
    /**/
    /* Parcel Manifest system information */
    /**/
    char rmthst [RTSTR1_LEN + 1];
    char shname [RTSTR1_LEN + 1];
    char sysnam [RTSTR1_LEN + 1];
    /**/
    /* Parcel package creation information */
    /**/
    char   refcmd [RTSTR1_LEN + 1];
    double wgtmul;
    /**/
    /* Parcel package shipping information */
    /**/
    char trltyp [RTSTR1_LEN + 1];
    /**/
    /* Parcel Manifest system defaults */
    /**/
    char defcty [RTSTR1_LEN + 1];
    char defrpt [RTSTR1_LEN + 1];
    /**/
    /* Parcel Manifest internal operating information */
    /**/
    long mulcar;  /* Multiple DCS carriers mapped to single Parcel Manifest carrier */
    /**/
} PM_CONFIG;

/* Parcel Manifest system data structures */

extern PM_CONFIG pmConfig;

/* Library function prototypes */

long   pmContextStart  (char *contxt);
long   pmContextEnd    (char *contxt);
long   pmContextDouble (char *contxt, double value, char *name);
long   pmContextLong   (char *contxt, long value, char *name);
long   pmContextString (char *contxt, char *value, char *name);
char  *pmFormatDcsCarrierSqlSelectionClause (char *carcod);
char  *pmFormatDcsCarrierSqlSelectionClauseDstcar (char *carcod);
long   pmGetCarrierShipDate (char *shdate, char *dstcar, long movflg);
double pmGetFloat  (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetLong   (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
char  *pmGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetFloatIfSet  (double *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetLongIfSet   (long *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetStringIfSet (char *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);
long   pmGetPackageIdInfo (char *carcod, char *mfsmsn, char *crtnid, char *invtid, char *wrkref, long opnflg);
long   pmGetPolicyInfo (char *polvar, char *polval, char *polsub, char *rtstr1, char *rtstr2, long *rtnum1, long *rtnum2, double *rtflt1, double *rtflt2);
long   pmInitialize (void);
long   pmLogMsg (char *header, char *format, ...);
RETURN_STRUCT *pmReturnRemoteHostError (long status_i);
long   pmTranslateFromCarrierCode (char *carcod, char *service_code);
long   pmTranslateFromServiceCode (char *carcod, char *srvlvl, char *service_code);
long   pmTranslateIntoCarrierCode (char *service_code, char *carcod);
long   pmTranslateIntoServiceCode (char *service_code, char *carcod, char *srvlvl);
long   pmTranslateIntoPackageType (char *package_type, char *paktyp);
long   pmVerifyGuiInitialization (char *carcod);
long   pmVerifyMultipleDcsCarriers (void);

#endif



#define MSG_HDR "DCSpm: (varListClosedPmManifests) "

LIBEXPORT
RETURN_STRUCT *varListClosedPmManifests (char *carcod_i)
{
    RETURN_STRUCT   *returnData;
    RETURN_STRUCT   *resultData;

    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer [PM_BUFFER_LEN];
    char    carcod [CARCOD_LEN + 1];
    char    srvcod [PM_SERVICE_CODE_LEN + 1];

    long    status;


    /* Load all arguments into internal argument structure */

    memset (carcod, 0, sizeof(carcod));
    memset (srvcod, 0, sizeof(srvcod));

    if (carcod_i && strlen (carcod_i))
        misTrimcpy (carcod, carcod_i, sizeof(carcod));

    /* Output informational diagnostic message */

    pmLogMsg (MSG_HDR, "Entering");

    /* Ensure Parcel Manifest system is initialized */

    status = pmInitialize ();
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld initializing Parcel Manifest system", status);
        return (srvResults (status, NULL));
    }

    /* If DCS carrier code is defined, then translate into Parcel Manifest (partial) service code */

    if (strlen (carcod))
    {
	status = pmTranslateIntoCarrierCode (srvcod, carcod);
	if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	{
	    pmLogMsg (MSG_HDR, "Error %ld translating DCS carrier code (%s)", status, carcod);
	    return (srvResults (status, NULL));
	}
	if (status == eDB_NO_ROWS_AFFECTED)
	{

	    /* DCS carrier code could not be translated, so use untranslated value */

	    misTrimcpy (srvcod, carcod, sizeof(srvcod));
	}
    }

    /* Format Parcel Manifest command */

    sprintf (buffer,
	" publish data where service_code = '%s' | get pm config | "
	" get pm custom where action='%s' | "
	" remote (@pm_remote_host) list closed parcel manifests",
	srvcod,
	PM_ACTION_LIST_CLOSED_MANIFESTS);

    /* Invoke Parcel Manifest command */

    status = srvInitiateCommand (buffer, &resultData);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld performing command [%s]", status, buffer);
	srvFreeMemory (SRVRET_STRUCT, resultData);
	return (pmReturnRemoteHostError (status));
    }

    /* Initialize Moca return data set */

    returnData = srvResultsInit (eOK,
	"carcod",   COMTYP_CHAR,    CARCOD_LEN,
	"mancls",   COMTYP_CHAR,    MANCLS_LEN,
	"shpdte",   COMTYP_CHAR,    STD_DATE_ONLY_LEN,
	"xmtflg",   COMTYP_CHAR,    FLAG_LEN,
	NULL);

    /* Move Parcel Manifest information into Moca return data set */

    resultSet = srvGetResults(resultData);
    for (resultRow = sqlGetRow (resultSet); resultRow; resultRow = sqlGetNextRow (resultRow))
    {

	/* If needed, translate DSC carrier code from Parcel Manifest (partial) service code */

        if (carcod_i && strlen (carcod_i))
	{
	    status = eOK;
	}
	else
	{
	    status = pmTranslateFromCarrierCode (
		carcod,
		pmGetString (resultSet, resultRow, "service_code"));
	    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	    {
		pmLogMsg (MSG_HDR, "Error %ld translating Parcel Manifest carrier code (%s)",
		    status, pmGetString (resultSet, resultRow, "service_code"));
		srvFreeMemory (SRVRET_STRUCT, resultData);
		srvFreeMemory (SRVRET_STRUCT, returnData);
		return (srvResults (status, NULL));
	    }
	}

	/* If matching DCS carrier code does not exist, then skip entry */

	if (status == eDB_NO_ROWS_AFFECTED)
	{
	    continue;
	}

	/* Move information into Moca return data set */

	srvResultsAdd (returnData,
	    carcod,
	    pmGetString (resultSet, resultRow, "manifest_id"),
	    pmGetString (resultSet, resultRow, "ship_date"),
	    pmGetString (resultSet, resultRow, "xmit_flag"),
	    NULL);
    }

    /* Free intermediate result data set */

    srvFreeMemory (SRVRET_STRUCT, resultData);

    /* Normal successful completion */

    pmLogMsg (MSG_HDR, "Normal sucessful completion");

    return (returnData);
}

