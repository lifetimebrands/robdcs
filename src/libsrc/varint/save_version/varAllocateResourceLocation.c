static const char *rcsid = "$Id: varAllocateResourceLocation.c,v 1.2 2002/07/10 15:33:30 prod Exp $";
/*#START**********************************************************************
 *  McHugh Software International 
 *  Copyright 2000 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END***********************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT
RETURN_STRUCT *varAllocateResourceLocation(char *stoloc_i, 
				  	   char *ship_id_i,
					   char *wkonum_i,
					   char *wkorev_i,
					   char *client_id_i)
{

    RETURN_STRUCT *CurPtr;
    mocaDataRes *res, *dres;
    mocaDataRow *row, *drow;
    char stoloc[STOLOC_LEN + 1];
    char arecod[ARECOD_LEN + 1];
    char locmstRescod[RESCOD_LEN + 1];
    char colnams[RTSTR1_LEN + 1];
    char ship_id[SHIP_ID_LEN + 1];
    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char client_id[CLIENT_ID_LEN+1];

    char rescod[RESCOD_LEN + 1];
    char sqlBuffer[2000];
    long ret_status;
    short asgflg = FALSE;
    long ignore_errors = 0;
    char *cptr;

    /* MR 3199. Raman Parthasarathy */
    char polval[POLVAL_LEN + 1]; 
    
    CurPtr = NULL;
    

    memset(stoloc, 0, sizeof(stoloc));
    memset(locmstRescod, 0, sizeof(rescod));
    memset(ship_id, 0, sizeof(ship_id));

    /* MR 3199. Raman Parthasarathy */
    memset (polval, 0, sizeof (polval));

    if (!stoloc_i || !misTrimLen(stoloc_i, STOLOC_LEN))
	return APPMissingArg("stoloc");
    
    misTrimcpy(stoloc, stoloc_i, STOLOC_LEN);
    
    if ((!ship_id_i || misTrimLen(ship_id_i, SHIP_ID_LEN) == 0) &&
	(!wkonum_i || misTrimLen(wkonum_i, WKONUM_LEN) == 0))
	return APPMissingArg("ship_id");
    
    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
	misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    if (wkonum_i && misTrimLen(wkonum_i, WKONUM_LEN))
	misTrimcpy(wkonum, wkonum_i, WKONUM_LEN);

    if (wkorev_i && misTrimLen(wkorev_i, WKOREV_LEN))
	misTrimcpy(wkorev, wkorev_i, WKOREV_LEN);

    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
	misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);

    /*
    **  Select arecod from locmst
    **  to determine if the record should be updated
    */
    sprintf(sqlBuffer,
	    "select arecod, rescod "
	    "  from locmst "
	    " where stoloc = '%s' "
	    "   for update of rescod ",
	    stoloc);
    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return APPInvalidArg(stoloc, "stoloc");
    }
    row = sqlGetRow(res);

    if (!sqlIsNull(res, row, "arecod"))
	misTrimcpy(arecod, sqlGetString(res, row, "arecod"), ARECOD_LEN);
    if (!sqlIsNull(res, row, "rescod"))
	misTrimcpy(locmstRescod, sqlGetString(res, row, "rescod"), RESCOD_LEN);

    sqlFreeResults(res);

    /*
    **  Get the rtstr2 value of the RESCOD-AREA-DERIVED policy
    sprintf(sqlBuffer,
	    "list policies "
	    " where polcod = '%s' "
	    "   and polvar = '%s' "
	    "   and polval = '%s' "
	    "   and rtstr1 = '%s' ",
	    POLCOD_ALLOCATE_INV,
            POLVAR_ALLOCINV_RESCOD_DEF,
	    POLVAL_ALLOCINV_RESCOD_DERIVE,
	    arecod);
    */
   
    /* MR 3199. Raman Parthasarathy. Changed standard code
       to look for all policies instead of just Derived policies 
    */
 
    sprintf(sqlBuffer,
	    "list policies "
	    " where polcod = '%s' "
	    "   and polvar = '%s' "
	    "   and rtstr1 = '%s' ",
	    POLCOD_ALLOCATE_INV,
            POLVAR_ALLOCINV_RESCOD_DEF,
	    arecod);

    CurPtr = NULL;
    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	return (srvSetupReturn(eINT_NO_RESCOD_POLICY_EXISTS, ""));
    }

    memset(colnams, 0, sizeof(colnams));
    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);
    if (row && !sqlIsNull(res, row, "rtstr2"))
    {
	strcpy(colnams, sqlGetString(res, row, "rtstr2"));
	strcpy(polval, sqlGetString(res, row, "polval"));
    }
    else
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	misTrc(T_FLOW,"Error getting rtstr2 value of RESCOD-AREA-DERIVED");
	return (srvSetupReturn(eINT_NO_RESCOD_POLICY_EXISTS, ""));
    }
    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    /* MR 3199. Raman Parthasarathy. I am going to add the COMMAND as a
       block of code here. That way my rcsdiff is cleaner. I am including
       only DERIVED as an option here, because at the present time, that
       should suffice for LT. Ideally, this code should handle all 
       variations for the policy. 
    */

    if (strcmp (polval, POLVAL_ALLOCINV_RESCOD_COMMAND) == 0)
    {
       misTrc(T_FLOW,"The res cod type is command ");
       sprintf(sqlBuffer,
        "%s where arecod='%s' and ship_id='%s'  ",
        colnams,  arecod, ship_id);
         
       CurPtr = NULL;
       ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
       if (ret_status != eOK)
       {
           if (CurPtr)
           if (ret_status == eDB_NO_ROWS_AFFECTED)
               ret_status = eINVALID_ARGS;
           return (ret_status);
       }
       else
       {
           res = srvGetResults(CurPtr);

           if (res && (row = sqlGetRow(res)))
               misTrimcpy(rescod, sqlGetValue(res, row, "rescod"),RESCOD_LEN);
           else
               return (eINVALID_ARGS);

           srvFreeMemory(SRVRET_STRUCT, CurPtr);
       } 
    }
   
    /*
    **  Get from shipment or shipment_line the value of the field defined in
    **  rtstr2 of the RESCOD-AREA-DERIVED policy and use
    **  that as the value to update rescod
    */
    if (strcmp (polval, POLVAL_ALLOCINV_RESCOD_DERIVE) == 0)
    {
       misTrc(T_FLOW,"The res cod type is derived ");
       if (strlen(ship_id) != 0)
       {
	   sprintf(sqlBuffer,
	   	   "select * "
	   	   "  from shipment "
		   " where ship_id = '%s' ",
		   ship_id);
	   ret_status = sqlExecStr(sqlBuffer, &res);

	   sprintf(sqlBuffer,
		   "select * "
		   "  from shipment_line "
		   " where ship_id = '%s' "
		   "   and rownum < 2 ",
		   ship_id);
	   ret_status = sqlExecStr(sqlBuffer, &dres);
       }
       else
       {
   	  sprintf(sqlBuffer,
		  "select * "
		  "  from wkohdr "
		  " where wkonum = '%s' "
		  "   and wkorev = '%s' "
		  "   and client_id = '%s' ",
		  wkonum, wkorev, client_id);
	  ret_status = sqlExecStr(sqlBuffer, &res);
	
	  sprintf(sqlBuffer,
		  "select * "
		  "  from wkodtl "
		  " where wkonum = '%s' "
		  "   and wkorev = '%s' "
		  "   and client_id = '%s' "
		  "   and rownum < 2",
		  wkonum, wkorev, client_id);
	
	  ret_status = sqlExecStr(sqlBuffer, &dres);
       }

       row = sqlGetRow(res);
       drow = sqlGetRow(dres);

       memset(rescod, 0, sizeof(rescod));
       for (cptr = strtok(colnams, "-"); cptr; cptr = strtok(NULL, "-"))
       {
	   if (!sqlIsNull(res, row, cptr))
	   {
	      char tmpstr[1000];

	      switch(sqlGetDataType(res, cptr))
	      {
	      case COMTYP_INT:
		  sprintf(tmpstr, "%d", sqlGetLong(res, row, cptr));
		  strcat(rescod, tmpstr);
		  break;
	      case COMTYP_FLOAT:
		  sprintf(tmpstr, "%f", sqlGetFloat(res, row, cptr));
		  strcat(rescod, tmpstr);
		  break;
	      default:
		  strcat(rescod, sqlGetString(res, row, cptr));
		  break;
	      }
	   }
	   else if (!sqlIsNull(dres, drow, cptr))
	   { 
	      char tmpstr[1000];

	      switch(sqlGetDataType(res, cptr))
	      {
	      case COMTYP_INT:
  		  sprintf(tmpstr, "%d", sqlGetLong(res, row, cptr));
		  strcat(rescod, tmpstr);
		  break;
	      case COMTYP_FLOAT:
		  sprintf(tmpstr, "%f", sqlGetFloat(res, row, cptr));
		  strcat(rescod, tmpstr);
		  break;
	      default:
		  strcat(rescod, sqlGetString(res, row, cptr));
		  break;
	      }
	    }
         }
         sqlFreeResults(res);
         sqlFreeResults(dres);
    }

    if (strlen(rescod) == 0)
    {
	return(APPError(eINT_BAD_RESOURCE_CODE));
    }
    /*
     * Now, if the rescod was not null, and it does not match the value that 
     * we were going to update the resource code with, give an error, because
     * this location is already assigned to something else.
     */

    if (strlen(locmstRescod) > 0) 
    {
	if (strncmp(locmstRescod, rescod, RESCOD_LEN) != 0)
            return (srvSetupReturn(eINT_RESCOD_ALREADY_ASSIGNED, ""));
    }
    
    /*
    **  Update the location record. 
    */
    sprintf(sqlBuffer,
	    "update locmst "
	    "   set rescod = '%s'"
	    " where stoloc = '%s' ",
	    rescod, stoloc);

    ret_status = sqlExecStr(sqlBuffer, NULL);

    return ((RETURN_STRUCT *) srvSetupReturn(ret_status, ""));
}
