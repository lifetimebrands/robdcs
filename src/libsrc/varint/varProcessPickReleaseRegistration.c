static char    *rcsid = "$Id: pckProcessPickReleaseRegistration.c,v 1.7 2003/06/26 16:21:07 verdeyen Exp $";
/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "pckRELlib.h"



LIBEXPORT 
RETURN_STRUCT *varProcessPickReleaseRegistration(char *pcksts_i,
						 char *cmbcod_i,
						 char *wrkref_i,
						 char *ship_id_i,
						 moca_bool_t *comflg_i,
                                                 moca_bool_t *skip_rpl_chk_i)
{
    mocaDataRes *res, *res1, *ordres;
    mocaDataRow *row, *row1, *ordrow;
    RETURN_STRUCT *ordCmdRes;
    char buffer[2000];
    char tmpbuf[200];
    char pcksts[PCKSTS_LEN+1];
    char cmbcod[CMBCOD_LEN+1];
    char wrkref[WRKREF_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    long ret_status;
    long commit_as_we_go;

    char rplwrk_buffer[500];
    char whereclause[2000];
    long looking_specific;
    long skipping_rplchk;

    RULES_STRUCT *rules;
    DATA_STRUCT *data;
    
    RETURN_STRUCT *CmdRes;

    skipping_rplchk = 0;
    commit_as_we_go = 0;

    if (comflg_i)
    {
	if (*comflg_i == BOOLEAN_TRUE)    
	    commit_as_we_go = BOOLEAN_TRUE;
    }

    memset(pcksts, 0, sizeof(pcksts));
    memset(cmbcod, 0, sizeof(cmbcod));
    memset(wrkref, 0, sizeof(wrkref));
    memset(ship_id, 0, sizeof(ship_id));

    pckrel_ElapsedMs();
    
    if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
	misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);
    else
	strcpy(pcksts, PCKSTS_PENDING);

    memset(whereclause, 0, sizeof(whereclause));
    if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
    {
	misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
	sprintf(tmpbuf, " and pw.cmbcod = '%s' ", cmbcod);
	strcat(whereclause, tmpbuf);
    }
    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
    {
	misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);
	sprintf(tmpbuf, " and pw.ship_id = '%s' ", ship_id);
	strcat(whereclause, tmpbuf);
    }
    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
    {
	misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);
	sprintf(tmpbuf, " and pw.wrkref = '%s' ", wrkref);
	strcat(whereclause, tmpbuf);
    }
    
    if (strlen(whereclause))
	looking_specific = 1;
    else
	looking_specific = 0;

    ret_status = pckrel_GetConfig(&data, &rules, 0);
    if (ret_status != eOK)
    {
	misTrc(T_FLOW, "Failed to load configuration: %d", ret_status);
	return(srvSetupReturn(ret_status, ""));
    }

    if (misCiStrncmp(rules->relgrp, "NONE", 4) == 0 ||
        (skip_rpl_chk_i && *skip_rpl_chk_i == 1))
    {
        skipping_rplchk = 1;
    }

    memset(rplwrk_buffer, 0, sizeof(rplwrk_buffer));
    if (!skipping_rplchk)
    {
        sprintf(rplwrk_buffer,
                "   and not exists "
                "      (select 1 "
                "         from rplwrk r, shipment_line sl "
                "        where sl.ship_line_id = r.ship_line_id "
                "          and r.rplsts not in ('%s', '%s') "
                "          and sl.ship_id      = sh.ship_id)",
                RPLSTS_WAITING_FOR_WORK_ORDER,
                RPLSTS_CROSS_DOCK);
    }
                

    /*
     * In the below, make sure we are not looking at batches that
     * are in -process.  An issue was encountered in sqlServer when
     * dirty reads were set where, if we didn't ignore those (which we
     * should be anyways), we were grabbing things in the middle of
     * trying to be allocated because it looked like they were ready to
     * go.
     */
    
    sprintf(buffer,
	    "select distinct pw.ship_id, pw.schbat "
	    "  from pckbat pb, pckwrk pw, shipment sh "
	    " where pw.pcksts  = '%s' "
            "   and pb.batsts != '%s' "
            "   and pb.schbat = pw.schbat "
	    "   and pw.ship_id = sh.ship_id "
	    "   %s "
	    "   and sh.alcdte is null "
            "   %s "
	    " group by pw.ship_id, pw.schbat ",
	    pcksts,
            BATSTS_ALLOC_IN_PROC,
	    whereclause,
            rplwrk_buffer);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	return (srvSetupReturn(ret_status, ""));
    }
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
	sqlFreeResults(res);
	pckrel_WriteTrc("There are currently no unregistered "
			"(incomplete alloc) shipments! (ms=%ld) ",
			pckrel_ElapsedMs());
	return(srvSetupReturn(eOK, ""));
    }

   pckrel_WriteTrc("Read the unregistered "
		   "shipments (ms=%ld) - %s",
		   pckrel_ElapsedMs(),
                   skipping_rplchk ? 
                   "replenishments ignored" :
                   "skipped any with replenishments");


    /* Loop through and apply only to replenishments and completely */
    /* allocated shipments */

   CmdRes = srvResultsInit(eOK,
			   "ship_id", COMTYP_CHAR, SHIP_ID_LEN, 
			   "schbat",  COMTYP_CHAR, SCHBAT_LEN, 
			   NULL);

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	long directXdk;
	long rplcnt;

	rplcnt = 0;
	directXdk = FALSE;
	if (rules->PolXDockInstalled)
	{
	    if ((looking_specific == TRUE) &&
		(misTrimLen(wrkref, WRKREF_LEN) > 0))
	    {
		sprintf(buffer,
			"select sh.dstloc "
			"  from pckwrk p, shipment sh, ord_line ol "
			" where p.wrkref    = '%s' "
			"   and sh.dstloc is not null "
			"   and ol.xdkflg   = '%ld' "
			"   and p.ship_id   = sh.ship_id "
			"   and p.client_id = ol.client_id "
			"   and p.ordnum    = ol.ordnum "
			"   and p.ordlin    = ol.ordlin "
			"   and p.ordsln    = ol.ordsln ",
			wrkref, BOOLEAN_TRUE);

		ret_status = sqlExecStr(buffer, NULL);
		if (ret_status != eOK &&
		    ret_status != eDB_NO_ROWS_AFFECTED)
		{
		    sqlFreeResults(res);
		    if (CmdRes)
			srvFreeMemory(SRVRET_STRUCT, CmdRes);

		    return (srvSetupReturn(ret_status, ""));
		}
		if (ret_status == eOK)
		    directXdk = TRUE;
	    }
	}
	if (directXdk == FALSE && !skipping_rplchk)
	{
	    /*  If this shipment is completely allocated, then initiate */
	    sprintf(buffer,
		    "select count(rplwrk.rplref) rplcnt "
		    "  from shipment_line shplin, rplwrk "
		    " where rplwrk.ship_line_id = shplin.ship_line_id "
		    "   and shplin.ship_id = '%s' "
		    "   and rplwrk.rplref not in "
		    "                (select xdkwrk.rplref "
		    "                   from xdkwrk "
		    "                  where xdkwrk.rplref = rplwrk.rplref)",
		    sqlGetString(res, row, "ship_id"));

	    ret_status = sqlExecStr(buffer, &res1);
	    if (ret_status != eOK &&
		ret_status != eDB_NO_ROWS_AFFECTED)
	    {
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		sqlFreeResults(res);
		return (srvSetupReturn(ret_status, ""));
	    }
	    row1 = sqlGetRow(res1);
	    rplcnt = (long) sqlGetFloat(res1, row1, "rplcnt");
	    sqlFreeResults(res1);
	}

	ret_status = eOK;
	if ((rplcnt == 0) &&
	    (rules->PolXDockInstalled) &&
	    (directXdk == FALSE))
	{
	    ret_status = pckrel_ProcessXDock("SHIP_ID", 
					     sqlGetString(res, row, "ship_id"),
					     sqlGetString(res, row, "schbat"),
                                             NULL,
                                             NULL,
                                             NULL,
					     wrkref);
	}
	if ((rplcnt == 0) &&
	    (ret_status == eOK))
	{
	    char srv_command[1000];
	    int addToRes;

	    sprintf(srv_command,
		    "process order fill restrictions "
		    "	where ship_id   = \"%s\" ",
		    sqlGetString(res, row, "ship_id"));

	    ret_status = srvInitiateInline(srv_command, &ordCmdRes);
	    if (ret_status != eOK)
	    {
		pckrel_WriteTrc("Error %ld processing order fill restrictions "
				"for ship %s ",
				ret_status,
				sqlGetString(res, row, "ship_id"));

		sqlFreeResults(res);
		if (ordCmdRes)
		    srvFreeMemory(SRVRET_STRUCT, ordCmdRes);
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return (srvSetupReturn(ret_status, ""));
	    }

          

	addToRes = 1;
	pckrel_WriteTrc("Examining Order Fill Restriction Results");
        ordres = srvGetResults(ordCmdRes);
    	for (ordrow = sqlGetRow(ordres); ordrow; 
		ordrow = sqlGetNextRow(ordrow))
	    if (sqlGetLong(ordres, ordrow, "ship_id_can_release") == 0)
		{
		addToRes = 0;
		pckrel_WriteTrc("Found Release Restriction - Not Releasing");
		}

    	srvFreeMemory(SRVRET_STRUCT, ordCmdRes);

	if (addToRes)
   	    { 
	    sprintf(srv_command,
		    "register shipment allocation complete "
		    "	where ship_id   = \"%s\" "
		    "	  and schbat    = \"%s\" ",
		    sqlGetString(res, row, "ship_id"),
		    sqlGetString(res, row, "schbat"));

	    ret_status = srvInitiateInline(srv_command, NULL);
	    if (ret_status != eOK)
	    {
		pckrel_WriteTrc("Error %ld registering completion of "
				"ship %s ",
				ret_status,
				sqlGetString(res, row, "ship_id"));
		sqlFreeResults(res);
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return (srvSetupReturn(ret_status, ""));
	    }

	    srvResultsAdd(CmdRes, 
			  sqlGetString(res, row, "ship_id"),
			  sqlGetString(res, row, "schbat"));

	    pckrel_WriteTrc("  ...registered alloc completion of "
			    "shipment %s (ms=%ld)",
			    sqlGetString(res, row, "ship_id"),
			    pckrel_ElapsedMs());
	    }
	else
	    pckrel_WriteTrc("  ...NOT Registering "
			    "shipment %s (ms=%ld) due to replens from Line Restriction Logic",
			    sqlGetString(res, row, "ship_id"),
			    pckrel_ElapsedMs());

	    if (commit_as_we_go)
		sqlCommit();
	    
	}
	else if (looking_specific == TRUE)
	{
	    if (ret_status == eERROR)
	    {
		pckrel_WriteTrc("  ...XDocks still exist on "
				"specific read, returning error ...");
		sqlFreeResults(res);
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return(srvSetupReturn(eINT_XDOCK_EXISTS, ""));
	    }
	    else
	    {
		pckrel_WriteTrc("  ...Replenishments still exist on "
				"specific read, returning error ...");
		sqlFreeResults(res);
		if (CmdRes)
		    srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return(srvSetupReturn(eINT_PICK_REL_REPLENS_EXIST, ""));
	    }
	}
    }	
    sqlFreeResults(res);
    return(CmdRes);
}
