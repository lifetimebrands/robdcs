 static const char *rcsid = "$Id: varVerifyPreticketingRequired.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************

 *  SAI Development Systems
 *  Copyright 1993-1997 Software Architects, Inc.
 *  Brookfield, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varVerifyPreticketingRequired.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application:
 *  Created:
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <varcolwid.h>
#include <vargendef.h>        

#define VC_PTKSTK_LEN	20

void *varVerifyPreticketingRequired(char *subucc_i)
{
    RETURN_STRUCT  *CurPtr;
    mocaDataRow    *next = 0;
    mocaDataRes    *ptr;
    long            errcode;
    char            datatypes[20];
    char            buffer[5000], tmpbuf[2000];
    char            Subucc[UCCCOD_LEN + 1];
    char            Subsid[SUBSID_LEN + 1];
    char            Ordnum[ORDNUM_LEN + 1];
    char            Ordlin[ORDLIN_LEN + 1];
    char            Ordsln[ORDSLN_LEN + 1];
    char            Prtnum[PRTNUM_LEN + 1];
    char            Lotnum[LOTNUM_LEN + 1];
    long	    xx, PT;
    char	    Dstare[ARECOD_LEN + 1];
    char	    VarPtkstk[VC_PTKSTK_LEN + 1];
    char	    Colnam[RTSTR1_LEN + 1];
    char	    Datatypes[10];

    memset(Subucc, 0, sizeof(Subucc));
    memset(Subsid, 0, sizeof(Subsid));
    memset(Ordnum, 0, sizeof(Ordnum));
    memset(Ordlin, 0, sizeof(Ordlin));
    memset(Ordsln, 0, sizeof(Ordsln));
    memset(Prtnum, 0, sizeof(Prtnum));
    memset(Lotnum, 0, sizeof(Lotnum));
    memset(Datatypes, 0, sizeof(Datatypes));

    if (subucc_i && misTrimLen(subucc_i, UCCCOD_LEN))
	strncpy(Subucc, subucc_i, misTrimLen(subucc_i, UCCCOD_LEN));

    /*  If we didn't get all the information, return Insufficient Args */
    if (strlen(Subucc) == 0)
	return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    /*  We need to find out if preticketing is required so we can return the
	appropriate response. */

    sprintf(buffer,
	    "select ord_line.ordnum, "
	    "       ord_line.prtnum, "
            "       ord_line.lotnum "
	    "  from pckwrk, ord_line "
	    " where pckwrk.ctnnum = (select pckwrk.subnum from pckwrk "
	    " where pckwrk.subucc = '%s') "
	    "   and pckwrk.ordnum = ord_line.ordnum "
	    "   and pckwrk.ordlin = ord_line.ordlin "
	    "   and pckwrk.ordsln = ord_line.ordsln ",
	    Subucc);
    errcode = sqlExecStr(buffer, &ptr);
    if (errcode == eOK)
    {
    	PT = FALSE;
	for (next = sqlGetRow(ptr); next; next = sqlGetNextRow(next))
	{
	    strncpy(Ordnum, (char *)sqlGetValue(ptr,next,"ordnum"),
		misTrimLen((char *)sqlGetValue(ptr,next,"ordnum"),ORDNUM_LEN));
    	    memset(Prtnum, 0, sizeof(Prtnum));
    	    memset(Lotnum, 0, sizeof(Lotnum));
	    strncpy(Prtnum, (char *)sqlGetValue(ptr,next,"prtnum"),
		misTrimLen((char *)sqlGetValue(ptr,next,"prtnum"),PRTNUM_LEN));
	    strncpy(Lotnum, (char *)sqlGetValue(ptr,next,"lotnum"),
		misTrimLen((char *)sqlGetValue(ptr,next,"lotnum"),LOTNUM_LEN));
            CurPtr = NULL;
            sprintf(buffer,
                    "get preticketing requirements where "
                    "ordnum = \"%s\" and "
                    "prtnum = \"%s\" and "
		    "lotnum = \"%s\" ",
                    Ordnum, Prtnum, Lotnum);
            errcode = srvInitiateCommand(buffer, &CurPtr);
            if (errcode == eOK)
            {
	        next = sqlGetRow(CurPtr->ReturnedData);
    	        memset(VarPtkstk, 0, sizeof(VarPtkstk));
    	        strncpy(VarPtkstk, sqlGetValue(CurPtr->ReturnedData,
			next, "vc_ptkstk"),
		        misTrimLen(sqlGetValue(CurPtr->ReturnedData, 
			next, "vc_ptkstk"), VC_PTKSTK_LEN));
		PT = TRUE;
            }
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
        }
    	sqlFreeResults(ptr);
    }
    else if ( errcode == eDB_NO_ROWS_AFFECTED )
    {
	return(srvSetupReturn(eINT_INVALID_UCCLBL, ""));
    }

    if (PT)
    {
	if (strlen(VarPtkstk)==0)
	    VarPtkstk[0] = '?';
        srvSetupColumns(1);
        xx = 0;
        Datatypes[xx]=srvSetColName(xx+1,"vc_ptkstk",COMTYP_CHAR,
	    VC_PTKSTK_LEN);
        xx++;
        return (srvSetupReturn(eOK, Datatypes, VarPtkstk));
    }
    else
	return (srvSetupReturn(eDB_NO_ROWS_AFFECTED, ""));

}
