static char    *rcsid = "$Id: varListAvailableOrders.c,v 1.7 2002/06/11 15:55:45 devrp Exp $"; 
/*#START*********************************************************************** 
 *  McHugh Software International 
 *  Copyright 2000 
 *  Waukesha, Wisconsin,  U.S.A. 
 *  All rights reserved. 
 * 
 *#END************************************************************************/ 
 
#include <moca_app.h> 
 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
 
#include <dcscolwid.h> 
#include <dcsgendef.h> 
#include <srvconst.h> 
#include <dcserr.h> 
#include <common.h> 

 
static mocaDataRes	*ohColumns = NULL; 
static mocaDataRes	*olColumns = NULL; 
     
static long sInitialize() 
{ 
    long ret_status; 
    char buffer[1000]; 
 
    if (!ohColumns) 
    { 
	sprintf(buffer,  
		" select * " 
		"   from ord " 
		"  where 1 = 2 "); 
 
	ret_status = sqlExecStr(buffer, &ohColumns); 
	if (ret_status != eDB_NO_ROWS_AFFECTED) 
	{ 
	    sqlFreeResults(ohColumns); 
	    ohColumns = NULL; 
	    return (ret_status); 
	} 
    } 
     
    if (!olColumns) 
    { 
	sprintf(buffer,  
		" select * " 
		"   from ord_line " 
		"  where 1 = 2 " ); 
 
	ret_status = sqlExecStr(buffer, &olColumns); 
	if (ret_status != eDB_NO_ROWS_AFFECTED) 
	{ 
	    sqlFreeResults(olColumns); 
	    olColumns = NULL; 
	    return (ret_status); 
	} 
    } 
 
    return (eOK); 
} /* End intialize */ 
 
static void sFormatWhere(char *buffer,  
			 char *table, 
			 char *argname,  
			 int  oper,  
			 void *argdata,  
			 char argtype, 
			 char dbType) 
{ 
    char temp1[200]; 
    char temp2[200]; 
    char temp3[200]; 
 
    memset (temp1, 0, sizeof(temp1)); 
    memset (temp2, 0, sizeof(temp2)); 
    memset (temp3, 0, sizeof(temp3)); 
 
    if (appDataToString(argdata, argtype, temp1) != eOK) 
	return; 
 
    /* Convert to date if we need to */ 
    if (dbType == COMTYP_DATTIM) 
    {  
        /*LHCSTART Ira Laksmono July 31, 01 */
        /*Create a temp3, to hold the end of the day enter
         *This way we can get data for the whole day
         */
          sprintf(temp3,
                "to_date(substr('%s', 1,8) ||'235986399','YYYYMMDDHH24MISSSSS')",
                temp1);
        /*LHCEND Ira Laksmono July 31, 01 */         
        sprintf(temp2,  
                "to_date('%s','YYYYMMDDHH24MISS') ", /* Dec27 karenh fixed*/
		temp1); 
        strcpy(temp1, temp2); 
	memset(temp2, 0, sizeof(temp2)); 
    } 
 
    sprintf(buffer, " and %s.%s ", table, argname); 
    switch (oper) 
    { 
        case OPR_NOTNULL: 
	    strcpy(temp2, "is not null "); 
	    break; 
        case OPR_ISNULL: 
	    strcpy(temp2, "is null "); 
	    break; 
        case OPR_EQ:  
            /*LHCSTART Ira Laksmono July 31, 01
             *Use between operator if user enter in date data time
             *Original Code:    
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " = '%s' " : " = %s ", temp1); 
             */
            if(argtype == COMTYP_STRING &&  dbType != COMTYP_DATTIM)
               sprintf(temp2, " = '%s' ", temp1);
            /*LH Start Ira Laksmono Oct 10,01 need to trap numeric data type */
            else if (argtype == COMTYP_STRING && dbType == COMTYP_DATTIM)
               sprintf(temp2, "between %s and %s", temp1,temp3);
            else
               sprintf(temp2, " = %s ", temp1);
            /*LHEND  Ira Laksmono Oct 10, 01 */
            /*LHCEND Ira Laksmono July 31, 01*/                      
	    break;  
        case OPR_NE: 
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " != '%s' " : " != %s ", temp1); 
	    break; 
        case OPR_LT: 
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " < '%s' " : " < %s ", temp1); 
	    break; 
        case OPR_LE: 
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " <= '%s' " : " <= %s ", temp1); 
	    break; 
        case OPR_GT: 
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " > '%s' " : " > %s ", temp1); 
	    break; 
        case OPR_GE: 
	    sprintf(temp2, argtype == COMTYP_STRING &&  
		    dbType != COMTYP_DATTIM ? " >= '%s' " : " >= %s ", temp1); 
	    break; 
        case OPR_LIKE: 
	    sprintf(temp2, " like '%s%%' ", temp1); 
	    break; 
    } 
    strcat(buffer, temp2); 
} /* Format Where */ 
 
LIBEXPORT  
RETURN_STRUCT *varListAvailableOrders(char *uc_dtc_only) 
{ 
    long	  ret_status; 
    char	  buffer[10000]; 
    char	  where_list_o[10000]; 
    char          where_list_ol[10000]; 
    /* MR 3277, Raman Parthasarathy */
    char          *arg_ptr;
    long          date_flg = 0; 
    char          table_list [200]; 
    /* End MR 3277 */
    RETURN_STRUCT *CurPtr; 
    /*LHCSTART Ira Laksmono 07/06/01*/ 
    RETURN_STRUCT *returnData=NULL; 
    /*LHCSTOP  Ira Laksmono 07/06/01*/ 
    mocaDataRes *res; 
 
    /* For srvEnumerateArgs */ 
    char	 argname[ARGNAM_LEN + 1]; 
    int 	 oper; 
    void	 *argdata; 
    char	 argtype; 
    SRV_ARGSLIST *CTX; 
 
    memset(where_list_o, 0, sizeof(where_list_o)); 
    memset(where_list_ol, 0, sizeof(where_list_ol)); 
    memset(table_list, 0, sizeof(table_list)); 

    misTrc(T_FLOW, "Ords no_dtc_flg: %s", uc_dtc_only);
    char no_dtc_SQL [300];
    if(strcmp(uc_dtc_only, "1") == 0)
       strcpy(no_dtc_SQL, " and o.btcust not in(select rtstr1 from poldat where polcod = 'USR' and polvar = 'DROP-SHIP' and polval = 'CUSTOMERS') ");
    else
       strcpy(no_dtc_SQL, " and 1 = 1 "); 
     
    misTrc(T_FLOW, "Ords_NODTC_SQL: %s", no_dtc_SQL);
    if (!ohColumns || !olColumns) 
    { 
	ret_status = sInitialize(); 
	if (ret_status != eOK) 
	    return (srvResults(ret_status, NULL)); 
    } 
 
	      
    /* Now we spin through our where clause... */ 
    CTX = NULL; 
    while(eOK == srvEnumerateArgList(&CTX, argname, &oper, &argdata, &argtype)) 
    { 
        buffer[0] = '\0'; 
 
	if (sqlFindColumn(ohColumns, argname) != -1) 
	{ 
	    /* We've found our argument in the ord table */ 
	    sFormatWhere(buffer, "o", argname, oper, argdata, argtype, 
                         sqlGetDataTypeByPos(ohColumns,  
					     sqlFindColumn(ohColumns,  
							   argname))); 
            if (strlen(buffer)) 
                strcat(where_list_o, buffer); 
	} 
	else if (sqlFindColumn(olColumns, argname) != -1) 
	{ 
	    /* We've found our argument in the ord_line table */ 
	    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype, 
                         sqlGetDataTypeByPos(olColumns,  
					     sqlFindColumn(olColumns,  
							   argname))); 
            if (strlen(buffer)) 
                strcat(where_list_ol, buffer); 
	}
        /* MR 3277, Raman P */ 
	else
	{
            /* See if the column is either a from date
             * or a to date
             */
            if (strncmp(argname, "from_", 5) == 0)
            {
                /* change the argname */
                arg_ptr = argname;
                strcpy(argname, arg_ptr + 5);
                oper = OPR_GE;
                date_flg = TRUE;
            }
            else if (strncmp(argname, "to_", 3) == 0)
            {
                arg_ptr = argname;
                strcpy(argname, arg_ptr + 3);
                oper = OPR_LE;
                date_flg = TRUE;

            }
            else
            {

                /* Column is not in either of the 2 tables.
                * Can not use it for selection criteria
                */
                oper = -1;
            }

            if (oper > 0)
            {
                if (sqlFindColumn(ohColumns, argname) != -1)
                {
                    /* We've found our argument in the ord table */
                    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                                sqlGetDataTypeByPos(ohColumns,
                                     sqlFindColumn(ohColumns,
                                                   argname)));
                    if (strlen(buffer))
                            strcat(where_list_o, buffer);
                }
                else if (sqlFindColumn(olColumns, argname) != -1)
                {
                   /* We've found our argument in the ord_line table */
                   sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                            sqlGetDataTypeByPos(olColumns,
                                     sqlFindColumn(olColumns,
                                                   argname)));
                    if (strlen(buffer))
                    strcat(where_list_ol, buffer);
                }
            }
	}
        /* End MR 3277, Raman P */
 
    } 
    srvFreeArgList(CTX); 

    if (date_flg == TRUE)
    {
       sprintf(table_list, " ord o, ord_line ol ");
    }
    else
    {
       sprintf(table_list, " ord_line ol, ord o ");
    }
 
   /* LHCSTART Ira Laksmono, MR#222, 07/06/01 */ 
   /* Get the early and late date of the shipdate and deliverydate */ 
   /* original code     	 
		sprintf(buffer,  
			" select o.* " 
             "  from ord o " 
             " where o.ordnum in " 
             "       (select ol.ordnum " 
             "          from ord_line ol " 
			 "         where ol.ordnum = o.ordnum " 
             "           and ol.client_id = o.client_id " 
             "           and ol.pckqty    > 0  " 
             "            %s) " 
             "            %s " 
             "order by o.ordnum ", 
             where_list_ol  ? where_list_ol : "", 	      
             where_list_o ? where_list_o : ""); 
    	CurPtr = NULL; 
		ret_status = sqlExecStr(buffer, &res); 
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
		{ 
	        sqlFreeResults(res); 
			return (srvSetupReturn(ret_status, "")); 
		} 
		CurPtr = srvAddSQLResults(res, eOK); 
		return (CurPtr); 
    */ 
   /* modified code */    
   /* Dt. Jan 24. Removed vc_extdte from the order select */  
   sprintf(buffer, 
	"[select o.client_id, o.ordnum, o.btcust, o.stcust, o.rtcust, " 
	"        o.bt_adr_id, o.st_adr_id, o.rt_adr_id, o.ordtyp, " 
	"        o.entdte, o.adddte, o.cpotyp, o.cponum, o.cpodte, " 
	"        o.paytrm, o.carflg, o.shipby, o.rrlflg, o.moddte, " 
	"        o.mod_usr_id, o.wave_flg, o.vc_slenam, " 
	"        o.vc_lstbchdte, o.vc_numbch, o.vc_walasn, " 
	"        a.adrnam bill_to_custname, "                            
	"        b.adrnam route_to_custname, "
	"        c.adrnam ship_to_custname, " 
	"        max(ol.early_shpdte) early_shpdte, " 
        "        min(ol.late_shpdte)  late_shpdte, " 
        "        max(ol.early_dlvdte) early_dlvdte, " 
        "        min(ol.late_dlvdte)  late_dlvdte, "
 	"	 vc_splordflg, vc_tckordflg, super_ord_flg, wh_id, "
	"        varGetExtDte(ol.ordnum) vc_extdte  "
	"   from adrmst a, "
	"        adrmst b, "
	"        adrmst c, "
        "        %s "
	/* "        ord_line ol, " 
	   "        ord o "  
        */
	"  where a.adr_id = o.bt_adr_id " 
	"    and b.adr_id = o.rt_adr_id " 
	"    and c.adr_id = o.st_adr_id " 
	"    and o.client_id = ol.client_id " 
	"    and o.ordnum = ol.ordnum " 
	"    and o.client_id = '----' " 
        "    and o.cpotyp is null "
	"    and ol.pckqty > 0 " 
	"    %s " 
	"    %s "  
	"    %s "  
	" group by o.client_id, o.ordnum, o.btcust, o.stcust, o.rtcust, " 
	"        o.bt_adr_id, o.st_adr_id, o.rt_adr_id, o.ordtyp, " 
	"        o.entdte, o.adddte, o.cpotyp, o.cponum, o.cpodte, " 
	"        o.paytrm, o.carflg, o.shipby, o.rrlflg, o.moddte, " 
	"        o.mod_usr_id, o.wave_flg, o.vc_slenam, " 
	"        o.vc_lstbchdte, o.vc_numbch, o.vc_walasn, " 
	"        a.adrnam, b.adrnam, c.adrnam, vc_splordflg, vc_tckordflg, super_ord_flg, wh_id, varGetExtDte(ol.ordnum)"
	" order by o.ordnum] ", 
        table_list, 
	where_list_ol  ? where_list_ol : "",
	where_list_o ? where_list_o : "", no_dtc_SQL);			 

	ret_status = srvInitiateCommand (buffer, &returnData); 
	return (returnData); 
} 
 
