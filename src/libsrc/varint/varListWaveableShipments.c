static const char *rcsid = "$Id: varListWaveableShipments.c,v 1.5 2002/05/09 01:50:04 prod Exp $";
/*#START*************************************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION  :  This function is used  by Wave Planning (Creation) to select shipments
 * that can be sent in a Wave.
 *#END***************************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <varcolwid.h>
#include <vargendef.h>

#define DEF_WORK_ORDER_TYPE "I"
#define DEF_WORK_ORDER_TEMPLATE_TYPE "T"

static mocaDataRes	*shColumns = NULL;
static mocaDataRes	*slColumns = NULL;
static mocaDataRes	*ohColumns = NULL;
static mocaDataRes	*olColumns = NULL;
static mocaDataRes	*adColumns = NULL;
static mocaDataRes	*pmColumns = NULL;
    
static long sInitialize(char **work_order_where)
{
    long ret_status;
    char buffer[1000];
    char *work_order_types = NULL;

    mocaDataRes *res;
    mocaDataRow *row;

    if (!shColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from var_wavshipment_view "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &shColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(shColumns);
	    shColumns = NULL;
	    return (ret_status);
	}
    }

    if (!pmColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from prtmst "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &pmColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(pmColumns);
	    pmColumns = NULL;
	    return (ret_status);
	}
    }

    if (!adColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from adrmst "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &adColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(adColumns);
	    adColumns = NULL;
	    return (ret_status);
	}
    }
    
    if (!slColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from shipment_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &slColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(slColumns);
	    slColumns = NULL;
	    return (ret_status);
	}
    }

    if (!ohColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from ord "
		"  where 1 = 2 ");

	ret_status = sqlExecStr(buffer, &ohColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(ohColumns);
	    ohColumns = NULL;
	    return (ret_status);
	}
    }
    
    if (!olColumns)
    {
	sprintf(buffer, 
		" select * "
		"   from ord_line "
		"  where 1 = 2 " );

	ret_status = sqlExecStr(buffer, &olColumns);
	if (ret_status != eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults(olColumns);
	    olColumns = NULL;
	    return (ret_status);
	}
    }

    sprintf(buffer,
            "select rtstr1 "
            " from poldat "
            " where polcod = '%s' "
            "   and polvar = '%s' "
            "   and polval = '%s' ",
            POLCOD_WORK_ORDER_PROC,
            POLVAR_MISC,
            POLVAL_ORDER_TYPE);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        for (row = sqlGetRow(res);row;row = sqlGetNextRow(row))
        {
            sprintf(buffer, "'%s',", 
		    sqlIsNull(res,row,"rtstr1") ? "" :
		    misTrim(sqlGetString(res,row,"rtstr1")));
            misDynStrcat(&work_order_types, buffer);
        }
    }
    sqlFreeResults(res); 
    
    sprintf(buffer,
            "select rtstr1 "
            " from poldat "
            " where polcod = '%s' "
            "   and polvar = '%s' "
            "   and polval = '%s' ",
            POLCOD_WORK_ORDER_PROC,
            POLVAR_MISC,
            POLVAL_TEMPLATE_ORDER_TYPE);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status == eOK)
    {
        for (row = sqlGetRow(res);row;row = sqlGetNextRow(row))
        {
            sprintf(buffer, "'%s',", 
		    sqlIsNull(res,row,"rtstr1") ? "" :
		    misTrim(sqlGetString(res,row,"rtstr1")));
            misDynStrcat(&work_order_types, buffer);
        }
    }
    sqlFreeResults(res); 

    if (work_order_types)
    {
        /* remove last comma */
        work_order_types[strlen(work_order_types)-1] = 0;
        sprintf(buffer, " and o.ordtyp not in (%s) ", work_order_types);  
        misDynStrcat(work_order_where, buffer);
        free(work_order_types);
    }
    else
    {
        sprintf(buffer, 
	        " and o.ordtyp not in ('%s', '%s' ) ",
                DEF_WORK_ORDER_TYPE, 
                DEF_WORK_ORDER_TEMPLATE_TYPE);
	misDynStrcat(work_order_where, buffer);
    }
    return (eOK);
} /* End intialize */

static void sFormatWhere(char *buffer, 
			 char *table,
			 char *argname, 
			 int  oper, 
			 void *argdata, 
			 char argtype,
			 char dbType)
{
    char temp1[200];
    char temp2[200];

    memset (temp1, 0, sizeof(temp1));
    memset (temp2, 0, sizeof(temp2));
	
    misTrc (T_FLOW, "argname = %s", argname);
 
    if (appDataToString(argdata, argtype, temp1) != eOK)
	return;

    /* Convert to date if we need to */
    if (dbType == COMTYP_DATTIM)
    {
        sprintf(temp2, 
	        "to_date('%s')",
		temp1);
        strcpy(temp1, temp2);
	memset(temp2, 0, sizeof(temp2));
    }

    sprintf(buffer, " and %s.%s ", table, argname);
    switch (oper)
    {
        case OPR_NOTNULL:
	    strcpy(temp2, "is not null ");
	    break;
        case OPR_ISNULL:
	    strcpy(temp2, "is null ");
	    break;
        case OPR_EQ:
	    if (argtype == COMTYP_STRING && dbType != COMTYP_DATTIM)
	    	sprintf(temp2," = '%s' ", temp1);
	    else {
		if (!misCiStrcmp(argname, "early_shpdte"))
	    		sprintf(temp2, " >= %s ", temp1);
		else if (!misCiStrcmp(argname, "late_shpdte"))
	    		sprintf(temp2, " <= %s ", temp1);
		else 
	    		sprintf(temp2, " = %s ", temp1);
	    }
	    break;
        case OPR_NE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " != '%s' " : " != %s ", temp1);
	    break;
        case OPR_LT:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " < '%s' " : " < %s ", temp1);
	    break;
        case OPR_LE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " <= '%s' " : " <= %s ", temp1);
	    break;
        case OPR_GT:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " > '%s' " : " > %s ", temp1);
	    break;
        case OPR_GE:
	    sprintf(temp2, argtype == COMTYP_STRING && 
		    dbType != COMTYP_DATTIM ? " >= '%s' " : " >= %s ", temp1);
	    break;
        case OPR_LIKE:
	    sprintf(temp2, " like '%s%%' ", temp1);
	    break;
    }
    strcat(buffer, temp2);
} /* Format Where */

static void sSetExclusions(RETURN_STRUCT *CurPtr)
{
    char buffer[500];
    char tmpbuf[100];
    char selClause[100];
    long ret_status;
    mocaDataRes *res, *dres;
    mocaDataRow *row, *drow;
    char colnam[COLNAM_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char *val;
    char *tmpptr;
    int header;
    int oper; 

    dres = CurPtr->ReturnedData;

    sprintf(buffer, 
	    "select lower(colnam) colnam, colval, client_id "
	    "  from pckexc "
	    " where untdte >= sysdate or untdte is null "
	    " order by colnam ");

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
        return;

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        header = 0;
	memset(colnam, 0, sizeof(colnam));
	misTrimcpy(colnam, sqlGetValue(res, row, "colnam"), COLNAM_LEN);
	    
	memset(client_id, 0, sizeof(client_id));
	if (!sqlIsNull(res, row, "client_id"))
	    strncpy(client_id, 
		    sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

	/* Let's see if our exception is a header or detail exception */
	header = sqlFindColumn(dres, colnam);

	val = sqlGetString(res,row,"colval");
	if (!val)
	    return;
        
	/* if it's a header, we should have the info in our select */
	if (header > 0)
	{
	    if (!sqlIsNull(dres, dres->Data,colnam))
	    {    
	        for (drow = sqlGetRow(dres);
		     drow; drow = sqlGetNextRow(drow))
	        {
		    if (*(sqlGetStringByPos(dres, drow, 0)) != ' ')
		        continue;

		    tmpptr = sqlGetString(dres, drow, colnam);

		    if ((tmpptr) &&
		        (appCmpStringWithWilds(tmpptr, val) == eOK))
		    {
			if (strlen(client_id))
			{
			    tmpptr = sqlGetString(dres, drow, "client_id");
				    
			    /* TODO
			     * if row/value matches pattern,
			     * then set first column (PCKEXC) value to EXCLUDED
			     * Should use new MOCA sqllib function to set
			     * value, when it is available
			     */
			    if (appCmpStringWithWilds(tmpptr, client_id) == eOK)
				*(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED;
			} 
			else
			{
			    /* TODO */
			    *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED;
			}
		    }
		}
	    }
	}

	/* It's a detail exclsuion, we'll need to look it up */
	else
	{
	    /* If we're excluding by detailed exception, the
	       assumption we use is that a client_id specified
	       in the exclusion is associated with the prt_client_id...
	       this may be a problem...and if it is, we probably
	       need to call out the two specific client fields
	       for exclusions...for now, we go with the assumption */
		
	    for (drow = sqlGetRow(dres); drow; drow = sqlGetNextRow(drow))
	    {
		if (*(sqlGetStringByPos(dres, drow, 0)) != ' ')
		    continue;

		memset(tmpbuf, 0, sizeof(tmpbuf));
	        if (strchr(val, '%')) /* This means it's a like */
	            oper = OPR_LIKE;
	        else
	            oper = OPR_EQ;

		if (sqlFindColumn(shColumns, colnam) != -1)
		{
		  /* We've found our argument in the shipment table */
		  sFormatWhere(tmpbuf, "h", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(shColumns,
						   sqlFindColumn(shColumns, 
								 colnam)));
		  sprintf(selClause, "h.%s", colnam);
		}
		else if (sqlFindColumn(slColumns, colnam) != -1)
		{
		  /* We've found our argument in the shipment_line table */
		  sFormatWhere(tmpbuf, "d", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(slColumns, 
						   sqlFindColumn(slColumns, 
								 colnam)));
		  sprintf(selClause, "d.%s", colnam);
		}
		else if (sqlFindColumn(adColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord table */
		  sFormatWhere(tmpbuf, "a", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(adColumns, 
						   sqlFindColumn(adColumns, 
								 colnam)));
		  sprintf(selClause, "a.%s", colnam);
		}
		else if (sqlFindColumn(ohColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord table */
		  sFormatWhere(tmpbuf, "o", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(ohColumns, 
						   sqlFindColumn(ohColumns, 
								 colnam)));
		  sprintf(selClause, "o.%s", colnam);
		}
		else if (sqlFindColumn(pmColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord table */
		  sFormatWhere(tmpbuf, "prtmst", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(ohColumns, 
						   sqlFindColumn(pmColumns, 
								 colnam)));
		  sprintf(selClause, "prtmst.%s", colnam);
		}
		else if (sqlFindColumn(olColumns, colnam) != -1)
		{
		  /* We've found our argument in the ord_liner table */
		  sFormatWhere(tmpbuf, "ol", colnam, oper, val, COMTYP_STRING,
		               sqlGetDataTypeByPos(olColumns, 
						   sqlFindColumn(olColumns, 
								 colnam)));
		  sprintf(selClause, "ol.%s", colnam);
		}
		else
		{
		    /* Column is not in any of the 4 tables.
		     * Can not use it for selection criteria
		     */
		    sprintf(selClause, "'bad_pckexc' ");
		}


		sprintf(buffer, 
		        "select %s "
			" from var_wavshipment_view h, shipment_line d, ord_line ol, ord o"
			" where d.ship_id  = '%s' "
			"   and h.ship_id = d.ship_id "
			"   and d.client_id = o.client_id "
			"   and d.ordnum = o.ordnum       "
			"   and d.client_id = ol.client_id "
			"   and d.ordnum = ol.ordnum       "
			"   and d.ordlin = ol.ordlin "
			"   and d.ordsln = ol.ordsln ",
			selClause, 
			sqlGetString(dres,drow,"ship_id"));
		        
		/* TODO */
	        strcat(buffer, tmpbuf);
	    
	        if (strlen(client_id))
	        {
	            if (strchr(client_id, '%')) sprintf(tmpbuf, 
                         " and prt_client_id like '%s' ", client_id);
	            else
		        sprintf(tmpbuf, " and prt_client_id = '%s' ", 
				client_id);

	            strcat(buffer, tmpbuf);
	        }

  	        ret_status = sqlExecStr(buffer, NULL);
	        if (ret_status == eOK)
	        {
		    /* TODO */
	            *(sqlGetStringByPos(dres, drow, 0)) = EXCLUDED_LINE;
  	        }
            }
	}
    }
    sqlFreeResults(res);
} /* end sFormatExclusions */

LIBEXPORT 
RETURN_STRUCT *varListWaveableShipments(void)
{
    long	  ret_status;
    char	  buffer[10000];
    char	  where_list[10000];
    char          where_list2[10000];
    char	  table_list[200];
    char          table_list2[200];
    char	  field_list[1000];
    char          field_list2[1000];
    RETURN_STRUCT *CurPtr;

    /* For srvEnumerateArgs */
    char	 argname[ARGNAM_LEN + 1];
    int 	 oper;
    void	 *argdata;
    char	 argtype;
    SRV_ARGSLIST *CTX;

    static char		*work_order_where=NULL;

    memset(table_list, 0, sizeof(table_list));
    memset(where_list, 0, sizeof(where_list));
    memset(table_list2, 0, sizeof(table_list2));
    memset(where_list2, 0, sizeof(where_list2));
    
    if (!pmColumns || !shColumns || !slColumns || !ohColumns || !olColumns || !adColumns)
    {
	ret_status = sInitialize(&work_order_where);
	if (ret_status != eOK)
	    return (srvResults(ret_status, NULL));
    }

    /* We have some required fields to pass back */
    /* TODO - should order header fields be returned? */
    
    sprintf(field_list, " h.ship_id, a.adrnam, h.host_client_id, "
	     	       " h.host_ext_id, h.shpsts, h.rt_adr_id, "
	     	       " h.cargrp, h.carcod, "
		       " h.srvlvl, sum(d.pckqty) pckqty, "
                       " h.sddflg, h.doc_num, h.track_num, "
		       " h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	     	       " h.alcdte, h.stgdte, h.loddte, h.entdte, "
	     	       " h.early_shpdte,"
		       " h.late_shpdte, "
	     	       " h.early_dlvdte, h.late_dlvdte, "
		       " nvl (round(sum(d.vc_wavcas_qty)), 0) wavcas_qty, " /*Wave casqty */
		       " to_char(h.vc_plan_shpdte, '%s')  pship_date,  "  /* Planned shpdte */
	     	       " h.dstare, h.dstloc, h.rrlflg, o.btcust, "
		       " decode (o.ordtyp, '%s', '%s', '%s') sample_order, " 
		       /* The small parcel flag just indicates if the
		          shipment has a small parcel carrier.
	               */
		       " h.wave_set, vc_splshipflg, "
			"vc_tckshipflg,  "
		       " usrGetSmallParcel (h.ship_id)  small_parcel,  "
		       " 1  line_type  ",
		       LODCMD_DATE_FORMAT, SAMPLE_ORDER_TYPE, YES_STR, NO_STR
		       );

	     
    /* Do the same for the union */
    sprintf (field_list2, " h.ship_id, a.adrnam, h.host_client_id, "
	     	       " h.host_ext_id, h.shpsts, h.rt_adr_id, "
	     	       " h.cargrp, h.carcod, "
		       " h.srvlvl, sum(d.pckqty) pckqty, "
                       " h.sddflg, h.doc_num, h.track_num, "
	     	       " h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	     	       " h.alcdte, h.stgdte, h.loddte, h.entdte, "
	     	       " h.early_shpdte, "
		       " h.late_shpdte,  "
	     	       " h.early_dlvdte, h.late_dlvdte, "
		       " nvl (round(sum(d.vc_wavcas_qty)), 0) wavcas_qty, " /* Wave casqty */
		       " to_char (h.vc_plan_shpdte, '%s') pship_date,  "   /* Planned shpdte */
	     	       " h.dstare, h.dstloc, h.rrlflg, o.btcust, "
		       " decode (o.ordtyp, '%s', '%s', '%s') sample_order, "
		       /* The small parcel flag just indicates if the
		          shipment has a small parcel carrier.
	               */
		       " h.wave_set, vc_splshipflg, "
			"vc_tckshipflg,  "
		       " usrGetSmallParcel(h.ship_id) small_parcel,  "
		       " 2 line_type  ",
		       LODCMD_DATE_FORMAT, SAMPLE_ORDER_TYPE, YES_STR, NO_STR
                       );

    /* Now we spin through our where clause... */
    CTX = NULL;
    while(eOK == srvEnumerateArgList(&CTX, argname, &oper, &argdata, &argtype))
    {
        buffer[0] = '\0';

	/* Do we have a route code? if so, we need to add the rtemst table*/
	if (!misCiStrcmp(argname, "rtecod"))
	{
	    strcpy(table_list, "rtemst, adrmst, ");
	    strcpy(table_list2, "rtemst, adrmst, ");

	    sprintf(buffer, 
		    " and rtemst.rtecod = '%s' "
		    " and rtemst.strgnc = adrmst.rgncod "
		    " and o.stcust = adrmst.adr_id", 
		    argdata);
        }
	else if (sqlFindColumn(shColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment table */
	    sFormatWhere(buffer, "h", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(shColumns, 
					     sqlFindColumn(shColumns, 
							   argname)));
        }
	else if (sqlFindColumn(pmColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment table */
	    sFormatWhere(buffer, "prtmst", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(pmColumns, 
					     sqlFindColumn(pmColumns, 
							   argname)));
        }
	else if (sqlFindColumn(adColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment_line table */
	    sFormatWhere(buffer, "a", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(adColumns, 
					     sqlFindColumn(adColumns, 
							   argname)));
	}
	else if (sqlFindColumn(slColumns, argname) != -1)
	{
	    /* We've found our argument in the shipment_line table */
	    sFormatWhere(buffer, "d", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(slColumns, 
					     sqlFindColumn(slColumns, 
							   argname)));
	}
	else if (sqlFindColumn(ohColumns, argname) != -1)
	{
	    /* We've found our argument in the ord table */
	    sFormatWhere(buffer, "o", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(ohColumns, 
					     sqlFindColumn(ohColumns, 
							   argname)));
	}
	else if (sqlFindColumn(olColumns, argname) != -1)
	{
	    /* We've found our argument in the ord_liner table */
	    sFormatWhere(buffer, "ol", argname, oper, argdata, argtype,
                         sqlGetDataTypeByPos(olColumns, 
					     sqlFindColumn(olColumns, 
							   argname)));
	}
	else
	{
	    /* Column is not in any of the 4 tables.
	     * Can not use it for selection criteria
	     */
            
	}

	if (strlen(buffer))
	{
	    strcat(where_list, buffer);
	    strcat(where_list2, buffer);
	}
    }
    srvFreeArgList(CTX);
    
    /* now let's build our table list */
    sprintf(buffer, 
	    "ftpmst, prtmst, adrmst a,ord_line ol, ord o, "
	    " shipment_line d, var_wavshipment_view h ");
    strcat(table_list, buffer);
    
    /* Build the list for the union also */
    sprintf(buffer, 
	    "ftpmst, prtmst, adrmst a, ord_line ol, ord o, "
	    " shipment_line d, var_wavshipment_view h  ");
    strcat(table_list2, buffer);
    
    /* AND now the where clause */
    sprintf(buffer, 
	     "      ftpmst.ftpcod = prtmst.ftpcod "
	     "  and prtmst.prtnum = ol.prtnum "
	     "  and a.adr_id = o.bt_adr_id "
	     "  and prtmst.prt_client_id = ol.prt_client_id "
             "  and d.linsts in ('%s', '%s') "
	     "  and d.pckqty    > 0 "
	     "  and d.ship_id   = h.ship_id "
	     "  and d.client_id = o.client_id"
	     "  and d.ordnum    = o.ordnum "
	     "  and d.client_id = ol.client_id"
	     "  and d.ordnum    = ol.ordnum "
	     "  and d.ordlin    = ol.ordlin "
	     "  and d.ordsln    = ol.ordsln "
	     "  and exists "
	     "     (select 1  "
	     "        from var_wavpar_dtl v "
	     "       where ((h.carcod = v.vc_carcod  and o.btcust = v.vc_cstnum ) "
	     "           or (h.carcod = v.vc_carcod  and v.vc_cstnum = '%s')   "                     
	     "           or (o.btcust = v.vc_cstnum and v.vc_carcod = '%s' )))  "
	     "  and h.shpsts in ('%s', '%s')" 
	     "  and h.wave_set is not NULL  "
	     "  and h.vc_wave_flg = '%d'    "
	     "  and d.schbat is null        "
             "  %s %s"
             "  and not exists "
             "    (select 1 "
             "       from rplwrk r "
             "      where r.ship_line_id = d.ship_line_id) "
             "  and not exists "
             "    (select 'x' "
             "       from xdkwrk x "
             "      where x.ship_line_id = d.ship_line_id) "
	     "  group by h.ship_id, a.adrnam, h.host_client_id, "
	     "           h.host_ext_id, h.shpsts, h.rt_adr_id, "
	     "           h.cargrp, h.carcod, "
	     "           h.srvlvl, h.sddflg, h.doc_num, h.track_num, "
	     "           h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	     "           h.alcdte, h.stgdte, h.loddte, h.entdte, "
	     "           h.early_shpdte, h.late_shpdte, "
	     "           h.early_dlvdte, h.late_dlvdte, "
	     "           h.dstare, h.dstloc, h.rrlflg,  "
	     "           o.btcust, "
	     "           decode (o.ordtyp, '%s', '%s', '%s'),    "
	     "           to_char(h.vc_plan_shpdte, '%s'),h.wave_set ",
	     LINSTS_PENDING, LINSTS_INPROCESS,
	     DEFAULT_NOT_NULL_STRING, DEFAULT_NOT_NULL_STRING,
	     /* include ready and in-process shipments */
	     SHPSTS_READY, SHPSTS_IN_PROCESS, TRUE,  
	     where_list,
	     work_order_where, SAMPLE_ORDER_TYPE,
	     YES_STR, NO_STR, 
	     LODCMD_DATE_FORMAT);
    strcpy(where_list, buffer);

    /* AND now the where clause for the union */
    sprintf(buffer, 
	     "      ftpmst.ftpcod = prtmst.ftpcod "
	     "  and prtmst.prtnum = ol.prtnum "
	     "  and prtmst.prt_client_id = ol.prt_client_id "
	     "  and a.adr_id = o.bt_adr_id "
             "  and d.linsts in ('%s', '%s') "
	     "  and d.pckqty    > 0 "
	     "  and d.ship_id   = h.ship_id "
	     "  and d.client_id = o.client_id"
	     "  and d.ordnum    = o.ordnum "
	     "  and d.client_id = ol.client_id"
	     "  and d.ordnum    = ol.ordnum "
	     "  and d.ordlin    = ol.ordlin "
	     "  and d.ordsln    = ol.ordsln "
	     "  and h.shpsts in ('%s', '%s')"
	     "  and h.wave_set is not NULL  "
	     "  and h.vc_wave_flg = '%d'    "
	     "  and d.schbat is null        "
             "  %s %s"
             "  and not exists "
             "    (select 1 "
             "       from rplwrk r "
             "      where r.ship_line_id = d.ship_line_id) "
             "  and not exists "
             "    (select 'x' "
             "       from xdkwrk x "
             "      where x.ship_line_id = d.ship_line_id) "
	     "  and  not exists  "
	     "    ( select 1  "
	     "        from var_wavpar_dtl v"
	     "        where   "
             "          ((h.carcod = v.vc_carcod  and o.btcust = v.vc_cstnum)   "
	     "            or (h.carcod = v.vc_carcod  and v.vc_cstnum = '%s')   "                     
	     "            or (o.btcust = v.vc_cstnum and v.vc_carcod = '%s' ))) "
	     "  group by h.ship_id, a.adrnam, h.host_client_id, "
	     "           h.host_ext_id, h.shpsts, h.rt_adr_id, "
	     "           h.cargrp, h.carcod, "
	     "           h.srvlvl, h.sddflg, h.doc_num, h.track_num, "
	     "           h.frtchg, h.frtcod, h.frtrte, h.adddte, "
	     "           h.alcdte, h.stgdte, h.loddte, h.entdte, "
	     "           h.early_shpdte, h.late_shpdte, "
	     "           h.early_dlvdte, h.late_dlvdte, "
	     "           h.dstare, h.dstloc, h.rrlflg,  "
	     "           o.btcust, "
	     "           decode (o.ordtyp, '%s', '%s', '%s'),  "
	     "           to_char(h.vc_plan_shpdte, '%s'), h.wave_set ",
	     LINSTS_PENDING, LINSTS_INPROCESS,
	     /* include ready and in-process shipments */
	     SHPSTS_READY, SHPSTS_IN_PROCESS, TRUE, 
	     where_list2, work_order_where, 
	     DEFAULT_NOT_NULL_STRING, DEFAULT_NOT_NULL_STRING,
	     SAMPLE_ORDER_TYPE,
	     YES_STR, NO_STR, 
	     LODCMD_DATE_FORMAT);

    strcpy(where_list2, buffer);

    sprintf(buffer, 
	     "[ select distinct ' ' pckexc, %s "
	     " from %s "
	     " where %s "
	     " UNION "
	     " select distinct ' ' pckexc, %s "
	     " from %s "
	     " where %s"
	     " order by  32, 33, line_type, wavcas_qty desc]" 
	     ,
	     field_list, 
	     table_list,
	     where_list,
	     field_list2,
	     table_list2,
	     where_list2
	     );

    misTrc (T_FLOW, "Buffering...." );
    misTrc (T_FLOW, "%s", buffer);

    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	 srvFreeMemory(SRVRET_STRUCT, CurPtr);
	 return (srvSetupReturn(ret_status, ""));
    }
    
    if (ret_status ==eOK)
	sSetExclusions(CurPtr); 
    
    return (CurPtr);
}

