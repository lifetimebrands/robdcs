static char *rcsid = "$Id: varConsolidateShipmentLinesByVolume.c,v 1.4 2002/02/27 20:40:56 prod Exp $"; 
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 McHugh Software International, Inc.
 *  Shelton, Connecticut,  U.S.A.
 *  All rights reserved.
 *
 *  Application:   tmint
 *  Created:	07/08/99
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct tm_row_struct {
  char ship_line_id[SHIP_LINE_ID_LEN + 1];
  long pckqty;
  long untcas;
  long untpak;
  moca_bool_t case_splflg;
  moca_bool_t pack_stdflg;

  }  TM_ROW_STRUCT; 

static double def_max_delta_volume = -10.0;
static double def_max_volume = -10.0;

int var_SplitShipmentByVolume(char *ship_id, double max_volume, double shpvol);
int var_SplitShpwthByVolume(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_volume, 
			   double *shpwthvol,
			   double *shpvol,
			   double *new_shpvol,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res);

int var_SplitLineByVolume(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_volume, 
			 double *linvol,
			 double *shpvol,
			 double *new_shpvol,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg);

long var_CreateNewLine(mocaDataRes *res, 
                     mocaDataRow *row, 
		     char *new_ship_id, 
		     long qty_fraction);

long var_RemoveShipmentLine(char *ship_line_id);


static void sInitialize(void)
{
	long ret_status;
    char sqlBuffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;

    if (def_max_delta_volume < 0)
    {
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'VAR' and "
			"polvar = 'ORDER-CONSOLIDATION' and polval = 'MAX-DELTA-VOLUME'");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    	row = sqlGetRow(res);
		def_max_delta_volume = sqlGetFloat(res, row, "rtflt1");
		}
	else			
        {
            /* No policy either, use default */
        def_max_delta_volume = 500; 
        }
    }

    if (def_max_volume < 0)
    {
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'VAR' and "                
                        "polvar = 'ORDER-CONSOLIDATION' and polval = 'MAX-VOLUME'");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    	row = sqlGetRow(res);
		def_max_volume = sqlGetLong(res, row, "rtflt1");
		}
	else			
        {
            /* No policy either, use default */
            def_max_volume = 1000000000; 
        }
    }
}

/* Consoldiate and split shipment lines based on volume */
LIBEXPORT 
RETURN_STRUCT *varConsolidateShipmentLinesByVolume(char *cons_batch_i, 
						  double *max_volume_i)
{
    int ret_status;
    char ship_id[SHIP_ID_LEN + 1];
    char cons_batch[CONS_BATCH_LEN + 1];
    char sqlBuffer[1000];
    double shpvol;
    double max_volume;
    mocaDataRes *res;
    mocaDataRow *row;

    if (!cons_batch_i && !misTrimLen(cons_batch_i, CONS_BATCH_LEN))
        return (APPMissingArg("cons_batch"));

    sInitialize();

    /* assume each shipment will fit in truck */
    misTrimcpy(cons_batch, cons_batch_i, CONS_BATCH_LEN);
    sprintf(sqlBuffer,
	/*
    *        "select ship_id, vc_maxshpvol maxvol, "
	*		" sum((caswid*cashgt*caslen)*ceil((sl.pckqty / decode(ol.untcas,0,p.untcas,ol.untcas )))) shpvol "
    *       "  from shipment_line sl, ord_line ol, ord, cstmst c, prtmst p, ftpmst f  "
    *        " where sl.cons_batch = '%s'                                  "
    *        "   and sl.client_id = ol.client_id                           "
    *        "   and ord.client_id = ol.client_id                          "
    *        "   and sl.ordnum = ol.ordnum                                 "
    *        "   and ord.ordnum = ol.ordnum                                "
    *        "   and ord.btcust = c.cstnum                                 "
    *        "   and sl.ordlin = ol.ordlin                                 "
    *        "   and sl.ordsln = ol.ordsln                                 "
    *        "   and ol.prt_client_id = p.prt_client_id                    "
    *        "   and ol.prtnum = p.prtnum                                  "
    *        "   and p.ftpcod = f.ftpcod                                   "
    *        "group by ship_id, vc_maxshpvol                               ",
	*/
		
            "select ship_id, vc_maxshpvol maxvol, "
			" sum(round((caslen*caswid*cashgt) *((sl.pckqty / decode(ol.untcas, 0, p.untcas, ol.untcas))), 2)) shpvol "
            "  from shipment_line sl, ord_line ol, ord, cstmst c, prtmst p, ftpmst f  "
            " where sl.cons_batch = '%s'                                  "
            "   and sl.client_id = ol.client_id                           "
            "   and ord.client_id = ol.client_id                          "
            "   and sl.ordnum = ol.ordnum                                 "
            "   and ord.ordnum = ol.ordnum                                "
            "   and ord.btcust = c.cstnum                                 "
            "   and sl.ordlin = ol.ordlin                                 "
            "   and sl.ordsln = ol.ordsln                                 "
            "   and ol.prt_client_id = p.prt_client_id                    "
            "   and ol.prtnum = p.prtnum                                  "
            "   and p.ftpcod = f.ftpcod                                   "
            "group by ship_id, vc_maxshpvol                               ",
            cons_batch);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* only process those shipments that don't fit */

	shpvol = sqlGetFloat(res, row, "shpvol");
    max_volume = -1;

	if(!sqlIsNull(res,row,"maxvol")) 
		max_volume = (float) sqlGetLong(res, row, "maxvol");
	else
		if (max_volume_i && *max_volume_i > 0)
			max_volume = *max_volume_i;
		else
			max_volume = def_max_volume;


	if (max_volume > 0 && max_volume < shpvol)
	{
	    /* this shipment exceeds the max, have to split it up. */

	    misTrimcpy(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
	    ret_status = var_SplitShipmentByVolume(ship_id, max_volume, shpvol);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (srvResults(ret_status, NULL));
	    }
	}
    }
    sqlFreeResults(res);
    return (srvResults(eOK, NULL));
}

int var_SplitShipmentByVolume(char *ship_id, double max_volume, double shpvol)
{
    int ret_status;
    long numrows, i;
    long pckqty;
    long old_pckqty;
    long untcas;
    long untpak;
    double linvol;
    double new_shpvol;
    double shpwthvol;
    double old_shpwthvol;
#define SHPWTH_LEN 10
    char shpwth[SHPWTH_LEN + 1];
    char new_ship_id[SHIP_ID_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    char sqlBuffer[2000];
    moca_bool_t case_splflg;
    moca_bool_t pack_stdflg;
    mocaDataRes *res;
    mocaDataRow *row;
    mocaDataRes *linres;
    mocaDataRow *linrow;
    TM_ROW_STRUCT *row_array;

    ret_status = appNextNum("ship_id", new_ship_id);
    if (ret_status != eOK)
	return ret_status;
    new_shpvol = 0;

    /* First, try the lines that are not constrained to 
     * "Ship with another line"
     * Then, try the constrained lines...
     */

    /* Should we use CONS_BATCH to restrict our search?
     * This shipment should be made entirely of 1 cons_batch
     * Therefore, shouldn't have to use cons_batch, ship_id is sufficient
     */
    sprintf(sqlBuffer,
	/*    "select sl.ship_line_id, sl.pckqty,                  "
	 *   "       ol.stdflg, ol.splflg,                        "
	 *   "       p.untcas, p.untpak,                          "
	 *   "       sum((caslen*caswid*cashgt) * (ceil((sl.pckqty / decode(ol.untcas,0,p.untcas,ol.untcas )))) ) linvol "
	 *   "  from ftpmst f, ord_line ol, prtmst p, shipment_line sl      "
	 *   " where sl.ship_id = '%s'                            "
	 *   "   and sl.client_id = ol.client_id                  "
	 *   "   and sl.ordnum = ol.ordnum                        "
	 *   "   and sl.ordlin = ol.ordlin                        "
	 *   "   and sl.ordsln = ol.ordsln                        "
	 *   "   and ol.prt_client_id = p.prt_client_id           "
	 *   "   and ol.prtnum = p.prtnum                         "
	 *   "   and p.ftpcod = f.ftpcod 			 "
	 *   "   and sl.shpwth is NULL                            "
	 *   "group by sl.ship_line_id, sl.pckqty, ol.stdflg,     "
	 *   "         ol.splflg, p.untcas, p.untpak              "
	 *   "order by linvol desc                                ",
	 */
	 "select x.ship_line_id,            "
"        x.pckqty,                 "
"        x.stdflg,                 "
"        x.splflg,                 "
"        x.untcas,                 "
"        x.untpak,                 "
"        x.linvol                  "
"   from(select sl.ship_line_id,   "
"            sl.ordnum,   "
"            sl.ordlin,   "
"            sl.ordsln,   "
"            sl.pckqty,   "
"            ol.stdflg,   "
"            ol.splflg,   "
"            p.untcas,    "
"            p.untpak,    "
"            sum(round((caslen*caswid*cashgt) *((sl.pckqty / decode(ol.untcas, 0, p.untcas, ol.untcas))), 2)) linvol   "
"       from ftpmst f,                           "
"            ord_line ol,                        "
"            prtmst p,                           "
"            shipment_line sl                    "
"      where sl.ship_id = '%s'           "
"        and sl.client_id = ol.client_id         "
"        and sl.ordnum = ol.ordnum               "
"        and sl.ordlin = ol.ordlin               "
"        and sl.ordsln = ol.ordsln               "
"        and ol.prt_client_id = p.prt_client_id  "
"        and ol.prtnum = p.prtnum  "
"        and p.ftpcod = f.ftpcod   "
"        and sl.shpwth is NULL     "
"      group by sl.ship_line_id,   "
"            sl.pckqty,            "
"            ol.stdflg,            "
"            ol.splflg,            "
"            p.untcas,             "
"            p.untpak,             "
"            sl.ordnum,            "
"            sl.ordlin,            "
"            sl.ordsln             "
"          order by sl.ordnum,     "
"                sl.ordlin,        "
"                sl.ordsln) x      ",
	    ship_id);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	sqlFreeResults(res);
        return (ret_status);
    }

    for (row = sqlGetRow(res);
         row && (max_volume < shpvol);
         row = sqlGetNextRow(row))
    {
	misTrimcpy(ship_line_id, 
		   sqlGetString(res, row, "ship_line_id"), 
		   SHIP_LINE_ID_LEN);
        linvol = sqlGetFloat(res, row, "linvol");
	pckqty = sqlGetLong(res, row, "pckqty");
	old_pckqty = pckqty;

	case_splflg = sqlGetBoolean(res, row, "splflg");
	pack_stdflg = sqlGetBoolean(res, row, "stdflg");
	untcas = MAX(sqlGetLong(res, row, "untcas"), 1);
	untpak = MAX(sqlGetLong(res, row, "untpak"), 1);

	while (pckqty && (max_volume < shpvol))
	{
	    /* This line is greater than a single truck 
	     * Try to split the line into multiple shipments
	     */
	    ret_status = var_SplitLineByVolume(ship_line_id, 
					      ship_id, 
					      new_ship_id, 
					      max_volume, 
					      &linvol,
					      &shpvol,
					      &new_shpvol,
					      &pckqty,
					      untcas,
					      untpak,
					      case_splflg,
					      pack_stdflg);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		return (ret_status);
	    }
	}

	if (pckqty == 0)
	{
	    /* The whole quantity of this line was moved to new shipments.
	     */
	    ret_status = var_RemoveShipmentLine(ship_line_id);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (ret_status);
	    }
	}
	else if (old_pckqty != pckqty)
	{
	    /* if pckqty changed, 
	     * then we have to update old shipment line 
	     */
	    sprintf(sqlBuffer,
		"update shipment_line set pckqty = '%ld' "
		"where ship_line_id = '%s'",
		pckqty,
		ship_line_id);

	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
	        return (ret_status);
	    }
	}
    }
    sqlFreeResults(res);

    if (max_volume < shpvol)
    {
	/* didn't split off enough yet.
	 * Now, we have to try more complicated shpwth splitting
	 */
	sprintf(sqlBuffer,
		"select sl.shpwth,                                   "
		"       sum(caslen*caslen*cashgt * (sl.pckqty / p.untcas) ) shpwthvol "
		"  from ftpmst f, prtmst p, ord_line ol, shipment_line sl             "
		" where sl.ship_id = '%s'                            "
		"   and sl.client_id = ol.client_id                  "
		"   and sl.ordnum = ol.ordnum                        "
		"   and sl.ordlin = ol.ordlin                        "
		"   and sl.ordsln = ol.ordsln                        "
		"   and ol.prt_client_id = p.prt_client_id           "
		"   and ol.prtnum = p.prtnum                         "
		"   and p.ftpcod = f.ftpcod                          "
		"   and sl.shpwth is not NULL                        "
		"group by shpwth                                     "
		"order by shpwthvol desc                                ",
		ship_id);

	ret_status = sqlExecStr(sqlBuffer, &res);
	if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	for (row = sqlGetRow(res);
	     row && (max_volume < shpvol);
	     row = sqlGetNextRow(row))
	{
	    misTrimcpy(shpwth, sqlGetString(res, row, "shpwth"), SHPWTH_LEN);
	    shpwthvol = sqlGetFloat(res, row, "shpwthvol");
	    old_shpwthvol = shpwthvol;

	    sprintf(sqlBuffer,
		    " select distinct sl.*, "
		    "        ol.splflg, ol.stdflg, p.untcas, p.untpak "
		    "   from shipment_line sl, ord_line ol, prtmst p "
		    "  where sl.shpwth = '%s'"
		    "    and sl.client_id = ol.client_id "
		    "    and sl.ordnum = ol.ordnum "
		    "    and sl.ordlin = ol.ordlin "
		    "    and sl.ordsln = ol.ordsln "
		    "    and ol.prt_client_id = p.prt_client_id "
		    "    and ol.prtnum = p.prtnum " ,
		    shpwth);

	    ret_status = sqlExecStr(sqlBuffer, &linres);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(linres);
		sqlFreeResults(res);
		return (ret_status);
	    }

	    numrows = sqlGetNumRows(res);
	    row_array = calloc(numrows, sizeof(TM_ROW_STRUCT));

	    for (linrow = sqlGetRow(linres), i = 0;
		 linrow && (i < numrows);
		 linrow = sqlGetNextRow(linrow), i++)
	    {
	        misTrimcpy(row_array[i].ship_line_id,
		           sqlGetString(linres, linrow, "ship_line_id"),
			   SHIP_LINE_ID_LEN);
	        row_array[i].pckqty = sqlGetLong(linres, linrow, "pckqty");
		row_array[i].case_splflg = sqlGetBoolean(res, row, "splflg");
		row_array[i].pack_stdflg = sqlGetBoolean(res, row, "stdflg");
		row_array[i].untcas = MAX(sqlGetLong(res, row, "untcas"), 1);
		row_array[i].untpak = MAX(sqlGetLong(res, row, "untpak"), 1);

	    }
	    sqlFreeResults(linres);

	    /* This line is greater than a single truck 
	     * Try to split the line into multiple shipments
	     */
	    while ((shpwthvol > 0) && (max_volume < shpvol))
	    {
		ret_status = var_SplitShpwthByVolume(shpwth, 
						    ship_id, 
						    new_ship_id, 
						    max_volume, 
						    &shpwthvol,
						    &shpvol,
						    &new_shpvol,
						    numrows,
						    row_array,
						    linres);
		if (ret_status != eOK)
		{
		    sqlFreeResults(res);
		    return (ret_status);
		}
	    }

	    if (shpwthvol == 0)
	    {
		/* The whole quantity of these lines were 
		 * moved to new shipments.
		 * Remove the old lines.
		 */
	        for (i = 0; i < numrows; i++)
		{
		  ret_status = var_RemoveShipmentLine(row_array[i].ship_line_id);
		  if (ret_status != eOK)
		  {
			sqlFreeResults(res);
			return (ret_status);
		  }
		}
	    }
	    else if (old_shpwthvol != shpwthvol)
	    {
		/* if volume changed, then we have to update 
		 * the pckqty of the old shipment lines
		 */
	        for (i = 0; i < numrows; i++)
		{
		    sprintf(sqlBuffer,
			"update shipment_line set pckqty = '%ld' "
			"where ship_line_id = '%s'",
			row_array[i].pckqty,
			row_array[i].ship_line_id);

		    ret_status = sqlExecStr(sqlBuffer, NULL);
		    if (ret_status != eOK)
		    {
			sqlFreeResults(res);
			return (ret_status);
		    }
		}
 	  }
	}
	sqlFreeResults(res);
    }
    return eOK;
}

int var_SplitShpwthByVolume(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_volume, 
			   double *shpwthvol,
			   double *shpvol,
			   double *new_shpvol,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res)
{
    int ret_status;
    long i;
    long qty_fraction;
    double vol_fraction;
    double vol_avail;
    double vol_ratio;
    mocaDataRow *row;

    vol_avail = max_volume - (*new_shpvol);

    if (vol_avail >= (*shpwthvol))
    {
	/* all lines in shpwth group fit in new shipment,
	 * just move them over to new shipment
	 */
	for (i = 0; i < numrows; i++)
	{
	    ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
					     row_array[i].ship_line_id);
	    if (ret_status != eOK)
	        return (ret_status);

	    row_array[i].pckqty = 0;
	}

	(*new_shpvol) += (*shpwthvol);
	(*shpvol) -= (*shpwthvol);
	(*shpwthvol) = 0;
    }
    else
    {
        /* all lines in shpwth group don't fit in new shipment together.
	 * Try to do proportional splitting of group.
	 *
         * Check if enough of the group of lines 
	 * will fit to make the split worthwhile.
	 * Otherwise, just start a new shipment 
	 */

    if (vol_avail < def_max_delta_volume)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpvol = 0;
	    vol_avail = max_volume;
	}

	/* We may have just changed the volume available.
	 * Check again if we can put the whole line in without changes
	 */
	if (vol_avail >= (*shpwthvol))
	{
	    /* all lines in shpwth group fit in new shipment,
	     * just move them over to new shipment
	     */
	    for (i = 0; i < numrows; i++)
	    {
		ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
						 row_array[i].ship_line_id);
		if (ret_status != eOK)
		    return (ret_status);

		row_array[i].pckqty = 0;
	    }

	    (*new_shpvol) += (*shpwthvol);
	    (*shpvol) -= (*shpwthvol);
	    (*shpwthvol) = 0;
	}
	else
	{
	    vol_ratio = vol_avail / (*shpwthvol);
	    for (i = 0; i < numrows; i++)
	    {
		/* part of the line will fit, 
		 * evaluate constraints to see how much
		 *
		 * Can we split a case?
		 * Can we split a package ?
		 */

		qty_fraction = (long) floor(((double) row_array[i].pckqty) 
		                            * vol_ratio);

		if (row_array[i].case_splflg == BOOLEAN_FALSE)
		{
		    /* Can not split cases */
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untcas) * 
		                          row_array[i].untcas;
		}
		else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		{
		    /* Must use standard packages */
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untpak) * 
		                          row_array[i].untpak;
		}
		vol_ratio = MIN(vol_ratio, qty_fraction/row_array[i].pckqty);
	    }

	    if (vol_ratio > 0)
	    {
	        /* iterate through row and array at the same time */
	        for (i = 0, row = sqlGetRow(res); 
		     row && i < numrows; 
		     row = sqlGetNextRow(row), i++)
		{
		    /* part of the group will fit, 
		     * even after constraint checks 
		     * Generate new pckqty by using ratio 
		     *   and pick constraints
		     * Make a new line out of each line
		     */

		    qty_fraction = (long) ceil(row_array[i].pckqty * 
			                       vol_ratio);

		    if (row_array[i].case_splflg == BOOLEAN_FALSE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untcas) * 
			                      row_array[i].untcas;
		    }
		    else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untpak) * 
			                      row_array[i].untpak;
		    }

		    ret_status = var_CreateNewLine(res, row, 
			                          new_ship_id, 
						  qty_fraction);
		    if (ret_status != eOK)
			return(ret_status);

		    vol_fraction = qty_fraction * ((*shpwthvol) / 
			                           (row_array[i].pckqty));

		    *new_shpvol += vol_fraction;
		    *shpwthvol -= vol_fraction;
		    *shpvol -= vol_fraction;
		    row_array[i].pckqty -= qty_fraction;
		}
	    }
	    else if (*new_shpvol == 0)
	    {
		/* no fraction of the line will fit, 
		 * even though this is a new shipment.
		 * Therefore, bomb out.
		 */
		return eINT_INVALID_SHIPMENT;
	    }
	    else
	    {
		ret_status = appNextNum("ship_id", new_ship_id);
		if (ret_status != eOK)
		    return ret_status;

		*new_shpvol = 0;
	    }
	} /* end if proportional splitting */
    } /* end if shpwth group doesn't fit in pre-existing shipment */

    return eOK;
}

int var_SplitLineByVolume(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_volume, 
			 double *linvol,
			 double *shpvol,
			 double *new_shpvol,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg)
{
    int ret_status;
    long qty_fraction;
    char sqlBuffer[1000];
    double vol_fraction;
    double vol_avail;
    double vol_ratio;
    mocaDataRes *res;
    mocaDataRow *row;

    vol_avail = max_volume - (*new_shpvol);
    // if (vol_avail >= (*linvol))

    if (1 != 1)
    {
        /* whole line fits, move whole quantity. */
        qty_fraction = (*pckqty);
    }
    else
    {
        /* Check if enough of the line will fit 
	 * to make the split worthwhile.
	 * Otherwise, just start a new shipment 
	 */
        if (vol_avail < def_max_delta_volume)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpvol = 0;
	    vol_avail = max_volume;
	}

	/* We may have just changed the volume available.
	 * Check again if we can put the whole line in without changes
	 */
	if (1==1)
	{
	    /* whole line fits, move whole quantity. */
	    qty_fraction = (*pckqty);
	}
	else
	{
	    vol_ratio = vol_avail / (*linvol);
	    qty_fraction = (long) floor(((double) *pckqty) * vol_ratio);

	    if (qty_fraction)
	    {
		/* part of the line will fit, 
		 * evaluate constraints to see how much
		 *
		 * Can we split a case?
		 * Can we split a package ?
		 */

		if (case_splflg == BOOLEAN_FALSE)
		{
		    /* Can not split cases */
		    qty_fraction = (long) floor(qty_fraction / untcas) * 
		                          untcas;
		}
		else if (pack_stdflg == BOOLEAN_TRUE)
		{
		    /* Must use standard packages */
		    qty_fraction = (long) floor(qty_fraction / untpak) * 
		                          untpak;
		}
	    }
	}
    }

    if (qty_fraction)
    {
        /* part of the line will fit, even after constraint checks 
	 * Make a new line out of this line
	 */

        sprintf(sqlBuffer, 
	        "select * from shipment_line where ship_line_id = '%s'",
		ship_line_id);

	ret_status = sqlExecStr(sqlBuffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	row = sqlGetRow(res);

        ret_status = var_CreateNewLine(res, row, new_ship_id, qty_fraction);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}

        vol_fraction = qty_fraction * ((*linvol) / (*pckqty));

        *new_shpvol += vol_fraction;
	*linvol -= vol_fraction;
        *shpvol -= vol_fraction;
        *pckqty -= qty_fraction;

	sqlFreeResults(res);
    }
    else if (*new_shpvol == 0)
    {
        /* no fraction of the line will fit, 
	 * even though this is a new shipment.
	 * Therefore, bomb out.
	 */
        return eINT_INVALID_SHIPMENT;
    }
    else
    {
	ret_status = appNextNum("ship_id", new_ship_id);
	if (ret_status != eOK)
	    return ret_status;

	*new_shpvol = 0;
    }

    return eOK;
}

long var_CreateNewLine(mocaDataRes *res, 
                      mocaDataRow *row, 
		      char *new_ship_id, 
		      long qty_fraction)
{
    long ret_status;
    char new_ship_line_id[SHIP_LINE_ID_LEN + 1];
    char new_values[100];

    ret_status = appNextNum("ship_line_id", new_ship_line_id);
    if (ret_status != eOK)
	return (ret_status);

    sprintf(new_values, 
	    "'%s','%s',%ld", 
	    new_ship_line_id,
	    new_ship_id, 
	    qty_fraction);

    misLogInfo(" DBG1: new_ship_line_id=%s, new_ship_id=%s ",
    		new_ship_line_id, new_ship_id);

    ret_status = appGenerateCommandWithRes("create shipment line",
					   res, row, 
					   "ship_line_id, ship_id, pckqty",
					   new_values, 
					   NULL); 

    return(ret_status);
}

long var_RemoveShipmentLine(char *ship_line_id)
{
   RETURN_STRUCT *CurPtr = NULL; /* kjh Feb27 new for MR1810 */
   char buffer[2000];		 /* kjh Feb27 new for MR1810 */
   int ret_status;	         /* kjh Feb27 new for MR1810 */
/* kjh Feb27 orig is removed    char sqlBuffer[100]; */

/* kjh Feb27 below is orig and we need fix for MR1810
    sprintf(sqlBuffer, 
	    "remove shipment line where ship_line_id = '%s'",
	    ship_line_id);

    return (srvInitiateCommand(sqlBuffer, NULL));
   kjh Feb27 above is orig and we need fix for MR1810*/
/* kjh Feb27 below is replacement to fix MR1810 from tmRemoveShipmentLine.c */
    /*delete instructions */
    sprintf (buffer, "list shipment line instructions "
                     " where ship_line_id = '%s' | "
                     " remove special handling instructions ",
                     ship_line_id );

    ret_status = srvInitiateInline(buffer, &CurPtr);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
        return CurPtr;
    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    /* delete the rows */
    sprintf(buffer,
            "delete shipment_line where ship_line_id = '%s' ",
            ship_line_id);
    ret_status = sqlExecStr(buffer, NULL);
    return (ret_status);
/* kjh Feb27 above is replacement to fix MR1810 */

}

int var_ChangeShipmentOnShipmentLine(char *ship_line_id, char *new_ship_id)
{
    char sqlBuffer[1000];

    sprintf(sqlBuffer,
            "update shipment_line "
            " set ship_id = '%s'  "
            "where ship_line_id = '%s' ",
            new_ship_id,
            ship_line_id);

    return (sqlExecStr(sqlBuffer, NULL));
}
