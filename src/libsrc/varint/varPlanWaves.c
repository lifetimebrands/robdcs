static const char *rcsid = "$Id: varPlanWaves.c,v 1.5 2002/10/11 23:15:02 prod Exp $";
/*#START**********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION  :  This function is used for Wave Planning (Creation). 
 *	  	   The user enters a plan Wave date while calling this function.
 *	   MR1042 added logic to only flag the shipment as small_package
 * 	   	  for fluid loading if it is a true small package shipment 
 *		  (carcod is in carxrf)
 *	       	  OR an LTL carrier that is flagged for fluid load
 *		  (carcod is in carxrf AND VAR policy exists for the carcod)
 *#END*************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <varcolwid.h>
#include <vargendef.h>
#include <varerr.h>

LIBEXPORT 
RETURN_STRUCT *varPlanWaves(char *wav_plndte_i, char *wave_set_i)
{
    RETURN_STRUCT *returnData=NULL;
    RETURN_STRUCT *return2Data=NULL;
    char buffer[3000] = "";  
    mocaDataRow   *cur_row;
    mocaDataRow   *row;
    mocaDataRes   *res;
    mocaDataRes   *com_res = NULL;
    mocaDataRes   *sh_res =  NULL;
    long ret_status;
    
    long wave_lane = 0;
    char wave_number[WAVE_NUMBER_LEN + 1];
    char wave_set [WAVE_SET_LEN +  1];
    char old_wave_number[WAVE_NUMBER_LEN + 1];
    long l_wave_number = 1; /* Long Wave Number */
    char ship_id[SHIP_ID_LEN+1] = "";
    long ship_wavcas_qty = 0; /* Shipment Wave Qty that goes across a divert */
    char carcod [CARCOD_LEN + 1] = DEFAULT_NOT_NULL_STRING;
    char cstnum [CSTNUM_LEN + 1] = DEFAULT_NOT_NULL_STRING;
    char sample_ord_flag [FLAG_LEN+1];			/* MR1042 declare fixed */
    char small_parcel_flag [FLAG_LEN+1];		/* MR1042 declare fixed */
    char reg_preticket_flag [FLAG_LEN+1];		/* MR1042 declare fixed */
    long ptktyp = 0;
    char wav_plndte [LODCMD_DATE_LEN + 1] ;
    long datdiff = 0;
    long count = 0;
    long pass = 0; 

    memset (ship_id, 0, sizeof (ship_id));
    memset (wave_number, 0, sizeof (wave_number));
    memset (old_wave_number, 0, sizeof (old_wave_number));
    memset (wave_set, 0, sizeof (wave_set));
    memset (wav_plndte, 0, sizeof (wav_plndte));

    /* If the userenters a plan date, we will use it, else use sysdate */
    if (wav_plndte_i && misTrimLen (wav_plndte_i, LODCMD_DATE_LEN) != 0) {
        misTrimcpy(wav_plndte, wav_plndte_i, LODCMD_DATE_LEN) ;
    }
    else {
	sprintf (buffer,
	         " Select to_char (sysdate, '%s')  temp_wav_dte "
		 " From dual ",
		 LODCMD_DATE_FORMAT);
	ret_status = sqlExecStr(buffer, &res) ;
	if (ret_status !=eOK) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	row = sqlGetRow (res);
	strncpy(wav_plndte,sqlGetString(res,row,"temp_wav_dte"), LODCMD_DATE_LEN);
	sqlFreeResults(res);
    }

    if (wave_set_i && misTrimLen (wave_set_i, WAVE_SET_LEN) != 0) {
       misTrimcpy(wave_set, wave_set_i, WAVE_SET_LEN) ;
    }
    else {
       return (srvSetupReturn(eVAR_INVALID_WAVESET,""));     
    }

    /* We will have the Wave plan date by now */
    /* select the max wave number from the pckbat table.
       Wave number is of the form <date><sequence>, we
       select the sequence */
    
    sprintf ( buffer, 
	" select to_number(max(substr(pckbat.schbat,7,3))) + 1 wave "
	" From   pckbat	"  
	" Where  pckbat.schbat like "
	" (Select substr ('%s', 3, 6)||'%s' "
	" From  dual)",
	wav_plndte, "%");

    ret_status = sqlExecStr(buffer, &res) ;
    if (ret_status !=eOK && ret_status != eDB_NO_ROWS_AFFECTED) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }
    
    if (ret_status != eDB_NO_ROWS_AFFECTED) {
	row = sqlGetRow (res);
	l_wave_number = sqlGetLong (res, row, "wave");
	if (l_wave_number <= 0) {
	    l_wave_number = 1;
	}
    	sqlFreeResults (res);
    }
    /*** If the wave number is NULL, it means that we are starting fresh 
	 for the day.  Note: The max function always returns a value */

    pass = 1;
    while (pass <= MAX_PASS)
    {
	/* Select all shipments that are available to be waved */
	sprintf (buffer, " List waveable shipments where wave_set = '%s' "
	   "|[Select decode (min(cp.vc_prenum), NULL, 'N', 'Y') "
	   " reg_preticket " /* check if this is a ticketed customer */
	   " From shipment_line sl, ord_line ol, cstprq cp, ord o "
	   " Where sl.ship_id =  @ship_id " 
	   " And sl.client_id = ol.client_id "
	   " And sl.ordnum = ol.ordnum "
	   " And sl.ordlin = ol.ordlin " 
	   " And sl.ordsln = ol.ordsln "                           
	   " And ol.ordnum = o.ordnum "                           
	   " And ol.client_id = o.client_id "                           
	   " And ol.client_id = cp.client_id " 
	   " And ol.prtnum = cp.prtnum "                           
	   " And ol.prt_client_id = cp.prt_client_id " 
	   " And cp.cstnum = o.btcust] " 
	   /* The wave number is of the form <date><sequence> */
	   "|[select substr('%s',3,6) || ltrim(to_char(%ld,'000')) wave, "
	   /* datdiff gives the difference between the Wave plan date 
	      and the early ship date for the shipment */
	   " (trunc(nvl (@early_shpdte:date, sysdate)) "
	   " -  trunc (to_date ('%s', '%s')))  datdiff, "
	   " to_date ('%s', '%s') wave_date from dual] "
	   /* Set the planned ship date to the bigger of
	      early ship date and wave plan date */
	   "| if (@datdiff > 0) { "
	   " [update shipment set vc_plan_shpdte = @early_shpdte:date "
	   " where  ship_id = @ship_id ] } "
	   " else { "
	   " [update shipment set vc_plan_shpdte = @wave_date:date "
	   " where  ship_id = @ship_id  ] } "
	   " | [select @pckexc pckexc, @wave  wave, @ship_id ship_id, "
	   " @carcod  carcod, @btcust cstnum, @sample_order sample_order, "
	   " @small_parcel small_parcel, @reg_preticket reg_preticket, "
	   " @wavcas_qty  ship_wavcas_qty, @datdiff datdiff "
	   " from dual] ", 
	   wave_set,
	   wav_plndte, 
	   l_wave_number,
	   wav_plndte, 
	   LODCMD_DATE_FORMAT,
	   wav_plndte, 
	   LODCMD_DATE_FORMAT
	   ); 
	   		 
	ret_status = srvInitiateCommand (buffer, &returnData);

       /* Make sure that there are shipments available to plan */

        if (ret_status == eDB_NO_ROWS_AFFECTED)  {
            if (pass == 1)
            {
	        srvFreeMemory(SRVRET_STRUCT, returnData);
                return (srvSetupReturn(eVAR_NO_SHIPMENTS_TO_PLAN, "")) ;  
            }
            else
            {
	        srvFreeMemory(SRVRET_STRUCT, returnData);
                pass++;
                /* We still need to create the batch */
                continue;  /* while */
            }
        } 
	if (ret_status !=eOK ) {
	    srvFreeMemory(SRVRET_STRUCT, returnData);
	    return (srvSetupReturn(ret_status,"")) ;
	}

	com_res = returnData->ReturnedData;

	/* Do the processing for each shipment */
	for(cur_row=sqlGetRow(com_res); cur_row; cur_row=sqlGetNextRow(cur_row))
	{
	    /* Exclude shipments that have the Exclusion flag set */
	     
	    if ((*sqlGetStringByPos (com_res, cur_row, 0) == EXCLUDED_LINE)
	        || (*sqlGetStringByPos (com_res, cur_row, 0) == EXCLUDED)) {
		continue; /* for  */
	    }

	    memset (ship_id, 0, sizeof (ship_id));
            memset (wave_number, 0, sizeof (wave_number));
            memset (carcod, 0, sizeof (carcod));
            memset (cstnum, 0, sizeof (cstnum));

	    strcpy (carcod, DEFAULT_NOT_NULL_STRING);
	    strcpy (cstnum, DEFAULT_NOT_NULL_STRING);

	    strncpy(wave_number,sqlGetString(com_res,cur_row,"wave"), WAVE_LEN);
	    strncpy(ship_id,sqlGetString(com_res,cur_row,"ship_id"), SHIP_ID_LEN); 
	    /* Set the wave lane to 0 */
	    wave_lane = 0;
	    if (misTrimLen (sqlGetString(com_res,cur_row,"carcod"), CARCOD_LEN)) {
	      misTrimcpy(carcod,sqlGetString(com_res,cur_row,"carcod"), CARCOD_LEN);
	    }
	    if (misTrimLen(sqlGetString (com_res,cur_row,"cstnum"), CSTNUM_LEN)) {
	      misTrimcpy(cstnum,sqlGetString(com_res,cur_row,"cstnum"), CSTNUM_LEN);
	    }
	     
	    ship_wavcas_qty = sqlGetLong (com_res, cur_row, "ship_wavcas_qty");
/* MR1042 bad syntax use of strncpy is being fixed here */
	    misTrimcpy(sample_ord_flag, 
		sqlGetString(com_res,cur_row,"sample_order"), FLAG_LEN);
            misTrimcpy(small_parcel_flag, 
		sqlGetString (com_res, cur_row, "small_parcel"), FLAG_LEN);
	    misTrimcpy(reg_preticket_flag, 
		sqlGetString (com_res, cur_row, "reg_preticket"), FLAG_LEN);
/* MR1042 end of fix of bad syntax */
	    
	    /* If it is a sample order or  a small parcel-ticketed order, we
	       cannot fluid load it. Which means that we also will not sort
	       such a shipment based on the carcod and cstnum. We blank them out.
	       This is neccessary in order to keep sample orders or small- preticketed
	       orders together.
	       Dt: 11/17/01 -- RP. We will not keep Sample/Preticketed orders
	       together. 
	    */
	    /* if  ((strncmp (sample_ord_flag, YES_STR, FLAG_LEN) == 0)
		|| ((strncmp (small_parcel_flag, YES_STR, FLAG_LEN) == 0)
		    && (strncmp (reg_preticket_flag, YES_STR, FLAG_LEN) == 0)))
	    {
                strcpy (carcod, DEFAULT_NOT_NULL_STRING);
		strcpy (cstnum, DEFAULT_NOT_NULL_STRING);
	    } */
	    
	    datdiff = sqlGetLong (com_res, cur_row, "datdiff");

	    /* If the early delivery date is before the plan date, then the
	       planned ship date would be the plan date. So, the datdiff 
	       would be zero.
	    */
	    if (datdiff < 0)
		datdiff = 0;
            
	    if (ship_wavcas_qty > 0 ) {
	        /* Call the Get Wave Lane to fetch an appropriate lane */
	            sprintf (buffer, 
		    " Get Wave Lane "
		    " Where carcod = '%s' "
		    " And cstnum = '%s' "
		    " And ship_id = '%s' "
		    " And shp_wavcas_qty = %ld "
		    " And datdiff = %ld "
		    " And wavnum = '%s' "
		    " And preticket_flag = '%s'  "
		    " And small_parcel_flag = '%s' "
		    " And sample_ord_flag = '%s' "
		    " And pass = %ld ",
		    carcod,
		    cstnum,
		    ship_id,
		    ship_wavcas_qty,
		    datdiff,
		    wave_number,
		    reg_preticket_flag,
		    small_parcel_flag,
		    sample_ord_flag, pass);

		ret_status = srvInitiateCommand (buffer, &return2Data);

		if ((ret_status !=eOK) && (ret_status!=eDB_NO_ROWS_AFFECTED)) {
		    srvFreeMemory(SRVRET_STRUCT, return2Data);
		    sqlFreeResults (com_res); com_res=NULL;
		    return (srvSetupReturn(ret_status,"")) ;
		}
                
		if (ret_status == eOK) {
		    sh_res = return2Data->ReturnedData;
		    row = sqlGetRow (sh_res);
	    	    wave_lane = sqlGetLong (sh_res, row, "vc_wave_lane");
		    sqlFreeResults (sh_res);
		}
		srvFreeMemory(SRVRET_STRUCT, return2Data);

		if (wave_lane <= 0)
		    continue; /*for */
	    }
	   
            /* Making a change where we assign the lane to the shipment line
               even if there are only pallet qtys as long as the line is
               Conveyable (not NCV) . Mr 4038. Raman P 
               "| if (@vc_wavcas_qty > 0) " */

	    sprintf (buffer , 
		  "List waveable shipment lines "
		  "where ship_id = '%s' "
	          "| Assign shipment line to pick batch "
		  " where ship_line_id = @ship_line_id "
		  " and schbat = '%s' "
		  /* Ignore lines which are Non conveyable or 
		     Bulk confirms (vc_wavcas_qty = 0) */
		  "| if (@ship_line_id) "
		  " [Update shipment_line set "
                  " vc_wave_lane = decode (%ld, 0, null, %ld) "
		  " where ship_line_id = @ship_line_id ] ",
		   ship_id, wave_number, wave_lane, wave_lane);

	    ret_status = srvInitiateCommand (buffer, &return2Data);

            srvFreeMemory(SRVRET_STRUCT, return2Data);
	    if ((ret_status !=eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
		sqlFreeResults (com_res); com_res = NULL;
		return (srvSetupReturn(ret_status,""));
	    }
	} /* for shipments */
        if  (com_res) {
	    sqlFreeResults (com_res);
	    com_res = NULL;
	}
	pass++;
	misLogInfo(" MR1042dbg1: pass=%ld ",pass);
     } /* while !max pass */
     /* Check to see if we really did something in the last wave  */

     count = 0;
     sprintf ( buffer, 
               " select count(1) count "
               " From   shipment_line  sl "  
               " Where sl.schbat = '%s' ",	
                wave_number );

     ret_status = sqlExecStr(buffer, &res) ;
     if( ret_status !=eOK ) 
     {
          sqlFreeResults(res);
          return (srvSetupReturn(ret_status,"")) ;
     }

     row = sqlGetRow (res);
     count = sqlGetLong (res, row, "count");
     /* We planned a few shipments. Lets create the pck batch */
     if (count > 0) 
     {
         sprintf (buffer,
             "Create pick batch where schbat = '%s' "
 	     "and batsts = '%s' and wave_prc_flg = %ld",
	     wave_number, WAVE_STATUS_PLANNED, BOOLEAN_TRUE );
	
         ret_status = srvInitiateCommand (buffer, &return2Data);

         srvFreeMemory(SRVRET_STRUCT, return2Data);
         if (ret_status !=eOK) {
         /* This looks unnecessary. Still for safey reasons */
	    if (com_res)
	    {
	        sqlFreeResults (com_res); 
	    }
	    com_res = NULL;
	    return (srvSetupReturn(ret_status,"")) ;
	 }
     }
     /* We could not plan any shipment last wave. Lets get out */
     else
     {
         sqlFreeResults(res);
         return (srvSetupReturn (eVAR_NO_SHIPMENTS_PLANNED, ""));
     }
     sqlFreeResults(res);
    

    if (com_res) {
        sqlFreeResults (com_res);
	com_res=NULL;
    }

    return (srvSetupReturn (eOK, ""));
}

