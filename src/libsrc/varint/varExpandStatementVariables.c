static const char *rcsid = "$Id: varExpandStatementVariables.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varExpandStatementVariables.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application:   Lifetime Varlib
 *  Created:       28-Jun-2001
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <trnlib.h>


LIBEXPORT 
RETURN_STRUCT *varExpandStatementVariables(char *input_string_i)
{ 
	char returnString[2000];
	char tempString[1000];
	char tempString2[1000];
	char tempData[1000];
	char *nextString;
	char *inputStringPtr;
	char dataType;
	void *dataPtr;
	long retStatus;

	nextString = NULL;
   	memset(returnString, 0, sizeof(returnString));
	strcpy( tempString, input_string_i); 
	inputStringPtr = tempString;

	do {
		nextString = strchr(inputStringPtr, ',');
		if (nextString)
			*nextString++ = '\0';
		
		misTrimLR(inputStringPtr);
		if (strchr(inputStringPtr, '.'))
			retStatus = srvGetNeededElement (strchr(inputStringPtr,'.')+1, NULL, &dataType, &dataPtr);
		else	
			retStatus = srvGetNeededElement (inputStringPtr, NULL, &dataType, &dataPtr);

		if (retStatus == eOK) { /* WE FOUND THE DATA FOR THIS GUY */
			memset (tempData, 0, sizeof (tempData));
			switch (dataType) {
				case COMTYP_BINARY:
				case COMTYP_INT:
				case COMTYP_LONG:
					sprintf(tempData, "%ld", (int *) dataPtr);
					break;
				case COMTYP_STRING:
				case COMTYP_DATTIM:
					sprintf(tempData, "'%s'", (char *)dataPtr);
					break;
				case COMTYP_FLOAT:
					sprintf(tempData, "%f", (double *) dataPtr);
			}	

			if (strlen(tempData)) {
				sprintf(tempString2, "%s = %s", inputStringPtr, tempData); 
				if (strlen(returnString))
					strcat (returnString, " and ");	
				strcat(returnString, tempString2);
			}	
		}		
		inputStringPtr = nextString;
	} while ( inputStringPtr );

	return srvResults(eOK,
          "exdstr", COMTYP_CHAR, strlen(returnString), returnString,
          NULL);

    return(srvSetupReturn(eOK, ""));
}
