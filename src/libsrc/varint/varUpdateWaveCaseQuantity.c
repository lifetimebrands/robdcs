static char    *rcsid = "$Id: varUpdateWaveCaseQuantity.c,v 1.3 2002/03/07 16:14:06 prod Exp $";
/*#START*********************************************************************** 
 *  McHugh Software International 
 *  Copyright 2000 
 *  Waukesha, Wisconsin,  U.S.A. 
 *  All rights reserved. 
 * 
 *#END************************************************************************/ 
 
#include <moca_app.h> 
 
#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
 
#include <dcscolwid.h> 
#include <dcsgendef.h> 
#include <srvconst.h> 
#include <dcserr.h> 
#include <applib.h> 
#include <common.h> 

/* MR 1723 START */
static long sHandleCartonizationCall(char *prtnum_list,
				     char *prt_client_id_list,
				     char *untqty_list,
				     char *ship_line_list,
				     long num_lines)
{
    char *buffer;
    long ret_status;
    RETURN_STRUCT *CurPtr;

    mocaDataRes *res;
    long num_cartons;
    double wavcas_qty;

    buffer = (char *) calloc(1, 1000 + 
			     strlen(prtnum_list) +
			     strlen(prt_client_id_list) +
			     strlen(untqty_list));

    if (!buffer)
    {
	misTrc(T_FLOW, "Failed to allocate memory for cartonization call");
	return(eNO_MEMORY);
    }
			     
    misTrc(T_FLOW, "Determining approximate cartons...");
    
    sprintf(buffer,
	    "process cartonization "
	    "  where prtnum_list = '%s' "
	    "    and prt_client_id_list = '%s' "
	    "    and untqty_list = '%s' ",
	    prtnum_list,
	    prt_client_id_list,
	    untqty_list);
    
    CurPtr = NULL;
    ret_status = srvInitiateCommand(buffer, &CurPtr);

    if (eOK != ret_status)
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
	free(buffer);
	misTrc(T_FLOW, 
	       "Error (%d) encountered during process cartonization.  ",
	       ret_status);
	return(ret_status);
    }

    res = srvGetResults(CurPtr);
    num_cartons = sqlGetNumRows(res);
    srvFreeMemory(SRVRET_STRUCT, CurPtr);
    CurPtr = NULL;

    wavcas_qty = (double) num_cartons / (double) num_lines;
    
    misTrc(T_FLOW,
	   "Determined that %d cartons would be created for "
	   "%d shipment lines.  Updating shipment lines.",
	   num_cartons,
	   num_lines);

    sprintf(buffer,
	    "update shipment_line "
	    "   set vc_wavcas_qty = vc_wavcas_qty + %f "
	    " where ship_line_id in (%s) ",
	    wavcas_qty,
	    ship_line_list);

    ret_status = sqlExecStr(buffer, NULL);
    
    if (eOK != ret_status)
    {
	if (buffer) 
	    free(buffer);
	misTrc(T_FLOW, 
	       "Error (%d) while updating the shipment_line.  Exiting.",
	       ret_status);
	return (ret_status);
    }
    free(buffer);
    return(eOK);
}
/* MR 1723 END */

 
LIBEXPORT  
RETURN_STRUCT *varUpdateWaveCaseQuantity(char *ship_id_i) 
{
    long	   status; 
    char	   buffer[2000];
    char           ship_id[SHIP_ID_LEN + 1];
    char           tmpstr[1000];
    long           vc_ncvflg;
    RETURN_STRUCT *ShipLinePtr, *CurPtr;
    mocaDataRes   *slRes, *res;
    mocaDataRow   *slRow, *row;
    double         wavcas_qty;
    long           num_lines, numpck, num_cartons;

    /* Dynamic values used to pass to process cartonization in "try mode" */
    char          *prtnum_list, *untqty_list, *shipline_list, *client_list;

    misTrc (T_FLOW, "Entering varUpdateWaveCaseQuantity...");

    memset(ship_id, 0, sizeof(ship_id)); 
    if (!ship_id_i || !misTrimLen(ship_id_i, SHIP_ID_LEN))
    { 
	if (buffer) free(buffer);
        return (APPMissingArg("ship_id"));
    }

    misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    /* First, get a list of valid shipment lines to update */
    ShipLinePtr = CurPtr = NULL;
    prtnum_list = untqty_list = shipline_list = client_list = NULL;

    sprintf(buffer,
            "list waveable shipment lines "
            " where ship_id = '%s' ",
            ship_id);
    
    status = srvInitiateCommand(buffer, &ShipLinePtr);

    if (eOK != status)
    { 
        srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
        return (srvResults(status, NULL));
    }

    slRes = srvGetResults(ShipLinePtr);
    num_lines = 0;

    for (slRow = sqlGetRow(slRes); slRow; slRow = sqlGetNextRow(slRow))
    {
	/* MR 1723 START */
	if (num_lines == 999)
	{
	    status = sHandleCartonizationCall(prtnum_list,
					      client_list,
					      untqty_list,
					      shipline_list,
					      num_lines);

	    if (status != eOK)
	    {
		if (prtnum_list) free(prtnum_list);
		if (untqty_list) free(untqty_list);
		if (shipline_list) free(shipline_list);
		if (client_list) free(client_list);
		return(srvResults(status, NULL));
	    }

	    num_lines = 0;
	    if (prtnum_list) free(prtnum_list);
	    if (untqty_list) free(untqty_list);
	    if (shipline_list) free(shipline_list);
	    if (client_list) free(client_list);
	    client_list = prtnum_list = untqty_list = shipline_list = NULL;
	}
	/* MR 1723 END */
	
        /* First, see if we should even update the line */
        sprintf(buffer,
                "select nvl(vc_ncvflg, 0) vc_ncvflg "
                "  from prtmst "
                " where prtnum = '%s' "
                "   and prt_client_id = '%s' ",
                sqlGetString(slRes, slRow, "prtnum"),
                sqlGetString(slRes, slRow, "prt_client_id"));

        status = sqlExecStr(buffer, &res);

        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
            sqlFreeResults(res);
            if (prtnum_list) free(prtnum_list);
            if (untqty_list) free(untqty_list);
            if (shipline_list) free(shipline_list);
            if (client_list) free(client_list);
            return (srvResults(status, NULL));
        }

        /* Get the ncvflg */
        row = sqlGetRow(res);
        vc_ncvflg = sqlGetLong(res, row, "vc_ncvflg");
        sqlFreeResults(res);
        
        /* If this isn't set, we don't want to do the calculations */
        if (!vc_ncvflg)
        {
            /* 
             * Find the untcas quantity by calling get shipmnet 
             * pick estimate.  We will add this to the shipment 
             * line.  Later we'll take cartonization into account.
             */

            misTrc (T_FLOW, 
                    "Part %s case picks will be determined.",
                    sqlGetString(slRes, slRow, "prtnum"));

            sprintf(buffer,
                    "  get shipment pick estimate "
                    "where ship_id = '%s' " 
                    "  and ship_line_id = '%s' ",
                    sqlGetString(slRes, slRow, "ship_id"),
                    sqlGetString(slRes, slRow, "ship_line_id"));

            status = srvInitiateCommand(buffer, &CurPtr);
            if (eOK != status)
            {
                srvFreeMemory(SRVRET_STRUCT, CurPtr);
                srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
                if (prtnum_list) free(prtnum_list);
                if (untqty_list) free(untqty_list);
                if (shipline_list) free(shipline_list);
                if (client_list) free(client_list);
                misTrc(T_FLOW, "Error while getting pick estimates, exiting.");
                return (srvResults(status, NULL));
            }

            res = srvGetResults(CurPtr);
            row = sqlGetRow(res);

            wavcas_qty = (double) sqlGetLong(res, row, "numcpk");
            numpck = sqlGetLong(res, row, "numpck");

            srvFreeMemory(SRVRET_STRUCT, CurPtr);
            CurPtr = NULL;

            misTrc(T_FLOW, 
                   "Got pick estimates for shipment line %s.  "
                   "Case picks: %f - Piece picks available for "
                   "cartonization: %d.",
                   sqlGetString(slRes, slRow, "ship_line_id"),
                   wavcas_qty,
                   numpck);

            /* Only add if we have pice picks available */
            if (numpck > 0)
            {
		misTrc(T_FLOW,"started this");
                /* Now we can add all of the appropriate values to the lists. */
                num_lines++;

		memset(tmpstr, 0, sizeof(tmpstr));

		sprintf(tmpstr,
			"%s'%s'",
			shipline_list ? "," : "",
			sqlGetString(slRes, slRow, "ship_line_id"));

                if (!misDynStrcat(&shipline_list, tmpstr))
                {
                    srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
                    if (prtnum_list) free(prtnum_list);
		    if (client_list) free(client_list);
                    if (untqty_list) free(untqty_list);
                    if (shipline_list) free(shipline_list);
                    return(srvResults(eNO_MEMORY, NULL));
                }

                sprintf(tmpstr, 
                        "%s%s",
                        prtnum_list ? "," : "",
                        sqlGetString(slRes, slRow, "prtnum"));

                if (!misDynStrcat(&prtnum_list, tmpstr))
                {
                    srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
                    if (prtnum_list) free(prtnum_list);
                    if (untqty_list) free(untqty_list);
                    if (shipline_list) free(shipline_list);
                    if (client_list) free(client_list);
                    return(srvResults(eNO_MEMORY, NULL));
                }

                sprintf(tmpstr, 
                        "%s%s",
                        client_list ? "," : "",
                        sqlGetString(slRes, slRow, "prt_client_id"));

                if (!misDynStrcat(&client_list, tmpstr))
                {
                    srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
                    if (prtnum_list) free(prtnum_list);
                    if (untqty_list) free(untqty_list);
                    if (shipline_list) free(shipline_list);
                    if (client_list) free(client_list);
                    return(srvResults(eNO_MEMORY, NULL));
                }

                sprintf(tmpstr, 
                        "%s%d",
                        untqty_list ? "," : "",
                        numpck);

                if (!misDynStrcat(&untqty_list, tmpstr))
                {
                    srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
                    if (prtnum_list) free(prtnum_list);
                    if (untqty_list) free(untqty_list);
                    if (shipline_list) free(shipline_list);
                    if (client_list) free(client_list);
                    return(srvResults(eNO_MEMORY, NULL));
                }

                misTrc(T_FLOW,
                       "Lists so far:\n"
                       "\tShipment Line: %s\n"
                       "\tParts: %s\n"
                       "\tClients: %s\n"
                       "\tPieces: %s",
                       shipline_list,
                       prtnum_list,
                       client_list,
                       untqty_list);

            } /* numpck > 0 */
        }
        else /* vc_ncvflg = 0 */
        {
            wavcas_qty = 0.0;
            misTrc (T_FLOW, 
                    "Part %s case picks will NOT be tracked.",
                    sqlGetString(slRes, slRow, "prtnum"));
        }

        /* Finally, update the shipment line */
        sprintf(buffer,
                "update shipment_line "
                "   set vc_wavcas_qty = %f "
                " where ship_line_id = '%s' ",
                wavcas_qty,
                sqlGetString(slRes, slRow, "ship_line_id"));
            
        status = sqlExecStr(buffer, NULL);

        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
            if (prtnum_list) free(prtnum_list);
            if (untqty_list) free(untqty_list);
            if (shipline_list) free(shipline_list);
            if (client_list) free(client_list);
            return(srvResults(status, NULL));
        }
    } /* Looping around shipment lines with ShipLinePtr and slRes */

    /* Free the shipment line results */
    srvFreeMemory(SRVRET_STRUCT, ShipLinePtr);
    ShipLinePtr = NULL;

    /*
     * Now all of the case picks have been accounted for.  Next we 
     * have to estimate how many cartons will be created from this 
     * list.
     */


    /* MR 1723 START */
    if (num_lines)
    {
	status = sHandleCartonizationCall(prtnum_list,
					  client_list,
					  untqty_list,
					  shipline_list,
					  num_lines);
	if (prtnum_list) free(prtnum_list);
	if (untqty_list) free(untqty_list);
	if (shipline_list) free(shipline_list);
	if (client_list) free(client_list);
	return(srvResults(status, NULL));
    }
    /* MR 1723 END */


    /* Free up memory if we used it */

    if (prtnum_list) free(prtnum_list);
    if (untqty_list) free(untqty_list);
    if (shipline_list) free(shipline_list);
    if (client_list) free(client_list);

    return (srvResults(eOK, NULL));
} 
 

