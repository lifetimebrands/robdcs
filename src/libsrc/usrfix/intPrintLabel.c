static char     rcsid[] = "$Id: intPrintLabel.c 210853 2009-07-25 20:26:46Z aying $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#define LOC_FMTLBL_LEN 5000 UTF8_SIZE

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"

static void     set_print_i8000(char *cmd);
static void     set_data_i8000(char *cmd, char *data);
static void     set_print_zebra(char *cmd);
static void     set_data_zebra(char *cmd, char *data);

static long QueueLabel(char *outfile, 
		       char *queue, 
		       moca_bool_t delflg);
static long WriteLabelFile(FILE *out_fp,
			   char *data, char *prttyp, 
			   char *format, char *wh_id_i, int lblqty);
static long WriteGenericDocumentFile (FILE *fp_out, char *prttyp, 
				      char *filnam, int docqty);
static long WritePassThruDocumentFile (FILE *fp_out, char *prttyp, 
				       char *filnam, int docqty);
static long BuildPrinterList(char *prtadr, char *netlistadr);

/*
 * intPrintLabel will get the next label from the prswrk
 * for the net/ptr combination.
 */
LIBEXPORT 
RETURN_STRUCT *intPrintLabel(char *prtadr_i, moca_bool_t *delflg_i, 
                             char *label_wrkref_i,
                             char *wh_id_i)
{
    RETURN_STRUCT  *CurPtr;
    long            ret_status;
    long            i;
    long            lblqty;
    char            buffer[2000];
    char            varbuf[1024];
    char            prtadr[PRTADR_LEN + 1];
    moca_bool_t     delflg;
    char            format[LBLFMT_LEN + 1];
    char            lbdata[LBDATA_LEN + 1];
    char            prttyp[PRTTYP_LEN + 1];
    char            prtque[PRTQUE_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            datatypes[utf8Size(50)];
    char            netadrlist[utf8Size(500)];
    long            prsreq;
    long            total = 0;
    mocaDataRes     *res;
    mocaDataRow    *row;
    char            fmtlbl[LOC_FMTLBL_LEN];
    char            outfile[FILNAM_LEN]; 
    FILE           *out_fp; 
    char            label_wrkref[WRKREF_LEN+1];
 
    /* Validate  prtadr */
    memset(prtadr, 0, sizeof(prtadr));
    memset(prtque, 0, sizeof prtque);
    memset(label_wrkref, 0, sizeof(label_wrkref));
    memset(wh_id, 0, sizeof(wh_id));
    
    if (wh_id_i && strlen(wh_id_i))
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
    else    
        return APPMissingArg("wh_id");
  
    if (prtadr_i && misTrimLen(prtadr_i, PRTADR_LEN))
	strncpy(prtadr, prtadr_i, misTrimLen(prtadr_i, PRTADR_LEN));
    else
	return (srvSetupReturn(eINVALID_ARGS, ""));

    if (delflg_i )
        delflg = *delflg_i;
    else
        delflg = BOOLEAN_TRUE;
    
    if (label_wrkref_i && misTrimLen(label_wrkref_i, WRKREF_LEN))
        misTrimcpy(label_wrkref, label_wrkref_i, WRKREF_LEN);

    /* Select to see if the printer is re-routed */
    sprintf(buffer,
	    "select prttyp, prtque "
	    "from prsmst "
	    "where prtadr = '%s'"
	    "  and rerprt is null",
	    prtadr);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
	CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK, "");
	return (CurPtr);
    }

    row = sqlGetRow(res);

    misTrimcpy(prttyp, sqlGetValue(res, row, "prttyp"), PRTTYP_LEN);
    if (!sqlIsNull(res,row,"prtque"))
	misTrimcpy(prtque, sqlGetValue(res, row, "prtque"), PRTQUE_LEN);

    sqlFreeResults(res);

    /*
     * Build a list of printer address to use in further selects
     * BuildPrintList is a recursive function that will find the
     * reroute printers for the printer, then the reroute printers for
     * the reroute printers, then the reroute printers for the reroute
     * printers of the reroute printers ...
     */
    
    memset (netadrlist, 0, sizeof netadrlist);
    sprintf(netadrlist, "'%s'", prtadr);
    ret_status = BuildPrinterList (prtadr, netadrlist);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
	return srvSetupReturn(ret_status, "");
    }
    
    if (misTrimLen(label_wrkref,WRKREF_LEN))
    {
        /* This will only print for the passed in work reference number */
         sprintf(buffer,
                 "select prsreq, lblqty, lbdata, lblfmt"
            	 "  from prswrk"
            	 " where prtadr in (%s) and wrkref = '%s' "
            	 " order by lbldte, prsreq", netadrlist, label_wrkref);
    }
    else
    {
        /* This select will get the Next label for this printer
           or any printers that have been re-routed to this one */
        sprintf(buffer,
            	"select prsreq, lblqty, lbdata, lblfmt"
            	"  from prswrk"
            	" where prtadr in (%s) "
            	" order by lbldte, prsreq", netadrlist);
    }

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	sqlFreeResults(res);
	return srvResults(ret_status, NULL);
    }

    /* If they passed a label_wrkref value then we're going to
     * generate a label file for each wrkref, rather than for the
     * entire process id.
     */

    if (misTrimLen(label_wrkref, WRKREF_LEN))
    {
        /* this may need to be moved */
        sprintf(outfile, "%s%clabel.%s", 
            misExpandVars(varbuf, DCS_REPORTS, sizeof(varbuf), NULL),
            PATH_SEPARATOR, label_wrkref);
    }
    else
    {
        /* this may need to be moved */
        sprintf(outfile, "%s%clabel.%d", 
	    misExpandVars(varbuf, DCS_REPORTS, sizeof(varbuf), NULL),
	    PATH_SEPARATOR, osGetProcessId());
    }

    /*
     * Open the output file.
     */

    out_fp = osFopen(outfile, "w");
    if (!out_fp)
    {
	misLogError("Error opening output file %s", outfile);
        return srvSetupReturn(ret_status, "");
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	total++;
        misTrc(T_FLOW, "processing label data for label #: %d", total);

	memset(lbdata, 0, sizeof(lbdata));
	memset(format, 0, sizeof(format));
	lblqty = 0;
	prsreq = sqlGetLong(res, row, "prsreq");
	misStrncpyChars(lbdata, 
	    misTrim((char*)sqlGetValue(res,row,"lbdata")),
	    LBDATA_LEN);
	misStrncpyChars(format, 
	    misTrim((char*)sqlGetValue(res,row,"lblfmt")),
	    LBLFMT_LEN);
	lblqty = sqlGetLong(res, row, "lblqty");

	/* Just a little firewall here */
	if (lblqty <= 0)
	    lblqty = 1;

	/* If print request is for a generic document, */
	/* then process generic document file          */

	if (strcmp (format, LBLFMT_GENDOC) == 0)
	{
	    ret_status = WriteGenericDocumentFile (out_fp, 
						   prttyp, lbdata, lblqty);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		fclose(out_fp);
		return srvSetupReturn(ret_status, "");
	    }
 	    if (delflg == BOOLEAN_TRUE)
 	    {
		remove(lbdata);
	    }
	}

	/* Else if print request is for a pass-thru document, */
	/* then process pass-thru document file               */

	else if (strcmp (format, LBLFMT_PASDOC) == 0)
	{
	    ret_status = WritePassThruDocumentFile (out_fp, 
						    prttyp, lbdata, lblqty);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
                fclose(out_fp);
		return srvSetupReturn(ret_status, "");
	    }
 	    if (delflg == BOOLEAN_TRUE)
 	    {
		remove(lbdata);
	    }
	}

	/* Else process label file using old hard-coded mechanisms */

	else
	{

	    /* Build the data string for the printer */
	    memset(fmtlbl, 0, sizeof(fmtlbl));

	    if (misStrncmpChars(prttyp, PRTTYP_INTERMEC, PRTTYP_LEN) == 0)
	    {
		set_data_i8000(fmtlbl, lbdata);
		set_print_i8000(fmtlbl);
	    }
	    else
	    {
		set_data_zebra(fmtlbl, lbdata);
		set_print_zebra(fmtlbl);
	    }

	    ret_status = WriteLabelFile(out_fp, fmtlbl, prttyp, format, wh_id,
	                                lblqty);

	    if (ret_status != eOK)
	    {
		sqlFreeResults(res);
		fclose(out_fp);
		return srvSetupReturn(ret_status, "");
	    }

	}
	
	misTrc(T_FLOW, "deleting the completed print request");

	/* Delete the completed print request */

        sprintf(buffer, "delete from prswrk where prsreq = %ld", prsreq);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
	    misLogInfo ("INTLIB: Error %d deleting prswrk entry %d", ret_status, prsreq);
	    fclose(out_fp);
            return srvSetupReturn(ret_status, "");
	}
    }

    sqlFreeResults(res);

    /* close the batch of labels file */
    fclose(out_fp);

    /* Print the batch of labels */

    ret_status = QueueLabel(outfile, strlen(prtque)?prtque:NULL, delflg);
    if (ret_status != eOK)
    {
	misLogInfo ("INTLIB: Error %d queueing label file %s to queue %s",
		   ret_status, outfile, prtque);
	return (srvSetupReturn (ret_status, ""));
    }

    /* Setup the Return and let it go */
    memset(datatypes, 0, sizeof(datatypes));

    /* Pass in the prsreq so the cancel can return it and be sure we are 
       deleting the record */
    srvSetupColumns(3);		/* Header indicates x columns */
    i = 0;
    datatypes[i] = srvSetColName(i + 1, "prtadr", COMTYP_CHAR, PRTADR_LEN);
    i++;
    datatypes[i] = srvSetColName(i + 1, "lblqty", COMTYP_INT, sizeof(long));
    i++;
    datatypes[i] = srvSetColName(i + 1, "filnam", COMTYP_CHAR, FILNAM_LEN);
    i++;

    CurPtr = (RETURN_STRUCT *) srvSetupReturn(eOK, datatypes,
					      prtadr,
					      total,
					      outfile); 

    return (CurPtr);
}

#define STX                     2
#define ETX                     3
#define ENQ                     5
#define ACK                     6
#define SEL                     7
#define DLE                     16
#define FF                      12
#define CR                      13
#define NAK                     21
#define ETB                     23
#define CAN                     24
#define EM                      25
#define ESC                     27

#define DEL                     127

/*
 * Queue the label file to a particular print queue.
 */
static long QueueLabel(char *outfile, char *queue, moca_bool_t delflg)
{

    long            ret_status;

    ret_status = osPrintFile(outfile, queue, 1, NULL, 1);

    if (ret_status == eOK)
    {
        if (delflg == BOOLEAN_TRUE)
            remove(outfile); 
    }
    else
    {
      misLogError("Error printing file %s to queue %s", outfile, queue);
    }

    return (ret_status);
}

static void set_data_i8000(char *cmd, char *data)
{
    char            tmpstr[utf8Size(50)];
    char            temp_mesg[LOC_FMTLBL_LEN];
    char            buffer[LOC_FMTLBL_LEN];
    long            buf_len;
    long            i;
    long            field;

    field = 0;

    memset(buffer, 0, sizeof(buffer));
    strcpy(buffer, data);

    memset(temp_mesg, 0, sizeof(temp_mesg));

    sprintf(tmpstr, "%cF%ld%c", ESC, field, DEL);
    strcat(temp_mesg, tmpstr);
    field++;

    buf_len = strlen(buffer) - 1;
    while ((buf_len > 0) &
	   (buffer[buf_len] != '~'))
    {
	buffer[buf_len] = 0;
	buf_len--;
    }

    buf_len = strlen(buffer);
    for (i = 0; i < buf_len; i++)
    {
	if (buffer[i] == '~')	/* if start of new field  */
	{
	    if (i != buf_len - 1)
	    {
		sprintf(tmpstr, "%cF%ld%c", ESC, field, DEL);
		strcat(temp_mesg, tmpstr);
		field++;
	    }
	}
	else
	{
	    temp_mesg[strlen(temp_mesg)] = buffer[i];
	}
    }

    strcat(cmd, temp_mesg);

    return;
}

static void set_print_i8000(char *cmd)
{
    char            temp_mesg[utf8Size(20)];

    memset(temp_mesg, 0, sizeof(temp_mesg));
    temp_mesg[0] = ETB;
    temp_mesg[1] = 12;		/* put out a form feed */
    temp_mesg[2] = ETX;		/* put on an EOT */
    temp_mesg[3] = ESC;		/* put on an ESC */

    strcat(cmd, temp_mesg);

    return;
}
static void set_data_zebra(char *cmd, char *data)
{
    char            tmpstr[utf8Size(50)];
    char            temp_mesg[LOC_FMTLBL_LEN];
    char            buffer[LOC_FMTLBL_LEN];
    long            buf_len;
    long            i;
    long            field;

	misTrc(T_FLOW, "In set_data_zebra...");
    
	field = 1;

    memset(buffer, 0, sizeof(buffer));
    strcpy(buffer, data);
	
	misTrc(T_FLOW, "Value of buffer is %s", buffer);

    memset(temp_mesg, 0, sizeof(temp_mesg));

    sprintf(tmpstr, "^FS^FN%ld^FH*^FD", field);
    strcat(temp_mesg, tmpstr);
    field++;

	misTrc(T_FLOW, "temp_mesg = %s, tmpstr = %s", temp_mesg, tmpstr);
	
    buf_len = strlen(buffer) - 1;
    while ((buf_len > 0) &
	   (buffer[buf_len] != '~'))
    {
	buffer[buf_len] = 0;
	buf_len--;
    }

    buf_len = strlen(buffer);
    for (i = 0; i < buf_len; i++)
    {
	if (buffer[i] == '~')
	{
	    if (i != buf_len - 1)
	    {
		sprintf(tmpstr, "^FS^FN%ld^FH*^FD", field);
		strcat(temp_mesg, tmpstr);
		misTrc(T_FLOW, "temp_mesg = %s, tmpstr = %s", temp_mesg, tmpstr);
		field++;
	    }
	}
	else
	{
	    temp_mesg[strlen(temp_mesg)] = buffer[i];
	}
    }

    strcat(cmd, temp_mesg);
	
	misTrc(T_FLOW, "temp_mesg = %s, tmpstr = %s", temp_mesg, tmpstr);
	
    return;
}
static void set_print_zebra(char *cmd)
{
    char            temp_mesg[utf8Size(20)];

    memset(temp_mesg, 0, sizeof(temp_mesg));

    strcat(temp_mesg, "^FS^XZ");

    strcat(cmd, temp_mesg);

    return;
}

static int fix_line(char *line, char *prttyp)
{
    misTrc(T_FLOW, "Inside fix_line..");
	misTrc(T_FLOW, "value of line is %s", line);
	misTrc(T_FLOW, "value of prttyp is %s", prttyp);
	
	int linelen;

    if (line[0] == '#')
	return 0;
    linelen = strlen(line);
    if (linelen > 1 && line[linelen-2] == ';' && line[linelen-1] == '\n')
    {
	line[linelen - 2] = '\n';
	line[linelen - 1] = '\0';
	linelen--;
    }
    else if (linelen > 1 && line[linelen-1] == ';')
    {
	line[linelen - 1] = '\0';
	linelen--;
    }

    if (linelen > 1 &&prttyp &&
	0 == misStrncmpChars(prttyp, PRTTYP_INTERMEC, PRTTYP_LEN) &&
	line[linelen-1] == '\n')
    {
	if (line[0] != STX)
	{
	    memmove(line+1, line, linelen+1);
	    line[0]=STX;
	    linelen++;
	}
	if (line[linelen-2] != ETX)
	{
	    line[linelen-1] = ETX;
	    line[linelen] = '\n';
	    line[linelen+1]='\0';
	    linelen++;
	}
    }
    return 1;
}
static FILE *sOpenFormatFile(char *prefix,
			     char *suffix,
			     char *format)
{

    char file[1000];
    char varbuf[1000];
    FILE *fp;
    char tmp_format[utf8Size(500)];
    char *p;

	misTrc(T_FLOW, "In sOpenFormatFile....");
	
    strcpy(tmp_format, format);
    for (p = tmp_format; *p; p++)
	*p = tolower(*p);

    memset(file, 0, sizeof(file));

    sprintf(file,
	    "%s%c%s%s%s",
	    misExpandVars(varbuf, LES_LABELS, sizeof(varbuf), NULL),
	    PATH_SEPARATOR, 
	    prefix,
	    tmp_format,
	    suffix);

    misTrc(T_FLOW, "Attempting to open file: %s", file);
    fp = osFopen(file, "r");
    if (fp != NULL)
	misTrc(T_FLOW, "Success");
    
    if (fp == NULL)
    {
	misTrc(T_FLOW, "failed...trying DCS directory");
	sprintf(file,
		"%s%c%s%s%s",
		misExpandVars(varbuf, DCS_LABELS, sizeof(varbuf), NULL),
		PATH_SEPARATOR, 
		prefix,
		tmp_format,
		suffix);

	misTrc(T_FLOW, "Attempting to open file: %s", file);
	fp = osFopen(file, "r");
	if (fp != NULL)
	    misTrc(T_FLOW, "Success");
	else
	    misTrc(T_FLOW, "Failed..");
    }
    /* If we still haven't successfully opened the file, try again,
     * but leave off the suffix/prefix
     */
    if (fp == NULL && (strlen(prefix) || strlen(suffix)))
	return(sOpenFormatFile("","", tmp_format));

    return(fp);
}

static long WriteLabelFile(FILE *out_fp, 
			   char *data, 
			   char *prttyp,
			   char *format,
			   char *wh_id, 
			   int lblqty)
{
    FILE *fp;
    char line[1000];
    char prefix[100], suffix[utf8Size(100)];
    char sfrmfile[200], sqtyfile[utf8Size(200)];
    char buffer[1000];
    long ret_status;
    int  first_line;
    char formid[utf8Size(100)];
    
    misTrc(T_FLOW, "In WriteLabelFile...");
        
    mocaDataRes *polRes;
    mocaDataRow *polRow;
    
    sprintf(buffer, "select rtstr1, rtstr2 "
		    "  from poldat_view"
		    " where polcod = 'LABEL-PRINTERS'"
		    "   and polvar = '%s'"
		    "   and polval = 'LABEL-PREFIX-N-SUFFIX'"
		    "   and wh_id  = '%s'",
		    prttyp,
		    wh_id);
    ret_status = sqlExecStr(buffer, &polRes);
    if (ret_status != eOK)
    {
	sqlFreeResults(polRes);
	return ret_status;
    }

    polRow = sqlGetRow(polRes);
    misTrimcpy(prefix, 
	       sqlGetValue(polRes, polRow, "rtstr1"), sizeof prefix - 1);
    misTrimcpy(suffix, 
	       sqlGetValue(polRes, polRow, "rtstr2"), sizeof suffix - 1);
    sqlFreeResults(polRes);

    sprintf(buffer, 
	    "select rtstr1, rtstr2 "
	    "  from poldat_view"
	    " where polcod = 'LABEL-PRINTERS'"
	    "   and polvar = '%s'"
	    "   and polval = 'LABEL-CONTROL-FILENAMES'"
	    "   and wh_id  = '%s'",
	    prttyp,
	    wh_id);
    ret_status = sqlExecStr(buffer, &polRes);
    if (ret_status != eOK)
    {
	sqlFreeResults(polRes);
	return ret_status;
    }

    polRow = sqlGetRow(polRes);
    misTrimcpy(sfrmfile,
	       sqlGetValue(polRes, polRow, "rtstr1"), sizeof sfrmfile - 1);
    misTrimcpy(sqtyfile,
	       sqlGetValue(polRes, polRow, "rtstr2"), sizeof sqtyfile - 1);
    sqlFreeResults(polRes);


    /*
     * Get the form file. Put it into the output file.
     */

    fp = sOpenFormatFile(prefix, suffix, format);
    if (!fp)
    {
	misLogError("Error opening label format file (%s, %s, %s)", 
		    prefix, format, suffix);
	return eERROR;
    }

	misTrc(T_FLOW, "Got the format file. Continuing....");
	
    first_line = 1;
    memset(formid, '\0', sizeof formid);
    while (fgets(line, sizeof line, fp))
    {
	if (fix_line(line, prttyp))
	{
	    /*
	     * OK, this is nasty, but if this is an intermec printer, we
	     * can only refer to forms by their "slot number".  We'll attempt
	     * to grab the slot number from the first "form" line of the 
	     * form file. (not the comment lines).  We're looking for
	     * text of the form F<num>;  We'll copy down the form number
	     * for later.
	     */
	    if (first_line && 
		misStrncmpChars(prttyp, PRTTYP_INTERMEC, PRTTYP_LEN) == 0)
	    {
		char *formpos, *semipos;

		first_line = 0;
		if ((formpos = strchr(line,'F')))
		{
		    semipos = strchr(formpos, ';');
		    if (semipos)
		    {
			strncpy(formid, formpos+1, semipos-formpos-1);
			formid[semipos-formpos-1]='\0';
		    }
		}
		
	    }
	    fputs(line, out_fp);
	}
    }

	misTrc(T_FLOW, "closing after processing format file...");
	
    fclose(fp);
	
	misTrc(T_FLOW, "file closed...");

    /*
     * Get the sfrm file, put it into the output file.
     */
    fp = sOpenFormatFile("","",sfrmfile);
    if (!fp)
    {
	misLogError("Error opening label sfrm file %s",
		    sfrmfile);
	return eDB_NO_ROWS_AFFECTED;
    }

    /* This should only be one line */
    if (fgets(line, sizeof line, fp))
    {
	line[strlen(line)-1] = '\0'; /* Chop the last character (newline) */
	fix_line(line, prttyp);

	/*
	 * If we're intermec, do it with the form number, otherwise
	 * use the form name.
	 */
	if (misStrncmpChars(prttyp, PRTTYP_INTERMEC, PRTTYP_LEN) == 0)
	    strcat(line,formid);
        else
	    strcat(line,format);

	fputs(line, out_fp);
    }

	misTrc(T_FLOW, "closing after processing sfrm file...");
	
    fclose(fp);
	
	misTrc(T_FLOW, "file closed...");

    /*
     * Get the sqty file, put it into the output file.  This is used to
     * set the number of copies to print.  Doing this via the printer is
     * somewhat more efficient, and generally more reliable than using
     * print queues.
     */
    fp = sOpenFormatFile("","",sqtyfile);
    if (!fp)
    {
	misLogError("Error opening label sqty file %s",
		    sqtyfile);
	return eDB_NO_ROWS_AFFECTED;
    }

    /* This should only be one line */
    if (fgets(line, sizeof line, fp))
    {
	char tmpbuf[utf8Size(20)];
	line[strlen(line)-1] = '\0'; /* Chop the last character (newline) */
	fix_line(line, prttyp);

	/*
	 * If we're intermec, do it with the form number, otherwise
	 * use the form name.
	 */
	sprintf(tmpbuf,"%d",lblqty);
	strcat(line, tmpbuf);
	fputs(line, out_fp);
    }
	
	misTrc(T_FLOW, "closing after processing sqty file...");

    fclose(fp);
	
	misTrc(T_FLOW, "file closed...");

    /*
     * Now, play with the data element, putting it into the output file.
     * We need to break this puppy up into lines.
     */
	 
	misTrc(T_FLOW, "Putting data element into output file...prttyp = %s", prttyp);
    { /* for scope */
	char *p, *thisline;
	thisline = data;
	misTrc(T_FLOW, "Starting...prttyp = %s", prttyp);
	misTrc(T_FLOW, "value of data is %s", data);
	for (p=strchr(data, '\n'); p; p=strchr(p+1, '\n'))
	{
	    misTrc(T_FLOW, " inside for loop...");
		
		strncpy(line, thisline, p-thisline);
	    line[p-thisline]='\0';
	    if (fix_line(line, prttyp))
		fputs(line, out_fp);
	    thisline = p;
	}
	misTrc(T_FLOW, "After for loop...prttyp = %s", prttyp);
	
	/*strcpy(line, thisline);*/
	misTrc(T_FLOW, "String copied...prttyp = %s", prttyp);
	misTrc(T_FLOW, "Calling fix_line with prttyp = %s and thisline = %s", prttyp, thisline);
	
	if (fix_line(thisline, prttyp))
	{
	    misTrc(T_FLOW, "inside if..");
		fputs(thisline, out_fp);
	}
    }
	
	misTrc(T_FLOW, "exiting..");
	
    return eOK;
}

static long WriteGenericDocumentFile (FILE *fp_out, 
				      char *prttyp, 
				      char *filnam, 
				      int docqty)
{
    RETURN_STRUCT *returnValues;
    mocaDataRes	  *resultSet;
    mocaDataRow	  *resultRow;

    FILE	  *fp_img;

    char	  buffer [1000];
    char	  imgfil [FILNAM_LEN];

    long	  doccnt;
    long	  rd_cnt;
    long	  status;
    long	  wt_cnt;

    /* Create document print image file */

    sprintf (buffer,
	     "create document print image where prttyp='%s' and filnam='%s'",
	     prttyp, filnam);

    status = srvInitiateCommand (buffer, &returnValues);
    if (status != eOK)
    {
	misLogInfo ("Error %ld generating document "
		    "print image file using %s & %s",
		    status, prttyp, filnam);
        srvFreeMemory (SRVRET_STRUCT, returnValues);
	return (status);
    }
    resultSet = returnValues->ReturnedData;
    resultRow = sqlGetRow (resultSet);
    misTrimcpy (imgfil, 
		sqlGetValue (resultSet, resultRow, "filnam"), sizeof (imgfil));
    srvFreeMemory (SRVRET_STRUCT, returnValues);

    /* Open document print image file */

    fp_img = osFopen (imgfil, "rb");
    if (fp_img == NULL)
    {
	misLogInfo ("Error %ld opening print image file %s", errno, imgfil);
	return (errno);
    }

    /* Do for each copy of the document to print */

    for (doccnt = 0; doccnt < docqty; doccnt++)
    {

	/* If not first copy, then rewind image file */

	if (doccnt)
	{
	    rewind (fp_img);
	}

	/* Copy image file contents to output file */

	while (rd_cnt = fread (buffer, 1, sizeof (buffer), fp_img))
	{
	    wt_cnt = fwrite (buffer, 1, rd_cnt, fp_out);
	    if (wt_cnt != rd_cnt)
	    {
	        misLogInfo ("INTLIB: Error %ld writing to output file", errno);
		perror ("Error writing to output file");
		fclose(fp_img);
		return (errno);
	    }
	}
	if (ferror (fp_img))
	{
	    misLogInfo("Error %ld reading from image file %s", errno, imgfil);
	    fclose(fp_img);
	    return (errno);
	}
   }

   fclose(fp_img);

   /* Delete temporary image file */

   status = remove (imgfil);
   if (status != eOK)
   {
       misLogInfo ("Error %ld deleting print image file %s", status, imgfil);
   }

   return (eOK);
}

static long WritePassThruDocumentFile (FILE *fp_out,
				       char *prttyp,
				       char *filnam,
				       int docqty)
{
    FILE	  *fp_img;

    char	  buffer [1000];
    char	  imgfil [FILNAM_LEN];

    long	  doccnt;
    long	  rd_cnt;
    long	  wt_cnt;

    /* Open finished document image file */

    fp_img = osFopen (filnam, "rb");
    if (fp_img == NULL)
    {
	misLogInfo ("INT: Error %ld opening print image file %s", 
		    errno, filnam);
	return (errno);
    }

    /* Do for each copy of the document to print */

    for (doccnt = 0; doccnt < docqty; doccnt++)
    {

	/* If not first copy, then rewind image file */

	if (doccnt)
	{
	    rewind (fp_img);
	}

	/* Copy image file contents to output file */

	while (rd_cnt = fread (buffer, 1, sizeof (buffer), fp_img))
	{
	    wt_cnt = fwrite (buffer, 1, rd_cnt, fp_out);
	    if (wt_cnt != rd_cnt)
	    {
	        misLogInfo ("Error %ld writing to output file", errno);
		fclose(fp_img);
		return (errno);
	    }
	}
	if (ferror (fp_img))
	{
	    misLogInfo ("Error %ld reading from image file %s", errno, imgfil);
	    fclose(fp_img);
	    return (errno);
	}
   }

   fclose(fp_img);

   return (eOK);
}

/* This is a recursive function to add the reroute printers for a printer
 * For following example, if prtadr is A, then B, C, D, E, F, G are added to 
 * the list.  B and C are reroute printers for A.  D is a reroute printer for B.
 * E is a reroute printer for C.  F and G are reroute printers for E.
 * 	
 * 	prtadr		rerprt
 * 	------		------
 * 	A		NULL
 * 	B		A
 * 	C		A
 * 	D		B
 * 	E		C
 * 	F		E
 * 	G		E
 */


static long BuildPrinterList (char *prtadr, char *netadrlist)
{

    long	ret_status;
    
    char	buffer[utf8Size(400)];
    char	tmp_prtadr[PRTADR_LEN+1];
    
    mocaDataRes    *res;
    mocaDataRow    *row;
    
    sprintf(buffer,
	    "select prtadr from prsmst "
	    " where rerprt = '%s' ",
	    prtadr);

    ret_status = sqlExecStr(buffer, &res);

    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	return(ret_status);
    }

    for (row = sqlGetRow(res); row; row=sqlGetNextRow(row))
    {
	misTrimcpy(tmp_prtadr, (char *) sqlGetValue(res,row,"prtadr"),
		   PRTADR_LEN);
	
	/* If not in printer list, put it in the list */
	sprintf(buffer, ",'%s'", tmp_prtadr);
	if (strstr(netadrlist,buffer) == 0)
	{
	    strcat(netadrlist, buffer);
	    
	/* Recursive call to add to the list any reroute printers for 
	 * this re-route printer */
	    ret_status = BuildPrinterList (tmp_prtadr, netadrlist);
            if ( (ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED) )
            {
	        sqlFreeResults(res);
	        return(ret_status);
            }
	}
    }

    sqlFreeResults(res);
    res = NULL;

    return (eOK);
}    
