/*#START***********************************************************************
 *
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $URL: https://athena.redprairie.com/svn/prod/wmd/tags/2013.2.1/src/libsrc/dcsrf/rflib.h $
 *  $Revision: 210852 $
 *  $Id: rflib.h 210852 2009-07-25 19:28:20Z aying $
 *
 *  Application:  Intrinsic Library
 *  Created:   31-Jan-1994
 *  $Author: aying $
 *
 *  Purpose:   Structure defs for RFLIB
 *
 *#END************************************************************************/

#include <applib.h>

#ifndef RFLIB_H
#define RFLIB_H

/* RF length definitions */

#define RF_BUFFER_LEN		2000 UTF8_SIZE
#define RF_FRMNAM_LEN		30 UTF8_SIZE


/* Library function prototypes */

long rfGetLong (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);

char *rfGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam);

long rfLogMsg (char *header, char *format, ...);

#endif

