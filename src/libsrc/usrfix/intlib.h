/*#START***********************************************************************

*

*  McHugh Software International

*  Copyright 1999

*  Waukesha, Wisconsin,  U.S.A.

*  All rights reserved.

*

*  Purpose:   Structure defs for INTLIB

*

*#END************************************************************************/

 

#ifndef INTLIB_H

#define INTLIB_H

 

#include <applib.h>

/* We define the gloable var here because if it defined in

* trnAllocInv.c file, and trnAllocInv.c is customized

* which is pretty normal for project team, then there

* will be 2 different instances for this var,which

* will cause it does not working as expected.

*/

char save_alloc_pick_group_schbat[SCHBAT_LEN + 1];

#endif