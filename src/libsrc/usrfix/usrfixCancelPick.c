static const char *rcsid = "$Id: usrfixCancelPick.c 701142 2013-12-17 20:51:06Z tkervin $";
/*#START***********************************************************************
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "intlib.h"

/* logPalletControlEvent
 * This method is used to do all the processing
 * before the Pallet Control event is triggered.
 * This method checks if a carrier move exists for
 * the schbat, If so trigger the pallet control event
 * to get a plan from pallet control
*/
static long logPalletControlEvent(char *pck_car_move_id,
                                  char *cangrp,
                                  char *wrkref, 
                                  char *wrkref_dtl,
                                  char *schbat,
                                  char *wh_id,
                                  moca_bool_t *palctl_flg,
                                  char *cancod)
{
    mocaDataRes    *palres;
    mocaDataRow    *palrow;

    char            pallet_id[PALLET_ID_LEN + 1];
    char            palletsts[PALCTLSTS_LEN + 1];
    char            car_move_id[CAR_MOVE_ID_LEN + 1];
    char            carcod[CARCOD_LEN + 1];
    char            srvlvl[SRVLVL_LEN + 1];
    char            buffer[2000];
    moca_bool_t log_palctl_flg;
    moca_bool_t records_found_flg;
    long status;
    memset(pallet_id, 0, sizeof(pallet_id));
    memset(palletsts, 0, sizeof(palletsts));
    memset(car_move_id, 0, sizeof(car_move_id));
    memset(carcod, 0, sizeof(carcod));
    memset(srvlvl, 0, sizeof(srvlvl));
    memset(buffer, 0, sizeof(buffer));
    
    /* This function is simply trying to get a car_move_id 
     * that is enabled for PalletCtl and is associated to a schbat 
     * passed in.  If we end up getting a car_move_id either at 
     * the PCKWRK_HDR, RPLWRK, or XDKWRK levels, we can then log 
     * the event to PalletCtl.
     */
     
    log_palctl_flg = BOOLEAN_FALSE;
    records_found_flg = BOOLEAN_FALSE;

    /* If the palctl_flg_i is null, we are setting the 
     * log_palctl_flag  to true, because we want to 
     * trigger the pallet_control event, when the 
     * palctl_flg_i flag is not explicitly set to false.
     */
    if (!palctl_flg)
    {
       log_palctl_flg = BOOLEAN_TRUE;
    }
    else 
    {
       log_palctl_flg = *palctl_flg;
    }

    /* We are checking if there is a valid cancod ,
     * if there's one, we need to find out the reaflg is
     * set to true,so that each cancelled pick is reallocated,
     * if the cancelled pick is reallocated, we dont need to
     * trigger Pallet control event while cancelling, We just dont 
     * trigger it in cancel pick, set the log_palctl_flag to false, 
     * and let it trigger while reallocating.
     */
    if (cancod && misTrimLen(cancod, CODVAL_LEN) != 0)
    {
        sprintf(buffer,
                "select * "
                "  from cancod "
                " where codval = '%s' "
                "   and reaflg = %d ",
                cancod,
                BOOLEAN_TRUE);
        status = sqlExecStr(buffer,NULL);

        if (status == eOK)
        {
            /* We have a cancod, whose realloc flag is set 
             * so, we'll not trigger pallet control here
             * let the realloc component trigger it
             */
            log_palctl_flg = BOOLEAN_FALSE;
        }
    }
    
    /* If we have a valid cancod or palctl_flg is set we continue */
    if (log_palctl_flg == BOOLEAN_TRUE)
    {
        /* We are checkng to see if we have a partially loaded trailer
         * if we do we will get eINT_TRAILER_NOT_FULLY_LOADED.
         * If its not that error we want to back out as that means
         * either the trailer is fully loaded eOK or that there
         * are no allocated pallet control pallets -1403
         */
        sprintf(buffer,
                "check palletctl carrier move loaded "
                " where car_move_id = '%s' ",
                pck_car_move_id);
        
        status = srvInitiateCommand(buffer, NULL);
        
        if (status == eDB_NO_ROWS_AFFECTED)
        {
            misTrc(T_FLOW,
                   "Nothing has been allocated for car_move %s nothing to send.",
                   pck_car_move_id);
            return eOK;
        }
        else if (status == eOK)
        {
            misTrc(T_FLOW,
                   "The trailer is fully loaded for car_move %s nothing to send.",
                   pck_car_move_id);
            return eOK;
        }
        else if (status != eINT_TRAILER_NOT_FULLY_LOADED)
        {
            /* If we get here there would appear to be a syntax
             * error in "check palletctl carrier move loaded" so 
             * this else should hopefully never happen.
             * If we do get eINT_TRAILER_NOT_FULLY_LOADED
             * we want to continue as this means we have a partially
             * loaded trailer and need to re-plan the palletctl load
             */
            return status;
        }
        
        /* PCKWRK_HDR:
         * Check if the palletctl_flg is set for the carrier type
         * trnsp_mode table.if it's set, get the carrier move id 
         * tied to the carrier pallet Control requires carrier 
         * move id to make a pallet plan. If the schbat is related to
         * multiple car_move_ids, we could use the incorrect car_move_id
         * to log the event. So here we use the wrkref to limit it.
         */
        sprintf(buffer,
               "select stop.car_move_id, "
               "       car_move.carcod, "
               "       cardtl.srvlvl "
               "  from trnsp_mode, "
               "       cardtl,"
               "       car_move,"
               "       stop,"
               "       shipment,"
               "       canpck "
               " where trnsp_mode.palletctl_flg = 1 "
               "   and trnsp_mode.trnsp_mode    = nvl(car_move.trans_mode,"
               "                                 cardtl.cartyp) "
               "   and cardtl.carcod            = car_move.carcod "
               "   and cardtl.srvlvl            = (select max(srvlvl)"
               "                                    from ship_struct_view "
               "                                   where ship_struct_view.car_move_id = stop.car_move_id) "
               "   and car_move.car_move_id     = stop.car_move_id "
               "   and stop.stop_id             = shipment.stop_id "
               "   and shipment.ship_id         = canpck.ship_id "
               "   and exists (select 1 "
               "                 from pckwrk_dtl "
               "                where pckwrk_dtl.ship_id = canpck.ship_id "
               "                  and pckwrk_dtl.appqty < pckwrk_dtl.pckqty) "
               "   and canpck.schbat            = '%s' "
               "   and canpck.wrkref            = '%s' "
               "   and canpck.wrkref_dtl        = '%s' "
               "   and canpck.cangrp            = '%s' "
               " group by stop.car_move_id,"
               "       car_move.carcod,"
               "       cardtl.srvlvl ",
                schbat,
                wrkref,
                wrkref_dtl,
                cangrp);

        status = sqlExecStr(buffer, &palres);

        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
            /* Error needs to be returned */
            sqlFreeResults(palres);
            return status;
        }

        if (status == eDB_NO_ROWS_AFFECTED) 
        {
            /* If the carrier move id does not exist,
             *  We are not returning because we
             * would like to check rplwrk and cross docks now
             */
             misTrc(T_FLOW,
                    " Could not retrieve a Carrier Move ID  for "
                    " the pick work records for the schbat [%s] ",
                    schbat);
                    
            sqlFreeResults(palres);
        }
        
        if (status == eOK)
        {
            /* pckwrk_hdr records found */
            records_found_flg = BOOLEAN_TRUE;
            
            palrow = sqlGetRow(palres);
            strcpy(car_move_id, sqlGetString(palres, palrow, "car_move_id"));
            strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
            strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
            sqlFreeResults(palres);

            /* Update palctlsts with WPPP/WPPH based on the pcksts */
            sprintf(buffer,
                   "[select decode(pckwrk_view.pcksts,'%s','%s','%s','%s') sts,"
                   "        cmbcod "
                   "   from stop, "
                   "        shipment,"
                   "        pckwrk_view "
                   "  where pckwrk_view.ship_id   = shipment.ship_id "
                   "    and shipment.stop_id = stop.stop_id "
                   "    and stop.car_move_id = '%s'"
                   "    and pckwrk_view.pcksts in ('%s','%s' )]"
                   "| "
                   "change pick work "
                   " where filter_columns = 'cmbcod' "
                   "   and filter_values = '@cmbcod' " 
                   "   and palctlsts     = @sts ",
                   PCKSTS_PENDING,
                   WAITING_PLTCTL_PCKWRK_PEND,
                   PCKSTS_HOLD,
                   WAITING_PLTCTL_PCKWRK_HOLD,
                   car_move_id,
                   PCKSTS_PENDING,
                   PCKSTS_HOLD);

            status = srvInitiateCommand(buffer, NULL);
            
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                misTrc(T_FLOW,"Error in updating the palctlsts in pckwrk_hdr...");
                return status;
            }
        }
        
        /* Check if short processing is enabled for PalletCtl */
        status = appIsPolicyEnabled(POLCOD_PALLETCTL,
                                    POLVAR_PALLETCTL_CONFIG,
                                    POLVAL_ENABLE_SHORT_PROCESSING,
                                    wh_id);

        if (status == BOOLEAN_TRUE)
        {
            if (records_found_flg == BOOLEAN_FALSE)
            {                
                /* RPLWRK:
                 * If pickwrk records where not found for
                 * sending to pallet control, then we need 
                 * to look for replenishment records. */
                sprintf(buffer,
                       "select stop.car_move_id, "
                       "       car_move.carcod, "
                       "       cardtl.srvlvl "
                       "  from trnsp_mode, "
                       "       cardtl,"
                       "       car_move,"
                       "       stop,"
                       "       shipment,"
                       "       rplwrk "
                       " where trnsp_mode.palletctl_flg = 1 "
                       "   and trnsp_mode.trnsp_mode    = nvl(car_move.trans_mode,"
                       "                                 cardtl.cartyp) "
                       "   and cardtl.carcod            = car_move.carcod "
                       "   and cardtl.srvlvl            = (select max(srvlvl)"
                       "                                    from ship_struct_view "
                       "                                   where ship_struct_view.car_move_id = stop.car_move_id) "
                       "   and car_move.car_move_id     = stop.car_move_id "
                       "   and stop.stop_id             = shipment.stop_id "
                       "   and shipment.ship_id         = rplwrk.ship_id "
                       "   and car_move.car_move_id     = '%s'"
                       "   and rplwrk.schbat            = '%s'"
                       " group by stop.car_move_id,"
                       "       car_move.carcod,"
                       "       cardtl.srvlvl ",
                       pck_car_move_id,
                       schbat);
                        
                status = sqlExecStr(buffer, &palres);
                
                if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                {
                    /* Error needs to be returned */
                    sqlFreeResults(palres);
                    return status;
                }

                if (status == eDB_NO_ROWS_AFFECTED) 
                {
                    /* If the carrier move id does not exist,
                    * We are not returning ,We want to check
                    * any cross docks there.
                    */
                    misTrc(T_FLOW,
                           " Could not retrieve a Carrier Move ID  for "
                           " the replenishment records for the schbat [%s]",
                           schbat);
                           
                    sqlFreeResults(palres);
                }
                 
                if (status == eOK)
                {
                    /* replenishment records were found */
                    records_found_flg = BOOLEAN_TRUE;
                    
                    palrow = sqlGetRow(palres);
                    strcpy(car_move_id, sqlGetString(palres, palrow, 
                           "car_move_id"));
                    strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
                    strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
                    sqlFreeResults(palres);
                }   
            }
            
            /* 
             * If either pckwork or rplwrk records were found,
             * try updating only any replenishment status records 
             */
             
            if (records_found_flg == BOOLEAN_TRUE)
            {
                sprintf(buffer,
                        "[select decode(rplwrk.rplsts,'%s','%s') sts,"
                        "        rplref "
                        "   from stop , "
                        "        shipment,"
                        "        rplwrk "
                        "  where rplwrk.ship_id = shipment.ship_id "
                        "    and shipment.stop_id = stop.stop_id "
                        "    and stop.car_move_id = '%s' "
                        "    and rplwrk.rplsts = '%s']catch (-1403)"
                        " | "
                        " if (@? = 0) "
                        " { "
                        "     [update rplwrk "
                        "        set rplsts = @sts "
                        "      where rplref = @rplref ] "
                        " } ",
                        RPLSTS_ISSUED,
                        RPLSTS_WAIT_ORTEC,
                        car_move_id,
                        RPLSTS_ISSUED);
                        
                status = srvInitiateCommand(buffer, NULL);
                
                if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                {
                    /* Error needs to be returned */
                    sqlFreeResults(palres);
                    return status;
                }
                
                if (status == eDB_NO_ROWS_AFFECTED)
                {
                    /* We are not raising error here,because there could be 
                     * no rplwrk records, we need to continue because we have 
                     * some pick works 
                     */
                    misTrc(T_FLOW,"Error in updating the rplsts in rplwrk...");
                }
            }
        }
        
        /* XDKWRK:
         * If not pick or replenishment work has been found yet,
         * we need to look for cross dock records (if the cross dock is enabled)
         */
        if (records_found_flg == BOOLEAN_FALSE)
        {
            if (appIsInstalledWhID(POLCOD_CROSS_DOCKING,wh_id))
            {
                /* Check if cross dock processing is enabled for PalletCtl */
                status = appIsPolicyEnabled(POLCOD_PALLETCTL,
                                            POLVAR_PALLETCTL_CONFIG,
                                            POLVAL_ENABLE_XDCK_PROCESSING,
                                            wh_id);

                if (status == BOOLEAN_TRUE )
                {
                    /* Looking for cross dock records. */
                    sprintf(buffer,
                           "select stop.car_move_id, "
                           "       car_move.carcod, "
                           "       cardtl.srvlvl "
                           "  from trnsp_mode, "
                           "       cardtl,"
                           "       car_move,"
                           "       stop,"
                           "       shipment,"
                           "       xdkwrk "
                           " where trnsp_mode.palletctl_flg = 1 "
                           "   and trnsp_mode.trnsp_mode    = nvl(car_move.trans_mode,"
                           "                                 cardtl.cartyp) "
                           "   and cardtl.carcod            = car_move.carcod "
                           "   and cardtl.srvlvl            = (select max(srvlvl)"
                           "                                    from ship_struct_view "
                           "                                   where ship_struct_view.car_move_id = stop.car_move_id) "
                           "   and car_move.car_move_id     = stop.car_move_id "
                           "   and stop.stop_id             = shipment.stop_id "
                           "   and shipment.ship_id         = xdkwrk.ship_id "
                           "   and car_move.car_move_id     = '%s'"
                           "   and xdkwrk.schbat            = '%s'"
                           " group by stop.car_move_id,"
                           "       car_move.carcod,"
                           "       cardtl.srvlvl ",
                           pck_car_move_id,
                           schbat);
                            
                    status = sqlExecStr(buffer, &palres);
                    
                    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                    {
                        /* Error needs to be returned */
                        sqlFreeResults(palres);
                        return status;
                    }

                    if (status == eDB_NO_ROWS_AFFECTED) 
                    {
                         misTrc(T_FLOW,
                               " Could not retrieve a Carrier Move ID  for "
                               " the cross dock records for the schbat [%s]",
                               schbat);
                        sqlFreeResults(palres);
                    }
                    
                    if(status == eOK)
                    {
                        /* found xdck record */
                        records_found_flg = BOOLEAN_TRUE;
                        
                        palrow = sqlGetRow(palres);
                        strcpy(car_move_id, sqlGetString(palres, palrow, 
                               "car_move_id"));
                        strcpy(carcod, sqlGetString(palres, palrow, "carcod"));
                        strcpy(srvlvl, sqlGetString(palres, palrow, "srvlvl"));
                        sqlFreeResults(palres);
                    }
                }
            }
        }
        
        /* 
         * Log the PalletCtl event if there exists either records for
         * picks, replenishments or cross docks for this carrier move
         */
        if (records_found_flg == BOOLEAN_TRUE)
        {
            /* We are going to look if there are any work currently exisiting 
            * for the Carrier Move. If so, suspend them.Because when Pallet 
            * control comes back with a new plan , we dont want the pick works
            * to be in a different status than the current one.
            */
            sprintf(buffer,
                    "select distinct reqnum "
                    "  from wrkque,"
                    "       pckwrk_view,"
                    "       shipment,"
                    "       stop "
                    " where wrkque.list_id   = pckwrk_view.list_id "
                    "   and wrkque.wrksts    = '%s'"
                    "   and wrkque.wh_id     = '%s'"
                    "   and pckwrk_view.unique_pallet_id is not null "
                    "   and pckwrk_view.ship_id   = shipment.ship_id "
                    "   and shipment.stop_id = stop.stop_id "
                    "   and stop.car_move_id = '%s'",
                    WRKSTS_PENDING,
                    wh_id,
                    car_move_id);

            status = sqlExecStr(buffer, &palres);

            /* We'll continue to the flow if there are 
             * no pending work queue entries.
             */
            if (status == eOK)
            {
                for (palrow = sqlGetRow(palres); palrow; 
                    palrow = sqlGetNextRow(palrow))
                {
                     sprintf(buffer,
                            "change work "
                            " where wrksts = '%s' "
                            "   and reqnum = %ld ",
                            WRKSTS_SUSPENDED,
                            sqlGetLong(palres, palrow,"reqnum"));

                    status = srvInitiateCommand(buffer, NULL);
                    if (status != eOK)
                    {
                        misTrc(T_FLOW,
                               "Failed to suspend "
                               ".List pick work");
                        sqlFreeResults(palres);
                        return status;
                    }
                }
            }
            else if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                /* Error needs to be returned */
                sqlFreeResults(palres);
                return status;
            }
            sqlFreeResults(palres);
           
            /* 
            * Log the PalletCtl event for the  carrier move
            */
            sprintf(buffer,
                    "log palletctl send transaction"
                      " where car_move_id = '%s' "
                        " and carcod = '%s' "
                        " and srvlvl = '%s' "
                        " and wh_id = '%s' ",
                    car_move_id,
                    carcod,
                    srvlvl,
                    wh_id);
            
            status = srvInitiateCommand(buffer, NULL);
            
            if (status != eOK)
            {
                misTrc(T_FLOW,"Error logging PALLETCTL_SEND event...");
                return status;
            }
        }        
        else
        {
          misTrc(T_FLOW,
              " There are no records to be planned by Pallet Control  ...");
        }
    }
    
    return eOK;
}
static long logCannedPicks(char *cangrp, char *wrktyp, 
               char *ctnnum, char *wrkref, char *wrkref_dtl,
               char *wrkdtl_clause, char *wrkdtl_excluded_clause,
               char *can_usr_id, char *cancod,
               char *wh_id)
{
    char            *buffer = NULL;
    char            *where_clause = NULL;
    char            *bufferlist = NULL;
    char            *wherelist = NULL;
    long            ret_status;
    long            len = 0;

    moca_bool_t     cnlsgnflg = BOOLEAN_FALSE; /*Cancel Single*/

    if (!wrkdtl_excluded_clause)
    {
        len = strlen(wrkdtl_clause);
    }
    else
    {
        len = strlen(wrkdtl_clause) > strlen(wrkdtl_excluded_clause) ?
              strlen(wrkdtl_clause) : strlen(wrkdtl_excluded_clause);
    }

    buffer = (char*)calloc(1,len + 2000);
    where_clause = (char*)calloc(1,len + 2000);
    
    /* Check if bto_dlv_seq is not null which means it is metered picking*/
    sprintf(buffer,
            "select 'x' "
            "   from pckwrk_dtl "
            "  where wrkref_dtl = '%s' "
            "    and bto_dlv_seq is not null ",
            wrkref_dtl);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
        /* Check if Single Cancel For BTO Metered Picking is enabled */
        ret_status = appIsPolicyEnabled(POLCOD_CANCEL_PICK,
                                        POLVAR_MISC,
                                        POLVAL_CANCEL_SINGLE_METERED_PICK,
                                        wh_id);
        if (ret_status == BOOLEAN_TRUE)
        {
            cnlsgnflg = BOOLEAN_TRUE;
        }
    }
    
    if (misStrncmpChars(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0) 
    {
        sprintf(where_clause,
          "select distinct pw.* "
          "  from pckwrk_view pw, "
          "       pckwrk_dtl pd, "
          "       pckwrk_view p1 "
          " where pw.cmbcod = pd.cmbcod "
          "   and pd.ctnnum = nvl(p1.subnum, p1.ctnnum) "
          "   and pw.prtnum != '%s' "
          "   and pw.dtl_pckqty > pw.dtl_appqty "
          /* include the picks to boxes and exclude the new box picks to carton*/
          "   and p1.ctnnum = '%s' ",
          PRTNUM_KIT, ctnnum);

        if (!misDynStrcpy(&wherelist, where_clause))
        {
            if (wherelist != NULL) free(wherelist);
            if (bufferlist != NULL) free(bufferlist);
            free(buffer);
            free(where_clause);
            return (eNO_MEMORY);
        }
    }
    else if (cnlsgnflg == BOOLEAN_TRUE)
    {
        sprintf(where_clause,
                "select * "
                "  from pckwrk_view "
                " where wrkref_dtl = '%s' ",
                wrkref_dtl);
    }
    else
    {
        /* If work refenece detail is one of the input,
         * we will only record current detail with
         * the cancel code in the thread.
         * If only work refernece input, all the details 
         * will be recorded with this cancel code.
         */
        if (wrkdtl_excluded_clause && strlen(wrkdtl_excluded_clause) != 0)
        {
            /* wrktyp is 'P', including normal pick and Box pick(KITPART)
             * if it's box pick,it will select all picks with 
             * ctnnum=Box number.
             * if it's normal pick,then we use wrkref to select itself
             */
            sprintf(where_clause,
                "select distinct pw.* "
                "  from pckwrk_view pw, "
                "       pckwrk_dtl pd "
                " where pw.cmbcod = pd.cmbcod " 
                "   and pw.prtnum != '%s' "
                "   and pw.dtl_pckqty > pw.dtl_appqty "
                "   and pd.subnum = '%s' "
                " union "
                "select distinct * "
                "  from pckwrk_view "
                " where prtnum != '%s' "
                "   and dtl_pckqty > dtl_appqty "
                "   and wrkref_dtl = '%s' ",
            PRTNUM_KIT, ctnnum, PRTNUM_KIT, wrkref_dtl);
        }
        else
        {
            sprintf(where_clause,
                "select distinct pw.* "
                "  from pckwrk_view pw, "
                "       pckwrk_dtl pd "
                " where pw.cmbcod = pd.cmbcod " 
                "   and pw.prtnum != '%s' "
                "   and pw.dtl_pckqty > pw.dtl_appqty "
                "   and pd.subnum = '%s' "
                " union "
                "select distinct * "
                "  from pckwrk_view "
                " where prtnum != '%s' "
                "   and dtl_pckqty > dtl_appqty "
                "   and wrkref_dtl %s ",
            PRTNUM_KIT, ctnnum, PRTNUM_KIT, wrkdtl_clause);
        }
    }
 
    /* Select everything from the pckwrk_hdr/pckwrk_dtl table and push it into the
       canpck.  This pulls everything for the cmbcod because deallocate
       inventory deallocates everything for the cmbcod associated with the
       wrkref.  */
    sprintf(buffer,
        "[%s] |  "
        "  create cancelled pick "
        "  where cangrp = '%s' "
        "    and can_usr_id = '%s' "
        "    and cancod     = '%s' "
        "    and remqty     = (@dtl_pckqty - @dtl_appqty) "
        "    and pckqty     = @dtl_pckqty "
        "    and appqty     = @dtl_appqty ",
            where_clause, 
            cangrp, 
            can_usr_id, 
            cancod);

    ret_status = srvInitiateCommand(buffer, NULL);

    /* if we got down this path all pick have been completed for the kit 
        and we cannot cancel the header record */

    if ((ret_status == eDB_NO_ROWS_AFFECTED) &&  
    (misStrncmpChars(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0))  
    {
        free(buffer);
        free(where_clause);
        return (ret_status = eINT_CANT_CANCEL_KIT_PICK);
    }

    if (ret_status != eOK)
    {
        free(buffer);
        free(where_clause);
        return (ret_status);
    }

    /* If we have other details in bulk pick, 
     * they will be cancelled and reallocated 
     * with cancel code CANCEL_REALLOC_REUSE_LOC 
     */
    if (wrkdtl_excluded_clause && strlen(wrkdtl_excluded_clause) != 0) 
    {
        sprintf(where_clause,
            "select * "
            "  from pckwrk_view "
            " where wrkref_dtl %s"
            "   and dtl_appqty < dtl_pckqty ",
            wrkdtl_excluded_clause);

        sprintf(buffer,
            "[%s] |  "
            "  create cancelled pick "
            "  where cangrp = '%s' "
            "    and can_usr_id = '%s' "
            "    and cancod     = '%s' "
            "    and remqty     = (@dtl_pckqty - @dtl_appqty) "
            "    and pckqty     = @dtl_pckqty "
            "    and appqty     = @dtl_appqty ",
            where_clause, 
            cangrp, 
            can_usr_id, 
            CANCEL_REALLOC_REUSE_LOC);

        ret_status = srvInitiateCommand(buffer, NULL);

        if ((ret_status == eDB_NO_ROWS_AFFECTED) &&  
        (misStrncmpChars(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0))  
        {
            free(buffer);
            free(where_clause);
            return (ret_status = eINT_CANT_CANCEL_KIT_PICK);
        }

        if (ret_status != eOK)
        {
            free(buffer);
            free(where_clause);
            return (ret_status);
        }
    }
    

    /* If PalletCtl is installed,we need to update the
     * Pallet control information to the canpck table
     */
    if (appIsInstalledWhID(POLCOD_PALLETCTL,wh_id) && 
        misStrncmpChars(wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0)
    {
         sprintf(buffer,
                " [select cmbcod, "
                "         unique_pallet_id, " 
                "         pallet_pos, "
                "         pallet_load_seq "
                "    from pckwrk_view "
                "   where cmbcod in "
                "        (select cmbcod "
                "           from pckwrk_dtl "
                "          where wrkref = '%s' "
                "            and wrkref_dtl = '%s' ) "
                "     and pckqty > appqty]"
                " | "
                " [update canpck "
                "     set pallet_id       = @unique_pallet_id,"
                "         pallet_pos      = @pallet_pos,"
                "         pallet_load_seq = @pallet_load_seq "
                "   where cmbcod          = @cmbcod ]",
                wrkref,wrkref_dtl);
         ret_status = srvInitiateCommand(buffer, NULL);

        if (ret_status != eOK)
        {
            free(buffer);
            free(where_clause);
            return (ret_status);
        }
    }
    free(buffer);
    free(where_clause);
    return (eOK);
}

static long clearLocation(char *stoloc, float qvl, char *wh_id)
{
    char            buffer[utf8Size(500)];
    long            ret_status;

    sprintf(buffer, 
        "update locmst "
        "    set pndqvl = pndqvl - %f "
        " where stoloc = '%s' "
            "   and wh_id = '%s'",
        qvl, stoloc, 
            wh_id);

    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status != eOK)
    return (ret_status);

    return (eOK);
}


static long removeFromWorkQueue(char *wrkref)
{
    long        ret_status;
    char    buffer[1000];
    char        lblbat[LBLBAT_LEN+1];

    mocaDataRes    *pckwrk_res;
    mocaDataRow    *pckwrk_row;
    
    memset(lblbat, 0, sizeof(lblbat));

    /*  Determine if pick is on a label batch */     
    
    sprintf(buffer,
        "select lblbat "
        "  from pckwrk_hdr "
        " where wrkref = '%s' "
        "   and lblbat is not null",
        wrkref);
    ret_status = sqlExecStr(buffer, &pckwrk_res);
    
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        return (ret_status);
    }
    if ((ret_status == eOK))
    {
        /* Grab the lblbat for use later on. */
        pckwrk_row = sqlGetRow(pckwrk_res);
        misTrimcpy(lblbat, sqlGetString(pckwrk_res, pckwrk_row, "lblbat"),
                            LBLBAT_LEN);
    }
    sqlFreeResults(pckwrk_res);

    /* If pick work is not on a label batch, complete any existing work. */
    if (ret_status == eDB_NO_ROWS_AFFECTED)
    {
        /* The picks with the same cmbcod are referred to as the same pick, 
         * they should be removed or updated or picked at the same time.
         * so here we remove the pick work queue records with the same cmbcod.
         * 
         * As to bulk pick, we will cancel the whole bulk pick if we cancel any of 
         * the detail pick. So all the works related will be removed.
         */
        sprintf(buffer, 
            "  [select wrkque.reqnum  "
            "     from wrkque "
            "     join pckwrk_hdr "
            "       on wrkque.wrkref = pckwrk_hdr.wrkref "
            "    where wrkque.wrkref = '%s'  "
            "      and pckwrk_hdr.pckqty > pckwrk_hdr.appqty"
            " group by wrkque.reqnum ] catch(-1403)"
            "  | "
            "  if(@? = 0) "
            "  { "
            "     [update wrkque "
            "         set wrksts = '%s' "
            "       where reqnum = @reqnum] "
            "     | "
            "     remove work request "
            "      where reqnum = @reqnum " 
            "        and wrkref = @wrkref "
            "        and prcmod = '%s' "
            "   }",
            wrkref,
            WRKSTS_CANL,
            PRCMOD_NOMOVE);
        ret_status = srvInitiateInline(buffer, NULL);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }
    else
    {
        /* If pick work is on a label batch, 
         * check if there are any picks on the label batch
         * that have not yet been picked.
         * We should check cmbcod rather than wrkref. because
         * pick works with the same cmbcod mean that they are the 
         * same pick works. Moreover, system will set pckqty = appqty
         * afterwards which means the work is completed already.
         * */
        sprintf(buffer,
                " select 'x' "
                "   from pckwrk_view "
                "  where lblbat = '%s' "
                "    and dtl_pckqty > dtl_appqty "
                "    and cmbcod not in (select cmbcod "
                "                         from pckwrk_dtl "
                "                        where wrkref = '%s')", 
                lblbat, wrkref);
        ret_status = sqlExecStr(buffer, NULL);

        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            return (ret_status);
        }

        /* If all picked or deleted, delete all work on label batch. */
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            /* Because the wrkref could be set to a pick that has been
                 * cancelled, we need to use the lblbat to get the reqnum for
                 * the work that we need to cancel.
                 */
            sprintf(buffer,
                    "[select reqnum "
                    "  from wrkque "
                    " where lblbat = '%s'] "
                    " | "
                    "complete work "
                    "   where reqnum = @reqnum " 
                    "     and prcmod = '%s' ",
                    lblbat, PRCMOD_NOMOVE);
             ret_status = srvInitiateInline(buffer, NULL);
                 if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
             {
                 return (ret_status);
             }
        }
    }
    return (eOK);
}    


static long getStdPalletVolume(char *wh_id)
{
    char            buffer[utf8Size(300)];
    long            ret_status;
    static long     volume;
    mocaDataRes    *res;
    mocaDataRow    *row;

    if (volume != 0)
    return (volume);

    sprintf(buffer,
        "select rtnum1 "
        "  from poldat_view "
        " where polcod = '%s' "
        "    and polvar = '%s' "
        "    and polval = '%s' "
        "    and wh_id  = '%s' ",
            POLCOD_PCKRELMGR,
            POLVAR_MISC,
        POLVAL_REL_PALLET_VOLUME,
        wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
    sqlFreeResults(res);
    volume = 120968;    /* just in case the def policy is missing */
    return (volume);
    }
    row = sqlGetRow(res);
    if (!sqlIsNull(res, row, "rtnum1"))
    volume = sqlGetLong(res, row, "rtnum1");
    else
    volume = 120968;

    sqlFreeResults(res);
    return (volume);
}

static long UnallocateIntermediate(char *wrkref, char *wrkref_dtl)
{
    long            ret_status;
    char            buffer[1000];
    mocaDataRes    *res, *tres;
    mocaDataRow    *row, *trow;
    double          wid, hgt, len;
    long            qty, stdpal, untcas;
    float           pallet_volume;
    float           product_volume;
    float           product_length;
    long            NumRows;
    
    if (misTrimIsNull(wrkref, WRKREF_LEN) || misTrimIsNull(wrkref_dtl, WRKREF_DTL_LEN))
    return (eOK);

    /* We cannot say a location is a hop or intermediate location 
     * if its area's praflg is TRUE, because a location could be a hop 
     * location even if the praflg is FALSE. Instead, we should check 
     * the pckmov table.
     */
    sprintf(buffer,
        "select pckmov.arecod, pckmov.stoloc, pckmov.seqnum, "
        "        pckwrk_view.lodlvl, pckwrk_view.ftpcod, aremst.wh_id, "
        "        pckwrk_view.untcas, pckwrk_view.prtnum, pckwrk_view.prt_client_id, "
        "        sum(pckwrk_view.dtl_pckqty) pckqty, "
        "        sum(pckwrk_view.dtl_appqty) appqty "
        "  from aremst, pckwrk_view, pckmov "
        " where pckwrk_view.cmbcod = (select cmbcod "
        "                   from pckwrk_dtl where wrkref_dtl = '%s') "
        "   and pckmov.cmbcod = pckwrk_view.cmbcod "
        "   and aremst.arecod = pckmov.arecod "
        "   and aremst.wh_id = pckmov.wh_id "
        " group by aremst.wh_id, pckmov.arecod, pckmov.stoloc, "
        "           pckmov.seqnum, pckwrk_view.lodlvl, pckwrk_view.ftpcod,  "
        "           pckwrk_view.untcas, pckwrk_view.prtnum, pckwrk_view.prt_client_id  "
        " order by pckmov.seqnum ",
        wrkref_dtl);

    ret_status = sqlExecStr(buffer, &res);
    if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults(res);
        return (ret_status == eDB_NO_ROWS_AFFECTED ? eOK : ret_status);
    }

    /* We will handle all intermediate locations first. So we leave the 
     * last row, which is the destnation location, unprocessed, because 
     * it will be handled later.
     */
    tres = NULL;
    NumRows = 0;
    NumRows = sqlGetNumRows(res);
    for (row = sqlGetRow(res); NumRows > 1 && row; 
        NumRows--, row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "stoloc"))
        {
            qty = (long) ( sqlGetFloat(res, row, "pckqty")
                  -  sqlGetFloat(res, row, "appqty"));
            untcas = sqlGetLong(res, row, "untcas");
            stdpal = getStdPalletVolume(sqlGetString(res, row, "wh_id"));
    
            sprintf(buffer,
                "select prtftp_dtl.len caslen, "
                "       prtftp_dtl.wid caswid, "
                "       prtftp_dtl.hgt cashgt, "
                "       aremst.loccod, aremst.arecod, aremst.sigflg "
                "  from aremst, prtftp_dtl "
                " where aremst.arecod = '%s' "
                "   and aremst.wh_id = '%s' "
                "   and prtftp_dtl.ftpcod = '%s' "
                "   and prtftp_dtl.prtnum = '%s' "
                "   and prtftp_dtl.prt_client_id = '%s' "
                "   and prtftp_dtl.wh_id = '%s' "
                "   and prtftp_dtl.cas_flg = %d ",
                sqlGetString(res, row, "arecod"),
                sqlGetString(res, row, "wh_id"),
                sqlGetString(res, row, "ftpcod"),
                sqlGetString(res, row, "prtnum"),
                sqlGetString(res, row, "prt_client_id"),
                sqlGetString(res, row, "wh_id"),
                BOOLEAN_TRUE);
    
            ret_status = sqlExecStr(buffer, &tres);
            if (ret_status != eOK)
            {
                sqlFreeResults(tres);
                sqlFreeResults(res);
                return (ret_status);
            }
            trow = sqlGetRow(tres);
            hgt = sqlGetFloat(tres, trow, "cashgt");
            wid = sqlGetFloat(tres, trow, "caswid");
            len = sqlGetFloat(tres, trow, "caslen");
    
            product_length = (float) (len * ((float) qty / untcas));
            product_volume = (float) ((hgt * wid * len) *
                          ((float) qty / untcas));
    
            if (misStrncmpChars(sqlGetString(res, row, "lodlvl"),
                LODLVL_LOAD, LODLVL_LEN) == 0)
                pallet_volume = (float) 1;
            else
                pallet_volume = (float) (product_volume / stdpal);
    
            if (trow &&
              sqlGetBoolean(tres, trow, "sigflg") == BOOLEAN_TRUE)
            {
                if (misStrncmpChars(sqlGetString(tres, trow, "loccod"),
                        LOCCOD_PALLET, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   pallet_volume,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (misStrncmpChars(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_VOLUME, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   product_volume,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (misStrncmpChars(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_EACHES, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   (float) qty,
                                   sqlGetString(res, row, "wh_id"));
                }
                else if (misStrncmpChars(sqlGetString(tres, trow, "loccod"),
                         LOCCOD_LENGTH, LOCCOD_LEN) == 0)
                {
                    ret_status = clearLocation(sqlGetString(res,
                                       row, "stoloc"),
                                   product_length,
                                   sqlGetString(res, row, "wh_id"));
                }
                if (ret_status != eOK)
                {
                    sqlFreeResults(tres);
                    sqlFreeResults(res);
                    return (ret_status);
                }
            }
        }
    }
    if (tres) sqlFreeResults(tres);
    sqlFreeResults(res);
    return (eOK);

}


/*
 **   usrfixCancelPick will Cancel pick for a specified pick request.
 **     Optionally, it will also error the location being picked on.
 */
LIBEXPORT 
RETURN_STRUCT *usrfixCancelPick(char *wrkref_i, char *wrkref_dtl_i, moca_bool_t *errflg_i,
                 char *devcod_i, char *usr_id_i,
                 char *cancod_i,
                 moca_bool_t *palctl_flg_i,
                 moca_bool_t *bulk_exclud_flg_i)
{
    long            status = eERROR;
    long            count  = 0;
    RETURN_STRUCT  *CurPtr, *CurPtr1;
    mocaDataRes    *res, *wrkres, *pckres, *canres, *shpres, *dtlres;
    mocaDataRow    *row, *wrkrow, *pckrow, *canrow, *shprow, *dtlrow;
    char            buffer[2000], *buf = NULL;
    char            wrkref_dtl[WRKREF_DTL_LEN + 1];
    char            *wrkdtl_clause = NULL;
    char            *wrkdtl_excluded_clause = NULL;
    char            usr_id[USR_ID_LEN + 1];
    char            cancod[CODVAL_LEN + 1];
    char            devcod[DEVCOD_LEN + 1];
    char            wrktyp[WRKTYP_LEN + 1];
    char            cangrp[CANGRP_LEN + 1];
    moca_bool_t     flag_error;
    long            delqty;

    char            wrkref[WRKREF_LEN + 1];
    char            ctnnum[CTNNUM_LEN + 1];
    char            wh_id[WH_ID_LEN + 1];
    char            ship_line_id[SHIP_LINE_ID_LEN + 1];
    char            list_id[LIST_ID_LEN + 1];
    char            command[utf8Size(500)];
    char            schbat[SCHBAT_LEN + 1];
    char            palctlsts[PALCTLSTS_LEN + 1];
    char            pck_car_move_id[CAR_MOVE_ID_LEN + 1];
    char            ship_id[SHIP_ID_LEN + 1];

    moca_bool_t     bulk_exclud_flg = BOOLEAN_TRUE;

    memset(wrkref, 0, sizeof(wrkref));
    memset(wrkref_dtl, 0, sizeof(wrkref_dtl));
    memset(devcod, 0, sizeof(devcod));
    memset(cancod, 0, sizeof(cancod));
    memset(cangrp, 0, sizeof(cangrp));
    memset(ship_line_id, 0, sizeof(ship_line_id));
    memset(usr_id, 0, sizeof(usr_id));
    memset(list_id, 0, sizeof(list_id));
    memset(wh_id, 0, sizeof(wh_id));
    memset(schbat, 0, sizeof(schbat));
    memset(pck_car_move_id,0,sizeof(pck_car_move_id));
    memset(ship_id, 0, sizeof(ship_id));

    if (misTrimIsNull(wrkref_i, WRKREF_LEN))
    {
        return (APPMissingArg("wrkref"));
    }
    misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);
    
   /* Get wrkref dtl that are partially picked
    * for cancellation process. For the subucc
    * scenarios wherein single pckwrk hdr
    * will have mutiple details associated to it
    * the system will mistake the entire
    * pick when one of the details pick works have
    * been already satisfied. This will lead the
    * system to cancel the entire pick.
    */
    sprintf(buffer,
            "select wrkref_dtl "
            "   from pckwrk_dtl "
            "  where wrkref = '%s' "
            "    and appqty < pckqty ",
            wrkref);
    status = sqlExecStr(buffer, &dtlres); 
    if (eOK != status)
    {
        sqlFreeResults(dtlres);
        return (srvResults (status, NULL));
    }
    
    if (bulk_exclud_flg_i)
        bulk_exclud_flg = *bulk_exclud_flg_i;
    
    wrkdtl_clause = (char*)calloc(1,dtlres->NumOfRows*(WRKREF_DTL_LEN+5) + 500);

    strcpy(wrkdtl_clause, " in (");
    for (dtlrow = sqlGetRow(dtlres); dtlrow; dtlrow = sqlGetNextRow(dtlrow))
    {
        if (count == 0)
        {
            if (misTrimIsNull(wrkref_dtl_i, WRKREF_DTL_LEN))
            {   
                /* If work reference detail is not passed in, 
                 * assume the first detail is current work detail.
                 */
                misTrimcpy(wrkref_dtl, 
                        sqlGetString(dtlres, dtlrow, "wrkref_dtl"), 
                        WRKREF_DTL_LEN); 
            }
            else 
            {
                /* The work reference detail passed in is used to track 
                 * current work reference detail.
                 */
                misTrimcpy(wrkref_dtl, wrkref_dtl_i, WRKREF_DTL_LEN);
            }

            strcat(wrkdtl_clause,"'");
            strcat(wrkdtl_clause,sqlGetString(dtlres, dtlrow, "wrkref_dtl"));
            strcat(wrkdtl_clause,"'");
        }
        else
        {
            strcat(wrkdtl_clause,",'");
            strcat(wrkdtl_clause,sqlGetString(dtlres, dtlrow, "wrkref_dtl"));
            strcat(wrkdtl_clause,"'");
        }
        count++;
    }
    strcat(wrkdtl_clause,")");

    if (!misTrimIsNull(wrkref_dtl_i, WRKREF_DTL_LEN)) 
    {
        wrkdtl_excluded_clause = (char*)calloc(1,dtlres->NumOfRows*(WRKREF_DTL_LEN+5) + 500);
        strcpy(wrkdtl_excluded_clause, " in (");
        count = 0;
        for (dtlrow = sqlGetRow(dtlres); dtlrow; dtlrow = sqlGetNextRow(dtlrow))
        {
            if (misStrncmpChars(wrkref_dtl, 
                        sqlGetString(dtlres, dtlrow, "wrkref_dtl"),
                        WRKREF_DTL_LEN) == 0)
                continue;

            if (count == 0)
            {
                strcat(wrkdtl_excluded_clause,"'");
                strcat(wrkdtl_excluded_clause,
                        sqlGetString(dtlres, dtlrow, "wrkref_dtl"));
                strcat(wrkdtl_excluded_clause,"'");
            }
            else 
            {
                strcat(wrkdtl_excluded_clause,",'");
                strcat(wrkdtl_excluded_clause,
                    sqlGetString(dtlres, dtlrow, "wrkref_dtl"));
                strcat(wrkdtl_excluded_clause,"'");
            }
            count++;
        }
        strcat(wrkdtl_excluded_clause,")");
        
        /* If only one pick detail in pick work, 
         * clear excluded string up.
         */
        if (count == 0 || !bulk_exclud_flg)
        {
            free(wrkdtl_excluded_clause);
            wrkdtl_excluded_clause = NULL;
        }
    }

    sqlFreeResults (dtlres);

    /*
     * If we got a user Id passed, we'll use it, otherwise we'll
     * get one from the envrionment.
     */

    if (usr_id_i && misTrimLen(usr_id_i, USR_ID_LEN) != 0)
    {
        misTrimcpy(usr_id, usr_id_i, USR_ID_LEN);
    }
    else
    {
        strcpy(usr_id, osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID) : "");
    }

    /*
     * If a cancel code is not passed in, then go and get the default.  If
     * there are multiple defaults, then just get the first one.
     */

    if (cancod_i && misTrimLen(cancod_i, CODVAL_LEN) != 0)
    {
        misTrimcpy(cancod, cancod_i, CODVAL_LEN);
    }
    else
    {
        CurPtr = NULL;
        sprintf (buffer,
                 "list cancel codes "
                 " where defflg = '%ld' ",
                 BOOLEAN_TRUE);

        status = srvInitiateCommand(buffer, &CurPtr); 
        if (eOK != status)
        {
            srvFreeMemory(SRVRET_STRUCT, CurPtr); 

            /* 
             * If we couldn't find a cancel code, we'll just keep going.
             * We'll rely on subsequent functionality to error if the cancel
             * code is needed later.
             */

            if ((eDB_NO_ROWS_AFFECTED != status) &&
                (eSRV_NO_ROWS_AFFECTED != status))
            {
                free(wrkdtl_clause);
                return (srvResults (status, NULL));
            }
        }
        else
        {
            res = srvGetResults(CurPtr);
            row = sqlGetRow (res);
            misTrimcpy (cancod, sqlGetString (res, row, "codval"), CODVAL_LEN);
            srvFreeMemory(SRVRET_STRUCT, CurPtr);
        }
    }

    flag_error = BOOLEAN_FALSE;
    if (errflg_i && *errflg_i == BOOLEAN_TRUE)
       flag_error = BOOLEAN_TRUE;

    /* See what type of work this is ... */
    memset(wrktyp, 0, sizeof(wrktyp));
    memset(ctnnum, 0, sizeof(ctnnum));
    memset(wh_id, 0, sizeof(wh_id));
    memset(palctlsts, 0, sizeof(palctlsts));

    sprintf(buffer,
        "select wrktyp, wh_id, ctnnum, subnum , schbat, palctlsts, ship_id"
        "  from pckwrk_view "
        " where wrkref = '%s' "
        "   and wrkref_dtl = '%s' ", 
        wrkref,
        wrkref_dtl);

    status = sqlExecStr(buffer, &res);
    if (status != eOK)
    {
        free(wrkdtl_clause);
        sqlFreeResults(res);
        return(srvResults(status, NULL));
    }
    row = sqlGetRow(res);
    misStrncpyChars(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);
    misStrncpyChars(wrktyp, sqlGetString(res, row, "wrktyp"), WRKTYP_LEN);
    misStrncpyChars(schbat, sqlGetString(res, row, "schbat"), SCHBAT_LEN);
    misStrncpyChars(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
    if (!sqlIsNull(res, row, "palctlsts"))
    {
        misStrncpyChars(palctlsts,
                sqlGetString(res, row, "palctlsts"),
                PALCTLSTS_LEN);
    }
    
    /* Validate if we are trying to cancel and reallocate an emergency 
     * replenishment pick. If yes, we throw an error, short allocations 
     * cannot be cancelled and automatically reallocated, because we do not
     * support this in allocate inventory.
     */ 

    if (misStrncmpChars(wrktyp, WRKTYP_EMERGENCY, WRKTYP_LEN) == 0 ||
        misStrncmpChars(wrktyp, WRKTYP_PIA_DEMAND, WRKTYP_LEN) == 0)
    {  
        sprintf(buffer,
            " select 1 "
            "   from cancod "
            "  where codval = '%s' "
            "    and reaflg = 1 ",
        cancod);
        status = sqlExecStr(buffer, NULL);

        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
            free(wrkdtl_clause);
            return(srvResults(status, NULL));
        }

        if (status == eOK)
        {
            free(wrkdtl_clause);
            return(srvResults(eINT_SHORT_ALLOC_CANT_BE_CANCELLED_AND_REALLOC, 
                   NULL));
        }
    }
    
    if (misStrncmpChars(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0)
    {
    /*
     * If it's a kit pick...
     */

    misTrc(T_FLOW, "Processing a Kit pick...");
    if (!sqlIsNull(res, row, "subnum"))
        misTrimcpy(ctnnum, sqlGetString(res, row, "subnum"), CTNNUM_LEN);
    }
    else
    {
    misTrc(T_FLOW, "Not processing a Kit pick...");
    if (!sqlIsNull(res, row, "ctnnum"))
        misTrimcpy(ctnnum, sqlGetString(res, row, "ctnnum"), CTNNUM_LEN);
    }
    sqlFreeResults(res);

    /* Populate the canceled pick table */
    memset(cangrp, 0, sizeof(cangrp));
    appNextNum(NUMCOD_CANGRP, cangrp);

    status = logCannedPicks(cangrp, wrktyp, ctnnum, wrkref,
                             wrkref_dtl, wrkdtl_clause, wrkdtl_excluded_clause, 
                             usr_id, cancod, wh_id);

    if (wrkdtl_excluded_clause)
        free(wrkdtl_excluded_clause);

    if (status != eOK)
    {
        free(wrkdtl_clause);
        return (srvResults(status, NULL));
    }

    status = removeFromWorkQueue(wrkref);
    if (status != eOK)
    {
        free(wrkdtl_clause);
        return (srvResults(status, NULL));
    }

    /* For wrktyp 'K',it's a carton pick(or Box pick),which may include
     * normal detail pick or Box Pick(Box pick only include detail pick).
     * In below union selection,first part is used to select 
     * normal detail picks for Carton or Box.
     * and second is used to select detail picks for Box Pick.
     */
    
    if (misStrncmpChars(wrktyp, WRKTYP_KIT, WRKTYP_LEN) == 0)
    {
        buf = buffer;
        sprintf(buf,
            "select distinct pw.wrkref, pw.wrkref_dtl, pw.srcloc, pw.cmbcod "
            "  from pckwrk_view pw, "
            "       pckwrk_view p1 "
            " where pw.ctnnum = p1.subnum "
            "   and pw.dtl_appqty < pw.dtl_pckqty "
            "   and pw.prtnum != '%s' "
            "   and p1.subnum = '%s' "
            " union "
            "select distinct pw.wrkref, pw.wrkref_dtl, pw.srcloc, pw.cmbcod "
            "  from pckwrk_view pw, "
            "       pckwrk_view p1 "
            " where pw.ctnnum = p1.subnum "
            "   and pw.dtl_appqty < pw.dtl_pckqty "
            "   and pw.prtnum != '%s' "
            "   and p1.ctnnum = '%s' ",
            PRTNUM_KIT, ctnnum, PRTNUM_KIT, ctnnum);
    }
    else
    {
        /* If this wrkref is for kitpck, we should get all the pick work
         * according to ctnnum; if it is a normal pick, just use wrkref.
         *
         * Carton pick can not be bulked. So we can assume only one
         * work reference detail involved.
         * */
        buf = (char*)calloc(1,strlen(wrkdtl_clause) + 1000);
        sprintf(buf,
            "select distinct wrkref, wrkref_dtl, srcloc, cmbcod, ship_line_id"
            "  from pckwrk_view "
            " where dtl_appqty < dtl_pckqty "
            "   and prtnum != '%s' "
            "   and wrkref_dtl %s"
            " union "
            "select distinct pw.wrkref, pw.wrkref_dtl, pw.srcloc, pw.cmbcod, pw.ship_line_id"
            "  from pckwrk_view pw, "
            "       pckwrk_dtl pd "
            " where pw.ctnnum = pd.subnum"
            "   and pw.dtl_appqty < pw.dtl_pckqty "
            "   and pw.prtnum != '%s' "
            "   and pd.wrkref_dtl = '%s' ",
            PRTNUM_KIT, wrkdtl_clause, PRTNUM_KIT, wrkref_dtl);
    }

    free(wrkdtl_clause);

    status = sqlExecStr(buf, &wrkres);

    if (buf && buf != buffer)
    {
        free(buf);
        buf = NULL;
    }
    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults(wrkres);
        return(srvResults(status, NULL));
    }

    /* Now, loop through each (or just one, in the case of the kit pick) 
     * of them 
     */
    CurPtr = CurPtr1 = NULL;
    for (wrkrow = sqlGetRow(wrkres); wrkrow; wrkrow = sqlGetNextRow(wrkrow))
    {
        memset(wrkref, 0, sizeof(wrkref));
        memset(wrkref_dtl, 0, sizeof(wrkref_dtl));
        memset(ship_line_id, 0, sizeof(ship_line_id));

        misStrncpyChars(wrkref, sqlGetString(wrkres, wrkrow, "wrkref"), WRKREF_LEN);
        misStrncpyChars(wrkref_dtl, sqlGetString(wrkres, wrkrow, "wrkref_dtl"), WRKREF_DTL_LEN);

        if (!sqlIsNull(wrkres, wrkrow, "ship_line_id"))
        {
            misTrimcpy(ship_line_id,
                       sqlGetString(wrkres, wrkrow, "ship_line_id"),
            SHIP_LINE_ID_LEN);
        }

        /* First, we get a list of all intermediate locations,
           after we're through processing, we call 
           deallocate resource location to clear the
           rescod if necessary... */
        pckres = NULL;
        sprintf(buffer,
            "select stoloc, wh_id from pckmov "
            " where cmbcod = '%s' and stoloc is not null",
            sqlGetString(wrkres, wrkrow, "cmbcod"));
        status = sqlExecStr(buffer, &pckres);
        if (status != eOK)
        {
            sqlFreeResults(pckres);
            pckres = NULL;
        }

        status = UnallocateIntermediate(wrkref, wrkref_dtl);
        if (status != eOK)
        {
            sqlFreeResults(wrkres);
            if (pckres) sqlFreeResults(pckres);
            return (srvResults(status, NULL));
        }

        /*
         ** Deallocate the destination location
         */
        sprintf(buffer, "deallocate location where wrkref = '%s' and wrkref_dtl = '%s'", 
                wrkref, wrkref_dtl);
        CurPtr1 = NULL;
        status = srvInitiateCommand(buffer, &CurPtr1);
        srvFreeMemory(SRVRET_STRUCT, CurPtr1);
        if (status != eOK)
        {
            sqlFreeResults(wrkres);
            if (pckres) sqlFreeResults(pckres);
            return (srvResults(status, NULL));
        }
        CurPtr1 = NULL;

        sprintf(command,
            "deallocate inventory where wrkref='%s'and wrkref_dtl = '%s'", wrkref, wrkref_dtl);
        CurPtr1 = NULL;
        status = srvInitiateCommand(command, &CurPtr1);
        srvFreeMemory(SRVRET_STRUCT, CurPtr1);
        if (status != eOK)
        {
            sqlFreeResults(wrkres);
            if (pckres) sqlFreeResults(pckres);
            return (srvResults(status, NULL));
        }
        /* In case Pick stealing/Cross dock criteria has matched,
         * we have record in invdtl_pck.
         * We need to clear those records if the pick is being cancelled.
         */
        sprintf(buffer,
            "delete from invdtl_pck "
            "    where wrkref = '%s' "
            "      and wrkref_dtl = '%s'", 
            wrkref, wrkref_dtl);
        status = sqlExecStr(buffer, NULL);
        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
            if (pckres) sqlFreeResults(pckres);
            return (srvResults(status, NULL));
        }
        /* If PalletCtl is installed,log Pallet control
         * event to get a fresh plan from pallet control
         */
        if (appIsInstalledWhID(POLCOD_PALLETCTL,wh_id) && 
            misStrncmpChars(wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0 &&
            misStrncmpChars(palctlsts, WAITING_SHORTS_PALLET_HOLD, PALCTLSTS_LEN) != 0)
        {
             /* A single schbat can have multiple car_moves associated with it.
              * Hence get the car_move of the pick we are cancelling.
              */
             sprintf(buffer,
                " select stop.car_move_id, "
                "        shipment.wh_id "
                "   from stop, "
                "        shipment "
                "  where stop.stop_id = shipment.stop_id "
                "    and shipment.ship_id = '%s' "
                "    and shipment.wh_id   = '%s'",
                ship_id,
                wh_id);
            status = sqlExecStr(buffer, &res);
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                if (pckres) sqlFreeResults(pckres);
                if (CurPtr)
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                return (srvResults(status, NULL));
            }
            else if (status == eOK)
            {
                row = sqlGetRow(res);
                misTrimcpy (pck_car_move_id, 
                sqlGetString(res, row, "car_move_id"), CAR_MOVE_ID_LEN);

                if (CurPtr)
                    srvFreeMemory(SRVRET_STRUCT, CurPtr);
                CurPtr = NULL;


                status = logPalletControlEvent(pck_car_move_id,
                                               cangrp,
                                               wrkref,
                                               wrkref_dtl,
                                               schbat,
                                               wh_id,
                                               palctl_flg_i,
                                               cancod_i);
                if (status != eOK)
                {
                    if (pckres) sqlFreeResults(pckres);
                    return (srvResults(status, NULL));
                }
            }
        }

            /* If this pick is for an order line that is planned into a
             * tms shipment, and there are no longer any picks, replens,
             * or cross dock work allocated for the tms_ordnum, then
             * then we may need to reset the tms_ord_stat back to "1".
             */

            if (strlen(ship_line_id))
            {    
                sprintf(buffer,
                    " reset tms order status "
                    " where ship_line_id = '%s' ",
                    ship_line_id);

                status = srvInitiateCommand(buffer, NULL);
                if (status != eOK)
                {
                    sqlFreeResults(wrkres);
                    if (pckres) sqlFreeResults(pckres);
                    return(srvResults(status, NULL));
                }
            }

        /*
         * Now...with everything deallocated, let's make an
         * attempt to clear the resource location...
         * Note: We don't need the return status, as it
         * may error, if there is nothing to clear. So set
         * the ignore_errors flag to TRUE so that the error
         * is not thrown in Deallocate Resource Location.
         */

        for (pckrow = sqlGetRow(pckres);
             pckrow; pckrow = sqlGetNextRow(pckrow))
        {
            sqlSetSavepoint("before_resource_deallocate");
            sprintf(buffer,
                "deallocate resource location "
                "where stoloc        = '%s'"
                "  and wh_id         = '%s'"
                "  and ignore_errors = '%d'",
                sqlGetString(pckres, pckrow, "stoloc"),
                sqlGetString(pckres, pckrow, "wh_id"),
                BOOLEAN_TRUE);
            CurPtr1 = NULL;
            status = srvInitiateCommand(buffer, &CurPtr1);
            srvFreeMemory(SRVRET_STRUCT, CurPtr1);
            CurPtr1 = NULL;
            if (status != eOK)
            sqlRollbackToSavepoint("before_resource_deallocate");
        }

        /*
         *  Error the source location, if required
         */
        if (flag_error == BOOLEAN_TRUE && !sqlIsNull(wrkres, wrkrow, "srcloc"))
        {
            sprintf(buffer,
                " error location "
                " where stoloc = '%s'"
                "   and wh_id = '%s'",
                sqlGetString(wrkres, wrkrow, "srcloc"), 
                        wh_id);

            status = srvInitiateCommand(buffer, NULL);
            if (status != eOK)
            {
            sqlFreeResults(wrkres);
            return(srvResults(status, NULL));
            }
        }
    }
    sqlFreeResults(wrkres);

    /* Finally, if this was cartonized, clean up the kits */


    if (strlen(ctnnum))
    {
        sprintf(buffer,
            "select wrkref, wrkref_dtl "
            "  from pckwrk_view "
            " where ctnnum = '%s' "
            "   and prtnum != '%s' for update ", /* Exclude the box pick for this carton */
            ctnnum, PRTNUM_KIT);

        status = sqlExecStr(buffer, NULL);
        if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
        {
            return(srvResults(status, NULL));
        }
        if (status == eDB_NO_ROWS_AFFECTED)
        {
            /* 
             * No more detail picks for this carton exist.
             * Let's clean up the kit pick.
             */

            /*
             * First we need to save off the shipment_line associated with
             * the kit pick (if there is one), because we'll try to reset
             * the shipment status once the kit pick is deleted.  Doing in
             * before will do no good, since the kit pick will prevent a 
             * shipment from going back to Ready.
             */

            sprintf(buffer,
                "select ship_line_id, list_id "
                "  from pckwrk_view "
                " where subnum = '%s' ",
                ctnnum);
            status = sqlExecStr(buffer, &shpres);
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
            return(srvResults(status, NULL));
            }
            if (status == eOK)
                {
                    shprow = sqlGetRow(shpres);
                if (!sqlIsNull(shpres, shprow, "ship_line_id"))
                    misTrimcpy(ship_line_id, 
                      sqlGetString(shpres, shprow, "ship_line_id"), 
                      SHIP_LINE_ID_LEN);
                if (!sqlIsNull(shpres, shprow, "list_id"))
                    misTrimcpy(list_id, 
                                sqlGetString(shpres, shprow, "list_id"), 
                                LIST_ID_LEN);
            sqlFreeResults(shpres);
                }
            /* At this point there are no detail picks for the carton.
             * The only remaining picks are 
             *            kit picks (if any) wrktyp = K  and prtnum = KITPART
             *        and box picks(if any) wrktyp = P and prtnum = KITPART 
             * we need to delete the qvlwrk, pckmovs and pckwrk_hdr/pckwrk_dtl(below) created for 
             * these picks also. 
             */
            sprintf(buffer,
                "[select distinct "
                "        cmbcod, "
                "        stoloc, "
                "        wh_id " 
                "  from pckmov "
                " where cmbcod in "
                " (select cmbcod from pckwrk_view "
                "   where subnum in ( select nvl(p1.subnum, p1.ctnnum) subnum "
                "                       from pckwrk_view p1 "
                "                      where nvl(p1.ctnnum, p1.subnum) = '%s') "
                "     and wrktyp in ('%s', '%s'))] "
                "|"
                "[delete from qvlwrk "
                "    where stoloc = @stoloc "
                "      and prtnum = '%s' "
                "      and wh_id  = @wh_id]catch (-1403) "
                "|"
                "[delete from pckmov "
                "   where cmbcod = @cmbcod]catch (-1403) ",
                ctnnum, WRKTYP_KIT, WRKTYP_PICK, PRTNUM_KIT);
            status = srvInitiateCommand(buffer, NULL);
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
            return(srvResults(status, NULL));
            }
            /* While cancelling the box picks at this stage the box picks 
             * (work type = 'P') and kit picks(work type = 'K') will be remaining
             * so to delete the box in a box pick which will be having the 
             * work type ='P' the work type 'P' is being passed here.
             */
            sprintf(buffer,
                    "[select wrkref,"
                    "        wrktyp "
                    "   from pckwrk_view "
                    "  where (subnum = '%s' or ctnnum = '%s') "
                    "    and wrktyp in ('%s', '%s')]"
                    "|"
                    "remove pick work"
                    "  where subnum = '%s' "
                    "    and wrktyp = @wrktyp "
                    "|"
                    "[select reqnum "
                    "   from wrkque "
                    "  where wrkref = @wrkref]catch(-1403)"
                    "|"
                    "remove work request "
                    " where reqnum = @reqnum catch(-1403)", 
                    ctnnum, ctnnum, WRKTYP_KIT, WRKTYP_PICK, ctnnum);
            status = srvInitiateCommand(buffer, NULL);
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
            return(srvResults(status, NULL));
            }
            /* WMD-37718. We try to clear pcklst and wrkque here, because if it is
             * the last pick to be cancelled in the list, pcklst and wrkque entries
             * for the list will not be cleared. The reason is that 
             * wrktyp 'P' picks are the ones added to canpck and these
             * do not have the list_id on them, only wrktyp 'K' picks have
             * the list_id, which isn't added to canpck, so the
             * triggers to clean up lists and wrkque, which rely on the canpck
             * record to have the list_id on it are not firing
             */
            if (strlen(list_id))
            {
                sprintf(buffer,
                        "select 'x' "
                        "  from pckwrk_hdr "
                        " where list_id = '%s'",
                        list_id);
                status = sqlExecStr(buffer, NULL);
                if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                {
                    return(srvResults(status, NULL));
                }
                if (status == eDB_NO_ROWS_AFFECTED)
                {
                    sprintf(buffer,
                            " remove pick list "
                            "  where list_id = '%s' "
                            "    and wh_id = '%s'",
                            list_id, wh_id);
                    status = srvInitiateCommand(buffer, NULL);
                    /* we may have no such list_id in pcklst table */
                    if (status != eOK && status != eAPP_INVALID_ARG)
                    {
                        return(srvResults(status, NULL));
                    }
                    sprintf(buffer,
                            "[select reqnum "
                            "  from wrkque "
                            " where list_id = '%s'] "
                            "| "
                            "remove work request "
                            " where reqnum = @reqnum ",
                            list_id);
                    status = srvInitiateCommand(buffer, NULL);
                    if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
                    {
                        return(srvResults(status, NULL));
                    }
                }            
            }
            /*
             * First, we may need to reset the shipment status.
             * Normally, this happens in deallocate inventory.  However, since
             * we call deallocate inventory on all picks for the kit before
             * removing the kit, the call to reset shipment status will never
             * success because of the kit pick that we're just processing now.
             * Therefore, now that we've cleaned up the kit pick, let's see if
             * the shipment status needs to be reset. 
             */

           if (strlen(ship_line_id))
               {
                   sprintf(buffer,
                       " reset shipment status "
                       " where ship_line_id = '%s' ",
                       ship_line_id);

                   status = srvInitiateCommand(buffer, NULL);
                   if (status != eOK)
                   {
                       return(srvResults(status, NULL));
                   }
               }
          
                /*
                 * Now...with everything deallocated, let's make an
                 * attempt to clear the resource location...
                 * Note: We don't need the return status, as it
                 * may error, if there is nothing to clear. So set
                 * the ignore_errors flag to TRUE so that the error
                 * is not thrown in Deallocate Resource Location.
                 */

               for (pckrow = sqlGetRow(pckres);
                    pckrow; pckrow = sqlGetNextRow(pckrow))
               {
                   sqlSetSavepoint("before_resource_deallocate");
                   sprintf(buffer,
                           "deallocate resource location "
                           "where stoloc        = '%s'"
                           "  and wh_id         = '%s'"
                           "  and ignore_errors = '%d'",
                           sqlGetString(pckres, pckrow, "stoloc"),
                           sqlGetString(pckres, pckrow, "wh_id"),
                           BOOLEAN_TRUE);
                   CurPtr1 = NULL;
                   status = srvInitiateCommand(buffer, &CurPtr1);
                   srvFreeMemory(SRVRET_STRUCT, CurPtr1);
                   CurPtr1 = NULL;
                   if (status != eOK)
                       sqlRollbackToSavepoint("before_resource_deallocate");
               }
        }
        else
        {
            /* since there are picks left for the carton, see if all of them
             * have been completed, if so, we need to call complete overpack
             * kit to update the appqty for the pick, delete any work that
             * exists for the kit pick and set the wrkref on the invsub
             * record
             */
            sprintf(buffer,
                    " select 1 "
                    "   from pckwrk_hdr p "
                    "  where p.ctnnum = '%s' "
                    "    and appqty < pckqty ",
                    ctnnum);
    
            status = sqlExecStr(buffer, NULL);
    
            if (status != eOK && status != eDB_NO_ROWS_AFFECTED)
            {
                if (pckres) sqlFreeResults(pckres);
                misTrc(T_FLOW,
                       "Error (%ld) determining if "
                       "carton had been picked complete",
                       status);
                return(srvResults(status, NULL));
            }
            else if (status == eDB_NO_ROWS_AFFECTED)
            {
                sprintf(buffer,
                        " complete overpack kit "
                        "    where kitnum = '%s' ",
                        ctnnum);
        
                status = srvInitiateCommand(buffer, NULL);
        
                if (status != eOK)
                {
                    if (pckres) sqlFreeResults(pckres);
                    misTrc(T_FLOW,
                           "Error (%ld) completing overpack kit",
                           status);
                    return(srvResults(status, NULL));
                }
    
                /* make sure that the carton is movable - during pick and pass,
                 * a carton is made non-movable when it is deposited into the 
                 * Pick N Pass drop area.  We need to make sure the carton is
                 * movable so that it can move the rest of the way through
                 * the warehouse
                 */
                sprintf(buffer,
                        " update invsub "
                        "    set mvsflg = '%ld' "
                        "  where subnum = '%s' ",
                        BOOLEAN_TRUE, ctnnum);

                status = sqlExecStr(buffer, NULL);

                if (status != eOK)
                {
                    misTrc(T_FLOW,
                           "Error (%ld) setting case movable",
                           status);
                    if (pckres) sqlFreeResults(pckres);
                    return(srvResults(status, NULL));
                }
            }
        }
    }

    if (pckres) sqlFreeResults(pckres);
    pckres = NULL;

    /* select everything that was written to the canpck table so it can
       be returned again. */

    sprintf(buffer,
            "select ship_id, ship_line_id, prtnum, prt_client_id, "
            "       wkonum, wkorev, wkolin, "
            "       client_id, ordnum, ordlin, ordsln, "
            "       (pckqty - appqty) delqty, srcloc, srcare, dstare,"
            "       ctnnum, cancod, wh_id, wrkref, wrkref_dtl, schbat "
            " from canpck "
            " where cangrp = '%s' ",
        cangrp);
   status = sqlExecStr(buffer, &canres);
   
   if (status != eOK)
   {
      sqlFreeResults(canres);
      return srvResults(status, NULL);
   }

   /* Initialize the result set */
    
   CurPtr = NULL;
   CurPtr = srvResultsInit(eOK,
                           "ship_id",    COMTYP_CHAR, SHIP_ID_LEN,
                           "ship_line_id",    COMTYP_CHAR, SHIP_LINE_ID_LEN,
                           "prtnum", COMTYP_CHAR, PRTNUM_LEN,
                           "prt_client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                           "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                           "wkonum",    COMTYP_CHAR, WKONUM_LEN,
                           "wkorev",    COMTYP_CHAR, WKOREV_LEN,
                           "wkolin",    COMTYP_CHAR, WKOLIN_LEN,
                           "ordnum",    COMTYP_CHAR, ORDNUM_LEN,
                           "ordlin",    COMTYP_CHAR, ORDLIN_LEN,
                           "ordsln",    COMTYP_CHAR, ORDSLN_LEN,
                           "delqty",    COMTYP_INT,  sizeof(long),
                           "srcloc",    COMTYP_CHAR, STOLOC_LEN,
                           "srcare",    COMTYP_CHAR, SRCARE_LEN,
                           "dstare",    COMTYP_CHAR, DSTARE_LEN,
                           "ctnnum",    COMTYP_CHAR, CTNNUM_LEN,
                           "cangrp",    COMTYP_CHAR, CANGRP_LEN,
                           "cancod",    COMTYP_CHAR, CODVAL_LEN,
                           "wh_id",    COMTYP_STRING, WH_ID_LEN,
                           "wrkref",    COMTYP_CHAR, WRKREF_LEN,
                           "wrkref_dtl", COMTYP_CHAR, WRKREF_DTL_LEN,
                           "schbat", COMTYP_CHAR, SCHBAT_LEN,
                           "palctlsts", COMTYP_CHAR, PALCTLSTS_LEN,
                           NULL);
      
   canrow = sqlGetRow(canres);

   while (canrow != NULL)
   {
       /* Publish everything  that used to be published and include the 
        * cangrp so people can reference the canpck in triggers 
    * if they want. 
    */
     /* Publish schbat to reference it in the triggers followed. */

      delqty = sqlGetLong(canres, canrow, "delqty");

      srvResultsAdd(CurPtr,
               sqlIsNull(canres, canrow, "ship_id") ? ""
                : sqlGetString(canres, canrow, "ship_id"),
            sqlIsNull(canres, canrow, "ship_line_id") ? ""
                : sqlGetString(canres, canrow, "ship_line_id"),
            sqlIsNull(canres, canrow, "prtnum") ? ""
                        : sqlGetString(canres, canrow, "prtnum"),
            sqlIsNull(canres, canrow, "prt_client_id") ? ""
                        : sqlGetString(canres, canrow, "prt_client_id"),
            sqlIsNull(canres, canrow, "client_id") ? ""
                        : sqlGetString(canres, canrow, "client_id"),
            sqlIsNull(canres, canrow, "wkonum") ? ""
                : sqlGetString(canres, canrow, "wkonum"),
            sqlIsNull(canres, canrow, "wkorev") ? ""
                : sqlGetString(canres, canrow, "wkorev"),
            sqlIsNull(canres, canrow, "wkolin") ? ""
                : sqlGetString(canres, canrow, "wkolin"),
            sqlIsNull(canres, canrow, "ordnum") ? ""
                : sqlGetString(canres, canrow, "ordnum"),
            sqlIsNull(canres, canrow, "ordlin") ? ""
                : sqlGetString(canres, canrow, "ordlin"),
            sqlIsNull(canres, canrow, "ordsln") ? ""
                : sqlGetString(canres, canrow, "ordsln"),
            delqty,
            sqlGetString(canres, canrow, "srcloc"),
            sqlIsNull(canres, canrow, "srcare") ? ""
                : sqlGetString(canres, canrow, "srcare"),
            sqlIsNull(canres, canrow, "dstare") ? ""
                : sqlGetString(canres, canrow, "dstare"),
            sqlIsNull(canres, canrow, "ctnnum") ? ""
                : sqlGetString(canres, canrow, "ctnnum"), 
                    cangrp,
                    sqlIsNull(canres, canrow, "cancod") ? ""
                                : sqlGetString(canres, canrow, "cancod"),
            sqlIsNull(canres, canrow, "wh_id") ? ""
                : sqlGetString(canres, canrow, "wh_id"),
            sqlIsNull(canres, canrow, "wrkref") ? ""
                : sqlGetString(canres, canrow, "wrkref"),
            sqlIsNull(canres, canrow, "wrkref_dtl") ? ""
                : sqlGetString(canres, canrow, "wrkref_dtl"),
            sqlIsNull(canres, canrow, "schbat") ? ""
                : sqlGetString(canres, canrow, "schbat"),
            palctlsts);

       canrow = sqlGetNextRow(canrow);  
    }

    sqlFreeResults (canres);
    return (CurPtr);
}
