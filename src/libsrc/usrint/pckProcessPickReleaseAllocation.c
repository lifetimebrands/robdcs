static char    *rcsid = "$Id: pckProcessPickReleaseAllocation.c 703581 2014-01-13 12:29:07Z sdurgam $";
/*#START***********************************************************************
 *  RedPrairie Corporation
 *  Copyright 2003
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "pckRELlib.h"

moca_bool_t gCacheLocations = 0;

typedef struct {
    char ftpcod[FTPCOD_LEN+1];
    int  length;
    mocaDataRow *row;
} NODE_SEARCH_STRUCT; 

typedef struct _alloc_queue
{
    long processed;
    char schbat[SCHBAT_LEN+1];
    char srcare[ARECOD_LEN+1];
    char cmbcod[CMBCOD_LEN+1];
    char wrktyp[WRKTYP_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    char srcloc[STOLOC_LEN+1];
    char dstare[ARECOD_LEN+1];
    char dstloc[STOLOC_LEN+1];  
    char wrkref[WRKREF_LEN+1];
    char lodlvl[LODLVL_LEN+1];
    char ftpcod[FTPCOD_LEN+1];
    long pckqty;
    long untcas;
    long seqnum;
    char wh_id[WH_ID_LEN + 1];
    char pm_arecod[ARECOD_LEN+1];
    char pm_stoloc[STOLOC_LEN+1];
    char pm_rescod[RESCOD_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char pm_rowid[utf8Size(100)];
    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char client_id[CLIENT_ID_LEN+1];

    /* space information */
    double piece_volume;
    double case_volume;
    double piece_length;
    double case_length;

    /* area props */
    moca_bool_t sigflg;
    moca_bool_t stgflg;
    char loccod[LOCCOD_LEN+1];
    char asset_typ[ASSET_TYP_LEN + 1];

    /* calculated measures for cmbcod */
    long cmb_tot_pckqty;      /* pckqty for cmbcod */
    double cmb_space_needed;  /* resource space needed for cmbcod */
    char shpsts[SHPSTS_LEN+1];
	
	/* MS-LBI*/
	char uc_csttyp[CSTTYP_LEN+1]; /* Customer Type for pick*/

    struct _alloc_queue *next;
} ALLOC_QUEUE;

typedef struct _location_list 
{
    long processed;

    char wh_id[WH_ID_LEN + 1];
    char stoloc[STOLOC_LEN+1];
    char rescod[RESCOD_LEN+1];
    double pndqvl;
    double curqvl;
    double maxqvl;
    moca_bool_t asgflg;
    char wrkzon[WRKZON_LEN+1]; 
    char pref_rescod[RESCOD_LEN+1];
    long pref_order;	
	
	/* MS-LBI*/
	char uc_csttyp[CSTTYP_LEN+1]; /* Customer Type for location*/
	double seqnum; /* Travel Sequence for location*/
	
    struct _location_list *next;
} LOCATION_LIST;

typedef struct _area_list
{
    char wh_id[WH_ID_LEN + 1];
    char arecod[ARECOD_LEN+1];
    LOCATION_LIST *Locations;

    struct _area_list *next;
} AREA_LIST;

typedef struct _space_reqs
{
    char wh_id[WH_ID_LEN + 1];
    char arecod[ARECOD_LEN+1];
    char rescod[RESCOD_LEN+1];
    char cmp_value[RESCOD_LEN+ARECOD_LEN+1];  
    double total_space_needed;
	
	/* MS-LBI*/
	double total_space_used; /* Total space used.*/	
    
	struct _space_reqs *next;
} SPACE_REQS;

#define HASH_SIZE 563 /* prime number */

static SPACE_REQS *SpaceRequiredTable[HASH_SIZE];

static long sGenHashKey(char *cmp_value)
{
    register char *cp;
    register unsigned long hash_sum;

    for (hash_sum = 0, cp = cmp_value; *cp; cp++)
    {
        hash_sum += (hash_sum * 13) + *cp;
    }
    return(hash_sum % HASH_SIZE);
}

static void sFreeSpaceReqs(SPACE_REQS *sp)
{
    SPACE_REQS *last_sp;

    last_sp = NULL;
    for (; sp; last_sp = sp, sp = sp->next)
    {
        if (last_sp)
            free(last_sp);
    }
    if (last_sp)
        free(last_sp);

    return;
}

static void sFreeSpaceRequiredTable()
{
    long i;

    for (i = 0; i < HASH_SIZE; i++)
    {
        if (SpaceRequiredTable[i])
            sFreeSpaceReqs(SpaceRequiredTable[i]);
        SpaceRequiredTable[i] = NULL;
    }
    return;
}

static SPACE_REQS *sGetHashEntry(char *wh_id,
                                 char *arecod,
                                 char *rescod)
{
    SPACE_REQS *sp, *last_sp;
    long hash_index;
    char cmp_value[RESCOD_LEN+ARECOD_LEN + WH_ID_LEN +1];

    memset(cmp_value, 0, sizeof(cmp_value));
    strcat(cmp_value, rescod);
    strcat(cmp_value, arecod);
    strcat(cmp_value, wh_id);

    hash_index = sGenHashKey(cmp_value);
    sp = SpaceRequiredTable[hash_index];

    if (!sp)
    {
        sp = (SPACE_REQS *) calloc(1, sizeof(SPACE_REQS));
        strcpy(sp->wh_id, wh_id);
        strcpy(sp->arecod, arecod);
        strcpy(sp->rescod, rescod);
        strcpy(sp->cmp_value, cmp_value);

        SpaceRequiredTable[hash_index] = sp;
        return(sp);
    }

    for (last_sp = NULL; sp; last_sp = sp, sp = sp->next)
    {
        if (strcmp(cmp_value, sp->cmp_value) == 0)
            return(sp);
    }
    sp = (SPACE_REQS *) calloc(1, sizeof(SPACE_REQS));
    strcpy(sp->wh_id, wh_id);
    strcpy(sp->arecod, arecod);
    strcpy(sp->rescod, rescod);
    strcpy(sp->cmp_value, cmp_value);

    pckrel_WriteTrc("     Hash table:  added chain for cmpval: %s,"
                    " hash_index: %d",
                    cmp_value, hash_index);
    last_sp->next = sp;
    return(sp);
}

static void sAddToSpaceRequired(char *wh_id,
                                char *arecod,
                                char *rescod,
                                double cmb_space_needed)
{
    SPACE_REQS *sp;

    sp = sGetHashEntry(wh_id, arecod, rescod);
    sp->total_space_needed += cmb_space_needed;
    return;
}

static void sAddToSpaceUsed(char *arecod,
							char *rescod,
							double space_used,
							char *wh_id)
{
	SPACE_REQS *sp;

	pckrel_WriteTrc("        MS-LBI:ID sAddToSpaceUsed" );
	pckrel_WriteTrc("        MS-LBI: arecod: %s, rescod: %s", arecod, rescod );
	pckrel_WriteTrc("        MS-LBI: AddToSpaceUsed: %f", space_used );

	sp = sGetHashEntry(wh_id, arecod, rescod);

	pckrel_WriteTrc("        MS-LBI: FoundArecod: %s, FoundRescod: %s", sp->arecod, sp->rescod );
	pckrel_WriteTrc("        MS-LBI: AddToSpaceUsed, orig space used: %f", sp->total_space_used );

	sp->total_space_used += space_used;

	pckrel_WriteTrc("        MS-LBI: TotalUsed is now: %f", sp->total_space_used );

	return;
}

static void sFreeAllocationQueue(ALLOC_QUEUE **Queue)
{
    ALLOC_QUEUE *qp, *last_qp;

    last_qp = NULL;
    for (qp = *Queue; qp; last_qp = qp, qp = qp->next)
    {
        if (last_qp)
        {
            free(last_qp);
        }
    }
    if (last_qp)
    {
        free(last_qp);
    }
    *Queue = NULL;
    return;
}

/*
 * sLoadAllocationQueue:
 *
 * This routine is used to load up all picks needing to be allocated
 * for pick release.  It is used by the main-line
 * pckProcessPickReleaseAllocation routine to load a queue which
 * is traversed throughout the rest of this routine.
 * 
 * Information passed to this function is used to pass along to 
 * the component: get pick release allocation queue which returns
 * the ordered list of picks to process.  The Queue variable is
 * written by this routine.  It points to a linked list of picks -
 * which should be freed upon mainline exit.
 */
static long sLoadAllocationQueue(ALLOC_QUEUE **Queue,
                                 long *looking_specific,
                                 char *cmbcod_i,
                                 char *ship_id_i,
                                 char *wrkref_i,
                                 char *pcksts,
                                 char *schbat,
                                 char *wh_id_i,
                                 long commit_as_we_go)
{
    char cmbcod[CMBCOD_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    char wrkref[WRKREF_LEN+1];
    char wh_id[WH_ID_LEN + 1];

    char whereclause[1000];
    char tmpbuf[utf8Size(200)];
    char buffer[4096];
    long ret_status;

    RETURN_STRUCT *CmdRes;
    mocaDataRow *row;
    mocaDataRes *res;
    ALLOC_QUEUE *head, *last, *qp;

    *Queue = NULL;
    head = NULL;
    last = NULL;

    memset(whereclause, 0, sizeof(whereclause));
	
	pckrel_WriteTrc("   MS-LBI: in sLoadAllocationQueue!" );
	
    if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
    {
        memset(cmbcod, 0, sizeof(cmbcod));
        misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
        sprintf(tmpbuf, " and cmbcod = '%s' ", cmbcod);
        strcat(whereclause, tmpbuf);
    }
    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
    {
        memset(ship_id, 0, sizeof(ship_id));
        misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);
        sprintf(tmpbuf, " and ship_id = '%s' ", ship_id);
        strcat(whereclause, tmpbuf);
    }
    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
    {
        memset(wrkref, 0, sizeof(wrkref));
        misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);
        sprintf(tmpbuf, " and wrkref = '%s' ", wrkref);
        strcat(whereclause, tmpbuf);
    }


    if (strlen(whereclause))
        *looking_specific = 1;

    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
    {
        memset(wh_id, 0, sizeof(wh_id));
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);
        sprintf(tmpbuf, " and wh_id = '%s' "   , wh_id);
        strcat(whereclause, tmpbuf);
    }

    sprintf(buffer,
            "get pick release allocation queue "
            "    where pcksts = \"%s\"         "
            "      and schbat = \"%s\"         "
            "      and comflg = %d             "
            "       %s",
            pcksts,
            schbat,
            commit_as_we_go,
            strlen(whereclause) == 0 ? "" : whereclause);

    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return(ret_status);
    }
    
    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        if (*looking_specific)
        {
            pckrel_WriteTrc("Didn't find any pick work/move on "
                            "specific read, returning error ...");
            return(ret_status);
        }
        pckrel_WriteTrc("  ...there are currently no"
                        " pick works to process! (ms=%ld)", 
                        pckrel_ElapsedMs());
        return(eOK);
    }
    /* Shucks...we've got some real work to do...load up the
     * structure... 
     */
    res = srvGetResults(CmdRes);

    pckrel_WriteTrc("  Read %ld outstanding pick work/move "
                    "entries for %s (ms=%ld)",
                    sqlGetNumRows(res),
                    schbat,
                    pckrel_ElapsedMs());

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        qp = (ALLOC_QUEUE *)calloc(1, sizeof(ALLOC_QUEUE));
        if (qp == NULL)
        {
            srvFreeMemory(SRVRET_STRUCT, CmdRes);
            pckrel_WriteTrc("  ...unable to load queue - no memory!");
            return(eNO_MEMORY);
        }

        if (!sqlIsNull(res, row, "schbat"))
            misStrncpyChars(qp->schbat, sqlGetString(res, row, "schbat"), SCHBAT_LEN);
        if (!sqlIsNull(res, row, "srcare"))
            misStrncpyChars(qp->srcare, sqlGetString(res, row, "srcare"), ARECOD_LEN);
        if (!sqlIsNull(res, row, "cmbcod"))
            misStrncpyChars(qp->cmbcod, sqlGetString(res, row, "cmbcod"), CMBCOD_LEN);
        if (!sqlIsNull(res, row, "wrktyp"))
            misStrncpyChars(qp->wrktyp, sqlGetString(res, row, "wrktyp"), WRKTYP_LEN);
        if (!sqlIsNull(res, row, "ship_id"))
            misStrncpyChars(qp->ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
        if (!sqlIsNull(res, row, "srcloc"))
            misStrncpyChars(qp->srcloc, sqlGetString(res, row, "srcloc"), STOLOC_LEN);
        if (!sqlIsNull(res, row, "dstare"))
            misStrncpyChars(qp->dstare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
        if (!sqlIsNull(res, row, "dstloc"))
            misStrncpyChars(qp->dstloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);
        if (!sqlIsNull(res, row, "wrkref"))
            misStrncpyChars(qp->wrkref, sqlGetString(res, row, "wrkref"), WRKREF_LEN);
        if (!sqlIsNull(res, row, "lodlvl"))
            misStrncpyChars(qp->lodlvl, sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
        if (!sqlIsNull(res, row, "ftpcod"))
            misStrncpyChars(qp->ftpcod, sqlGetString(res, row, "ftpcod"), FTPCOD_LEN);
        if (!sqlIsNull(res, row, "wh_id"))
            misStrncpyChars(qp->wh_id,  sqlGetString(res, row, "wh_id"),  WH_ID_LEN);
        if (!sqlIsNull(res, row, "asset_typ"))
            misStrncpyChars(qp->asset_typ, sqlGetString(res, row, "asset_typ"),ASSET_TYP_LEN);
        if (!sqlIsNull(res, row, "shpsts"))
            misStrncpyChars(qp->shpsts,
                            sqlGetString(res, row, "shpsts"),SHPSTS_LEN);

        qp->pckqty = sqlGetLong(res, row, "pckqty");
        qp->untcas = sqlGetLong(res, row, "untcas");
        qp->seqnum = sqlGetLong(res, row, "seqnum");


        if (!sqlIsNull(res, row, "pm_arecod"))
            misStrncpyChars(qp->pm_arecod, 
                    sqlGetString(res, row, "pm_arecod"), ARECOD_LEN);
        if (!sqlIsNull(res, row, "pm_stoloc"))
            misStrncpyChars(qp->pm_stoloc, 
                    sqlGetString(res, row, "pm_stoloc"), STOLOC_LEN);
        if (!sqlIsNull(res, row, "pm_rescod"))
            misStrncpyChars(qp->pm_rescod,
                    sqlGetString(res, row, "pm_rescod"), RESCOD_LEN);
        if (!sqlIsNull(res, row, "prtnum"))
            misStrncpyChars(qp->prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
        if (!sqlIsNull(res, row, "prt_client_id"))
            misStrncpyChars(qp->prt_client_id, 
                    sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
        if (!sqlIsNull(res, row, "pm_rowid"))
        {
            if (sqlGetDataType(res, "pm_rowid") == COMTYP_STRING)
            {
                strncpy(qp->pm_rowid, sqlGetString(res, row, "pm_rowid"), 100);
            }
            else
            {
                /* DB2/MSSQL have a numeric rowid. Binding will handle SQL
                ** statements, but we need to convert to C datatype here
                */
                sprintf(qp->pm_rowid, "%ld", sqlGetLong(res, row, "pm_rowid"));
            }
        }
        if (!sqlIsNull(res, row, "wkonum"))
            misStrncpyChars(qp->wkonum, sqlGetString(res, row, "wkonum"), WKONUM_LEN);
        if (!sqlIsNull(res, row, "wkorev"))
            misStrncpyChars(qp->wkorev, sqlGetString(res, row, "wkorev"), WKOREV_LEN);
        if (!sqlIsNull(res, row, "client_id"))
            misStrncpyChars(qp->client_id, 
                    sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

        if (!sqlIsNull(res, row, "piece_volume"))
            qp->piece_volume = sqlGetFloat(res, row, "piece_volume");
        if (!sqlIsNull(res, row, "case_volume"))
            qp->case_volume = sqlGetFloat(res, row, "case_volume");
        if (!sqlIsNull(res, row, "piece_length"))
            qp->piece_length = sqlGetFloat(res, row, "piece_length");
        if (!sqlIsNull(res, row, "case_length"))
            qp->case_length = sqlGetFloat(res, row, "case_length");
			
	    /* MS-LBI: Adding customer type to the structure.*/
		if (!sqlIsNull(res, row, "uc_csttyp"))
            misStrncpyChars(qp->uc_csttyp, 
                    sqlGetString(res, row, "uc_csttyp"), CSTTYP_LEN);		

        if (!sqlIsNull(res, row, "sigflg"))
            qp->sigflg = sqlGetBoolean(res, row, "sigflg");
        if (!sqlIsNull(res, row, "stgflg"))
            qp->stgflg = sqlGetBoolean(res, row, "stgflg");
        if (!sqlIsNull(res, row, "loccod"))
            misStrncpyChars(qp->loccod, sqlGetString(res, row, "loccod"), LOCCOD_LEN);

        if (last == NULL)
        {
            head = qp;
            last = qp;
        }
        else
        {
            last->next = qp;
            last = qp;
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    *Queue = head;
    return(eOK);
}

/*
 * This routine is used set all location preferences for a set 
 * of locations listed in a given area.  It performs its work
 * based off the return of the "get pick release location preferences"
 * component.  This routine allows us to send picks to staging lanes
 * which match up with where an appointment is scheduled or a truck is
 * already arrived.
 */
static void sSetLocationPreferences(LOCATION_LIST *Locations,
                                    char *wh_id_i,
                                    char *arecod, char *ship_id)
{
    long ret_status;
    RETURN_STRUCT *CmdRes;
    mocaDataRow *row;
    mocaDataRes *res;
    char wh_id[WH_ID_LEN + 1];
    char stoloc[STOLOC_LEN+1];
    char rescod[RESCOD_LEN+1];
    LOCATION_LIST *lp;

    ret_status = 
        srvInitiateInlineFormat(&CmdRes,
                                "get pick release location preferences"
                                " where wh_id = '%s' "
                                "   and arecod = '%s' "
                                "   and ship_id = '%s' ",
                                wh_id_i, arecod, ship_id);

    if (ret_status != eOK)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        return;
    }

    res = srvGetResults(CmdRes);
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        memset(wh_id, 0, sizeof(wh_id));
        memset(stoloc, 0, sizeof(stoloc));
        memset(rescod, 0, sizeof(rescod));


        misStrncpyChars(wh_id, sqlGetString(res, row, "wh_id"), WH_ID_LEN);      
        misStrncpyChars(stoloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
        misStrncpyChars(rescod, sqlGetString(res, row, "rescod"), RESCOD_LEN);

        for (lp = Locations; lp; lp = lp->next)
        {
            if (misStrncmpChars(lp->stoloc, stoloc, STOLOC_LEN) == 0 &&
                misStrncmpChars(lp->wh_id,  wh_id, WH_ID_LEN) == 0)
            {
                misStrncpyChars(lp->pref_rescod, rescod, RESCOD_LEN);
                if (strlen(lp->rescod) > 0 &&
                    strncmp(lp->rescod, "^^^^", 4) != 0 &&
                    misStrncmpChars(lp->rescod, rescod, RESCOD_LEN) != 0)
                {
                    pckrel_WriteTrc("   ** Note ** Wh %s Loc %s is preferred for "
                                    "rescod: %s, ",
                                    lp->wh_id, lp->stoloc, lp->pref_rescod);
                    pckrel_WriteTrc("              but currently assigned "
                                    "rescod: %s ",
                                    lp->rescod);
                }
                break;
            }
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);
    return;
}
/*
 * This routine is used to build the list of locations available for
 * use in a given area. 
 */
static long sGetLocationListForArea(char *wh_id,
                                    char *arecod,
                                    char *order_by,
                                    char *ship_id,
                                    AREA_LIST **AreaListHead,
                                    LOCATION_LIST **ret_loc)
{
    char buffer[1000];
    long ret_status;
    RETURN_STRUCT *CmdRes = NULL;
    mocaDataRow *row = NULL;
    mocaDataRes *res = NULL;
    DATA_STRUCT *data = NULL;
    RULES_STRUCT *rules = NULL;

    LOCATION_LIST *lp = NULL, *last_lp = NULL;
    AREA_LIST *ap = NULL, *previous = NULL, *next = NULL;

    pckrel_WriteTrc("MS-LBI: In sGetLocationListForArea!");
    pckrel_WriteTrc("MS-LBI: arecod: %s, order_by: %s, ship_id: %s", arecod, order_by, ship_id);
	
	ret_status = pckrel_GetConfig(&data, &rules, 0, wh_id);
    if (ret_status != eOK)
        return(0);

    /* To allow use of dock door associations for assigning pick 
       release locations the system must be configured to always
       reload the location list due to changing date that will
       change the preferential location availability.  We will 
       only always reload the location list for shippable picks 
       since dock associations only work for shipment staging 
       locations now.  This could be a performance hit in sites 
       with lots of picks to release in which case this policy 
       can be turned off */

    previous = NULL;
    for (ap = *AreaListHead; ap; ap = next)
    {
        if (misStrncmpChars(ap->arecod, arecod, ARECOD_LEN) == 0 &&
            misStrncmpChars(ap->wh_id, wh_id, WH_ID_LEN) == 0)
        {
           if(rules->reloadLocs == BOOLEAN_TRUE && ship_id != NULL)
           {
              /* We need to remove the area from the list
               * and reload it to get refreshed data for the
               * location preferences.  After we remove the node
               * we drop out of the for loop to continue on with
               * to get the data again for the area */
              
              /*this should handle the first and only case*/
              if (ap == *AreaListHead)
              {
                  /* Remove First Node */
                  *AreaListHead = ap->next;
              }
              /*this should handle the middle and last case*/
              else
              {
                  /*  Remove Middle or Last Node */
                  previous->next = ap->next;
              }
              /*Now we need to free the locations inside the area list*/
              for (lp = ap->Locations; lp; last_lp = lp, lp = lp->next)
              {
                  if (last_lp)
                      free(last_lp);
              }

              last_lp = NULL;
              lp = NULL;

              next = ap->next;
              free(ap);
              continue;

           }
           else
           {
              /* If we are not configured to always reload the list for
               * shipment destination areas (i.e. not using preferred locations)
               * then we just return the list from the firs time the area was
               * loaded.
               */
                  *ret_loc = ap->Locations;
                  return(eOK);
            }
       }
       previous = ap;
       next = ap->next;
    }

    /* Didn't find it, or we need to always need to reload - go 
       ahead and load */

	pckrel_WriteTrc("MS-LBI: Reloading Locations . . .");
	
    sprintf(buffer,
            "get pick release locations for area "
            " where arecod = '%s'"
            "   and order_by = '%s'"
            "   and wh_id = '%s' ",
            arecod, order_by, wh_id);

    CmdRes = NULL;
    ret_status = srvInitiateCommand(buffer, &CmdRes);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        pckrel_WriteTrc("Error (%d) loading locations for area: %s",
                        ret_status, arecod);
        *ret_loc = NULL;
        return(ret_status);
    }

    ap = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
    if (ap == NULL)
    {
        srvFreeMemory(SRVRET_STRUCT, CmdRes);
        pckrel_WriteTrc("Error allocating memory for area list");
        *ret_loc = NULL;
        return(eNO_MEMORY);
    }
    misStrncpyChars(ap->wh_id, wh_id, WH_ID_LEN);
    misStrncpyChars(ap->arecod, arecod, ARECOD_LEN);

    ap->next = *AreaListHead;
    *AreaListHead = ap;


    /* ret_status may be "no rows affected" here, so if that is the
     * case, we just want to maintain an area entry so we don't sit
     * and bash the db
     */
    if (ret_status == eOK)
    {
        res = srvGetResults(CmdRes);
        last_lp = NULL;
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            lp = (LOCATION_LIST *)calloc(1, sizeof(LOCATION_LIST));
            if (lp == NULL)
            {
                pckrel_WriteTrc("Error allocating memory for location list");
                *ret_loc = NULL;
                srvFreeMemory(SRVRET_STRUCT, CmdRes);
                return(eNO_MEMORY);
            }

            misStrncpyChars(lp->wh_id,
                    sqlGetString(res, row, "wh_id"), WH_ID_LEN);

            misStrncpyChars(lp->stoloc,
                    sqlGetString(res, row, "stoloc"), STOLOC_LEN);
            if (sqlIsNull(res, row, "rescod"))
            {
                memset(lp->rescod, '^', RESCOD_LEN);
            }
            else
            {
                misStrncpyChars(lp->rescod,
                        sqlGetString(res, row, "rescod"), RESCOD_LEN);
            }
            misStrncpyChars(lp->wrkzon, 
                    sqlGetString(res, row, "wrkzon"), WRKZON_LEN);
            lp->maxqvl = sqlGetFloat(res, row, "maxqvl");
            lp->curqvl = sqlGetFloat(res, row, "curqvl");
            lp->pndqvl = sqlGetFloat(res, row, "pndqvl");
            lp->asgflg = sqlGetBoolean(res, row, "asgflg");
			
            /* MS-LBI: Adding uc_csttyp, trvseq to the location list*/
			lp->seqnum = sqlGetFloat(res, row, "trvseq");			
			misStrncpyChars(lp->uc_csttyp, 
                    sqlGetString(res, row, "uc_csttyp"), CSTTYP_LEN);

			pckrel_WriteTrc("EM: loaded loc: %s, rescod: %s, maxqvl: %f, curqvl: %f, pndqvl: %f, asgflg: %ld, wrkzon: %s, seqnum: %f, uc_csttyp: %s",
				lp->stoloc,
				lp->rescod,
				lp->maxqvl,
				lp->curqvl,
				lp->pndqvl,
				lp->asgflg,
				lp->wrkzon,
				lp->seqnum,
				lp->uc_csttyp);
					
            if (last_lp)
            {
                last_lp->next = lp;
            }
            else
            {
                ap->Locations = lp;
            }
            last_lp = lp;
        }
    }
    srvFreeMemory(SRVRET_STRUCT, CmdRes);

    sSetLocationPreferences(ap->Locations, wh_id, arecod, ship_id);

    *ret_loc = ap->Locations;
    return(eOK);
}

static void sFreeLocationLists(AREA_LIST **AreaListHead)
{
    AREA_LIST *ap, *last_ap;
    LOCATION_LIST *lp, *last_lp;
    
    /* If caching is turned on...don't bother with the free */
    if (gCacheLocations)
        return;

    misTrc(T_FLOW, "Freeing the location lists");

    last_ap = NULL;
    for (ap = *AreaListHead; ap; last_ap = ap, ap = ap->next)
    {
        if (last_ap)
            free(last_ap);

        last_lp = NULL;
        for (lp = ap->Locations; lp; last_lp = lp, lp = lp->next)
        {
            if (last_lp)
                free(last_lp);
        }
        if (last_lp)
            free(last_lp);
    }
    if (last_ap)
        free(last_ap);

    *AreaListHead = NULL;
    return;
}

/* 
 * Get asset info such as height and volume
 */
static long sGetAssetDimension(char *asset_typ_i,
                                  double *asset_hgt_o,
                                  double *asset_vol_o)
{
    static mocaDataRes *assetRes = NULL;
    mocaDataRow *assetRow = NULL;

    char buffer[utf8Size(500)];
    long ret_status;

    *asset_hgt_o = 0.0;
    *asset_vol_o = 0.0;

    if (!asset_typ_i || !misTrimLen(asset_typ_i, ASSET_TYP_LEN))
        return (eOK);

    if (assetRes == NULL)
    {
        /* The volume of an asset should be determined in one of two ways. If
         * the asset is a container, the max_vol is the volume of the asset.
         * If the asset is not a container, such as a pallet, the volume of
         * the asset is determined from its length, width, and height. */
        memset(buffer, 0, sizeof(buffer));

        sprintf(buffer,
                " select asset_typ, "
                "        asset_hgt, "
                "        decode(container_flg, "
                "               1, "
                "               max_vol, "
                "               (asset_len * asset_wid * asset_hgt)) asset_vol "
                "   from asset_typ");

        ret_status = sqlExecStr(buffer, &assetRes);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sqlFreeResults(assetRes);
            assetRes = NULL;

            return (ret_status);
        }

        /* Cache the assets. */
        misFlagCachedMemory((OSFPTR) sqlFreeResults, assetRes);
    }

    for (assetRow=sqlGetRow(assetRes); assetRow; assetRow = sqlGetNextRow(assetRow))
    {
        if (misStrncmpChars(asset_typ_i, sqlGetString(assetRes, assetRow, "asset_typ"),
                    ASSET_TYP_LEN) == 0)
        {
            *asset_hgt_o = sqlGetFloat(assetRes, assetRow, "asset_hgt");
            *asset_vol_o = sqlGetFloat(assetRes, assetRow, "asset_vol");

            break;
        }
    }

    return (eOK);
}

static double   sCalculateFtpInvVolume(ALLOC_QUEUE *qp)
{ 
       double total_volume; 
       long num_full_cases; 
       long num_partial_cases; 
       double total_cases; 
       char lodlvl[LODLVL_LEN+1]; 

       /* If the load level is at a load or subload level 
          Then we will assume the case foot print is used 
          for the left over units (i.e. less than a full case 
          is still sitting in the same size case. 

          At the detail level we will first attempt to use 
          the untlen, untwid, unthgt values. If they are 
          not there or are zero, we will use the case values 
          and divide the untqty by the untcas and then multiply 
          by the case volume. 
       /* ********************** */ 

       memset(lodlvl, 0, sizeof(lodlvl)); 
       misTrimcpy(lodlvl, qp->lodlvl, LODLVL_LEN);

       /* Calculate the number of cases. The untcas may be zero if we
        * are picking from a location with mixed untcas. To prevent
        * division by zero we just default to a untcas of 1. 
        */ 
       num_full_cases = qp->pckqty / (qp->untcas ? qp->untcas : 1);

       num_partial_cases = (qp->pckqty % (qp->untcas ? qp->untcas : 1)) ? 1 : 0;

       total_cases = (double) qp->pckqty / (double) (qp->untcas ? qp->untcas : 1);

       if (misStrncmpChars(lodlvl, LODLVL_DETAIL, LODLVL_LEN)==0)
       { 
           if (qp->piece_volume > 0)
           { 

               total_volume = 
                   (double) qp->pckqty * qp->piece_volume;
           } 
           else
           { 
               /* This uses the number of full cases plus the fractional 
                  case value. */ 
               total_volume = total_cases * qp->case_volume;
           } 
       } 
       else
       { 
          /* This uses the number of full cases plus one for the fraction 
             of a case. */ 
           total_volume = 
               ((double)num_full_cases + (double)num_partial_cases) 
               * qp->case_volume;
       } 

    return(total_volume); 

} 

static double   sCalculateFtpInvLength(ALLOC_QUEUE *qp)
{ 
       double total_length; 
       long num_full_cases; 
       long num_partial_cases; 
       double total_cases; 
       char lodlvl[LODLVL_LEN+1]; 

       /* If the load level is at a load or subload level 
          Then we will assume the case foot print is used 
          for the left over units (i.e. less than a full case 
          is still sitting in the same size case. 

          At the detail level we will first attempt to use 
          the untlen value. If it is not there or it is zero, 
          we will use the case value and divide the untqty by 
          the untcas and then multiply by the case length. 
       /* ********************** */ 

       memset(lodlvl, 0, sizeof(lodlvl)); 
       misTrimcpy(lodlvl, qp->lodlvl, LODLVL_LEN); 

       /* Calculate the number of cases. The untcas may be zero if we
        * are picking from a location with mixed untcas. To prevent
        * division by zero we just default to a untcas of 1. 
        */ 
       num_full_cases = qp->pckqty / (qp->untcas ? qp->untcas : 1);

       num_partial_cases = (qp->pckqty % (qp->untcas ? qp->untcas : 1)) ? 1 : 0;

       total_cases = (double) qp->pckqty / (double) (qp->untcas ? qp->untcas : 1);

       if (misStrncmpChars(lodlvl, LODLVL_DETAIL, LODLVL_LEN)==0)
       { 
           if (qp->piece_length > 0)
           { 
               total_length = (double) qp->pckqty * qp->piece_length;
           } 
           else
           { 
               total_length = total_cases * qp->case_length;
           } 
       } 
       else
       { 
           total_length = ((double)num_full_cases + (double)num_partial_cases) 
               * qp->case_length;
       } 
       return(total_length); 
}
static void sSetCmbcodSpace(ALLOC_QUEUE *start_qp, 
                            ALLOC_QUEUE *last_qp, 
                            long pckqty,
                            double space_needed)
{
    ALLOC_QUEUE *qp;

    qp = start_qp; 
    do 
    {
        qp->cmb_tot_pckqty = pckqty;
        qp->cmb_space_needed = space_needed;

        qp = qp->next;
    } while (qp != last_qp->next && qp);

    return;
}

static ALLOC_QUEUE *sCalculateSpaceForCmbcod(ALLOC_QUEUE *qp, char *wh_id)
{
    register ALLOC_QUEUE *start_qp, *last_qp;
    long pckqty;
    double space_needed;
    DATA_STRUCT *data;
    RULES_STRUCT *rules;

    double asset_hgt;
    double asset_vol;

    pckrel_GetConfig(&data, &rules, 0, wh_id);

    pckqty = 0;
    space_needed = 0;
    start_qp = qp;
    last_qp = qp;
    for (; qp; last_qp = qp, qp = qp->next)
    {
        if (strcmp(qp->pm_rescod, start_qp->pm_rescod) != 0 ||
            strcmp(qp->pm_arecod, start_qp->pm_arecod) != 0 ||
            strcmp(qp->wh_id,     start_qp->wh_id    ) != 0 ||
            strcmp(qp->cmbcod, start_qp->cmbcod) != 0)
        {
            /* PR 56507
             * if start_qp->asset_typ is not empty, and start_qp->lodlvl is 'L',
             * and start_qp->loccod is 'V' (volume tracked), need to calculate 
             * the asset volume and add it to total volume.
             *
             * But Please NOTE !!!!!!!!!!
             * for a cmbcod, we can only count asset once.
             */      
            if (misTrimLen(start_qp->asset_typ, ASSET_TYP_LEN) > 0 &&
                misStrncmpChars(start_qp->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 &&
                misStrncmpChars(start_qp->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0)
            {
                sGetAssetDimension(start_qp->asset_typ,
                                      &asset_hgt,
                                      &asset_vol);
                space_needed += asset_vol;
            }

			pckrel_WriteTrc("        MS-LBI: SpaceNeeded is  %f", space_needed );
            /* we're all done */
            sSetCmbcodSpace(start_qp, 
                            last_qp, 
                            pckqty,
                            space_needed);
            return(qp);
        }
        /* Still the same cmbcod, rescod, and arecod */
        pckqty += qp->pckqty;
		
		pckrel_WriteTrc("        MS-LBI: loccod is %s, lodlvl is %s", qp->loccod, qp->lodlvl );

        if (misStrncmpChars(qp->loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
        {
            space_needed = 1.0;
        }
        else if (misStrncmpChars(qp->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
        {
            space_needed += sCalculateFtpInvLength(qp);
        }
        else
        {
            space_needed += sCalculateFtpInvVolume(qp);
        }
    }

    /* PR 56507
     * if start_qp->asset_typ is not empty, and start_qp->lodlvl is 'L',
     * and start_qp->loccod is 'V' (volume tracked), need to calculate 
     * the asset volume and add it to total volume.
     *
     * But Please NOTE !!!!!!!!!!
     * for a cmbcod, we can only count asset once.
     */
    if (misTrimLen(start_qp->asset_typ, ASSET_TYP_LEN) > 0 &&
        misStrncmpChars(start_qp->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0 &&
        misStrncmpChars(start_qp->loccod, LOCCOD_VOLUME, LOCCOD_LEN) == 0)
    {
        sGetAssetDimension(start_qp->asset_typ,
                              &asset_hgt,
                              &asset_vol);
        space_needed += asset_vol;
    }

    pckrel_WriteTrc("        MS-LBI: SpaceNeeded is  %f", space_needed );
	
	sSetCmbcodSpace(start_qp, 
                    last_qp, 
                    pckqty,
                    space_needed);
    return(NULL);
}
static void sSetResourceSpaceRequirements(ALLOC_QUEUE *qp, char *wh_id)
{
    ALLOC_QUEUE *tmp_qp;

    while (qp)
    {
		pckrel_WriteTrc("        MS-LBI: ID sSetResourceSpaceRequirements %s", qp->cmbcod);
		pckrel_WriteTrc("        MS-LBI: rescod is %s, arecod is %s", qp->pm_rescod, qp->pm_arecod);
  
        tmp_qp = qp;
        qp = sCalculateSpaceForCmbcod(qp, wh_id);
        sAddToSpaceRequired(wh_id,
                            tmp_qp->pm_arecod,
                            tmp_qp->pm_rescod,
                            tmp_qp->cmb_space_needed);
    }
    return;
}
static void sGetSpaceReqs(ALLOC_QUEUE *qp,
                          double *cmb_space_needed,
                          double *con_space_needed,
                          long *cmb_tot_pckqty)
{
    SPACE_REQS *sp;
	
	pckrel_WriteTrc("        MS-LBI:ID sGetSpaceReqs" );
	
    sp = sGetHashEntry(qp->wh_id,
                       qp->pm_arecod,
                       qp->pm_rescod);
					   
    pckrel_WriteTrc("        MS-LBI:  arecod: %s, rescod %s ", qp->pm_arecod, qp->pm_rescod );
    pckrel_WriteTrc("        MS-LBI:  sp->total_pace_needed: %f", sp->total_space_needed );
    pckrel_WriteTrc("        MS-LBI:  sp->total_pace_used: %f", sp->total_space_used );

    *cmb_space_needed = qp->cmb_space_needed;
    *con_space_needed = sp->total_space_needed - sp->total_space_used;
    *cmb_tot_pckqty = qp->cmb_tot_pckqty;
	
	pckrel_WriteTrc("        MS-LBI: setting cmb_space_needed to %f", *cmb_space_needed );
    pckrel_WriteTrc("        MS-LBI: setting con_space_needed to %f", *con_space_needed );
    pckrel_WriteTrc("        MS-LBI: setting cmb_tot_pckqty to %ld", *cmb_tot_pckqty );
	
    return;
}

static int  sRequiresSameSrcZone(char *wh_id,
                                 char *src_arecod,
                                 char *dst_arecod,
                                 char *src_lodlvl,
                                 char *fin_arecod)
{
    long            ii;
    short           found_it;
    DATA_STRUCT *data;
    RULES_STRUCT *rules;
    long ret_status;

    ret_status = pckrel_GetConfig(&data, &rules, 0, wh_id);
    if (ret_status != eOK)
        return(0);

    ii = rules->move_path_start;
    found_it = FALSE;    
    misTrim(src_lodlvl);
    misTrim(wh_id);
    misTrim(src_arecod);
    misTrim(dst_arecod);
    misTrim(fin_arecod);
    while (ii < rules->rel_size && found_it == FALSE)
    {
        if (misStrncmpChars(rules->rel[ii].wrktyp, "**********", WRKTYP_LEN) == 0 &&
            strcmp(src_lodlvl, rules->rel[ii].lodlvl) == 0 &&
            ((strcmp(src_arecod, rules->rel[ii].srcare) == 0 &&
              strcmp(dst_arecod, rules->rel[ii].dstare) == 0) ||
             (strcmp(dst_arecod, rules->rel[ii].srcare) == 0 &&
              strcmp(src_arecod, rules->rel[ii].dstare) == 0)) &&
            strcmp(fin_arecod, rules->rel[ii].finare) == 0 &&
            strcmp(wh_id, rules->rel[ii].wh_id) == 0)
            found_it = TRUE;
        else
            ii++;
    }
    /* If we didn't find it then lets default to same zone not needed */
    if (found_it == FALSE)
        return (FALSE);

    /* If this is a same zone first movement, then return true ... */
    if (strstr(rules->rel[ii].rtstr2, "SAME-ZONE-FIRST"))
        return (TRUE);


    return (FALSE);
}

static int sRequiresSameDstZone(char *wh_id,
                                char *src_arecod,
                                char *dst_arecod,
                                char *src_lodlvl,
                                char *fin_arecod)
{
    long            ii;
    short           found_it;
    DATA_STRUCT *data;
    RULES_STRUCT *rules;
    long ret_status;

    ret_status = pckrel_GetConfig(&data, &rules, 0, wh_id);
    if (ret_status != eOK)
        return(0);

    ii = rules->move_path_start;
    found_it = FALSE;
    misTrim(src_lodlvl);
    misTrim(wh_id);
    misTrim(src_arecod);
    misTrim(dst_arecod);
    misTrim(fin_arecod);
    while (ii < rules->rel_size && found_it == FALSE)
    {
        if (misStrncmpChars(rules->rel[ii].wrktyp, "**********", WRKTYP_LEN) == 0 &&
            strcmp(src_lodlvl, rules->rel[ii].lodlvl) == 0 &&
            ((strcmp(src_arecod, rules->rel[ii].srcare) == 0 &&
              strcmp(dst_arecod, rules->rel[ii].dstare) == 0) ||
             (strcmp(dst_arecod, rules->rel[ii].srcare) == 0 &&
              strcmp(src_arecod, rules->rel[ii].dstare) == 0)) &&
            strcmp(fin_arecod, rules->rel[ii].finare) == 0 &&
            strcmp(wh_id, rules->wh_id) == 0)
            found_it = TRUE;
        else
            ii++;
    }
    /* If we didn't find it then lets default to same zone not needed */
    if (found_it == FALSE)
        return (FALSE);

    /* If this is a same zone movement, then return true ... */
    if (strcmp(rules->rel[ii].wrktyp, rules->rel[ii + 1].wrktyp) == 0 &&
        strcmp(rules->rel[ii].lodlvl, rules->rel[ii + 1].lodlvl) == 0 &&
        strcmp(rules->rel[ii].wh_id, rules->rel[ii + 1].wh_id) == 0 &&
        strcmp(rules->rel[ii].dstare, rules->rel[ii + 1].srcare) == 0 &&
        strcmp(rules->rel[ii].finare, rules->rel[ii + 1].finare) == 0 &&
        misStrncmpChars(rules->rel[ii + 1].rtstr2,
            "SAME-ZONE-SECOND-HOP", RTSTR2_LEN) == 0)
        return (TRUE);


    return (FALSE);
}
/*
 * Get the "dest loc" of the pick.  Return either the pckwrk_dtl.dstloc 
 * or get the next loc in the sequence.  Wh_id is not needed
 * because alloc_queue is only of 1 warehouse
 */
static char *sGetDestLoc(ALLOC_QUEUE *qp)
{
    if (!qp)
        return "";

    if (strlen(qp->dstloc) != 0 && strlen(qp->wh_id) != 0)
    {
        return(qp->dstloc);
    }

    if (qp->next &&
        strlen(qp->next->pm_stoloc) != 0 &&
        misStrncmpChars(qp->cmbcod, qp->next->cmbcod, CMBCOD_LEN) == 0)
    {
        return(qp->next->pm_stoloc);
    }

    return "";
}

static long sGetLocationWrkzon(char *wh_id,
                               char *stoloc,
                               char *wrkzon_o)
{
    long ret_status;
    char buffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;

    sprintf(buffer,
            "select wrkzon "
            "  from locmst "
            " where stoloc = '%s' "
            "   and wh_id  = '%s' " ,
            stoloc,
            wh_id);

    ret_status = sqlExecStr(buffer, &res);
    if (ret_status != eOK)
    {
        sqlFreeResults(res);
        pckrel_WriteTrc("Error getting "
                        "zone, sts: %d"
                        " - SKIPPING",
                        ret_status);
        return(ret_status);
    }
    row = sqlGetRow(res);
    misStrncpyChars(wrkzon_o, sqlGetString(res, row, "wrkzon"), WRKZON_LEN);
    sqlFreeResults(res);

    return(eOK);
}
/*
 * sEvaluateBestFit
 *
 * This function is called to evaluate the best match for resource 
 * locations.  This will handle rules
 *
 */

static void sEvaluateBestFit(LOCATION_LIST **best_alloc_lp,
                             LOCATION_LIST *lp,
                             double con_space_needed,
                             moca_bool_t stgflg,
                             long small_lane_size)
{

	pckrel_WriteTrc("        MS-LBI: Evaluating %s maxqvl = %f curqvl = %f pndqvl = %f", 
		lp->stoloc, lp->maxqvl, lp->curqvl, lp->pndqvl);

	pckrel_WriteTrc("        MS-LBI: Needed: %f  Small Lane Size: %ld", con_space_needed, small_lane_size);

    if (stgflg &&
        (lp->maxqvl < small_lane_size))
    {
        /* 
         * For small staging lanes, we don't want to 
         * burn up any more than 2 lanes for a single 
         * rescod allocation.  Therefore, the maxqvl on 
         * a small staging lane needs to be >= 1/2 of the
         * space of what is required for the resource code.
         *
         * As confusing as this comparison seems - it is correct.
         * The point is that we don't want a "small lanes" to 
         * get chewed up by one shipment:  I.e. we want to use
         * no more than 2 small lanes for any given shipment...which
         * is why the "odd looking" comparison below.
         */
        if ((lp->maxqvl >= con_space_needed / 2.0))
        {
            if (*best_alloc_lp == NULL)
			{
				pckrel_WriteTrc("        MS-LBI: *** Setting loc to %s", lp->stoloc);			
                *best_alloc_lp = lp;
			}
        }
        else
        {
            pckrel_WriteTrc("   Lane %s is defined as small - maxqvl = %f,"
                            "needed space = %f...skipping",
                            lp->stoloc, lp->maxqvl, con_space_needed);
            return;
        }
    }
    else 
    {
        if (*best_alloc_lp == NULL)
		{
			pckrel_WriteTrc("        MS-LBI: *** Setting loc to %s", lp->stoloc);		
            *best_alloc_lp = lp;
		}
    }

    /*
     * Regardless of whether or not we're a staging lane,
     * we now want to choose the "best" by minimizing the 
     * amount of dead space...so pick the lane with the least
     * qvl which satisfies what is needed
     */

    /* MS-LBI: Commenting out the standard code below.*/
	/*
	if ((lp->maxqvl >= con_space_needed) &&
        (lp->maxqvl < (*best_alloc_lp)->maxqvl))
    {
        pckrel_WriteTrc("   Switching to loc %s from %s due to better fit",
                        lp->stoloc, (*best_alloc_lp)->stoloc);
        *best_alloc_lp = lp;
    }
	*/
	
	/* MS-LBI: Custom Logic ported from 2004 */
	pckrel_WriteTrc("        MS-LBI: checking if locs max >= needed and locs max < our current best choice");

	pckrel_WriteTrc("        MS-LBI: checking if %f >= %f and %f < %f", lp->maxqvl, 
		con_space_needed, lp->maxqvl, (*best_alloc_lp)->maxqvl);

	pckrel_WriteTrc("        MS-LBI: else check loc max < needed and loc max > current max");

	pckrel_WriteTrc("        MS-LBI: checking if %f < %f and %f > %f", lp->maxqvl, 
		con_space_needed, lp->maxqvl, (*best_alloc_lp)->maxqvl);

	pckrel_WriteTrc("        MS-LBI: BestSeqnum: %f vs inSeqnum: %f", (*best_alloc_lp)->seqnum, lp->seqnum );
	
	if( lp->maxqvl >= con_space_needed ) {

		/* EM: if current loc fits all then we only switch if we are smaller to be
		a better fit */

		if ((*best_alloc_lp)->maxqvl >= con_space_needed && 
			lp->maxqvl < (*best_alloc_lp)->maxqvl) {

				pckrel_WriteTrc("        MS-LBI: *** FIT ALL Switching to loc %s from %s due to better fit SMALLER",
					lp->stoloc, (*best_alloc_lp)->stoloc);

				*best_alloc_lp = lp;
		}
		/* EM: We can fit all and the previous loc cannot, lets do it */
		else if( (*best_alloc_lp)->maxqvl < con_space_needed ){

			pckrel_WriteTrc("        MS-LBI: *** FIT ALL Switching to loc %s from %s due to better fit BIGGER",
				lp->stoloc, (*best_alloc_lp)->stoloc);

			*best_alloc_lp = lp;
		}
		else if( lp->seqnum > 0 && lp->maxqvl == (*best_alloc_lp)->maxqvl ) {

			if( (*best_alloc_lp)->seqnum == 0 || 
				lp->seqnum < (*best_alloc_lp)->seqnum ) {

				pckrel_WriteTrc("        MS-LBI: *** FIT ALL Switching to loc %s from %s due SEQUENCE",
				    lp->stoloc, (*best_alloc_lp)->stoloc);

			    *best_alloc_lp = lp;
			}
		}
	}
	else
		/* EM: loc is less then what we need for the shipment so only switch locs if its a bigger loc 
		then what we have */
	{

		if( lp->maxqvl > (*best_alloc_lp)->maxqvl ) {
			pckrel_WriteTrc("        MS-LBI: *** NOT ALL FIT Switching to loc %s from %s due to better fit BIGGER",
				lp->stoloc, (*best_alloc_lp)->stoloc);

			*best_alloc_lp = lp;
		}
		else if( lp->seqnum > 0 && lp->maxqvl == (*best_alloc_lp)->maxqvl ) {

			if( (*best_alloc_lp)->seqnum == 0 || 
				lp->seqnum < (*best_alloc_lp)->seqnum ) {

				pckrel_WriteTrc("        MS-LBI: *** FIT ALL Switching to loc %s from %s due SEQUENCE",
				    lp->stoloc, (*best_alloc_lp)->stoloc);

			    *best_alloc_lp = lp;
			}
		}
	}

	pckrel_WriteTrc("        MS-LBI: Leaving Evaluate");	
	
    return;
}
/*
 * sGetNextLoc:  Get the next matching location in the list
 *               satisfying the criteria specified.
 *
 *   LocationList : position in the list to begin looking
 *   required_zone: if a matching work zone is required, this will
 *                  be non-null
 *   rescod       : resource code which is requested
 *   search_type  : type of search
 *                   1 = looking for pre-existing matching rescod
 *                   2 = looking for a preferred rescod match
 *                   3 = looking for a non-preferred (by anyone) match
 *                   4 = looking for any non-assigned location
 */
static LOCATION_LIST *sGetNextLoc(LOCATION_LIST *LocationList,
                                  char *wh_id,
                                  char *required_zone,
                                  double cmb_space_needed,
                                  char *rescod, 
                                  long search_type,
								  char *uc_csttyp)
{

    LOCATION_LIST *lp;

    if (LocationList == NULL)
        return(NULL);

    /* Search type = 1, existing location assigments */
    if (search_type == 1)
    {
        for (lp = LocationList; lp; lp = lp->next)
        {
            /* If we found a match */
            if (misStrncmpChars(rescod, lp->rescod, RESCOD_LEN) == 0 &&
                lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
                misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0)
            {
                /* if zone match is required, check that */
                if (required_zone && strlen(required_zone))
                {
                    if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
                    {
                        /* zone match was good...return success */
                        pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
                                        lp->stoloc, lp->maxqvl, lp->pndqvl);
                        return(lp);
                    }
                }
                else
                {
                    pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
                                    lp->stoloc, lp->maxqvl, lp->pndqvl);
                    return(lp);
                }
            }
            else
            {
                /*
                 * The selected location is not fit because of 
                 * either rescod is not matching or the location
                 * cannot accomodate the inventory.
                 */
                if (misStrncmpChars(rescod, lp->rescod, RESCOD_LEN) != 0)
                {
                    misTrc(T_FLOW, "Failure Cause: %s is not "
                           "matching %s rescod %s",
                           rescod, 
                           lp->stoloc, 
                           lp->rescod);
                }
                if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
                {
                    misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
                           "maxqvl %f curqvl %f and pndqvl %f",
                           cmb_space_needed,
                           lp->stoloc,
                           lp->maxqvl,
                           lp->curqvl,
                           lp->pndqvl);
                }
            }
        }
        return(NULL);
    }

    /* Search type = 2, preferred locations */
    if (search_type == 2)
    {
        for (lp = LocationList; lp; lp = lp->next)
        {
            if (misStrncmpChars(lp->pref_rescod, rescod, RESCOD_LEN) == 0 && 
                lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
                misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0)
            {
                if (strlen(lp->rescod) > 0 &&
                    strncmp(lp->rescod, "^^^^", 4) != 0 &&
                    misStrncmpChars(lp->rescod, rescod, RESCOD_LEN) != 0 && 
                    misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0) 
                {
                    pckrel_WriteTrc("    ...found preferred loc: %s, but"
                                    " currently assigned to rescod: %s"
                                    " - SKIPPED",
                                    lp->stoloc, lp->rescod);
                    continue;
                }

                /* if zone match is required, check that */
                if (required_zone && 
                    strlen(required_zone))
                {
                    if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
                    {
                        /* zone match was good...return success */
                        pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
                                        lp->stoloc, lp->maxqvl, lp->pndqvl);
                        return(lp);
                    }
                }
                else
                {
                    pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
                                    lp->stoloc, lp->maxqvl, lp->pndqvl);
                    return(lp);
                }
            }
            else
            {
                /*
                 * The selected location is not fit because of 
                 * either prefered rescod is not matching or the location
                 * cannot accomodate the inventory.
                 */
                if (misStrncmpChars(rescod, lp->pref_rescod, RESCOD_LEN) != 0)
                {
                    misTrc(T_FLOW, "Failure Cause: %s is not "
                           "matching %s rescod %s",
                           rescod, 
                           lp->stoloc, 
                           lp->pref_rescod);
                }
                if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
                {
                    misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
                           "maxqvl %f curqvl %f and pndqvl %f",
                           cmb_space_needed,
                           lp->stoloc,
                           lp->maxqvl,
                           lp->curqvl,
                           lp->pndqvl);
                }
            }
        }
        return(NULL);
    }

    /* 
     * search_type = 3, available locations with no preference
     * (for anything) 
     */
    if (search_type == 3)
    {
        for (lp = LocationList; lp; lp = lp->next)
        {
            if(strncmp(lp->uc_csttyp, uc_csttyp, CSTTYP_LEN) == 0 || (strlen(lp->uc_csttyp) == 0 && strlen(uc_csttyp) == 0))
			{
				pckrel_WriteTrc( "  MS-LBI: (3 - First Pass) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				pckrel_WriteTrc( "  MS-LBI: (3 - First Pass) checking for similar assignments: locmst.uc_csttyp: %s, pick.uc_csttyp: %s", 
								lp->uc_csttyp, 
								uc_csttyp);
				
				if ((strlen(lp->rescod) == 0 ||
					 strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0) && 
					 misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0)                                 
				{
					/* if zone match is required, check that */
					if (required_zone && 
						strlen(required_zone))
					{
						if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
											lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
										lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					/*
					 * The selected location is not fit because of 
					 * either rescod is not freed or
					 * cannot accomodate the inventory.
					 */
					if ((strlen(lp->rescod) != 0 &&
						 strncmp(lp->rescod, "^^^^", 4) != 0) ||
						strlen(lp->pref_rescod) != 0)
					{
						misTrc(T_FLOW, "Failure Cause: %s has "
							   "rescod %s and prefered rescod %s",
							   lp->stoloc, 
							   lp->rescod,
							   lp->pref_rescod);
					}
					if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
					{
						misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
							   "maxqvl %f curqvl %f and pndqvl %f",
							   cmb_space_needed,
							   lp->stoloc,
							   lp->maxqvl,
							   lp->curqvl,
							   lp->pndqvl);
					}
				}
			}
			else
			{
				pckrel_WriteTrc("    MS-LBI: (3 - First Pass) Skipping location due to csttyp mismatch. Location: %s,"
								"locmst.uc_csttyp: %s, pick's csttyp: %s",
								lp->stoloc,
								lp->uc_csttyp,
								uc_csttyp);
				continue;
			}
        }
		
		/* MS-LBI: A case could be made for running another pass where pick's csttyp is not null
		 * and location's csttyp is null. However not considering it to keep the logic from 2004 
		 * version.
		 */
		
        return(NULL);
    }

    if (search_type == 4)
    {
        /* MS-LBI: We will run a bunch of iterations here.
		 * First Pass - Try to assign a location where pick and location csttyp are same. lp->pref_rescod == 0
		 * Second Pass - Try to assign a location where pick and location csttyp are different. lp->pref_rescod == 0
		 * Third Pass - Try to assign a location where pick and location csttyp are same. lp->pref_rescod > 0
		 * Fourth Pass - Try to assign a location where pick and location csttyp are different. lp->pref_rescod > 0		
		 */
		 
		/* MS-LBI: First Pass*/ 
		for (lp = LocationList; lp; lp = lp->next)
        {
            if(strncmp(lp->uc_csttyp, uc_csttyp, CSTTYP_LEN) == 0 || (strlen(lp->uc_csttyp) == 0 && strlen(uc_csttyp) == 0))
			{
				pckrel_WriteTrc( "  MS-LBI: (4 - First Pass) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				
				pckrel_WriteTrc(" -- MS-LBI: test 1: %d ", 
					(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));

				pckrel_WriteTrc(" -- MS-LBI: test 2: %d ", 
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0));				
				
				if ((strlen(lp->rescod) == 0 ||
					 strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					strlen(lp->pref_rescod) == 0 && 
					misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0) 
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
											lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
										lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
				
					/*
					 * The selected location is not fit because of 
					 * either rescod is not freed or
					 * cannot accomodate the inventory.
					 */
					if ((strlen(lp->rescod) != 0 &&
						 strncmp(lp->rescod, "^^^^", 4) != 0) ||
						strlen(lp->pref_rescod) != 0)
					{
						misTrc(T_FLOW, "Failure Cause: %s has "
							   "rescod %s and prefered rescod %s",
							   lp->stoloc, 
							   lp->rescod,
							   lp->pref_rescod);
					}
					if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
					{
						misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
							   "maxqvl %f curqvl %f and pndqvl %f",
							   cmb_space_needed,
							   lp->stoloc,
							   lp->maxqvl,
							   lp->curqvl,
							   lp->pndqvl);
					}
				}
			}
			else
			{
				pckrel_WriteTrc("    MS-LBI: (4 - First Pass) Skipping location due to csttyp mismatch. Location: %s,"
				"locmst.uc_csttyp: %s, pick's csttyp: %s",
				lp->stoloc,
				lp->uc_csttyp,
				uc_csttyp);
				continue;
			}
		}

		/* MS-LBI: Second Pass*/ 
		for (lp = LocationList; lp; lp = lp->next)
        {
            if(strncmp(lp->uc_csttyp, uc_csttyp, CSTTYP_LEN) != 0 && (strlen(lp->uc_csttyp) > 0 || strlen(uc_csttyp) > 0))
			{
				pckrel_WriteTrc( "  MS-LBI: (4 - Second Pass) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				
				pckrel_WriteTrc(" -- MS-LBI: test 1: %d ", 
					(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));

				pckrel_WriteTrc(" -- MS-LBI: test 2: %d ", 
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0));				
				
				if ((strlen(lp->rescod) == 0 ||
					 strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					strlen(lp->pref_rescod) == 0 && 
					misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0) 
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
											lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
										lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
				
					/*
					 * The selected location is not fit because of 
					 * either rescod is not freed or
					 * cannot accomodate the inventory.
					 */
					if ((strlen(lp->rescod) != 0 &&
						 strncmp(lp->rescod, "^^^^", 4) != 0) ||
						strlen(lp->pref_rescod) != 0)
					{
						misTrc(T_FLOW, "Failure Cause: %s has "
							   "rescod %s and prefered rescod %s",
							   lp->stoloc, 
							   lp->rescod,
							   lp->pref_rescod);
					}
					if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
					{
						misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
							   "maxqvl %f curqvl %f and pndqvl %f",
							   cmb_space_needed,
							   lp->stoloc,
							   lp->maxqvl,
							   lp->curqvl,
							   lp->pndqvl);
					}
				}
			}
			else
			{
				pckrel_WriteTrc("    MS-LBI: (4 - Second Pass) Skipping location due to csttyp mismatch. Location: %s,"
				"locmst.uc_csttyp: %s, pick's csttyp: %s",
				lp->stoloc,
				lp->uc_csttyp,
				uc_csttyp);
				continue;
			}
		}		
		
		/* MS-LBI: Third Pass*/ 
		for (lp = LocationList; lp; lp = lp->next)
        {
            if(strncmp(lp->uc_csttyp, uc_csttyp, CSTTYP_LEN) == 0 || (strlen(lp->uc_csttyp) == 0 && strlen(uc_csttyp) == 0))
			{
				pckrel_WriteTrc( "  MS-LBI: (4 - Third Pass) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				
				pckrel_WriteTrc(" -- MS-LBI: test 1: %d ", 
					(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));

				pckrel_WriteTrc(" -- MS-LBI: test 2: %d ", 
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) > 0));				
				
				if ((strlen(lp->rescod) == 0 ||
					 strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					strlen(lp->pref_rescod) > 0 && 
					misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0) 
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
											lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
										lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
				
					/*
					 * The selected location is not fit because of 
					 * either rescod is not freed or
					 * cannot accomodate the inventory.
					 */
					if ((strlen(lp->rescod) != 0 &&
						 strncmp(lp->rescod, "^^^^", 4) != 0) ||
						strlen(lp->pref_rescod) != 0)
					{
						misTrc(T_FLOW, "Failure Cause: %s has "
							   "rescod %s and prefered rescod %s",
							   lp->stoloc, 
							   lp->rescod,
							   lp->pref_rescod);
					}
					if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
					{
						misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
							   "maxqvl %f curqvl %f and pndqvl %f",
							   cmb_space_needed,
							   lp->stoloc,
							   lp->maxqvl,
							   lp->curqvl,
							   lp->pndqvl);
					}
				}
			}
			else
			{
				pckrel_WriteTrc("    MS-LBI: (4 - Third Pass) Skipping location due to csttyp mismatch. Location: %s,"
				"locmst.uc_csttyp: %s, pick's csttyp: %s",
				lp->stoloc,
				lp->uc_csttyp,
				uc_csttyp);
				continue;
			}
		}

		/* MS-LBI: Fourth Pass*/ 
		for (lp = LocationList; lp; lp = lp->next)
        {
            if(strncmp(lp->uc_csttyp, uc_csttyp, CSTTYP_LEN) != 0 && (strlen(lp->uc_csttyp) > 0 || strlen(uc_csttyp) > 0))
			{
				pckrel_WriteTrc( "  MS-LBI: (4 - Fourth Pass) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				
				pckrel_WriteTrc(" -- MS-LBI: test 1: %d ", 
					(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));

				pckrel_WriteTrc(" -- MS-LBI: test 2: %d ", 
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0));				
				
				if ((strlen(lp->rescod) == 0 ||
					 strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					strlen(lp->pref_rescod) > 0 && 
					misStrncmpChars(wh_id, lp->wh_id, WH_ID_LEN) == 0) 
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (misStrncmpChars(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
											lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
										lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
				
					/*
					 * The selected location is not fit because of 
					 * either rescod is not freed or
					 * cannot accomodate the inventory.
					 */
					if ((strlen(lp->rescod) != 0 &&
						 strncmp(lp->rescod, "^^^^", 4) != 0) ||
						strlen(lp->pref_rescod) != 0)
					{
						misTrc(T_FLOW, "Failure Cause: %s has "
							   "rescod %s and prefered rescod %s",
							   lp->stoloc, 
							   lp->rescod,
							   lp->pref_rescod);
					}
					if (lp->curqvl + lp->pndqvl + cmb_space_needed > lp->maxqvl)
					{
						misTrc(T_FLOW, "Failure Cause: Needs %f capacity but %s has "
							   "maxqvl %f curqvl %f and pndqvl %f",
							   cmb_space_needed,
							   lp->stoloc,
							   lp->maxqvl,
							   lp->curqvl,
							   lp->pndqvl);
					}
				}
			}
			else
			{
				pckrel_WriteTrc("    MS-LBI: (4 - Fourth Pass) Skipping location due to csttyp mismatch. Location: %s,"
				"locmst.uc_csttyp: %s, pick's csttyp: %s",
				lp->stoloc,
				lp->uc_csttyp,
				uc_csttyp);
				continue;
			}
		}		
		        
		return(NULL);
    }
	
    return(NULL);
}
/*
 * sRecheckResourceAssignment
 *
 * This routine is used to re-validate that a resource assignment that
 * we are about to make does not conflict with any pckmovs already out
 * there.  This check is turned on via a policy (which sets the
 * chkmov_flag) and is only really necessary when process pick release
 * is being called in more than one place in the system
 */
static long sRecheckResourceAssignment(char *wh_id,
                                       char *stoloc,
                                       char *rescod,
                                       char *loc_rescod,
                                       char *chkmov_flag)
{
    char buffer[1000];
    long ret_status;

    if (misCiStrncmp(chkmov_flag, "Y", 1) != 0)
    {
        return(eOK);
    }

    if (strncmp(loc_rescod, "^^^^", 4) != 0)
    {
        /*
         * don't need to worry about it..we never thought it was 
         * empty to start with
         */
        return(eOK);
    }
    pckrel_WriteTrc("  ...checking location %s "
                    "to guard against rescod "
                    "mixing!",
                    stoloc);
    sprintf(buffer,
            "select cmbcod "
            "  from pckmov "
            " where prcqty = '0' "
            "	and rescod <> '%s' "
            "	and stoloc = '%s' "
            "   and wh_id = '%s'",
            rescod,
            stoloc,
            wh_id);
    ret_status = sqlExecStr(buffer, NULL);
    if (ret_status == eOK)
    {
        pckrel_WriteTrc("FOUND a combo code "
                        "with prcqty of 0 and a "
                        "different rescod! "
                        "Skipping ...");
        return(!eOK);
    }
    return(eOK);
}

/*
 * sFindResourceLocation
 *
 * Given a list of locations, this routine will attempt to find the
 * best matched location to use.  
 */

static LOCATION_LIST *sFindResourceLocation(LOCATION_LIST *LocationList,
                                            char *rescod,
                                            long small_lane_size,
                                            double con_space_needed,
                                            double cmb_space_needed,
                                            char *wh_id,
                                            char *required_zone,
                                            moca_bool_t sigflg,
                                            moca_bool_t stgflg,
                                            char *chkmov_flag,
											/* MS-LBI*/
											char *uc_csttyp,
											char *arecod)
{
    LOCATION_LIST *alloc_lp, *best_alloc_lp, *lp;

    alloc_lp = best_alloc_lp = NULL;
	
	/* MS-LBI: Declaring variables to run policy check. */
	char buffer[1000];
	long ret_status;
	RETURN_STRUCT *CmdRes;
	
     pckrel_WriteTrc("   MS-LBI: In sFindResourceLocation");
	/*
     * First let's check for a location which already has
     * a matching resource code
     */

    pckrel_WriteTrc("   ...looking for existing "
                    "location assignment for rescod: %s and warehouse %s",
                    rescod, wh_id);
    alloc_lp = sGetNextLoc(LocationList,
                           wh_id,
                           required_zone,
                           cmb_space_needed,
                           rescod, 
                           1,
						   uc_csttyp);

    if (alloc_lp)
        return(alloc_lp);
    /*
     * Next we look for any location which is "preferred"
     */
    pckrel_WriteTrc("   ...looking for preferred "
                    "location assignment for rescod: %s and warehouse %s",
                    rescod, wh_id);

    for (lp = sGetNextLoc(LocationList, wh_id,
                          required_zone, cmb_space_needed, rescod, 2, uc_csttyp);
         lp != NULL && alloc_lp == NULL;
         lp = sGetNextLoc(lp->next, wh_id, required_zone, 
                          cmb_space_needed, rescod, 2, uc_csttyp))
    {
        if (sRecheckResourceAssignment(lp->wh_id,
                                       lp->stoloc,
                                       rescod,
                                       lp->rescod,
                                       chkmov_flag) != eOK)
        {
            continue;
        }

        if (sigflg == BOOLEAN_TRUE ||
            (sigflg == BOOLEAN_FALSE &&
             lp->pndqvl == 0.0 &&
             lp->asgflg == BOOLEAN_FALSE))
        {
            sEvaluateBestFit(&best_alloc_lp,
                             lp,
                             con_space_needed,
                             stgflg,
                             small_lane_size);
        }
    }
    if (alloc_lp == NULL)
        alloc_lp = best_alloc_lp;

    if (alloc_lp)
        return(alloc_lp);

    /*
     * Now go with the non-preferred locations
     *
     */
	 
	/* MS-LBI: Adding a policy check to avoid search type 3 for configured areas */
	/* Not using search type 5 as it was same as 3*/
	
	sprintf(buffer,
		"[select 1 "
		"   from poldat_view "
		"  where polcod = 'USR' "
		"    and polvar = 'PICK-RELEASE' "
		"    and polval = 'BEST-FIT-STAGING-AREAS' "
		"    and wh_id = '%s' "
		"    and rtstr1 = '%s'] ",
		wh_id, arecod);

	CmdRes = NULL;

	ret_status = srvInitiateCommand(buffer, &CmdRes);
	
	if (ret_status == eOK )
	{
		pckrel_WriteTrc("   MS-LBI: ...looking for big "
			"locations for rescod %s, uc_csttyp = %s, space %f ",
			rescod, uc_csttyp, con_space_needed);

		/* 
		EM: 08/25/20008: Making this the main block.  Sending in cmb_space_needed, not con
		Space needed.  In theory if it were intensive this makes sense to loop twice, first
		trying to fit it all but we still loop all the locs and the best fit check is nothing
		so lets just bubble the best locs up and skip the search_type 3 loop below as we can get
		this puppy done here if we have available locs.  Keep the 4 loop incase we have to mix new 
		building / old building to get her done.
		*/

		for (lp = sGetNextLoc(LocationList, wh_id,
                          required_zone, cmb_space_needed, rescod, 3, uc_csttyp);
			lp && alloc_lp == NULL;
		lp = sGetNextLoc(lp->next, wh_id, required_zone, 
                          cmb_space_needed, rescod, 3, uc_csttyp))
		{
			pckrel_WriteTrc("MS-LBI: Loop 5 loc: %s . . .", lp->stoloc);

			if (sRecheckResourceAssignment(lp->wh_id,
                                       lp->stoloc,
                                       rescod,
                                       lp->rescod,
                                       chkmov_flag) != eOK)
			{

				pckrel_WriteTrc(" MS-LBI: Loop 5 Recheck of Resource Code failed");
				continue;
			}

			if (sigflg == BOOLEAN_TRUE ||
				(sigflg == BOOLEAN_FALSE &&
				lp->pndqvl == 0.0 &&  
				lp->asgflg == BOOLEAN_FALSE))
			{				
				/* EM: 08/22/2008: Not desperate yet so don't take a loc that's too big */

				pckrel_WriteTrc(" MS-LBI: Checking if loc is too big: maxqvl: %f, too_big: %f", 
					lp->maxqvl, con_space_needed * 1.2);
            /*
				if ((lp->maxqvl) > (con_space_needed * 1.2) ) {
					pckrel_WriteTrc(" MS-LBI: Loop 5 This location is too big, maxqvl: %f", lp->maxqvl);

					continue;
				} 

				pckrel_WriteTrc("  MS-LBI: Loop 5 Best Fit Check l.maxqvl = %f, b.maxqvl: %f,  needed: %f", 
					lp->maxqvl, best_alloc_lp->maxqvl, con_space_needed);
                 */
				sEvaluateBestFit(&best_alloc_lp,
					lp,
					con_space_needed,
					stgflg,
					small_lane_size);
			}
		}

		if (alloc_lp == NULL)
			alloc_lp = best_alloc_lp;

		if (alloc_lp) {
			pckrel_WriteTrc("   MS-LBI: Loop 5 allocated loc: %s", alloc_lp->stoloc);

			sAddToSpaceUsed(
				arecod,
				rescod,
				alloc_lp->maxqvl,
				wh_id);

			return(alloc_lp);
		}
	}
	/*
	else
	{
		pckrel_WriteTrc("   ...looking for "
						"locations for rescod %s and warehouse %s ",
						rescod, wh_id);
		for (lp = sGetNextLoc(LocationList, wh_id, required_zone, 
							  cmb_space_needed, rescod, 3, uc_csttyp);
			 lp && alloc_lp == NULL;
			 lp = sGetNextLoc(lp->next, wh_id, required_zone, 
							  cmb_space_needed, rescod, 3, uc_csttyp))
		{
			if (sRecheckResourceAssignment(lp->wh_id,
										   lp->stoloc,
										   rescod,
										   lp->rescod,
										   chkmov_flag) != eOK)
			{
				continue;
			}
			if (sigflg == BOOLEAN_TRUE ||
				(sigflg == BOOLEAN_FALSE &&
				 lp->pndqvl == 0.0 &&  
				 lp->asgflg == BOOLEAN_FALSE))
			{
				sEvaluateBestFit(&best_alloc_lp,
								 lp,
								 con_space_needed,
								 stgflg,
								 small_lane_size);
			}
		}

		if (alloc_lp == NULL)
			alloc_lp = best_alloc_lp;

		if (alloc_lp)
			return(alloc_lp);
	}
	*/
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
    /*
     * Now go with the non-preferred locations
     *
     */
    pckrel_WriteTrc("   ...looking for any available  "
                    "locations for rescod %s and warehouse %s",
                    rescod, wh_id);

    for (lp = sGetNextLoc(LocationList, wh_id, required_zone, 
                          cmb_space_needed, rescod, 4, uc_csttyp);
         lp && alloc_lp == NULL;
         lp = sGetNextLoc(lp->next, wh_id, required_zone, 
                          cmb_space_needed, rescod, 4, uc_csttyp))
    {
        if (sRecheckResourceAssignment(lp->wh_id,
                                       lp->stoloc,
                                       rescod,
                                       lp->rescod,
                                       chkmov_flag) != eOK)
        {
            continue;
        }
        if (sigflg == BOOLEAN_TRUE ||
            (sigflg == BOOLEAN_FALSE &&
             lp->pndqvl == 0.0 &&  
             lp->asgflg == BOOLEAN_FALSE))
        {
            sEvaluateBestFit(&best_alloc_lp,
                             lp,
                             con_space_needed,
                             stgflg,
                             small_lane_size);
        }
    }

    if (alloc_lp == NULL)
        alloc_lp = best_alloc_lp;

     return(alloc_lp);
}

static void sFlagPicksForSkip(ALLOC_QUEUE *Queue,
                              char *rescod,
                              char *wh_id,
                              char *arecod)
{
    ALLOC_QUEUE *qp, *qp_tmp;

    /* Save the first cmbcod that we get. */
    qp_tmp = Queue;
    
    for (qp = Queue; qp; qp = qp->next)
    {
        if (misCiStrncmpChars(qp_tmp->cmbcod, qp->cmbcod, CMBCOD_LEN))
            qp_tmp = qp;

        if (qp->processed)
            continue;

        if (misStrncmpChars(qp->pm_arecod, arecod, ARECOD_LEN) == 0 &&
            misStrncmpChars(qp->pm_rescod, rescod, RESCOD_LEN) == 0 &&
            misStrncmpChars(qp->wh_id,  wh_id,  WH_ID_LEN) == 0)
        {
            /* 
             * Need to set all of the hops for a cmbcod
             * to processed in order to skip the pick.
             */
            for(qp = qp_tmp; 
                qp && !misCiStrncmpChars(qp_tmp->cmbcod, qp->cmbcod, CMBCOD_LEN);
                qp = qp->next)
            {
                /* 
                 * This is used to hold on to the previous
                 * pointer value so when we pop out of this loop
                 * we can get back to where we need to be pointing
                 * so that the outer loop can increment this and not
                 * miss any of the pointer elements.
                 */
                qp_tmp = qp;
                
                qp->processed = 1;

                misTrc(T_FLOW, "setting to processed: \n"
                               "   cmbcod '%s' \n"
                               "   seqnum %ld \n"
                               "   wh_id  '%s' \n"
                               "   arecod '%s' \n"
                               "   rescod '%s' ",
                               qp_tmp->cmbcod,
                               qp_tmp->seqnum,
                               qp_tmp->wh_id,
                               qp_tmp->pm_arecod,
                               qp_tmp->pm_rescod);

            }

            /* 
             * Need to back up one here because the outer FOR loop
             * will set it to qp->next and we don't want to miss any.
             */
            qp = qp_tmp;
        }
    }
    return;
}

/*
 * sAllocateLocationsForCmbcod
 *
 * This routine is called for each cmbcod on the allocation queue
 * It will determine if a resource location is needed for a cmbcod
 * and any zone matching/space requirements for that location.
 * Finally, it will determine the best resource location to allocate
 * and allocate it.
 */
static long sAllocateLocationsForCmbcod(ALLOC_QUEUE *qp,
                                        AREA_LIST **AreaListHead,
                                        RULES_STRUCT *rules,
                                        DATA_STRUCT *data,
                                        long looking_specific)
{
    ALLOC_QUEUE *mov_qp, *nxt_mov_qp, *prv_mov_qp, *tmp_qp;
    LOCATION_LIST *LocationList, *alloc_lp;

    char jump_srcare[ARECOD_LEN+1];
    char jump_srcloc[STOLOC_LEN+1];
    char wh_id[WH_ID_LEN + 1];
    char jump_dstare[ARECOD_LEN+1];
    char jump_dstloc[STOLOC_LEN+1];
    char jump_prvare[ARECOD_LEN+1];
    char jump_prvloc[STOLOC_LEN+1];
    char next_arecod[ARECOD_LEN+1];
    char from_area[ARECOD_LEN+1];
    char to_area[ARECOD_LEN+1];
    char to_loccod[LOCCOD_LEN+1];
    char first_cmbpck_wrkref[WRKREF_LEN+1];
    char first_cmbpck_dstare[ARECOD_LEN+1];

    char src_wrkzon[WRKZON_LEN+1];
    char dst_wrkzon[WRKZON_LEN+1];
    char required_zone[WRKZON_LEN+1];
    long ret_status;
    long alloc_something;
    char work_rescod[RESCOD_LEN+1];
    char work_arecod[ARECOD_LEN+1];
    char tmp_stoloc[STOLOC_LEN + 1];
    double con_space_needed, cmb_space_needed;
    long cmb_tot_pckqty;
    char buffer[2000];

    pckrel_WriteTrc("        MS-LBI: In sAllocateLocationsForCmbcod!" );

	pckrel_WriteTrc("  ...processing combination code %s (ms=%ld) ", 
                    qp->cmbcod, pckrel_ElapsedMs());

    /* zones that we have */
    memset(jump_srcare, 0, sizeof(jump_srcare));
    memset(jump_srcloc, 0, sizeof(jump_srcloc));
    memset(jump_dstare, 0, sizeof(jump_dstare));
    memset(jump_dstloc, 0, sizeof(jump_dstloc));
    memset(jump_prvare, 0, sizeof(jump_prvare));
    memset(jump_prvloc, 0, sizeof(jump_prvloc));
    memset(wh_id,  0, sizeof(wh_id));
    memset(next_arecod, 0, sizeof(next_arecod));
    alloc_something = FALSE;

    /* Next, lets loop through and allocate the series */
    /* of jumps that can be made */

    mov_qp = qp;
    prv_mov_qp = NULL;
    while ((mov_qp != NULL) &&
           misStrncmpChars(mov_qp->cmbcod, qp->cmbcod, CMBCOD_LEN) == 0)
    {
        /* We loop through for a given instance of a cmbcod */
        
        misTrc(T_FLOW, "We loop here because the mov_qp is not null (%ld) "
                       "and the cmbcod hasn't changed. ",
                       mov_qp);

        if (mov_qp != qp)
        {
            /* We're not on the first row so let's
             * check to see if the wrkref here matches the
             * the first_wrkref. If not, we'll flag the rest
             * of this cmbcod as fully processed.
             */
             if ((misStrncmpChars(mov_qp->wrkref, first_cmbpck_wrkref, WRKREF_LEN) != 0) &&
                 (misStrncmpChars(mov_qp->pm_arecod, first_cmbpck_dstare, ARECOD_LEN) == 0) &&
                 (!mov_qp->processed))
             {
                 pckrel_WriteTrc("     - We've allocated the destination loc "
                                 " for this pckmov for this cmbcod "
                                 " so flag the rest of the picks as processed "
                                 " so we don't repeat this step with them.");
                 tmp_qp = NULL;
                 for (tmp_qp = qp; tmp_qp; tmp_qp = tmp_qp->next)
                 {
                     if ((misCiStrncmpChars(tmp_qp->cmbcod, mov_qp->cmbcod, CMBCOD_LEN) == 0) &&
                         (misCiStrncmpChars(tmp_qp->pm_arecod, first_cmbpck_dstare, ARECOD_LEN) == 0))
                     {
                         /* Check if the shpsts of shipment is staged
                          * or stoloc entry in pckmov is not empty then
                          * processed flag is set to 1 for WMD-94352.
                          */
                         if ((misTrimLen(tmp_qp ->pm_stoloc, STOLOC_LEN) != 0)||
                             (misCiStrncmpChars(tmp_qp->shpsts, SHPSTS_STAGED, SHPSTS_LEN) != 0))
                         {
                             misTrc(T_FLOW, "Updating cmbcod %s, wrkref %s, pm_arecod %s as processed.",
                                         tmp_qp->cmbcod, tmp_qp->wrkref, tmp_qp->pm_arecod);
                             tmp_qp->processed = 1;
                         }
                     }
                 }
             }
        }

        tmp_qp = NULL;


        misTrc(T_FLOW, "For cmbcod %s the processed flag = %ld", 
                       mov_qp->cmbcod,
                       mov_qp->processed);
        
        if (mov_qp->processed)
        {
            /* The processed will be set if 
             * we have already considered this row.
             * skip processing if that is the case.
             */
            mov_qp = mov_qp->next;
            memset(first_cmbpck_wrkref, 0, sizeof(first_cmbpck_wrkref));
            memset(first_cmbpck_dstare, 0, sizeof(first_cmbpck_dstare));
            continue;
        }


        memset(src_wrkzon, 0, sizeof(src_wrkzon));
        memset(dst_wrkzon, 0, sizeof(dst_wrkzon));
        
        misTrc(T_FLOW, "Processing cmbcod %s, wrkref %s, pm_arecod %s.",
                       mov_qp->cmbcod, mov_qp->wrkref, mov_qp->pm_arecod);
        if (mov_qp == qp)
        {
            /* if we are on our first row */
            misStrncpyChars(wh_id,  qp->wh_id, WH_ID_LEN);
            misStrncpyChars(jump_srcare, qp->srcare, ARECOD_LEN);
            misStrncpyChars(jump_srcloc, qp->srcloc, STOLOC_LEN);
            misStrncpyChars(jump_dstare, qp->pm_arecod, ARECOD_LEN);
            misStrncpyChars(jump_dstloc, qp->pm_stoloc, STOLOC_LEN);
            misStrncpyChars(first_cmbpck_wrkref, qp->wrkref, WRKREF_LEN);
            misStrncpyChars(first_cmbpck_dstare, qp->pm_arecod, ARECOD_LEN);
            misTrc(T_FLOW, "first_cmbpck_wrkref %s, first_cmbpck_dstare %s",
                    first_cmbpck_wrkref, first_cmbpck_dstare);

            memset(next_arecod, 0, sizeof(next_arecod));
            for (tmp_qp = mov_qp->next; tmp_qp; tmp_qp = tmp_qp->next)
            {
                if (tmp_qp->seqnum > qp->seqnum &&
                    misStrncmpChars(tmp_qp->cmbcod, mov_qp->cmbcod, CMBCOD_LEN) == 0)
                {
                    nxt_mov_qp = tmp_qp;
                    misStrncpyChars(next_arecod, tmp_qp->pm_arecod, ARECOD_LEN);
                    break;
                }
            }

            misTrc(T_FLOW, "For first row:");
            misTrc(T_FLOW, "  wh_id = '%s'  ", wh_id);
            misTrc(T_FLOW, "  jump_srcare = '%s' ", jump_srcare);
            misTrc(T_FLOW, "  jump_srcloc = '%s' ", jump_srcloc);
            misTrc(T_FLOW, "  jump_dstare = '%s' ", jump_dstare);
            misTrc(T_FLOW, "  jump_dstloc = '%s' ", jump_dstloc);
            if(misTrimLen(next_arecod, 1))
                misTrc(T_FLOW, "  next_arecod = '%s' ", next_arecod);
            else
                misTrc(T_FLOW, "  next_arecod = NULL ");
        }
        else
        {
            /* We are not on the first row */
            misStrncpyChars(wh_id , prv_mov_qp->wh_id, WH_ID_LEN);
            misStrncpyChars(jump_srcare, prv_mov_qp->pm_arecod, ARECOD_LEN);
            misStrncpyChars(jump_srcloc, prv_mov_qp->pm_stoloc,	STOLOC_LEN);
            misStrncpyChars(jump_dstare, mov_qp->pm_arecod, ARECOD_LEN);
            misStrncpyChars(jump_dstloc, mov_qp->pm_stoloc, STOLOC_LEN);
            misStrncpyChars(first_cmbpck_wrkref, mov_qp->wrkref, WRKREF_LEN);
            misStrncpyChars(first_cmbpck_dstare, mov_qp->pm_arecod, ARECOD_LEN);
            misTrc(T_FLOW, "not 1st row - first_cmbpck_wrkref %s, first_cmbpck_dstare %s",
                    first_cmbpck_wrkref, first_cmbpck_dstare);

            memset(next_arecod, 0, sizeof(next_arecod));
            for (tmp_qp = mov_qp->next; tmp_qp; tmp_qp = tmp_qp->next)
            {
                if (tmp_qp->seqnum > mov_qp->seqnum &&
                    misStrncmpChars(tmp_qp->cmbcod, qp->cmbcod, CMBCOD_LEN) == 0)
                {
                    nxt_mov_qp = tmp_qp;
                    misStrncpyChars(next_arecod, tmp_qp->pm_arecod, ARECOD_LEN);
                    break;
                }
            }

            misTrc(T_FLOW, "For the next row:");
            misTrc(T_FLOW, "  wh_id  = '%s' ", wh_id);
            misTrc(T_FLOW, "  jump_srcare = '%s' ", jump_srcare);
            misTrc(T_FLOW, "  jump_srcloc = '%s' ", jump_srcloc);
            misTrc(T_FLOW, "  jump_dstare = '%s' ", jump_dstare);
            misTrc(T_FLOW, "  jump_dstloc = '%s' ", jump_dstloc);
            if(misTrimLen(next_arecod, 1))
                misTrc(T_FLOW, "  next_arecod = '%s' ", next_arecod);
            else
                misTrc(T_FLOW, "  next_arecod = NULL ");
        }

        /* Removed the check for jump_dstloc to be = 0 since
         * we want to still go ahead and check if we can use
         * the location in case this is already reserved for
         * something.
         */
        if (misTrimLen(wh_id, WH_ID_LEN) > 0 &&
            misTrimLen(jump_dstare, ARECOD_LEN) > 0 &&
            misTrimLen(jump_dstloc, STOLOC_LEN) == 0)
        {

            memset(from_area, 0, sizeof(from_area));
            memset(to_area, 0, sizeof(to_area));
            memset(to_loccod, 0, sizeof(to_loccod));

            misTrc(T_FLOW, "We have a dstare (%s), but no dstloc ",
                        jump_dstare);

            /* If the next two steps are for the same 
             * area then we must be moving out and back 
             * in through staging or something similar 
             * ... if so check to see if this is really 
             * a move within the same aisle which means 
             * we can just change the pick work to be a 
             * simple move ... this code is assuming 
             * that if we are moving between two 
             * locations in the same area then the work 
             * zone rules are automatically enforced 
             */

            if (misStrncmpChars(jump_dstare,next_arecod, ARECOD_LEN) == 0 &&
                misStrncmpChars(qp->wh_id, wh_id, WH_ID_LEN) == 0)
            {
                misTrc(T_FLOW, "Jump destination area is the same as the next"
                               "warehouse ('%s') area ('%s') ",
                               wh_id,
                               jump_dstare);

                misTrc(T_FLOW, "about to get work zone for src location '%s' ",
                        jump_srcloc);

                ret_status = sGetLocationWrkzon(wh_id,
                                                jump_srcloc,
                                                src_wrkzon);


                if (ret_status != eOK)
                {
                    pckrel_WriteTrc("Error selecting srcloc "
                                    "wrkzon - status = %d - "
                                    "SKIPPING",
                                    ret_status);
                    prv_mov_qp = mov_qp;
                    mov_qp = mov_qp->next;
                    continue;
                }

                misTrc(T_FLOW, "source work zone is '%s' ",
                        src_wrkzon);

                /* Since we really expanded our 
                 * intermediate locations we really 
                 * need to see if a next area exists 
                 * after the arecod in variable 
                 * next_arecod. If we are at the end 
                 * of the list or change cmbcods then 
                 * use dstloc 
                 */

                misTrc(T_FLOW, "about to get work zone for dst location.");

                misStrncpyChars(tmp_stoloc,  sGetDestLoc(qp), STOLOC_LEN);
                ret_status = sGetLocationWrkzon(wh_id,
                                                tmp_stoloc,
                                                dst_wrkzon);                

                if (ret_status != eOK)
                {
                    pckrel_WriteTrc("Error selecting dstloc"
                                    "wrkzon, stat: %d, SKIPPING",
                                    ret_status);
                    prv_mov_qp = mov_qp;
                    mov_qp = mov_qp->next;
                    continue;
                }

                misTrc(T_FLOW, "destination work zone is '%s' ",
                        dst_wrkzon);

                if (strcmp(src_wrkzon, dst_wrkzon) != 0)
                {
                 
                    misStrncpyChars(from_area, jump_srcare, ARECOD_LEN);
                    misStrncpyChars(to_area, jump_dstare, ARECOD_LEN);
                }
                else
                {
                    sprintf(buffer,
                            "delete from pckmov "
                            " where cmbcod = '%s' "
                            "   and (seqnum='%ld' or seqnum='%ld') ",
                            mov_qp->cmbcod,
                            mov_qp->seqnum,
                            nxt_mov_qp->seqnum);
                    ret_status = sqlExecStr(buffer, NULL);
                    if (ret_status != eOK)
                    {
                        pckrel_WriteTrc("Error deleting "
                                        "pckmov by cmbcod");
                        return (ret_status);
                    }
                    mov_qp->processed = 1;
                    nxt_mov_qp->processed = 1;
                    
                    alloc_something = TRUE;
                }
            }
            else
            {

                misStrncpyChars(from_area, jump_srcare, ARECOD_LEN);
                misStrncpyChars(to_area, jump_dstare, ARECOD_LEN);
                
                misTrc(T_FLOW, "setting wh_id = '%s' from_area = '%s' \n"
                               "    and wh_id = '%s'   to_area = '%s'",
                               wh_id,
                               from_area,
                               wh_id,
                               to_area);

            }
            
            /* If we have a needed area code, */
            /* then try to allocate */

            if (strlen(to_area))
            {
                memset(work_rescod, '^', RESCOD_LEN);
                work_rescod[RESCOD_LEN] = 0;

                misTrc(T_FLOW, "About to get location list for area %s ",
                               to_area);

                LocationList = NULL;
                ret_status = sGetLocationListForArea(wh_id,
                                                     to_area,
                                                     rules->ord_by,
                                                     qp->ship_id,
                                                     AreaListHead,
                                                     &LocationList);
                if (ret_status != eOK)
                {
                    pckrel_WriteTrc("Error loading locations - %d",
                                    ret_status);
                    pckrel_WriteTrc("*Order-By "
                                    "policy = %s, Area = %s",
                                    rules->ord_by, to_area);
                    return (ret_status);
                }

                misTrc(T_FLOW, "Check Pointers: \n"
                               "   Location List = '%ld' \n"
                               "       Area List = '%ld' \n",
                               LocationList,
                               AreaListHead);

                memset(required_zone, 0, sizeof(required_zone));
                if (sRequiresSameSrcZone(wh_id,
                                         from_area,
                                         to_area,
                                         qp->lodlvl,
                                         qp->dstare))
                {
                    if (strcmp(src_wrkzon, "") == 0)
                    {
                        ret_status = sGetLocationWrkzon(wh_id,
                                                        jump_srcloc,
                                                        src_wrkzon);
                        if (ret_status != eOK)
                        {
                            misTrc(0, "SAME-ZONE logic is being used but "
                                "we failed to get a workzone.");
                            continue;
                        }
                      
                    }
                    misStrncpyChars(required_zone, src_wrkzon, WRKZON_LEN);
                   
                }
                else if (sRequiresSameDstZone(wh_id,
                                              from_area,
                                              to_area,
                                              qp->lodlvl,
                                              qp->dstare))
                                              
                {
                    misStrncpyChars(tmp_stoloc, 
                            sGetDestLoc(qp), STOLOC_LEN);
                    ret_status = sGetLocationWrkzon(wh_id,
                                                    tmp_stoloc,
                                                    dst_wrkzon);
                    if (ret_status != eOK)
                    {
                        misTrc(0, "SAME-ZONE-SECOND logic is being used but "
                            "we failed to get a destination workzone.");
                    }
                    else
                    {
                        misStrncpyChars(required_zone, dst_wrkzon, WRKZON_LEN);
                    }
                }

                /* Loop through the entire result set 
                 * and find what we need for the whole
                 * resource code and what we need for 
                 * the combination code 
                 *
                 * Since we are calculating for the 
                 * entire rescod we should compare to 
                 * the row we are on (mov_row) and not 
                 * the top pointer (row) (I THINK). 
                 * We are in the same cmbcod but there 
                 * may be a different rescod associated 
                 * to every arecod I think another 
                 * assumption made here was that 
                 * anything but a pallet pick would 
                 * have only one wrkref per cmbcod. 
                 * We will change that to accumulate 
                 * the qty for pckqty and cmbcod qvl 
                 * so we can put out a qvlwrk if we 
                 * have to. Make sure the cmb_space 
                 * and cmb_tot stay on the same level 
                 * or seqnums are equal 
                 */

                /* WMD-36893:
                 * We should skip the following process for those pick moves 
                 * with same cmbcod.
                 */
                if ( prv_mov_qp == NULL 
                  || strncmp(mov_qp->pm_rowid, prv_mov_qp->pm_rowid, 100) != 0 )
                {
                    cmb_tot_pckqty = 0;
                    con_space_needed = cmb_space_needed = 0.0;
                    memset(work_rescod, 0, sizeof(work_rescod));
                    misTrimcpy(work_rescod, mov_qp->pm_rescod, RESCOD_LEN);

                    memset(work_arecod, 0, sizeof(work_arecod));
                    misTrimcpy(work_arecod, mov_qp->pm_arecod, ARECOD_LEN);


                    sGetSpaceReqs(mov_qp,
                                  &cmb_space_needed,
                                  &con_space_needed,
                                  &cmb_tot_pckqty);
								  
					pckrel_WriteTrc("        MS-LBI: con_space_needed: %f, cmb_space_needed: %f", con_space_needed, cmb_space_needed);

                    misTrc(T_FLOW, "About to get a location list pointer using "
                                   "sFindResourceLocation");

                    alloc_lp = sFindResourceLocation(LocationList,
                                                     mov_qp->pm_rescod,
                                                     rules->small_lane,
                                                     con_space_needed,
                                                     cmb_space_needed,
                                                     wh_id,
                                                     required_zone,
                                                     mov_qp->sigflg,
                                                     mov_qp->stgflg,
                                                     rules->chkmov_flag,
													 mov_qp->uc_csttyp,
													 mov_qp->pm_arecod);
                    if (alloc_lp != NULL)
                    {
                        pckrel_WriteTrc("  Using location - %s", 
                                        alloc_lp->stoloc);
                        pckrel_WriteTrc("  ...setting cmbcod %s, "
                                        "to stoloc %s (ms=%d)",
                                        mov_qp->cmbcod,
                                        alloc_lp->stoloc,
                                        pckrel_ElapsedMs());
                        sprintf(buffer,
                                "update pckmov "
                                "   set stoloc = '%s' "
                                " where rowid = '%s' ",
                                alloc_lp->stoloc,
                                mov_qp->pm_rowid);
                        ret_status = sqlExecStr(buffer, NULL);
                        if (ret_status != eOK)
                        {
                            pckrel_WriteTrc("Error updating "
                                            "pckmov by cmb");
                            return (ret_status);
                        }
                        else
                        {
                            misTrimcpy(mov_qp->pm_stoloc, alloc_lp->stoloc, STOLOC_LEN);
                        }
                        /* Do not insert QVLWRK for KITPART since we are already
                         * inserting the records into qvlwrk for the actual
                         * parts.
                         * If jump_dstloc is not empty then do not insert QVLWRK
                         * since it may lead to the creation of duplicate record
                         * for top off replenishment.
                         */
                        if (mov_qp->sigflg == BOOLEAN_TRUE &&
                            misStrncmpChars(mov_qp->prtnum, PRTNUM_KIT, PRTNUM_LEN) != 0 &&
                            misTrimLen(jump_dstloc, STOLOC_LEN) == 0)
                        {
                            if (misTrimLen(mov_qp->asset_typ, ASSET_TYP_LEN) > 0)
                            {
                                sprintf(buffer,
                                        "insert into qvlwrk "
                                        " (stoloc,prtnum,untqty,"
                                        "pndqvl, prt_client_id, wh_id, asset_typ) "
                                        " values"
                                        " ('%s','%s','%ld','%f','%s','%s','%s') ",
                                        alloc_lp->stoloc,
                                        mov_qp->prtnum,
                                        cmb_tot_pckqty,
                                        cmb_space_needed,
                                        mov_qp->prt_client_id,
                                        alloc_lp->wh_id,
                                        mov_qp->asset_typ);
                            }
                            else
                            {
                                sprintf(buffer,
                                        "insert into qvlwrk "
                                        " (stoloc,prtnum,untqty,"
                                        "pndqvl, prt_client_id, wh_id) "
                                        " values"
                                        " ('%s','%s','%ld','%f','%s','%s') ",
                                        alloc_lp->stoloc,
                                        mov_qp->prtnum,
                                        cmb_tot_pckqty,
                                        cmb_space_needed,
                                        mov_qp->prt_client_id,
                                        alloc_lp->wh_id);
                            }
                            ret_status = sqlExecStr(buffer,
                                                    NULL);
                            if (ret_status != eOK)
                            {
                                pckrel_WriteTrc("Error inserting "
                                                "qvlwrk");
                                return (ret_status);
                            }
                        }
                        sprintf(buffer,
                                "assign resource code "
                                " where delta_pndqvl =  %f "
                                "   and rescod = '%s' "
                                "   and stoloc = '%s' "
                                "   and wh_id  = '%s' ",
                                cmb_space_needed,
                                mov_qp->pm_rescod,
                                alloc_lp->stoloc,
                                alloc_lp->wh_id);

                        /* Now, update the result set */
                        /* values since we may re-use */
                        /* the locations */

                        strcpy(alloc_lp->rescod, mov_qp->pm_rescod);
                        alloc_lp->pndqvl += cmb_space_needed;
                        ret_status = srvInitiateCommand(buffer, NULL);
                        if (ret_status != eOK)
                        {
                            pckrel_WriteTrc("Error updating locmst rescod..");
                            return(ret_status);
                        }
                        misStrncpyChars(jump_prvloc, alloc_lp->stoloc, STOLOC_LEN);
                        alloc_something = TRUE;
                    }
                    else if (looking_specific == TRUE)
                    {
                        pckrel_WriteTrc("Unable to find "
                                        "a location on specific read,"
                                        "returning error ...");
                        return (eINT_PICK_REL_NO_LOC_AVAIL);
                    }
                    else
                    {
                        misTrc(T_FLOW, "Location list pointer was NULL using "
                                   "sFindResourceLocation");

                        /* Fast forward past this */
                        /* combination code */
                        while ((mov_qp != NULL) &&
                               misStrncmpChars(mov_qp->cmbcod, 
                                       qp->cmbcod, CMBCOD_LEN) == 0)
                        {
                            mov_qp = mov_qp->next;
                        }
                        alloc_something = FALSE;

                        misStrncpyChars(jump_prvloc, jump_dstloc, STOLOC_LEN);

                        pckrel_WriteTrc("   Could not allocate in "
                                        "   Warehouse : %s "
                                        "%s for shipment %s, "
                                        "wrkref %s, rescod %s",
                                        wh_id,
                                        to_area,
                                        qp->ship_id,
                                        qp->wrkref,
                                        qp->pm_rescod);
                        pckrel_WriteTrc("     - flagging remaining matching picks"
                                        " for skip");
                        sFlagPicksForSkip(qp->next,
                                          qp->pm_rescod,
                                          wh_id,
                                          to_area);

                        }
                }
            }

        }
        else
        {
            misStrncpyChars(jump_prvloc, jump_dstloc, STOLOC_LEN);
            if(misTrimLen(wh_id, WH_ID_LEN) > 0 &&
               misTrimLen(jump_dstare, ARECOD_LEN) > 0 && 
               misTrimLen(jump_dstloc, STOLOC_LEN) > 0 &&
               misTrimLen(mov_qp->ship_id,SHIP_ID_LEN) > 0)
            {
               misTrc(T_FLOW, "We have dstare (%s) and dstloc (%s)",
                        jump_dstare,jump_dstloc);

               if ( prv_mov_qp == NULL 
                    || strncmp(mov_qp->pm_rowid, prv_mov_qp->pm_rowid, 100) != 0 )
                {
                    cmb_tot_pckqty = 0;
                    con_space_needed = cmb_space_needed = 0.0;


                    sGetSpaceReqs(mov_qp,
                                  &cmb_space_needed,
                                  &con_space_needed,
                                  &cmb_tot_pckqty);

                    if (mov_qp->sigflg == BOOLEAN_TRUE &&
                        misStrncmpChars(mov_qp->prtnum, PRTNUM_KIT, PRTNUM_LEN) != 0)
                    {
                        sprintf(buffer,
                                "select 1 from qvlwrk "
                                " where stoloc = '%s'         and"
                                "       prtnum = '%s'         and"
                                "       untqty = '%ld'        and"
                                "       pndqvl = '%f'         and"
                                "       prt_client_id = '%s'  and"
                                "       wh_id = '%s'             ",
                                jump_dstloc,
                                mov_qp->prtnum,
                                cmb_tot_pckqty,
                                cmb_space_needed,
                                mov_qp->prt_client_id,
                                wh_id);
                        ret_status = sqlExecStr(buffer,NULL);
                        if (ret_status == eDB_NO_ROWS_AFFECTED)
                        {
                            if (misTrimLen(mov_qp->asset_typ, ASSET_TYP_LEN) > 0)
                            {
                                sprintf(buffer,
                                        "insert into qvlwrk "
                                        " (stoloc,prtnum,untqty,"
                                        "pndqvl, prt_client_id, wh_id, asset_typ) "
                                        " values"
                                        " ('%s','%s','%ld','%f','%s','%s','%s') ",
                                        jump_dstloc,
                                        mov_qp->prtnum,
                                        cmb_tot_pckqty,
                                        cmb_space_needed,
                                        mov_qp->prt_client_id,
                                        wh_id,
                                        mov_qp->asset_typ);
                            }
                            else
                            {
                                sprintf(buffer,
                                        "insert into qvlwrk "
                                        " (stoloc,prtnum,untqty,"
                                        "pndqvl, prt_client_id, wh_id) "
                                        " values"
                                        " ('%s','%s','%ld','%f','%s','%s') ",
                                        jump_dstloc,
                                        mov_qp->prtnum,
                                        cmb_tot_pckqty,
                                        cmb_space_needed,
                                        mov_qp->prt_client_id,
                                        wh_id);
                            }
                            ret_status = sqlExecStr(buffer,
                                                    NULL);
                            if (ret_status != eOK)
                            {
                                pckrel_WriteTrc("Error inserting "
                                                "qvlwrk");
                                return (ret_status);
                            }
                        }
                    }
                    sprintf(buffer,
                            "assign resource code "
                            " where delta_pndqvl =  %f "
                            "   and rescod = '%s' "
                            "   and stoloc = '%s' "
                            "   and wh_id  = '%s' ",
                            cmb_space_needed,
                            mov_qp->pm_rescod,
                            jump_dstloc,
                            wh_id);

                    ret_status = srvInitiateCommand(buffer, NULL);
                    if (ret_status != eOK)
                    {
                        pckrel_WriteTrc("Error updating locmst rescod..");
                        return(ret_status);
                    }
                    alloc_something = TRUE;
                }
            }
        }
        prv_mov_qp = mov_qp;
        if (mov_qp)
            mov_qp = mov_qp->next;
    }
    misTrc(T_FLOW, "Done with this cmbcod in sAllocateLocationForCmbcod, "
                   "returning eOK");

    return(eOK);
}

LIBEXPORT 
RETURN_STRUCT *pckProcessPickReleaseAllocation(char *pcksts_i,
                                               char *cmbcod_i,
                                               char *wrkref_i,
                                               char *ship_id_i,
                                               moca_bool_t *comflg_i,
                                               char *schbat_i,
                                               moca_bool_t *cache_locs_i,
                                               moca_bool_t *skip_rplchk_i,
                                               char *wh_id_i)  
{
    char pcksts[PCKSTS_LEN+1];
    char schbat[SCHBAT_LEN+1];

    char save_relgrp[RTSTR1_LEN + 1];
    char comp_relgrp[RTSTR1_LEN + 1];
    char save_cmbcod[CMBCOD_LEN + 1];
    char comp_ship_id[SHIP_ID_LEN + 1];
    char save_ship_id[SHIP_ID_LEN + 1];
    char wh_id[WH_ID_LEN + 1];

    char buffer[2000];
    long resources_required;
    long ret_status;
    long process_relgrp;
    long commit_as_we_go;
    long looking_specific = 0;
    long resource_table_filled = 0;
    long skip_rplchk;

    DATA_STRUCT *data;
    RULES_STRUCT *rules;
    ALLOC_QUEUE *Queue, *qp;
    static AREA_LIST *AreaListHead;

    RETURN_STRUCT *CurPtr = NULL;
    mocaDataRes *res= NULL;
    mocaDataRow *row = NULL;

    memset(pcksts, 0, sizeof(pcksts));
    memset(schbat, 0, sizeof(schbat));
    memset(wh_id, 0, sizeof(wh_id));
    
    if (wh_id_i && misTrimLen(wh_id_i, WH_ID_LEN))
    { 
        misTrimcpy(wh_id, wh_id_i, WH_ID_LEN);      
    }

    pckrel_GetConfig(&data, &rules, 0, wh_id);


    /*
     * Set the gCacheLocations global variable
     * - this will control whether or not the 
     * "sFreeLocationLists" actually does anything
     */
    if (cache_locs_i)
    {
        gCacheLocations = *cache_locs_i;
        if (*cache_locs_i == 0 &&
            (misTrimIsNull(schbat_i, SCHBAT_LEN)))
        {
            /* 
             * Don't really need to do anything...call was made 
             * (by process pick release) to
             * reset the cache flag only.  The global flag is set to 0,
             * which will cause sFreeLocationLists to free the location
             * list.
             */

            if (AreaListHead)
                sFreeLocationLists(&AreaListHead);

            return(srvResults(eOK, NULL));
        }
    }
    else
    {
        gCacheLocations = 0;
    }

    if (skip_rplchk_i && *skip_rplchk_i)
        skip_rplchk = *skip_rplchk_i;
    else
        skip_rplchk = 0;

    if (AreaListHead)
        sFreeLocationLists(&AreaListHead);

    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
    {
        misTrimcpy(schbat, schbat_i, SCHBAT_LEN);
    }
    else
    {
        return(APPMissingArg("schbat"));
    }

    if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
        misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);
    else
        strcpy(pcksts, PCKSTS_PENDING);

    commit_as_we_go = BOOLEAN_FALSE;
    if (comflg_i)
    {
        if (*comflg_i == BOOLEAN_TRUE)    
            commit_as_we_go = BOOLEAN_TRUE;
    }


    ret_status = sLoadAllocationQueue(&Queue,
                                      &looking_specific,
                                      cmbcod_i,
                                      ship_id_i,
                                      wrkref_i,
                                      pcksts,
                                      schbat,
                                      wh_id,
                                      commit_as_we_go);

    /*
     * if ret_status is eDB_NO_ROWS_AFFECTED, 
     * which means we didn't find any pick work/move
     * we should skip this schbat
     * and continue to do the logic.
     */
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return(srvResults(ret_status, NULL));
    }

    /* Loop through and apply only to replenishments and completely 
     * allocated shipments 
     */

    memset(save_relgrp, 0, sizeof(save_relgrp));
    memset(comp_relgrp, 0, sizeof(comp_relgrp));
    memset(save_cmbcod, 0, sizeof(save_cmbcod));


    for (qp = Queue; qp; qp = qp->next)
    {        
        pckrel_WriteTrc("  MS-LBI: Main Loop Begin . . .");
		pckrel_WriteTrc("   MS-LBI: qp->wrkref: %s, qp->cmbcod: %s", qp->wrkref, qp->cmbcod );		
		
		/* Process each combination code ... */
        if (misStrncmpChars(qp->cmbcod, save_cmbcod, CMBCOD_LEN) != 0)
        {

            misStrncpyChars(save_cmbcod, qp->cmbcod, CMBCOD_LEN);

            /* First, if this is a pick ... we need to make sure 
             * that we are only releasing if the entire group has 
             * been allocated 
             */

            resources_required = FALSE;

            /* Now, lets pull out the release grouping to see 
             * if we should even consider releasing this stuff 
             */

           if (misCiStrncmp(rules->relgrp, "SHIP_ID", strlen("SHIP_ID")) == 0)
            {
                memset(comp_relgrp, 0, sizeof(comp_relgrp));
                if (misStrncmpChars(qp->wrktyp,
                            WRKTYP_REPLENISH, WRKTYP_LEN) == 0  ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_MANUAL, WRKTYP_LEN) == 0 || 
                    misStrncmpChars(qp->wrktyp, WRKTYP_TOP_OFF, WRKTYP_LEN) == 0 || 
                    misStrncmpChars(qp->wrktyp, WRKTYP_TRIGGERED, WRKTYP_LEN) == 0 ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_EMERGENCY, WRKTYP_LEN) == 0 ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_PIA_DEMAND, WRKTYP_LEN) == 0)
                {
                    misStrncpyChars(comp_relgrp, qp->schbat, SCHBAT_LEN);
                }
                else
                {
                    if (strlen(qp->wkonum) != 0)
                    {
                        misStrncpyChars(comp_relgrp, qp->wkonum, WKONUM_LEN);
                    }
                    else
                    {
                        misStrncpyChars(comp_relgrp, qp->ship_id, SHIP_ID_LEN);
                    }
                }
            }
            else if (misCiStrncmp(rules->relgrp,
                                  "SCHBAT", strlen("SCHBAT")) == 0)
            {
                memset(comp_relgrp, 0, sizeof(comp_relgrp));
                if (misStrncmpChars(qp->wrktyp, WRKTYP_REPLENISH, WRKTYP_LEN) == 0  ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_MANUAL, WRKTYP_LEN) == 0 || 
                    misStrncmpChars(qp->wrktyp, WRKTYP_TOP_OFF, WRKTYP_LEN) == 0 || 
                    misStrncmpChars(qp->wrktyp, WRKTYP_TRIGGERED, WRKTYP_LEN) == 0 ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_EMERGENCY, WRKTYP_LEN) == 0 ||
                    misStrncmpChars(qp->wrktyp, WRKTYP_PIA_DEMAND, WRKTYP_LEN) == 0)
                {
                    misStrncpyChars(comp_relgrp, qp->schbat, SCHBAT_LEN); 
                }
                else
                {
                    misStrncpyChars(comp_relgrp, qp->schbat, SCHBAT_LEN);
                }
            }
            else
            {
                process_relgrp = TRUE;
            }

            if (misStrncmpChars(save_relgrp, comp_relgrp, RTSTR1_LEN) != 0)
            {

                misStrncpyChars(save_relgrp, comp_relgrp, RTSTR1_LEN);

                /* Only process this group if we have allocated */
                /* the same amount of product that the group of */
                /* lines are for ... */

                ret_status = pckrel_PrcReleaseGroup(&process_relgrp,
                                                    qp->ship_id,
                                                    qp->schbat,
                                                    qp->wkonum,
                                                    qp->wkorev,
                                                    qp->client_id,
                                                    save_relgrp,
                                                    qp->wrkref,
                                                    skip_rplchk,
                                                    qp->wh_id);
                if (ret_status != eOK)
                {
                    sFreeLocationLists(&AreaListHead);
                    sFreeAllocationQueue(&Queue);
                    return (srvResults(ret_status, NULL));
                }
            }
            /* Now, if we think we are supposed to process, make 
             * sure the REGISTER SHIPMENT ALLOCATION COMPLETE has 
             * been called ... this used to try and process only if 
             * we were is split mode, but if you think about it, we 
             * always need to check because in single mode, we may 
             * read and process a bunch of stuff at the top, read 
             * again in the bottom half and have different data 
             */

            /* 
             *  We only make the checks below if we don't have a wkonum
             */
            if (process_relgrp == TRUE &&
                strlen(qp->wkonum) == 0 &&
                (misStrncmpChars(qp->wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0 ||
                (misStrncmpChars(qp->wrktyp, WRKTYP_BULK_PICK, WRKTYP_LEN ==0))))
            {
                memset(comp_ship_id, 0, sizeof(comp_ship_id));
                sprintf(comp_ship_id, "%s", qp->ship_id);
                if (strcmp(comp_ship_id, save_ship_id) != 0)
                {
                    sprintf(buffer,
                            "select alcdte "
                            "  from shipment "
                            " where alcdte is not null "
                            "   and ship_id = '%s' ",
                            qp->ship_id);
                    ret_status = sqlExecStr(buffer, NULL);
                    if (ret_status != eOK &&
                        ret_status != eDB_NO_ROWS_AFFECTED)
                    {
                        sFreeAllocationQueue(&Queue);
                        pckrel_WriteTrc("Error reading for shipment."
                                        "alcdte ...");
                        return (srvResults(ret_status, NULL));
                    }
                    if (ret_status != eOK)
                    {
                        process_relgrp = FALSE;
                    }
                    else
                    {
                        memset(save_ship_id, 0, sizeof(save_ship_id));
                        sprintf(save_ship_id, "%s", qp->ship_id);
                    }
                }
            }
            /*
             * Now, if we should release this grouping of picks, 
             * then begin processing them 
             */

            if (process_relgrp == TRUE)
            {                
				pckrel_WriteTrc("   MS-LBI: processing relgrp . . ." );				
				
				if (!resource_table_filled)
                {
                    pckrel_WriteTrc("   MS-LBI: populating resource table . . ." );
					sSetResourceSpaceRequirements(Queue, wh_id);
                    resource_table_filled = 1;
                }


                ret_status = sAllocateLocationsForCmbcod(qp,
                                                         &AreaListHead,
                                                         rules,
                                                         data,
                                                         looking_specific);

                if (ret_status != eOK)
                {
                    pckrel_WriteTrc("  ERROR during cmbcod allocation: %d",
                                    ret_status);
                    sFreeLocationLists(&AreaListHead);
                    sFreeAllocationQueue(&Queue);
                    sFreeSpaceRequiredTable();
                    return(srvResults(ret_status, NULL));
                }

            }
            /* The picks were released even when replenishments existed 
             * in case of work order.Hence added check to ensure that picks
             * are not released when replenishemnts exist for work order
             */
            else if (looking_specific == TRUE
                     || (process_relgrp == FALSE && strlen(qp->wkonum) != 0 ))
            {
                pckrel_WriteTrc("  Release group not ready on "
                                "specific read, returning error ...");

                sFreeLocationLists(&AreaListHead);
                sFreeAllocationQueue(&Queue);
                sFreeSpaceRequiredTable();
                return (srvResults(eINT_PICK_REL_GROUP_NOT_READY, NULL));
            }
        }

        if (commit_as_we_go)
            srvCommit();
    }
    sFreeLocationLists(&AreaListHead);
    sFreeAllocationQueue(&Queue);
    if (resource_table_filled)
        sFreeSpaceRequiredTable();

    return(srvResults(eOK, NULL));

}
