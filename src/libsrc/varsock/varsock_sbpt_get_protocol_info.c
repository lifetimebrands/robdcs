static char RCS_Id[] = "$Id: varsock_sbpt_get_protocol_info.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_sbpt_get_protocol_info.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level component.  Contains information specific to Skill Bosch protocol.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <applib.h>

#include "varsock_colwid.h"
#include "varsock_err.h"
#include "varsock_util.h"

/* get the items specific to Skill Bosch communication protocol. */
#include "varsock_sbpt.h"

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const DATA_STRING     = "data_string";
static const char *const DATA_LENGTH     = "data_length";

/* columns names */
static const char *const PREFIX          = "prefix";
static const char *const PREFIX_LENGTH   = "prefix_length";
static const char *const SUFFIX          = "suffix";
static const char *const SUFFIX_LENGTH   = "suffix_length";
static const char *const MIN_ACK_NAK_LEN = "min_ack_nak_len";

/* other LOCAL #defines go here */

/* forward declaration of local functions. */

/* global variables. */

/* static functions */

/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_sbpt_get_protocol_info (
	char *data_string_i, /* The message packet that is to be send to host */
	long *data_length_i  /* length of the data string being passed in. */
)
{
	/* locals for storing input variables */
	char prefix [ PREFIX_LEN + 1 ];
	long prefix_length;
	char suffix [ SUFFIX_LEN + 1 ];
	long suffix_length;

	/* scratch variables -- may get used in more than one context. */
	long retVal = !eOK;
	char buffer[1024];

	/* Other variables */
	RETURN_STRUCT *Result = 0;
	const char *const fn = "varsock_sbpt_get_protocol_info";

	misTrc (T_FLOW, "%s - entered", fn);

	/* firewall the input arguments. */
	/* Even though we do NOT use the data_string we must check it to make sure
	 * that socket adapter code pass it in. */
	if (!data_string_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, DATA_STRING);
		return APPMissingArg(DATA_STRING);
	}
	else {
		/* NOTE that it is legal to call this function with a NULL value string as a data_string */
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, DATA_STRING, data_string_i);
	}

	/* even though we do NOT use the data_string or its length we must check it
	 * to make sure that socket adapter code passed it in! */
	if (!data_length_i) {
		misTrc(T_FLOW, "%s - required argument %s is not specified.", fn, DATA_LENGTH);
		APPMissingArg(DATA_LENGTH);
	}
	else {
		misTrc(T_FLOW, "%s - checking to see if argument %s(%ld) is valid.", fn, DATA_LENGTH, *data_length_i);
		if (*data_length_i < 0) {
			ZAP(buffer);
			sprintf(buffer, "%s - argument %s specified as %ld.  It should NOT be less than 0", DATA_LENGTH, *data_length_i);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			APPInvalidArg(DATA_STRING, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, DATA_LENGTH, *data_length_i);
	}

	/* Log the value of input parameters */
	misTrc(T_FLOW, "%s - input values: %s(%s) %s(%ld)", fn, DATA_STRING, data_string_i, DATA_LENGTH, *data_length_i);

	/* generate the prefix string and capture its length. */
	ZAP( prefix );
	/* Right justify the mailbox-name "%-31s", prefix the length in hex "%03X" and a NULL in the end. */
	prefix_length = sprintf(prefix, "%-31s%03X%c", TO_MAILBOX_NAME, *data_length_i, '\0');
	/* since we cannot pass around NULLs in a string through MOCA, mask them with some other character.
	 * receiving function can then convert them and get back the original string. */
	misTrc(T_FLOW, "%s - before NULL masking prefix(%s) prefix_length(%ld).", fn, prefix, prefix_length);
	sMaskNullsInString(prefix, prefix_length);

	/* In this protocol there is no suffix to be attached to the message. */
	ZAP( suffix );
	suffix_length = 0;

	/* setup the column names in the results touple */
	Result = srvResultsInit(eOK,
			PREFIX,          COMTYP_CHAR, PREFIX_LEN,
			PREFIX_LENGTH,   COMTYP_INT,  sizeof(long),
			SUFFIX,          COMTYP_CHAR, SUFFIX_LEN,
			SUFFIX_LENGTH,   COMTYP_INT,  sizeof(long),
			MIN_ACK_NAK_LEN, COMTYP_INT,  sizeof(long),
			NULL);

	if (!Result) {
		/* srvResultInit failed! */
		misTrc(T_FLOW, "%s - Unexpected error! srcResultsInit() failed.  Exiting...", fn);

		/* return error to calling process. */
		return srvResults(eVAR_SRV_RESULTS_INIT_FAILED, NULL);
	}

	/* do we have a ack or a nak */
	misTrc(T_FLOW, "%s - sending back %s(%s), %s(%ld), %s(%s), %s(%ld), %s(%i)", fn,
			PREFIX, prefix, PREFIX_LENGTH, prefix_length, SUFFIX, suffix,
			SUFFIX_LENGTH, suffix_length, MIN_ACK_NAK_LEN, ACK_NAK_MSG_LEN);

	retVal = srvResultsAdd(Result, prefix, prefix_length, suffix, suffix_length, ACK_NAK_MSG_LEN);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - unexpected error! srvResultsAdd returned error(%ld)!"
				"  Exiting...", fn, retVal);
		return srvResults(eVAR_SRV_RESULTS_ADD_FAILED, NULL);
	}

	return Result;
}

