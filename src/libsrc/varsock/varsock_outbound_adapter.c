static char RCS_Id[] = "$Id: varsock_outbound_adapter.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_outbound_adapter.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: Simple component to test a VAR-level command.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <oslib.h>
#include <applib.h>
#include <sqllib.h>

#include <varsock_colwid.h>
#include <varsock_err.h>

/* SeamLES include files. */
#include <lib_include.h>

#include <varsock_util.h>

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const IFD_DATA_DTL_CNT      = "ifd_data_dtl_cnt";
static const char *const IFD_DATA_DTL          = "ifd_data_dtl";
static const char *const HOSTID                = "host_id";
static const char *const PORTNUM               = "portnum";
static const char *const ACK_TIMEOUT           = "ack_timeout";
static const char *const RECONNECT_TIMEOUT     = "reconnect_timeout";
static const char *const GET_PROTOCOL_INFO_CMD = "get_protocol_info_cmd";
static const char *const CHECK_ACK_NAK_CMD     = "check_ack_nak_cmd";
static const char *const TEST_MODE             = "test_mode";
static const char *const TEST_STRING           = "test_string";
static const char *const SOCKET_TRACE_FILENAME = "socket_trace_filename";

/* forward declaration of local functions. */
static long sConnectLoop(SOCKET_FD *fd, const char *host, long port, long timeout);
static long sFormatMsg(const char *cmd_name, const char *data, long data_len,
						char *message, long *message_len, long *min_response_len);
static int sIsItAck(const char *msg, long msg_len, const char *ack, long ack_len, const char *cmd);
static char *sReadMessage(char *);
static void sCloseSocket (void);
static void sAtExit (void);

/* global variables. */
static SOCKET_FD fd = -1; /* socket descriptor of the connect'ed socket. */
static char connected_hostid [ HOSTID_LEN + 1 ];
static long connected_portnum;
static FILE *socket_trace_fp = 0;
static int socket_trace = 0;
static char open_socket_trace_filename[SOCKET_TRACE_FILENAME_LEN + 1];

/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_outbound_adapter (
	double *ifd_data_blk_ndx_i,    /* Currently NOT used. */
	double *ifd_data_dtl_cnt_i,    /* Used to access the event data to be send */
	void **ifd_data_dtl_i,         /* Used to access the event data to be send */
	void **alg_context_i,          /* Currently NOT used. */
	char *hostid_i,                /* Name of the computer to connect to */
	long *portnum_i,               /* Port number to connect to */
	long *ack_timeout_i,           /* number of seconds to wait for getting
							    	* an ACK/NAK from host */
	long *reconnect_timeout_i,     /* time in seconds to wait before retrying
							    	* to connect to the host after a connection
							    	* attempt fails.*/
	char *get_protocol_info_cmd_i, /* name of the MOCA component to be used to 
				                    * package the user data with protocol fluff.*/
	char *check_ack_nak_cmd_i,     /* name of the MOCA component to be used
							    	* to check ack/naks */
	long *test_mode_i,             /* if this adapter is to be run in test mode. */
	char *test_string_i,           /* The test message to be sent to the host systems. */
	char *socket_trace_filename_i  /* The File in which the socket activity would be logged. */
)
{
	/* locals for storing input variables */
	double ifd_data_dtl_cnt;
	char hostid [ HOSTID_LEN + 1 ];
	long portnum;
	long ack_timeout;
	long reconnect_timeout;
	char get_protocol_info_cmd [ COMMAND_LEN + 1 ];
	char check_ack_nak_cmd [ COMMAND_LEN + 1 ]; 

	/* scratch variables -- may get used in more than one context. */
	char buffer[1024];
	long retVal = !eOK;
	/* the user data to be packaged and set to host */
	char data_string [ DATA_STRING_LEN + 1 ];
	/* user date along with the protocol embellishments, <ETX, <STX>, etc. */
	char message_string [ MESSAGE_STRING_LEN + 1 ];
	/* length of the message string. */
	long message_length;
	/* ACK/NAK response received from host systems */
	char response_string [ RESPONSE_STRING_LEN + 1 ];
	/* lenght of the minimum ACK/NAK response string to be expected from the host systems. */
	long min_ack_nak_len = 0;
	/* are we running in test mode? */
	long test_mode;
	/* test string, in case the adapter is to be run to just send out a test message. */
	char test_string [ DATA_STRING_LEN + 1 ];
	/* trace file in which socket activity would be logged. */
	char socket_trace_filename[ SOCKET_TRACE_FILENAME_LEN + 1];

	/* Other variables */
	char remote_hostid [ HOSTID_LEN + 1 ];
	short remote_portnum;
	const char *const fn = "varsock_outbound_adapter";
	int read_complete_ack = 1;
	int current_response_length = 0;

	/* The structure that would be used to access the event data information */
	slObjIFD_IFDDataPrcArrData_TypPtr ifd_data_dtl;
	long ndx;

	misTrc (T_FLOW, "%s - entered", fn);

	/* firewall the input arguments. */
	/* check if the test_mode flag has been specified. */
	if (!test_mode_i || !*test_mode_i) {
		misTrc(T_FLOW, "%s - parameter %s was not specified.  OK! it is an optional paramter.", fn, TEST_MODE);
		test_mode = 0;
	}
	else {
		test_mode = *test_mode_i;
		misTrc(T_FLOW, "%s - checking to see if argument %s(%ld) is correct.", fn, TEST_MODE, test_mode);
		if (test_mode != 0 && test_mode != 1) {
			ZAP(buffer);
			sprintf(buffer, "Invalid value specified for %s(%ld).  The valid values are 0 or 1",
					TEST_MODE, test_mode);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(TEST_MODE, buffer);
		}
	}
	if (test_mode) {
		misTrc(T_FLOW, "%s - %s(%ld) adapter is running in test mode.", fn, TEST_MODE, test_mode);
		misTrc(T_FLOW, "%s - It will send out the test_string specified and read the ack/nak response."
				" It does not interact with the seamles system in anyway.", fn);
	}
	else {
		misTrc(T_FLOW, "%s - %s(%ld) adapater is NOT in test mode.  The data to be sent out is retrived from SeamLES.",
				fn, TEST_MODE, test_mode);
	}

	/* ifd_data_dtl_cnt_i is a parameter used to get the event data and is required. */
	if (test_mode) {
		misTrc(T_FLOW, "%s - %s is not a required argument if %s is %ld", fn, IFD_DATA_DTL_CNT, TEST_MODE, test_mode);
		/* set this to one so that we can make one iteration through the loop. */
		ifd_data_dtl_cnt = 1;
		misTrc(T_FLOW, "%s - setting %s to %f", fn, IFD_DATA_DTL_CNT, ifd_data_dtl_cnt);
	}
	else {
		if (!ifd_data_dtl_cnt_i) {
			misTrc(T_FLOW, "%s - required argument %s was not specified!  Exiting...", fn, IFD_DATA_DTL_CNT);
			APPMissingArg(IFD_DATA_DTL_CNT);
		}
		else {
			ifd_data_dtl_cnt = *ifd_data_dtl_cnt_i;
			misTrc(T_FLOW, "%s - Checking if argument %s(%f) is valid.", fn, IFD_DATA_DTL_CNT, ifd_data_dtl_cnt);

			if (ifd_data_dtl_cnt <= 0) {
				ZAP(buffer);
				sprintf(buffer, "%s specified as %ld. It should be more than 0",
					IFD_DATA_DTL_CNT, ifd_data_dtl_cnt);
				misTrc (T_FLOW, "%s - %s", fn, buffer);

				return APPInvalidArg(IFD_DATA_DTL_CNT, buffer);
			}
			misTrc(T_FLOW, "%s - Argument %s(%f) checks out fine.", fn, IFD_DATA_DTL_CNT, ifd_data_dtl_cnt);
		}
	}

	/* ifd_data_dtl_i is a parameter used to get the event data and is required. */
	if (test_mode) {
		misTrc(T_FLOW, "%s - %s is not a required argument is %s is %ld", fn, IFD_DATA_DTL, TEST_MODE, test_mode);
	}
	else {
		if (!ifd_data_dtl_i) {
			misTrc (T_FLOW, "%s - required argument %s was not specified!  Exiting...", fn, IFD_DATA_DTL);
			return APPMissingArg(IFD_DATA_DTL);
		}
	}

	/* hostid is required. */
	if (!hostid_i || !misTrimLen(hostid_i, HOSTID_LEN)) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, HOSTID);
		return APPMissingArg(HOSTID);
	}
	else {
		ZAP(hostid);
		misTrimcpy(hostid, hostid_i, HOSTID_LEN);
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, HOSTID, hostid);
	}

	/* portnum is required. */
	if (!portnum_i || !*portnum_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, PORTNUM);
		return APPMissingArg(PORTNUM);
	}
	else {
		portnum = *portnum_i;
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, PORTNUM, portnum);
	}

	/* ack timeout is required. 
	 * in principle we could assume an arbitary value of this timer,
	 * but as of now we are choosing not to do so. Also we disallow 
	 * a value of 0 as that would cause select to spin in an infinite
	 * loop and become a CPU hog. */
	if (!ack_timeout_i) {
		misTrc (T_FLOW, "%s - Required argument ack_timeout was not specified!  Exiting...", fn);
		return APPMissingArg(ACK_TIMEOUT);
	}
	else {
		ack_timeout = *ack_timeout_i;
		misTrc(T_FLOW, "%s - Checking if argument ack_timeout (%ld) is valid.", fn, ack_timeout);

		if (ack_timeout <= 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld. It should be more than 0",
				ACK_TIMEOUT, ack_timeout);
			misTrc (T_FLOW, "%s - %s", fn, buffer);

			return APPInvalidArg(ACK_TIMEOUT, buffer);
		}
		misTrc(T_FLOW, "%s - Argument ack_timeout(%ld) checks out fine.", fn, ack_timeout);
	}
	
	/* reconnect_timeout is required.  A default value of 60seconds is
	 * specified in the moca function definition. */
	if (!reconnect_timeout_i) {
		misTrc (T_FLOW, "%s - Required argument reconnect_timeout was not specified!  Exiting...", fn);
		return APPMissingArg(RECONNECT_TIMEOUT);
	}
	else {
		reconnect_timeout = *reconnect_timeout_i;
		misTrc(T_FLOW, "%s - Checking if argument reconnect_timeout (%ld) is valid.", fn, reconnect_timeout);

		if (reconnect_timeout < 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld. It should be more than 0."
					" Or specify 0 to indicate that connection should be tried only once.",
					RECONNECT_TIMEOUT, reconnect_timeout);
			misTrc (T_FLOW, "%s - %s", fn, buffer);

			return APPInvalidArg(RECONNECT_TIMEOUT, buffer);
		}
		misTrc(T_FLOW, "%s - Argument reconnect_timeout(%ld) checks out fine.", fn, reconnect_timeout);
	}

	/* get_protocol_info_cmd is required. */
	if (!get_protocol_info_cmd_i || !misTrimLen(get_protocol_info_cmd_i, COMMAND_LEN)) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, GET_PROTOCOL_INFO_CMD);
		return APPMissingArg(GET_PROTOCOL_INFO_CMD);
	}
	else {
		ZAP( get_protocol_info_cmd );
		misTrimcpy(get_protocol_info_cmd, get_protocol_info_cmd_i, COMMAND_LEN);

		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, GET_PROTOCOL_INFO_CMD, get_protocol_info_cmd);
	}

	/* check_ack_nak is required. */
	if (!check_ack_nak_cmd_i || !misTrimLen(check_ack_nak_cmd_i, COMMAND_LEN)) {
		misTrc (T_FLOW, "%s - Required argument check_ack_nak_cmd was not specified!  Exiting...", fn);
		return APPMissingArg(CHECK_ACK_NAK_CMD);
	}
	else {
		ZAP( check_ack_nak_cmd );
		misTrimcpy(check_ack_nak_cmd, check_ack_nak_cmd_i, COMMAND_LEN);

		misTrc(T_FLOW, "%s - Argument check_ack_nak_cmd (%s) checks out fine.", fn, check_ack_nak_cmd);
	}

	/* the test_data is not a mandatory paramter, but if it is present extract it
	 * and store it in a local variable */
	ZAP(test_string);
	if (test_mode) {
		if (test_string_i && misTrimLen(test_string_i, DATA_STRING_LEN)) {
			misTrimcpy(test_string, test_string_i, DATA_STRING_LEN);

			misTrc(T_FLOW, "%s - argument %s(%s) checks out fine.", fn, TEST_STRING, test_string);
			misTrc(T_FLOW, "%s - running the outbound adapter to send a test message", fn);
		}
		else {
			ZAP(buffer);
			sprintf(buffer, "%s must is be specified if running adapter in test mode, i.e. %s is set to 1",
					TEST_STRING, TEST_MODE);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPMissingArg(TEST_STRING);
		}
	}
	else {
		misTrc(T_FLOW, "%s - %s is not required if %s specified as %ld", fn, TEST_STRING, TEST_MODE, test_mode);
	}

	ZAP(socket_trace_filename);
	if (socket_trace_filename_i && misTrimLen(socket_trace_filename_i, SOCKET_TRACE_FILENAME_LEN)) {
		misTrc(T_FLOW, "%s - Argument %s(%s) specified. Socket activity would be logged to a seperate tracefile.",
				fn, SOCKET_TRACE_FILENAME, socket_trace_filename_i);
		misExpandVars(socket_trace_filename, socket_trace_filename_i, sizeof(socket_trace_filename), 0);
		misTrc(T_FLOW, "%s - Expanded filename for socket trace is (%s)", fn, socket_trace_filename);

		/* if the current file to which trace messages are going is different from this,
		 * then we need to close the currently open file and open the new one.  Else do nothing. */
		if (strcmp(open_socket_trace_filename, socket_trace_filename)) {
			misTrc(T_FLOW, "%s - Currently trace is going to (%s).  But now it has to go to (%s).",
					fn, open_socket_trace_filename, socket_trace_filename);
			/* first close the existing file that is open */
			if (socket_trace_fp) {
				fclose(socket_trace_fp);
				misTrc(T_FLOW, "%s - closing currently open file (%s)", fn, open_socket_trace_filename);
			}

			/* now open the new file.  The trace file is _ALWAYS_ opened in APPEND mode. */
			socket_trace_fp = fopen(socket_trace_filename, "a");
			/* Bail-out if tracefile cannot be opened! */
			if (!socket_trace_fp) {
				misTrc(T_FLOW, "%s - cannot open file (%s) for socket trace.  Fatal error!  Exiting...",
					fn, socket_trace_filename);
				misTrc(T_FLOW, "%s - fopen error: %s", fn, osError());
				return srvResults(eVAR_SOCKET_TRACE_FILE_OPEN_ERROR, 0);
			}
			misTrc(T_FLOW, "%s - opened the new socket trace file(%s)", fn, socket_trace_filename);

			/* store the name of the current tracefile in a local variable so that next
			 * time we can check if the trace has to go to the same file or not. */
			strcpy(open_socket_trace_filename, socket_trace_filename);

			misTrc(T_FLOW, "%s - unbuffer the output to the socket trace file.", fn);
			setbuf(socket_trace_fp, 0);
		}
		else {
			misTrc(T_FLOW, "%s - socket trace file specified (%s) is same as the one that is currently"
				" being used (%s). OK!", fn, socket_trace_filename, open_socket_trace_filename);
		}

		socket_trace = 1;
	}
	else {
		misTrc(T_FLOW, "%s - Argument %s not specified.  OK, it is an optional argument.", fn, SOCKET_TRACE_FILENAME);
		socket_trace = 0;
	}

	misTrc(T_FLOW, "%s - input parameters: %s(%s), %s(%ld), %s(%ld), %s(%ld), %s(%s), %s(%s), %s(%ld), %s(%s), %s(%s)",
			fn, HOSTID, hostid, PORTNUM, portnum, ACK_TIMEOUT, ack_timeout,
			RECONNECT_TIMEOUT, reconnect_timeout, GET_PROTOCOL_INFO_CMD, get_protocol_info_cmd,
			CHECK_ACK_NAK_CMD, check_ack_nak_cmd, TEST_MODE, test_mode, TEST_STRING, test_string,
			SOCKET_TRACE_FILENAME, socket_trace_filename);

	if (socket_trace) {
		fprintf(socket_trace_fp, "%s ------------------------------------------------------------------\n",
				sGetTimeString());
	}

	/* first ensure that the values of portnum and hostid are the same as the last time. */
	if (strcmp(connected_hostid, hostid) != 0 || connected_portnum != portnum) {
		misTrc(T_FLOW, "%s - socket %i is currently connected to %s at %ld.", fn,
				sGetSockFd(fd), connected_hostid, connected_portnum);
		misTrc(T_FLOW, "%s - where as now, the function has been called with %s(%s) %s(%ld)", fn, 
				HOSTID, hostid, PORTNUM, portnum);
		misTrc(T_FLOW, "%s - hence we will close the existing socket (%i)", fn, sGetSockFd(fd));
		sCloseSocket();
	}

	/* Test to see of the socket is aready connected to the server, is the connection good? */
	misTrc(T_FLOW, "%s - Checking to see if the socket is already connected to a host", fn);
	misTrc(T_FLOW, "%s - connected socket fd is (%i)", fn, sGetSockFd(fd));
	if (fd == -1) {
		/* If fd is -1 then one of the following is true:
		 * 1 - Either we have closed an already connected socket due to some problem:
		 *    + either a new set of port and host this time
		 *    + or we received a nak in trying to send message.
		 *    + or we encounterd an error while sending stuff (socket reset by server) and we had to close
		 *  	the connection.
		 *    In all of these situations we close the connection and reset fd to a value of 0.
		 * 2 - The other situation when fd would be -1 is when this function is being entered for the
		 *     first time fd, which is a static variable, would be initialized to -1.
		 * In both of these case we need not call the osSockAddress.  Because calling osSockAddress
		 * on a socket of value 0 causes a message to appear on the stderr to appear! So we need to avoid it. */
		retVal = !eOK;
	}
	else {
		retVal = osSockAddress(fd, remote_hostid, sizeof(remote_hostid), &remote_portnum);
		if (retVal != eOK) {
			misTrc(T_FLOW, "%s - existing connected socket (%i) does not appear to be good!  Closing the socket.",
				fn, sGetSockFd(fd));
			sCloseSocket();
		}
	}

	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - the socket has net yet been connected to.  Get a new socket connection.", fn);
		if (sConnectLoop(&fd, hostid, portnum, reconnect_timeout) != eOK) {
			misTrc(T_FLOW, "%s - Fetal error! Cannot continue. Exiting", fn);
			return srvResults(eVAR_SOCK_CONNECT_FAILED, 0);
		}

		misTrc(T_FLOW, "%s - Socket Connection sucessful to Socket(%i).", fn, sGetSockFd(fd));
		/* now store the value of the hostid and portnum in a static variable, so that next time
		 * if someone calls the adapter function with a different hostid/portnum then we can close
		 * this socket and start a new one. */
		strcpy(connected_hostid, hostid);
		connected_portnum = portnum;
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - Successfully connected to %s at %i.\n", sGetTimeString(),
					connected_hostid, connected_portnum);
		}

		/* Now register the at-exit function so that the socket would be closed if this
		 * process terminates.  This would avoid resource leakage. */
		retVal = osAtexit(sAtExit);
		if (retVal == 0) {
			misTrc(T_FLOW, "%s - Sucessfully, registered the at exit function"
				" to close the socket when the process terminates.", fn);
		}
		else {
			misTrc(T_FLOW, "%s - Error trying to register the at-exit function!  Exiting...", fn);
			return srvResults(eVAR_CANNOT_REGISTER_ATEXIT_FUNC, NULL);
		}
	}
	else {
		misTrc(T_FLOW, "%s - Socket is already connected.  Local socket(%i),"
			" remote_hostid(%s) remote_socket(%i)", fn, sGetSockFd(fd),
			remote_hostid, (int)remote_portnum);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - Already connected to %s at %i.\n", sGetTimeString(),
					connected_hostid, connected_portnum);
		}
	}

	ifd_data_dtl = (slObjIFD_IFDDataPrcArrData_Typ *)*ifd_data_dtl_i;
	for ( ndx = 0; ndx < ifd_data_dtl_cnt; ndx++, ifd_data_dtl++) {
		/* variable used only in the body of this for loop. */
		int retVal = !eOK;
		int isAck = FALSE;
		/* length of the data_string */
		long data_length;

		ZAP(data_string);
		/* get the event data */
		if (*test_string) {
			misTrc(T_FLOW, "%s - %s is NOT null. The adapter was invoked to send out a test message", fn, TEST_STRING);
			misTrc(T_FLOW, "%s - do not get data from SeamLES, use the test string passed in.", fn);

			strcpy(data_string, test_string);
			data_length = strlen(data_string);
		}
		else {
			/* currently we do not anticipate to receive anything in predata or postdata */
			data_length = sprintf( data_string, "%s%s%s",
							   ifd_data_dtl->PreData,
							   ifd_data_dtl->Data,
							   ifd_data_dtl->PostData);
		}

		misTrc(T_FLOW, "%s - the data to be sent over the socket is (%s) of length(%ld).",
				fn, data_string, data_length);

		/* format the message, pad it with STX/ETX, add the message number to it, etc. */
		misTrc(T_FLOW, "%s - formating the data into packet.", fn);
		ZAP(message_string);
		retVal = sFormatMsg(get_protocol_info_cmd, data_string, data_length, message_string,
							&message_length, &min_ack_nak_len);
		if (eOK != retVal) {
			/* some problem formating the data. */
			/* TODO */
			misTrc(T_FLOW, "%s - problem encounterd while formatting the"
				" user data into protocol packet.  Cannot recover. Exiting...", fn);
			return srvResults(eVAR_CANNOT_MARSHAL_DATA, NULL);
		}
		else {
			misTrc(T_FLOW, "%s - data(%s) sucessfully marshalled into a messge packet(%s)"
					" of length(%ld).", fn, data_string, message_string, message_length);
		}

		/* send the message over the socket */
		misTrc(T_FLOW, "%s - sending data over the socket", fn);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - Sending data over socket. Length(%ld), message(%s).\n",
					sGetTimeString(), message_length, sGetPrintablePacket(message_string, message_length));
		}
		retVal = sSend(fd, message_string, message_length);
		if (retVal != eOK) {
			/* there was a problem encountered while sending stuff over
			 * the socket, close the socket. */ 
			misTrc(T_FLOW, "%s - Error encountered sending message over the socket."
				" Closing the socket!", fn);
			sCloseSocket();
			if (socket_trace) {
				fprintf(socket_trace_fp, "%s - Error encountered while sending data. Errno(%ld), error(%s)."
						" Socket connection closed.\n", sGetTimeString(), osSockErrno(), osSockError());
			}
			return srvResults(eVAR_SOCK_ERROR_SENDING_DATA, NULL);
		}
		else {
			misTrc(T_FLOW, "%s - data sent over the socket.", fn);
			if (socket_trace) {
				fprintf(socket_trace_fp, "%s - Data send successful.\n", sGetTimeString());
			}
		}

		/* now wait for an ack to come back. */
		misTrc(T_FLOW, "%s - now wait for ACK/NAK response from the receiving system.", fn);

		/* setup the variables which control this while loop. */
		read_complete_ack = 0;
		current_response_length = 0;
		ZAP(response_string);
		while (!read_complete_ack) {
			int len = 0;
			int how_much_to_read = 0;
			if (current_response_length == 0) {
				misTrc(T_FLOW, "%s - we have not yet read any response string characters", fn);
				misTrc(T_FLOW, "%s - start by doing a blocking read for the minimum"
						" number of ACK/NAK characters that we should read for the protocol.", fn);
				misTrc(T_FLOW, "%s - minimimum number of ACK/NAK characters as returned by previous"
						" user supplied server command is %ld.", fn, min_ack_nak_len);
				how_much_to_read = min_ack_nak_len;
			}
			else {
				misTrc(T_FLOW, "%s - we have already read a part of the response string.", fn);
				misTrc(T_FLOW, "%s - read additional portions of response string one chanacter at a time.", fn);
				how_much_to_read = 1;
			}
			if (socket_trace) {
				fprintf(socket_trace_fp, "%s - Starting a Blocking read for (%i) characters of ACK/NAK packet.\n",
						sGetTimeString(), how_much_to_read);
			}
			len = sReceive(fd, how_much_to_read,
					response_string + current_response_length, RESPONSE_STRING_LEN - current_response_length);
			if (len == -1) {
				/* there was problem encounterd while trying to read the ACK/NAK response
				 * back from the receiving system. */
				 misTrc(T_FLOW, "%s - error encountered while receiving the ACK/NAK"
						" reponse over the socket from the host.", fn);
				sCloseSocket();
				if (socket_trace) {
					fprintf(socket_trace_fp, "%s - Error reading ACK/NAK packet.  Closed socket.\n", sGetTimeString());
				}
				return srvResults(eVAR_SOCK_ERROR_RECEIVING_RESPONSE, NULL);
			}
			else {
				current_response_length += len;
				misTrc(T_FLOW, "%s - Sucesfully read a part of the response string from the socket.", fn);
				if (socket_trace) {
					fprintf(socket_trace_fp, "%s - socket read successful. Current Ack packet is (%s)\n",
							sGetTimeString(), sGetPrintablePacket(response_string, current_response_length));
				}
			}

			misTrc(T_FLOW, "%s - Check to see if the response if enough and if it is an ack.", fn);

			/* Check to see if the response if enough and if it is an ack */
			isAck = sIsItAck(message_string, message_length, response_string, current_response_length, check_ack_nak_cmd);
			if (isAck == TRUE) {
				/* we received a valid ack message */
				misTrc(T_FLOW, "%s - Received an ack for msg. Msg(%s), ack(%s).", fn,
					data_string, response_string);
				read_complete_ack = 1;
				fprintf(socket_trace_fp, "%s - Ack/nak packet read success/complete.\n", sGetTimeString());
			}
			else if (isAck == FALSE) {
				/* TODO */
				/* received a NAK or invalid message.  What to do now?  */
				misTrc(T_FLOW, "%s - Received a NAK from host while trying to send message(%s)", fn, data_string);
				misTrc(T_FLOW, "%s - Closing socket connection. Socket fd(%i)", fn, sGetSockFd(fd));
				sCloseSocket();
				read_complete_ack = 1;
				if (socket_trace) {
					fprintf(socket_trace_fp, "%s - received a nak from host! Closed socket connection.\n",
							sGetTimeString());
				}

				return srvResults(eVAR_SOCK_RECD_INVALID_RESPONSE, NULL);
			}
			else if (isAck == MAYBE) {
				/* the message that we have readis incomplete.  We need to
				 * read more from socket before we can ascertain if it is an ACK
				 * or a NAK. */
				misTrc(T_FLOW, "%s - we have not yet read sufficient number of"
						" characters to make up a complete ACK/NAK string. Continue reading more.", fn);
				if (socket_trace) {
					fprintf(socket_trace_fp, "%s - We have not read a complete ACK/NAK packet yet."
							" Continue reading more.\n", sGetTimeString());
				}
			}
		}
	}

    return srvResults(eOK, 
		      NULL);
}

/* if this function returns only in two cases:
 * - if timout was specified as 0
 * - a sucessful socket connections is made.
 */
static long sConnectLoop(SOCKET_FD *fd, const char *host, long port, long timeout)
{
	const char *const fn = "sConnectLoop";
	long retVal = !eOK;
	long attempts = 0;
	char buffer[1024];

	/* no matter what is the specified timeout value, we have to try connect'ing once! */
	retVal =  osTCPConnect(fd, (char *)host, (short)port);
	if (retVal != eOK)
	{
		sprintf(buffer, "osSockConnect: Cannot connect: "
			"errno(%ld), error(%s).", osSockErrno(), osSockError());
		misTrc(T_FLOW, "%s - %s", fn, buffer);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
		}
	}
	else /* retVal == eOK */
	{
		sprintf(buffer, "Successfully connected to host(%s) on port(%i) fd(%i)", host, (int)port, sGetSockFd(*fd));
		misTrc(T_FLOW, "%s - %s", fn, buffer);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
		}
		return retVal;
	}
	if (timeout == 0)
	{
		sprintf(buffer, "FIRST attempt to connect to host(%s) on port(%i) failed!", host, (int)port);
		misTrc(T_FLOW, "%s - %s", fn, buffer);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
		}

		sprintf(buffer, "Timout value (%ld) is specified as 0, i.e. try to connect"
				" only once and return status back to caller.", timeout);
		misTrc(T_FLOW, "%s - %s", fn, buffer);
		if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
		}

		return retVal;
	}

	/* we will reach here if and ONLY if:
	 * - first connection attempt failed.
	 * - reconnect timeout was specified as more than 0.
	 */
	sprintf(buffer, "Connect failed!  Will continue to retry connection"
		" after every %ld seconds.", timeout);
	misTrc(T_FLOW, "%s - %s", fn, buffer);
	if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
	}

	while (retVal != eOK)
	{
		/* Sleep for timout seconds before retrying to connect. */
		osSleep(timeout, 0);
		attempts++;

		retVal = osTCPConnect(fd, (char *)host, (short)port);
	}

	sprintf(buffer, "Scoket Connention succeded after %ld attempts", attempts);
	misTrc(T_FLOW, "%s - %s", fn, buffer);
	if (socket_trace) {
			fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
	}

	sprintf(buffer, "Successfully connected to host(%s) on port(%i) fd(%i)", host, (int)port, sGetSockFd(*fd));
	misTrc(T_FLOW, "%s - %s", fn, buffer);
	if (socket_trace) {
		fprintf(socket_trace_fp, "%s - %s\n", sGetTimeString(), buffer);
	}

	return retVal;
}

static char *sReadMessage(char *str)
{
	static n = 0;
	static char *s[] = {
		"Hi There",
		"Hi Here",
		"Msg # 3",
		""
	};

	return strcpy(str, s[n++]);
}

/* This function taks in the input parameters of
 * - name of the server command to be used for getting the marshalling information
 * - user data string that has to marshalled into packet
 * the output parameters are:
 * - the protocol packet, i.e. message string
 * - minimum size of the response string to expect back from the receiving system.
 * It returns:
 * - the status of calling the server command, eOK indicates sucess
 *   in case of an error message and min_response_len are left untouched. */
static long sFormatMsg(const char *cmd_name, const char *data, long data_len,
						char *message, long *p_message_len, long *min_ack_nak_len)
{
	const char *const fn = "sFormatMsg";
	/* Buffer has to store the data length in it and has some other stuff. */
	char buffer[DATA_STRING_LEN + 200];
	char tmp_data[DATA_STRING_LEN + 1];
	long tmp_data_len;
	long status = !eOK;

	/* variables for making server call */
	RETURN_STRUCT *StructPtr = 0;
	mocaDataRes *Res = 0;
	mocaDataRow *Row = 0;

	/* log input variable information. */
	misTrc(T_FLOW, "%s - input received: cmd_name(%s), data(%s) data_len(%ld)", fn,
			cmd_name, data, data_len);

	/* if the data to be sent out has single quotes in it then this moca command would start to bomb.  So mask
	 * the single quotes. */
	misTrc(T_FLOW, "%s - Making sure that any single quotes in the data string are taken care of.", fn);
	ZAP(tmp_data);
	tmp_data_len = sMaskQuotes(data, data_len, tmp_data, sizeof(tmp_data));
	if (tmp_data_len < data_len) {
		misTrc(T_FLOW, "%s - error trying to mask embedded single quotes.", fn);
	}

	/* generate the command string to be executed. */
	sprintf(buffer, "%s where data_string = '%s' and data_length = %ld", cmd_name, tmp_data, data_len);
	/* Note that here we must _NOT_ pass in tmp_data_len.  Even though we have asked for single quotes
	 * to be masked -- which will potentially make the length of tmp_data to be more than data. But still
	 * we MUST pass in data_len to this command because before the data_string reaches the sever command
	 * this extra quote would have been taken out by the moca command parser. */

	status = srvInitiateCommand(buffer, &StructPtr);
	if (status != eOK)
	{
		misTrc(T_FLOW, "%s - Error executing command.  Returning false.", fn);
	}
	else
	{
		char *prefix; /* we do not know the length of the prefix or suffix yet */
		long prefix_len;
		char *suffix;
		long suffix_len;

		/* command was sucessful */
		Res = StructPtr->ReturnedData;
		Row = sqlGetRow(Res);

		/* get the values of prefix, suffix and minimum ack/nak response length. */
		/* first we have to get the prefix length. */
		prefix_len = sqlGetLong(Res, Row, "prefix_length");
		/* now allocate enough space so that we can hold the prefix */
		prefix = (char *)malloc(prefix_len * sizeof(char) + 1);
		/* copy the prefix into place. strcpy ensures that the string would be terminated. */
		strcpy(prefix, sqlGetString(Res, Row, "prefix"));
		/* convert and masked NULLs back into NULLs */
		sChagneIntoOriginalString(prefix, prefix_len);

		/* similarly get the suffix and suffix_len */
		suffix_len = sqlGetLong(Res, Row, "suffix_length");
		suffix = (char *)malloc(suffix_len * sizeof(char) + 1);
		strcpy(suffix, sqlGetString(Res, Row, "suffix"));
		sChagneIntoOriginalString(suffix, suffix_len);

		*min_ack_nak_len = sqlGetLong(Res, Row, "min_ack_nak_len");

		misTrc(T_FLOW, "%s - return values recd from cmd(%s): prefix(%s), prefix_length(%ld),"
				"suffix(%s), suffix_length(%ld), min_ack_nak_len(%ld)", fn, cmd_name,
				prefix, prefix_len, suffix, suffix_len, *min_ack_nak_len);

		/* build the data packet. */
		memcpy(message, prefix, prefix_len);
		memcpy(message + prefix_len, data, data_len);
		memcpy(message + prefix_len + data_len, suffix, suffix_len);

		*p_message_len = prefix_len + data_len + suffix_len;
		/* terminate the string anyway. */
		message[*p_message_len] = 0;

		/* free the allocated storage */
		free(prefix); free(suffix);
	}

	/* free the memory held by ret struct */
	srvFreeMemory(SRVRET_STRUCT, StructPtr);
	StructPtr = 0;
	Res = 0;

	return status;
}

/* function taks the mesage string, the ack reccived till now, and the
 * name of a server command that can be used to check if the ack is good
 * enough for the passed message.  returns
 *  0 (FALSE) if it is a NAK
 *  1 (TRUE) if it is a ACK
 * -1 (MAYBE) if more characters should be read before ACK/NAK is fully read from socket.
 */
static int sIsItAck(const char *message, long message_len, const char *response,
		long response_len, const char *cmd)
{
	const char *const fn = "SocketOutbound::sIsItAck";
	char buffer[MESSAGE_STRING_LEN + RESPONSE_STRING_LEN + 200];
	char tmp_message [ MESSAGE_STRING_LEN + 1 ];
	long tmp_message_len;
	char tmp_response [ RESPONSE_STRING_LEN + 1 ];
	long tmp_response_len;
	long retVal = 0;
	long status = !eOK;
	/* variables for making server call */
	RETURN_STRUCT *StructPtr = 0;
	mocaDataRes *Res = 0;
	mocaDataRow *Row = 0;

	misTrc(T_FLOW, "%s - Make sure that any embedded single quotes in the string have been taken care of.", fn);
	ZAP(tmp_message);
	tmp_message_len = sMaskQuotes(message, message_len, tmp_message, sizeof(tmp_message));
	if (tmp_message_len < message_len) {
		misTrc(T_FLOW, "%s - error trying to mask embedded single quotes in message.", fn);
	}

	ZAP(tmp_response);
	tmp_response_len = sMaskQuotes(response, response_len, tmp_response, sizeof(tmp_response));
	if (tmp_response_len < response_len) {
		misTrc(T_FLOW, "%s - error trying to mask embedded single quotes in response.", fn);
	}

	sprintf(buffer, "%s where message_string = '%s' and message_length = %ld"
					"     and response_string = '%s' and response_length = %ld",
					cmd, tmp_message, message_len, tmp_response, response_len);

	status = srvInitiateCommand(buffer, &StructPtr);
	if (status != eOK)
	{
		misTrc(T_FLOW, "%s - Error executing command.  Returning false.", fn);
		retVal = FALSE; /* it's a NAK */
	}
	else
	{
		/* command was sucessful */
		Res = StructPtr->ReturnedData;
		Row = sqlGetRow(Res);

		/* before checking if it is an ack we need to check if we have
		 * gotten all of ACK or not!  And then go back and read more for nak */
		if (sqlGetLong(Res, Row, "read_more") == 1)
		{
			retVal = MAYBE; /* more chars should be read before we can conclude */
		}
		else if (sqlGetLong(Res, Row, "ack") == 1)
		{
			/* YES! it was an ACK that we received. */
			retVal = TRUE; /* its and ACK */
		}
		else
		{
			/* The ack that we received was either a nak or did not match
			 * up with the message or some other problem happened with it and
			 * the cmd did not like it. */
			retVal = FALSE; /* Its a NAK */
		}
	}

	/* free the memory held by ret struct */
	srvFreeMemory(SRVRET_STRUCT, StructPtr);
	StructPtr = 0;
	Res = 0;

	return retVal;
}

static void sCloseSocket (void)
{
	sClose(fd);
	fd = -1;
	ZAP(connected_hostid);
	connected_portnum = 0;
}

static void sAtExit (void)
{
	fclose(socket_trace_fp);
	sCloseSocket();
}
