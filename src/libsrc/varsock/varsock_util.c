static char RCS_Id[] = "$Id: varsock_util.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_util.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-component.  Utility functions for socket programmming.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>
#include <oslib.h>
#include <mislib.h>
#include <mocaerr.h>
#include <time.h>
#include <ctype.h>

#include <varsock_util.h>
#include <varsock_colwid.h>

/* this function returns a inter number that corresponds to the
 * underlying socket descriptor.  For unix sustems it is the 
 * SOCKET_FD itself as for Unix SOCKET_FD is a typedf for int!
 */
int sGetSockFd(const SOCKET_FD fd)
{
	return (int) fd;
}

/* This function accepts a socket_Fd and closes that socket.
 * it returns the status of close operation to indicate if 
 * close was sucessful or not.
 */
long sClose (SOCKET_FD fd)
{
	const char *const fn = "sClose";
	/* scratch variable */
	long retVal = !eOK;

	misTrc(T_FLOW, "%s - Closing socket connection.  Socket is (%i).", fn, sGetSockFd(fd));

	retVal = osSockClose(fd);
	if (retVal != eOK)
	{
		misTrc(T_FLOW, "%s - Error closing socket connection, retVal(%ld)!", fn, retVal);
		misTrc(T_FLOW, "%s - osSockClosr: Cannot close: errno(%ld), error(%s).", fn,
				osSockErrno(), osSockError());
	}
	else
	{
		misTrc(T_FLOW, "%s - Socket connection sucessfully closed!", fn);
	}

	return retVal;
}

/* This function taks a socket descriptor and a string and sends out
 * the string over the socket.  It returns eOK if the entire string was
 * sent out sucessfully, else it returns the error.
 */
long sSend(SOCKET_FD fd, const char *s, int len)
{
	const char *const fn = "sSend";
	int iAlreadySent = 0;

	misTrc(T_FLOW, "%s - input parameters: fd(%i), message(%s), message length(%i)", fn,
			(int)sGetSockFd(fd), s, len);
	while (iAlreadySent < len)
	{
		long retVal = osSockSend(fd, (char *)(s + iAlreadySent), (len -  iAlreadySent), 0);
		misTrc(T_FLOW, "%s - osSockSend retVal(%ld)", fn, retVal);
		/* it should write at least one byte! If not then we assume that socket was
		 * shutdown. */
		if (retVal > 0)
		{
			/* retVal contains the number of bytes actauly sent. */
			iAlreadySent += retVal;
		}
		else
		{
			/* TODO */
			/* There was error sending mesage to the server, what should the client do now? */
			int err = osSockErrno();

			misTrc(T_FLOW, "%s - Error writing to socket (%i)", fn, sGetSockFd(fd));

			misTrc(T_FLOW, "%s - Error returned: osSockSend: errno(%ld), errmsg(%s)", fn,
				err, osSockError());
			return !eOK;
		}
	}
	misTrc(T_FLOW, "%s - sucessfully sent msg(%s) of length (%i)", fn, s, iAlreadySent);

	return eOK;
}

/* The function takes a socket descriptor and a count of number of chars
 * to read from that socket and returns the read characters in the string
 * pointed to by response.  It is assumed that response has enough space to hold the 
 * incoming message.  It returns the number of bytes read.  If an error is
 * encountered while reading then it returns -1. */
long sReceive(SOCKET_FD fd, int expected_length, char *response, int buffer_size)
{
	const char *const fn = "sReceive";
	int iAlreadyRecvd = 0;
	int inError = 0;

	misTrc(T_FLOW, "%s - input parameters: fd(%i), expected length(%i),"
		" response buffer size(%ld)", fn, (int)sGetSockFd(fd), expected_length, buffer_size);

	if (expected_length <= 0) {
		misTrc(T_FLOW, "%s - invalid parameter expected_length(%i). Valid value"
			" is more than 0. Returning error -1.", fn, expected_length);
		return -1;
	}

	while (iAlreadyRecvd < expected_length)
	{
		long retVal = osSockRecv(fd, response + iAlreadyRecvd, expected_length, 0);
		misTrc(T_FLOW, "%s - retVal(%ld)", fn, retVal);
		/* osSockRecv() returns the return value of recv(2), which returns the numbers of
		 * bytes sent, or 0 if connection was gracefully closed or -1 to indicate an error. */
		if (retVal > 0)
		{
			/* retVal contains the number of chars actually read from the stream. */
			iAlreadyRecvd += retVal;
		}
		else
		{
			/* TODO */
			/* There was error reading from the socket.  What to do? */
			/* there was an error encountered while trying to read from socket. */
			int err = osSockErrno();

			misTrc(T_FLOW, "%s - Error reading from socket %i", fn, sGetSockFd(fd));
			
			misTrc(T_FLOW, "%s - Error returned: osSockRecv: errno (%ld), errmsg(%s)", fn,
				err, osSockError());
			/* we do not want to read any more. Break out of the loop. */
			inError = 1;
			break; 
		}
	}
	/* terminate the string with a NULL.  Note that there may be other nulls
	 * inside of the string.  So this NULL termination is to be relied on
	 * with caution, i.e. if there is a gurantee that a NULL won't come over
	 * the socket ever.  In other cases rely on the len being passed in. */

	*(response + iAlreadyRecvd) = 0;

	misTrc(T_FLOW, "%s - Recvd message (%s) from socket (%i)", fn, response, sGetSockFd(fd));
	if (inError) {
		return -1;
	}
	else {
		return iAlreadyRecvd;
	}
}

long sAccept(SOCKET_FD fd, SOCKET_FD *p_new_fd)
{
	const char *const fn = "sAccept";
	long retVal = !eOK;

	misTrc(T_FLOW, "%s - input parameters: listner(%i)", fn, sGetSockFd(fd));

	retVal = osSockAccept(fd, p_new_fd, 0);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - Error accepting new socket connection, retVal(%ld)!", fn, retVal);
		misTrc(T_FLOW, "%s - osSockAccept: Cannot accept: errno(%ld), error(%s).", fn,
				osSockErrno(), osSockError());
	}
	else {
		/* accept was sucessful */
		misTrc(T_FLOW, "%s - newly accepted socket fd (%i)", fn, sGetSockFd(*p_new_fd));
	}	

	return retVal;
}

char *sMaskNullsInString(char *s, long size)
{
	int i;
	for (i = 0; i < size; i++) {
		if (s[i] == 0) {
			s[i] = 127;
		}
	}

	return s;
}

char *sChagneIntoOriginalString(char *s, long size)
{
	int i;
	for (i = 0; i < size; i++) {
		if (s[i] == 127) {
			s[i] = 0;
		}
	}

	return s;
}

long sMaskQuotes(const char *s_in, long s_in_len, char *s_out, long s_out_len)
{
	int i, j;
	for(i = 0, j = 0; i < s_in_len && j < s_out_len; i++, j++) {
		s_out[j] = s_in[i]; /* copy one character at a time */
		if (s_in[i] == '\'') { /* check if the character that we just copied was a single quote.*/
			s_out[++j] = '\'';/* add one more quote to the out string. */
		}
	}

	return j;
}

const char *sGetTimeString(void)
{
	static char str[sizeof("mm/dd/yyyy HH:MM:SS")];
	time_t tod;

	time(&tod);
	strftime(str, sizeof(str), "%m/%i/%Y %H:%M:%S", localtime(&tod));

	return str;
}

const char *sGetPrintablePacket(const char *s, long len)
{
	static char buffer[MESSAGE_STRING_LEN + 1];
	int i, j;
	const char *const fn = "sGetPrintablePacket";
	
	for (i = 0, j = 0, *buffer = 0; i < len; i++) {
		if(isprint(s[i])) {
			buffer[j++] = s[i];
		}
		else if (s[i] == 0) {
			/* it is a null */
			j += sprintf(buffer + j, "<NULL>");
		}
		else if (s[i] == 2) {
			/* it is an STX */
			j += sprintf(buffer + j, "<STX>");
		}
		else if (s[i] == 3) {
			/* it is an ETX */
			j += sprintf(buffer + j, "<ETX>");
		}
		else if (s[i] == 6) {
			/* it is an ACK */
			j += sprintf(buffer + j, "<ACK>");
		}
		else if (s[i] == 21) {
			/* it is an NAK */
			j += sprintf(buffer + j, "<NAK>");
		}
		else {
			/* it is some other control character. */
			j += sprintf(buffer + j, "<%0#4x>", s[i]);
		}
	}
	*(buffer + j) = 0;

	return buffer;
}

