// Include standard RF definitions

#include "rf_format.h"

// Text & Message definitions
#define FORM_HELPTEXT           hlpUndirUnpick,      "Enter Inventory Identifier"
#define ERR_INV_LOC             "errInvalidLoc",       "Invalid Location"
#define ERR_INV_INVTID          "errInvalidInvtid",    "invalid inventory identifier"
#define ERR_ID_NOT_PICKED	"errIdNotPicked",      "identifier not picked"
#define ERR_INV_CODVAL          "errInvalidCancod",    "invalid cancel code value"

#define STS_ID_UNPICKED 	"stsIdentifierUnpicked", "id was unpicked"

#define DLG_OK_TO_UNPICK	"dlgOkToUnpick",       "ok to unpick"

//Raman.P, MR 2810 
#define ERR_INV_ARECOD          "errInvalidArecod",   "identifier in ship location"

// Field & Label positioning definitions

#define LABEL1_CAPTION       ttlUndirUnpick,	"Unpick"
#define LABEL1_TAG           ttlUndirUnpick

#define LABEL2_CAPTION       invtid, "id:"
#define LABEL2_TAG           invtid 
#define LABEL2_LENGTH        5 

#define LABEL3_CAPTION      lblCancelCode, "cancel code:"
#define LABEL3_TAG          lblCancelCode
#define LABEL3_LENGTH       18

#define CODVAL_TAG          codval

#ifdef RF_FORMAT_40X8

#define LABEL1_TOP           0
#define LABEL1_LEFT          13

#define LABEL2_TOP           2
#define LABEL2_LEFT          14 
#define LABEL2_JUSTIFY       right

#define INVTID_TOP           2
#define INVTID_LEFT          20 

#define LABEL3_TOP          4
#define LABEL3_LEFT         1
#define LABEL3_JUSTIFY      right

#define CODVAL_TOP          4
#define CODVAL_LEFT         20

#endif

#ifdef RF_FORMAT_20X16

#define LABEL1_TOP           0
#define LABEL1_LEFT          1

#define LABEL2_TOP           2
#define LABEL2_LEFT          1
#define LABEL2_JUSTIFY       left

#define INVTID_TOP           3
#define INVTID_LEFT          1

#define LABEL3_TOP          4
#define LABEL3_LEFT         1
#define LABEL3_JUSTIFY       left

#define CODVAL_TOP          5
#define CODVAL_LEFT         1

#endif

// Form definition

Begin Form UNDIR_UNPICK
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  FORM_HELPTEXT
    
    // Function key definitions
    Begin Fkey FKEY_BACK
        Caption   FKEY_BACK_CAPTION
        Begin Action 

	    //Don't let the user back out if there is inventory on his RF device.
            ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod']")
	    Begin Result !eOK		//No records returned, nothing on RDT
	        Set(inprog, 0)
                Back()
	    End
	    Begin Result eOK		//There's inventory on RF, force deposit
                Set (INIT_POLICIES.prvfrm,"UNDIR_UNPICK")
                ExecuteForm ("PUTAWAY_LOAD")

	    End
        End
    End

    Begin Fkey FKEY_DONE
        Caption FKEY_DONE_CAPTION
	Begin Action
	    //Don't let the user back out if there is inventory on his RF device.
            ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod']")
	    Begin Result !eOK		//No records returned, nothing on RDT
	        Set(inprog, 0)
	    End
	    Begin Result eOK		//There's inventory on RF, force deposit
                Set (INIT_POLICIES.prvfrm,"UNDIR_UNPICK")
                ExecuteForm ("PUTAWAY_LOAD")
	    End
	End
    End
   
    Begin Fkey FKEY_TOOL
        Caption FKEY_TOOL_CAPTION
        Begin Action
            CallForm ("TOOLS_MENU")
        End
    End 

    Begin Fkey FKEY_HELP
        Caption   FKEY_HELP_CAPTION
        Begin Action 
            DisplayHelp ()
        End
    End
   
    // Label definitions

    Begin Label LABEL1
        Tag       LABEL1_TAG 
        Caption   LABEL1_CAPTION
        Left      LABEL1_LEFT
        Top       LABEL1_TOP
        Height    RF_FIELD_HEIGHT
    End     // End Label LABEL1
    
    Begin Label LABEL2
        Tag       LABEL2_TAG 
        Caption   LABEL2_CAPTION
        Left      LABEL2_LEFT
        Top       LABEL2_TOP
        Width     LABEL2_LENGTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL2_JUSTIFY
    End     // End Label LABEL2
    
    Begin Label LABEL3
        Tag       LABEL3_TAG 
        Caption   LABEL3_CAPTION
        Left      LABEL3_LEFT
        Top       LABEL3_TOP
        Width     LABEL3_LENGTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL3_JUSTIFY
    End     // End Label LABEL3
    
    // Local field definitions

    Begin Field stoloc 
      #include "local_string.h"
      Width     STOLOC_LEN
    End       // End Field

    Begin Field lodnum
      #include "local_string.h"
      Width     LODNUM_LEN
    End       // End Field
    
    Begin Field subnum
      #include "local_string.h"
      Width     SUBNUM_LEN
    End       // End Field

    Begin Field dtlnum
      #include "local_string.h"
      Width     DTLNUM_LEN
    End       // End Field

    Begin Field colnam
      #include "local_string.h"
      Width     COLNAM_LEN
    End       // End Field

    Begin Field junk
      #include "local_string.h"
      Width     40
    End       // End Field
    
    Begin Field inprog 
      #include "local_integer.h"
      Width     RF_QUANTITY_LEN 
    End       // End Field
    
    Begin Field codval
        Tag             CODVAL_TAG
        Left            CODVAL_LEFT
        Top             CODVAL_TOP
        Width           CODVAL_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        2
        DisplayMask     0
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

        Begin Action ExitAction

            // Validate pick cancel code value

            ExecuteDSQL ("[ select codval from cancod where rftflg=1 and codval='@codval' ]")
            Begin Result !eOK
                Beep (3)
                DisplayMessage (ERR_INV_CODVAL)
                Reenter ()
            End

            Length (invtid)
	    Begin Result 1

                // Prompt user for confirmation to continue
         

                PromptYN (DLG_OK_TO_UNPICK)
                Begin Result 0
                    ExecuteForm ("UNDIR_UNPICK")
                End

                //Added fix to prevent users from unpicking if load is on a trailer,
                // Raman.P, MR 2810

                Verify(colnam, "lodnum")
		Begin Result 1
                    ExecuteDSQL("list inventory where lodnum = '@lodnum' and arecod = 'SHIP' ")
                End
		Verify(colnam, "subnum")
		Begin Result 1
                    ExecuteDSQL("list inventory where subnum = '@lodnum' and arecod = 'SHIP' ")
		End
		Verify(colnam, "dtlnum")
		Begin Result 1
                    ExecuteDSQL("list inventory where dtlnum = '@lodnum' and arecod = 'SHIP' ")
		End
		GetResults()
                Begin Result eOK
		    Beep(3)
		    DisplayMessage(ERR_INV_ARECOD)
		    Reenter()
                End
               
                //End MR 2810, Raman P         

                //OK, nove the inventory.  We're going to move the inventory to
                //the PERM-SHP-LOC location, and then back to the RFT.  By moving
                //it to the adjustment location, it will force any triggers to fire
                //for unpicking inventory.

                Verify(colnam, "lodnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where lodnum = '@lodnum' and dstloc = 'PERM-SHP-LOC' and cancod = '@codval' and spcind = 'PRA-REVERSE' ")
                End
		Verify(colnam, "subnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where subnum = '@subnum' and dstloc = 'PERM-SHP-LOC' and cancod = '@codval' and spcind = 'PRA-REVERSE' ")
		End
		Verify(colnam, "dtlnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where dtlnum = '@dtlnum' and dstloc = 'PERM-SHP-LOC' and cancod = '@codval' and spcind = 'PRA-REVERSE' ")
		End
		GetResults()
                Begin Result !eOK
		    Beep(3)
		    DisplayMessageText()
		    Reenter()
                End

                //Now, we have to move the lpn back to the RF, because that's where
                //it really is...

                Verify (colnam, "lodnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where lodnum = '@lodnum' and dstloc = '@global.devcod' ")
                End
		Verify (colnam, "subnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where subnum = '@subnum' and dstloc = '@global.devcod' ")
		End
		Verify (colnam, "dtlnum")
		Begin Result 1
                    ExecuteDSQL("move inventory where dtlnum = '@dtlnum' and dstloc = '@global.devcod' ")
		End
		GetResults()
                Begin Result !eOK
		    Beep(3)
		    DisplayMessageText()
		    ExecuteForm("UNDIR_UNPICK")
                End
                
		GetResponse(STS_ID_UNPICKED)
                ResetMessageLine()
		ClearField("invtid")
            End
        End // End Field codval ExitAction

    End // End Field codval

    Begin Field invtid 
        Tag         INVTID_TAG
        Left        INVTID_LEFT 
        Top         INVTID_TOP 
        Width       INVTID_LEN
        Height      RF_FIELD_HEIGHT
        Datatype    String
        TabOrder    1
        DisplayMask 0
        DefaultData 0
        EntryMask   ENTRY_MASK_INPUT_REQUIRED
        
        Begin Action ExitAction

            Length (invtid)
            Begin Result 1
	        //Find out what the user really scanned...

	        ExecuteDSQL("get translated inventory identifier where id = '@invtid' into :colnam, :junk, :junk, :stoloc, :lodnum, :junk, :subnum, :junk, :dtlnum, :junk, :junk ")
	        GetResults()
	        Begin Result !eOK
	            Beep(3)
		    GetResponse(ERR_INV_INVTID)
	            Reenter()
                End 
           
	        //User may only enter load/sublod/dtl, not a part number.

	        Verify (colnam, "subucc")
                Begin Result 1
                    Set (colnam, "subnum")
                End

                Verify (colnam, "dtlucc")
                Begin Result 1
                    Set (colnam, "dtlnum")
                End

                Verify (colnam, "lodnum")
                Begin Result 0
                    Verify (colnam, "subnum")
                    Begin Result 0
                        Verify (colnam, "dtlnum")
                        Begin Result 0
                            Beep (3)
                            GetResponse (ERR_INV_INVTID)
                            Reenter()
                        End
                    End
                End
	       
               // Update RF location to the location of the scnanned load.

                ExecuteDSQL ("change rf device where devcod = '@global.devcod' and curstoloc = '@stoloc' ")

                //Make sure the id scanned is acutally part of picked inventory.
		ExecuteDSQL("list inventory where lodnum = '@lodnum' and ship_line_id is null")
		GetResults()
		Begin Result eOK
		    Beep(3)
		    GetResponse(ERR_ID_NOT_PICKED) 
		    Reenter()
		End
       
            End
        End // End Field invtid ExitAction
    End

    Begin Action EntryAction
        ClearForm()
	Set(inprog, 1)
       
       // Retrieve default pick work entry cancel code value

        ExecuteDSQL ("[select codval into :codval from cancod where rftflg=1 and defflg=1]")
        GetResults ()

    End

    Begin Action ExitAction
        ClearForm()
    End

End     // End of Form UNDIR_UNPICK
