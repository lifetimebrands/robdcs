/* Include standard RF definitions */

#include "rf_format.h"
#include "rf_err.h"

/* Text & Message definitions */

#define CART_NEXT  "stsCartNext",     "ANY CART"
#define INV_MOVED  "stsInvMoved",     "Inventory Moved"
#define CART_MOVED  "stsCartMoved",     "Cart Moved"
#define CART_PRINTED  "stsCartPrinted",     "Cart Already Printed"
#define ERR_INV_CART  "errInvalidcart",     "Invalid CartID"
#define ERR_INV_LOC  "errInvalidcart",     "Invalid Location"

#define ERR_INV_PRTADR  "errInvalidPrtadr",     "invalid printer address"

#define ERR_PRINTING  "errPrinting",     "Error Printing"

#define CONFIRM_PRINT  "ConfirmPrint",     "Printed Ticket, Scan Next"

/* Field & Label positioning definitions */

#define LABEL1_CAPTION          ttlLoadCart, "Pack Movement"
#define LABEL1_TAG              ttlLoadCart
#define LABEL1_LEFT             RF_TITLE_LEFT
#define LABEL1_TOP              RF_TITLE_TOP
#define LABEL1_WIDTH            RF_TITLE_WIDTH

#ifdef RF_FORMAT_40X8
#    define LABEL10_CAPTION      mydevcod, "Station:"
#    define LABEL10_TAG          devcod 
#    define LABEL10_LEFT         1
#    define LABEL10_TOP          1
#    define LABEL10_WIDTH        21
#    define LABEL10_JUSTIFY      Right 
#    define LABEL2_CAPTION      fromid, "Cart ID:"
#    define LABEL2_TAG          fromid 
#    define LABEL2_LEFT         1
#    define LABEL2_TOP          2
#    define LABEL2_WIDTH        21
#    define LABEL2_JUSTIFY      Right 
#    define LABEL3_CAPTION      toid,  "To Location:"
#    define LABEL3_TAG          toid 
#    define LABEL3_LEFT         1
#    define LABEL3_TOP          3
#    define LABEL3_WIDTH        21
#    define LABEL3_JUSTIFY      Right

#    define DEVCOD_TAG          devcod 
#    define DEVCOD_LEFT         23
#    define DEVCOD_TOP         1 
#    define FROMID_TAG         fromid 
#    define FROMID_LEFT         23
#    define FROMID_TOP          2
#    define TOID_TAG          toid 
#    define TOID_LEFT         23
#    define TOID_TOP          3 


#endif

#ifdef RF_FORMAT_20X16
#    define LABEL10_CAPTION      mydevcod, "Station:"
#    define LABEL10_TAG          devcod 
#    define LABEL10_LEFT         1
#    define LABEL10_TOP          1
#    define LABEL10_WIDTH        RF_TERMINAL_WIDTH 
#    define LABEL10_JUSTIFY      Left

#    define LABEL2_CAPTION      fromid, "Cart ID:"
#    define LABEL2_TAG          fromid
#    define LABEL2_LEFT         1
#    define LABEL2_TOP          3
#    define LABEL2_WIDTH        RF_TERMINAL_WIDTH
#    define LABEL2_JUSTIFY      Left

#    define LABEL3_CAPTION      toid, "To Location:"
#    define LABEL3_TAG          toid 
#    define LABEL3_LEFT         1
#    define LABEL3_TOP          5
#    define LABEL3_WIDTH        RF_TERMINAL_WIDTH
#    define LABEL3_JUSTIFY      Left

#    define DEVCOD_TAG         devcod 
#    define DEVCOD_LEFT        4
#    define DEVCOD_TOP         2 

#    define FROMID_TAG         fromid
#    define FROMID_LEFT        4
#    define FROMID_TOP         4 

#    define TOID_TAG          toid 
#    define TOID_LEFT         4
#    define TOID_TOP          6


#endif

/* Form definition */
Begin Form PACK_MOVEMENTS 
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  HELP_LABELLOAD

    Begin Fkey FKEY_BACK
        Caption   FKEY_BACK_CAPTION
        Begin Action
            Back()
        End
    End

    Begin Fkey FKEY_HELP
        Caption FKEY_HELP_CAPTION
        Begin Action
            DisplayHelp ()
        End 
    End


    /* Label definitions */

    Begin Label LABEL1
        Caption   LABEL1_CAPTION
        Tag       LABEL1_TAG
        Left      LABEL1_LEFT
        Top       LABEL1_TOP
        Width     LABEL1_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   Center
    End 


    Begin Label LABEL10
        Caption   LABEL10_CAPTION
        Tag       LABEL10_TAG 
        Left      LABEL10_LEFT
        Top       LABEL10_TOP
        Width     LABEL10_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL2_JUSTIFY 
    End 

    Begin Label LABEL2
        Caption   LABEL2_CAPTION
        Tag       LABEL2_TAG 
        Left      LABEL2_LEFT
        Top       LABEL2_TOP
        Width     LABEL2_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL2_JUSTIFY 
    End 

    Begin Label LABEL3
        Caption   LABEL3_CAPTION
        Tag       LABEL3_TAG 
        Left      LABEL3_LEFT
        Top       LABEL3_TOP
        Width     LABEL3_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL3_JUSTIFY 
    End 

    /* Timer definitions */

    Begin Timer RF_SHORT_TIMER_LENGTH_IN_SECONDS
        Begin Action
        Length(fromid)
          Begin Result 0
	      ExecuteForm ("PACK_MOVEMENTS")	
          End
        End
    End


    /* Local field definitions */
    
    Begin Field return_to_labeling
#        include "local_integer.h"
        Width    RF_FLAG_LEN
    End

    Begin Field junk
      #include "local_string.h"
      Width     40
    End       // End Field


    Begin Field print_msg
      #include "local_string.h"
      Width     40
    End       // End Field

    Begin Field wrkref
        #include "local_string.h"
        Width    WRKREF_LEN
    End       // End Field

    Begin Field printed_flag 
        #include "local_integer.h"
        Width    RF_FLAG_LEN
    End       // End Field
 
    Begin Field devcod 
	Tag             DEVCOD_TAG 
	Left            DEVCOD_LEFT 
	Top             DEVCOD_TOP 
	Width           STOLOC_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
        
	Begin Action ExitAction
	     	ExecuteDSQL ("[select aisle_id from locmst where stoloc = '@devcod'] ")
	     	  Begin Result !0
		    Beep(3)
		    DisplayMessage(ERR_INV_LOC)
		    ReEnter()
	     	  End
	     	  Begin Result 0
			SetFieldAttr ("devcod", ENTRY_MASK_PROTECTED) 		   
	     	  End
        End 
    End 
 
    Begin Field fromid 
	Tag             FROMID_TAG 
	Left            FROMID_LEFT 
	Top             FROMID_TOP 
	Width           STOLOC_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

	Begin Action EntryAction
                ExecuteDSQL ("get usr next pack cart  into :junk' where devcod = '@devcod'")
                  Begin Result 0
                        GetResults()
                        DisplayMessage ("lblGotNext", junk)
                  End
                  Begin Result !0
                    DisplayMessage(CART_NEXT)
                  End
	End        


	Begin Action ExitAction
	     	ExecuteDSQL ("check usr pack movement where invtid = '@fromid' ")
	     	  Begin Result !0
		    Beep(3)
		    DisplayMessage(ERR_INV_CART)
		    ReEnter()
	     	  End
        End /* End Field cartid ExitAction */

    End 


    Begin Field toid

        Tag             TOID_TAG
        Left            TOID_LEFT
        Top             TOID_TOP
        Width           STOLOC_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

          Begin Action ExitAction

            ExecuteDSQL ("process usr pack movement where fromid = '@fromid' and toid = '@toid'")
            Begin Result !eOK
                Beep (3)
                DisplayMessageText()
                ClearField("toid")
                DisplayField("toid")
                Reenter ()
            End
            Begin Result eOK
		GetResponse(CART_MOVED)
		ExecuteForm ("PACK_MOVEMENTS")	
	    End
         End /* End Field ExitAction */

    End /* End  */

    Begin Action EntryAction

           ClearField("fromid")  
           ClearField("toid")  
            Length (devcod)
            Begin Result 0
                SetFieldAttr ("devcod", ENTRY_MASK_INPUT_REQUIRED)
            End 
	    Begin Result !0
		SetFieldAttr ("devcod", ENTRY_MASK_PROTECTED) 		   
	    End
            ExecuteDSQL ("get usr next pack cart into :junk where devcod = '@devcod'")
                Begin Result 0
                       GetResults()
                       DisplayMessage ("lblGotNext", junk)
               End
               Begin Result !0
                 DisplayMessage(CART_NEXT)
               End
  
	

    End /* End of form EntryAction */

End /* End of form */

