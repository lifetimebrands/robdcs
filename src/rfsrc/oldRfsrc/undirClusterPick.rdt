// This form started as a copy of standard undirCaseConfirm.rdt,
// and was modified from there. The directed work functionality
// was not preserved.  This form is intended to be run in
// undirected mode.  Both cartons and cases can be added to the
// batch and picked. 
//----------------------------------------------------------------

// Include standard RF definitions

#include "rf_format.h"

// Text & Message definitions
#define FORM_HELPTEXT             "hlpUndirClusterPick",      "scan carton"
#define ERR_INV_CARTON            "errInvalidCarton",         "invalid carton"
#define ERR_CARTON_NOT_REL	  "errCartonPickNotReleased", "carton pick not released"
#define ERR_CARTON_COMPLETE       "errCartonPickIsComplete",  "carton is complete"
#define ERR_NO_PICKS_PENDING 	  "stsNoPicksPending",        "no picks pending"
#define ERR_CARTON_IN_PROCESS	  "errCartonInProcess",       "carton in process"
#define ERR_CARTON_INCOMPLETE     "errCartonIncomplete",      "incomplete carton in batch"
#define ERR_CMP_WRKREQ          "errCompleteWrkreq",          "error completing work request"
#define ERR_NO_BATCH              "errNoPicksInBatch",        "no picks in batch"
#define ERR_NOT_AUTHORIZED	  "errClusterPickNotAuthorized", "?Device Not Authorized"
#define ERR_SLOT_TOO_HIGH	  "errSlotTooHigh",	      "?Slot Exceeds Maximum"

#define MSG_BATCH_CMP             "stsBatchComplete",         "batch is complete"
#define ERR_RF_HAS_INV            "errRFHasInventory",        "rf has inventory"
#define MSG_ADDED_TO_BATCH        "stsAddedToBatch",          "added to batch"

//  *** GAP 09.03 ****  start  *****  //


#define ERR_BATCH_FULL		  "errBatchFull",             "this batch is full " 
#define ERR_SLOT_IN_USE           "errSlotInUse",	      " slot is in use " 
                        
//  *** GAP 09.03 ****  end  *****  //


// Field & Label positioning definitions

#ifdef RF_FORMAT_40X8

#define LABEL1_CAPTION       ttlClusterPick,	"build batch"
#define LABEL1_LEFT          13
#define LABEL1_TOP           0

#define LABEL2_CAPTION       lblCaseNum,	"case number:"
#define LABEL2_TOP           3
#define LABEL2_LEFT          3
#define SUBNUM_TOP           3
#define SUBNUM_LEFT          16

//  *** GAP 09.03 ****   start *****  //

#define LABEL3_CAPTION       slot,	"slot number:"
#define LABEL3_TOP           2
#define LABEL3_LEFT          3
#define SLOT_TOP             2
#define SLOT_LEFT            16

//  *** GAP 09.03 ****  end  *****  //

// 07/13/2001 Patrick Pape Start
// Add carton count to the screen
#define LABEL4_CAPTION		batch_count,	"Cases in Batch:"
#define LABEL4_TOP		4
#define LABEL4_LEFT		20
#define BATCH_COUNT_TOP		4
#define BATCH_COUNT_LEFT	38
#define BATCH_COUNT_LEN		3
// 07/13/2001 Patrick Pape Stop

// 07/23/2001 Patrick Pape Start
// Add Carton Code to the screen
#define LABEL5_CAPTION		carton_code,	"Carton Code:"
#define LABEL5_TOP		4
#define LABEL5_LEFT		3
#define CTNCOD_TOP		4
#define CTNCOD_LEFT		16
// 07/23/2001 Patrick Pape Stop

// MSGFLDA & MSGFLDB are used to display messages with run-time information, such as user entries.
// This method allows dynamic messages in rdt.
#define MSGFLDA_LEFT         1
#define MSGFLDA_TOP          5
#define MSGFLDA_LEN          20

#define MSGFLDB_LEFT         22
#define MSGFLDB_TOP          5
#define MSGFLDB_LEN          19

#endif

#ifdef RF_FORMAT_20X16

#define LABEL1_CAPTION       ttlClusterPick,	"build batch"
#define LABEL1_LEFT          8
#define LABEL1_TOP           0

#define LABEL2_CAPTION       lblCaseNum,	"case number:"
#define LABEL2_TOP           3
#define LABEL2_LEFT          1
#define SUBNUM_TOP           4
#define SUBNUM_LEFT          1

//  *** GAP 09.03 ****   start *****  //

#define LABEL3_CAPTION       slot,	"Slot number:"
#define LABEL3_TOP           2
#define LABEL3_LEFT          1
#define SLOT_TOP             2
#define SLOT_LEFT            17

//  *** GAP 09.03 ****  end  *****  //

// 07/13/2001 Patrick Pape Start
// Add carton count to the screen
#define LABEL4_CAPTION		batch_count,	"Cases in Batch:"
#define LABEL4_TOP		6
#define LABEL4_LEFT		1
#define BATCH_COUNT_TOP		6
#define BATCH_COUNT_LEFT	17
#define BATCH_COUNT_LEN		3
// 07/13/2001 Patrick Pape Stop

// 07/23/2001 Patrick Pape Start
// Add Carton Code to the screen
#define LABEL5_CAPTION		carton_code,	"Carton Code:"
#define LABEL5_TOP		5
#define LABEL5_LEFT		1
#define CTNCOD_TOP		5
#define CTNCOD_LEFT		17
// 07/23/2001 Patrick Pape Stop

#define MSGFLDA_LEFT         1
#define MSGFLDA_TOP          11
#define MSGFLDA_LEN          20

#define MSGFLDB_LEFT         1
#define MSGFLDB_TOP          12
#define MSGFLDB_LEN          20 

#endif

// Form definition

Begin Form UNDIR_CLUSTER_PICK 
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  FORM_HELPTEXT
    
    // Function key definitions
    Begin Fkey FKEY_BACK
        Caption   FKEY_BACK_CAPTION
        Begin Action 
	  //Check if user is assigned to any cartons 
	  //If he's assigned non-kit picks, it's OK, we'll just deassign them.
          ExecuteDSQL ("[select 1 from pckwrk where ackdevcod = '@global.devcod'  and ctnnum is not null group by ctnnum having sum(appqty) > 0 and sum(appqty) < sum(pckqty)]")
          Begin Result eOK
            GetResponse (ERR_CARTON_INCOMPLETE)  // incomplete carton pick in batch
	    ResetMessageLine()
	  End
          Begin Result !eOK  //No cartons are in process

	    //Since none of the cartons are in process, we can let the user back out.
	    //We'll deassign him from any picks, and, make sure any work requests
	    //that were acknowledged get unacknowledged.

	    ExecuteDSQL( "[update pckwrk set vc_slotno = 0 where ackdevcod = '@global.devcod']" )
	    ExecuteDSQL("process work nack where devcod = '@global.devcod'")

            ExecuteDSQL ("deassign user from picks where devcod = '@global.devcod' ")
            ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod' ]")
            Begin Result !eOK   // no records returned -- nothing on RDT
	        Back()
            End
            Begin Result eOK // There's inventory on RDT; force deposit!
                Verify (INIT_POLICIES.lbllodund, RF_FLAG_YES)
                Begin Result 1
                    ExecuteForm ("LABEL_LOAD")
                End
                Begin Result 0
                    Set (INIT_POLICIES.prvfrm,"UNDIR_CLUSTER_PICK")
                    ExecuteForm ("DEPOSIT_A")
                End
            End
          End
        End     // End Action FKEY_BACK
    End     // End Fkey FKEY_BACK
    
    Begin Fkey FKEY_DONE  // build batch done; start picking
         Caption FKEY_DONE_CAPTION
         Begin Action
             Set (msgflda, "")
             Set (msgfldb, "")
             DisplayField("msgflda")
             DisplayField("msgfldb")
             Set (pckmod,1)  // set to picking mode

	     //  *** GAP 09.03 ****  start  *****  //
	     SetFieldAttr ("slotno", ENTRY_MASK_PROTECTED)
	     //  *** GAP 09.03 ****  end  *****  //


         End  // End Action FKEY_DONE
    End  // End Fkey FKEY_DONE
 
    Begin Fkey FKEY_HELP
        Caption   FKEY_HELP_CAPTION
        Begin Action 
            DisplayHelp ()
        End     // End Action FKEY_HELP
    End     // End Fkey FKEY_HELP

    // Label definitions

    Begin Label LABEL1
        Caption   LABEL1_CAPTION
        Left      LABEL1_LEFT
        Top       LABEL1_TOP
        Height    RF_FIELD_HEIGHT
    End     // End Label LABEL1
    
    Begin Label LABEL2
        Caption   LABEL2_CAPTION
        Left      LABEL2_LEFT
        Top       LABEL2_TOP
        Height    RF_FIELD_HEIGHT
    End     // End Label LABEL2

    //  *** GAP 09.03 ****  start  *****  //

    Begin Label LABEL3
        Caption   LABEL3_CAPTION
        Left      LABEL3_LEFT
        Top       LABEL3_TOP
        Height    RF_FIELD_HEIGHT
    End     // End Label LABEL3

    //  *** GAP 09.03 ****  end  *****  //

    // 07/13/2001 Patrick Pape Start
    // Add carton count to the screen 
    Begin Label LABEL4
	Caption	LABEL4_CAPTION
	Left	LABEL4_LEFT
	Top	LABEL4_TOP
	Height	RF_FIELD_HEIGHT
    End
    // 07/13/2001 Patrick Pape Stop

    // 07/23/2001 Patrick Pape Start
    // Add Carton Code to the screen
    Begin Label	LABEL5
	Caption	LABEL5_CAPTION
	Left	LABEL5_LEFT
	Top	LABEL5_TOP
	Height	RF_FIELD_HEIGHT
    End
    // 07/23/2001 Patrick Pape Stop
    
    // Local field definitions

    Begin Field pckmod              // 0 = build batch mode, 1 = picking mode
	#include "local_integer.h"
        Width       RF_FLAG_LEN
    End       // End Field

    Begin Field crekit
	#include "local_string.h"
        Width       FLAG_LEN
    End       // End Field

    Begin Field dspsub
	#include "local_string.h"
        Width       SUBNUM_LEN
    End       /* End Field*/

    Begin Field crelod
	#include "local_string.h"
        Width       FLAG_LEN
    End       // End Field
    
    Begin Field nxtloc
	#include "local_string.h"
        Width       STOLOC_LEN
    End       // End Field
    
    Begin Field wrkref
	#include "local_string.h"
        Width       WRKREF_LEN
    End       // End Field
    
    Begin Field inpval 
	#include "local_string.h"
        Width       CTNNUM_LEN
    End       // End Field
    
    Begin Field pick_id_wrkref       // Will hold the wrkref for the translated pick identifier
	#include "local_string.h"
        Width       WRKREF_LEN
    End       // End Field
    
    Begin Field lodnum
	#include "local_string.h"
        Width       LODNUM_LEN
    End

    Begin Field carton
	#include "local_string.h"
        Width       CTNNUM_LEN
    End       // End Field
    
    Begin Field ackdevcod 
	#include "local_string.h"
        Width       DEVCOD_LEN
    End       // End Field
    
    Begin Field pcksts 
	#include "local_string.h"
        Width       PCKSTS_LEN
    End       // End Field
    
    Begin Field appqty 
	#include "local_integer.h"
        Width      RF_QUANTITY_LEN 
    End       // End Field

    Begin Field pckqty 
	#include "local_integer.h"
        Width      RF_QUANTITY_LEN 
    End       // End Field
  
   Begin Field wrkqueackdevcod
       #include "local_string.h"
       Width       DEVCOD_LEN
    End       // End Field

    Begin Field reqnum
       #include "local_integer.h"
       Width      RF_QUANTITY_LEN
    End       // End Field

    Begin Field maxslt
       #include "local_integer.h"
       Width      RF_QUANTITY_LEN
    End       // End Field


    Begin Field curslt
       #include "local_integer.h"
       Width      RF_QUANTITY_LEN
    End       // End Field

    Begin Field returning
	#include "local_integer.h"
	Width	1
    End

    Begin Field subucc
	#include "local_string.h"
	Width	20
    End

    Begin Field save_subnum
	#include "local_string.h"
	Width	20
    End

    Begin Field kit_wrkref
	#include "local_string.h"
	Width	WRKREF_LEN
    End



    Begin Field wrkzon
       #include "local_string.h"
       Width      WRKZON_LEN
    End       /* End Field*/

    Begin Field wrkzon_next
       #include "local_string.h"
       Width      WRKZON_LEN
    End       /* End Field*/

                         /* cluster pick wrkzon change, 		*/
                         /*      1= user needs to enter dep loc		*/
                         /*      0 = policy loc valid and will be used	*/

    Begin Field depflg              
        #include "local_integer.h"                                    
        Width      RF_FLAG_LEN
    End       /* End Field*/




    // Field definitions

    // 07/13/2001 Patrick Pape Start
    // Add carton count to the screen
    Begin Field batch_count
	Left		BATCH_COUNT_LEFT
	Top		BATCH_COUNT_TOP
	Width		BATCH_COUNT_LEN
	Height		RF_FIELD_HEIGHT
	Datatype	Long
	TabOrder	3
	DisplayMask	DISPLAY_MASK_NO_DEFAULT_CHAR
	DefaultData	0
	EntryMask	ENTRY_MASK_PROTECTED
    End
    // 07/13/2001 Patrick Pape Stop

    // 07/23/2001 Patrick Pape Start
    // Add carton code to the screen
    Begin Field ctncod
	Left		CTNCOD_LEFT
	Top		CTNCOD_TOP
	Width		CTNCOD_LEN
	Height		RF_FIELD_HEIGHT
	Datatype	String
	TabOrder	3
	DisplayMask	DISPLAY_MASK_NO_DEFAULT_CHAR
	DefaultData	0
	EntryMask	ENTRY_MASK_PROTECTED
    End
    
    Begin Field msgflda
        Left        MSGFLDA_LEFT
        Top         MSGFLDA_TOP
        Width       MSGFLDA_LEN
        Height      RF_FIELD_HEIGHT
        Datatype    String
        TabOrder    3
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End

    Begin Field msgfldb
        Left        MSGFLDB_LEFT
        Top         MSGFLDB_TOP
        Width       MSGFLDB_LEN
        Height      RF_FIELD_HEIGHT
        Datatype    String
        TabOrder    3
        DisplayMask DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData 0
        EntryMask   ENTRY_MASK_PROTECTED
    End


    //  *** GAP 09.03 ****  start  *****  //

    Begin Field slotno
        Left        SLOT_LEFT 
        Top         SLOT_TOP 
        Width       3
        Height      RF_FIELD_HEIGHT
        Datatype    Long
        TabOrder    0
        DisplayMask 0
        DefaultData 0
        EntryMask   ENTRY_MASK_INPUT_REQUIRED

	Begin Action ExitAction 

	    Compare (pckmod, 1) 
	    Begin Result !eOK 
		    
		ExecuteDSQL ("[select to_number(rtstr1)  from poldat into :maxslt where polcod = 'VAR' and polvar = 'MAX-SLOTS-PER-DEVICE' and polval = '@global.devcod' ]") 
		GetResults() 

		Begin Result !eOK 
		    GetResponse( ERR_NOT_AUTHORIZED )
		    ResetMessageLine()
		    Reenter()
		End 

		Compare( maxslt, slotno )  
		Begin Result -1         // max no of cartons 
		    GetResponse( ERR_SLOT_TOO_HIGH )
		    ResetMessageLine()
		    Reenter()
		End 

		ExecuteDSQL ("[select 'x' from pckwrk into :curslt where ackdevcod = '@global.devcod'  and vc_slotno = '@slotno'] ")
		GetResults()  

		Begin Result eOK 
		    GetResponse(ERR_SLOT_IN_USE) 
		    ResetMessageLine()
		    Reenter () 

		End
	    End 
	End     

    End 

    //  *** GAP 09.03 ****  end  *****  //

    Begin Field subnum
        Left        SUBNUM_LEFT 
        Top         SUBNUM_TOP 
        Width       SUBNUM_LEN
        Height      RF_FIELD_HEIGHT
        Datatype    String
        TabOrder    3 
        DisplayMask 0
        DefaultData 0
        EntryMask   ENTRY_MASK_INPUT_REQUIRED
        
        Begin Action EntryAction
            ResetMessageLine ()
	    Verify (pckmod,1)
	    Begin Result 1 // User is done creating batch, now in picking mode

	        //The following checks apply only if user scanned a carton for a kit
		//pick. They do not apply if the user scanned a case
                Length(subnum)
                Begin Result 1
		    //See if there are picks remaining for the carton
                    ExecuteDSQL (" [select pcksts from pckwrk pw into :pcksts, :pckqty, :appqty where pw.ctnnum = '@subnum' and pw.appqty < pw.pckqty and wrktyp != 'K']")
		    GetResults()
                    Begin Result !eOK
			Set( save_subnum, subnum )
			ExecuteDSQL("[select subucc, ctncod from pckwrk into :subucc, :ctncod where subnum = '@subnum']")
			GetResults()
			Begin Result eOK
			    Length( subucc )
			    Begin Result 1
			        Set( subnum, subucc )
			        DisplayField( "subnum" )
			    End
			End

			DisplayField( "ctncod" )

			GetResponse(ERR_CARTON_COMPLETE)
			
			Set( subnum, save_subnum )

			//No picks left, let's complete the overpack kit...
			//Also, let's make sure the wrkref on the subnum reflects 
			//the wrkref.  It may not, if the sub-load was created
			//directly onto the RF device.
			//Since we don't have the Kit pick wrkref ID saved anywhere
			//(depending on the order user scanned cartons into the batch,
    			//we may or may not), we'll grab it out of the pckwrk table.

			ExecuteDSQL("[update invsub set wrkref = (select wrkref from pckwrk where wrktyp = 'K' and subnum = '@subnum') where subnum = '@subnum']")

			ExecuteDSQL("complete overpack kit where kitnum = '@subnum' and devcod = 'global.devcod'")


			//If there was a work request for the kit we just completed
			//let's make sure we also complete the work request
			//We need to re-find the reqnum, because we might not have the
			//right value sitting in the field depnding on the order we
			//scanned the carton numbers to build the batch.

                        ClearField("reqnum")
			ExecuteDSQL("[select reqnum into :reqnum from wrkque w, pckwrk p where p.wrktyp = 'K' and p.subnum = '@subnum'  and w.wrkref = p.wrkref]")
                        GetResults()
			Begin Result eOK 
			    ExecuteDSQL("complete work where reqnum = '@reqnum' and prcmod = 'nomove'")
			    Begin Result ! eOK
			        Beep(3)
				GetResponse(ERR_CMP_WRKREQ)
				Set(ERROR_LOGOUT.errstr, ERR_CMP_WRKREQ)
				ExecuteForm("ERROR_LOGOUT")
			    End
			End

			//Also, we'll then update the appqty for the kit pick
			//itself, since we won't be generating a pckwrk record
			//to move the kit pick itself which is what we would
			//normally do from a processing area.

                        ExecuteDSQL("[update pckwrk set appqty = pckqty, vc_slotno = 0 where subnum = '@subnum' and wrktyp = 'K']")
			
			ClearField("subnum")
			DisplayField("subnum")

                    End  //Result eOK for ExecuteDSQL
                 End //Result 1 length(subnum)

                //  nxtloc is set here; pickup_a uses nxtloc as a deposit loc for completed cartons.
                // If records are found, there are picks left for the user's batch.
		//
		// Remember, the user may scan either a case or a kit pick...

	        ExecuteDSQL ("[select pw.ctnnum, pw.wrkref, pm.stoloc, pw.ctnnum from pckmov pm, pckwrk pw, locmst lm  into :subnum, :wrkref, :nxtloc, :carton where pw.ackdevcod = '@global.devcod' and pw.srcloc = lm.stoloc and pw.appqty < pw.pckqty and pw.pcksts = 'R' and pw.wrktyp = 'P' and pm.cmbcod = pw.cmbcod and pm.seqnum = (select min(pmov.seqnum) from pckmov pmov where pmov.cmbcod = pw.cmbcod) order by lm.trvseq]") 
	        GetResults()
	        Begin Result !eOK
                    Length (wrkref)
                    Begin Result 0  // wrkref does not have length, is null
			ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod' ]")
			Begin Result eOK // There's inventory on RDT; force deposit!
			    Verify (INIT_POLICIES.lbllodund, RF_FLAG_YES)
			    Begin Result 1
				ExecuteForm ("LABEL_LOAD")
			    End
			    Begin Result 0
				Set (INIT_POLICIES.prvfrm,"UNDIR_CLUSTER_PICK")
				ExecuteForm ("DEPOSIT_A")
			    End

			    ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod' ]") 
			    Begin Result eOK
				Reenter()
			    End
			End
			
			Begin Result !eOK
			    ClearField( "ctncod" )
			    DisplayField( "ctncod" )
                            GetResponse (ERR_NO_BATCH)  // nothing in batch
                            ResetMessageLine ()
			End

                        Set (pckmod, 0)

			//  *** GAP 09.03 ****  start  *****  //
			ExecuteDSQL ("[update pckwrk set vc_slotno = 0 where ackdevcod = '@global.devcod']")
			//  *** GAP 09.03 ****   end   *****  //

			ExecuteDSQL ("deassign user from picks where devcod = '@global.devcod' ")

			ClearForm()
		        ExecuteForm ("UNDIR_CLUSTER_PICK") 
                    End
                    Begin Result 1  //wrkref has length, is not null
		        ClearField ("msgflda")
			ClearField ("msgfldb")
			ClearField ("slotno")
			ClearField ("ctncod")
			DisplayField("msgflda")
			DisplayField("msgfldb")
			DisplayField("slotno")
			DisplayField("ctncod")
                        GetResponse (MSG_BATCH_CMP) // batch complete
                        ResetMessageLine ()

			//  *** GAP 09.03 ****  start  *****  //
			ExecuteDSQL (" [update pckwrk set vc_slotno = 0 where ackdevcod = '@global.devcod'  and vc_slotno = '@slotno']")
			//  *** GAP 09.03 ****   end   *****  //

			ExecuteDSQL ("deassign user from picks where devcod = '@global.devcod' ")

                        ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod']")
		        Begin Result !eOK // No inventory on RDT
                            ClearForm()
                            Set (pckmod, 0)
		            ExecuteForm ("UNDIR_CLUSTER_PICK") 
                        End
                        Begin Result eOK // There is inventory on RDT; force deposit!
                            Verify (INIT_POLICIES.lbllodund, RF_FLAG_YES)
                            Begin Result 1
                                ExecuteForm ("LABEL_LOAD")
                            End
                            Begin Result 0
                                Set (INIT_POLICIES.prvfrm,"UNDIR_CLUSTER_PICK")
                                ClearForm()
                                Set (pckmod, 0)  // set to building batch mode
                                ExecuteForm ("DEPOSIT_A")
                            End
                        End
                    End
	        End
		Set (crekit,"N")
		Set (crelod,"N")
                Length (carton)
		Begin Result 1
		   ExecuteDSQL ("list inventory where invsub.subnum = '@subnum'")
		   Begin Result !eOK

		      //The invsub record hasn't been created yet, so we need to tell
		      //the pickup form that a consolidated load and overpack kit will
		      //need to be created.
		      
		      Set (crekit,"Y")
		      // Set (crelod,"Y")
		      Length(lodnum)
		      Begin Result 0
			Set (crelod,"Y")
		        ExecuteDSQL ("generate next number into :lodnum where numcod = 'lodnum'")
		        GetResults()
		      End
		   End
		End
		Set(INIT_POLICIES.prvfrm,"UNDIR_CLUSTER_PICK")
                ExecuteForm ("PICKUP_A")
	    End  // pick mode
        End  // Action EntryAction
        
        Begin Action ExitAction
            Verify (pckmod,1)
            Begin Result 0 // 'We're still building the cluster batch 
	        //We need to get the translated pick identifier here...
		ExecuteDSQL("get translated pick identifier into :inpval, :pick_id_wrkref where inpval = '@subnum'")
		GetResults()
		Begin Result !eOK
		    GetResponse( ERR_INV_CARTON )
		    ClearField( "ctncod" )
		    DisplayField( "ctncod" )
		    ResetMessageLine()
		    Reenter()
		End

		//First things first, if there's a work request out there for this
		//work reference, let's acknowledge it, so nobody else gets directed
		//to it.

                ExecuteDSQL("[select reqnum, ackdevcod from wrkque into :reqnum, :wrkqueackdevcod where wrkref = '@pick_id_wrkref']")
                GetResults()

		//If the result is not OK, we do not care, it just means there is not
		//work out there.
		Begin Result eOK

		    //Make sure it is not acknowledged by someone else.
		    Length(ackdevcod)
		    Begin Result 0		//Not in process by someone else
		        //Assign user to the piece of work.
			ExecuteDSQL("process work ack where reqnum = '@reqnum' and ackdevcod = '@global.devcod'")
		    End 
		    Begin Result 1 //Length(ackdevcod)
		        //Has a devcie assigned, see if it's the same one we're on...
			Verify(wrkqueackdevcod, global.devcod)
			Begin Result 0
			    GetResponse(ERR_CARTON_IN_PROCESS)
			    ClearField("subnum")
			    ResetMessageLine()
			    Reenter()
			End
		    End
		End


		//We should make sure that the carton the user entered acutally
		//has picks remaining

                ExecuteDSQL ("[select pcksts, pckqty, appqty, ackdevcod, ctncod from pckwrk pw into :pcksts, :pckqty, :appqty, :ackdevcod, :ctncod where pw.wrkref = '@pick_id_wrkref']")
		GetResults()
                Begin Result !eOK
                    Beep(3)
		    GetResponse(ERR_INV_CARTON)
		    ClearField("subnum")
		    ClearField("ctncod")
		    DisplayField( "ctncod" )
		    ResetMessageLine()
		    Reenter() 
		End

		DisplayField( "ctncod" )

	        //Make sure the pick is not already complete
	        Verify(pcksts, "C")
	        Begin Result 1		//Carton is complete
	            //Carton is already complete
		    GetResponse(ERR_CARTON_COMPLETE)
		    ClearField("subnum")
		    ClearField("ctncod")
		    DisplayField("ctncod")
		    ResetMessageLine()
		    Reenter() 
	        End

		// Since it's not complete, make sure the pick is released
		Verify(pcksts, "R")
		Begin Result 0
		    GetResponse(ERR_CARTON_NOT_REL)
		    ClearField("subnum")
		    ClearField("ctncod")
		    DisplayField("subnum")
		    DisplayField("ctncod")
		    ResetMessageLine()
		End

		Begin Result 1

                    // Verify the the applied qty is still < the pick qty. 
		    // If not, give message that carton is complete.
		    // We'll do this check in case we have completed the kit yet,
		    // so the pcksts might still be R, with nothing left to do.

		    Compare(appqty, pckqty)
		    Begin Result !-1  	// pckqty less than appqty, is all picked 
		        //Carton is already complete
		        GetResponse(ERR_NO_PICKS_PENDING)
		        ClearField("subnum")
			ClearField("ctncod")
			DisplayField("ctncod")
			ResetMessageLine()
				
			//  *** GAP 09.03 ****  start  *****  //
			//   Free teh slot since this subnum will not be a part of teh batch // 
			ExecuteDSQL (" [update pckwrk set vc_slotno = 0 where ackdevcod = '@global.devcod'  and vc_slotno = '@slotno']")
			//  *** GAP 09.03 ****   end   *****  //

		        Reenter() 
		    End

		    // Make sure the carton is not being worked on by 
		    // someone else.
		    Length(ackdevcod)
		    Begin Result 0		//Not in process by someone else
		    
   	                //Assign the user to the carton he just scanned.
                        ExecuteDSQL ("assign user to pick where wrkref = '@pick_id_wrkref'")
			Begin Result !eOK
			    DisplayMessageText()
			    ResetMessageLine()
			    Reenter()
			End

			//  *** GAP 09.03 ****  start  *****  //
			ExecuteDSQL (" [update pckwrk set vc_slotno = '@slotno' where ackdevcod = '@global.devcod'  and wrkref = '@pick_id_wrkref' ]")
			//  *** GAP 09.03 ****  end  *****  //

			Begin Result !eOK
			    DisplayMessageText()
			    ResetMessageLine()
			    Reenter() 
			End	

			ExecuteDSQL("[update pckwrk set vc_slotno = '@slotno' where ackdevcod = '@global.devcod' and wrktyp = 'P' and ctnnum = ( select subnum from pckwrk where wrkref = '@pick_id_wrkref')]")

                        Set (msgflda, subnum)
                        Translate (msgfldb, MSG_ADDED_TO_BATCH) 
                        DisplayField("msgflda")
                        DisplayField("msgfldb")
		        ClearField("subnum")
		        //Run the form again to allow the user to pick the next carton.
                        ExecuteForm ("UNDIR_CLUSTER_PICK")
		    End

		    Begin Result 1 //length(ackdevcod)
		        //Has a device assigned, so see if it's the same as
		        //the one we're on.

	                Verify(ackdevcod, global.devcod)
		        Begin Result 0	//Is a different device
		            GetResponse(ERR_CARTON_IN_PROCESS)
		            ClearField("subnum")
			    ResetMessageLine()
		            Reenter() 
		        End
   	                //Assign the user to the carton he just scanned.
                        ExecuteDSQL ("assign user to pick where wrkref = '@pick_id_wrkref'")
			Begin Result !eOK
			    DisplayMessageText()
			    ResetMessageLine()
			    Reenter()
			End

			ExecuteDSQL (" [update pckwrk set vc_slotno = '@slotno' where ackdevcod = '@global.devcod'  and wrkref = '@pick_id_wrkref' ]")

			Begin Result !eOK
                            DisplayMessageText()
			    ResetMessageLine()
                            Reenter()
                        End    

			ExecuteDSQL("[update pckwrk set vc_slotno = '@slotno' where ackdevcod = '@global.devcod' and wrktyp = 'P' and ctnnum = ( select subnum from pckwrk where wrkref = '@pick_id_wrkref')]")

                        Set (msgflda, subnum)
                        Translate(msgfldb, MSG_ADDED_TO_BATCH) 
                        DisplayField("msgflda")
                        DisplayField("msgfldb")
		        ClearField("subnum")
		        //Run the form again to allow the user to pick the next carton.
                        ExecuteForm ("UNDIR_CLUSTER_PICK")
		    End
		End
	    End
        End     // End Action
    End     // End Field 

    Begin Action EntryAction
        Verify (pckmod,1)
        Begin Result 0 // building mode
//	    ExecuteDSQL ("[select 'x' from invlod where stoloc = '@global.devcod' ]")
//	    Begin Result eOK // There's inventory on RDT; force deposit!
//	        GetResponse (ERR_RF_HAS_INV)
//		DisplayMessage (RF_MSG_PROCESSING)
//	        Verify (INIT_POLICIES.lbllodund, RF_FLAG_YES)
//	        Begin Result 1
//	            ExecuteForm ("LABEL_LOAD")
//	        End
//	        Begin Result 0
//	            Set (INIT_POLICIES.prvfrm,"UNDIR_CLUSTER_PICK")
//	            ExecuteForm ("DEPOSIT_A")
//	        End
//	    End
	    SetFieldAttr ("slotno",ENTRY_MASK_INPUT_REQUIRED)

	    //Unassign the slot number from any pckwrk that has been completed
	    ExecuteDSQL( "[update pckwrk set vc_slotno = 0 where ackdevcod = '@global.devcod' and pckqty = appqty]" )
       End

       //MR 473 08/01/2001 Patrick Pape Start
       //Display the slot number correctly on the screen
       Verify( returning, 1 )  
       Begin Result 1
           Set( returning, 0 )
           DisplayField( "slotno" )
       End
       //MR 473 08/01/2001 Patrick Pape Stop

       // 07/13/2001 Patrick Pape Start
       // Add carton count to the screen    
       ExecuteDSQL( "[select count( 'x') from pckwrk into :batch_count where ackdevcod = '@global.devcod' and vc_slotno is not null and pckqty > appqty and ( wrktyp = 'K' or ( wrktyp = 'P' and ctnnum is null ))]" )
       GetResults()
       DisplayField( "batch_count" )
       // 07/13/2001 Patrick Pape Stop

   End

End     // End of Form UNDIR_CLUSTER_PICK 
