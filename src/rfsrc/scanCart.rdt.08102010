/* Include standard RF definitions */

#include "rf_format.h"
#include "rf_err.h"

/* Text & Message definitions */

#define CART_NEXT  "stsCartNext",     "ANY CART"
#define CART_LOADED  "stsCartLoaded",     "Cart Picks Loaded"
#define CART_PRINTED  "stsCartPrinted",     "Cart Already Printed"
#define ERR_INV_SUBUCC  "errInvalidSubucc",     "invalid case id"

#define ERR_INV_PRTADR  "errInvalidPrtadr",     "invalid printer address"

#define ERR_PRINTING  "errPrinting",     "Error Printing"

#define CONFIRM_PRINT  "ConfirmPrint",     "Printed Ticket, Scan Next"

/* Field & Label positioning definitions */

#define LABEL1_CAPTION          ttlLoadCart, "Scan Cart"
#define LABEL1_TAG              ttlLoadCart
#define LABEL1_LEFT             RF_TITLE_LEFT
#define LABEL1_TOP              RF_TITLE_TOP
#define LABEL1_WIDTH            RF_TITLE_WIDTH

#ifdef RF_FORMAT_40X8
#    define LABEL2_CAPTION      cartman, "Cart Manifest:"
#    define LABEL2_TAG          cartman 
#    define LABEL2_LEFT         1
#    define LABEL2_TOP          1
#    define LABEL2_WIDTH        21
#    define LABEL2_JUSTIFY      Right 
#    define LABEL3_CAPTION      prtadr, "printer address:"
#    define LABEL3_TAG          prtadr
#    define LABEL3_LEFT         1
#    define LABEL3_TOP          2
#    define LABEL3_WIDTH        21
#    define LABEL3_JUSTIFY      Right

#    define CARTMAN_TAG         cartman 
#    define CARTMAN_LEFT         23
#    define CARTMAN_TOP          1
#    define PRTADR_TAG          prtadr
#    define PRTADR_LEFT         23
#    define PRTADR_TOP          2 


#endif

#ifdef RF_FORMAT_20X16
#    define LABEL2_CAPTION      cartman, "Cart Manifest:"
#    define LABEL2_TAG          cartman 
#    define LABEL2_LEFT         1
#    define LABEL2_TOP          1
#    define LABEL2_WIDTH        RF_TERMINAL_WIDTH
#    define LABEL2_JUSTIFY      Left

#    define LABEL3_CAPTION      prtadr, "printer address:"
#    define LABEL3_TAG          prtadr
#    define LABEL3_LEFT         1
#    define LABEL3_TOP          3
#    define LABEL3_WIDTH        RF_TERMINAL_WIDTH
#    define LABEL3_JUSTIFY      Left


#    define CARTMAN_TAG         cartman 
#    define CARTMAN_LEFT         4
#    define CARTMAN_TOP         2 

#    define PRTADR_TAG          prtadr
#    define PRTADR_LEFT         4
#    define PRTADR_TOP          4


#endif

/* Form definition */
Begin Form SCAN_CART
    Left      RF_TERMINAL_LEFT
    Top       RF_TERMINAL_TOP
    Width     RF_TERMINAL_WIDTH
    Height    RF_TERMINAL_HEIGHT
    Caption   RF_FORM_CAPTION
    HelpText  HELP_LABELLOAD

    Begin Fkey FKEY_BACK
        Caption   FKEY_BACK_CAPTION
        Begin Action
            Back()
        End
    End

    Begin Fkey FKEY_HELP
        Caption FKEY_HELP_CAPTION
        Begin Action
            DisplayHelp ()
        End 
    End


    /* Timer definitions */

    /* Label definitions */

    Begin Label LABEL1
        Caption   LABEL1_CAPTION
        Tag       LABEL1_TAG
        Left      LABEL1_LEFT
        Top       LABEL1_TOP
        Width     LABEL1_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   Center
    End 

    Begin Label LABEL2
        Caption   LABEL2_CAPTION
        Tag       LABEL2_TAG 
        Left      LABEL2_LEFT
        Top       LABEL2_TOP
        Width     LABEL2_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL2_JUSTIFY 
    End 

    Begin Label LABEL3
        Caption   LABEL3_CAPTION
        Tag       LABEL3_TAG 
        Left      LABEL3_LEFT
        Top       LABEL3_TOP
        Width     LABEL3_WIDTH
        Height    RF_FIELD_HEIGHT
        Justify   LABEL3_JUSTIFY 
    End 


    /* Local field definitions */
    
    Begin Field return_to_labeling
#        include "local_integer.h"
        Width    RF_FLAG_LEN
    End

    Begin Field junk
      #include "local_string.h"
      Width     40
    End       // End Field

    Begin Field assign_user
	Left        1
	Top         1
	Height      RF_FIELD_HEIGHT
	Datatype    Long
	TabOrder    1
	DisplayMask 0
	DefaultData 1
	EntryMask   ENTRY_MASK_LOCAL_DATA_ONLY
      	Width     RF_FLAG_LEN 
    End       // End Field

    Begin Field print_msg
      #include "local_string.h"
      Width     40
    End       // End Field

    Begin Field wrkref
        #include "local_string.h"
        Width    WRKREF_LEN
    End       // End Field

    Begin Field printed_flag 
        #include "local_integer.h"
        Width    RF_FLAG_LEN
    End       // End Field
 
    Begin Field cartman 
	Tag             CARTMAN_TAG 
	Left            CARTMAN_LEFT 
	Top             CARTMAN_TOP 
	Width           STOLOC_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED
        
	Begin Action EntryAction
        	Verify (assign_user, 1)
		Begin Result 1
	     	  ExecuteDSQL ("get usr next cart suggestion into :junk'")
	     	  Begin Result 0
			GetResults()
			DisplayMessage ("lblGotNext", junk)  
	     	  End 
	     	  Begin Result !0
		    DisplayMessage(CART_NEXT)
	     	  End
		End
	End

        Begin Action ExitAction
            ExecuteDSQL("check usr cart printed into :printed_flag where cartman = '@cartman' ")
	    GetResults()
	    
	    Verify (printed_flag, 1)
            Begin Result 1 
                Verify (assign_user, 0)
		 Begin Result 1
                    Beep(3)
                    GetResponse (CART_PRINTED)
                    ClearField("cartman")
		    Reenter() 
		 End 
 	        Begin Result 0
		   SetFieldAttr ("prtadr", ENTRY_MASK_PROTECTED) 
		End
            End
	    Begin Result 0
		   SetFieldAttr ("prtadr", ENTRY_MASK_INPUT_REQUIRED) 
	    End
        End /* End Field cartid ExitAction */

    End 


    Begin Field prtadr
        Tag             PRTADR_TAG
        Left            PRTADR_LEFT
        Top             PRTADR_TOP
        Width           PRTADR_LEN
        Height          RF_FIELD_HEIGHT
        Datatype        String
        TabOrder        1
        DisplayMask     DISPLAY_MASK_NO_DEFAULT_CHAR
        DefaultData     0
        EntryMask       ENTRY_MASK_INPUT_REQUIRED

          Begin Action ExitAction

            /* Verify that the scaned printer address is valid in the system*/

            ExecuteDSQL ("[select 'data' from dual where exists (select * from prsmst where prtadr ='@prtadr')]")
            Begin Result !eOK
                Beep (3)
                GetResponse (ERR_INV_PRTADR)
                ClearField("prtadr")
                DisplayField("prtadr")
                Reenter ()
            End


         End /* End Field prtadrExitAction */

    End /* End prtadr */

    /* Form action definitions */

        Begin Action ExitAction
	    DisplayMessage (RF_MSG_PROCESSING)
            ExecuteDSQL("assign usr cart to device where cartman = '@cartman' and prtadr = '@prtadr' and devcod = '@global.devcod' and assign_user = @assign_user")
            Begin Result !eOK
                Beep(3)
               DisplayMessageText () 
                ClearField("cartid")
                DisplayField("cartid")
                ResetMessageLine () 	
		Reenter()
            End
	    Begin Result eOK
                GetResponse (CART_LOADED)

		Verify(assign_user, 1)
                Begin Result 1
             	   Set (UNDIR_CLUSTER_PICK.msgflda, "")
             	   Set (UNDIR_CLUSTER_PICK.msgfldb, "")
             	   Set (UNDIR_CLUSTER_PICK.pckmod,1)  // set to picking mode

	            SetFieldAttr ("UNDIR_CLUSTER_PICK.slotno", ENTRY_MASK_PROTECTED)

	            ExecuteDSQL ("set cluster confirmation mode where mode = '@UNDIR_CLUSTER_PICK.cluster_pick_mode'")
		   ExecuteForm("UNDIR_CLUSTER_PICK")
		End
		Begin Result 0
                  ResetMessageLine () 	
		  ExecuteForm("SCAN_CART")
		End	
	    End
        End /* End ExitAction */
    
    Begin Action EntryAction

        ClearForm()  
    End /* End of form EntryAction */

End /* End of form Pricelabel */

