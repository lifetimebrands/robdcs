<command>

<name>list var staged assigned shipments for ship dock</name>

<description>List Staged Assigned Shipments For Ship Dock</description>

<type>Local Syntax</type>

<local-syntax>

<![CDATA[
    {
    [select distinct sl.stoloc stoloc, ssv.ship_id, 
            ord.stcust, ord.adrnam, ord.cponum, ord.rtcust, 
		ord.adrnam2, ssv.waybil stop_doc_num, 
            ssv.carcod, ssv.srvlvl,ssv.track_num,  ssv.shpsts, 
            ord.carflg, ord.client_id, ord.ordnum, 
            ssv.stop_id, 
            ssv.stop_nam,
            ssv.stop_seq,
            ssv.vc_print_dte,
            ssv.car_move_id,
            ssv.trlr_num,
            ssv.trlr_id,
 	    ssv.trlr_stat,
            ssv.yard_loc,
            ord.early_dlvdte, 
            ord.late_dlvdte, ord.early_shpdte, 
            late_shpdte, ord.vc_extdte
       from locmst, var_shploc sl, var_shpord_view ord,
	    CARDTL, var_ship_struct_view ssv
      where ssv.carcod         = cardtl.carcod
        and ssv.srvlvl         = cardtl.srvlvl
        and cardtl.cartyp <> 'S' 
	and ssv.ship_id = ord.ship_id 
        and ssv.shpsts        in ('S','L', 'X','D','C') 
	and ssv.trlr_stat ! = 'D'
        and ssv.stgdte    is not null 
        and ssv.super_ship_flg = 0
        and ssv.stop_id         is not null
 	and locmst.stoloc = sl.stoloc
        and sl.ship_id = ord.ship_id
        and sl.ordnum = ord.ordnum
        and sl.client_id = ord.client_id
        and @+ssv.doc_num
        and @+ssv.track_num
        and @+ssv.trlr_id
        and @+ssv.ship_id
        and @+ssv.stop_id
        and @+ssv.car_move_id
        and @+ord.rtcust
        and @+ord.stcust
        and @+ord.ordnum
        and @+ord.cponum
        and @+locmst.stoloc
        and @+ssv.carcod
        and @+ssv.srvlvl
        and @+ord.late_dlvdte:date
        and @+ord.early_dlvdte:date
        and @+ssv.late_shpdte:date
        and @+ord.early_shpdte:date
	and @*
      order by 1,2] catch (-1403) } >> res3
	|
        append shipment change deferred flag 
        where res = @res3 
          and status = @? catch(-1403) /* This will be either eOK or NO_ROWS */

]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the shipments by carrier move. 
  It's primary purpose is for use by Ship Dock Operations, to allow users
  to see all shipments that area assigned to carrier moves, in the areas
  designated for use by Ship Dock Operations
  </p>
  <p>
  This command will show staged shipments in only those areas that are defined
  for use by Ship Dock Operations since this purpose of this command is
  for use by Ship Dock Operatios.
  llThis command assumes that the shipping strucutre has been defined for
  the shipment.
  </p>
  <p>
  Plese note the following:
     <li>Need to use numerics in the order by to support sql Server.</li>
     <li>We need to look at the stage date in addition to the status.  As of
         6.1, is it possible to have a shipment in a loading status whose 
         stage date is not set (due to fluid loading), so we still have
         to make sure the stage date is null. </li>

    <li>We need to exclude non-allocatable order lines.  Since they have
        a stgdte and stgqty, they "appear" like a standard order line for
        which the inventory does not currently exist in the staging area
        unless we exclude them.</li>

    <li>We need to exclude any shipments whose carrier has a carrier type
        of Small Package.  Manifest problems arise when a small package
        shipment gets closed from ship dock operations.</li>
  </p>

  <p>
  The second half of the union handles the situation where a shipment
  may have at one time been staged, but there is currently
  no inventory in the staging area.
  For example, we have have an order that doesn't allow partials,
  but only part of it was picked, then the other pick was cancelled,
  so it got staged and loaded (may have been fluid loaded).
  Without the union, we would have no visibility to that shipment
  since there is currently no inventory in the staging lane.
  </p>

  <p>
  After all of that has been done, this command publishes the results from
  the initial selects and then appends the results with the deferred flag.
  The framework will initially call this command with a 1=2 in the where
  clause to insure NO ROWS in the data, but still expects to get the column
  headers.  We do the catch above because if we let it go the command would
  terminiate there and the framework would not see (and then not show) the
  deferred flag.  To make this work, it will do the catch on a NO ROWS, but
  then publish the status (@?), which will be either eOK or NO_ROWS from above.
  </p>

]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Specified shipment not found</exception>

<seealso cref="list shipment staging areas"> </seealso>
<seealso cref="list staged unassigned shipments for ship dock"> </seealso>
<seealso cref="append shipment change deferred flag"> </seealso>

</documentation>
</command>
