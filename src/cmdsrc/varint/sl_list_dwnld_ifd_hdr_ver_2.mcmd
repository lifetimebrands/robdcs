<command>
<name>sl_list dwnld_ifd_hdr ver_2</name>
<description>List ifd header nodes for the Download Viewer screen</description>
<type>Local Syntax</type>
<argument name="max_rows" default="1000" datatype="integer">
   Maximum number of rows.
</argument>
<argument name="common_evt_filter" datatype="string">
   Common event filter.
</argument>
<argument name="common_dwnld_filter" datatype="string">
   Common download filter.
</argument>
<argument name="common_dwnld_ifd_hdr_filter" datatype="string">
   Common IFD header filter.
</argument>
<argument name="sort_asc_flg" datatype="string">
   Sort ascending flag.
</argument>
<argument name="from_dwnld_lin_seq" datatype="float">
   From download line sequence.
</argument>
<argument name="to_dwnld_lin_seq" datatype="float">
   To download line sequence.
</argument>
<argument name="evt_created_flg" datatype="string">
   Event created flag.
</argument>
<argument name="identified_flg" datatype="string">
   Identified IFD flag.
</argument>
<argument name="ifd_data_err_seq_list" datatype="string">
   List of ifd data error sequences.
</argument>
<argument name="ifd_data_seq_list" datatype="string">
   List of IFD data sequences.
</argument>
<argument name="commit_ctxt_idseq_list" datatype="string">
   List of download commit contexts.
</argument>
<argument name="ifd_action_list" datatype="string">
   List of IFD actions.
</argument>
<argument name="evt_data_seq_list" datatype="string">
   List of event data sequences .
</argument>
<argument name="evt_id_list" datatype="string">
   List of event Ids.
</argument>
<argument name="evt_stat_cd_list" datatype="string">
   List of event status codes.
</argument>
<local-syntax>
<![CDATA[
publish data
where max_rows = nvl(@max_rows, 1000)
  and common_evt_filter = nvl(@common_evt_filter, "1=1")
  and common_dwnld_filter = nvl(@common_dwnld_filter, "1=1")
  and common_dwnld_ifd_hdr_filter = nvl(@common_dwnld_ifd_hdr_filter, "1=1")
  and order_by_syntax = decode(@sort_asc_flg, 'F','desc','asc')
  and from_line_syntax_idd = decode(nvl(@from_dwnld_lin_seq,0), 0, '1=1', 
          'idd.dwnld_lin_seq >= ' || @from_dwnld_lin_seq) 
  and to_line_syntax_idd = decode(nvl(@to_dwnld_lin_seq,0), 0, '1=1', 
          'idd.dwnld_lin_seq <= ' || @to_dwnld_lin_seq) 
  and from_line_syntax_ide = decode(nvl(@from_dwnld_lin_seq,0),0, '1=1', 
          'ide.dwnld_lin_seq >= ' || @from_dwnld_lin_seq) 
  and to_line_syntax_ide = decode(nvl(@to_dwnld_lin_seq,0),0, '1=1', 
          'ide.dwnld_lin_seq <= ' || @to_dwnld_lin_seq) 
  and evt_created_syntax = decode(@evt_created_flg,
           'T', 'idh.evt_data_seq is not null',
           'F', 'idh.evt_data_seq is null', '1=1')
  and proc_err_flg_syntax = decode(@proc_err_flg,
           'T', 'idh.proc_err_flg = ''T''',
           'F', 'nvl(idh.proc_err_flg,''F'') = ''F''', '1=1')
  and max_rows = nvl(@max_rows, 1000)
  and identified_filter_idh = decode( @identified_flg, 'F', '1=0', '1=1')
  and identified_filter_ide = decode( @identified_flg, 'T', '1=0', '1=1')
  and ifd_data_err_filter = decode( 'X' || @ifd_data_err_seq_list, 'X', '1=1',
         'ide.ifd_data_err_seq in (' || @ifd_data_err_seq_list || ')')
  and ifd_data_seq_filter = decode( 'X' || @ifd_data_seq_list, 'X', '1=1',
         'idh.ifd_data_seq in (' || @ifd_data_seq_list || ')')
  and commit_ctxt_filter = decode( 'X' || @commit_ctxt_idseq_list, 'X', '1=1',
         'idh.commit_ctxt_idseq in (' || @commit_ctxt_idseq_list || ')')
  and ifd_action_filter = decode( 'X' || @ifd_action_list, 'X', '1=1',
         'idh.ifd_action in (' || @ifd_action_list || ')')
  and evt_data_seq_filter = decode( 'X' || @evt_data_seq_list, 'X', '1=1',
         'idh.evt_data_seq in (' || @evt_data_seq_list || ')')
  and evt_id_cd_filter = decode( 'X' || @evt_id_list || @evt_stat_cd_list,
        'X', '1=1',
                '('
                   ||
                   decode( 'X' || @evt_id_list, 'X', '1=1',
                              'evt.evt_id in (' || @evt_id_list || ')')
                   || ' and '
                   || decode( 'X' || @evt_stat_cd_list, 'X', '1=1',
                              'evt.evt_stat_cd in (' || @evt_stat_cd_list || ')')
                   ||
                ')'
        )
|
if(dbtype = 'ORACLE')
{
  publish data
  where ORACLE_PRE = 'select * from ('
    and ORACLE_POST  = ') xxx where rownum <= ' || @max_rows
    and MSSQL_ROWNUM = '1=1'
}
else
{
  publish data
  where ORACLE_PRE = ''
    and ORACLE_POST = ''
    and MSSQL_ROWNUM = 'rownum <= ' || @max_rows
}
|
[
   @ORACLE_PRE:raw

   select /*+ RULE */ rec_type,
          pk,
          dwnld_lin_seq,
          data_seq,
          proc_err_flg,
          evt_data_seq,
          ifd_action_cd,
          evt_stat_cd
   from
   (
      select 'IFD_DATA_HDR' rec_type,
             'IFD_DATA_HDR'
               || '|' || to_char(idh.dwnld_seq)
               || '|' || to_char(idh.ifd_data_seq)
               || '|' || idh.ifd_id
               || '|' || idh.ifd_ver
               || '|' || to_char(count(idd.ifd_data_dtl_seq)) pk,
             min(idd.dwnld_lin_seq) dwnld_lin_seq,
             idh.ifd_data_seq data_seq,
             idh.proc_err_flg proc_err_flg,
             idh.evt_data_seq evt_data_seq,
             idh.ifd_action_cd ifd_action_cd,
             evt.evt_stat_cd evt_stat_cd
      from   sl_evt_data evt, sl_ifd_data_hdr idh,
             sl_ifd_data_dtl idd
      where @identified_filter_idh:raw
        and idh.dwnld_seq = @dwnld_seq
        and idh.evt_data_seq = evt.evt_data_seq (+)
        and idh.ifd_data_seq = idd.ifd_data_seq (+)
        and @from_line_syntax_idd:raw
        and @to_line_syntax_idd:raw
        and @ifd_data_seq_filter:raw
        and @commit_ctxt_filter:raw
        and @common_dwnld_ifd_hdr_filter:raw
        and @ifd_action_filter:raw
        and @proc_err_flg_syntax:raw
        and @evt_created_syntax:raw
        and @evt_data_seq_filter:raw
        and @evt_id_cd_filter:raw
      group  by
             idh.dwnld_seq,
             idh.ifd_data_seq,
             idh.ifd_id,
             idh.ifd_ver,
             idh.proc_err_flg,
             idh.evt_data_seq,
             idh.ifd_action_cd,
             evt.evt_stat_cd
      union  all
      select 'IFD_DATA_ERR' rec_type,
             'IFD_DATA_ERR'
               || '|' || to_char(ide.dwnld_seq)
               || '|' || to_char(ide.dwnld_lin_seq)
               || '|' || to_char(ide.ifd_data_err_seq)  pk,
             ide.dwnld_lin_seq dwnld_lin_seq,
             ide.ifd_data_err_seq data_seq,
             null proc_err_flg,
/* Modified by aisrael on 03/04/05 to resolve the type conversion issue in 8.1.7  comment starts here
             null evt_data_seq, */
             0 evt_data_seq,
             null ifd_action_cd,
             null evt_stat_cd
      from   sl_ifd_data_err ide
      where @identified_filter_ide:raw
        and ide.dwnld_seq = @dwnld_seq
        and @from_line_syntax_ide:raw
        and @to_line_syntax_ide:raw
        and @ifd_data_err_filter:raw
   )      xxx_view
   where  
          @MSSQL_ROWNUM:raw

   order  by dwnld_lin_seq @order_by_syntax:raw, 
             data_seq @order_by_syntax:raw

   @ORACLE_POST:raw
]

]]>
</local-syntax>
<documentation>
<private>
</private>
<remarks>
<![CDATA[
<p>
This component is used to list ifd header nodes nodes to the Download Viewer screen. 
You can also pass additional SQL filters to the component. </p>
]]>
</remarks>
<retcol name="rec_type" type="COMTYP_STRING">
</retcol>
<retcol name="pk" type="COMTYP_STRING">
</retcol>
<retcol name="dwnld_lin_seq" type="COMTYP_FLOAT">
</retcol>
<retcol name="data_seq" type="COMTYP_FLOAT">
</retcol>
<retcol name="proc_err_flg" type="COMTYP_STRING">
</retcol>
<retcol name="evt_data_seq" type="COMTYP_FLOAT">
</retcol>
<retcol name="ifd_action_cd" type="COMTYP_STRING">
</retcol>
<retcol name="evt_stat_cd" type="COMTYP_STRING">
</retcol>
<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Did not find any records.</exception>
</documentation>
</command>
