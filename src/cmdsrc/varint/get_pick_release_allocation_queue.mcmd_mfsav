<command>
   <name>get pick release allocation queue</name>

   <description>Get Pick Release Allocation Queue</description>

   <type>Local Syntax</type>

   <local-syntax>
      <![CDATA[
	[select count(*) cnt from pckwrk where ship_id is null and 
	    schbat = @schbat]
        |
	if (@cnt != 0)
		^get pick release allocation queue
        else
          {
          [select pw.schbat, 
                  pw.srcare, pw.cmbcod,
                  pw.wrktyp, pw.ship_id, 
                  pw.srcloc, pw.dstare,
                  pw.dstloc, pw.wrkref, pw.lodlvl, 
                  pw.ftpcod, pw.pckqty,
                  pw.untcas, 
                  NVL(pm.seqnum, 1) seqnum,
                  pm.arecod pm_arecod, pm.stoloc pm_stoloc, 
                  pm.rescod pm_rescod, 
                  pw.prtnum, 
                  pw.prt_client_id, 
		  varGetNewFacilityFlag(pw.cmbcod) var_facflg,
                  pm.rowid pm_rowid, pw.wkonum, pw.wkorev, pw.client_id,
                  (fm.untwid * fm.unthgt * fm.untlen) piece_volume,
                  varGetCaseVolByWrkref(wrkref, wrktyp) case_volume,
                  fm.caslen case_length,
                  fm.untlen piece_length,
                  am.stgflg, am.sigflg, am.loccod
             from aremst am, ftpmst fm, pckbat pb, pckmov pm, pckwrk pw 
            where pw.schbat = pb.schbat 
              and pw.cmbcod = pm.cmbcod 
              and pm.arecod = am.arecod
              and pw.ftpcod = fm.ftpcod(+)
              and pw.pcksts = @pcksts
              and pw.schbat = @schbat
              and pw.pipflg != 1 
              and pb.batsts != 'AINP'
              and @+pw.wrkref
              and @+pw.ship_id
              and @+pw.cmbcod
	      and pw.lodlvl != 'D'
         order by pb.pricod, pw.schbat, 
                  pw.cmbcod, 
                  pw.wrkref, pm.seqnum 
         for update of pw.pcksts] catch (@?) >> pckres
|
if (@? != 0)
	^get pick release allocation queue
else
         publish data combination where res = @pckres
}
      ]]>
   </local-syntax>

   <documentation>
      <remarks>
         <![CDATA[
           <p>
                 This command is used to return a list of pckwrk entries which
                 require allocation processing for the pick release process.
                 If this command is customized, then it is necessary for the 
                 customized command to return at least the set of columns returned
                 by this command.  This command is called by process pick release
                 allocation.
           </p>
         ]]>
      </remarks>

      <exception value="eOK">Normal successful completion</exception>

      <exception value="eDB_NO_ROWS_AFFECTED">No orders found</exception>

      <seealso cref="process pick release allocation">
      </seealso>
   </documentation>
</command>

