<command>
<name>list var replen issue</name>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
/* Get the pickface location information -- note that the input params
of pak and lot are shortened forms of untpak and lotnum respectively because
of the limitation of the string size for commands in dcsmgtopr 
*/
{ [select am.loccod, rcfg.stoloc dedicated_pf,      lm.lochgt pf_lochgt, 
	lm.cipflg pf_cipflg, lm.locsts pf_locsts,   lm.arecod pf_arecod, 
	lm.pckflg pf_pckflg, rcfg.invsts pf_invsts, lm.arecod pf_arecod, 
	lm.useflg pf_useflg, lm.lokcod pf_lokcod,   lm.maxqvl pf_maxqvl, 
	lm.erfpct pf_erfpct, lm.pndqvl pf_pndqvl,   lm.curqvl pf_curqvl
	from aremst am, locmst lm, rplcfg rcfg
	where rcfg.prtnum = @prtnum and rcfg.vc_lotnum = @lot
	and rcfg.vc_untpak = @pak   and rcfg.stoloc is not null 
	and lm.stoloc = rcfg.stoloc and rcfg.arecod = am.arecod
  ] catch(@?) 
  |
  if(@? != 0) {
	publish data where ship_id=@ship_id and prtnum = @prtnum 
		and lotnum=@lot and untpak = @pak
		and reacod = 'No dedicated pickface for part/lot/pak'
	        and action = 'Double click for inventory lookup'
  } 
  else if(@rplsts = 'F' or @rplsts = 'S' or @rplsts = 'I') {
    {[select decode(@loccod, 
	'E', sum(ivs.untqty) - sum(ivs.comqty), 
	'P', count(distinct il.lodnum), 
        (sum(ivs.untqty)-sum(ivs.comqty))*(min(fm.unthgt) *
		min(fm.untwid)*min(fm.unthgt))) avlqty,    
	min(lm.cipflg) cipflg,    	min(ivs.invsts) min_invsts,
	max(ivs.invsts) max_invsts,	min(fm.cashgt) cashgt,
	min(lm.locsts) locsts,	min(lm.pckflg) pckflg, 
	min(lm.arecod) arecod,	min(lm.stoloc) stoloc, 
	min(ivs.untcas) untcas,	min(lm.useflg) useflg, 
	min(lm.lokcod) lokcod,	min(ivs.untqty) untqty, 
	min(ivs.comqty) comqty,	min(ivs.pndqty) pndqty, 
	'PATH' path,		max(id.ship_line_id) ship_line_id
	from invdtl id, invsub isb, ftpmst fm, prtmst pm, 
		invlod il, locmst lm, invsum ivs
	where ivs.prtnum = @prtnum and ivs.stoloc = lm.stoloc
	and lm.stoloc != @dedicated_pf
	and exists (select 'x' from poldat, bldg_mst
		where polcod = bldg_mst.bldg_id||'-RPL-PREF'
		and polvar = 'dstare' and lm.arecod = rtstr1
		and @pf_arecod = substr(polval,1,instr(polval,'_')-1))
	and pm.prtnum = @prtnum and id.prtnum = pm.prtnum
/* Dec 20 do we want to check for lotnum and untpak here?? */
	and pm.prt_client_id='----' and fm.ftpcod=pm.ftpcod
	and il.stoloc=lm.stoloc and id.subnum=isb.subnum
	and il.lodnum=isb.lodnum
	group by lm.stoloc
     UNION 
      select decode(@loccod, 
	'E', sum(ivs.untqty) - sum(ivs.comqty), 
	'P', count(distinct il.lodnum), 
	 (sum(ivs.untqty)-sum(ivs.comqty))*(min(fm.unthgt) *
		min(fm.untwid) * min(fm.unthgt))) avlqty,    
	min(lm.cipflg) cipflg,	min(ivs.invsts) min_invsts,
	max(ivs.invsts) max_invsts,	min(fm.cashgt) cashgt,
	min(lm.locsts) locsts,	min(lm.pckflg) pckflg, 
	min(lm.arecod) arecod,	min(lm.stoloc) stoloc, 
	min(ivs.untcas) untcas,	min(lm.useflg) useflg, 
	min(lm.lokcod) lokcod,	min(ivs.untqty) untqty, 
	min(ivs.comqty) comqty,	min(ivs.pndqty) pndqty, 
	'NONPATH' path,		max(id.ship_line_id) ship_line_id
	from invdtl id, invsub isb, ftpmst fm, prtmst pm, 
		invlod il, locmst lm, invsum ivs
	where ivs.prtnum = @prtnum and id.prtnum = ivs.prtnum
	and pm.prtnum = ivs.prtnum and pm.prt_client_id = '----'
/* Dec 20 do we want to check for lotnum and untpak here?? */
	and ivs.stoloc = lm.stoloc and lm.stoloc != @dedicated_pf
	and not exists (select 'x' from poldat, bldg_mst
		where polcod = bldg_mst.bldg_id||'-RPL-PREF'
		and polvar = 'dstare' and lm.arecod = rtstr1
		and @pf_arecod = substr(polval,1,instr(polval,'_')-1))
	and fm.ftpcod = pm.ftpcod  and il.stoloc = lm.stoloc
	and id.subnum = isb.subnum and il.lodnum = isb.lodnum
	group by lm.stoloc
     UNION 
      select nvl(sum(ivs.untqty - ivs.comqty),0) avlqty, 
	min(lm.cipflg) cipflg,	min(ivs.invsts) min_invsts, 
	max(ivs.invsts) max_invsts,	min(fm.cashgt) cashgt,
	min(lm.locsts) locsts, 	min(lm.pckflg) pckflg, 
	min(lm.arecod) arecod, 	min(lm.stoloc) stoloc, 
	min(nvl(ivs.untcas,0)) untcas, min(lm.useflg) useflg, 
	min(lm.lokcod) lokcod, 	sum(nvl(ivs.untqty,0)) untqty,
	sum(nvl(ivs.comqty,0)) comqty, sum(nvl(ivs.pndqty,0)) pndqty,
	'PF' path, 			max('') ship_line_id
	from ftpmst fm, prtmst pm, invsum ivs, locmst lm, rplcfg
	where rplcfg.stoloc = @dedicated_pf
	and rplcfg.stoloc = lm.stoloc and pm.prtnum = @prtnum
/* Dec 20 do we want to check for lotnum and untpak here?? */
	and fm.ftpcod = pm.ftpcod and rplcfg.stoloc = ivs.stoloc(+)
	group by lm.stoloc ] catch (@?)
    } /* endif for rplsts F,S,I */
    |
    if(@dedicated_pf = @stoloc) {
      publish data where pf_pndqty = @pndqty and pf_comqty = @comqty 
		and pf_untqty = @untqty
    } |
    if ((@reacod is NULL) and (@ship_line_id is not null)) {
      publish data 
	where reacod = 'Inventory is already picked for an order'
    } |
    if (@reacod is NULL and @cashgt > @pf_lochgt) {
      publish data where reacod = 'Pickface is not high enough for part'
    } |
    if (@path = 'NONPATH') {
      publish data 
	where reacod = 'Location is not in replenishment search path'
    } |
    if(@reacod is NULL and @pf_useflg=0 and @stoloc=@dedicated_pf) {
      publish data where reacod = 'Pickface location is unusable'
    } |
    if (@reacod is NULL and @useflg = 0) {
      publish data where reacod = 'Reserve location is unusable'
    } |
    if(@reacod is NULL and @pf_pckflg=0 and @stoloc=@dedicated_pf) {
      publish data where reacod = 'Pickface location is unpickable'
    } |
    if (@reacod is NULL and @pckflg = 0) {
      publish data where reacod = 'Reserve location is unpickable'
    } |
    if(@reacod is NULL and @pf_locsts='I' and @stoloc=@dedicated_pf) {
      publish data where reacod=
	'Pickface location is in error. Fix error and reset status'
    } |
    if (@reacod is NULL and @locsts = 'I') {
      publish data where reacod=
	'Reserve location is in error. Fix error and reset status'
    } |
    if (@reacod is NULL and @pf_cipflg=1 and @stoloc=@dedicated_pf) {
      publish data 
	where reacod = 'Pickface location has count in progress'
    } |
    if (@reacod is NULL and @cipflg = 1) {
      publish data 
	where reacod = 'Reserve location has count in progress'
    } |
    if (@reacod is NULL 
	and (@pf_invsts != @max_invsts or @pf_invsts != @min_invsts) 
	and @min_invsts is not NULL and @pf_invsts is not NULL) {
      publish data where reacod = 'Inventory Status in location '||
		'does not match pickface inventory status'
    } |
    /* NEED TO ADD 
    if (@reacod is NULL and @pf_untcas != @untcas 
	and @pf_untcas is not NULL and @untcas is not NULL) {
      publish data 
	where reacod='Unit per Case in locations do not match'
    } |
    END OF ADD NEED TO */
    if (@reacod is NULL and @loccod = 1) {
      publish data where reacod = 'Reserve location is locked'
    } |
    if (@reacod is NULL and @pf_loccod = 1) {
      publish data where reacod = 'Pickface location is locked'
    } |
    if(@reacod is NULL 
	and ((@untqty-@comqty)<=0) and (@untqty is not NULL)
	and (@untqty != 0) and (@stoloc != @dedicated_pf)) {
      publish data 
	where reacod = 'Reserve location inventory is commited'
    } |
    if((@dedicated_pf=@stoloc) 
	and (@reacod is NULL) and (@pf_pndqvl>0)) {
      publish data where reacod = 'Pickface has pending '||
		'inventory to it - move inventory to the pickface'
    } |
    if ((@dedicated_pf != @stoloc) and (@reacod is NULL) 
	and((@pf_maxqvl*@pf_erfpct/100)-@pf_curqvl+@pf_pndqvl)<@avlqty){
      publish data where reacod = 'Pickface Maximum will not '||
		'hold inventory - check the current pickface '||
		'quantity plus pending'
    } |
    if (@reacod is NULL and (@pf_untqty-@pf_comqty-@pf_pckqty)<=0 
	and @pf_untqty is not NULL and @pf_untqty != 0 
	and @stoloc = @dedicated_pf) {
      publish data 
	where reacod = 'Pickface location inventory is committed'
    } |
    if (@reacod is NULL and @rplsts = 'I') {
      publish data 
	where reacod = 'Waiting For Replenishment Manager to Run'
    } |
    publish data where prtnum = @prtnum 
	and lotnum = @lot 		and untpak = @pak
	and stoloc = @stoloc 		and arecod = @arecod
	and locsts = @locsts	 	and reacod = @reacod
	and vc_pf_stoloc=@dedicated_pf	and vc_pf_arecod = @pf_arecod 	
	and cipflg = @cipflg 		and pckflg = @pckflg 		
	and loccod = @loccod 		and vc_pf_pndqvl = @pf_pndqvl 	
	and vc_pf_curqvl = @pf_curqvl   and vc_pf_maxqvl = @pf_maxqvl 	
	and vc_avlqty = @avlqty 	and vc_pf_pndqty = @pf_pndqty	
	and vc_pf_comqty = @pf_comqty   and vc_pf_untqty = @pf_untqty  
  } /* end block for rplsts=F or S or I*/
  else if (@rplsts = 'D') { 	/* Pending Deposit */
    [select pw.wrkref, pw.srcloc, pw.pckqty, 
	wq.ack_usr_id, pw.prtnum, wq.oprcod, wq.reqnum, 
	pw.pcksts, pw.lblbat, pw.subucc
	from wrkque wq, pckwrk  pw
	where pw.wrktyp = 'R' and pw.prtnum = @prtnum
	and pw.lotnum = @lot and pw.untpak = @pak
	and pw.appqty != pw.pckqty and pw.lblbat = wq.lblbat(+)
	and not exists 
		(select 'x' from wrkque w2 where w2.wrkref=pw.wrkref)
     union
     select pw.wrkref, pw.srcloc, pw.pckqty, 
	wq.ack_usr_id, pw.prtnum, wq.oprcod, wq.reqnum, 
	pw.pcksts, pw.lblbat, pw.subucc
	from wrkque wq, pckwrk pw
	where pw.wrktyp = 'R' and pw.prtnum = @prtnum
	and pw.lotnum = @lot and pw.untpak = @pak
	and pw.wrkref = wq.wrkref (+)
	and not exists 
		(select 'x' from wrkque w2 where w2.lblbat=pw.lblbat)
    ] catch (@?) |
    if(@? = 0) {     
      if(@pcksts = 'P') {
	publish data where reacod = 'Pick status is Pending.' 
	  and wrkref=@wrkref and oprcod=@oprcod and reqnum=@reqnum 
	  and pcksts=@pcksts and srcloc=@srcloc and pckqty=@pckqty 
	  and prtnum=@prtnum and lotnum=@lot and untpak=@pak
      }
      else {
	if(@reqnum != '') {
	  publish data where reacod = 'Directed Work exists.'
	    and wrkref=@wrkref and srcloc=@srcloc and pckqty=@pckqty
	    and prtnum=@prtnum and lotnum=@lot and untpak=@pak
	    and oprcod=@oprcod and reqnum=@reqnum
	    and pcksts=@pcksts and ack_usr_id=@ack_usr_id
	}
	else {
	  if(@subucc is null) {
	    publish data where reacod='Directed Work not Created. '|| 
		' Check Replen Configuration' 
	        and wrkref=@wrkref and oprcod=@oprcod 
	        and reqnum=@reqnum and pcksts=@pcksts
          }
	  else {
	    publish data where 
		reacod = 'Replen Label exists for subucc='||@subucc
	        and wrkref=@wrkref and srcloc=@srcloc and pckqty=@pckqty
	        and prtnum=@prtnum and lotnum=@lot and untpak=@pak
		and pcksts=@pcksts
          }
        }
      }
    } 
    else { /* Dec19 MR1327 says lotnum not always stamped-we will punt*/
      [select count('x') pickCancelled from canpck 
	where prtnum=@prtnum and wrktyp='R'] 
      |
      if(@pickCancelled > 0) {
        publish data where ship_id=@ship_id and prtnum=@prtnum 
         and lotnum=@lot and untpak=@pak
	 and reacod = 'Replenishment Pick was Cancelled from '||
		' Pick Cancellation Operations, Cancel Replenishment.'
      }
      |
      [select count('x') wrkqueWorkCancelled from dlytrn 
	where actcod='WRKCAN' and movref=@lblbat] 
      |
      if(@wrkqueWorkCancelled > 0) {
	publish data where reacod = 
	  'Directed Work was Cancelled. Manually do the pick.' 
	  and wrkref=@wrkref and oprcod=@oprcod 
	  and reqnum=@reqnum and pcksts=@pcksts
      }  
      else {
	[select nxtloc_view.stoloc locationWithWork
	   from locmst lm, nxtloc_view, rplwrk rw
	   where rw.prtnum=@prtnum and nxtloc_view.prtnum=rw.prtnum
	   and rw.lotnum=@lot and nxtloc_view.lotnum=rw.lotnum
	   and rw.untpak=@pak and nxtloc_view.untpak=rw.untpak
  	   and lm.stoloc=nxtloc_view.pndloc and lm.arecod=rw.dstare
	] catch(@?) 
	|
	if (@locationWithWork is not NULL) {
	  [select il.lodnum, id.dtlnum, isb.subnum, 
		id.untqty, id.invsts, id.untcas
	   from invdtl id, invsub isb, invlod il
	   where il.stoloc=@locationWithWork
	   and il.lodnum=isb.lodnum and id.subnum=isb.subnum
	   and id.prtnum=@prtnum and id.lotnum=@lot and id.untpak=@pak
	  ] catch (@?) 
	  |
	  publish data where ship_id=@ship_id	and prtnum=@prtnum 
          	and lotnum=@lot and untpak=@pak
		and reacod='Replenishment inventory is in transition'
		and stoloc=@locationWithWork 	and lodnum=@lodnum 
		and subnum=@subnum and dtlnum=@dtlnum 
		and untqty=@untqty and untcas=@untcas and invsts=@invsts
	}
      }
    }
  } /* end block for rplsts of D */
} /* end the pickface lookup */

]]>
</local-syntax>
</command>
