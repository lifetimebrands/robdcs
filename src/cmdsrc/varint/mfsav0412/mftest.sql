SELECT distinct shipment.ship_id, '----', ord.ordnum,0,0,0, btcust, 
stcust, rtcust ffcust, '0000' shpseq, '0000' subsid, stop_id waybil, 
doc_num pronum, ordtyp, min(ord_line.late_shpdte) cpodte, ord.adddte, 
alcdte, stgdte, loddte, max(ord_line.early_shpdte) entdte, st.adrnam stname, 
st.adrln1 stadd1, st.adrln2 stadd2, st.adrln3 stadd3, st.adrcty stcity, 
st.adrstc ststcd, st.adrpsz stposc, st.CTRY_NAME stcnty, ff.adrnam ffname, 
ff.adrln1 ffadd1, ff.adrln2 ffadd2, ff.adrln3 ffadd3, ff.adrcty ffcity, 
ff.adrstc ffstcd, ff.adrpsz ffposc, ff.CTRY_NAME ffcnty, cponum,  
shipment.carcod,  shipment.cargrp,  shipment.shpsts, shipment.srvlvl,  
carnam cardsc, null, 'SALE', carflg, ord.bt_adr_id, ord.st_adr_id, ord.rt_adr_id, 
ord_line.early_shpdte, ord_line.early_dlvdte, ord_line.late_dlvdte
FROM    shipment, ord, ord_line, shipment_line, adrmst st, adrmst ff,  carhdr
WHERE shipment.ship_id = '1' and  ff.adr_id = ord.rt_adr_id  
        and st.adr_id = ord.st_adr_id  
        and carhdr.carcod = shipment.carcod 
        and shipment.ship_id = shipment_line.ship_id 
        and shipment_line.client_id = '----' 
        and shipment_line.ordnum = ord.ordnum
        and ord_line.ordlin = shipment_line.ordlin
        and ord.client_id = '----' 
        and ord_line.ordlin = (select min(ordlin) from ord_line where ordnum = 
	ord.ordnum and client_id = '----'
        and ordnum=shipment_line.ordnum and ordlin=shipment_line.ordlin 
	and ordsln=shipment_line.ordsln)
        and ord.ordnum = ord_line.ordnum 
group by
shipment.ship_id, ord.ordnum, btcust, stcust, rtcust, stop_id, doc_num, 
ordtyp, ord.adddte, alcdte, stgdte, loddte, st.adrnam, 
st.adrln1, st.adrln2, st.adrln3, st.adrcty, st.adrstc, st.adrpsz, 
st.CTRY_NAME, ff.adrnam, ff.adrln1, ff.adrln2, ff.adrln3, 
ff.adrcty, ff.adrstc, ff.adrpsz, ff.CTRY_NAME, cponum,  
shipment.carcod,  shipment.cargrp,  shipment.shpsts, shipment.srvlvl,
carnam, carflg, ord.bt_adr_id, ord.st_adr_id, ord.rt_adr_id
,
ord_line.early_shpdte, ord_line.early_dlvdte, ord_line.late_dlvdte
/
