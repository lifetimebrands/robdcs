<command>
<name>create topoff replenishment</name>
<description>Create Topoff Replenishment</description>
<type>C Function</type>
<function>varCreateTopoffReplenishment</function>
<argument name="arecod" alias="area" datatype="string">
</argument>
<argument name="stoloc" datatype="string">
</argument>
<argument name="prtnum" alias="part" datatype="string">
</argument>
<argument name="prt_client_id" alias="client_id" datatype="string">
</argument>
<argument name="trace_suffix" datatype="string">
</argument>
<documentation>
<remarks>
<![CDATA[
  <p>
  This command will find locations that need to be filled with inventory, using
the values passed to the command and/or the policy setup for topoff 
replenishments. Once it has determined which locations need to be topped off,
it will generate replenishment picks to fill those locations.
  </p>

  <p> 
  If logging is enabled for topoff replenishments, a log file of the process will be created in the %LESDIR/LOGS directory with a standard filename of "Replen-Topoff.Sts". If a trace_suffix value is passed to the command, that value will be appended to the end of the standard filename. 
  </p>

  <p>
  Typically this command is run through a scheduled process, such as through
Schedule Agent Operations, or manually processed through Replenishment 
Configuration Maintenance.
  </p>

  <p>
  The command returns a list of replenishment picks that were generated.
  </p>
]]>
</remarks>
<retcol name="srcloc" type="COMTYP_CHAR">Source Location</retcol>
<retcol name="srcare" type="COMTYP_CHAR">Source Area</retcol>
<retcol name="dstloc" type="COMTYP_CHAR">Destination Location</retcol>
<retcol name="pckqty" type="COMTYP_CHAR">Pick QuantityDestination Area</retcol>
<retcol name="prtnum" type="COMTYP_CHAR">Part Number</retcol>
<retcol name="prt_client_id" type="COMTYP_CHAR">Part Client ID</retcol>
<retcol name="invsts" type="COMTYP_CHAR">Inventory Status</retcol>
<retcol name="wrkref" type="COMTYP_CHAR">Pick Work Reference</retcol>
<retcol name="cmbcod" type="COMTYP_CHAR">Combination Code</retcol>
<exception value="eOK">Normal successful completion</exception>
<exception value="eNO_MEMORY">Unable to allocate memory.</exception>
<exception value="eINT_NO_LOCS_TO_TOPOFF">If configured BY-INVSUM, there are no locations in the area to topoff. If configured BY-RPLCFG, there are no rplcfgs for the area.</exception>
</documentation>
</command>
