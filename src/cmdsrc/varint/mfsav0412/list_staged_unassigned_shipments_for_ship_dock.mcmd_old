<command>

<name>list staged unassigned shipments for ship dock</name>

<description>List Staged Unassigned Shipments For Ship Dock</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
    {
    list shipment staging areas
    |
    [select distinct sl.stoloc, shipment.ship_id, 
            ord.stcust, ord.cponum, stname adrnam, ffcust rtcust, ffname adrnam2, shipment.carcod, 
            shipment.srvlvl, shipment.shpsts, 
            ord.carflg, ord.client_id, ord.ordnum, 
            shipment.early_dlvdte, 
            shipment.late_dlvdte, shipment.early_shpdte, 
            shipment.late_shpdte 
       from locmst, var_shploc sl, var_shpord ord, 
            shipment,
            cardtl
      where shipment.ship_id        = ord.ship_id
	and ord.ship_id  	= sl.ship_id 
	and ord.client_id	= sl.client_id
	and ord.ordnum =	sl.ordnum
	and sl.stoloc 		= locmst.stoloc 
        and shipment.carcod         = cardtl.carcod
        and shipment.srvlvl         = cardtl.srvlvl
        and cardtl.cartyp <> 'S'
        and shipment.shpsts        in ('S','L', 'X') 
        and shipment.stgdte    is not null 
        and shipment.super_ship_flg = 0
        and shipment.stop_id       is null
        and locmst.arecod||'' = @arecod 
        and @+shipment.ship_id
        and @+ord.cponum
        and @+ord.ordnum
        and @+locmst.stoloc
        and @+shipment.carcod
        and @+shipment.srvlvl
        and @+ord.stcust
        and @+ord.btcust
        and @+shipment.late_dlvdte:date
        and @+shipment.shpsts 
        and @+shipment.early_dlvdte:date
        and @+shipment.late_shpdte:date
        and @+shipment.early_dlvdte:date
      order by 1,2] catch (-1403)
    }  >> res                   /* Save results to res (even if No Rows)     */
|
       append shipment change deferred flag 
       where res = @res 
        and status = @? catch(-1403) /* This will be either eOK or NO_ROWS */

]]>
</local-syntax>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the shipments that are not yet assigned to
  a stop. 
  It's primary purpose is for use by Ship Dock Operations, to allow users
  to see all shipments that area assigned to carrier moves, in the areas
  designated for use by Ship Dock Operations
  </p>
  <p>
  This command will show staged shipments in only those areas that are defined
  for use by Ship Dock Operations since this purpose of this command is
  for use by Ship Dock Operatios.
  This command assumes that the shipping structure has been defined for
  the shipment.
  </p>
  <p>
  Plese note the following:
     <li>Need to use numerics in the order by to support sql Server.</li>
     <li>We need to look at the stage date in addition to the status.  As of
         6.1, is it possible to have a shipment in a loading status whose 
         stage date is not set (due to fluid loading), so we still have
         to make sure the stage date is null.</li>

    <li>We need to exclude non-allocatable order lines.  Since they have
        a stgdte and stgqty, they "appear" like a standard order line for
        which the inventory does not currently exist in the staging area
        unless we exclude them.</li>

    <li>We need to exclude any shipments whose carrier has a carrier type
        of Small Package.  Manifest problems arise when a small package
        shipment gets closed from ship dock operations.</li>

    <li>This command should not have an @*.  Since it is used by Ship Dock
        we want to make sure we are not picking up arguments that do not
        belong to any of the tables listed in the join.  For example,
        ship dock may have car_move as a critiera field because it also
        has a list command to show shipments assigned to stops.  As a 
        result, those fields would be on the stack, but we dont not want to pick
        them up.  Therefore, each possible query argument needs to be 
        specified directly.</li>
  </p>

  <p>
  The second select handles the situation where a shipment
  may have at one time been staged, but there is currently
  no inventory in the staging area.
  For example, we have have an order that doesn't allow partials,
  but only part of it was picked, then the other pick was cancelled,
  so it got staged and loaded (may have been fluid loaded).
  Without the union, we would have no visibility to that shipment
  since there is currently no inventory in the staging lane.
  </p>

  <p>
  After all of that has been done, this command publishes the results from
  the initial selects and then appends the results with the deferred flag.
  The framework will initially call this command with a 1=2 in the where
  clause to insure NO ROWS in the data, but still expects to get the column
  headers.  We do the catch above because if we let it go the command would
  terminiate there and the framework would not see (and then not show) the
  deferred flag.  To make this work, it will do the catch on a NO ROWS, but
  then publish the status (@?), which will be either eOK or NO_ROWS from above.
  </p>

]]>
</remarks>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Specified shipment not found</exception>

<seealso cref="list shipment staging areas"> </seealso>
<seealso cref="list staged assigned shipments for ship dock"> </seealso>
<seealso cref="append shipment change deferred flag"> </seealso>

</documentation>
</command>
