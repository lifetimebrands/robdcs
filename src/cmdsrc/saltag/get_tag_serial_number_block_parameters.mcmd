<command>

<name>get tag serial number block parameters</name>
<description>Gets policies for serial number blocking.</description>
<argument name="prtnum" datatype="string">Part Number</argument>

<type>Local Syntax</type>
<local-syntax>
<![CDATA[

    /* ********************************************************************
     * We need to get the Request System, Request Mode, Block-Size, and
     * Block Threshold (Request Point)
     * *******************************************************************/

    /**********************************************************************
     * This command has two basic parts. The first gets the default
     * values and then the second gets any specific values for a prtnum.
     * any changes should be done in both parts.
     **********************************************************************/
     
    /* *******************************************************************
     *                     First get the default values.
     * *******************************************************************/

    /* Get the request system */ 
    [select rtstr2 sernum_request_system
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'REQUEST-SYSTEM'
        and rtstr1 = 'DEFAULT' ]
      catch (-1403)
    |

    /* Get the request method */
    [select rtstr2 sernum_request_method
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'REQUEST-METHOD'
        and rtstr1 = 'DEFAULT' ]
      catch (-1403)
    |

    /* Get the block size */
    [select rtnum2 sernum_block_size
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'BLOCK-SIZE'
        and rtstr1 = 'DEFAULT' ]
      catch (-1403)
    |
    if (@? != 0) publish data where sernum_block_size = 1000
    |

    /* Get the block threshold */
    [select rtnum2 sernum_block_threshold
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'BLOCK-THRESHOLD'
        and rtstr1 = 'DEFAULT' ]
      catch (-1403)
    |
    if (@? != 0) publish data where sernum_block_threshold = 200
    |
    
    publish data
      where sernum_request_system = @sernum_request_system
        and sernum_request_method = @sernum_request_method
        and sernum_block_size = @sernum_block_size
        and sernum_block_threshold = @sernum_block_threshold
    |    

    /* *******************************************************************
     *                Now get the part specific values.
     * *******************************************************************/

    /* 
     * See if the prtnum was passed it, or if we should use the default
     */
    if(!@prtnum or @prtnum = '')
    {
        publish data where prtnum='DEFAULT'
    }
    |

    /* Get the request system */
    [select rtstr2 request_system
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'REQUEST-SYSTEM'
        and rtstr1 = @prtnum ]
      catch (-1403)
    |
    if(@? = -1403)
    {
        /* No request system defined for this part, use default */
        publish data where request_system = @sernum_request_system
    }
    |

    /* Get the request method */
    [select rtstr2 request_method
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'REQUEST-METHOD'
        and rtstr1 = @prtnum ]
      catch (-1403)
    |
    if(@? = -1403)
    {
        /* No request method defined for this part, use default */
        publish data where request_method = @sernum_request_method
    }
    |

    /* Get the block size */
    [select rtnum2 block_size
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'BLOCK-SIZE'
        and rtstr1 = @prtnum ]
      catch (-1403)
    |
    if(@? = -1403)
    {
        /* No block size defined for this part, use default */
        publish data where block_size = @sernum_block_size
    }
    |

    /* Get the block threshold */
    [select rtnum2 block_threshold
       from poldat
      where polcod = 'RFID'
        and polvar = 'TAG-SERNUM'
        and polval = 'BLOCK-THRESHOLD'
        and rtstr1 = @prtnum ]
      catch (-1403)
    |
    if(@? = -1403)
    {
        /* No block threshold defined for this part, use default */
        publish data where block_threshold = @sernum_block_threshold
    }
    |
    /* Use the IP and port to define the site */
    get connection manager configuration
    |
    publish data where sitnam=@ip||':'||@port
    |
    publish data
      where prtnum = @prtnum
        and request_system = @request_system
        and request_method = @request_method
        and block_size = @block_size
        and block_threshold = @block_threshold
        and sitnam = nvl(@sitnam,'----')
        
]]>
</local-syntax>

<documentation>

<remarks>
<![CDATA[
 This command gets the configuration information for serial number block generation.
]]>
</remarks>

<retcol name="prtnum" type="string"> </retcol>
<retcol name="request_system" type="string"> </retcol>
<retcol name="request_method" type="string"> </retcol>
<retcol name="block_size" type="integer"> </retcol>
<retcol name="block_threshold" type="integer"> </retcol>

<exception value="eOK"> The command completed successfully.  </exception>

</documentation>
</command>
