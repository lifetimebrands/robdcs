<command>
<name>list usr my shipment info</name>
<description>list usr my shipment info</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
if (!@ordnum and !@ship_id and !@cponum)
  set return status where status = 91855
else
{
[select o.btcust,
        a.adrnam,
        o.ordnum,
        o.cponum,
        s.adddte shipment_adddte,
        s.alcdte shipment_alcdte,
        sl.ship_id,
        s.shpsts,
        s.wave_set,
        s.carcod,
        s.srvlvl,
        s.vc_shppri,
        nvl(s.uc_slsp_ind, 0) slsp_ind
   from adrmst a,
        ord o,
        ord_line ol,
        shipment_line sl,
        shipment s
  where a.adr_id = o.bt_adr_id
    and o.ordnum = ol.ordnum
    and o.client_id = ol.client_id
    and ol.ordnum = sl.ordnum
    and ol.client_id = sl.client_id
    and ol.ordlin = sl.ordlin
    and ol.ordsln = sl.ordsln
    and sl.ship_id = s.ship_id
    and @+o.cponum
    and @+o.ordnum
 /* and @+sl.schabt */
    and @+sl.ship_id
    and s.shpsts != 'B']
|
/* inventory */
{
    [select ' ' || x.stoloc || '/' || x.case_cnt || ' ' myinv
       from (select l.stoloc,
                    count(distinct s.subnum) case_cnt
               from shipment_line sl,
                    invdtl d,
                    invsub s,
                    invlod l
              where sl.ship_id = @ship_id
                and sl.ship_line_id = d.ship_line_id
                and d.subnum = s.subnum
                and s.lodnum = l.lodnum
              group by l.stoloc) x] catch(-1403) >> myres
    |
    if (@? = 0)
    {
        convert column results to string
         where res = @myres
           and colnam = 'myinv'
           and separator = '|'
        |
        publish data
         where inventory = @result_string
    }
}
|
/*carting group priority */
[select *
   from usr_cust_carrier_priorities v
  where btcust = @btcust
    and my_carcod = @carcod
    and rownum < 2] catch(-1403)
|
{
    /*shipment cart ids */
    [select uw.cartman
       from pckwrk pw,
            usr_cartpicks up,
            usr_cartwrk uw
      where pw.ship_id = @ship_id
        and pw.wrkref = up.wrkref
        and up.cartid = uw.cartid] catch(-1403) >> myres2
    |
    if (@? = 0)
    {
        convert column results to string
         where res = @myres2
           and colnam = 'cartman'
           and separator = '|'
        |
        publish data
         where carts = @result_string
    }
}
|
/* case picks */
[select count(pw.wrkref) case_picks
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'S'
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 0]
|
/* case picks picked*/
[select count(pw.wrkref) case_picks_picked
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'S'
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 0
    and pw.pckqty != appqty]
|
/* kit picks */
[select count(pw.wrkref) kit_picks
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'S'
    and pw.wrktyp = 'K'
    and nvl(pw.lodflg, 0) = 0]
|
/* kit picks picked*/
[select count(pw.wrkref) kit_picks_picked
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'S'
    and pw.wrktyp = 'K'
    and nvl(pw.lodflg, 0) = 0
    and pw.pckqty != appqty]
|
/* load picks */
[select count(pw.cmbcod) load_picks
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 1]
|
/* load picks picked*/
[select count(pw.cmbcod) load_picks_picked
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 1
    and pw.pckqty != appqty]
|
/* detail picks */
[select count(pw.wrkref) detail_picks
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'D'
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 0]
|
/* detail picks picked*/
[select count(pw.wrkref) detail_picks_picked
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.lodlvl = 'D'
    and pw.wrktyp = 'P'
    and nvl(pw.lodflg, 0) = 0
    and pw.pckqty != appqty]
|
/* label batch*/
[select max(pw.lblbat) lblbat
   from pckwrk pw
  where pw.ship_id = @ship_id
    and pw.wrktyp = 'P']
|
[select max(usr_id) myuser
  from dlytrn d
  where d.trndte > trunc(sysdate) - 5
  and d.movref = @ship_id
  and d.to_subnum like 'CTN%'
  and d.actcod = 'S PICK'
  and d.lodnum like 'W%'
  and d.usr_id is not null]
|
publish data
 where btcust = @btcust
   and adrnam = @adrnam
   and ordnum = @ordnum
   and cponum = @cponum
   and shipment_adddte = @shipment_adddte
   and shipment_alcdte = @shipment_alcdte
   and ship_id = @ship_id
   and shpsts = @shpsts
   and wave_set = @wave_set
   and carcod = @carcod
   and srvlvl = @srvlvl
   and slsp_ind = @slsp_ind
   and carting_group = nvl(@vc_shppri,@priority)
   and carts = @carts
   and inventory = @inventory
   and lblbat = @lblbat
   and case_picks = @case_picks_picked
   and case_picks_picked = @case_picks_picked
   and kit_picks = @kit_picks
   and kit_picks_picked = @kit_picks_picked
   and detail_picks = @detail_picks
   and detail_picks_picked = @detail_picks_picked
   and load_picks = @load_picks
   and load_picks_picked = @load_picks_picked
   and usr_id = @myuser
}
]]>
</local-syntax>
</command>
