<command>
<name>allocate location</name>
<description>allocate location</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
publish data where v_hotexpr_flg = 0
|
if(@lodnum) {
  get usr info for identified load where lodnum = @lodnum
  |
  if(@v_switch_status_flg = 1 and @v_rcv_curare = 'EXPR' and @v_flippy_module_flg = 1) 
      change usr receive status for load where lodnum = @lodnum and v_switch_status = @v_switch_status
  |
  if(@v_rcv_load_exists_flg = 1 and @v_rcv_curare = 'EXPR' and @v_idn_load_at_trkloc_flg = 1 
           and @v_idn_parts_on_load = 1 and @v_load_in_process_flg = 0 and @v_hotitm_flg = 1 and @v_crossdk_module_flg = 1) {      

      /* so recursive call will not do this and allocate a location, value derived
          in get usr info for identified load and put in v_load_in_process_flg */
      save session variable where name = 'RCV_LOAD_IN_PROCESS' and value = '1'
      |
      check usr item for priority qa where lodnum = @lodnum
      |
      if(@v_qa_loc_needed = 1 or @v_pf_allocated_flg = 1) {
          assign usr identified load to qa lane where lodnum = @lodnum catch(@?)
          |
          if(@? = 0 and @v_qa_lane != null) {
            [update invmov set seqnum = seqnum + 1 where lodnum = @lodnum]
            |
            [insert into invmov( lodnum, lodlvl, seqnum, stoloc, xdkqty ) values( @lodnum, 'L', 0, @v_qa_lane, 0)]
            |
            [update locmst set pndqvl = pndqvl + 1 where stoloc = @v_qa_lane]
            |
            write daily transaction 
            where lodnum = @v_rcv_trknum 
               and actcod = 'RCVHOT'
               and adj_ref1 = 1 
               and adj_ref2 = @v_inv_rcvkey 
               and to_lodnum = @lodnum
          }
          |
          publish data 
            where v_hotexpr_flg = 1 
                and v_qa_loc_needed = @v_qa_loc_needed 
                and v_pf_allocated_flg = @v_pf_allocated_flg
                and v_qa_lane = @v_qa_lane
      }
      |
      [select stoloc v_lstdst,
                 case 
                      when @v_hotexpr_flg = 0 then 'NOT HOT' 
                      when @v_hotexpr_flg != 0 and nvl(@v_pf_allocated_flg, 0) != 0 then 'HOT FPF ALC'
                      when @v_hotexpr_flg != 0 and nvl(@v_qa_loc_needed, 0) != 0 then 'HOT NO FPF ALC'
                      else 'HOT ERROR'
                 end v_statmsg 
          from (
               select * 
                  from invmov 
               where lodnum = @lodnum
                 order by seqnum desc )
       where rownum < 2] catch(@?)
      |
      write daily transaction 
    where lodnum = @lodnum
        and actcod = 'RCVLOAD'
        and movref = @v_inv_rcvkey
        and trlr_num = @v_rcv_trknum
        and prtnum = @v_rcv_prtnum
        and lotnum = @v_rcv_lotnum
        and frinvs = @v_inv_invsts
        and trnqty = @v_inv_totqty
        and frstol = @v_rcv_curloc
        and frarecod = @v_rcv_curare
        and subnum = @v_inv_not_in_qasts_cnt
        and dtlnum = @v_inv_in_qa_priloc_cnt
        and adj_ref1 = @v_qa_lane
        and adj_ref2 = @v_lstdst
        and to_dtlnum = @v_hotexpr_flg          
        and to_subnum = @v_statmsg  catch(@?)
    |
    publish data where v_hotexpr_flg = @v_hotexpr_flg                  
  }
}
|
if(@v_hotexpr_flg = 0) {

    /* if we are allocating a replen to slow pick see if there is already enough available 
        if so get out */
    if (@arecod = 'RACKSPCK' and @alctyp = 'REPLEN' and @prtnum and @lotnum and @invsts and !@stoloc)
    {
         [select nvl(sum((untqty - comqty) + pndqty),0) avail_qty
             from invsum  
          where arecod = @arecod    
              and prtnum = @prtnum
              and prt_client_id = '----'
              and invsts in (select invsts 
                                      from prgmst
                                   where invsts_prg = @invsts)]
         |
         [select nvl(sum(pckqty),0) qty_needed
            from rplwrk 
         where prtnum = @prtnum
             and invsts_prg = @invsts
             and lotnum = @lotnum
             and dstare in ('SSTG', 'FSTG')]
         |
         if (@avail_qty >= @qty_needed)
             set return status where status = -1403
    }
    |
    /* allocate location wrapper to save the part number */
    if (@prtnum)
        save session variable where name = 'location_prtnum' and value = @prtnum  >>my_res
    |
    ^allocate location
}

/* |
if (@? = -1403 and @type = 'REPLEN')
{
  commit
  |
  [select 1 commited_on_replen from dual]
  |
  set return status where status = -1403
}
else if (@? = -1403)
  set return status where status = -1403 */
]]>
</local-syntax>
</command>
