<command>
<name>handle usr cartonization pick work data request</name>
<description>handle usr cartonization pick work data request</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
publish data
 where handler_name = 'handle usr cartonization pick work data request'
|
if (@request = 'get pick list for shipment')
{
    get usr cartonization mode values
     where ship_id = @ship_id
    |
    publish data
     where r_name = 'get pick list for shipment'
    |
    {
        publish data
         where ctnz_request = @r_name
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_msg = 'extra_sql: ' || @extra_sql || '; select_this: ' || @select_this || '; no_res: ' || @no_res || ';'
    }
    |
    if (@is_live_flg = 1)
        publish data
         where pck_table = ' pckwrk '
    else
        publish data
         where pck_table = ' usr_pckwrk_ctnz '
    |
    publish data
     where extra_sql = nvl(@extra_sql, ' 1 = 1')
       and select_this = nvl(@select_this, ' pck_data.*,
            pck_data.pckqty - (pck_data.pack_units * pck_data.untpak) - (pck_data.case_units * pck_data.untcas) each_units,
            pck_data.case_units + pck_data.pack_units + pck_data.pckqty - (pck_data.pack_units * pck_data.untpak) - (pck_data.case_units * pck_data.untcas) total_units ')
    |
    publish data
     where x_sql = '[select ' || @select_this
    |
    publish data
     where x_sql = @x_sql || ' from (select pw.wrkref,
               pw.ship_id,
               pw.ordnum,
               pw.ordlin,
               pw.ordsln,
               pw.prtnum,
               pw.ftpcod,
               pw.untcas,
               pw.untpak,
               pw.pckqty,
               pw.srcare,
               case when pw.untcas > 1 then trunc (pw.pckqty / pw.untcas) else 0 end case_units,
               case when pw.untpak > 1 then trunc (mod (pw.pckqty, pw.untcas) / pw.untpak) else 0 end pack_units,
               case when pw.untpak > 1 then mod (mod (pw.pckqty, pw.untcas), pw.untpak) else mod (pw.pckqty, pw.untcas) end each_units,
               (ff.untlen * ff.untwid * ff.unthgt) ftp_each_cube,
               (ff.paklen * ff.pakwid * ff.pakhgt) ftp_pack_cube,
               (ff.caslen * ff.caswid * ff.cashgt) ftp_case_cube,
               ff.untlen,
               ff.untwid,
               ff.unthgt,
               ff.paklen,
               ff.pakwid,
               ff.pakhgt,
               ff.caslen,
               ff.caswid,
               ff.cashgt,
               prtm.usr_fragility_yn is_fragile,
               prtm.vc_grswgt_each * pw.pckqty pick_weight,
               nvl(prtm.usr_default_ctncod, ''NULL'') default_ctncod,
               prtm.usr_ctnz_envlp_code preferred_ctntyp,
               k.rpkcls
          from ftpmst ff,
               shipment sh,
               prtmst prtm,
               prtrpk k, ' || @pck_table || ' pw where pw.ship_id = @ship_id
       and pw.wrktyp = ''P''
       and pw.lodlvl = ''D''
       and pw.ctnnum is null
       and pw.appqty < pw.pckqty
       and sh.ship_id = pw.ship_id
       and ff.ftpcod = pw.ftpcod
       and prtm.prtnum = pw.prtnum
       and prtm.prt_client_id = pw.prt_client_id
       and k.prtnum(+) = prtm.prtnum
       and k.prt_client_id(+) = prtm.prt_client_id
       and not exists(select 1
                        from ' || @pck_table || ' z where z.wrkref = pw.wrkref
                         and z.ctnnum is not null)) pck_data
 where '
    |
    publish data
     where x_sql = @x_sql || @extra_sql || ']'
    |
    if (!@no_res = 1)
    {
        execute server command
         where cmd = @x_sql >> pick_list
    }
    else
        execute server command
         where cmd = @x_sql
}
else if (@request = 'get total picks and each qty')
{
    publish data
     where r_name = 'get total picks and each qty'
    |
    save session variable
     where name = 'pick_counter'
       and value = 0
    |
    save session variable
     where name = 'pick_qty_counter'
       and value = 0
    |
    {
        publish data combination
         where res = @pick_list
        |
        get session variable
         where name = 'pick_counter'
        |
        save session variable
         where name = 'pick_counter'
           and value = @value + 1
        |
        get session variable
         where name = 'pick_qty_counter'
        |
        save session variable
         where name = 'pick_qty_counter'
           and value = @value + @pckqty
    } >> tempRes
    |
    get session variable
     where name = 'pick_counter'
    |
    publish data
     where number_of_picks = @value
    |
    get session variable
     where name = 'pick_qty_counter'
    |
    publish data
     where total_pick_qty = @value
    |
    {
        publish data
         where ctnz_request = @r_name
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_flg = 2
           and ctnz_msg = 'Total Pick Qty: ' || @total_pick_qty || ' ; num_picks: ' || @number_of_picks
    }
    |
    publish data
     where total_pick_qty = @total_pick_qty
       and number_of_picks = @number_of_picks
}
else if (@request = 'get max med min')
{
    publish data
     where r_name = 'get max med min'
    |
    if (@pick_unit = 'CASE')
        [select caslen dim1,
                caswid dim2,
                cashgt dim3
           from ftpmst
          where ftpcod = @ftpcod]
    else if (@pick_unit = 'PACK' and @ftp_pack_cube + 0.000 > 0.5)
        [select paklen dim1,
                pakwid dim2,
                pakhgt dim3
           from ftpmst
          where ftpcod = @ftpcod]
    else
        [select untlen dim1,
                untwid dim2,
                unthgt dim3
           from ftpmst
          where ftpcod = @ftpcod]
    |
    get usr cartonization min med max data
    |
    {
        publish data
         where ctnz_request = @r_name
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_flg = 2
           and ctnz_msg = 'wrkref: ' || @wrkref || ' ; pick_unit:' || @pick_unit || ' ; pick_max:' || @maxdim || ' ; pick_med:' || @meddim || ' ; pick_min;' || @mindim || ' ; pckqty: ' || @pckqty || ' ; num_eaches: ' || @each_units || ' ; num_packs: ' || @pack_units || ' ; num_cases: ' || @case_units || ' ; untpak: ' || @untpak || ' ; untcas: ' || @untcas || ' ; pref_ctn: ' || @preferred_ctntyp || ' ; default_ctncod: ' || @default_ctncod || ' ; rpkcls: ' || @rpkcls || ' ;'
    }
    |
    get session variable
     where name = 'cur_pick_max_dim' catch(@?)
    |
    publish data
     where cur_pick_max_dim = nvl(@value, @maxdim)
    |
    get session variable
     where name = 'cur_pick_med_dim' catch(@?)
    |
    publish data
     where cur_pick_med_dim = nvl(@value, @meddim)
    |
    get session variable
     where name = 'cur_pick_min_dim' catch(@?)
    |
    publish data
     where cur_pick_min_dim = nvl(@value, @mindim)
    |
    publish data
     where cur_pick_max_dim = @cur_pick_max_dim
       and cur_pick_med_dim = @cur_pick_med_dim
       and cur_pick_min_dim = @cur_pick_min_dim
    |
    if (@maxdim + 0.0000 >= @cur_pick_max_dim + 0.0000)
    {
        save session variable
         where name = 'cur_pick_max_dim'
           and value = @maxdim
    }
    |
    if (@meddim + 0.0000 >= @cur_pick_med_dim + 0.0000)
    {
        save session variable
         where name = 'cur_pick_med_dim'
           and value = @meddim
    }
    |
    if (@mindim + 0.0000 >= @cur_pick_min_dim + 0.0000)
    {
        save session variable
         where name = 'cur_pick_min_dim'
           and value = @mindim
    }
    |
    get session variable
     where name = 'cur_pick_max_dim' catch(@?)
    |
    publish data
     where cur_pick_max_dim = nvl(@value, @maxdim)
    |
    get session variable
     where name = 'cur_pick_med_dim' catch(@?)
    |
    publish data
     where cur_pick_med_dim = nvl(@value, @meddim)
    |
    get session variable
     where name = 'cur_pick_min_dim' catch(@?)
    |
    publish data
     where cur_pick_min_dim = nvl(@value, @mindim)
    |
    {
        publish data
         where ctnz_request = @r_name
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_flg = 1
           and ctnz_msg = 'wrkref: ' || @wrkref || ' ; pick_unit: ' || @pick_unit || ' ; ovrd_max: ' || @cur_pick_max_dim || ' ; ovrd_med: ' || @cur_pick_med_dim || ' ; ovrd_min: ' || @cur_pick_min_dim || ' ;'
    }
    |
    publish data
     where pick_max_dim = @cur_pick_max_dim
       and pick_med_dim = @cur_pick_med_dim
       and pick_min_dim = @cur_pick_min_dim
}
else
    set return status
     where status = 99987
]]>
</local-syntax>
</command>
