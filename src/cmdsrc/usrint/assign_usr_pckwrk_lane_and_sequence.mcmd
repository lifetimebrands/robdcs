<command>
<name>assign usr pckwrk lane and sequence</name>
<description>assign usr pckwrk lane and sequence where ship_id = @ship_id and needs_slotting = 0/1</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
if (!@ship_id or !@needs_slotting)
{
    set return status
     where status = -1403
}
|
if (@needs_slotting = 1)
{
    /** Get Pallet Volume to Use **/
    get usr pallet volume to use for sid
     where ship_id = @ship_id
    |
    /** Save off some session vars */
    save session variable
     where name = 'CURRENT-VOLUME'
       and value = 0
    |
    save session variable
     where name = 'HOLD-LANE'
       and value = 0
    |
    save session variable
     where name = 'HOLD-PART'
       and value = 'NONE'
    |
    save session variable
     where name = 'CURRENT-LANE'
       and value = 0
    |
    save session variable
     where name = 'CURRENT-SLOT'
       and value = 'NONE'
    |
    save session variable
     where name = 'PALLET-VOLUME'
       and value = @pallet_volume
    |
    save session variable
     where name = 'CUSTOM-FLAG'
       and value = @custom_slotting_flag
    |
    save session variable
     where name = 'LANES-AVAILABLE-FLAG'
       and value = 'YES'
    |
    if (@sid_is_singular_flg = 1)
    {
        [select stoloc slot_loc,
                to_number(substr(stoloc, 5, 3)) slot_lane
           from locmst lm
          where lm.arecod = 'PALLET'
            and lm.rescod = @ship_id
          for update] catch(@?)
        |
        if (@? = 0)
        {
            save session variable
             where name = 'CURRENT-LANE'
               and value = @slot_lane
            |
            save session variable
             where name = 'CURRENT-SLOT'
               and value = @slot_loc
            |
            publish data
             where needs_slotting = 99
        }
        |
        publish data
         where needs_slotting = @needs_slotting
    }
}
/** end if needs_slotting = 1 **/
|
/** List picks to sequencd **/
list usr picks to sequence
|
get session variable
 where name = 'LANES-AVAILABLE-FLAG' catch(@?)
|
publish data
 where lanes_available_flag = @value
|
if (@needs_slotting = 1 and @lanes_available_flag = 'YES')
{
    /** Get stored variables needed **/
    get session variable
     where name = 'CURRENT-VOLUME' catch(@?)
    |
    publish data
     where current_volume = @value
    |
    get session variable
     where name = 'CURRENT-SLOT' catch(@?)
    |
    publish data
     where current_slot = @value
    |
    get session variable
     where name = 'CURRENT-LANE' catch(@?)
    |
    publish data
     where current_lane = @value
    |
    get session variable
     where name = 'HOLD-LANE' catch(@?)
    |
    publish data
     where hold_lane = @value
    |
    get session variable
     where name = 'PALLET-VOLUME' catch(@?)
    |
    publish data
     where pallet_volume = @value
    |
    /** Have a break point on shipment_line, try to use the lane that was already assigned during release **/
    if (@hold_lane != @break_key)
    {
        get usr lane and slot for assignment
         where vc_lane = @vc_wave_lane
           and use_lane = 0
           and in_ship_id = @ship_id
        |
        save session variable
         where name = 'LANES-AVAILABLE-FLAG'
           and value = decode(@slot_lane, 0, 'NO', 'YES')
        |
        save session variable
         where name = 'HOLD-LANE'
           and value = @break_key
        |
        save session variable
         where name = 'CURRENT-LANE'
           and value = @slot_lane
        |
        save session variable
         where name = 'CURRENT-SLOT'
           and value = @slot_loc
        |
        save session variable
         where name = 'CURRENT-VOLUME'
           and value = 0
        |
        publish data
         where current_lane = @vc_wave_lane
           and current_shipment_line = @ship_line_id
           and current_lane = @slot_lane
           and current_slot = @slot_loc
           and current_volume = 0
           and lanes_available_flag = decode(@slot_lane, 0, 'NO', 'YES')
    }
    |
    get session variable
     where name = 'LANES-AVAILABLE-FLAG' catch(@?)
    |
    publish data
     where lanes_available_flag = @value
    |
    get session variable
     where name = 'CUSTOM-FLAG' catch(@?)
    |
    publish data
     where custom_slotting_flag = @value
    |
    if (@custom_slotting_flag = 1)
    {
        get session variable
         where name = 'HOLD-PART' catch(@?)
        |
        publish data
         where hold_prtnum = @value
        |
        if (@prtnum != @hold_prtnum)
        {
            if (@hold_prtnum != 'NONE')
            {
                list usr picks to sequence
                 where ship_id = @ship_id
                   and in_prtnum = @prtnum
                   and in_break_key = @break_key
                   and pre_calc_flg = 1
                |
                [select @prtnum,
                        @hold_prtnum,
                        case when @break_vol_out > (@pallet_volume - @current_volume) then 1
                             else 0
                        end item_break
                   from dual]
                |
                if (@item_break = 1)
                {
                    get usr lane and slot for assignment
                     where vc_lane = @current_lane
                       and use_lane = 0
                       and in_ship_id = @ship_id
                    |
                    save session variable
                     where name = 'LANES-AVAILABLE-FLAG'
                       and value = decode(@slot_lane, 0, 'NO', 'YES')
                    |
                    save session variable
                     where name = 'CURRENT-LANE'
                       and value = @slot_lane
                    |
                    save session variable
                     where name = 'CURRENT-SLOT'
                       and value = @slot_loc
                    |
                    save session variable
                     where name = 'CURRENT-VOLUME'
                       and value = 0
                    |
                    save session variable
                     where name = 'HOLD-PART'
                       and value = @prtnum
                    |
                    publish data
                     where current_slot = @slot_loc
                       and current_lane = @slot_lane
                       and current_volume = 0
                       and lanes_available_flag = decode(@slot_lane, 0, 'NO', 'YES')
                }
                else
                {
                    save session variable
                     where name = 'HOLD-PART'
                       and value = @prtnum
                }
            }
            else
            {
                save session variable
                 where name = 'HOLD-PART'
                   and value = @prtnum
            }
        }
    }
    |
    if (@lanes_available_flag = 'YES')
    {
        [select case when (@current_volume + @shpvol > @pallet_volume)
                 and to_number(@pallet_volume) != 9999999 then 1
                     else 0
                end pallet_break
           from dual]
        |
        if (@pallet_break = 1)
        {
            get usr lane and slot for assignment
             where vc_lane = @current_lane
               and use_lane = 0
               and in_ship_id = @ship_id
            |
            save session variable
             where name = 'LANES-AVAILABLE-FLAG'
               and value = decode(@slot_lane, 0, 'NO', 'YES')
            |
            save session variable
             where name = 'CURRENT-LANE'
               and value = @slot_lane
            |
            save session variable
             where name = 'CURRENT-SLOT'
               and value = @slot_loc
            |
            save session variable
             where name = 'CURRENT-VOLUME'
               and value = @shpvol
            |
            publish data
             where current_volume = @shpvol
               and current_slot = @slot_loc
               and current_lane = @slot_lane
               and lanes_available_flag = decode(@slot_lane, 0, 'NO', 'YES')
        }
        /** end if volumne > pallet_volumne **/
        else
        {
            /** otherwise we just tally volume **/
            save session variable
             where name = 'CURRENT-VOLUME'
               and value = @current_volume + @shpvol
            |
            publish data
             where current_volume = @current_volume + @shpvol
        }
    }
}
/* end if @needs_slotting = 1 */
|
if (@needs_slotting = 2)
{
    [select wrktyp x_typ
       from pckwrk
      where wrkref = @wrkref]
    |
    if (@x_typ = 'K')
    {
        publish data
         where needs_slotting = 0
    }
    |
    publish data
     where needs_slotting = @needs_slotting
}
|
if (@needs_slotting = 0)
{
    check usr parcel lane split catch(@?)
    |
    save session variable
     where name = 'CURRENT-LANE'
       and value = @vc_wave_lane
    |
    save session variable
     where name = 'CURRENT-SLOT'
       and value = '-'
    |
    check usr fedex lane split catch(@?)
    |
    if (@my_fedex_vc_wave_lane and @my_fedex_slot)
    {
        save session variable
         where name = 'CURRENT-LANE'
           and value = @my_fedex_vc_wave_lane
        |
        save session variable
         where name = 'CURRENT-SLOT'
           and value = @my_fedex_slot
    }
    |
    check usr usps lane split catch(@?)
    |
    if (@my_usps_vc_wave_lane and @my_usps_slot)
    {
        save session variable
         where name = 'CURRENT-LANE'
           and value = @my_usps_vc_wave_lane
        |
        save session variable
         where name = 'CURRENT-SLOT'
           and value = @my_usps_slot
    }
}
|
if (@needs_slotting = 2)
{
    get usr wmcom order type
     where wrkref = @wrkref
    |
    publish data
     where polval = 'LANE-SLOT-' || @wmcom_ord_type
    |
    /* Walmart dot com */
    [select rtstr1 x_current_lane,
            rtstr2 x_current_slot
       from poldat pd
      where pd.polcod = 'USR'
        and pd.polvar = 'WMCOM'
        and pd.polval = @polval]
    |
    save session variable
     where name = 'CURRENT-LANE'
       and value = @x_current_lane
    |
    save session variable
     where name = 'CURRENT-SLOT'
       and value = @x_current_slot
}
|
if (@needs_slotting = 3)
{
    [select rtstr1 x_current_slot,
            rtnum1 x_current_lane
       from poldat
      where polcod = 'USR-SLOTTING'
        and polvar = 'SINGLE-BOX-PALLET'
        and polval = 'LOCATION'
        and nvl(rtnum2, 0) = 1
        and rownum < 2] catch(@?)
    |
    if (@? = 0)
    {
        save session variable
         where name = 'CURRENT-LANE'
           and value = @x_current_lane
        |
        save session variable
         where name = 'CURRENT-SLOT'
           and value = @x_current_slot
        |
        {
            [select wrkref,
                    wrktyp,
                    subnum xsubn,
                    ship_id xsid
               from pckwrk
              where wrkref = @wrkref]
            |
            [update locmst
                set rescod = null
              where rescod = nvl(@xsid, 'FOO')
                and arecod = 'SSTG'] catch(@?)
            |
            [update pckmov
                set stoloc = 'SHIP-SINGLE'
              where cmbcod in (select cmbcod
                                 from pckwrk pw
                                where pw.wrkref = @wrkref)]
            |
            if (@wrktyp = 'K')
            {
                [update pckmov
                    set stoloc = 'SHIP-SINGLE'
                  where cmbcod in (select cmbcod
                                     from pckwrk pw
                                    where pw.ship_id = nvl(@xsid, 'FOO')
                                      and pw.ctnnum = nvl(@xsubn, 'FOO')
                                      and lodlvl = 'D')]
            }
        }
    }
}
|
get session variable
 where name = 'CURRENT-SLOT' catch(@?)
|
publish data
 where current_slot = @value
|
get session variable
 where name = 'CURRENT-LANE' catch(@?)
|
publish data
 where current_lane = @value
|
if (@current_lane > 0)
{
    [update pckwrk
        set uc_pck_seqnum = @current_slot,
            vc_pck_wave_lane = @current_lane
      where wrkref = @wrkref]
    |
    {
        write wave info to host
         where wrkref = @wrkref catch(@?)
        |
        if (@? != 0)
        {
            save session variable
             where name = 'STAT-MSG'
               and value = 'WWITHFAIL'
        }
        else
        {
            save session variable
             where name = 'STAT-MSG'
               and value = 'WWITHOK'
        }
    } >> myres1
}
else
{
    save session variable
     where name = 'STAT-MSG'
       and value = 'NO-LANES'
}
|
get session variable
 where name = 'PALLET-VOLUME' catch(@?)
|
publish data
 where pallet_volume = @value
|
get session variable
 where name = 'STAT-MSG' catch(@?)
|
publish data
 where wwith_msg = @value
|
get session variable
 where name = 'CUSTOM-FLAG' catch(@?)
|
publish data
 where custom_slotting_flag = @value
|
[insert
   into dlytrn(trndte, actcod, oprcod, movref, trlr_num, carcod, fr_arecod, tostol, subnum, lodnum, ship_id, to_lodnum, adj_ref1, adj_ref2, to_subnum, to_dtlnum, usr_id, devcod, dtlnum, arcdte, trndtl_id)
 values (sysdate, 'SIDSEQASG', 'SIDSEQASG', @wrkref, @ship_line_id, @wrktyp, @vc_wave_lane, @stoloc, @break_key, @ship_id, @ship_id, @shpvol, @current_lane, @current_slot, @current_volume, @pallet_volume, @@usr_id, @@devcod, @wwith_msg, @ctl_date, @custom_slotting_flag)] catch(@?)
]]>
</local-syntax>
</command>
