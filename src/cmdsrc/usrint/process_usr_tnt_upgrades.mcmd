<command>
<name>process usr tnt upgrades</name>
<description>process usr tnt upgrades</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
[select distinct s.carcod my_up_carcod,
  o.cpodte my_up_cpodte,
  o.cponum,
  o.ordnum,
  a.adrpsz my_dst_zipcod
 from  cardtl c,
  shipment s, 
  shipment_line sl, 
  ord o, 
  adrmst a,
  poldat p1
 where c.carcod = s.carcod
  and c.srvlvl = s.srvlvl
  and s.ship_id = sl.ship_id 
  and sl.ordnum = o.ordnum
  and sl.client_id = o.client_id
  and o.rt_adr_id = a.adr_id
  and p1.polcod = 'USR'
  and p1.polvar = 'SHIP-METHOD-UPGRADE'
  and p1.polval = 'TNT-UPGRADE-CUSTOMERS'
  and p1.rtnum1 = 1
  and o.btcust||'' = p1.rtstr1
  and s.ship_id = @ship_id
  and c.cartyp = 'S'] catch (-1403)
|
if (@? = 0)
{
  check usr shipment country where ship_id = @ship_id
  |
  if (@stcnty = 'US' and @apo_fpo_flg = 0)
  {
    /* get the number of days to ship for the sid and
       make sure that the po date is within the po date range.  */
    [select p1.rtnum1 days_to_goal_delivery,
          nvl(p1.rtflt1, 0) dollar_minimum,
          (select p3.rtflt1
            from poldat p3
           where p3.polcod = 'USR' 
                and p3.polvar = 'SHIP-METHOD-UPGRADE' 
                and p3.polval = 'TNT-UPGRADE-PO-DATES-OVERRIDE' 
                and @my_up_cpodte >= trunc(to_date(p3.rtstr1, 'MM/DD/YYYY')) 
                and @my_up_cpodte <= trunc(to_date(p3.rtstr2, 'MM/DD/YYYY') + .99999) 
                and p3.rtnum1 = decode(@my_up_carcod, 'UPSS', 4, 'UPS3', 3, 'UPS2', 2, 9)) dollar_minimum_override
     from poldat p1,
          poldat p2
      where p1.polcod = 'USR'
       and p1.polvar = 'SHIP-METHOD-UPGRADE'
       and p1.polval = 'TNT-UPGRADE-DAYS-TO-GOAL-DELIVERY'
       and sysdate >= to_date(p1.rtstr1, 'MM/DD/YYYY HH24:MI:SS')
       and sysdate <= to_date(p1.rtstr2, 'MM/DD/YYYY HH24:MI:SS')
       and p2.polcod = 'USR' 
       and p2.polvar = 'SHIP-METHOD-UPGRADE' 
       and p2.polval = 'TNT-UPGRADE-PO-DATES'  
       and @my_up_cpodte >= trunc(to_date(p2.rtstr1, 'MM/DD/YYYY')) 
       and @my_up_cpodte <= trunc(to_date(p2.rtstr2, 'MM/DD/YYYY') + .99999) 
       and p2.rtnum1 = decode(@my_up_carcod, 'UPSS', 4, 'UPS3', 3, 'UPS2', 2, 9)] catch(-1403)
    |
    if (@?= 0)
    { 
       if (@dollar_minimum_override and @dollar_minimum_override != '')
       {
          [select distinct 1 from 
             shipment_line sl,
             ord o,
             dtcuser.po_header ph,
             dtcuser.po_details pd
            where sl.ordnum = o.ordnum 
              and sl.client_id = o.client_id
              and o.cponum = ph.customerponumber 
              and ph.billtoid = pd.billtoid
              and ph.customerponumber = pd.customerponumber
              and nvl(trim(pd.giftwrap),'N') = 'Y'
              and sl.ship_id = @ship_id] catch (-1403)
           |
           if (@? = 0)
             publish data where dollar_minimum = @dollar_minimum_override
           else 
             publish data where my_up_carcod  = 'UPSX'
       }
       |  
       /* if its ups then check the table to get the TNT */
       if (@my_up_carcod = 'UPSS')
       {
         [select vc_dlvdays dest_dvl_days
          from usr_ups_dlvdays
          where vc_to_dstzip = @my_dst_zipcod] catch (-1403)  
         |
         [select (nvl(@days_to_goal_delivery,0) - nvl(@dest_dvl_days,0)) on_time_difference from dual]
         |
         if(nvl(@on_time_difference,0) < 0)
         {
           /* see if the dollar value of what we are shipping is >= the minimum */
           [select distinct sid_value from 
            (select sum((sl.pckqty + sl.inpqty) * NVL(ol.vc_cust_untcst ,0)) sid_value,
              sl.ordnum,
              sl.ship_id 
             from shipment s, 
              shipment_line sl, 
              ord o, 
              ord_line ol
             where s.ship_id = sl.ship_id 
              and sl.ordnum = o.ordnum
              and sl.client_id = o.client_id
              and sl.ordnum = ol.ordnum 
              and sl.ordlin = ol.ordlin 
              and sl.client_id = ol.client_id 
              and sl.ordsln = ol.ordsln 
              and (ol.prjnum is null or (ol.prjnum is not null and ol.ordsln = '0000'))
              and s.ship_id = @ship_id
             group by   sl.ordnum,
              sl.ship_id)] catch (-1403)
            |
            if (@? = 0 and @sid_value >= @dollar_minimum)
            {
              [select * from 
                (select rtstr1 upgrade_to_carcod
                 from poldat p
                 where p.polcod = 'USR'
                  and p.polvar = 'SHIP-METHOD-UPGRADE'
                  and p.polval = 'TNT-UPGRADE-SHIP-METHODS'
                  and p.rtnum1 <= @days_to_goal_delivery
                 order by p.rtnum1 desc)
                 Where rownum < 2] catch (-1403)
              |
              if (@? = 0 and @upgrade_to_carcod)
                /* put shipment row update here also log a daily tran row*/
              {
                [update shipment set carcod = @upgrade_to_carcod where ship_id = @ship_id] 
                |
                write daily transaction where ship_id = @ship_id
                 and actcod = 'S CHANGE'
                 and carcod = @upgrade_to_carcod
                 and movref = 'from '||@my_up_carcod||' to '||@upgrade_to_carcod >> myres
                |
                publish data where upgraded_sid = 1
              }
            }
         } 
      }
      /* its not upss so see if we have to upgrade futher */ 
      else
      {
        [select p.rtnum1 exp_delivery_days
         from poldat p
         where p.polcod = 'USR'
          and p.polvar = 'SHIP-METHOD-UPGRADE'
          and p.polval = 'TNT-UPGRADE-SHIP-METHODS'
          and p.rtstr1 = @my_up_carcod] catch (-1403)
        | 
        if (@? = 0)
        {
          [select (nvl(@days_to_goal_delivery,0) - nvl(@exp_delivery_days,0)) exp_delivery_days_difference from dual] 
          |
          if(nvl(@exp_delivery_days_difference,0) < 0)
          {
            /* 
            [select distinct sid_value from 
            (select sum((sl.pckqty + sl.inpqty) * NVL(ol.vc_cust_untcst ,0)) sid_value,
              sl.ordnum,
              sl.ship_id 
             from shipment s, 
              shipment_line sl, 
              ord o, 
              ord_line ol
             where s.ship_id = sl.ship_id 
              and sl.ordnum = o.ordnum
              and sl.client_id = o.client_id
              and sl.ordnum = ol.ordnum 
              and sl.ordlin = ol.ordlin 
              and sl.client_id = ol.client_id 
              and sl.ordsln = ol.ordsln 
              and (ol.prjnum is null or (ol.prjnum is not null and ol.ordsln = '0000'))
              and s.ship_id = @ship_id
             group by   sl.ordnum,
              sl.ship_id)] catch (-1403)
            |
            we dont care what the dollar value is for expedite we want to get it there on time no matter what  */
            publish data where dollar_minimum = 0
            |
            if (@? = 0 and @sid_value >= @dollar_minimum)
            {
              [select * from 
                (select rtstr1 upgrade_to_carcod
                 from poldat p
                 where p.polcod = 'USR'
                  and p.polvar = 'SHIP-METHOD-UPGRADE'
                  and p.polval = 'TNT-UPGRADE-SHIP-METHODS'
                  and p.rtnum1 <= @days_to_goal_delivery
                 order by p.rtnum1 desc)
                 Where rownum < 2] catch (-1403)
              |
              if (@? = 0 and @upgrade_to_carcod)
                /* put shipment row update here also log a daily tran row */
              {
                [update shipment set carcod = @upgrade_to_carcod where ship_id = @ship_id] 
                |
                write daily transaction where ship_id = @ship_id
                 and actcod = 'S CHANGE'
                 and carcod = @upgrade_to_carcod
                 and movref = 'from '||@my_up_carcod||' to '||@upgrade_to_carcod >> myres
                |
                publish data where upgraded_sid = 1
              }
            } 
          }
        }
      }  
    }      
  }
}
|
publish data where upgraded_sid = nvl(@upgraded_sid,0)
]]>
</local-syntax>
</command>
