<command>

<name>list shipments for display</name>

<description>List Shipments For Display</description>

<type>Local Syntax</type>

<local-syntax>
<![CDATA[
[select distinct shipment.*,
                stop.stop_nam,
                stop.stop_seq,
                stop.car_move_id,
		adrmst1.adrnam ship_to_custname,
		adrmst2.adrnam route_to_custname 
           from adrmst adrmst1, adrmst adrmst2, ord,
                stop,
                shipment_line sl, 
                shipment 
          where sl.ship_id   = shipment.ship_id 
            and sl.ordnum    = ord.ordnum
            and ord.st_adr_id = adrmst1.adr_id
            and ord.rt_adr_id = adrmst2.adr_id
            and sl.client_id = ord.client_id
            and stop.stop_id = shipment.stop_id
            and @+sl.ordnum
            and @+sl.schbat
            and @+sl.client_id
            and @+shipment.carcod
            and @+shipment.srvlvl
            and @+shipment.doc_num
            and @+shipment.track_num
            and @+shipment.ship_id
            and @+shipment.stop_id
            and @+shipment.adddte:date
            and @+shipment.alcdte:date
            and @+shipment.stgdte:date
            and @+shipment.loddte:date
            and @+shipment.entdte:date
            and @+shipment.early_shpdte:date
            and @+shipment.late_shpdte:date
            and @+shipment.early_dlvdte:date
            and @+shipment.late_dlvdte:date
            and @*
       union  
        select distinct shipment.*,
                null stop_nam,
                to_number(null) stop_seq,
                null car_move_id,
		adrmst1.adrnam ship_to_custname,
		adrmst2.adrnam route_to_custname 
           from adrmst adrmst1, adrmst adrmst2, ord,
                shipment_line sl, 
                shipment 
          where sl.ship_id   = shipment.ship_id 
            and ord.st_adr_id = adrmst1.adr_id
            and ord.rt_adr_id = adrmst2.adr_id
            and sl.ordnum    = ord.ordnum
            and sl.client_id = ord.client_id
            and shipment.stop_id is null
            and @+sl.ordnum
            and @+sl.schbat
            and @+sl.client_id
            and @+shipment.carcod
            and @+shipment.srvlvl
            and @+shipment.doc_num
            and @+shipment.track_num
            and @+shipment.ship_id
            and @+shipment.stop_id
            and @+shipment.adddte:date
            and @+shipment.alcdte:date
            and @+shipment.stgdte:date
            and @+shipment.loddte:date
            and @+shipment.entdte:date
            and @+shipment.early_shpdte:date
            and @+shipment.late_shpdte:date
            and @+shipment.early_dlvdte:date
            and @+shipment.late_dlvdte:date
	    and nvl(@car_move_id, '><><') = '><><'
	    and nvl(@stop_nam, '><><') = '><><'
	    and nvl(@stop_seq, '><><') = '><><'
            and @*
       order by 1] catch(-1403) >> res /* Save results to res
                                        * (even if No Rows)     */
        |
        /* Now publish the above results w/ deferred flag appended.
         * Status (@?) will be either eOK or NO_ROWS from above. We need this
         * because the framework will send a 1=2 in the where clause to
         * insure a NO ROWS but still needs to get the column headers.
         * We do the catch above because if we let it go, the framework would
         * not see (and then not show) the deferred flag.
         */
        append shipment change deferred flag
        where res = @res
          and status = @? catch(-1403)
        |
        if (@? = 0)
        {
            [select sum(pckqty) pckqty,
                    sum(inpqty) inpqty,
                    sum(stgqty) stgqty,
                    sum(shpqty) shpqty,
                    sum(oviqty) oviqty
               from shipment_line
              where shipment_line.ship_id = @ship_id]
            |
            filter data
             where moca_filter_level = 3
               and pckqty = @pckqty
               and inpqty = @inpqty
               and stgqty = @stgqty
               and oviqty = @oviqty
               and shpqty = @shpqty
        }
        else
        {
            /* NO ROWS - Need to return the column headers, though */
            [select distinct shipment.*,
                    stop.stop_nam,
                    stop.stop_seq,
                    stop.car_move_id,
                    to_number(null) def_shp_flg,
                    to_number(null) pckqty,
                    to_number(null) inpqty,
                    to_number(null) stgqty,
                    to_number(null) oviqty,
                    to_number(null) shpqty
               from ord,
                    stop,
                    shipment_line sl, 
                    shipment 
              where sl.ship_id   = shipment.ship_id 
                and sl.ordnum    = ord.ordnum
                and sl.client_id = ord.client_id
                and stop.stop_id = shipment.stop_id
                and @+shipment.stop_id
                and @+stop.stop_id
                and @+shipment.carcod
                and @+shipment.srvlvl
                and @+shipment.doc_num
                and @+shipment.track_num
                and 1 = 0]
        }

]]>
</local-syntax>

<argument name="@*">Any shipment header field value</argument>

<documentation>
<remarks>
<![CDATA[
  <p>
  This command is used to list the shipments. 
  It is primarily designed for use by the Shipment Display application.
  </p>

  <p>
  We have this separate command to list shipments, in order to allow the
  user to also query shipments based on order attributes.
  Although this query only includes shipment information, we may want
  to only return those shipments that have specific order attributes 
  associated with them.
  </p>
  <p>
  This command then calculates the sum of the various quantities on the
  shipment line.  Although it is a performace issue that we are querying
  against the shipment line twice, we did not want to include the sums
  in the original select, in order to let that command select all
  columns, including custom columns, which prevented using a group by.
  </p>
  <p>
  Note that this command does an order by on the first column. The assumption
  is that the first column will be a ship id.  However, we can not use
  "order by shipment.ship_id" because that statement fails in an Oracle
  database. So, we assume that ship_id will be the first column and just
  order by 1.
  </p>
]]>
</remarks>

<retcol name="(all shipment fields)" type="(varies according to field type)"> </retcol>

<exception value="eOK">Normal successful completion</exception>
<exception value="eDB_NO_ROWS_AFFECTED">Specified shipment not found</exception>

<seealso cref="list shipments"> </seealso>

</documentation>
</command>
