<command>
<name>process var usr weekly activity</name>

<description>process var usr weekly activity to creating a job</description>

<type>Local Syntax</type>

<local-syntax>

<![CDATA[

/* List locations which will be excluded for weekly activity calculation */
[select rtstr1 stoloc
   from poldat
  where polcod = 'VAR'
    and polvar = 'INVENTORY-BALANCING-RULE'
    and polval = 'EXCLUSION-LOCATION'] catch(-1403) >>res
|
if (@? = 0)
{
    convert column results to string
      where resultset = @res
        and colnam = 'stoloc'
        and separator = '%'' and pckwrk.srcloc not like ''%'
    |
    publish data where srcloc = 'pckwrk.srcloc not like ''%'||@result_string||'%'''
}
else
{
    publish data where srcloc = '1 = 1'
}
|
/* List the value for picks which can be thought as pallet value*/
[select rtnum1 pallel
   from poldat
  where polcod = 'VAR'
    and polvar = 'INVENTORY-BALANCING-RULE'
    and polval = 'PALLET-PICK-LEVEL'] catch(-1403)
|
if (@? = -1403)
{
    publish data where pallel = 70
}
|
/* List high volume customers which will be exclude for case picks*/
[select rtstr1 btcust
   from poldat
  where polcod = 'VAR'
    and polvar = 'INVENTORY-BALANCING-RULE'
    and polval = 'HIGH-VOLUME-CUSTOMER'
] catch(-1403) >>res
|
if (@? = 0)
{
    convert column results to string
      where resultset = @res
        and colnam = 'btcust'
    |
    convert list 
      where string = @result_string
        and type = 'S'
        and colnam = 'btcust'
}
|
if(@retstr)
{
    publish data where high_btcust = 'ord.btcust not in ('||@retstr||')'
}
else
{
    publish data where high_btcust = '1 = 1'
}
|
[select to_char(sysdate, 'YYYYIW') cur_yw from dual]
|
[select distinct to_char(pw.adddte, 'IW') week,
                 to_char(pw.adddte, 'YYYY') year
            from pckwrk pw
       left join shipment_line sl
              on pw.ship_id = sl.ship_id
             and pw.ship_line_id = sl.ship_line_id
       left join shipment sp
              on sp.ship_id = sl.ship_id
           where sp.shpsts != 'B'
             and sl.linsts != 'B'
             and to_char(pw.adddte, 'YYYYIW') != @cur_yw
        order by year desc,
                 week desc] catch(-1403)
|
if(@? = 0)
{
     [select count(1) week_cnt
        from usr_weeklyactivity
       where week = @week
         and year = @year] catch(-1403)
    |
    if(@week_cnt > 0)
    {
        publish data where year = null and week = null
    }
    else
    {
        publish data where year = @year and week = @week
    }
    |
    publish data where year = @year and week = @week
}
|
if ((@year and @year is not null) and (@week and @week is not null))
{
    [truncate table temp_usr_weekly] catch(@?)
    |
    /* sum case/unit in temp*/
    [select pckwrk.client_id,
            pckwrk.prtnum,
            sum(pckwrk.pckqty) as pckqty,
            pckwrk.ordnum,
            ord.btcust,
            ord_line.lotnum
       from shipment,
            shipment_line,
            ord_line,
            ord,
            pckwrk
      where shipment.ship_id = shipment_line.ship_id
        and shipment_line.client_id = ord_line.client_id
        and shipment_line.ordnum = ord_line.ordnum
        and shipment_line.ordlin = ord_line.ordlin
        and shipment_line.ordsln = ord_line.ordsln
        and ord.ordnum = ord_line.ordnum
        and ord.client_id = ord_line.client_id
        and pckwrk.ship_line_id = shipment_line.ship_line_id
        and shipment.shpsts != 'B'
        and shipment_line.linsts != 'B'
        and @srcloc:raw
        and @high_btcust:raw
        and pckwrk.lodflg = 0
        and pckwrk.prtnum != 'KITPART'
        and to_char(pckwrk.adddte, 'IWYYYY') = @week || @year
   group by pckwrk.client_id,
            pckwrk.ordnum,
            pckwrk.prtnum,
            ord.btcust,
            ord_line.lotnum] catch(-1403)
    |
    if (@? = 0)
    {
        [insert
           into temp_usr_weekly(client_id, prtnum, pckqty, ordnum, btcust, lotnum, lodflg, palflg)
         values (@client_id, @prtnum, @pckqty, @ordnum, @btcust, @lotnum, 0, 0)] catch(-1403)
    }
    ;
    /* lodflg = 1 in temp - if pckqty/untpal >= pallel/100, it is pallet pick; if pckqty/untpal < pallel/100, it is case pick*/
    [select pckwrk.client_id,
            pckwrk.prtnum,
            sum(pckwrk.pckqty) as pckqty,
            pckwrk.cmbcod,
            ord.btcust,
            ord_line.lotnum,
            case when mod(sum(pckwrk.pckqty / (prtmst.untpal *1.0)), 1) >= @pallel/100.0 
                   or sum(pckwrk.pckqty / (prtmst.untpal *1.0)) >= 1
                 then 1
                 else 0 
            end palflg
       from shipment,
            shipment_line,
            ord_line,
            ord,
            pckwrk,
            prtmst
      where shipment.shpsts != 'B'
        and to_char(pckwrk.adddte, 'IWYYYY') = @week || @year
        and shipment.ship_id = shipment_line.ship_id
        and shipment_line.linsts != 'B'
        and shipment_line.client_id = ord_line.client_id
        and shipment_line.ordnum = ord_line.ordnum
        and shipment_line.ordlin = ord_line.ordlin
        and shipment_line.ordsln = ord_line.ordsln
        and ord.ordnum = ord_line.ordnum
        and ord.client_id = ord_line.client_id
        and pckwrk.ship_line_id = shipment_line.ship_line_id
        and prtmst.prtnum = pckwrk.prtnum
        and @srcloc:raw
        and @high_btcust:raw
        and pckwrk.prtnum != 'KITPART'
        and pckwrk.lodflg = 1
   group by pckwrk.client_id,
            pckwrk.cmbcod,
            pckwrk.prtnum,
            ord.btcust,
            ord_line.lotnum] catch(-1403)
    |
    if (@? = 0)
    {
        [insert
           into temp_usr_weekly(client_id, prtnum, pckqty, ordnum, btcust, lotnum, lodflg, palflg)
         values (@client_id, @prtnum, @pckqty, @cmbcod, @btcust, @lotnum, 1, @palflg)] catch(-1403)
    }
    ;
    /* Deal with lodflg = 0 for case and unit in weekly activity*/
    [select distinct prtnum,
            client_id
       from temp_usr_weekly
      where (lodflg = 0 or (lodflg = 1 and palflg = 0))] catch(-1403)
    |
    if (@? = 0)
    {
        [select untcas
           from prtmst
          where prtnum = @prtnum] catch(@?)
        |
        [select sum(a.unit_qty) unt_qty,
                sum(a.case_qty) case_qty,
                prtnum,
                btcust,
                lotnum
           from (select ordnum,
                        prtnum,
                        btcust,
                        lotnum,
                        sum(mod(pckqty, @untcas)) unit_qty,
                        sum(floor(pckqty / (@untcas *1.0))) case_qty
                   from temp_usr_weekly
                  where prtnum = @prtnum
                    and client_id = @client_id
                    and (lodflg = 0 or (lodflg = 1 and palflg = 0))
                  group by ordnum,
                        prtnum,
                        btcust,
                        lotnum) a
          group by prtnum,
                   btcust,
                   lotnum] catch(@?)
        |
        [insert
           into usr_weeklyactivity(client_id, prtnum, year, week, btcust, lotnum, case_qty, unt_qty, pal_qty)
         values (@client_id, @prtnum, @year, @week, @btcust, @lotnum, @case_qty, @unt_qty, 0)] catch(-1403)
    }
    ;
    /* update palqty lodflg = 1 and palflg in weekly activity*/
    [select distinct prtnum,
            client_id
       from temp_usr_weekly
      where lodflg = 1 and palflg = 1] catch(-1403)
    |
    if (@? = 0)
    {
        [select untpal
           from prtmst
          where prtnum = @prtnum] catch(@?)
        |
        [select sum(a.pal_qty) pal_qty,
                prtnum,
                btcust,
                lotnum
           from (select ordnum,
                        prtnum,
                        btcust,
                        lotnum,
                        ceil(sum(pckqty / (@untpal *1.0))) pal_qty
                  from temp_usr_weekly
                 where prtnum = @prtnum
                   and client_id = @client_id
                   and lodflg = 1
                   and palflg = 1
              group by ordnum,
                       prtnum,
                       btcust,
                       lotnum) a
       group by prtnum,
                btcust,
                lotnum]
        |
        [update usr_weeklyactivity
            set pal_qty = @pal_qty
          where prtnum = @prtnum
            and week = @week
            and year = @year
            and btcust = @btcust
            and lotnum = @lotnum] catch(-1403)
    }
    ;
    /* update total quantity*/
    [select distinct prtnum,
            client_id
       from temp_usr_weekly]catch(-1403)
    |
    if (@? = 0)
    {
        [select prtnum,
                btcust,
                lotnum,
                sum(pckqty) totqty
           from temp_usr_weekly
          where prtnum = @prtnum
            and client_id = @client_id
       group by prtnum,
                btcust,
                lotnum]
        |
        [update usr_weeklyactivity
            set totqty = @totqty
          where prtnum = @prtnum
            and week = @week
            and year = @year
            and btcust = @btcust
            and lotnum = @lotnum] catch(-1403)
    
    }
    ;
    /* update seq_case_qty in weekly activity*/
    [select distinct week,
            year
       from usr_weeklyactivity
      where seq_case_qty is null] catch(-1403)
    |
    [select case_qty,
            prtnum,
            btcust,
            lotnum
       from usr_weeklyactivity
      where week = @week
        and year = @year
      order by case_qty] catch(-1403)
    |
    publish data
     where seq_case_qty = 0
    |
    if (@case_qty > 0)
    {
        [select max(nvl(seq_case_qty, 0)) max_seq_case_qty
           from usr_weeklyactivity
          where case_qty > 0
            and year = @year
            and week = @week] catch(@?)
        |
        if (@max_seq_case_qty = 0)
        {
            publish data
             where seq_case_qty = 1
        }
        else
        {
            [select max(seq_case_qty) seq_case_qty
               from usr_weeklyactivity
              where case_qty = @case_qty
                and seq_case_qty is not null
                and year = @year
                and week = @week
                and rownum < 2] catch(@?)
            |
            if (@seq_case_qty = 0)
            {
                publish data
                 where seq_case_qty = @max_seq_case_qty + 1
            }
            |
            publish data
             where seq_case_qty = @seq_case_qty
        }
        |
        publish data
         where seq_case_qty = @seq_case_qty
    }
    |
    [update usr_weeklyactivity
        set seq_case_qty = @seq_case_qty
      where prtnum = @prtnum
        and week = @week
        and year = @year
        and btcust = @btcust
        and lotnum = @lotnum] catch(-1403)
    ;
    /* update seq_unt_qty in weekly activity*/
    [select distinct week,
            year
       from usr_weeklyactivity
      where seq_unt_qty is null] catch(-1403)
    |
    [select unt_qty,
            prtnum,
            btcust,
            lotnum
       from usr_weeklyactivity
      where week = @week
        and year = @year
      order by unt_qty] catch(-1403)
    |
    publish data
     where seq_unt_qty = 0
    |
    if (@unt_qty > 0)
    {
        [select nvl(max(seq_unt_qty), 0) max_seq_unt_qty
           from usr_weeklyactivity
          where unt_qty > 0
            and year = @year
            and week = @week] catch(@?)
        |
        if (@max_seq_unt_qty = 0)
        {
            publish data
             where seq_unt_qty = 1
        }
        else
        {
            [select max(seq_unt_qty) seq_unt_qty
               from usr_weeklyactivity
              where unt_qty = @unt_qty
                and seq_unt_qty is not null
                and year = @year
                and week = @week
                and rownum < 2] catch(@?)
            |
            if (@seq_unt_qty = 0)
            {
                publish data
                 where seq_unt_qty = @max_seq_unt_qty + 1
            }
            |
            publish data
             where seq_unt_qty = @seq_unt_qty
        }
        |
        publish data
         where seq_unt_qty = @seq_unt_qty
    }
    |
    [update usr_weeklyactivity
        set seq_unt_qty = @seq_unt_qty
      where prtnum = @prtnum
        and week = @week
        and year = @year
        and btcust = @btcust
        and lotnum = @lotnum] catch(-1403)
    ;
    /* update seq_pal_qty in weekly activity*/
    [select distinct week,
            year
       from usr_weeklyactivity
      where seq_pal_qty is null] catch(-1403)
    |
    [select pal_qty,
            prtnum,
            btcust,
            lotnum
       from usr_weeklyactivity
      where week = @week
        and year = @year
      order by pal_qty] catch(-1403)
    |
    publish data
     where seq_pal_qty = 0
    |
    if (@pal_qty > 0)
    {
        [select nvl(max(seq_pal_qty), 0) max_seq_pal_qty
           from usr_weeklyactivity
          where pal_qty > 0
            and year = @year
            and week = @week] catch(@?)
        |
        if (@max_seq_pal_qty = 0)
        {
            publish data
             where seq_pal_qty = 1
        }
        else
        {
            [select max(seq_pal_qty) seq_pal_qty
               from usr_weeklyactivity
              where pal_qty = @pal_qty
                and seq_pal_qty is not null
                and year = @year
                and week = @week
                and rownum < 2] catch(@?)
            |
            if (@seq_pal_qty = 0)
            {
                publish data
                 where seq_pal_qty = @max_seq_pal_qty + 1
            }
            |
            publish data
             where seq_pal_qty = @seq_pal_qty
        }
        |
        publish data
         where seq_pal_qty = @seq_pal_qty
    }
    |
    [update usr_weeklyactivity
        set seq_pal_qty = @seq_pal_qty
      where prtnum = @prtnum
        and week = @week
        and year = @year
        and btcust = @btcust
        and lotnum = @lotnum] catch(-1403)
}
]]>
</local-syntax>
</command>
