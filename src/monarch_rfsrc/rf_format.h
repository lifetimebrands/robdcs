// Standard DCS definitions

#define MSCGENDEF_H	// Suppress unwanted sub-includes
#define MOCAGENDEF_H

#include "dcscolwid.h"  
#include "dcsgendef.h"

// Standard error code definitions
// (defined here because we cannot use standard header files)

#define eDB_NO_ROWS_AFFECTED	-1403

// LMS (Labor Management Systems) code definitions

#define RF_LMSCOD_INDIRECT      "I"
#define RF_LMSTYP_PLACE     "P"

#define RF_ACTCOD_SORTED_PUTAWAY     "PUT_SORT"
#define RF_ACTCOD_DIRECTED_PUTAWAY   "PUT_DRCT"
#define RF_ACTCOD_UNDIRECTED_PUTAWAY "PUT_UNDR"
#define RF_ACTCOD_CROSSDOCK_PUTAWAY  "PUT_XDCK"

// RF menu structure definitions

#define RF_MNU_NAV_BAR_ID       "RF"
#define RF_MNU_OPT_TYP          "O"

// RF menu option definitions (used to detect if option is available)

#define RF_MNU_OPT_STS_CHG	"TOOLS_STS_CHANGE"

// RF terminal size definitions

#ifdef RF_FORMAT_40X8
    #define RF_TERMINAL_LEFT    1
    #define RF_TERMINAL_TOP     1
    #define RF_TERMINAL_WIDTH   40
    #define RF_TERMINAL_HEIGHT  8
#endif

#ifdef RF_FORMAT_20X16
    #define RF_TERMINAL_LEFT    1
    #define RF_TERMINAL_TOP     1
    #define RF_TERMINAL_WIDTH   20
    #define RF_TERMINAL_HEIGHT  16
#endif

// RF form title field definitions

#ifdef RF_FORMAT_40x8
    #define RF_TITLE_LEFT       6
    #define RF_TITLE_TOP        0
    #define RF_TITLE_WIDTH      28
#endif

#ifdef RF_FORMAT_20X16
    #define RF_TITLE_LEFT       6
    #define RF_TITLE_TOP        0
    #define RF_TITLE_WIDTH      15
#endif

// RF form caption & message definitions

#define RF_FORM_CAPTION         ttlBanner,              "dcs"
#define RF_APPLICATION_VERSION  lblVersion,             "version: vX.X"

#define RF_MSG_PROCESSING       "stsProcessing",        "processing request, please wait"

// RF standard function key definitions

#define FKEY_BACK               F1
#define FKEY_BACK_QUOTED        "F1"
#define FKEY_BACK_CAPTION       lblFkeyBack, "back"
#define FKEY_LOOK               F2
#define FKEY_LOOK_QUOTED        "F2"
#define FKEY_LOOK_CAPTION       lblFkeyLook, "lookup"
#define FKEY_ALOC               F3
#define FKEY_ALOC_QUOTED        "F3"
#define FKEY_ALOC_CAPTION       lblFkeyAloc, "allloc"
#define FKEY_OVER               F3
#define FKEY_OVER_QUOTED        "F3"
#define FKEY_OVER_CAPTION       lblFkeyOver, "override"
#define FKEY_CANC               F4
#define FKEY_CANC_QUOTED        "F4"
#define FKEY_CANC_CAPTION       lblFkeyCanc, "cancel"
#define FKEY_DONE               F6
#define FKEY_DONE_QUOTED        "F6"
#define FKEY_DONE_CAPTION       lblFkeyDone, "done"
#define FKEY_TOOL               F7
#define FKEY_TOOL_QUOTED        "F7"
#define FKEY_TOOL_CAPTION       lblFkeyTool, "tools"
#define FKEY_MODE               F8
#define FKEY_MODE_QUOTED        "F8"
#define FKEY_MODE_CAPTION       lblFkeyMode, "mode"
#define FKEY_PQTY               F9
#define FKEY_PQTY_QUOTED        "F9"
#define FKEY_PQTY_CAPTION       lblFkeyPqty, "parqty"
#define FKEY_HELP               F10
#define FKEY_HELP_QUOTED        "F10"
#define FKEY_HELP_CAPTION       lblFkeyHelp, "help"

// RF standard timer definitions

#define RF_LOOK_WORK_TIMEOUT                    60
#define RF_LOOP_TIMER_LENGTH_IN_SECONDS         5
#define RF_SHORT_TIMER_LENGTH_IN_SECONDS        30
#define RF_TIMER_LENGTH_IN_SECONDS              300

// RF standard field height & length definitions

#define RF_FIELD_HEIGHT         1

#define RF_FLAG_LEN             2
#define RF_FORM_NAME_LEN        50
#define RF_GEN_STRING_LEN       50
#define RF_MENU_OPTDSC_LEN      16
#define RF_MENU_OPTNUM_LEN      2
#define RF_QUANTITY_LEN         5
#define RF_FLOAT_LEN            7
#define RF_SHORT_QUANTITY_LEN   3
#define RF_UNTQTY_LEN           8

// RF standard field display width definitions

#define RF_FTPCOD_DSP_WIDTH     20
#define RF_PRTNUM_DSP_WIDTH     20

// RF standard flag value definitions

#define RF_FLAG_NO              "N"
#define RF_FLAG_YES             "Y"

#define RF_FLAG_TRUE            1
#define RF_FLAG_FALSE           0

#define RF_MODE_DIRECTED        "D"
#define RF_MODE_UNDIRECTED      "U"

// RF field text definitions

#define RF_INS_IDENTIFY         "IDN"
#define RF_INS_RECEIVE          "RCV"

#define RF_OPR_UNDIR_IDENTIFY   "UID"
#define RF_OPR_UNDIR_RECEIVE    "URC"
#define RF_OPR_UNDIR_TRANSFER   "UTR"
#define RF_OPR_UNDIR_PICK       "UPK"

#define RF_TXT_ARECOD_RDCK      "RDCK"
#define RF_TXT_CODE4_UNUSED     "----"
#define RF_TXT_UOM_UNUSED       "--"

// RF completion status definitions

#define RF_STS_EXISTS           512

// RF Display Masks

#define DISPLAY_MASK_INVISIBLE		"NNN"
#define DISPLAY_MASK_REVERSE		"YNY"	
#define DISPLAY_MASK_NO_DEFAULT_CHAR	"YYN"
#define DISPLAY_MASK_LOCAL_DATA_ONLY	"NNN"

// RF Entry Masks

#define ENTRY_MASK_INVISIBLE					"NNNNNNNNNN"
#define ENTRY_MASK_ENABLED					"YNNNNNNNNN"
#define ENTRY_MASK_PROTECTED					"YNNYNNNNNN"
#define ENTRY_MASK_NO_ECHO					"YNNNYNNNNN"	
#define ENTRY_MASK_REVERSE					"YNNYNNNNNN"	
#define ENTRY_MASK_INPUT_REQUIRED				"YNNNNNYNNN"
#define ENTRY_MASK_PROTECTED_AND_NO_ECHO_AND_INPUT_REQUIRED	"YNNYYNYNNN"
#define ENTRY_MASK_NO_DEFAULT_CHAR				"YYNNNNNNNN"	
#define ENTRY_MASK_PROTECTED_AND_NO_DEFAULT_CHAR		"YYNYNNNNNN"	
#define ENTRY_MASK_ALPHANUMERIC					"YNNNNNNYNN"	
#define ENTRY_MASK_INTEGER					"YNNNNNNNYN"	
#define ENTRY_MASK_LOCAL_DATA_ONLY				"YNNNNNNNNY"	
