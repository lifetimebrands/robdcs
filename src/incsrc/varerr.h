/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varerr.h,v $
 *  $Revision: 1.6 $
 *  $Author: prod $
 *
 *  Description: VAR-level error code definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#ifndef VARERR_H
#define VARERR_H

/*
 *  VAR Error Codes 80000-89999.
 */

typedef enum VARErrors
{

    eVAR_TOO_MANY_PARTS = 80001,
    eVAR_NO_SUBLOAD_FOUND = 80002,
    eVAR_NOT_WHOLE_CASES = 80003 ,
    eVAR_NO_PRINTER_FOR_LOCATION = 80004,
    eVAR_NO_CARCOD_OR_CSTNUM = 80006,
    eVAR_OPTION_NOT_SUPPORTED_BY_HOST = 80007,
    eVAR_PLAN_SHIP_DATE_BAD = 80008,
    eVAR_SHIPMENTS_MISSING_CARRIER_CODE = 80009,
    eVAR_INVALID_UCC = 80010,
    eVAR_CANNOT_SPLIT_KITS = 80016,
    eVAR_MULTIPLE_SHIPIDS_NOT_ALLOWED = 80017,
    eVAR_INVALID_WAVESET = 80018,
    eVAR_NO_SHIPMENTS_TO_PLAN = 80019,
    eVAR_NO_SHIPMENTS_PLANNED = 80020,
/* the following are place holders for whoever added codes but did not complete
    eVAR_this_code_is_used_in_a_trigger_CODER_UNKNOWN = 80021, 
    eVAR_this_code_is_also_used_in_a_trigger_CODER_UNKNOWN = 80022, 
    eVAR_this_code_is_again_also_used_in_a_trigger_CODER_UNKNOWN = 80023, 
*/
    eVAR_LOD_ALREADY_PENDING = 80024,
    eVAR_SUB_ALREADY_PENDING = 80025, 
    eVAR_DTL_ALREADY_PENDING = 80026,
    eVAR_PCKMOV_PENDING = 80031,
    eVAR_TICKET_INVENTORY_ISNOT_IN_THE_TICKET_AREA = 80032,
    eVAR_SAMPLE_INVENTORY_ISNOT_IN_THE_SAMPLE_AREA = 80033,
    eVAR_INVALID_ID = 80034,
    eVAR_DUPLICATE_VALUE_SMPORD_PROCESSLOC = 80042,
    eVAR_DUPLICATE_VALUE_SMPORD_DESTLOC = 80043,
    eVAR_INVALID_SMPORD_PROCLOC = 80044,
    eVAR_INVALID_SMPORD_DESTLOC = 80045,
    eVAR_DUPLICATE_VALUE_PRTTCKOPR_PROCESSLOC = 80046,
    eVAR_DUPLICATE_VALUE_PRTTCKOPR_DESTLOC = 80047,
    eVAR_INVALID_PRTTCKOPR_PROCLOC = 80048,
    eVAR_INVALID_PRTTCKOPR_DESTLOC = 80049,
    eVAR_CANT_PROCESS_UNTIL_ALL_ARVQTY_SET = 80035,
    eVAR_CANT_PRINT_LABEL_PCKNOTRLS = 80050,
    eVAR_LANE_HAS_INVENTORY = 80051,
    eVAR_PART_ALLOCATION_PERCENT = 80052,
    eVAR_TOO_MANY_LINE_SPLITS = 80053,
    eVAR_NOT_EXPECTED_SUBUCC = 80054,
    eVAR_BAD_SRC_STAGING_LOC = 80055,
    eVAR_BAD_SRC_STAGING_STS = 80056,
    eVAR_MUST_BE_SSTG_AREA = 80057,
    eVAR_SHIPMENT_NOT_IN_LOADING = 80058,
    eVAR_WORK_ALREADY_EXISTS = 80059,
    eVAR_LODNUM_TOO_LONG = 80060,

    eVAR_LAST_ONE_DONT_REMOVE = 89999

} VARErrors;

#endif
