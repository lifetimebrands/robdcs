/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varsock_util.h,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level header file.  Contains utility function for sockets.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#ifndef VARSOCK_UTIL_H__
#define VARSOCK_UTIL_H__


/* use this with CAUTION.  If the array s is not an array but only a
 * char * then the sizeof() would NOT return the size of array but
 * only the size of char *! */
#define ZAP(s)  memset((s), 0, sizeof((s)))

#ifndef TRUE
	#define TRUE    (1)
# endif

#ifndef FALSE
	#define FALSE   (0)
#endif

#define MAYBE  (-1)

#include <oslib.h>
/* this function returns a inter number that corresponds to the underlying socket
 * descriptor.  For unix sustems it is the SOCKET_FD itself as for Unix SOCKET_FD
 * is a typedf for int!  */
int sGetSockFd(const SOCKET_FD fd);

/* This function accepts a socket_Fd and closes that socket. It returns the status
 * of close operation to indicate if close was sucessful or not.  */
long sClose (SOCKET_FD fd);

/* This function taks a socket descriptor and a string and sends out the string
 * over the socket.  It returns eOK if the entire string was sent out sucessfully,
 * else it returns the error. */
long sSend(SOCKET_FD fd, const char *s, int len);

/* The function takes a socket descriptor and a count of number of chars to read
 * from that socket and returns the read characters in the string pointed to by s.
 * It is assumed that s has enough space to hold the incoming message.  It returns
 * eOK if alls well, else returns error code. */
long sReceive(SOCKET_FD fd, int expected_length, char *response, int buffer_size);

/* this function accepts the listner socket fd and a pointer to SOCKET_FD variable.
 * Does a BLOCKING accept on the lisnter and returns the newly accept'ed connetion
 * socket. Since it does a blocking accept, it should be called ONLY if select detects
 * a connection coming in on the listner. */
long sAccept(SOCKET_FD fd, SOCKET_FD *p_new_fd);

/* Moca does not allow to pass around strings with NULLs embedded in them, hence we
 * need to replace any NULLs with an arbiterary character.  We choose to replace it
 * with '\127' because:
 * - firstly it will work with signed chars.
 * - and it is unlikely to be part of a message itself.
 * However, choosing it also means that we cannot allow any message from containing
 * '\127' as part of it, because then we will not know an actual '\127' from the
 * one that is there as a place holder for a NULL. */
/* This function looks at s[0] through s[size] and replaces any occurance of '\0'
 * in s with '\127'.  It returns s. */
char *sMaskNullsInString(char *s, long size);

/* This function looks for any instance of '\127 in s, from s[0] through s[size],
 * and replaces it with '\0'.  So it reverses the effects of the sMaskNullsInString().
 * It returns s. */
char *sChagneIntoOriginalString(char *s, long size);

/* This function copies s_in to s_out as is, except that for every occurance of a 
 * single quote in s_in it put two double quotes in s_out.  This is needed if moca
 * has to correctly handle arguments containing embedded single quotes in it. */
long sMaskQuotes(const char *s_in, long s_in_len, char *s_out, long s_out_len);

/* This function returns the string representation of the current time in the format
 * mm/dd/yyyy HH:MM:SS.  The string is stored in a static duration string inside the
 * function itself. */
const char *sGetTimeString(void);

/* This function takes in a message packet and its length and gives back a static
 * duration string which contains printable string instead of non-printable characters,
 * e.g. an embedded null would get replaced by the string "<NULL>", etc.  The maximum 
 * size of the static duration string is 5K-1. */
const char *sGetPrintablePacket(const char *s, long len);


#endif /* VARSOCK_UTIL_H__  */

