/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varsock_err.h,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level error code definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#ifndef VARSOCK_ERR_H__
#define VARSOCK_ERR_H__

/*
 *  VARSOCK Error Codes 89901-89999.
 */

typedef enum VARSOCKErrors
{

	/* the socket error message would be numbered from 89901 through 89999 */
	eVAR_SOCK_CONNECT_FAILED                       = 89901,
	eVAR_CANNOT_REGISTER_ATEXIT_FUNC               = 89902,
	eVAR_CANNOT_MARSHAL_DATA                       = 89003,
	eVAR_SOCK_ERROR_SENDING_DATA                   = 89004,
	eVAR_SOCK_ERROR_RECEIVING_RESPONSE             = 89005,
	eVAR_SOCK_RECD_INVALID_RESPONSE                = 89006,
	eVAR_SRV_RESULTS_INIT_FAILED                   = 89007,
	eVAR_SRV_RESULTS_ADD_FAILED                    = 89008,
	eVAR_SOCK_LISTEN_FAILED                        = 89009,
	eVAR_SOCK_SELECT_FAILED                        = 89010,
	eVAR_SOCK_ACCEPT_FAILED                        = 89011,
	eVAR_INCOMING_DATA_PROCESSING_ERROR            = 89012,
	eVAR_SOCKET_TRACE_FILE_OPEN_ERROR              = 89013,


} VARSOCKErrors;

#endif  /* VARSOCK_ERR_H__ */

