/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varsock_lh.h,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: Header file contains protocol specific information specific to Lifetime Hoan protocol.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#ifndef VAR_SL_SOCKET_LH_H__
#define VAR_SL_SOCKET_LH_H__

/* This file contains the items specific to the TCP/IP communication protocol
 * used by Lifetime Hoan host systems. */

/* names of other ASCII constants used by the protocol */
static const char STX = 2;
static const char ETX = 3;
static const char ACK = 6;
static const char NAK = 21;

/* size of the ack/nak packet for Lifetime Hoan is fixed and is the same. */
#define ACK_NAK_MSG_LEN       8
#define MIN_MSG_LEN           8
#define MSG_SEQUENCE_LEN      5
#define PREFIX_LEN            6
#define SUFFIX_LEN            1

#endif /* VAR_SL_SOCKET_LH_H__ */

