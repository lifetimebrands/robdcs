/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/vargendef.h,v $
 *  $Revision: 1.6 $
 *  $Author: prod $
 *
 *  Description: VAR-level generic definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#ifndef VARGENDEF_H
#define VARGENDEF_H

#define LES_LABELS  "%LESDIR%" PATH_SEPARATOR_STR "labels"

#define POLVAR_CYCCNT_INVENTORY_ADJ_OPTIONS     "INVENTORY-ADJ-OPTIONS"

#define POLVAL_CYCCNT_DISCREPANCY_LIMIT         "DISCREPANCY-LIMIT"
#define POLVAL_CYCCNT_DISCREPANCY_TYPE          "DISCREPANCY-TYPE"
#define POLVAL_CYCCNT_SHOW_DOLLAR_AMT           "SHOW-DOLLAR-AMT"


/* For UPGRADE */
#define POLCOD_SORT_BY_CLAUSES      "SORT-BY-CLAUSES"
#define SHPSTS_PRINTED 		    "P"


#define POLCOD_PLAN_WAVE			"PLAN-WAVE"
#define POLVAL_LARGE_SHIPMENT			"LARGE-SHIPMENT"

#define WAVE_STATUS_PLANNED                     "PLAN"
#define WAVE_NUMBER_FORMAT                      "0000000"
#define MAX_WAVE_NUMBER                         999

#define POLCOD_ALLOCATE_WAVE                    "ALLOCATE-WAVE"
#define POLVAL_AllOCATE_TYPE                    "ALLOCATE-TYPE"
#define POLVAL_PERFORM_CONSOLIDATION_BY         "PERFORM-CONSOLIDATION-BY"

#define SAMPLE_ORDER_TYPE                       "S"

#define DEFAULT_NOT_NULL_STRING                 "----"

#define YES_NO_STR_LEN				1

#define POLVAR_FLUID_LOAD_LTL			"FLUID-LOAD-LTL"
#define POLVAL_CARRIERS				"CARRIERS"

#define MAX_PASS                                2

#define POLCOD_VAR                               "VAR"
#define POLVAR_CYCLE_COUNTS                      "CYCLE_COUNTS"
#define POLVAL_PHYSICAL_IN_PROCESS               "PHYSICAL-IN-PROCESS"
#endif
