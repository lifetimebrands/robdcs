/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/vartbldef.h,v $
 *  $Revision: 1.5 $
 *  $Author: prod $
 *
 *  Description: VAR-level tablespace definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

/*
 *  The VAR's tablespace definition file can be use to override
 *  the default tablespaces that a table will be created in.
 *
 *  The tablespace a table is created in is determined by using the
 *  tablespace macro as it's defined in the tablespace definition files
 *  using the following order of precedence:
 *
 *      1 - usrtbldef.h
 *      2 - vartbldef.h
 *      3 - <prod>tbldef.h
 *
 *  To use the database user's default tablespace for all tables:
 *
 *      #define USE_DEFAULT_TABLESPACE
 *
 *  To remap a tablespace macro to a different tablespace name:
 *
 *      #ifndef MCS_TBS_DEFS
 *      #define MCS_TBS_DEFS
 *      #define MCS_DATA_TBS_01   MCS_D_01
 *      #define MCS_INDEX_TBS_01  MCS_X_01
 *      #endif
 */

#ifndef VARTBLDEF_H
#define VARTBLDEF_H


#define VAR_INDEX_TBS_01       DCS_X_01
#define VAR_INDEX_TBS_03       DCS_X_03
#define VAR_INDEX_TBS_04       DCS_X_04

#define VAR_DATA_TBS_01        DCS_D_01

/*
 * DF 01/04/01
 * for development, there is only one seamles tablespace, LHDEV_SEAMLES.
 */
#ifndef SL_TBS_DEFS
#define SL_TBS_DEFS

#define SL_INDEX_TBS_01 SL_DATA_EO
#define SL_INDEX_TBS_02 SL_DATA_EVT
#define SL_INDEX_TBS_03 SL_DATA_IFD
#define SL_INDEX_TBS_04 SL_DATA_OTHER
#define SL_INDEX_TBS_05 SL_DEF_EO
#define SL_INDEX_TBS_06 SL_DEF_EVT
#define SL_INDEX_TBS_07 SL_DEF_IFD
#define SL_INDEX_TBS_08 SL_DEF_OTHER

#endif

#ifndef VAR_WAVPAR_HDR_TBL

#define VAR_WAVPAR_HDR_PK_TBLSPC  VAR_INDEX_TBS_01
#define VAR_WAVPAR_HDR_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#define VAR_WAVPAR_HDR_TBL_TBLSPC  VAR_DATA_TBS_01
#define VAR_WAVPAR_HDR_TBL_STORAGE /* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */
#define VAR_WAVPAR_HDR_TBL
#endif

#ifndef VAR_WAVPAR_DTL_TBL

#define VAR_WAVPAR_DTL_PK_TBLSPC  VAR_INDEX_TBS_01
#define VAR_WAVPAR_DTL_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#define VAR_WAVPAR_DTL_TBL_TBLSPC  VAR_DATA_TBS_01
#define VAR_WAVPAR_DTL_TBL_STORAGE /* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */
#define VAR_WAVPAR_DTL_TBL
#endif


#ifndef VAR_ALT_PRTNUM_TBL

#define VAR_ALT_PRTNUM_PK_TBLSPC  VAR_INDEX_TBS_01
#define VAR_ALT_PRTNUM_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#define VAR_ALT_PRTNUM_TBL_TBLSPC  VAR_DATA_TBS_01
#define VAR_ALT_PRTNUM_TBL_STORAGE /* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */
#define VAR_ALT_PRTNUM_TBL
#endif


#ifndef VAR_BKGCMD_TBL

#define VAR_BKGCMD_PK_TBLSPC  VAR_INDEX_TBS_01
#define VAR_BKGCMD_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#define VAR_BKGCMD_TBL_TBLSPC  VAR_DATA_TBS_01
#define VAR_BKGCMD_TBL_STORAGE /* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */
#define VAR_BKGCMD_TBL
#endif


#ifndef VAR_INV_SNAPSHOT_TBL

#define VAR_INV_SNAPSHOT_PK_TBLSPC  VAR_INDEX_TBS_01
#define VAR_INV_SNAPSHOT_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#define VAR_INV_SNAPSHOT_TBL_TBLSPC  VAR_DATA_TBS_01
#define VAR_INV_SNAPSHOT_TBL_STORAGE/* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */
#define VAR_INV_SNAPSHOT_TBL

#define VAR_INV_SNAPSHOT_IDX1_TBLSPC  VAR_INDEX_TBS_01
#define VAR_INV_SNAPSHOT_IDX1_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */

#endif

#ifndef VAR_SHPORD_TBL    

#define VAR_SHPORD_PK_TBLSPC  VAR_INDEX_TBS_01    
#define VAR_SHPORD_PK_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */    

#define VAR_SHPORD_TBL_TBLSPC  VAR_DATA_TBS_01    
#define VAR_SHPORD_TBL_STORAGE /* STORAGE (INITIAL 5M NEXT 5M PCTINCREASE 0) */ 
#define VAR_SHPORD_TBL    
#endif

#ifndef VAR_STOP_DOCNUM_IDX

#define VAR_STOP_DOCNUM_IDX_TBLSPC VAR_INDEX_TBS_01
#define VAR_STOP_DOCNUM_IDX_STORAGE /* STORAGE (INITIAL M NEXT M PCTINCREASE 0) */    

#define VAR_STOP_DOCNUM_IDX
#endif

#endif
