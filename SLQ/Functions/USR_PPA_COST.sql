CREATE OR REPLACE FUNCTION PROD.Usr_Ppa_Cost
(in_ship_id SHIPMENT.ship_id%TYPE,
 in_cponum  ORD.cponum%TYPE,
 in_paytrm  ORD.paytrm%TYPE)
RETURN NUMBER
IS
  v_cst NUMBER(12,2) := NULL;
BEGIN
  -- IF PO IS REPLACEMENT OR THIS IS NOT A PA LEAVE
  IF in_cponum LIKE '%REPL%' OR nvl(in_paytrm, 'NOTPA') != 'PA'
  THEN
    RETURN 0.00;
  ELSE 
    -- Try to get info from the MANIFEST table
    SELECT SUM(DECODE(
              m.carcod,'UPS',  m.frtrte * 1.25,
                       'UPSG', m.frtrte * 1.25,
                       'UPSS', m.frtrte * 1.25,
                       'LANDMS', m.frtrte * 1.2,
                       'UPSTD', m.frtrte * 1.6,
                               m.frtrte * 1.25 )) 
    INTO v_cst
    FROM MANFST m WHERE m.ship_id = in_ship_id;
    
    -- If not in MANIFEST table try to get from USR_SHPCOSTS table
    IF v_cst IS NULL
    THEN
      SELECT u.shpcost
        INTO v_cst 
        FROM USR_SHPCOSTS u 
       WHERE u.ship_id = in_ship_id;
    END IF;
    
    RETURN ROUND(NVL( v_cst, 0.00 ), 2);
  END IF;
EXCEPTION
WHEN OTHERS THEN
  RETURN 0.00;
END;
/
